#!/usr/bin/python3
# pip install PySDL2

"""
~158km, hracka, asi 3 zastavky.
196km,  regulator 15, coal 737, hot ~400, 
103km, regulator 40, all max
"""

import sdl2
import sdl2.ext
import random
import time
hid = True
if hid:
    import rail

# Game Constants
WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480
PLAYER_WIDTH = 40
PLAYER_HEIGHT = 40
GROUND_LEVEL = WINDOW_HEIGHT - PLAYER_HEIGHT
OBSTACLE_WIDTH = 20
OBSTACLE_HEIGHT = 50
OBSTACLE_SPEED = 5
JUMP_VELOCITY = 15
GRAVITY = 1

# Initialize SDL
sdl2.ext.init()
window = sdl2.ext.Window("Iron horse", size=(WINDOW_WIDTH, WINDOW_HEIGHT))
window.show()
renderer = sdl2.ext.Renderer(window)

# Player attributes
player_x = 100
player_y = GROUND_LEVEL
score = 0

# Obstacle attributes
obstacles = []
obstacle_spawn_time = 5  # Seconds between spawns
last_obstacle_time = time.time()

class Steam:
    coal_hot = 100
    coal = 0
    coal_store = 20000
    water = 50
    water_temp = 95
    water_store = 20000
    steam = 0
    engine_weight = 50000
    train_weight = 100000

    brake = 0
    brake_manual = 0
    brake_control = 50

    air = 30
    regulator = 10
    auto_coal = 200

    speed = 0
    distance = 0

    auto_protect = True

    coal_energy = 0

    def show(m):
        print()
        print("Coal:  hot %.0f ready %.0f (auto %.0f) air %.0f store %.0f" % (m.coal_hot, m.coal, m.auto_coal,  m.air, m.coal_store))
        print("Water: hot %.0f temp %.0f store %.0f" % (m.water, m.water_temp, m.water_store))
        print("Steam:     %.0f" % (m.steam))
        print("Regulator: %.0f reverser %.0f" % (m.regulator, 0))
        print("Brake:     %.0f control %.0f manual %.0f" % (m.brake, m.brake_control, m.brake_manual))
        print("Speed:     %.2f distance %.0f" % (m.speed, m.distance))
        if m.water < 30:
            print("\a")

    def single_coal(m):
        l = 20
        if m.coal_store > l and m.coal + m.coal_hot < 2000:
            m.coal += l
            m.coal_store -= l

    def single_water(m):
        l = 20
        # FIXME: We should cool down rest of the water!
        if m.water_store > l and m.water < 2000:
            m.water_temp = m.water_temp * m.water / (l + m.water)
            m.water += l
            m.water_store -= l
            
    def apply_brake(m, strength, pressure):
        damp = 0.02 * strength / 45
        m.brake = m.brake * (1-damp) + pressure * damp
                
    def step(m):
        print("\n---")
        dt = 0.1
        air = m.air / 100
        if air < 0.1:
            air = 0.1
        if m.air > 0.5:
            air = 0.5
        energy = m.coal_hot / 100 * dt
        energy *= 2 * air

        damp = 0.001
        energy = m.coal_energy * (1-damp) + energy * damp
        if energy > m.coal_hot:
            energy = m.coal_hot
        m.coal_energy = energy
        m.coal_hot -= energy
        print("Coal use: %.2f" % (energy))

        burning = (m.coal_hot * m.coal) / 1000000 + m.coal_hot / 1500
        burning *= 2 * air
        if burning < 0.1:
            burning = 0.1
        if burning > m.coal:
            burning = m.coal
        m.coal -= burning
        m.coal_hot += burning

        if m.water:
            m.water_temp += (energy * 100) / m.water
        while m.water_temp > 100 and m.water > 1:
            m.water_temp -= 5
            steam = m.water / 30
            m.water -= steam
            m.steam += steam
        if m.water_temp > 200:
            print("\n\n\n*** Boiler destroyed")
            m.water = 0
            m.water_store = 0
            m.steam = 0
        m.water_temp *= 0.9999

        energy = m.regulator / 100 * 0.1 * m.steam * dt
        m.steam -= energy
        print("Steam use: %.2f" % (energy))

        if m.brake_control >= 55:
            m.apply_brake(m.brake_control-55, m.steam)

        if m.brake_control <= 45:
            m.apply_brake(45-m.brake_control, 0)

        force = energy
        braking = 1		# some kind of independend resistance
        braking += m.speed*m.speed / 100	# air resistance
        braking += m.brake_manual / 5
        braking += m.brake
        braking *= 0.01
        
        m.speed += 150000 * force / (m.engine_weight + m.train_weight)
        if braking > m.speed:
            braking = m.speed
        m.speed -= braking
        m.distance += m.speed * dt

        if m.steam > 200:
            print("\n\n\n\a*** Emergency steam release")
            m.steam = 200

        if m.auto_protect and m.water < 50:
            m.single_water()
        if m.auto_coal and (m.coal_hot + m.coal) < m.auto_coal:
            m.single_coal()

    def adj_auto_coal(m, step):
        m.auto_coal += step
        if m.auto_coal < 0:
            m.auto_coal = 0
        if m.auto_coal > 2000:
            m.auto_coal = 2000

    def adj_regulator(m, step):
        m.regulator += step
        if m.regulator < 0:
            m.regulator = 0
        if m.regulator > 100:
            m.regulator = 100

    def adj_air(m, step):
        m.air += step
        if m.air < 0:
            m.air = 0
        if m.air > 100:
            m.air = 100

    def adj_brake_manual(m, step):
        m.brake_manual += step
        if m.brake_manual < 0:
            m.brake_manual = 0
        if m.brake_manual > 100:
            m.brake_manual = 100
            
    def adj_brake_control(m, step):
        m.brake_control += step
        if m.brake_control < 0:
            m.brake_control = 0
        if m.brake_control > 100:
            m.brake_control = 100

    def set_auto_coal(m, val):
        v1 = 85
        v2 = 180
        v = (val-v1) / (v2-v1)
        v *= 2000
        m.auto_coal = v
        m.adj_auto_coal(0)

    def set_air(m, val):
        v1 = 85
        v2 = 180
        v = (val-v1) / (v2-v1)
        v *= 100
        m.air = v
        m.adj_air(0)

    def set_regulator(m, val):
        v1 = 85
        v2 = 180
        v = (val-v1) / (v2-v1)
        v *= 100
        m.regulator = 100 - v
        m.adj_regulator(0)
        
    def set_brake_control(m, val):
        v1 = 45
        v2 = 220
        v = (val-v1) / (v2-v1)
        v *= 100
        m.brake_control = v
        m.adj_brake_control(0)
        
class Game:
    def __init__(m):
        if hid:
            m.rd = rail.RailDriver()
        m.steam = Steam()

    # Reset game
    def reset_game(m):
        global player_x, player_y, score, obstacles
        player_x = 100
        player_y = GROUND_LEVEL
        m.player_velocity_y = 0
        score = 0
        obstacles = []
        m.is_jumping = False

    # Spawn a new obstacle
    def spawn_obstacle(m):
        x_pos = WINDOW_WIDTH  # Spawn off-screen to the right
        y_pos = GROUND_LEVEL
        obstacles.append((x_pos, y_pos))

    def input_sdl(m):
        # Event handling
        for event in sdl2.ext.get_events():
            if event.type == sdl2.SDL_QUIT:
                m.running = False
                break
            elif event.type == sdl2.SDL_KEYDOWN:
                if event.key.keysym.sym == sdl2.SDLK_SPACE and not m.is_jumping:
                    # Start jump
                    m.is_jumping = True
                    m.player_velocity_y = -JUMP_VELOCITY
                if event.key.keysym.sym == sdl2.SDLK_q:
                    m.steam.adj_auto_coal(50)
                if event.key.keysym.sym == sdl2.SDLK_a:
                    m.steam.adj_auto_coal(-50)
                if event.key.keysym.sym == sdl2.SDLK_z:
                    m.steam.single_coal()
                if event.key.keysym.sym == sdl2.SDLK_x:
                    m.steam.single_water()
                if event.key.keysym.sym == sdl2.SDLK_e:
                    m.steam.adj_air(5)
                if event.key.keysym.sym == sdl2.SDLK_d:
                    m.steam.adj_air(-5)
                if event.key.keysym.sym == sdl2.SDLK_r:
                    m.steam.adj_regulator(5)
                if event.key.keysym.sym == sdl2.SDLK_f:
                    m.steam.adj_regulator(-5)
                if event.key.keysym.sym == sdl2.SDLK_i:
                    m.steam.adj_brake_control(1)
                if event.key.keysym.sym == sdl2.SDLK_k:
                    m.steam.adj_brake_control(-1)
                if event.key.keysym.sym == sdl2.SDLK_o:
                    m.steam.adj_brake_manual(.1)
                if event.key.keysym.sym == sdl2.SDLK_l:
                    m.steam.adj_brake_manual(-.1)

    def input_rail(m):
        prev = None
        while True:
            data = m.rd.read_data()
            print("Loop")
            if data == None:
                break
            prev = data
            break
        print(0, prev[0], 1, prev[1], 2, prev[2], 3, prev[3], prev[4], 5, prev[5], 6, prev[6], 7, prev[7])
        m.steam.set_brake_control(prev[1])
        m.steam.set_regulator(prev[2])
        m.steam.set_auto_coal(prev[5])
        m.steam.set_air(prev[6])
                    
    def run(m):
        global player_x, player_y, score, obstacles, last_obstacle_time
        m.running = True
        m.is_jumping = False

        # Main Game Loop
        while m.running:
            t = time.time()
            m.input_sdl()
            if hid:
                m.input_rail()
            t1 = time.time() - t

            m.steam.step()
            m.steam.show()
            
            # Update game state
            # Handle jumping physics
            if m.is_jumping:
                player_y += m.player_velocity_y
                m.player_velocity_y += GRAVITY
                if player_y >= GROUND_LEVEL:
                    player_y = GROUND_LEVEL
                    m.is_jumping = False
                    m.player_velocity_y = 0

            # Move obstacles left
            # FIXME
            obstacles = [(x - int(m.steam.speed), y) for x, y in obstacles if x > -OBSTACLE_WIDTH]

            # Check for collision with obstacles
            player_rect = sdl2.SDL_Rect(player_x, int(player_y), PLAYER_WIDTH, PLAYER_HEIGHT)
            for obs_x, obs_y in obstacles:
                obstacle_rect = sdl2.SDL_Rect(obs_x, obs_y, OBSTACLE_WIDTH, OBSTACLE_HEIGHT)
                # FIXME
                if False and sdl2.SDL_HasIntersection(player_rect, obstacle_rect):
                    m.reset_game()
                    break

            # Increment score and spawn obstacles periodically
            score += 1
            if time.time() - last_obstacle_time > obstacle_spawn_time:
                m.spawn_obstacle()
                last_obstacle_time = time.time()

            # Rendering
            renderer.clear(sdl2.ext.Color(135, 206, 235))  # Clear screen to sky blue
            renderer.fill((player_x, int(player_y), PLAYER_WIDTH, PLAYER_HEIGHT), sdl2.ext.Color(255, 0, 0))  # Draw player
            for obs_x, obs_y in obstacles:
                renderer.fill((obs_x, obs_y, OBSTACLE_WIDTH, OBSTACLE_HEIGHT), sdl2.ext.Color(0, 0, 0))  # Draw obstacles
            renderer.present()

            t = time.time()-t
            print("Time: ", t, t1)
            # Frame rate control
            time.sleep(0.016)  # ~60 FPS

g = Game()
g.reset_game()
g.run()

# Clean up
sdl2.ext.quit()

