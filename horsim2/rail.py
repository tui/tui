#!/usr/bin/python3

# 05f3 00d2
# pip3 install hidapi
# sudo chown pavel /dev/hidraw3
import hid
import time

class RailDriver:
    MAX_STR = 255
    VID = 0x5f3  # Vendor ID
    PID = 0x00d2  # Product ID

    def __init__(self):
        self.handle = None
        self.debug = False
        self.connect()

    def msleep(self, milliseconds):
        time.sleep(milliseconds / 1000)

    def connect(self):
        # Initialize and connect to the device
        try:
            self.handle = hid.device()
            self.handle.open(self.VID, self.PID)
            print("Device opened successfully.")
        except IOError as e:
            print("Unable to open device:", e)
            return

        # Display device information
        if self.debug:
            print("Manufacturer String:", self.handle.get_manufacturer_string())
            print("Product String:", self.handle.get_product_string())
            print("Serial Number String:", self.handle.get_serial_number_string())
            print("Indexed String 1:", self.handle.get_indexed_string(1))

    def display(self, v1, v2, v3):
        buf = [0] * 5
        buf[0] = 0
        buf[1] = 134
        buf[2] = v1
        buf[3] = v2
        buf[4] = v3

        try:
            self.handle.write(bytes(buf))
            self.msleep(30)
        except IOError as e:
            print("Error writing to device:", e)

    def read_data(self):
        try:
            data = self.handle.read(13)
            return data
        except IOError as e:
            print("Error reading from device:", e)
            return None

    def run(self):
        # Main loop
        t = 0
        while True:
            self.display((t >> 16) & 0xFF, (t >> 8) & 0xFF, t & 0xFF)
            t += 1
            self.msleep(10)

            # Read the state from the device
            data = self.read_data()
            if data:
                # Print out the returned buffer
                for i in range(13):
                    print(f"buf[{i}]: {data[i]}")
            else:
                print("Failed to read data.")
            self.msleep(100)
                
    def close(self):
        if self.handle:
            self.handle.close()
            print("Device closed.")

    def __del__(self):
        # Cleanup on object deletion
        self.close()

# Example usage
if __name__ == "__main__":
    rail_driver = RailDriver()
    try:
        rail_driver.run()
    except KeyboardInterrupt:
        print("Program interrupted.")
    finally:
        rail_driver.close()

