Je 10teho dubna... a vypada to ze si muzem vybrat svuj osud na pristi mesice. Muzeme si vybrat jatka ted, par mesicu vezeni a jatka potom, nebo 18 mesicu vezeni a snad ockovani, no a nebo nejakou formu chytre karanteny.

A nebo mozna uplne hloupou karantenu. Treba to bude stacit. (Na telefonech bez Androidu/iOSu stejne nic jinyho nebude).

Krok 1: V telefonu zapnout Bluetooth, zapnout "discoverable" rezim, a nastavit jmeno zarizeni na "Covid moje@docasna.mailova.adresa" nebo "Covid +420123456789".

Krok 2: Na chytrem telefonu neni problem neni problem periodicky scanovat okoli, a vysledky ukladat pro pozdejsi zpracovani. Jednou za 3 minuty by melo stacit... Pokud mam stesti a zadnej virus nechytim, nejspis ani nic zpracovavat nebudu, a tady koncime.

(scanovaci script pro Linux a N900 planuju vytvorit)

Krok 3: Pokud mam smulu, projdu zaznamy, a pokusim se kontaktovat lidi kteri se tam v kriticke dobe prilis opakuji; meli by zpozornet, mozna par hodin nelibat babicky a mozna se dokonce zavrit v mistnosti a objednat testovani...

Krok 4: Slo by vytvorit sdilenou databazi obsahujici MAC adresy a/nebo jmena zarizeni nakazenych lidi, takze Ti, kteri maji pusten scanovaci script, muzou zkusit overit jestli se potkali s nekym nakazenym.

(tenhle krok neplanuju delat sam, ale rad s nim nejak pomuzu)



Jenze chytra karantena je tezka, takze:

Uplne blba karantena
====================

Chytra karantena vyzaduje spolupraci hygieniku, armady, ministerstva zdravotnictvi, Googlu, Applu a kdovi koho jeste.. a pak taky potrebuje chytrej telefon. Nerikam ze se to nemuze podarit, ale je to... docela hodne spoluprace.

Ale venku je uplne blbej virus; nestacila by uplne blba karantena?

Jaka je minimalni nutna funkcionalita hloupeho zarizeni? Stacilo by vysilat nejake rozume unikatni ID. Kdyz potom nekdo chce sledovat moji polohu, staci ta ID sbirat, a bylo by dobre aby mel dost informaci k tomu aby me kontaktoval.

Aha, a presne tohle Bluetooth umi: v "discoverable" rezimu vysila svoji MAC adresu a nastavitelny retezec. Takze co ho nastavit na "Covid moje@docasna.mailova.adresa" nebo "Covid +420kartakoupenanarohu"? Jednoduche, mnoho telefonu to umi (paradoxne na tech novejsich to byva tezsi), a mohlo by to stacit. Umi to vykopavky jako nokia 6230, stoji par korun, vydrzi na baterii tyden...

[Bylo by mozne nekde udrzovat databazi MAC adresa -- kontaktni udaj, protoze napriklad chytre hodinky nejspis nebudou snadno podporovat zmenu retezce; potom by bylo nejspis mozne vyuzit i Bluetooth LE.]

No dobra, takze ted me mohou sledovat -- s pomoci ted uz chytreho telefonu, a kdyz zjisti ze jsem se vyskytoval v zamorene oblasti, mohou me kontaktovat. Dobry zacatek. Take me mohou kontaktovat ruzni podvodnici, ale treba si daji prazdniny. MAC adresa je plus/minus unikatni, takze ruzni marketaci mohou sledovat muj pohyb... No, co se da delat. (Lepsi nez vezeni.)

Ale tohle je jen jedna polovina -- sice umoznuju okoli sledovat me, ale nemel bych take ja sledovat okoli?

Nastesti druha polovina je take jednoducha:

Na chytrem telefonu neni problem neni problem periodicky scanovat okoli, a vysledky ukladat pro pozdejsi zpracovani. Jednou za 3 minuty by melo stacit... Pokud mam stesti a zadnej blbej virus nechytim, nejspis ani nic zpracovavat nebudu. Pokud mam smulu, projdu zaznamy, a pokusim se kontaktovat lidi kteri se tam v kriticke dobe prilis opakuji; meli by zpozornet, mozna par hodin nelibat babicky a mozna se dokonce zavrit v mistnosti a objednat testovani...

Tak ono nic statniho nebude potreba, pokud bude dobre fungovat cokoliv jineho. V nejjednodussi versi by stacilo zapnout bluetooth, a jako jmeno stanice vyplnit "covid +420123456"...