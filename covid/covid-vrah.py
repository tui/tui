#!/usr/bin/python3

class Country:
    start = 2020.03
    data = ( 150, 300, 600, 1200, 2500, 5000,
    #        09                          01
             10000, 20000, 40000, 30000, 60000, 120000, 240000, 480000,
             # 5
             300000, 200000 )

    def adj(m, i):
        i += 0.01
        if i > 2020.125 and i < 2021:
            i = 2021.01
        return i

    def run(m, me):
        i = m.start
        j = 0
        inf = 0
        my = 0
        inf_my = 0

        while j < len(m.data):
            monthly = m.data[j]
            if me == i:
                my = 1. / monthly
            inf += monthly
            inf_my += my * monthly
            #print(i, inf, "infected", "%.0f" % inf_my, "my infections", "%.1f" % (inf_my * 0.02), "my kills")
            j += 1
            i = m.adj(i)

        print("If someone was infected in ", me, " he infected ", inf_my, " people and ", inf_my * 0.02, " of them died.")

    def run_all(m):
        i = 2020.04
        while i < 2021.08:
            m.run(i)
            i = m.adj(i)

class Czechia(Country):
    #          Mar 31                      8/
    deaths_cumul = ( 23, 236, 320, 348, 379, 424, 655, 3078, 8295, 11580,
    #        1/2021
                   16211, 20399 )
    
    #          Mar 31          May 31                    Sep
    data_cumul = ( 3308, 7682, 9268, 11954, 16371, 24618, 70763, 323673, 523000, 718000,
                   # Jan                     Apr
                   984000, 1240000, 1530000, 1630000, 1660000, 1670000 )

    def __init__(m):
        m.data = ()
        i = m.data_cumul[0]
        for j in m.data_cumul[1:]:
            m.data = m.data + ( j-i )
            i = j

c = Country()
#c.run(2020.03)
c.run_all()
