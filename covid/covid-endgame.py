#!/usr/bin/python3

celkem = 10000000
mrtvych = 30000
r0_max = 8
# FIXME: tady by to chtelo odhadnout r bez opatreni.
r_cur = 1.36
# Optimisticky predpokladame, ze vsechny vekove skupiny jsou
# proockovane tak jako 70-79+ k 10.7. (coz je maximum).
oockovano = 0.83

def zkus_cfr(cfr):
    print("Cfr: ", cfr)
    imunnich = mrtvych / cfr
    print("... imunnich: ", imunnich)
    if imunnich > celkem:
        print("... to je nesmysl, virus by nakazil vsechny")
        return
    if imunnich / celkem > 1 - 1/r0_max: 
        print("... to je nesmysl, virus by davno skoncil")
        return
    if imunnich / celkem > (1 - 1/r0_max)/r_cur: 
        print("... to je nesmysl, virus by se nemohl sirit takhle rychle")
        return
    # Optimisticky predpokladame ze ockujeme pouze ne-imunni
    nachylnych = celkem - imunnich - (oockovano * celkem)
    print("... nachylnych: ", nachylnych)
    print("... zemre: ", nachylnych * cfr)

    nachylnych = (celkem - imunnich) * (1-oockovano)
    print("... nachylnych: ", nachylnych)
    print("... zemre: ", nachylnych * cfr)


zkus_cfr(0.003)
zkus_cfr(0.004)
zkus_cfr(0.0043)
zkus_cfr(0.0046)
zkus_cfr(0.005)
zkus_cfr(0.006)
zkus_cfr(0.007)
zkus_cfr(0.008)
zkus_cfr(0.01)
zkus_cfr(0.013)
zkus_cfr(0.016)
zkus_cfr(0.018)
zkus_cfr(0.0182) # CFR v CR dle ourworldindata k 19.7
zkus_cfr(0.02)
zkus_cfr(0.025)
zkus_cfr(0.03)
zkus_cfr(0.035)
zkus_cfr(0.04)
