#!/usr/bin/python3

import datetime

class Sim:
    def __init__(m):
        m.init()

    def init(m):
        m.today = datetime.date(2021, 9, 17)
        # FIXME: jsem optimista a predpokladam ze imunita neklesa
        m.populace =  10701777
        m.ockovanych = 5840144
        m.imunnich =   2000000    # imunnich z neockovanych. FIXME -- jak odhadnout?
        m.krok = 5                # pocet dni
        m.nakazenych = 367 * m.krok
        m.ockovanych_za_krok = (185000 / 30.)*m.krok         # upraveno podle udaju zari..listopad. Ale ono jich ted bude pribyvat.
        m.R1 = 1.42 * 3.7	# R0 se zapocitanym efektem opatreni, ale bez efektu promoreni 
        
    def run(m, dnu):
        den = 0
        nakazeni = []
        nakazenych = m.nakazenych
        imunnich = m.imunnich
        ockovanych = m.ockovanych
        R1 = m.R1
        zemrelo = 0
        today = m.today
        
        while den < dnu:
            den += m.krok
            today += datetime.timedelta(days = m.krok)

            imunnich += 2*nakazenych # Spousta nakazenych je bezpriznakova a testy je neodhali
            S = (m.populace - ockovanych - imunnich)
            R = R1 * (S / m.populace)
            R1 = R1 * 1.02  # teplota do konce roku klesne o 20C. FIXME. 1.02 se zda byt rozumny udaj?
            zemrelo_krok = 0.0059 * nakazenych # hmm. ono by to chtelo simulaci podle veku... protoze v podzimni vlne je hodne nakazenych deti.
            zemrelo += zemrelo_krok
            nakazeni += [ nakazenych ]
            nemocnice = 0.030 * sum(nakazeni[-3:]) # Podle nejakych odhadu do nemocnic jdou 4% nakazenych.
            nakazenych *= R
            ockovanych += m.ockovanych_za_krok
            M = 1000000
            print("%d.%d." % (today.day, today.month), "den", den, "R1 %.2f" % R1, "R %.2f" % R, "nakazenych %.0f/d" % (nakazenych / m.krok), "ockovanych %.2fM" % (ockovanych / M), "S %.2f" % (S / M), "zemrelo %.0f/d" % (zemrelo_krok / m.krok), "nemocnice %.0f" % nemocnice )
            # Za zahlceni nemocnic se povazuje 10000 pacientu, IIRC.
s = Sim()
s.run(90+20)

# https://www.idnes.cz/zpravy/domaci/koronavirus-covid-19-srovnani-cisla-nakazeni-ockovani.A211004_095117_domaci_indr
# Ze zacatku rijna: V nemocnicích modely Ústavu zdravotnických informací a statistiky odhadují na konci měsíce kolem 500 pacientů s covidem-19, denně pak kolem 30 nových.
# 

# https://cnn.iprima.cz/epidemie-dal-sili-na-konci-rijna-muze-byt-2000-pripadu-denne-varuje-sef-uzis-dusek-35024?fbclid=IwAR0xNpCOpiGFH4pdIiIA1UCBUvBJ08dXVAaMckKXhISOqKaeuq8zDSFSIhM
# Na konci října může být podle predikcí Ústavu zdravotnických informací a statistiky (ÚZIS) při podobné rychlosti šíření epidemie kolem dvou tisíc nových případů covidu denně, v pondělí jich bylo přes 800. V nemocnicích modely odhadují na konci měsíce kolem 500 pacientů s koronavirem, denně bude přibývat dalších 30 nových. V současné době je v nemocnici s covidem 200 lidí a 53 na jednotkách intenzivní péče. V úterý to řekl ředitel ÚZIS Ladislav Dušek.

