#!/usr/bin/env python3

import time, os, sys

class Dialog:
    def start_dialog(m, title):
        m.title = title
        m.actions = {}
        return 0

class MenuDialog(Dialog):
    def add_action(m, name, attrib):
        attrib["name"] = name
        m.actions[ attrib["key"] ] = attrib
        return 0

    def run_blocking(m):
        cmd = """
mode: menu
widgets:
  - label: %s
    type: label
""" % m.title

        print ("")
        print ("")
        print ("")
        print (str(m.title))
        print ("--------------------------------------------------------------------")
        for k in m.actions:
            a = m.actions[k]
            print("(", a["key"], ") ", a["name"]) # "--", a["description"]
            cmd += """
  - label: %s
    type: button
    action: {key: [ %d, 13 ], type: send-key}
""" % (a["name"], ord(a["key"]))
        print ("--------------------------------------------------------------------")
        igi_cmd(cmd)
        while True:
            c = input("Choice? ")
            if c in m.actions:
                break
            print("Invalid choice.")
        igi_cmd("""
mode: terminal
""")
        return m.actions[c]["name"]

def igi_cmd(s):
    os.system("echo '%s' > $IGI &" % s)
    #print(chr(7))

def big_font():
    igi_cmd("""
modify:
  vte:
    font_scale: 2
    """)

def run_tetris():
    igi_cmd("""
modify:
  vte:
    font_scale: 0.5
widgets:
  - action: {key: [ 106 ], type: send-key}
    label: left
    type: button
  - action: {key: [ 107 ], type: send-key}
    label: rotate
    type: button
  - action: {key: [ 108 ], type: send-key}
    label: right
    type: button
  - action: {key: [ 32 ], type: send-key}
    label: down
    type: button
""")
    #os.system("tetris-bsd")
    os.system("tint -l 1")

class Run:
    mypath = "/my/"
    def __init__(m, cmd):
        os.system(cmd)

class RunGraphics(Run):
    def __init__(m, path, cmd):
        os.system("chdir "+m.mypath+path+" ; "+cmd)

class Submenu:
    def __init__(m):
        m.run()

    def run(m):
        pass

class CameraMenu(Submenu):
    def display_size(m, free, width, height):
        minutes = free/ (width*height*60*30/4)
        print(", %.1f min @" % minutes, height, "p", end = "")
    
    def display_sizes(m):
        result=os.statvfs('/tmp')
        block_size=result.f_frsize
        total_blocks=result.f_blocks
        free_blocks=result.f_bfree
        giga=1024*1024*1024.
        total_size=total_blocks*block_size/giga
        free_size=free_blocks*block_size
        print("%.02f" % (free_size/giga), 'GiB free', end = "")
        m.display_size(free_size, 1920, 1080)
        m.display_size(free_size, 1280, 720)
        m.display_size(free_size, 640, 480)
        m.display_size(free_size, 320, 240)
        print()

    def mpegize(m, s):
        RunGraphics("tui/ucam", "./mpegize.py " + s)
    
    def run(m):
        while True:
            iface = MenuDialog()
            iface.start_dialog("Camera menu")
            iface.add_action("photo", { 'key': 'p', 'description': 'take a photo', 'path': 'camera|' } )
            iface.add_action("video 1080p", { 'key': '0', 'description': 'unicsy camera', 'path': 'camera' } )
            iface.add_action("video 720p", { 'key': '7', 'description': 'unicsy camera', 'path': 'camera' } )
            iface.add_action("video 480p", { 'key': '4', 'description': 'unicsy camera', 'path': 'camera' } )
            iface.add_action("video 240p", { 'key': '2', 'description': 'unicsy camera', 'path': 'camera' } )
            iface.add_action("gc", { 'key': 'g', 'description': '', 'path': 'camera' } )
            iface.add_action("convert", { 'key': 'c', 'description': '', 'path': 'camera' } )
            iface.add_action("remove", { 'key': 'r', 'description': '', 'path': 'camera' } )
            iface.add_action("quit", { 'key': 'q', 'description': 'return', 'path': 'camera' } )
            m.display_sizes()
            r = iface.run_blocking()
            print("Got action >> ", r, " << ")
            if r == "photo": RunGraphics("gigapixels", "build/megapixels")
            elif r == "video 1080p": RunGraphics("tui/ucam", "./run 1080")
            elif r == "video 720p": RunGraphics("tui/ucam", "./run 720")
            elif r == "video 480p": RunGraphics("tui/ucam", "./run 480")
            elif r == "video 240p": RunGraphics("tui/ucam", "./run 240")
            elif r == "gc": m.mpegize("gc")
            elif r == "convert":
                m.mpegize("convert")
                os.system("mv -v /tmp/delme.smo/* $HOME/Picture")
            elif r == "remove":
                os.system("rm -ri /tmp/delme.sm*")
            elif r == "quit": return
            else: print("Something got confused?")

def selftests():
    big_font()
    igi_cmd("""
mode: terminal
widgets:
  - label: Starting up
    type: label
""")
    print("Starting up...")
    time.sleep(1)
    print("Preparing food for unicorns...")
    time.sleep(1)
    print("Checking uniform hooves...")
    time.sleep(1)
    print("...all done")
    igi_cmd("""
mode: terminal
widgets:
  - label: Working hard
    type: label
""")
    print("Connecting somewhere...")
    time.sleep(1)
    print("Downloading something...")
    time.sleep(1)
    print("Working hard...")
    time.sleep(1)
    print("Hardly working?")
    time.sleep(1)
    print("...all done")

def main_menu():
    while True:
        if False:
            selftests()

        iface = MenuDialog()
        iface.start_dialog("Main menu")
        iface.add_action("foxtrotgps", { 'key': 'f', 'description': 'display a map', 'path': 'world|' } )
        iface.add_action("nowcast", { 'key': 'n', 'description': 'get weather nowcast from radar', 'path': 'world|' } )
        #iface.add_action("grec", { 'key': 'r', 'description': 'record a video', 'path': 'camera|' } )
        iface.add_action("cam", { 'key': 'c', 'description': 'unicsy camera', 'path': 'camera' } )
        iface.add_action("tetris", { 'key': 'Q', 'description': 'play tetris', 'path': 'games|' } )
        iface.add_action("offlineimap", { 'key': 'o', 'description': 'sync e-mail', 'path': 'net|' } )
        iface.add_action("web", { 'key': 'w', 'description': 'web browser', 'path': 'net|' } )
        iface.add_action("compass", { 'key': 'm', 'description': 'compass', 'path': 'world|' } )
        iface.add_action("quit", { 'key': 'q', 'description': 'return', 'path': 'camera' } )

        #iface.add_action("say-time", { 'key': 't', 'description': 'get current time' })
        #iface.add_action("set-alarm", { 'key': 's', 'description': 'set alarm' })
        #iface.add_action("exit", { 'key': 'x', 'description': 'exit' })
        r = iface.run_blocking()
        print("Got action >> ", r, " << ")
        if r == "foxtrotgps": RunGraphics("foxtrotgps", "DISPLAY=:0.0 src/foxtrotgps --gui=data/foxtrotgps.glade")
        elif r == "nowcast": RunGraphics("tui/rnowcast", "cargo run --release")
        elif r == "grec": RunGraphics("tui/pp", "./grec.py")
        elif r == "cam": CameraMenu()
        elif r == "tetris": run_tetris()
        elif r == "offlineimap": Run("offlineimap")
        elif r == "web": Run("firefox file:///my/tui/ofone/home.html &")
        elif r == "compass": RunGraphics("tui/tricorder", "./sphere.py")
        elif r == "quit": return
        else: print("Something got confused?")

main_menu()
