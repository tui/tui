#!/usr/bin/env python3
# https://askubuntu.com/questions/154354/how-to-add-vte-terminal-widget-in-gtk3
# sudo apt install python3-yaml gir1.2-vte-2.91

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Vte
from gi.repository import Gio
from gi.repository import GLib
import os
import stat
import yaml
import copy

class MyTerm(Vte.Terminal):
    def __init__(self, *args, **kwds):
        super(MyTerm, self).__init__(*args, **kwds)
        self.spawn_sync(
            Vte.PtyFlags.DEFAULT,
#            os.environ['HOME'] + "/g/tui/zpells/vte",
#            os.getcwd(),
            "/my/tui/igi",
#            ["/bin/bash"],
            ["./main-igi.py"],
            ["IGI=/tmp/igi"],
            GLib.SpawnFlags.DO_NOT_REAP_CHILD,
            None,
            None,
            )

    def do_bell(m):
        m.ui.do_bell()

class TouchTerm:
    def __init__(m):
        try:
            os.mknod("/tmp/igi", stat.S_IFIFO | 0o600)
        except:
            print("comm exists")

        m.term = MyTerm()
        m.term.ui = m
        m.term.set_font_scale(1.2)
        m.mainbox = Gtk.Box() 
        m.mainbox.add(m.term)
        m.rightbox = Gtk.VBox()
        m.mainbox.add(m.rightbox)
        m.bigbox = Gtk.VBox()
        m.bigbox.add(m.mainbox)
        m.bottombox = Gtk.Box()
        m.bigbox.add(m.bottombox)

        m.widgets = []
        
        m.read("/tmp/igi")
        

    def do_bell(m):
        print("bell!")
        return
        #print(m.get_text(None))
        f = open("/tmp/igi")
        l = f.read(100*1024)
        print(l)
        m.yaml_handle(l)

    def test_buttons(m):
        b1 = Gtk.Button(label="Left")
        b2 = Gtk.Button(label="rotate")
        b3 = Gtk.Button(label="Right")
        b1.connect("clicked", lambda _: m.feed(b"j"))
        b2.connect("clicked", lambda _: m.feed(b"k"))
        b3.connect("clicked", lambda _: m.feed(b"l"))
        m.rightbox.add(b3)
        m.rightbox.add(b2)
        m.rightbox.add(b1)
        
    def feed(m, text):
        m.term.feed_child(text)
        m.term.grab_focus()

    def remove_widgets(m):
        box = m.bottombox
        for w in m.widgets:
            box.remove(w)
        box = m.rightbox
        for w in m.widgets:
            box.remove(w)
        m.widgets = []

    def yaml_widgets(m, wl):
        box = m.bottombox
        for w in wl:
            if w['type'] == "label":
                b = Gtk.Label(w['label'])
                m.widgets += [b]
                box.add(b)
            if w['type'] == "button":
                box = m.rightbox
                b = Gtk.Button(label=w['label'])
                a = w['action']
                if a['type'] == "send-key":
                    key = a["key"]
                    cback = lambda _, key=key: m.feed(key)
                    #cback = lambda _, key=key: print(key)
                    print("widget ", w['label'], " key ", key, cback)
                    b.connect("clicked", cback)
                print("widget ", b)
                m.widgets += [b]
                box.add(b)
        box.show_all()

    def yaml_handle(m, s):
        y = yaml.safe_load(s)
        if "mode" in y:
            wl = y["mode"]
            m.remove_widgets()
            if wl == "menu":
                print("Requested menu")
            if wl == "terminal":
                print("Requested terminal")
        if "widgets" in y:
            wl = y["widgets"]
            m.yaml_widgets(wl)
        if "modify" in y:
            zoom = y["modify"]["vte"]["font_scale"]
            m.term.set_font_scale(zoom)

    def test_yaml(m):
        m.yaml_handle("""
modify:
  vte:
    font_scale: 1
""")

    def read(m, uri):
        print("Reading %s" % uri)
        giofile = Gio.File.new_for_path(uri)
        giofile.load_contents_async(None, m._load_contents_cb, None)        

    def _load_contents_cb(m, giofile, result, user_data=None):
        success, contents, etag = giofile.load_partial_contents_finish(result)
        print("Finished reading %d bytes" % len(contents))
        m.yaml_handle(contents)
        m.read("/tmp/igi")

win = Gtk.Window()
win.connect('delete-event', Gtk.main_quit)
win.set_default_size(360, 680)
t = TouchTerm()
t.test_yaml()
win.add(t.bigbox)
win.show_all()

Gtk.main()
