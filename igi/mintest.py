#!/usr/bin/python3
# sudo apt install python3-tk
# pip3 install --break-system-packages mininterface

from dataclasses import dataclass
from mininterface import run
from mininterface.tag import Tag

@dataclass
class Env:
    """ This calculates something. """

    my_flag: bool = False
    """ This switches the functionality """

    my_number: int = 4
    """ This number is very important """

def callback_1():
    """ Dummy function """
    print("Priting text")
    return 1

def callback_2():
    """ Dummy function """
    print("Priting text")
    return 2

def simple():
    with run(Env) as m:
        print(f"Your important number is {m.env.my_number}")
        boolean = m.is_yes("Is that alright?")

        out = m.choice({
            "My choice1": callback_1,
            "My choice2": callback_2,
            # Not supported: "My choice3": Tag(callback_tag, annotation=CallbackTag),
        })
        print(out)
        
def form():
    m = run()
    r = m.is_yes("hello?")
    print(r)
    out = m.choice({
        "My choice1": callback_1,
        "My choice2": callback_2,
        # Not supported: "My choice3": Tag(callback_tag, annotation=CallbackTag),
    })
    print(out)

if __name__ == "__main__":
    form()
