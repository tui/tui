#!/usr/bin/python3
import osmium
import sys
sys.path += [ "../lib" ]
import nearest
import json
import towns

# This needs aptitude install python3-pyosmium

class CityPrinterHandler(osmium.SimpleHandler):
    def node(m, node):
        tags = node.tags
        if not 'place' in tags:
            return
        if not 'name' in tags:
            print("place=%s with no name?" % tags['place'])
            return
        if tags['place'] in ( 'city', 'town', 'suburb', 'village' ):
            pl = nearest.Place(node.location.lat, node.location.lon, tags['name'])
            print(pl.fmt())
            m.places += [ ( pl.lat, pl.lon, pl.name ) ]

class TownData(towns.Data):
    def __init__(m, osm_name = "/data/gis/osm/dumps/czech_republic-2010-10-20.osm.bz2"):
        #m.load_osm_towns(osm_name)
        m.load_json_towns()
        
    def load_osm_towns(m, fname):
        h = CityPrinterHandler()
        h.places = []
        h.apply_file(fname)
        open("towns.json", "w").write(json.dumps(h.places))
        m.decode_towns(h.places)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        d = TownData(sys.argv[1])
    else:
        d = TownData()
    print("Computing nearest...")
    point = nearest.Place(50.07411, 14.421315, "")
    (place, distance) = d.get_nearest(point, d.towns)
    print(distance, place.fmt())
