#!/usr/bin/python3
import sys
sys.path += [ "../lib" ]
import nearest
import json

class TownData(nearest.Data):
    def __init__(m, osm_name = "/data/gis/osm/dumps/czech_republic-2010-10-20.osm.bz2"):
        #m.load_osm_towns(osm_name)
        m.load_json_towns()
        
    def load_json_towns(m):
        places = json.load(open("../osm/towns.json"))
        m.decode_towns(places)
        
    def decode_towns(m, places):
        m.towns = []
        for lln in places:
            lat, lon, name = lln
            m.towns += [ nearest.Place(lat, lon, name) ]
        m.places = [ ( "town",  m.towns ) ]

