#include "helloworld.h"
#include <gtkmm/application.h>

int main (int argc, char *argv[])
{
#if 0
  Glib::RefPtr<Gtk::Application> 
#else
  auto
#endif
       app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  HelloWorld helloworld;

  //Shows the window and returns when it is closed.
  return app->run(helloworld);
}
