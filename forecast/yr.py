#!/usr/bin/python

import requests
import threading
import xml.etree.ElementTree as ET
import base

class Point:
    def __init__(m):
        m.time = None
        m.summary = None
        m.icon = None
        m.dewPoint = None
        m.windspeed = None
        m.windbearing = None
        m.cloudcover = None
        m.humidity = None
        m.pressure = None
        m.visibility = None
        m.ozone = None
        m.sunriseTime = None
        m.precipIntensity = None
        m.precipIntensityMax = None
        m.precipProbability = None
        m.precipType = None
        m.precipAccumulation = None
        m.temperatureMin = None
        m.temperature = None

    def assert_unit(m, data, val):
        if data.attrib["unit"] != val:
            print "Bad units: expect", val, "got", data.attrib["unit"]
            raise "unexpected units"

    def parse_location(m, location):
        for data in location:
            print " --- ", data.tag, data.attrib
            if data.tag == "temperature":
                m.assert_unit(data, "celsius")
                m.temperature = float(data.attrib["value"])
            if data.tag == "dewPointTemperature":
                m.assert_unit(data, "celsius")
                m.dewPoint = float(data.attrib["value"])
            if data.tag == "pressure":
                m.assert_unit(data, "hPa")
                m.pressure = float(data.attrib["value"])
            if data.tag == "windSpeed":
                # FIXME: what does it expect
                m.windspeed = float(data.attrib["mps"])
            if data.tag == "windDirection":
                # FIXME: what does it expect
                m.windbearing = float(data.attrib["deg"])
            if data.tag == "humidity":
                m.assert_unit(data, "percent")
                m.humidity = float(data.attrib["value"])

    def parse_time(m, p):
        if p.tag != "time":
            print "Something is wrong: wrong tag"
            return
        if p.attrib["datatype"] != "forecast":
            print "Something is wrong: not a forecast"
            return
        m.time = p.attrib['from']
        m.time_to = p.attrib['to']
        for location in p:
            m.parse_location(location)

class YRForecast:
    def load(m, lat, lon):
        url = "http://api.yr.no/weatherapi/locationforecast/1.9/?lat=%f;lon=%f" % (lat, lon)
        res = requests.get(url)
        print res
        print res.headers
        print res.text
        return res


yr = YRForecast()
res = yr.load(50.14, 14.50)

root = ET.fromstring(res.text)
print root

for c in root:
    # product
    for p in c:
        print p.tag, p.attrib
        point = Point()
        point.parse_time(p)
        point.details()
