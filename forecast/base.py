class BasePoint:
    base_vars = [ "time", "summary", "icon", "sunriseTime", "sunsetTime",
                  "precipIntensity", "precipIntensityMax", "precipIntensityMaxTime",
                  "precipProbability", "precipType", "precipAccumulation",
                  "temperature", "temperatureMin", "temperatureMinTime",
                  "temperatureMax", "temperatureMaxTime", "dewPoint",
                  "windspeed", "windbearing", "cloudcover", "humidity",
                  "pressure", "visibility", "ozone" ]

    def clear_base_vars(m):
        for v in m.base_vars:
            m.__dict__[v] = None

    def details(m):
        print "----------- Data for ", m.time
        print m.summary
        if m.icon:
            print m.icon
        if m.sunriseTime:
            print "Sun rise", m.sunriseTime
            print "    set ", m.sunsetTime

        print "Precipation intensity ", m.precipIntensity
        if m.precipIntensityMax:
            print "                      max", m.precipIntensityMax
            print "                      max time", m.precipIntensityMaxTime
        if m.precipProbability:
            print "            probability", m.precipProbability * 100, "%"
        if m.precipType:
            print "            type", m.precipType
        if m.precipAccumulation:
            print "            accumulation", m.precipAccumulation
        print "Temperature", m.temperature, "C"
        if m.temperatureMin:
            print "            min", m.temperatureMin, "C"
            print "            min time", m.temperatureMinTime
            print "            max", m.temperatureMax, "C"
            print "            max time", m.temperatureMaxTime
        print "Dew point", m.dewPoint, "C"
        print "Wind speed", m.windspeed, "m/s"
        print "     bearing", m.windbearing
        if m.cloudcover:
            print "Cloud", m.cloudcover*100. , "%"
        if m.humidity:
            print "Humidity", m.humidity*100. , "%"
        print "Pressure", m.pressure, "hPa"
        print "Visibility", m.visibility, "km"
        print "Ozone", m.ozone

    def one_line(m):
	print m.time, 
        if m.precipProbability:
            print m.precipProbability*100, "%", m.precipType, 
	if m.temperature:
            print m.temperature, "C", 
	if m.temperatureMin:
            print m.temperatureMin, "C ..", m.temperatureMax, "C",
	print m.summary
