#!/usr/bin/python

import fio
import pylab
import re
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime

m = fio.CachedForecast()

m.load_now()

#t = datetime.datetime(2014, 7, 27, 0, 0)
#m.load_history(t)

#m.load("data_50.000000_14.750000/log_1406883043.fcast")
# This corresponds to 2014 08 01 1 04
# Lets see if they lie...
#t = datetime.datetime(2014, 8, 1, 23, 4)
#m.load_history(t)

by_hour = m.fc.hourly()
#print "Hourly Summary: %s" % (by_hour.summary)

fig = pylab.figure()
#fig.title("Near term weather")
ax1 = fig.add_subplot(211)

x = []
temp_y = []
precip_y = []
cloud_y = []
wind_y = []
pres_y = []
humid_y = []
for point in by_hour.data:
    hour = point.time.hour
    if hour == 0:
	ax1.axvline(point.time, -10, 30)

    x += [ point.time ]
    temp_y += [ point.temperature ]
    precip_y += [ point.precipProbability ]
    cloud_y += [ point.cloudcover ]
    wind_y += [ point.windspeed ]
    pres_y += [ point.pressure ]
    humid_y += [ point.humidity ]

ax1.plot_date(x, temp_y, '-r', label = "temperature")
ax1.xaxis.set_major_formatter(mdates.DateFormatter('%d %H:%M'))
ax1.xaxis.set_major_locator(mdates.DayLocator(interval=1))

ax2 = ax1.twinx()
ax2.plot_date(x, precip_y, '-b', label = 'precipation')
ax2.set_ylim(0, 1)
#for tl in ax2.get_yticklabels():
#    tl.set_color('b')

ax3 = ax1.twinx()
ax3.plot_date(x, cloud_y, '-k', label = 'cloud')
ax3.set_ylim(0, 1)

ax4 = ax1.twinx()
ax4.plot_date(x, humid_y, '--k', label = 'humidity')
ax4.set_ylim(0, 1)

ax10 = fig.add_subplot(212)
ax10.plot_date(x, wind_y, '-k', label = 'wind')

ax3.xaxis.set_major_formatter(mdates.DateFormatter('%d %H:%M'))
ax3.xaxis.set_major_locator(mdates.DayLocator(interval=1))


ax11 = ax10.twinx()
ax11.plot_date(x, pres_y, '-b', label = 'pressure')

ax11.xaxis.set_major_formatter(mdates.DateFormatter('%d %H:%M'))
ax11.xaxis.set_major_locator(mdates.DayLocator(interval=1))

pylab.legend()
pylab.show()

