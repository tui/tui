#!/usr/bin/python3

# https://wiki.gnome.org/WeatherApplet
# https://lazka.github.io/pgi-docs/

# sudo apt install libgweather-4-dev

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('GWeather', '4.0')
from gi.repository import GWeather
from gi.repository import GLib
from gi.repository import Gtk

# 
lo = GWeather.Location.new_detached("", None, 50, 14)
#lo = GWeather.Location.new_detached("", None, 40, -73)
print(lo)
print("Country:" , lo.get_country_name())
print("City:" , lo.get_city_name())
print("Code:" , lo.get_code())

#print("-----------------")
i = GWeather.Info.new(lo)
i.set_enabled_providers( GWeather.Provider.ALL )
i.connect("updated", lambda x: signal_updated())
#i.connect("updated", lambda x: print("got signal"))
#i.update()

def print_weather_info(i):
    print("Weather information")
    if not i.is_valid():
        print("... invalid")
        return
    print("  for location:", i.get_location_name())
    print("  provided by:", i.get_enabled_providers())
    print("  provided by:", i.get_attribution())
    print()
    print("  conditions:", i.get_conditions())
    print("  summary", i.get_weather_summary())

    print("apparent", i.get_apparent())
    print("dew", i.get_dew())
    print("humidity", i.get_humidity())
    print("icon", i.get_icon_name())
    print("symbolic icon", i.get_symbolic_icon_name())
    # get_forecast_list()

    print("pressure", i.get_pressure())
    print("radar", i.get_radar())
    print("sky", i.get_sky())

    print("temp summary", i.get_temp_summary())    
    print("update", i.get_update())
    print("moon phase", i.get_value_moonphase())
    print("visibility", i.get_visibility())
    print("wind", i.get_wind())
    print("is day", i.is_daytime())
    #print("moonphases", i.get_upcoming_moonphases())

    print("  temperature:", i.get_value_temp(GWeather.TemperatureUnit.CENTIGRADE)[1], "C")
    print("  temperature:", i.get_temp())
    print("  temperature min/max:", i.get_temp_min(), i.get_temp_max())

    print("  sunrise:", i.get_value_sunrise()[1])
    print("  sunset:", i.get_value_sunset()[1])

def print_weather_info_short(i):
    if not i.is_valid():
        return
    print(i.get_update(), end=" ")
    #print(i.get_weather_summary(), end="")

    print("", i.get_temp_summary(), end="")
    print("   ", i.get_wind(), end="")
    print(" ", i.get_conditions(), i.get_sky())


def signal_updated():
    print("Got signal")
    print_weather_info_short(i)

    print()
    print("Forecasts: ", len(i.get_forecast_list()))
    for l in i.get_forecast_list():
        if not l.is_daytime():
            continue
        print_weather_info_short(l)
    
    Gtk.main_quit()

# Need to run main loop here?
# ...and wait for ::updated signal

#mainloop = GLib.MainLoop()
#mainloop.run()
Gtk.main()
#for i in range(1000):
#    Gtk.main_iteration()
