#!/usr/bin/python

# apt-get install python-pymetar
import pymetar
import base

class MetarPoint(base.BasePoint):
    def query_now(m, code):
        f = pymetar.ReportFetcher()
        r = f.FetchReport(code)
        print r.getFullReport()
        r = pymetar.ReportParser(r)
        r = r.ParseReport()
        m.clear_base_vars()
        m.time = r.getISOTime()
        m.dewPoint = r.getDewPointCelsius()
        hum = r.getHumidity()
        if hum:
            m.humidity = hum / 100.
        m.summary = r.getWeather()
        m.pressure = r.getPressure()
        m.temperature = r.getTemperatureCelsius()
        m.visibility = r.getVisibilityKilometers()
        m.windbearing  = r.getWindDirection()
        m.windspeed = r.getWindSpeed()

if __name__ == "__main__":
    pt = MetarPoint()
    pt.query_now("LKPR")
    pt.details()
