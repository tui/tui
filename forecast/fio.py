#!/usr/bin/python
# https://github.com/ZeevG/python-forcast.io
# use
# sudo pip install python-forecastio
#
# Error of 5C is very significant.
#
# It will happily return "historical" data from future.
# It contains historical data from 1952?
# Data from years around 1926 are all the same?!
#
# Some information about forecasts:
# http://www.nssl.noaa.gov/users/brooks/public_html/feda/note/topic4.html

# Fixme in upstream: windbaring, visbility

# 14.4.2014 16:20, Zernovka -- zacinaji padat kroupy

import sys
sys.path.insert(0, "/home/pavel/g/python-forcast.io")
import api_key
import forecastio
import datetime
import time
import json
import os
from base import *

class FioPoint(BasePoint):
    def __init__(m, point):
        for v in m.base_vars:
            if v in point.__dict__:
                m.__dict__[v] = point.__dict__[v]
            else:
                m.__dict__[v] = None

class Forecast:
    def print_hourly(m):
	by_hour = m.fc.minutely()

	for hourly_data_point in by_hour.data:
            print "### Minutely data available -- write code"

	by_hour = m.fc.hourly()
	#print "Hourly Summary: %s" % (by_hour.summary)

	for hourly_data_point in by_hour.data:
            p = FioPoint(hourly_data_point)
            p.one_line()

    def print_daily(m):
	by_day = m.fc.daily()
	#print "Daily Summary: %s" % (by_day.summary)

	for daily_data_point in by_day.data:
            p = FioPoint(daily_data_point)
            p.one_line()

    def pprint(m):
	now = FioPoint(m.fc.currently())
	now.details()

	print "===========Hourly Data========="
	m.print_hourly()
	print "===========Daily Data========="
        m.print_daily()

    def save(m, name):
        # 
	with open(name, 'w') as outfile:
            json.dump(m.fc.json, outfile)

    def load(m, name):
	json_data=open(name)
	data = json.load(json_data)
	m.fc = forecastio.models.Forecast(data, None, None)

    def query_now(m):
	m.fc = forecastio.load_forecast(api_key.api_key, api_key.lat, api_key.lon, units="si")

    def query_history(m, dt):
        m.fc = forecastio.load_forecast(api_key.api_key, api_key.lat, api_key.lon, units="si", time=dt)

class CachedForecast(Forecast):
    def __init__(m):
        m.datadir = "data_%f_%f/" % (api_key.lat, api_key.lon)

    def load_now(m):
        m.query_now()
        dt = datetime.datetime.now()
        m.save(m.datadir + 'log_%s.fcast' % dt.isoformat('_'))

    def history_name(m, dt):
        return m.datadir + 'hist_%s.fcast' % dt.isoformat('_')

    # Strange.
    # History always query presents "hourly data" for that day.
    # For hour h, "history data" seems to match reality for hours <= h
    # and are very close for > h.
    def load_history(m, dt):
        name = m.history_name(dt)
        if os.path.exists(name):
            m.load(name)
            return
        m.query_history(dt)
        m.save(name)

class Comparator:
    def load(m, f1, f2):
        m.f1 = f1
        m.f2 = f2

    def sqr(m, v1, v2, scale):
        v = ((v1-v2)/scale)
        return v*v

    def compare(m):
        score = 0.0
        p1 = m.f1.currently()
        p2 = m.f2.currently()
        score += m.sqr(p1.temperature, p2.temperature, 4)
        score += m.sqr(p1.precipProbability, p2.precipProbability, 1.0)
        score += m.sqr(p1.windspeed, p2.windspeed, 3.0)
#        score += m.sqr(p1.cloudcover, p2.cloudcover, 3.0)

        print "Score: ", score
        if score > 1:
            print "Temperatures ", p1.temperature, p2.temperature
            print "Precipation probability ", p1.precipProbability, p2.precipProbability
            print "Wind speed ", p1.windspeed, p2.windspeed
            print "Cloud cover ", p1.cloudcover, p2.cloudcover

        return score


def print_now():
    m = CachedForecast()
    m.load_now()
    m.pprint()

def load_month(year, month):
    m = CachedForecast()
    for day in range(1, 30):
        t = datetime.datetime(year, month, day, 0, 0)
        m.load_history(t)
        m.print_daily()

def load_month_part(year, month):
    m = CachedForecast()
    for day in range(1, 9):
        for hour in range(24):
            t = datetime.datetime(year, month, day, hour, 0)
            m.load_history(t)
            m.print_daily()

def load_hist():
    m = CachedForecast()
    for year in range(1989, 2014):
        for month in range(1, 13):
            for day in range(1, 32):
                # 0, 6, 18
                for hour in 12,:
                    try:
                        t = datetime.datetime(year, month, day, hour, 0)
                    except:
                        continue
                    m.load_history(t)
                    m.print_daily()

def process_april(year, month):
    m1 = CachedForecast()
    m2 = CachedForecast()
    score = 0.0
    num = 0
    for day in range(2, 9):
        for hour in range(1, 24):
            t = datetime.datetime(year, month, day, hour, 0)
            m1.load_history(t)
            t = datetime.datetime(year-1, month, day, hour, 0)
            m2.load_history(t)
            c = Comparator()
            c.load(m1.fc, m2.fc)
            score += c.compare()
            num += 1
    print "Average score: ", score/num

def print_daily_history():
    now = datetime.datetime.now()
    m = CachedForecast()
    for year in range(1989, now.year):
        t = datetime.datetime(year, now.month, now.day, 12, 0)
        m.load_history(t)
        m.print_daily()

def test():
    m = CachedForecast()
    t = datetime.datetime(2014, 8, 15, 13, 0)
    m.load_history(t)
    m.pprint()
        
if __name__ == "__main__":
#    test()
    print_now()
#    print_daily_history()

# Soubor je z budoucna... schvalne jak se bude lisit?
# data_50.100000_14.250000/hist_2014-08-05_13\:00\:00.fcast
# data_50.100000_14.250000/hist_2014-08-15_13\:00\:00.fcast
