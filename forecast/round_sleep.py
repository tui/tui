#!/usr/bin/python
# Sleep until "round" time.
# http://stackoverflow.com/questions/645992/bash-sleep-until-a-specific-time-date

import time
import sys

def round_sleep(mod):
    now = time.time()
    until = now - now % mod + mod
    print "sleeping until", until

    while True:
        delta = until - time.time()
        if delta <= 0:
            print "done sleeping ", time.time()
            break
        time.sleep(delta / 2)

if __name__ == "__main__":
    round_sleep(float(sys.argv[1]))

