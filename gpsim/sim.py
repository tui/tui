#!/usr/bin/python3

# Writes output to /tmp/gpsfake_pipe
# -> mknod /tmp/gpsfake_pipe p
# -> ../loc/gpsfake

# sudo apt install python3-geopy python3-pint
# 

import sys
sys.path += [ "../maemo", "../lib", "../osm", "../loc" ]
import util
import time
import os
import geopy
from geopy.distance import great_circle
import math
import select
import re
from pint import UnitRegistry

ur = UnitRegistry()

class Vehicle(geopy.Point):
    def __init__(m):
        super().__init__()
        m.mps = ur.meters / ur.second
        
    def update_trivial(m, step = 1):
        m.latitude += 0.001
        m.longitude += 0.001

    def update_target(m):
        m.target_meters = geopy.distance.distance(m, m.destination).meters * ur.meters
        p1 = util.FakePos()
        p2 = util.FakePos()        
        p1.lat, p1.lon = m.latitude, m.longitude
        p2.lat, p2.lon = m.destination.latitude, m.destination.longitude
        m.target_bearing = util.bearing(p1, p2)

    def update(m, delta = 1):
        m.delta = delta * ur.second
        m.update_target()

        m.orig_heading = m.heading
        m.orig_speed = m.speed

        m.navigation()
        if m.speed > m.target_speed: m.speed -= m.acceleration * m.delta
        if m.speed < m.target_speed: m.speed += m.acceleration * m.delta
        
        # This does not really work near poles.
        orig_xv = math.sin(math.radians(m.orig_heading))*m.orig_speed
        orig_yv = math.cos(math.radians(m.orig_heading))*m.orig_speed
        xv = math.sin(math.radians(m.heading))*m.speed
        yv = math.cos(math.radians(m.heading))*m.speed
        f = ((orig_xv-xv)**2 + (orig_yv-yv)**2).magnitude
        delta_v = math.sqrt(f) * m.mps
        m.cur_acceleration = delta_v /delta 
        print("To target %.0f m, %.0f deg; speed %.0f m/sec, %.0f deg, acc = %.2f m/s^2" % (m.target_meters.magnitude, m.target_bearing, m.speed.to(m.mps).magnitude, m.heading, m.cur_acceleration.magnitude))

        step = great_circle(meters=(m.speed*delta).magnitude).destination(m, m.heading)
        m.latitude = step.latitude
        m.longitude = step.longitude

class Crazy_UFO(Vehicle):
    def __init__(m):
        super().__init__()
        m.latitude = 0
        m.longitude = 0
        m.destination = geopy.Point(latitude = 50, longitude = 15)
        m.heading = 180
        m.speed = 10000 * ur.meters / ur.second
        m.acceleration = 5 * ur.meters / (ur.second * ur.second)

    def speed_limit(m):
        if m.target_meters > 10: m.target_speed = 0 * m.mps
        if m.target_meters > 100: m.target_speed = 20 * m.mps
        if m.target_meters > 1000: m.target_speed = 70 * m.mps
        if m.target_meters > 10000: m.target_speed = 150 * m.mps
        if m.target_meters > 100000: m.target_speed = 1000 * m.mps
        if m.target_meters > 1000000: m.target_speed = 2000 * m.mps
        if m.target_meters > 10000000: m.target_speed = 10000 * m.mps

    def navigation(m):
        print("Speed ", m.speed, m.mps)
        if m.speed > 1*m.mps:
            max_turn = m.acceleration*100/m.speed * m.delta
        else:
            max_turn = 5
        if max_turn > 5:
            max_turn = 5
        if m.heading - 2 < m.target_bearing < m.heading + 2: pass
        elif m.heading < m.target_bearing + 180 < m.heading + 180: m.heading -= max_turn
        elif m.target_bearing >  m.heading + 180: m.heading -= max_turn
        else: m.heading += max_turn
        if m.heading < 0: m.heading += 360
        if m.heading > 360: m.heading -= 360
        m.speed_limit()

class Reasonable_UFO(Crazy_UFO):
    def __init__(m):
        super().__init__()
        m.latitude = 50.15
        m.longitude = 14.50
        m.destination = geopy.Point(latitude = 50, longitude = 15)
        m.heading = 180
        m.speed = 220 * m.mps
        m.acceleration = 20 * ur.meters / (ur.second * ur.second)

    def speed_limit(m):
        f = (2 * m.target_meters * m.acceleration).magnitude
        m.target_speed = 0.95 * math.sqrt(f) * m.mps
    
    def navigation(m):
        super().navigation()
        if abs(m.target_bearing - m.heading) > 30: m.target_speed = 0 * m.mps

class GPS(geopy.Point):
    def __init__(m):
        m.sats = []

    def update_sats(m):
        m.sats = []
        m.sats += [ (m.dt.tm_sec, 15, m.dt.tm_sec * (360/60), 10) ]
        m.sats += [ (m.dt.tm_min, 30, m.dt.tm_min * (360/60), 30) ]
        m.sats += [ (m.dt.tm_hour, 40, m.dt.tm_hour * (360/12), 60) ]
        m.sats += [ (5, 75 - m.veh.cur_acceleration.magnitude, m.veh.target_bearing, 10) ]
        m.sats += [ (6, 85 - m.veh.speed.magnitude/100, m.veh.heading, 60) ]

        m.sats += [ (7, 60, m.veh.speed.magnitude, 30) ]
        m.sats += [ (8, 70, m.veh.speed.magnitude/10, 30) ]
        m.sats += [ (9, 80, m.veh.speed.magnitude/50, 30) ]
        
        return
        m.sats += [ (m.dt.tm_sec, 10, (10 - m.dt.tm_sec) * (360/60), 5) ]
        m.sats += [ (m.dt.tm_sec, 10, (30 - m.dt.tm_sec) * (360/60), 5) ]
        m.sats += [ (m.dt.tm_sec, 10, (50 - m.dt.tm_sec) * (360/60), 5) ]
        
    def update(m):
        m.dt = time.localtime()
        m.latitude = m.veh.latitude
        m.longitude = m.veh.longitude
        m.update_sats()
        
class NmeaOut:
    def send(m, s):
        os.system("echo -n '"+s+"' > /tmp/gpsfake_pipe")

    def send_position(m):
        gps = m.gps
        latD = abs(int(gps.latitude))
        latM = (abs(gps.latitude) - latD) * 60
        latL = 'N'
        if gps.latitude < 0: latL = 'S'
        lonD = abs(int(gps.longitude))
        lonM = (abs(gps.longitude) - lonD) * 60
        lonL = 'E'
        if gps.longitude < 0: lonL = 'W'
        hsec = int((gps.dt.tm_sec - int(gps.dt.tm_sec)) * 100)

        s_time = "%02d%02d%02d" % (gps.dt.tm_hour, gps.dt.tm_min, int(gps.dt.tm_sec))
        s_pos = "%d%09.6f,%c,%d%09.6f,%c" % (latD, latM, latL, lonD, lonM, lonL)
        s = "$GPGLL,%s,%s.%02d,A" % \
               (s_pos, s_time, hsec)
        print(s)
        m.send('n'+s)

        gps.altitude = gps.veh.speed.magnitude / 10.
        gps.geoid_above_elipsoid = 34.5
        gps.hdop = 5.0
        s = 'n$GPGGA,%s,%s,1,%02d,%.1f,%3.1f,M,%3.1f,M,,' % ( s_time, s_pos, len(gps.sats), gps.hdop, gps.altitude, gps.geoid_above_elipsoid )
        m.send(s)
        speed_knots = gps.veh.speed.to(ur.knots).magnitude
        s = 'n$GPRMC,%s,A,%s,%3.1f,%3.1f,%s,0,E,A' % ( s_time, s_pos, speed_knots, gps.veh.heading, '120216' ) # date
        m.send(s)

        # http://aprs.gids.nl/nmea/#rma
        # GPRMB: allows us to also tell our destination, bearing to destination

        s = 'n$GPGSA,A,3,7,8,9,18,27,22,31,39,,,,,5.0,5.0,5.0'
        m.send(s)
        # $GPGSA
        # GPS DOP and active satellites

    def send_sats_clever(m, sats):
        # lines total/current, sats total
        i = 0
        for sat in sats:
            if not i % 3:
                if i:
                    m.send('n'+s)                    
                s = "$GPGSV,%d,%d,%d" % ((len(sats) + 2) / 3, (i + 3) / 3, len(sats))
            # id, ele, azimut, snr
            s += ",%02d,%02d,%03d,%02d" % sat
            i += 1
        m.send('n'+s)

    def send_sats(m, sats):
        # lines total/current, sats total
        i = 0
        for sat in sats:
            s = "$GPGSV,%d,%d,%d" % (len(sats), i+1, len(sats))
            # id, ele, azimut, snr
            s += ",%02d,%02d,%03d,%02d" % sat
            i += 1
            print(s)
            m.send('n'+s)

    def handle(m, line):
        if re.match("^target", line):
            s = line.split(" ")
            m.gps.veh.destination.latitude = float(s[1])
            m.gps.veh.destination.longitude = float(s[2])
            return
        if re.match("^teleport", line):
            s = line.split(" ")
            m.gps.veh.latitude = float(s[1])
            m.gps.veh.longitude = float(s[2])
            return
        print("Unknown command ", line)

    def run(m):
        while True:
            while sys.stdin in select.select([sys.stdin], [], [], 1)[0]:
                line = sys.stdin.readline()
                if line:
                    m.handle(line)
                else: # an empty line means stdin has been closed
                    exit(0)
            else:
                m.gps.veh.update()
                m.gps.update()
                m.send_position()
                #m.send_sats(m.gps.sats)

out = NmeaOut()
out.gps = GPS()
out.gps.veh = Reasonable_UFO()
out.run()

