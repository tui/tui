#!/usr/bin/python

import gps
import math
import time
import sys
sys.path += [ "../ofone", "../lib", "/usr/share/unicsy/lib" ]
import client
import watchdog
import util

# aptitude install python-gps, no python3 version afaict
# http://catb.org/gpsd/libgps.html

# Unfortunately, "eph" from gps3 does not seem to be passed here.

class GpsRecorder(client.GpsQuality):
    def __init__(m, name):
        if name == "":
            m.fd = open("/data/tmp/tracklog-%d.egt" % time.time(), "w")
        else:
            m.fd = open(name, "w")
        client.GpsQuality.__init__(m)

    def global_status(m, status):
        pass

    def print_report(m, report, t):
        if not ('mode' in report):
            print("# No mode?")
            return
        m.mode = report['mode']
        if m.mode > 1:
            if not ('lat' in report):
                print("# No latitude?")
                return
            if not ('lon' in report):
                print("# No longitude?")
                return
            s = ''
            s += '%f %f' % (report['lat'], report['lon'])
            def add_report(in_report, in_output):
                if in_report in report:
                    return in_output % report[in_report]
                return ""
                
            s += add_report('alt', ' ele=%.2f')
            s += add_report('time', ' time=%s')
            s += add_report('track', ' track=%.1f')
            s += add_report('speed', ' speed=%.2f')
            s += add_report('climb', ' climb=%.2f')
            m.global_status('recording')
            s += " # quality %f" % m.quality
            #print(s)
            m.fd.write(s + "\n")
            if m.mode > 2:
                m.fix_3d(report)
            m.fd.flush()
            return
        m.global_status('no gps')
        print("# No signal %s" %  t)
        
    def fix_3d(m, report):
        pass

class GpsRecorderWd(GpsRecorder):
    def __init__(m, name):
        GpsRecorder.__init__(m, name)
        m.wd = watchdog.Watchdog("track")
        m.pos = None
        m.distance = 0.	# km
        m.ascent = 0.
        m.descent = 0.
    
    def global_status(m, s):
        m.wd.write(s, "Distance km: %f\nAscent m: %f\nDescent m: %f\n" % 
                   (m.distance, m.ascent, m.descent))

    def fix_3d(m, report):
        if not 'alt' in report:
            return

        pos = util.FixPos(report['lat'], report['lon'])
        pos.alt = report['alt']

        if m.pos:
            m.distance += util.distance(m.pos, pos)
            m.handle_vertical(pos.alt, m.pos.alt)

        m.pos = pos

    def handle_vertical(m, now, prev):
        if now > prev:
            m.ascent += now - prev
        if now < prev:
            m.descent -= now - prev

#q = GpsRecorderWd("")
q = GpsRecorder("/dev/stdout")
q.run()

        
