function bench() {
for (q=0; q<10; q++) {
  s=0
  for (n=0; n<100; n++) {
    a = n+1;
    for (i=0; i<10; i++)
        a=Math.sqrt(a);
    for (i=0; i<10; i++)
        a=a*a
      s = s+a
  }
}
}

function run() {
t1 = getTime();
bench();
print(1010-s/5);
print("Oldbench: ", getTime() - t1);
}

function f() {
  "compiled";
  var Xr = 0;
  var Xi = 0;
  var i = 0;
  var Cr=(4*x/64)-2;
  var Ci=(4*y/64)-2;
  while ((i<32) & ((Xr*Xr+Xi*Xi)<4)) {
    var t=Xr*Xr - Xi*Xi + Cr;
    Xi=2*Xr*Xi+Ci;
    Xr=t;
    i=i+1;
  }
  return i;
}

var x,y;
for (y=0;y<64;y++) {
 line="";
 for (x=0;x<64;x++) line += " *"[f()&1];
 print(line);
}

print("Running benchmark");
setTimeout(run, 100);
