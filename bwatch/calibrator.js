//var Layout = require("Layout");

const W = g.getWidth();
const H = g.getHeight();

var cx = 100; cy = 105; sc = 70;
var buzz = "", msg = "";
temp = 0; alt = 0; bpm = 0;

function touchHandler(d) {
  let x = Math.floor(d.x);
  let y = Math.floor(d.y);
  
  print("drag:", d);
 
  g.setColor(0.25, 0, 0);
  g.fillCircle(W-x, W-y, 5);
}

function add0(i) {
  if (i > 9) {
    return ""+i;
  } else {
    return "0"+i;
  }
}

function draw() {
  g.setColor(0, 0, 0);
  g.fillRect(0, 0, W, H);
  g.setFont('Vector', 36);

  g.setColor(1, 1, 1);
  g.setFontAlign(-1, 1);
  let now = new Date();
  g.drawString(now.getHours() + ":" + add0(now.getMinutes()) + ":" + add0(now.getSeconds()), 10, 40);
  

  step = Bangle.getStepCount();
  bat = E.getBattery();
  //print(acc.x, acc.y, acc.z);
  
  g.setColor(0, 1, 0);
  g.drawCircle(cx, cy, sc);
  
  if (0) {
    g.setColor(0, 0.25, 0);
    g.fillCircle(cx + sc * acc.x, cy + sc * acc.y, 5);
    g.setColor(0, 0, 0.25);
    g.fillCircle(cx + sc * acc.x, cy + sc * acc.z, 5);
  }
  
  //g.setColor(diff, diff, diff);
  g.setColor(1, 1, 1);
  
  g.setFont('Vector', 22);
  g.drawString(now.getDate()+"."+(now.getMonth()+1)+" "+now.getDay(), 3, 60);
  g.drawString(msg, 3, 80);
  g.drawString("S" + step + " B" + Math.round(bat/10) + (Bangle.isCharging()?"c":""), 3, 100);
  g.drawString("I" + getTime(), 3, 120);
     
  queueDraw();
}

var drawTimeout;

function queueDraw() {
  if (drawTimeout) clearTimeout(drawTimeout);
  next = 50;
  drawTimeout = setTimeout(function() {
    drawTimeout = undefined;
    draw();
  }, next - (Date.now() % next));

}

  function onGPS(fix) {
    if (fix.fix && fix.time) {
      let gt = fix.time.getTime()/1000;
  
      g.setColor(1, 1, 1);
  
      g.setFont('Vector', 22);
      g.drawString("G" + gt, 3, 140);
      g.drawString("D" + gt - getTime(), 3, 160);
    }
  }


function start() {
  Bangle.on("drag", touchHandler);
  //Bangle.on("accel", accelHandler);
  Bangle.setGPSPower(1, "calibrator");
  Bangle.on('GPS', onGPS);
  
  draw();
}


g.reset();
Bangle.setUI();
start();