#!espruino

eval(require("fs").readFile("sdl.js"));
sx = 360
sy = 648
initWindow(sx, sy);

function touchHandler(d) {
  let x = Math.floor(d.x);
  let y = Math.floor(d.y);

  if (1) { /* Just a debugging feature */
    g.setColor(0.25, 0, 0);
    g.fillCircle(x, y, 5);
  }
}

function touchTest() {
  g.reset().setColor(1,1,0).fillRect(0,0, sx,sy);
  g.setColor(1,1,1).fillRect(5,5, sx-5,sy-5);
  g.setColor(0,0,0).setFont("Vector",25);
  g.setFontAlign(0,0);
  g.drawString("SDL test", 85,35);
  g.setColor(0,0,1).setFont("Vector",18);
  g.drawString("input", 85,55);

  Bangle.on("drag", touchHandler);
}

touchTest();
