#!espruino

eval(require("fs").readFile("sdl.js"));
sx = 360
sy = 648
initWindow(sx, sy);

function parseOrgFile(data) {
  let events = [];
  let lines = data.split("\n");

  let taskTitle = "";
  for (let line of lines) {
    let match;

    print("Line: ", line);
    // Match headers
    if (line.startsWith("* ")) {
      taskTitle = line.substring(2).trim();
      print("Title: ", taskTitle);
    }

    // Match SCHEDULED or DEADLINE
    //if ((match = line.match(/(SCHEDULED|DEADLINE):\s*<(\d{4}-\d{2}-\d{2})/))) {
//      events.push({ title: taskTitle, date: match[2], type: match[1] });
  //  }

    // Match standalone timestamps
    if ((match = /<(\d\d\d\d-\d\d-\d\d)/.exec(line))) {
      print("Match: ", match);
      events.push({ title: taskTitle, date: match[1], type: "EVENT" });
    }
  }

  print("Unsorted:", events);

  // Sort by date
  //events.sort((a, b) => a.date.localeCompare(b.date));

  return events;
}

function displayEvents(events) {
  g.clear();
  g.setFont("6x8", 2);
  g.drawString("Upcoming Events:", 10, 10);

  let y = 30;
  for (let i = 0; i < Math.min(events.length, 35); i++) {
    let e = events[i];
    g.drawString(`${e.date}: ${e.title}`, 10, y);
    print(e);
    y += 20;
  }

  g.flip();
}

print("Reading events");
// Example: Load from Storage
g.setColor(0);
data = require("fs").readFile("events.org")
if (data) {
    let events = parseOrgFile(data);
    print("Have events", events);
    displayEvents(events);
} else {
    print("No events");
    g.clear();
    g.drawString("No events found", 10, 10);
}

