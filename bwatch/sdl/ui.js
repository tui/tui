#!espruino

eval(require("fs").readFile("sdl.js"));
sx = 360
sy = 648
initWindow(sx, sy);

function promises() {
E.showAlert("UI tests").then(function() {
  E.showPrompt("Try menu?").then(function(b) {
    if (!b) {
      E.showMessage("Ok, no menu");
      print("done");
    } else {
      E.showMenu({
    "" : { "title" : "Test menu" },
    "Item 1" : () => { print("it 1"); },
    "Item 2" : () => { print("it 2"); }
      });
      print("done with menu");
    }
  });
});
}

promises();
