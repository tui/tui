
/* pie library v0.0.1 */
pie = {
  radians: function(a) { return a*Math.PI/180; },

  // Function to draw a filled arc (pie slice)
  fillArc: function(g, centerX, centerY, radius, startAngle, endAngle) {
    const points = [];
    points.push(centerX, centerY); // Start at the center
    
    // Step through angles to create points on the arc
    for (let angle = startAngle; angle <= endAngle; angle += 5) {
      const x = centerX + Math.cos(this.radians(angle)) * radius;
      const y = centerY + Math.sin(this.radians(angle)) * radius;
      points.push(x, y);
    }
    
    // Add the final point at the end angle
    points.push(centerX + Math.cos(this.radians(endAngle)) * radius);
    points.push(centerY + Math.sin(this.radians(endAngle)) * radius);
    
    g.fillPoly(points); // Draw the arc as a polygon
  },

  // Function to draw the pie chart
  drawPieChart1: function(g, centerX, centerY, radius, data, colors) {
    let startAngle = 0;
    
    // Loop through the data to draw each segment
    for (let i = 0; i < data.length; i++) {
      const angle = data[i];         // Get the angle for the current section
      const endAngle = startAngle + angle; // Calculate the end angle
      
      g.setColor(colors[i]);  // Set the fill color
      this.fillArc(g, centerX, centerY, radius, startAngle, endAngle, colors[i]); // Draw the arc
      startAngle = endAngle; // Update the start angle for the next segment
    }

    g.flip(); // Update the screen
  },
  twoPie: function(centerX, centerY, radius, altitude, altChange) {
    g.clear();

    // Altitude range and mapping to a logarithmic scale
    const altitudeMin = -1000, altitudeMax = 1000;
    const altitudeLog = log2(Math.abs(altitude) + 1) * Math.sign(altitude); // Logarithmic scaling
    const altitudeAngle = E.clip((altitudeLog - log2(1)) / (log2(1001) - log2(1)), -1, 1) * 180;

    // Altitude Change (linear scale)
    const altChangeMin = -30, altChangeMax = 30;
    const altChangeAngle = E.clip((altChange - altChangeMin) / (altChangeMax - altChangeMin), 0, 1) * 360;

    // Outer Ring (Altitude Change) - Full circle segment
    g.setColor(1, 0.5, 0.1);
    // Outer Ring (Altitude Change) - Draw a segment based on altitude change

    g.setColor(0, 0.25, 0); // Set a color for the outer ring
    this.fillArc(g,
                 centerX, centerY,
                 radius, // Define the thickness of the outer ring
                 0, altChangeAngle // Draw based on altitude change
                );
    
    // Inner Ring (Altitude) - Draw a segment based on altitude angle
    const innerRadius = radius * 0.6; // Inner ring size
    g.setColor(0, 0, 0.25); // Set a color for the inner ring
    this.fillArc(g,
                 centerX, centerY,
                 innerRadius, // Define thickness of inner ring
                 0, altitudeAngle // Draw based on altitude
                );

    // Draw the baseline circle for reference
    g.setColor(0, 0, 0); // Gray for baseline
    g.drawCircle(centerX, centerY, innerRadius); // Inner circle (reference)
    g.drawCircle(centerX, centerY, radius); // Outer circle (reference)

    // Render the chart
    g.flip();
  }

};

// Test the functions
const data = [60, 120, 90, 90]; // Angles for each section (in degrees)
const colors = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00"]; // Colors for the sections
const centerX = 88; // Center of the pie chart (half of 176)
const centerY = 88;
const radius = 70;

pie.drawPieChart1(g, centerX, centerY, radius, data, colors);



function log2(x) {
  return Math.log(x) / Math.log(2);
}



let altitude = 320; // Current altitude (-1000 to 1000 meters)
let altChange = 12; // Altitude change (-30 to +30)

pie.twoPie(centerX, centerY, radius, altitude, altChange);

