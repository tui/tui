/* North guide */

const BANGLEJS2 = process.env.HWVERSION == 2; // check for bangle 2

/* fmt library v0.1.1 */
let fmt = {
    icon_alt : "\0\x08\x1a\1\x00\x00\x00\x20\x30\x78\x7C\xFE\xFF\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
    icon_m : "\0\x08\x1a\1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
    icon_km : "\0\x08\x1a\1\xC3\xC6\xCC\xD8\xF0\xD8\xCC\xC6\xC3\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
    icon_kph : "\0\x08\x1a\1\xC3\xC6\xCC\xD8\xF0\xD8\xCC\xC6\xC3\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\xFF\x00\xC3\xC3\xFF\xC3\xC3",
    icon_c : "\0\x08\x1a\1\x00\x00\x60\x90\x90\x60\x00\x7F\xFF\xC0\xC0\xC0\xC0\xC0\xFF\x7F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",

  /* 0 .. DD.ddddd
     1 .. DD MM.mmm'
     2 .. DD MM'ss"
   */
    geo_mode : 1,
    
    init: function() {},
    fmtDist: function(km) { return km.toFixed(1) + this.icon_km; },
    fmtSteps: function(n) { return fmtDist(0.001 * 0.719 * n); },
    fmtAlt: function(m) { return m.toFixed(0) + this.icon_alt; },
    fmtTimeDiff: function(d) {
	if (d < 180)
	    return ""+d.toFixed(0);
	d = d/60;
	return ""+d.toFixed(0)+"m";
    },
    fmtAngle: function(x) {
	switch (this.geo_mode) {
	case 0:
            return "" + x;
	case 1: {
            let d = Math.floor(x);
            let m = x - d;
            m = m*60;
            return "" + d + " " + m.toFixed(3) + "'";
	}
	case 2: {
            let d = Math.floor(x);
            let m = x - d;
            m = m*60;
            let mf = Math.floor(m);
            let s = m - mf;
            s = s*60;
            return "" + d + " " + mf + "'" + s.toFixed(0) + '"';
	}
	}
	return "bad mode?";
    },
    fmtPos: function(pos) {
	let x = pos.lat;
	let c = "N";
	if (x<0) {
	    c = "S";
	    x = -x;
	}
	let s = c+this.fmtAngle(x) + "\n";
	c = "E";
	if (x<0) {
	    c = "W";
	    x = -x;
	}
	return s + c + this.fmtAngle(x);
    },
};

/* ui library 0.1 */
let ui = {
  display: 0,
  numScreens: 2,
  drawMsg: function(msg) {
  g.reset().setFont("Vector", 35)
    .setColor(1,1,1)
    .fillRect(0, this.wi, 176, 176)
    .setColor(0,0,0)
    .drawString(msg, 5, 30);
  },
  drawBusy: function() {
    this.drawMsg("\n.oO busy");
  },
  nextScreen: function() {
    print("nextS");
    this.display = this.display + 1;
    if (this.display == this.numScreens)
	    this.display = 0;
    this.drawBusy();
  },
  prevScreen: function() {
    print("prevS");
    this.display = this.display - 1;
    if (this.display < 0)
	    this.display = this.numScreens - 1;
    this.drawBusy();
},
  onSwipe: function(dir) {
    this.nextScreen();
},
    h: 176,
  w: 176,
  wi: 32,
  last_b: 0,
  touchHandler: function(d) {
    let x = Math.floor(d.x);
    let y = Math.floor(d.y);
    
     if (d.b != 1 || this.last_b != 0) {
	     this.last_b = d.b;
	     return;
    }
    
    print("touch", x, y, this.h, this.w);
    
    if ((x<this.h/2) && (y<this.w/2)) {
    }
    if ((x>this.h/2) && (y<this.w/2)) {
    }

    if ((x<this.h/2) && (y>this.w/2)) {
      print("prev");
	    this.prevScreen();
    }
    if ((x>this.h/2) && (y>this.w/2)) {
      print("next");
	    this.nextScreen();
    }
    },
  init: function() {
  }
};

var cur_h, mark_h = 0;

function drawStats() {
    let msg = "";
    c = Bangle.getCompass();

    if (c) {
      cur_h = c.heading;
      msg += cur_h.toFixed(0) + " deg\n";
      msg += c.x.toFixed(0) + "/" + c.y.toFixed(0) + "/" + c.z.toFixed(0) + "\n";
      msg += c.dx.toFixed(0) + "/" + c.dy.toFixed(0) + "/" + c.dz.toFixed(0) + "\n";
    }
    msg += mark_h.toFixed(0) + " deg\n";
    {
      let d = cur_h - mark_h;
      let h = Math.floor((d + 45) / 90);
      h = h % 4;
      d = cur_h - mark_h - (90*h);
      if (d > 180) d -= 360;
      msg += h + "/" + d.toFixed(0);
    }
    print(msg);

    g.reset().clear().setFont("Vector", 31)
  	.setColor(1,1,1)
	  .fillRect(0, 24, 176, 100)
	  .setColor(0,0,0)
	  .drawString(msg, 3, 25);
}

function updateGps() {
    if (ui.display == 0) {
      setTimeout(updateGps, 1000);
      drawStats();
    }
    if (ui.display == 1) {
    }
}

function touchHandler(d) {
    let x = Math.floor(d.x);
    let y = Math.floor(d.y);

     if (d.b != 1 || ui.last_b != 0) {
	     ui.last_b = d.b;
	     return;
    }

    if ((x<ui.h/2) && (y<ui.w/2)) {
      ui.drawMsg("Marked");
      mark_h = cur_h;
    }
    if ((x>ui.h/2) && (y<ui.w/2)) {
      ui.drawMsg("Reset");
      Bangle.resetCompass();
    }
  ui.touchHandler(d);
}


fmt.init();
ui.init();
ui.drawBusy();
Bangle.setCompassPower(1, "speedstep");
Bangle.on("drag", touchHandler);
Bangle.setUI({
  mode : "custom",
  swipe : (s) => ui.onSwipe(s),
  clock : 0
});

if (1) {
  g.reset();
  updateGps();
}
