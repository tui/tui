#!/bin/bash
# For use with: https://github.com/jeffmer/Twatch_Espruino

flash() {
	echo -n "data='"
	cat $1 | base64 | tr -d '\n'
	echo "'"
	echo "data=atob(data)"
	echo "require('Storage').write('$2', data)"
}

# require('Storage').read('lcd.js');

cd ~/g/t-watch/espruino/Twatch_Espruino

base() {
       flash boot0.js .boot0
       flash bootcde.js .bootcde
       for A in lcd.js rtc.js touch.js; do
           flash $A $A
       done
}

base
flash apps/launch/launch.js launch.js

#flash apps/clock/clock.img clock.img
#flash apps/clock/clock.info clock.info
#flash apps/clock/clock.js clock.app.js
#flash apps/clock/ana.js ana.face.js
#flash apps/clock/big.js big.face.js
#flash apps/clock/digi.js digi.face.js
#flash apps/clock/txt.js txt.face.js
