Bluetooth.println("");
//Bluetooth.println(JSON.stringify({t:"intent",target:"activity",action:"android.intent.action.VOICE_COMMAND",flags:["FLAG_ACTIVITY_NEW_TASK"]}));

// Creates "toast", not really visible.
Bluetooth.println(JSON.stringify({t:"error",msg:"Hello world"}));

//No visible effect
//Bluetooth.println(JSON.stringify({t:"call",n:"START"}));

// .DIAL nic nedela, .CALL taky ne
Bluetooth.println(JSON.stringify({t:"intent",action:"android.intent.action.DIAL",target:"activity",data:"tel:800123456"}));

Bluetooth.println(JSON.stringify({t:"intent",action:"android.media.action.STILL_IMAGE_CAMERA",target:"activity"}));

// Creates "toast", not really visible.
Bluetooth.println(JSON.stringify({t:"error",msg:"All done"}));

//java
//Intent callIntent = new Intent(Intent.ACTION_CALL);
//    callIntent.setData(Uri.parse("tel:123456789"));
//    mActivity.startActivity(callIntent);

//t:"intent", target:"...", action:"...", flags:["flag1", "flag2",...], //categories:["category1","category2",...], package:"...", class:"...", //mimetype:"...", data:"...", extra:{someKey:"someValueOrString", //anotherKey:"anotherValueOrString",...

Bluetooth.println(JSON.stringify({t:"call",n:"START"}));

setTimeout(function(){Bangle.showClock();},0);