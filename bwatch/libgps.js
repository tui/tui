var gps_state = {};

function on_gps(f)
{
  fix = {};
  fix.fix = 0;
  fix.lat = 50;
  fix.lon = 14;
  fix.alt = 200;
  fix.speed = 5;
  fix.course = 30;
  fix.time = Date();
  fix.satellites = 5;
  fix.hdop = 12;
  f(fix);

  /*
  "lat": number,      // Latitude in degrees
  "lon": number,      // Longitude in degrees
  "alt": number,      // altitude in M
  "speed": number,    // Speed in kph
  "course": number,   // Course in degrees
  "time": Date,       // Current Time (or undefined if not known)
  "satellites": 7,    // Number of satellites
  "fix": 1            // NMEA Fix state - 0 is no fix
  "hdop": number,     // Horizontal Dilution of Precision
  */
  gps_state.timeout = setTimeout(on_gps, 1000, f);
}

function off_gps()
{
  clearTimeout(gps_state.timeout);
}

//Bangle.on('GPS', print);
on_gps(print);