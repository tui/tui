var interval = 5;

function draw() {
    const history3 = require('Storage').readJSON("widbaroalarm.log.json", true) || []; // history of recent 3 hours

    const now = new Date()/(1000);
    let curtime = now-3*60*60; // 3h ago
    const data = [];
    while (curtime <= now) {
      // find closest value in history for this timestamp
      const closest = history3.reduce((prev, curr) => {
        return (Math.abs(curr.ts - curtime) < Math.abs(prev.ts - curtime) ? curr : prev);
      });
      data.push(closest.p);
      curtime += interval*60;
    }

    Bangle.setUI({
      mode: "custom",
      back: () => showMainMenu(),
    });

    g.reset().setFont("Vector",16);
    require("graph").drawLine(g, data, {
      axes: true,
      x: 4,
      y: Bangle.appRect.y+8,
      height: Bangle.appRect.h-20,
      gridx: 1,
      gridy: 5,
      miny: Math.min.apply(null, data)-1,
      maxy: Math.max.apply(null, data)+1,
      title: /*LANG*/"Barometer history (mBar)",
      ylabel: y => y,
      xlabel: i => {
        const t = -3*60 + interval*i;
        if (t % 60 === 0) {
          return "-" + t/60 + "h";
        }
        return "";
      },
    });
  }

g.reset().clear();
draw();
