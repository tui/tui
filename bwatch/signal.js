function drawSignalBars(x, y, barWidth, barSpacing, barHeights) {
  g.clear(); // Clear the screen

  for (let i = 0; i < barHeights.length; i++) {
    let barX = x + i * (barWidth + barSpacing); // Calculate the X position of each bar
    let barHeight = barHeights[i]; // Get the height of the bar
    let barY = y - barHeight; // Align the bar from the bottom up
    g.fillRect(barX, barY, barX + barWidth - 1, y); // Draw the bar
  }

  g.flip(); // Render the drawn image to the display
}

// Configuration: X, Y position, bar width, spacing, and heights
let x = 50; // Starting X position
let y = 160; // Baseline Y position
let barWidth = 10; // Width of each bar
let barSpacing = 5; // Spacing between bars
let barHeights = [10, 20, 40, 60, 80]; // Heights of the bars

drawSignalBars(x, y, barWidth, barSpacing, barHeights);

