/* Shadow arrow ? */

/* and weather radar needs to be called "storm shadow" :-) */

/* fmt library v0.2.2 */
let fmt = {
  icon_alt : "\0\x08\x1a\1\x00\x00\x00\x20\x30\x78\x7C\xFE\xFF\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
  icon_m : "\0\x08\x1a\1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
  icon_km : "\0\x08\x1a\1\xC3\xC6\xCC\xD8\xF0\xD8\xCC\xC6\xC3\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
  icon_kph : "\0\x08\x1a\1\xC3\xC6\xCC\xD8\xF0\xD8\xCC\xC6\xC3\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\xFF\x00\xC3\xC3\xFF\xC3\xC3",
  icon_c : "\0\x08\x1a\1\x00\x00\x60\x90\x90\x60\x00\x7F\xFF\xC0\xC0\xC0\xC0\xC0\xFF\x7F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",
  icon_hpa : "\x00\x08\x16\x01\x00\x80\xb0\xc8\x88\x88\x88\x00\xf0\x88\x84\x84\x88\xf0\x80\x8c\x92\x22\x25\x19\x00\x00",
  icon_9 : "\x00\x08\x16\x01\x00\x00\x00\x00\x38\x44\x44\x4c\x34\x04\x04\x38\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",
  icon_10 : "\x00\x08\x16\x01\x00\x08\x18\x28\x08\x08\x08\x00\x00\x18\x24\x24\x24\x24\x18\x00\x00\x00\x00\x00\x00\x00",

  /* 0 .. DD.ddddd
     1 .. DD MM.mmm'
     2 .. DD MM'ss"
  */
  geo_mode : 1,

  init: function() {},
  fmtDist: function(km) {
    if (km >= 1.0) return km.toFixed(1) + this.icon_km;
    return (km*1000).toFixed(0) + this.icon_m;
  },
  fmtSteps: function(n) { return this.fmtDist(0.001 * 0.719 * n); },
  fmtAlt: function(m) { return m.toFixed(0) + this.icon_alt; },
  fmtTemp: function(c) { return c.toFixed(1) + this.icon_c; },
  fmtPress: function(p) { 
    if (p < 900 || p > 1100)
      return p.toFixed(0) + this.icon_hpa; 
    if (p < 1000) {
      p -= 900;
      return this.icon_9 + this.add0(p.toFixed(0)) + this.icon_hpa;
    }
    p -= 1000;
    return this.icon_10 + this.add0(p.toFixed(0)) + this.icon_hpa;
  },
  draw_dot : 1,
  add0: function(i) {
    if (i > 9) {
      return ""+i;
    } else {
      return "0"+i;
    }
  },
  fmtTOD: function(now) {
    this.draw_dot = !this.draw_dot;
    let dot = ":";
    if (!this.draw_dot)
      dot = ".";
    return now.getHours() + dot + this.add0(now.getMinutes());
  },
  fmtNow: function() { return this.fmtTOD(new Date()); },
  fmtTimeDiff: function(d) {
    if (d < 180)
      return ""+d.toFixed(0);
    d = d/60;
    return ""+d.toFixed(0)+"m";
  },
  fmtAngle: function(x) {
    switch (this.geo_mode) {
    case 0:
      return "" + x;
    case 1: {
      let d = Math.floor(x);
      let m = x - d;
      m = m*60;
      return "" + d + " " + m.toFixed(3) + "'";
    }
    case 2: {
      let d = Math.floor(x);
      let m = x - d;
      m = m*60;
      let mf = Math.floor(m);
      let s = m - mf;
      s = s*60;
      return "" + d + " " + mf + "'" + s.toFixed(0) + '"';
    }
    }
    return "bad mode?";
  },
  fmtPos: function(pos) {
    let x = pos.lat;
    let c = "N";
    if (x<0) {
      c = "S";
      x = -x;
    }
    let s = c+this.fmtAngle(x) + "\n";
    c = "E";
    if (x<0) {
      c = "W";
      x = -x;
    }
    return s + c + this.fmtAngle(x);
  },
  fmtFix: function(fix, t) {
    if (fix && fix.fix && fix.lat) {
      return this.fmtSpeed(fix.speed) + " " +
        this.fmtAlt(fix.alt);
    } else {
      return "N/FIX " + this.fmtTimeDiff(t);
    }
  },
  fmtSpeed: function(kph) {
    return kph.toFixed(1) + this.icon_kph;
  },
  radians: function(a) { return a*Math.PI/180; },
  degrees: function(a) { return a*180/Math.PI; },
  // distance between 2 lat and lons, in meters, Mean Earth Radius = 6371km
  // https://www.movable-type.co.uk/scripts/latlong.html
  // (Equirectangular approximation)
  // returns value in meters
  distance: function(a,b) {
    var x = this.radians(b.lon-a.lon) * Math.cos(this.radians((a.lat+b.lat)/2));
    var y = this.radians(b.lat-a.lat);
    return Math.sqrt(x*x + y*y) * 6371000;
  },
  // thanks to waypointer
  bearing: function(a,b) {
    var delta = this.radians(b.lon-a.lon);
    var alat = this.radians(a.lat);
    var blat = this.radians(b.lat);
    var y = Math.sin(delta) * Math.cos(blat);
    var x = Math.cos(alat) * Math.sin(blat) -
        Math.sin(alat)*Math.cos(blat)*Math.cos(delta);
    return Math.round(this.degrees(Math.atan2(y, x)));
  },
};

/* gps library v0.1.4 */
let gps = {
  emulator: -1,
  init: function(x) {
    this.emulator = (process.env.BOARD=="EMSCRIPTEN" 
                     || process.env.BOARD=="EMSCRIPTEN2")?1:0;
  },
  state: {},
  on_gps: function(f) {
    let fix = this.getGPSFix();
    f(fix);

    /*
      "lat": number,      // Latitude in degrees
      "lon": number,      // Longitude in degrees
      "alt": number,      // altitude in M
      "speed": number,    // Speed in kph
      "course": number,   // Course in degrees
      "time": Date,       // Current Time (or undefined if not known)
      "satellites": 7,    // Number of satellites
      "fix": 1            // NMEA Fix state - 0 is no fix
      "hdop": number,     // Horizontal Dilution of Precision
    */
    this.state.timeout = setTimeout(this.on_gps, 1000, f);
  },
  off_gps: function() {
    clearTimeout(this.state.timeout);
  },
  getGPSFix: function() {
    if (!this.emulator)
      return Bangle.getGPSFix();
    let fix = {};
    fix.fix = 1;
    fix.lat = 50;
    fix.lon = 14-(getTime()-this.gps_start) / 1000; /* Go West! */
    fix.alt = 200;
    fix.speed = 5;
    fix.course = 30;
    fix.time = Date();
    fix.satellites = 5;
    fix.hdop = 12;
    return fix;
  },
  gps_start : -1,
  start_gps: function() {
    Bangle.setGPSPower(1, "libgps");
    this.gps_start = getTime();
  },
  stop_gps: function() {
    Bangle.setGPSPower(0, "libgps");
  },
};

let ui = {
  display: 0,
  numScreens: 2,
  drawMsg: function(msg) {
    g.reset().setFont("Vector", 35)
      .setColor(1,1,1)
      .fillRect(0, this.wi, 176, 176)
      .setColor(0,0,0)
      .drawString(msg, 5, 30)
      .flip();
  },
  drawBusy: function() {
    this.drawMsg("\n.oO busy");
  },
  nextScreen: function() {
    print("nextS");
    this.display = this.display + 1;
    if (this.display == this.numScreens)
      this.display = 0;
    this.drawBusy();
  },
  prevScreen: function() {
    print("prevS");
    this.display = this.display - 1;
    if (this.display < 0)
      this.display = this.numScreens - 1;
    this.drawBusy();
  },
  onSwipe: function(dir) {
    this.nextScreen();
  },
  h: 176,
  w: 176,
  wi: 32,
  last_b: 0,
  touchHandler: function(d) {
    let x = Math.floor(d.x);
    let y = Math.floor(d.y);
    
    if (d.b != 1 || this.last_b != 0) {
      this.last_b = d.b;
      return;
    }
    
    print("touch", x, y, this.h, this.w);

    /*
      if ((x<this.h/2) && (y<this.w/2)) {
      }
      if ((x>this.h/2) && (y<this.w/2)) {
      }
    */

    if ((x<this.h/2) && (y>this.w/2)) {
      print("prev");
      this.prevScreen();
    }
    if ((x>this.h/2) && (y>this.w/2)) {
      print("next");
      this.nextScreen();
    }
  },
  init: function() {
    this.drawBusy();
  }
};

/* sun version 0.0.4 */
let sun	= {
  SunCalc: null,
  lat: 50,
  lon: 14,
  rise: 0,  /* Unix time of sunrise/sunset */
  set: 0,
  init: function() {  
    try {
      this.SunCalc = require("suncalc"); // from modules folder
    } catch (e) {
      print("Require error", e);
    }
    print("Have suncalc: ", this.SunCalc);
  },
  sunPos: function() {
    let d = new Date();
    if (!this.SunCalc) {
      let sun = {};
      sun.azimuth = 175;
      sun.altitude = 15;
      return sun;
    }
    let sun = this.SunCalc.getPosition(d, this.lat, this.lon);
    print(sun.azimuth, sun.altitude);
    return sun;
  },
  moonPos: function() {
    let d = new Date();
    if (!this.SunCalc) {
      let sun = {};
      sun.azimuth = 195;
      sun.altitude = 15;
      return sun;
    }
    let sun = this.SunCalc.getMoonPosition(d, this.lat, this.lon);
    print(sun.azimuth, sun.altitude);
    return sun;
  },
  sunTime: function() {
    let d = new Date();
    if (!this.SunCalc) {
      let sun = {};
      sun.sunrise = d;
      sun.sunset = d;
      return sun;
    }
    let sun = this.SunCalc.getTimes(d, this.lat, this.lon);
    return sun;
  },
  adj: function (x) {
    if (x < 0)
      return x + 24*60*60;
    return x;
  },
  toSunrise: function () {
    return this.adj(this.rise - getTime());
  },
  toSunset: function () {
    return this.adj(this.set - getTime());
  },
  update: function () {
    let t = this.sunTime();
    this.rise = t.sunrise.getTime() / 1000;
    this.set  = t.sunset.getTime() / 1000;
  },
  // < 0 : next is sunrise, in abs(ret) seconds
  // > 0 
  getNext: function () {
    let rise = this.toSunrise();
    let set = this.toSunset();
    if (rise < set) {
      return -rise;
    }
    return set;
 //   set = set / 60;
 //   return s + (set / 60).toFixed(0) + ":" + (set % 60).toFixed(0);
  },
};

/* arrow library v0.0.1 -- see waypoints */
let arrow = {
  waypoint: { lat: 0, lon: 0 },
  updateWaypoint: function(lat, lon) {
    this.waypoint.lat = lat;
    this.waypoint.lon = lon;
  },
  
  // Calculate the bearing to the waypoint
  bearingToWaypoint: function(currentPos) {
    return fmt.bearing(currentPos, this.waypoint);
  },

  // Calculate distance to the waypoint
  distanceToWaypoint: function(currentPos) {
    return fmt.distance(currentPos, this.waypoint);
  },

  // Calculate compass heading from current GPS data
  compassHeading: function(currentHeading) {
    return currentHeading || Bangle.getCompass().heading;
  },

  // Display function to show arrows for waypoint, north, and sun
  displayNavigation: function(currentPos, currentHeading) {
    g.clear().setFont("Vector", 22).setFontAlign(0, 0);
    
    // Calculate bearings
    let waypointBearing = this.bearingToWaypoint(currentPos);
    let distance = this.distanceToWaypoint(currentPos);
    let northHeading = this.compassHeading(currentHeading);

    // Format distance
    let distStr = fmt.fmtDist(distance/1000);

    northHeading = 45;
    // Draw compass arrow for north
    this.drawArrow(northHeading, "N", 1);

    // Draw arrow towards waypoint
    if (1)
      this.drawArrow(waypointBearing, `${distStr}`, 3);

    let s;
    
    s = sun.sunPos();
    // Draw sun arrow if sun is visible
    if (s.altitude > 0) {
      this.drawArrow(s.azimuth, "Sun", 1);
    }
    s = sun.moonPos();
    // Draw sun arrow if sun is visible
    if (s.altitude > 0) {
      this.drawArrow(s.azimuth, "Moon", 1);
    }
  },

  drawArrow: function(angle, label, width) {
    // Convert angle to radians
    let rad = angle * Math.PI / 180;

    // Arrow parameters
    let centerX = 88;
    let centerY = 88;
    let length = 60; // Arrow length
    
    g.drawCircle(centerX, centerY, length);

    // Calculate the rectangle's corner points for the rotated arrow
    let dx = Math.sin(rad) * length;
    let dy = -Math.cos(rad) * length;
    let px = Math.sin(rad + Math.PI / 2) * (width / 2);
    let py = -Math.cos(rad + Math.PI / 2) * (width / 2);

    // Calculate each corner of the rectangle (arrow body)
    let x1 = centerX + px;
    let y1 = centerY + py;
    let x2 = centerX - px;
    let y2 = centerY - py;
    let x3 = x2 + dx * 0.9;
    let y3 = y2 + dy * 0.9;
    let x4 = x1 + dx * 0.9;
    let y4 = y1 + dy * 0.9;

    // Draw the filled rectangle for the arrow body
    g.fillPoly([x1, y1, x2, y2, x3, y3, x4, y4]);

    // Draw the label at the arrow's endpoint
    g.setFontAlign(0, 0);
    g.drawString(label, x3 + dx * 0.4, y3 + dy * 0.4);

    // Draw arrowhead
    this.drawArrowHead(rad, centerX + dx, centerY + dy);
  },

  drawArrowHead: function(rad, x, y) {
    // Arrowhead parameters
    let headLength = 16;   // Length of each arrowhead side
    let headAngle = Math.PI / 6; // Arrowhead angle

    let angle = rad - Math.PI/2;

    // Calculate positions for arrowhead points
    let xHead1 = x - headLength * Math.cos(angle - headAngle);
    let yHead1 = y - headLength * Math.sin(angle - headAngle);
    let xHead2 = x - headLength * Math.cos(angle + headAngle);
    let yHead2 = y - headLength * Math.sin(angle + headAngle);

    // Draw the arrowhead as a filled triangle
    g.fillPoly([x, y, xHead1, yHead1, xHead2, yHead2]);
  }
};

// Initialize libraries
fmt.init();
gps.init();
sun.init();
ui.init();

gps.start_gps();

function step() {
  fix = gps.getGPSFix();
  g.clear();
  arrow.displayNavigation(fix, fix.course);
  if (1) {
    g.setFont("Vector", 28);
    g.setFontAlign(-1, -1).drawString("", 0, 0);
    g.setFontAlign(1, -1).drawString("", ui.w, 0);
    g.setFontAlign(-1, 1).drawString("", 0, ui.h);
    g.setFontAlign(1, 1).drawString("", ui.w, ui.h);
  }
  setTimeout(step, 1000);
}

// Waypoint coordinates for testing (update with actual values)
arrow.updateWaypoint(50.087, 14.421); // Example coordinates for Prague

step();

// Swipe to toggle different screens
Bangle.on('swipe', ui.onSwipe);
Bangle.on('touch', ui.touchHandler);

// Display initialization message
ui.drawMsg("Initializing...");
