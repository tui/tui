//var Layout = require("Layout");

const W = g.getWidth();
const H = g.getHeight();

var cx = 100; cy = 105; sc = 70;
var buzz = "", msg = "", inm = "", l = "", note = "";
temp = 0; alt = 0; bpm = 0;

function inputHandler(s) {
  print("Ascii: ", s);
  note = note + s;
}

function morseToAscii(morse) {
  const morseDict = {
    '.-': 'A',
    '-...': 'B',
    '-.-.': 'C',
    '-..': 'D',
    '.': 'E',
    '..-.': 'F',
    '--.': 'G',
    '....': 'H',
    '..': 'I',
    '.---': 'J',
    '-.-': 'K',
    '.-..': 'L',
    '--': 'M',
    '-.': 'N',
    '---': 'O',
    '.--.': 'P',
    '--.-': 'Q',
    '.-.': 'R',
    '...': 'S',
    '-': 'T',
    '..-': 'U',
    '...-': 'V',
    '.--': 'W',
    '-..-': 'X',
    '-.--': 'Y',
    '--..': 'Z'
  };

  return morseDict[morse];
}

function asciiToMorse(char) {
  const asciiDict = {
    'A': '.-',
    'B': '-...',
    'C': '-.-.',
    'D': '-..',
    'E': '.',
    'F': '..-.',
    'G': '--.',
    'H': '....',
    'I': '..',
    'J': '.---',
    'K': '-.-',
    'L': '.-..',
    'M': '--',
    'N': '-.',
    'O': '---',
    'P': '.--.',
    'Q': '--.-',
    'R': '.-.',
    'S': '...',
    'T': '-',
    'U': '..-',
    'V': '...-',
    'W': '.--',
    'X': '-..-',
    'Y': '-.--',
    'Z': '--..'
  };

  return asciiDict[char];
}

function morseHandler() {
  inputHandler(morseToAscii(inm));
  inm = "";
  l = "";
}

function touchHandler(d) {
  let x = Math.floor(d.x);
  let y = Math.floor(d.y);

  g.setColor(0.25, 0, 0);
  g.fillCircle(W-x, W-y, 5);

  if (d.b) {
  if (x < W/2 && y < H/2 && l != ".u") {
    inm = inm + ".";
    l = ".u";
  }
  if (x > W/2 && y < H/2 && l != "-u") {
    inm = inm + "-";
    l = "-u";
  }
  if (x < W/2 && y > H/2 && l != ".d") {
    inm = inm + ".";
    l = ".d";
  }
  if (x > W/2 && y > H/2 && l != "-d") {
    inm = inm + "-";
    l = "-d";
  }
    
  } else
    morseHandler();
  
  print(inm, "drag:", d);
}

function add0(i) {
  if (i > 9) {
    return ""+i;
  } else {
    return "0"+i;
  }
}

function draw() {
  g.setColor(1, 1, 1);
  g.fillRect(0, 0, W, H);
  g.setFont('Vector', 60);

  g.setColor(0, 0, 0);
  g.setFontAlign(-1, 1);
  let now = new Date();
  g.drawString(now.getHours() + ":" + add0(now.getMinutes()), 10, 60);

  g.setFont('Vector', 26);
  g.drawString(note, 10, 120);
  
  queueDraw();
}

var drawTimeout;

function queueDraw() {
  if (drawTimeout) clearTimeout(drawTimeout);
  next = 250;
  drawTimeout = setTimeout(function() {
    drawTimeout = undefined;
    draw();
  }, next - (Date.now() % next));

}

function start() {
  Bangle.on("drag", touchHandler);

  draw();
}


g.reset();
Bangle.setUI();
start();