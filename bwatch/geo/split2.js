#!/usr/bin/nodejs

// npm install geo-splitter

//import geojsonSample from 'tatry.gjson'; //Import your data
//import split from 'geo-splitter';

// THis one can only split polygons :-(.

var split = require('geo-splitter');

var geojsonSample = require("./delme.json");

const xStart = 0;
const xEnd = 100;
const yStart = 0;
const yEnd = 50;
const gridSize = 10;
const bypassAnalysis = false; //(Default to false)

console.log("Hello world");
const splittedSample = split.split(geojsonSample, xStart, xEnd, yStart, yEnd, gridSize, bypassAnalysis);
