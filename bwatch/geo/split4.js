#!/usr/bin/nodejs

// From https://github.com/Rich-Harris/geotile

var geotile = require( 'geotile' );

//const fs = require('fs');
//s = fs.readFileSync("reduce.geojson")
//s = JSON.parse(s);

//s = require("./reduce.geojson");
s = require("./delme.json");


console.log(s);
console.log(geotile({}));

// get a specific region of a single source
var region = geotile( s ).constrain({
  north: 50.1,
  east: 15.1,
  south: 50.0,
  west: 15.0
});

// save region as GeoJSON. The object's `bbox` property
// is equivalent to [ west, south, east, north ]
region.toJSON();
region.toString(); // equivalent to JSON.stringify( region.toJSON() );

// create a grid of tiles. This returns a one-dimensional
// array of regions. All settings are optional (you can just
// call `.grid()`), and default to the values below
var grid = geotile( land ).grid({
  // default to the entire world...
  north: 90,
  east: 180,
  south: -90,
  west: -180,

  // ...split into tiles of 10x10 degrees
  horizontalDivisions: 36,
  verticalDivisions: 18
});

// save GeoJSON files
grid.forEach( function ( region ) {
  var filename = 'tiles/' + region.west + '_' + region.north + '.json';
  fs.writeFile( filename, region.toString() );
});
