#!/usr/bin/nodejs

const fs = require('fs');

// JSON.parse(string)
//fs.open("tatry.geojson", "r", function (err, f) {
//    console.log(f)
//});

// Converted using https://tyrasd.github.io/osmtogeojson/

debug = 1
if (debug) console.log("Opening")
s = fs.readFileSync("cr.geojson")
if (debug) console.log("Parsing")
d = JSON.parse(s)
if (debug) console.log("Walking")

if (d.type != "FeatureCollection")
    console.log("Expecting collection")
n = {}
n.type = "FeatureCollection"
n.features = []
for (var a of d.features) {
    take = false;
    if (a.type != "Feature")
	console.log("Expecting feature")
    //    if (a.properties.highway != "path" && a.properties.place != "locality")
    if (a.properties.highway == "primary" || a.properties.highway == "secondary" || a.properties.highway == "tertiary")
	take = true;
    if (a.properties.place == "city" || a.properties.place == "town" || a.properties.place == "village")
	take = true;

    if (!take)
	continue;
    //console.log(a.properties.place, a.properties.name)
    n.features.push(a);
}
//console.log(JSON.stringify(n, "", " "))
fs.writeFileSync("reduce.geojson", JSON.stringify(n, "", " "))
