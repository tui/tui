const math = require('mathjs');

function latLonToTile(lat, lon, z) {
  const n = 2 ** z;
  const x = math.floor((lon + 180) * n / 360);
  const y = math.floor((math.pi * (lat + 90) / 180) * n - 0.5);
  return [x, y, z];
}

if (require.main === module) {
  const tile = latLonToTile(37.421928, -122.084667, 18);
  console.log(tile);
}

const math = require('mathjs');

function tileToLatLon(x, y, z) {
  const n = 2 ** z;
  const lat = math.atan(math.sinh(math.pi * (y + 0.5) / n)) * 180 / math.pi;
  const lon = (x * 360) / n - 180;
  return [lat, lon];
}

if (require.main === module) {
  const latLon = tileToLatLon(1234, 5678, 18);
  console.log(latLon);
}
