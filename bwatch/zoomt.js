/* zoom library v0.0.3 */
var zoom = {
  buf : 0,
  /* y coordinate is "strange" -- positive values go north */ 
  /* x1 -- left, x2 -- right of simulated canvas.
     we want x1 < x2, y1 < y2. */
  x1 : 0, x2 : 0, y1 : 0, y2 : 0, 
  /* screen size in pixels */
  ss : 176,
  /* size in pixels */
  init : function(size) {
    this.size = size;
    this.buf = Graphics.createArrayBuffer(size, size, 2, { msb: true });
  },
  clear : function() {
    this.buf.reset().clear();
  },
  /* output: 0..1 */
  xrel : function(i)  {
    let r = {};
    r.x = ((i.x - this.x1) / (this.x2 - this.x1));
    r.y = ((i.y - this.y1) / (this.y2 - this.y1));
    return r;
  },
  /* input: meters, output: pixels in buf*/
  xform : function(p) {
    let r = this.xrel(p);
    r.x *= this.size;
    r.y *= -this.size;
    r.y += this.size;
    return r;
  },
  /* takes x, y with lat/lon m */
  geoLine : function(i1, i2) {
    this.drawLine(Bangle.project(i1), Bangle.project(i2));
  },
  /* takes x, y in m */
  drawLine : function(i1, i2) {
    let p1 = this.xform(i1);
    let p2 = this.xform(i2);
    print("line", p1, p2);
    this.buf.drawLine(p1.x, p1.y, p2.x, p2.y);
  },
  geoPaint : function(i, head, z) {
    this.mPaint(Bangle.project(i), head, z);
  },
  /* vx, vy: viewpoint in meters,
     head: which heading to display as up,
     zoom: how many meters from center of screen to edge */
  mPaint : function(v, head, z) {
    let sh = this.xrel(v);
    sh.x = sh.x - 0.5;
    sh.y = 0.5 - sh.y;
    let scale = ((this.y2-this.y1)/(z*2)) * this.ss/this.size;
    let dist = Math.sqrt(sh.x*sh.x + sh.y*sh.y) * this.ss * scale;
    let theta = Math.atan2(-sh.y, sh.x);
    let rad = (head / 360) * 2 * Math.PI;
    let ox = Math.sin(theta - rad + 1.5*Math.PI) * dist;
    let oy = Math.cos(theta - rad + 1.5*Math.PI) * dist;
    /*
    print("scale", scale);
    print("dist", dist);
    print("o", ox, oy);
    */
    
    /* drawimage... takes middle of the image, and rotates around it.
                ... in pixels
       scale is pixels to pixels */
    g.drawImage(zoom.buf, 176/2 + ox, 176/2 + oy,
                { rotate: rad,
                  scale: scale });
  }
};


function testimg(g) {
  g.reset().clear();
  g.setFont("Vector", 31);
  g.setFontAlign(-1, -1);
  g.drawString("Hello world \n This is a \n long story \n of watch \n resisting", 3, 3);

  g.setFont("Vector", 23);
  //g.setColor(0, 0, 0);
  g.setFontAlign(-1, 1);
  g.drawString("12:34", 3, 176);
  g.setFontAlign(1, 1);
  g.drawString("567m", 176-3, 176);

  zoom.drawLine({ x: -9.9, y: -9.9 + yoff }, { x: 9.9, y: -9.9 + yoff } );
  zoom.drawLine({ x:  9.9, y: -9.9 + yoff }, { x: 9.9, y:  9.9 + yoff } );
  zoom.drawLine({ x:  9.9, y:  9.9 + yoff }, { x: -9.9, y: 9.9 + yoff } );
  zoom.drawLine({ x: -9.9, y:  9.9 + yoff }, { x: -9.9, y: -9.9 + yoff} );

  zoom.drawLine({ x: -10, y:  -10 + yoff }, { x: 10, y: 10 + yoff } );
}

g.reset().clear();
zoom.init(176);
yoff = 300;
zoom.x1 = -10; zoom.y1 = -10 + yoff;
zoom.x2 =  10; zoom.y2 = 10 + yoff;
testimg(zoom.buf);

var i = 0, j = 0;

function draw() {
  g.reset().clear();
  print(".");
  //g.drawImage(zoom.buf, i, i, { rotate: Math.PI / 4 + i/100, scale: 1-i/100 });
  
  //zoom.mPaint({ x: 10, y: 10 + yoff}, i, 10); // rotate around top right corner
  //zoom.mPaint({ x: 10, y: -10 + yoff }, i, 10); // rotate around bottom right corner
  
  
  zoom.mPaint({ x: 0, y: yoff}, i, 10 - i/10); // rotate around middle
  i++;
  if (i == 90)
    i = 0;
}

function step() {
  let v1 = getTime();
  draw();
  let v2 = getTime();
  print("Step took", (v2-v1), "seconds");
  setTimeout(step, 100);
}

setTimeout(step, 100);
