var  on = false;
setInterval(function() {
  on = !on;
  LED1.write(on);
}, 500);

function sum(a,b) {
  //"compiled";
  let s = 0;
  for (let i=0; i<4000; i++)
    s += i*a + b;
  return s;
}

// A C function to add an offset to each element of an integer array
//var addOffset = E.nativeCall(`
//  void addOffset(int *arr, int len, int offset) {
//    for (int i = 0; i < len; i++) {
//      arr[i] += offset;
 //   }
//  }
//`, "void(int*,int,int)");

// Allocate an array in JS and apply the function
var arr = new Int32Array([10, 20, 30, 40]);
//addOffset(arr, arr.length, 5); // Add 5 to each element
print(arr); // Outputs: new Int32Array([15, 25, 35, 45])

var c = E.compiledC(`
// int fibo(int)
int fibo(int n){
 if(n <= 1){
  return n;
 }
 int fibo = 1;
 int fiboPrev = 1;
 for(int i = 2; i < n; ++i){
  int temp = fibo;
  fibo += fiboPrev;
  fiboPrev = temp;
 }
 return fibo;
}
`);

print(c.fibo(20));

t1 = getTime();
print(sum(1,2));
print(getTime() - t1);