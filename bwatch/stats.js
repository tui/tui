/* fmt library v0.2.0 */
let fmt = {
    icon_alt : "\0\x08\x1a\1\x00\x00\x00\x20\x30\x78\x7C\xFE\xFF\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
    icon_m : "\0\x08\x1a\1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
    icon_km : "\0\x08\x1a\1\xC3\xC6\xCC\xD8\xF0\xD8\xCC\xC6\xC3\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\x00\x00\x00\x00\x00\x00\x00",
    icon_kph : "\0\x08\x1a\1\xC3\xC6\xCC\xD8\xF0\xD8\xCC\xC6\xC3\x00\xC3\xE7\xFF\xDB\xC3\xC3\xC3\xC3\x00\xFF\x00\xC3\xC3\xFF\xC3\xC3",
    icon_c : "\0\x08\x1a\1\x00\x00\x60\x90\x90\x60\x00\x7F\xFF\xC0\xC0\xC0\xC0\xC0\xFF\x7F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",

  /* 0 .. DD.ddddd
     1 .. DD MM.mmm'
     2 .. DD MM'ss"
   */
    geo_mode : 1,
    
    init: function() {},
    fmtDist: function(km) {
	if (km >= 1.0) return km.toFixed(1) + this.icon_km;
	return (km*1000).toFixed(0) + this.icon_m;
    },
    fmtSteps: function(n) { return this.fmtDist(0.001 * 0.719 * n); },
    fmtAlt: function(m) { return m.toFixed(0) + this.icon_alt; },
    draw_dot : 1,
    add0: function(i) {  
      if (i > 9) {
        return ""+i;
      } else {
        return "0"+i;
      }
    },
    fmtTOD: function(now) {
      this.draw_dot = !this.draw_dot;
      let dot = ":";
      if (!this.draw_dot)
        dot = ".";
      return now.getHours() + dot + this.add0(now.getMinutes());
    },
    fmtNow: function() { return this.fmtTOD(new Date()); },
    fmtTimeDiff: function(d) {
        if (d < 180)
            return ""+d.toFixed(0);
        d = d/60;
        return ""+d.toFixed(0)+"m";
    },
    fmtAngle: function(x) {
        switch (this.geo_mode) {
        case 0:
            return "" + x;
        case 1: {
            let d = Math.floor(x);
            let m = x - d;
            m = m*60;
            return "" + d + " " + m.toFixed(3) + "'";
        }
        case 2: {
            let d = Math.floor(x);
            let m = x - d;
            m = m*60;
            let mf = Math.floor(m);
            let s = m - mf;
            s = s*60;
            return "" + d + " " + mf + "'" + s.toFixed(0) + '"';
        }
        }
        return "bad mode?";
    },
    fmtPos: function(pos) {
        let x = pos.lat;
        let c = "N";
        if (x<0) {
            c = "S";
            x = -x;
        }
        let s = c+this.fmtAngle(x) + "\n";
        c = "E";
        if (x<0) {
            c = "W";
            x = -x;
        }
        return s + c + this.fmtAngle(x);
    },
    fmtFix: function(fix, t) {
        if (fix && fix.fix && fix.lat) {
          return this.fmtSpeed(fix.speed) + " " +
                  this.fmtAlt(fix.alt);
        } else {
          return "N/FIX " + this.fmtTimeDiff(t);
        }
    },
    fmtSpeed: function(kph) {
      return kph.toFixed(1) + this.icon_kph;
    },
  radians: function(a) { return a*Math.PI/180; },
  degrees: function(a) { return a*180/Math.PI; },
  // distance between 2 lat and lons, in meters, Mean Earth Radius = 6371km
  // https://www.movable-type.co.uk/scripts/latlong.html
  // (Equirectangular approximation)
  // returns value in meters
  distance: function(a,b) {
    var x = this.radians(b.lon-a.lon) * Math.cos(this.radians((a.lat+b.lat)/2));
    var y = this.radians(b.lat-a.lat);
    return Math.sqrt(x*x + y*y) * 6371000;
  },
};

function new_day() {
  let r = {};
  r.lines = 0;
  r.points = 0;
  r.day = -1;
  r.dist = 0;
  r.pos = undefined;
  return r;
}

function print_day(d) {
  let a = Date(d.day * (60*60*24) * 1000);
  //a.getFullYear(), a.getMonth()+1, a.getDay(),
  print("day", d.day, a.toString(), d.points, "/", d.lines, "points", d.dist / 1000, "km");
}

function parse(l) {
  let r = {};
  let s = l.split(' ');
  
  if (s[1].split('=')[1] === undefined) {
    r.lat = 1 * s[0];
    r.lon = 1 * s[1];
  }

  for (let fi of s) {
      let f = fi.split('=');
      if (f[0] == "utime") {
        r.utime = 1 * f[1];
      }
  }

  return r;
}

function myseek(f, v) {
  let step = 32768;
  for (let i=0; i<Math.floor(v/step); i++) {
    print("seek", i, v/step);
    f.read(step);
  }
  f.read(v%step);
}

function toDay(v) {
  return Math.floor(v / (60*60*24));
}

function read() {
  let f = require("Storage").open("sixths.egt", "r");
  let l = f.readLine();
  //myseek(f, 4000000);
  let num = 1000;
  let day = new_day();
  while (l!==undefined && --num != 0) {
    l = ""+l;
    //print(l);
    day.lines ++;
    let p = parse(l);
    if (p.lat) {
      day.points ++;
      if (day.pos) {
        let dt = p.utime - day.pos.utime;
        let dd = fmt.distance(day.pos, p);
        if (dt > 60*60) {
          print("Too long");
        } else if ((dd / dt) > 2000) {
          print("Too fast");
        } else
          day.dist += dd;
      }
      day.pos = p;
    }
    if (p.utime) {
        let nday = toDay(p.utime);
        if (nday != day.day) {
          print_day(day);
          //print("New day", day);
          day = new_day();
          day.day = nday;
        }
      //print("ut", p.utime);
    }
    
    l = f.readLine();
    if (!(num % 100))
      print(num, "to go");
  }
}

function time_read() {
  let v1 = getTime();
  read();
  let v2 = getTime();
  print("Read took", (v2-v1)/60, "minutes");
  print("Today", toDay(v2));
}


fmt.init();
setTimeout(time_read, 200);
