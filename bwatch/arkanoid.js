// Arkanoid Clone for Bangle.js 2

const SCREEN_WIDTH = g.getWidth();
const SCREEN_HEIGHT = g.getHeight();

let paddle = { x: SCREEN_WIDTH / 2 - 20, y: SCREEN_HEIGHT - 20, width: 40, height: 8 };
let ball = { x: SCREEN_WIDTH / 2, y: SCREEN_HEIGHT / 2, radius: 5, dx: 3, dy: 1 };
let bricks = [];
let brickCols = 6;
let brickRows = 3;
let brickWidth = SCREEN_WIDTH / brickCols;
let brickHeight = 15;

// Initialize bricks
function initBricks() {
  bricks = [];
  for (let row = 0; row < brickRows; row++) {
    for (let col = 0; col < brickCols; col++) {
      bricks.push({
        x: col * brickWidth,
        y: row * brickHeight,
        width: brickWidth - 2,
        height: brickHeight - 2,
        isHit: false,
      });
    }
  }
}

// Draw paddle
function drawPaddle() {
  g.setColor(0, 0, 1);
  g.fillRect(paddle.x, paddle.y, paddle.x + paddle.width, paddle.y + paddle.height);
}

// Draw ball
function drawBall() {
  g.setColor(0, 1, 1);
  g.fillCircle(ball.x, ball.y, ball.radius);
}

// Draw bricks
function drawBricks() {
  g.setColor(1, 0, 0);
  bricks.forEach(brick => {
    if (!brick.isHit) {
      g.fillRect(brick.x, brick.y, brick.x + brick.width, brick.y + brick.height);
    }
  });
}

// Move paddle
function movePaddle(e) {
  let pos = e.x;
  print(e);
  if (!e.b)
    return;
  paddle.x = Math.max(0, Math.min(SCREEN_WIDTH - paddle.width, pos - paddle.width / 2));
}

// Ball movement and collision detection
function moveBall() {
  ball.x += ball.dx;
  ball.y += ball.dy;

  // Ball collision with walls
  if (ball.x < ball.radius || ball.x > SCREEN_WIDTH - ball.radius) ball.dx *= -1;
  if (ball.y < ball.radius) ball.dy *= -1;

  // Ball collision with paddle
  if (
    ball.x > paddle.x &&
    ball.x < paddle.x + paddle.width &&
    ball.y + ball.radius >= paddle.y &&
    ball.y - ball.radius <= paddle.y + paddle.height
  ) {
    let dx = ball.x - paddle.x;
    dx /= paddle.width;
    dx -= 0.5;
    dx *= 4;
    ball.dx = dx;
    ball.dy *= -1;
    ball.y = paddle.y - ball.radius; // Move the ball out of the paddle
  }

  // Ball collision with bricks
  bricks.forEach(brick => {
    if (
      !brick.isHit &&
      ball.x > brick.x &&
      ball.x < brick.x + brick.width &&
      ball.y - ball.radius <= brick.y + brick.height &&
      ball.y + ball.radius >= brick.y
    ) {
      // Determine collision side (top/bottom or left/right)
        let isVerticalHit =
          ball.x > brick.x && ball.x < brick.x + brick.width;

        if (isVerticalHit) {
          ball.dy *= -1; // Reverse vertical direction
        } else {
          ball.dx *= -1; // Reverse horizontal direction
        }

      brick.isHit = true;
    }
  });

  // Check for game over
  if (ball.y > SCREEN_HEIGHT) resetGame();
}

// Reset game
function resetGame() {
  ball.x = SCREEN_WIDTH / 2;
  ball.y = SCREEN_HEIGHT / 2;
  ball.dy = 3 * (SCREEN_HEIGHT / 176);
  ball.dx = 3 * (SCREEN_HEIGHT / 176);
  initBricks();
}

// Main game loop
function gameLoop() {
  g.clear();
  drawPaddle();
  drawBall();
  drawBricks();
  moveBall();
  g.flip();
}

// Set up game
initBricks();
resetGame();
setInterval(gameLoop, 50);
Bangle.on('drag', movePaddle);
