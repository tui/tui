function eat(on) {
  Bangle.setOptions({backlightTimeout: 0})
  Bangle.setGPSPower(on, "sixths");
  Bangle.setCompassPower(on, "sixths");
  Bangle.setBarometerPower(on, "sixths");
  Bangle.setHRMPower(on, "sixths");
  Bangle.setBacklight(on);
  if (on) {
    for (let i=0; i<10000; i++) ;
  }
}

function logstamp(s) {
    logfile.write("utime=" + getTime() + " " + s + "\n");
}

drain = 0;
cooldown = 0;

function periodic() {
  bat = E.getBattery();
  
  g.setColor(1, 1, 1);
  g.fillRect(0, 24, 240, 240);
  g.setFontAlign(-1, 1);
  g.setColor(0, 0, 0);
  g.setFont('Vector', 26);
  let now = new Date();
  msg = now.getHours() + ":" + now.getMinutes() + " " + bat + "% ";
  msg2 = cooldown + "/" + drain;
  g.drawString(msg, 10, 90);
  g.drawString(msg2, 10, 130);
  
  print(msg + msg2);
  logstamp(msg + msg2)
  if (bat < 2) {
    if (drain)
      drain--;
    else {
      if (cooldown) {
        eat(0);
        cooldown--;
      } else {
        cooldown = 7*60;
        drain = 2*60;
        eat(1);
      }
    }
  } else {
      eat(1);
      drain = 2*60;
  }
}

let logfile = require("Storage").open("crash.log", "a");
setInterval(periodic, 1000);