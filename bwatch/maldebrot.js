//var Layout = require("Layout");

const W = g.getWidth();
const H = g.getHeight();

var minX = -2;
var maxX = 1;
var minY = -1;
var maxY = 2;
var step = 30;
var zoom = 0.75;


var cx = 100; cy = 105; sc = 70;
var buzz = "", msg = "";
temp = 0; alt = 0; bpm = 0;

function shiftx(v) { minX += v; }
function shifty(v) { minY += v; }

function touchHandler(b, d) {
  let x = Math.floor(d.x);
  let y = Math.floor(d.y);
  
  if (x > 0 && x < W/3 && y > H/3 && y < (2*H)/3) {
    shiftx(-zoom);
  } else if (x > (2*W/3) && x < W && y > H/3 && y < (2*H)/3) {
    shiftx(+zoom);
  } else if (x > W/3 && x < (2*W/3) && y > 0 && y < H/3) {
    shifty(-zoom);
  } else if (x > W/3 && x < (2*W/3) && y > (2*H)/3 && y < H) {
    shifty(+zoom);
  } else if (x > 0 && x < W/3 && y > 0 && y < H/3) {
    step = step*0.66;
  } else if (x > 0 && x < W/3 && y > (2*H)/3 && y < H) {
    step = step*1.5;
  } else if (x > (2*W)/3 && y > 0 && y < H/3) {
    zoom = zoom*0.66;
  } else if (x > (2*W)/3 && y > (2*H)/3 && y < H) {
    zoom = zoom*1.5;
  } else if (x > W/3 && x < (2*W/3) && y > H/3 && y < (2*H)/3) {
    step = 1;
  }
  
  maxX = minX + 2*zoom;
  maxY = minY + 2*zoom;

  print(minX, minY, maxX, maxY, zoom, step);

  paint(step);
}

function paint(step) {
  // Set canvas dimensions and max iterations
  const canvasWidth = W;
  const canvasHeight = H;
  const maxIterations = 100;

  // Define the range and scaling factor for the Mandelbrot set
  const scaleX = canvasWidth / (maxX - minX);
  const scaleY = canvasHeight / (maxY - minY);
  
  g.setColor(1, 0, 0);
  g.fillRect(0, 0, W, H);

  // Loop through all pixels and calculate whether they belong to the Mandelbrot set
  for (let x = 0; x < canvasWidth; x+=step) {
   for (let y = 0; y < canvasHeight; y+=step) {
    // Convert pixel coordinates to Mandelbrot set coordinates
    const cx = (x / scaleX) + minX;
    const cy = (y / scaleY) + minY;
    
    // Initialize variables for Mandelbrot set calculation
    let zx = 0;
    let zy = 0;
    let i = 0;
    
    // Calculate whether the current point belongs to the Mandelbrot set
    while (zx * zx + zy * zy < 4 && i < maxIterations) {
      const tmp = zx * zx - zy * zy + cx;
      zy = 2 * zx * zy + cy;
      zx = tmp;
      i++;
    }
    
    // Set the color of the pixel based on how many iterations it took to escape the Mandelbrot set
    let color = i === maxIterations ? 0 : i / maxIterations;
    if (color)
      color = 1;
    g.setColor(Math.floor(color * 255), Math.floor(color * 255), Math.floor(color * 255));
    g.fillRect(x, y, x+step, y+step);
  }
  g.flip();
  print(x, "/", W);
 }
}

var drawTimeout;

function queueDraw() {
  if (drawTimeout) clearTimeout(drawTimeout);
  next = 500;
  drawTimeout = setTimeout(function() {
    drawTimeout = undefined;
    draw();
  }, next - (Date.now() % next));

}

function start() {
  Bangle.on("touch", touchHandler);
  paint(step);
}

g.reset();
Bangle.setUI();
start();