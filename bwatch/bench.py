#!/usr/bin/python3
# http://oldcomputer.info/8bit/benchmarks/index.htm
# 10000x! Lasts 3 sec on X220. 
import math
for q in range(10000):
  s=0
  for n in range(100):
    a = n+1
    for i in range(10):
    	a=math.sqrt(a)
    for i in range(10):
    	a=a*a
    s = s+a
print(1010-s/5)
