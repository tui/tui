
// 0100 1001
// 0010 0100
var c = E.compiledC(`
// int c_test(int)
// int drawGradient(int, int, int, int)

int  drawGradient(int, int, int, int, int, int, int, int, int) {
return 0;
}

unsigned int rand_simple(void) {  static unsigned int seed = 12345;  seed = (1664525 * seed + 1013904223) % 0xFFFFFFFF;    return seed; }

// Framebuffer dimensions as constants
const int WIDTH = 176;  // Display width
const int HEIGHT = 176; // Display height

int c_fill(unsigned char *c) {
  for(int i=0; i<(176*176*3)/8; ) {
    int v = 0x249 << 2;

    if (rand_simple() < 0x80000000)      v = 0;
    c[i++] = v;
    c[i++] = v >> 1;
    c[i++] = v >> 2; 
  } 
  return 1234;
}

typedef unsigned char uint8_t;
typedef unsigned int uint32_t;


// Framebuffer buffer: WIDTH * HEIGHT * 3 bits per pixel

// Set a pixel at (x, y) to a specific color (0-7)
void setPixel(unsigned char *framebuffer, int x, int y, uint8_t color) {
    if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT || color > 7) {
        // Invalid input: Out of bounds or color out of range
        return;
    }

    // Calculate pixel position in the framebuffer
    int pixel_index = y * WIDTH + x;         // Linear index of the pixel
    int bit_index = pixel_index * 3;        // Start bit position in the buffer
    int byte_index = bit_index / 8;         // Byte containing the pixel
    int bit_offset = bit_index % 8;         // Bit offset within that byte

    int mask = 0xe000 >> bit_offset;
    int val =  (color << 13) >> bit_offset;

    // Clear the existing pixel value
    framebuffer[byte_index] &= ~(mask >> 8); // Clear 3 bits for the pixel
    framebuffer[byte_index] |= (val >> 8);  // Set new value

    // Handle bits spilling into the next byte
    if (mask & 0x00ff) {
        framebuffer[byte_index + 1] &= ~mask; // Clear next byte's bits
        framebuffer[byte_index + 1] |= val;  // Set next byte's bits
    }
}


// Function to convert RGB888 to a 3-bit color (0-7)
uint8_t rgb888To3bpp(uint8_t r, uint8_t g, uint8_t b) {
    int v = 0;
    if (rand_simple() >> 24 < r) v |= 4;
    if (rand_simple() >> 24 < g) v |= 2;
    if (rand_simple() >> 24 < b) v |= 1;
    return v;

    // Approximate brightness (luminance-based mapping)
    // Formula: Y = 0.2126*R + 0.7152*G + 0.0722*B
    float luminance = 0.2126f * r + 0.7152f * g + 0.0722f * b;

    // Map luminance (0-255) to 3bpp (0-7)
    return (uint8_t)(luminance * 7 / 255);
}

// Linear interpolation function
float lerp(float a, float b, float t) {
    return a + t * (b - a);
}

// Draw a gradient-filled rectangle with dithering
int drawGradientRectangle(unsigned char *c, int x0, int y0, int x1, int y1,
                           uint32_t c00, uint32_t c01, uint32_t c10, uint32_t c11) {
    if (x0 > x1) { return -1; }
    if (y0 > y1) { return -1; }

    // Extract RGB values from 32-bit corner colors (RGB888)
    uint8_t c00_r = (c00 >> 16) & 0xFF, c00_g = (c00 >> 8) & 0xFF, c00_b = c00 & 0xFF;
    uint8_t c01_r = (c01 >> 16) & 0xFF, c01_g = (c01 >> 8) & 0xFF, c01_b = c01 & 0xFF;
    uint8_t c10_r = (c10 >> 16) & 0xFF, c10_g = (c10 >> 8) & 0xFF, c10_b = c10 & 0xFF;
    uint8_t c11_r = (c11 >> 16) & 0xFF, c11_g = (c11 >> 8) & 0xFF, c11_b = c11 & 0xFF;

    // Loop through each pixel in the rectangle
    for (int y = y0; y <= y1; y++) {
        for (int x = x0; x <= x1; x++) {
            // Compute normalized position in the rectangle
            float tx = (float)(x - x0) / (x1 - x0);
            float ty = (float)(y - y0) / (y1 - y0);

            // Interpolate RGB values
            uint8_t r = (uint8_t)lerp(lerp(c00_r, c01_r, tx), lerp(c10_r, c11_r, tx), ty);
            uint8_t g = (uint8_t)lerp(lerp(c00_g, c01_g, tx), lerp(c10_g, c11_g, tx), ty);
            uint8_t b = (uint8_t)lerp(lerp(c00_b, c01_b, tx), lerp(c10_b, c11_b, tx), ty);

            // Convert interpolated RGB888 color to 3-bpp
            uint8_t color = rgb888To3bpp(r, g, b);

            setPixel(c, x, y, color);
        }
    }
  return 0;
}

/* static uint32_t rand_range(uint32_t min, uint32_t max) {    return min + (rand_simple() % (max - min + 1)); } */

int c_fill2(unsigned char *c) {
  for(int y=0; y<176; y++) {
    for(int x=0; x<176; x++) {
      setPixel(c, x, y, 4);
    }
  }
  return 2345;
}

int c_test(unsigned char *c) {
  return drawGradientRectangle(c, 0, 0, WIDTH, HEIGHT,
     0x000000, 0xff0000, 0x00ff00, 0xffffff);
}

`);

var img = {
  width : 176, height : 176, bpp : 3,
  buffer : new Uint8Array(176*176*(3/8)).buffer
};

function j_test(c) {
  for(let i=0; i<(176*176*3)/8; ) {
    // 1 aka 0x49 -> green
    // 2 aka      -> red
    // 4          -> blue
    let v = 0x249;
    c[i++] = v & 0xff; 
    c[i++] = v >> 1 & 0xff;
    c[i++] = v >> 2 & 0xff;
  } 
}

function drawGradientRectangle(img, p1, p2, p3, p4, c1, c2, c3, c4) {
  //let r = c.drawGradientRectangle(E.getAddressOf(img.buffer, true), p1, p2, p3, p4, c1, c2, c3, c4);
  //print("Res:" , r);
}

function drawC() {
  g.reset().clear();
  if (1) {
    print("c returns ", c.c_test(E.getAddressOf(img.buffer, true)));
  } else {
    j_test(img.buffer);
  }
  g.drawImage(img, 0, 0);
}

// #####################################################################

const h = g.getHeight();
const w = g.getWidth();

function draw() {
  let t1 = getTime();
  g.reset().clear();
  g.setColor(g.theme.fg);
  g.setFontAlign(0, 0);
  g.setFont("Vector", 30);
  g.drawString('Mode ' + mode, w/2, (h/3));
  g.flip();
  
  print("Painting, mode ", mode);

  if (mode >= 1 && mode <=10)
    drawColorTable();
  else if (mode > 10)
    drawSolid(mode-10);
  else if (mode == -10)
    drawDitheringAll();
  else if (mode == -11)
    drawDithering();
  else if (mode == 0)
    drawWhite();
  else if (mode == -12)
    drawWeb();
  else if (mode == -1) {
    drawC();
    drawGradientRectangle(img, 0, 0, w, h, 0, 0, 0xffffff, 0xffffff);
  } else if (mode <= -1)
    drawC();
  else
    drawEmpty();
  print("... took ", getTime()-t1);
}

/* With backlight on, fujitsu desktop monitor, gimp color picker

                          H    S    V
 0 Black is      #403f5f  241  33%  37%
   
 1 Blue is about #4b6ec0  222  33%  61%
 2 Green is      #839d68   89  33%  61%
 3 "sage green"  #7ea581  123  23%  64%
 4 Red is about  #9d6868    0  33%  61%
 5 "muted mauve" #897593  280  20%  57%
 6 "sage gray"   #cdd5bc   79  11%  83%
   
 7 White is cca  #a3e8b0  131  30%  91%
 
   Backlight on
   
1                #15285b  224  76%  35%
2  Green                   88  33%  50%
4  Red           #823636    0  58%  51%
6                #747256   56  25%  46%
7  White         #6f7d71  128  11%  48%
*/

function hexToAdditiveRGB(hex) {
    // Helper function to convert gamma-encoded sRGB to linear RGB
    function gammaToLinear(channel) {
        const normalized = channel / 255; // Normalize to 0–1
        return normalized <= 0.04045 
            ? normalized / 12.92 
            : Math.pow((normalized + 0.055) / 1.055, 2.4);
    }
    
    // Remove '#' and parse hex components
    hex = hex.replace('#', '');
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);

    // Convert to linear RGB
    return {
        r: gammaToLinear(r),
        g: gammaToLinear(g),
        b: gammaToLinear(b)
    };
}

// Example usage
const hexColor = "#403f5f";
const additiveRGB = hexToAdditiveRGB(hexColor);
console.log("Additive RGB:", additiveRGB);

function linearToGamma(linear) {
    return linear <= 0.0031308 
        ? linear * 12.92 
        : 1.055 * Math.pow(linear, 1 / 2.4) - 0.055;
}

function additiveToHex(linearRGB) {
    const r = Math.round(linearToGamma(linearRGB.r) * 255);
    const g = Math.round(linearToGamma(linearRGB.g) * 255);
    const b = Math.round(linearToGamma(linearRGB.b) * 255);

    return `#${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1).toUpperCase()}`;
}

function getClosest(inputLinear, palette) {
    // Find the closest primary color
    let closestPrimary = null;
    let closestDistance = Infinity;

    for (const color of palette) {
        const distance = Math.sqrt(
            Math.pow(inputLinear.r - color.l.r, 2) +
            Math.pow(inputLinear.g - color.l.g, 2) +
            Math.pow(inputLinear.b - color.l.b, 2)
        );

        if (distance < closestDistance) {
            closestDistance = distance;
            closestPrimary = color;
        }
    }

  return { dist: closestDistance, color: closestPrimary };
}

function linearAdd(inputLinear, closestPrimary) {
  return {
        r: inputLinear.r + closestPrimary.r,
        g: inputLinear.g + closestPrimary.g,
        b: inputLinear.b + closestPrimary.b
    };
}

function linearDiff(inputLinear, closestPrimary) {
  return {
        r: inputLinear.r - closestPrimary.r,
        g: inputLinear.g - closestPrimary.g,
        b: inputLinear.b - closestPrimary.b
    };
}

function selectColorsForDithering(inputHex, palette) {
    const inputLinear = hexToLinearRGB(inputHex);

    let closestPrimary = getClosest(inputLinear, palette).color;

    // Calculate residual error
    const error = linearDiff(inputLinear, closestPrimary.l);

    // Find the second color to reduce the error
    let closestSecondary = null;
    let secondaryDistance = Infinity;

    for (const color of palette) {
        const distance = Math.sqrt(
            Math.pow(error.r - color.l.r, 2) +
            Math.pow(error.g - color.l.g, 2) +
            Math.pow(error.b - color.l.b, 2)
        );

        if (distance < secondaryDistance) {
            secondaryDistance = distance;
            closestSecondary = color;
        }
    }

    return {
        primary: closestPrimary.originalHex,
        secondary: closestSecondary.originalHex,
        weights: {
            primary: 0.5, // Example: 50% each for simple dithering
            secondary: 0.5
        }
    };
}

hexToLinearRGB = hexToAdditiveRGB;

// Example palette (3-bit display colors)
const palette = [
    { originalHex: "#000000", l: hexToLinearRGB("#403f5f") }, // Black
    { originalHex: "#0000ff", l: hexToLinearRGB("#4b6ec0") }, // Blue
    { originalHex: "#00ff00", l: hexToLinearRGB("#839d68") }, // Green
    { originalHex: "#00ffff", l: hexToLinearRGB("#7ea581") }, // Sage Green
    { originalHex: "#ff0000", l: hexToLinearRGB("#9d6868") }, // Red
    { originalHex: "#ff00ff", l: hexToLinearRGB("#897593") }, // Muted Mauve
    { originalHex: "#ffff00", l: hexToLinearRGB("#cdd5bc") }, // Sage Gray
    { originalHex: "#ffffff", l: hexToLinearRGB("#a3e8b0") }  // White
];

function fillColor(u1, u2, c) {
  let lin = hexToLinearRGB(c);
  let err = { r:0, g:0, b:0 };
  for (let y=u1; y<u2; y++)
    for (let x=0; x<w; x++) {
      let req = linearAdd(lin, err);
      let near = getClosest(lin, palette).color;
      g.setPixel(x,y, near.originalHex);
      err = linearDiff(req, near.l);
    }
  print("Final error:", err);
}

function fillColorBig(u1, u2, c) {
  let lin = hexToLinearRGB(c);
  let err = { r:0, g:0, b:0 };
  for (let y=u1; y<u2; y+=16)
    for (let x=0; x<w; x+=16) {
      let req = linearAdd(lin, err);
      let near = getClosest(req, palette).color;
      g.setColor(near.originalHex);
      g.fillRect(x, y, x+16, y+16);
      err = linearDiff(req, near.l);
    }
  print("Final error:", err);
}

function binColor(c) {
  let R = (c&4) / 4;
  let G = (c&2) / 2;
  let B = c&1;
  g.setColor(R, G, B);
}

function dither(x1, y1, x2, y2, i, d) {
  if (d <= 0) {
    console.error("d must be greater than 0");
    return;
  }
  print("dither");
  
  // Ensure coordinates are within canvas bounds
  x1 = Math.max(0, x1);
  y1 = Math.max(0, y1);
  x2 = Math.min(w, x2);
  y2 = Math.min(h, y2);

  for (let x_ = x1; x_ < x2; x_ += d) {
    for (let y_ = y1; y_ < y2; y_ += d) {
      let j = i;

      // Loop through each cell in the `d x d` block
      for (let dx = 0; dx < d; dx++) {
        for (let dy = 0; dy < d; dy++) {
          // Avoid drawing outside the defined area
          if (x_ + dx < x2 && y_ + dy < y2) {
            binColor(j & 7);
            g.setPixel(x_ + dx, y_ + dy);
          }
          j >>= 3; // Shift to the next 3-bit value
        }
      }
    }
  }
  
  print("done dither");
  g.flip();
}


function dither2(x, y, i) {
  let d = 2;
  print("dither");
  for (let x_ = x; x_ < w; x_+=d)
    for (let y_ = y; y_ < h; y_+=d) {
      let j = i;
      binColor(j&7); g.setPixel(x_, y_); j >>= 3;
      binColor(j&7); g.setPixel(x_+1, y_); j >>= 3;
      binColor(j&7); g.setPixel(x_, y_+1); j >>= 3;
      binColor(j&7); g.setPixel(x_+1, y_+1); j >>= 3;
    }
  print("done dither");
  g.flip();
}

function drawBlack() {
  let c = "#000000";
  
  let u = h/2;
  
  g.reset().clear();
  g.setColor(c);
  g.fillRect(0, 0, w, u);
  g.flip();

  dither2(0, h/2, 2);
  g.flip();
}

function drawWhite() {
  let s = 0; let H = 17;
  dither(0, s, w, s+H, 0, 3); s+= H;
  dither(0, s, w, s+H, 0700000000, 3); s+= H;
  dither(0, s, w, s+H, 0700700000, 3); s+= H;
  dither(0, s, w, s+H, 0700700070, 3); s+= H;
  dither(0, s, w, s+H, 0700707070, 3); s+= H;
  dither(0, s, w, s+H, 0700707070, 3); s+= H;
  dither(0, s, w, s+H, 0707707077, 3); s+= H;
  dither(0, s, w, s+H, 0707777077, 3); s+= H;
  dither(0, s, w, s+H, 0707777777, 3); s+= H;
  dither(0, s, w, s+H, 0777777777, 3); s+= H;
}

function problemdemo_drawWhite() {
  dither2(0, 0, 05005); /* Shows we can't just count # red/green/blue dots in area */
  dither2(0, h/2, 01441);
}

function dis_drawWhite() {
  let c = "#ffffff";
  
  let u = h/2;
  
  g.reset().clear();
  g.setColor(c);
  g.fillRect(0, 0, w, u);
  g.flip();

  //dither(0, h/2, 0xb76);
  dither2(0, h/2, 0x1d);
  g.flip();
}



function drawWeb() {
  //let c = "#9535ab";
  //let c = "#ffffff";
  //let c = "#c08080";
  //let c = "#d6a3e8";
  //let c = "#60d060";
  //let c = "#ae79c0";
  let c = "#8ad436";
  
  let u = h/16;
  
  g.reset().clear();
  g.setColor(c);
  g.fillRect(0, 0, w, 2*u);

  const ditherSelection = selectColorsForDithering(c, palette);
  console.log("Dither Colors:", ditherSelection);
  
  c1 = ditherSelection.primary;
  c2 = ditherSelection.secondary;
  
  g.setColor(c1);
  g.fillRect(0, 4*u, w, 6*u);
  g.setColor(c2);
  g.fillRect(0, 6*u, w, 8*u);
  g.flip();
  
  for (let y=2*u; y<4*u; y++)
    for (let x=0; x<w; x++)
     {
      if ((x+y) % 2)
        g.setPixel(x,y, c1);
      else
        g.setPixel(x,y, c2);
    }
  g.flip();
  
  fillColorBig(u*8, u*10, c);
  g.flip();
  fillColor(u*12, u*14, c);

}

function drawSolid(c) {
  g.reset();
  let R = (c&4) / 4;
  let G = (c&2) / 2;
  let B = c&1;
  g.setColor(R, G, B);
  g.fillRect(Bangle.appRect);
  g.setColor(1-R, 1-G, 1-B);
  g.setFontAlign(0, 0);
  g.setFont("Vector", 40);
  g.drawString('Color ' + c, w/2, (h/3));
  g.flip();
}
  
//takes cca 45 sec.
function drawDitheringAll() {
  for (i=0; i<1; i++)
    for (y=0; y<h; y++)
      for (x=0; x<w; x++) {
        c = Math.round(Math.random() * 65535);
        g.setPixel(x, y, c);
      }
}

function drawDithering() {
  drawEmpty();
  for (i=0; i<1; i++) {
    for (y=30; y<40; y++)
      for (x=0; x<w; x++) {
        c = toColorD(x/w, 0, 0);
        g.setPixel(x, y, c);
      }
    for (y=50; y<60; y++)
      for (x=0; x<w; x++) {
        c = toColorD(0, x/w, 0);
        g.setPixel(x, y, c);
      }
    for (y=70; y<80; y++)
      for (x=0; x<w; x++) {
        c = toColorD(0, 0, x/w);
        g.setPixel(x, y, c);
      }
    for (y=100; y<110; y++)
      for (x=0; x<w; x++) {
        c = toColorD(x/w, x/w, x/w);
        g.setPixel(x, y, c);
      }

  }
}

function drawEmpty() {
  g.reset();
  g.setColor(g.theme.bg);
  g.fillRect(Bangle.appRect);
}

function toCol(x) {
  if (x > 1)
    return (x-1)*2 + 1;
  return x;
}  /* 0->0, 1->1, 2->3, 3->5, 4->7 */

function toColor(R, G, B) {
  return (toCol(R) << 13) + (toCol(G) << 8) + (toCol(B) << 2);
}

var err = 0.0;

function toColD(x) {
  if (x < 0.01)
    return 0;
  if (x > 0.99)
    return 7;
  if (Math.random() < x + err) {
    err = err - 1 + x;
    return 7;
  } else {
    err = err + x;
    return 0; 
  }
}

function toColorD(R, G, B) {
  return (toColD(R) << 13) + (toColD(G) << 8) + (toColD(B) << 2);
}


function drawColorTable() {
  drawEmpty();
  
  s=11;
  B = mode - 1;
  for (R=0; R<5; R++) 
    for (G=0; G<5; G++) 
      for (Bx=0; Bx<2; Bx++)
        for (By=0; By<2; By++) {
    // setColor expects RGB565, and does dithering on 4 pixel groups
          //g.setColor();
          g.setColor(toCol(R) / 8.0, toCol(G) / 8.0, toCol(B) / 8.0);
          x = Bx*w/2 + R*s;
          y = By*h/2 + G*s;
          g.fillRect(x, y, x+s,y+s);
        }
  
  
  g.setColor(g.theme.fg);
  g.setFontAlign(0, 0);
  g.setFont("Vector", 30);
  g.drawString('Blue=' + B, w/2, (h/3));
}

mode = 0;

Bangle.setUI("clockupdown", btn=> {
  if (btn<0) mode = mode - 1;
  if (btn>0) mode = mode + 1;
  draw();
});

g.clear();
//Bangle.loadWidgets();
//widget_utils.hide();
setTimeout(draw, 500);



