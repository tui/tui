let ui = {
  display: 0,
  numScreens: 2,
  drawMsg: function(msg) {
    g.reset().setFont("Vector", 35)
      .setColor(1,1,1)
      .fillRect(0, this.wi, 176, 176)
      .setColor(0,0,0)
      .drawString(msg, 5, 30)
      .flip();
  },
  drawBusy: function() {
    this.drawMsg("\n.oO busy");
  },
  nextScreen: function() {
    print("nextS");
    this.display = this.display + 1;
    if (this.display == this.numScreens)
      this.display = 0;
    this.drawBusy();
  },
  prevScreen: function() {
    print("prevS");
    this.display = this.display - 1;
    if (this.display < 0)
      this.display = this.numScreens - 1;
    this.drawBusy();
  },
  onSwipe: function(dir) {
    this.nextScreen();
  },
  h: 176,
  w: 176,
  wi: 32,
  last_b: 0,
  touchHandler: function(d) {
    let x = Math.floor(d.x);
    let y = Math.floor(d.y);
    
    if (d.b != 1 || this.last_b != 0) {
      this.last_b = d.b;
      return;
    }
    
    print("touch", x, y, this.h, this.w);

    /*
      if ((x<this.h/2) && (y<this.w/2)) {
      }
      if ((x>this.h/2) && (y<this.w/2)) {
      }
    */

    if ((x<this.h/2) && (y>this.w/2)) {
      print("prev");
      this.prevScreen();
    }
    if ((x>this.h/2) && (y>this.w/2)) {
      print("next");
      this.nextScreen();
    }
  },
  init: function() {
    this.drawBusy();
  }
};

function buzz(time) {
  g.setColor(0,0,0).fillRect(0, ui.wi, 176, 176);
  Bangle.buzz(time, 1);
  setTimeout(buzzDone, time, 0);
}

function buzzDone() {
  g.reset().clear();
  paint();
}

function paint() {
  let l = 0.5;
  let rem = end - getTime();
  if (rem < 60) {
    l = rem / 60;
  }
  if (Math.random() > l)
    per -= 30;
  else
    per += 30;
  if (per < min)
    per = min;
  if (per > max)
    per = max;
  let msg = "" + per + "\n" + l + "\n" + rem + "\n";
  let t = per, b = 100;
  if (lost && getTime() > lost) {
    print("lost");
    ui.drawMsg("lost");
    t = 300;
    b = 2000;
  }
  if (!lost && (getTime() > end)) {
    lost = getTime() + 10*(2 + 6*Math.random());
  }
  if (lost) {
    print("end");
    msg = "end";
    b = 1000;
    t = 1000;
  }
  if (!lost) {
    let a = Bangle.getAccel();
    let diff = a.x - lastz;
    lastz = a.x;
    if (diff > 0.3) {
      msg = "restart\n" + a.diff;
      b = 300;
      t = 10000;
      restart();
    }
  }
  g.reset().setFont("Vector", 35)
    .setColor(1,1,1)
    .fillRect(0, ui.wi, 176, 176)
    .setColor(0,0,0)
    .drawString(msg, 5, 30);
  print(msg);
  setTimeout(buzz, t, b);
}

function restart() {
  end = getTime() + 60*(3 + 3*Math.random());
}

var min = 500, per = 700, max = 1200,
    end = 0, lost = 0, lastz = 0;

restart();
//end = getTime() + 90;

paint();



