#!/usr/bin/nodejs
// http://oldcomputer.info/8bit/benchmarks/index.htm
// 10000! -- 0.4 sec on X220
// 100!   -- 1m35 sec on banglejs2 -- 23750x slower than X220 (esp32 is only 180x slower?) -> 100x slower than esp32?
// 1!     -- 61 sec on Commodore 64 -- 155x slower than banglejs2

function oldbech() {
for (q=0; q<100; q++) {
  s=0
  for (n=0; n<100; n++) {
    a = n+1;
    for (i=0; i<10; i++)
    	a=Math.sqrt(a);
    for (i=0; i<10; i++)
    	a=a*a
      s = s+a
  }
}
  print(1010-s/5)
}

t1 = getTime();
oldbench();
print("Time: ", getTime()-t1);
