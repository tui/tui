function predictWeather(prevTimestamp, prevPressure, currTimestamp, currPressure) {
    // Calculate the time difference in hours
    const timeDiffHours = (currTimestamp - prevTimestamp) / (1000 * 60 * 60);
    
    // Calculate the pressure change rate (hPa per hour)
    const pressureChangeRate = (currPressure - prevPressure) / timeDiffHours;
    const absPressureChangeRate = Math.abs(pressureChangeRate);
    
    // Interpret the pressure change
    let weatherPrediction;
    
    if (pressureChangeRate > 0) {
        if (absPressureChangeRate > 1) {
            weatherPrediction = "Rapidly improving weather, likely clearing up significantly.";
        } else {
            weatherPrediction = "Gradually improving weather.";
        }
    } else if (pressureChangeRate < 0) {
        if (absPressureChangeRate > 1) {
            weatherPrediction = "Rapidly deteriorating weather, likely worsening significantly.";
        } else {
            weatherPrediction = "Gradually deteriorating weather.";
        }
    } else {
        weatherPrediction = "Stable weather conditions.";
    }
    
    return {
        timeDiffHours: timeDiffHours.toFixed(2),
        pressureChangeRate: pressureChangeRate.toFixed(2),
        weatherPrediction: weatherPrediction
    };
}

// Example usage:
const prevTimestamp = new Date('2023-07-03T12:00:00Z').getTime();
const prevPressure = 1012; // Previous sea level pressure in hPa
const currTimestamp = new Date('2023-07-04T12:00:00Z').getTime();
const currPressure = 1005; // Current sea level pressure in hPa

const prediction = predictWeather(prevTimestamp, prevPressure, currTimestamp, currPressure);
console.log(prediction);

