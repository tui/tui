#include <stdbool.h>

#ifdef INTERNAL
struct usb_device_descriptor usb_device;
struct usb_qualifier_descriptor usb_qualifier;
struct usb_config_descriptor usb_config;
struct usb_interface_descriptor usb_interface;
struct usb_endpoint_descriptor usb_endpoint;
char usb_hid_report[];
struct hid_descriptor usb_hid;
#endif

struct usb_gadget {
	int fd;
	int size_usb_hid_report;
};

extern void usb_key_press(struct usb_gadget *g, char scan, bool down);





