/* -*- linux-c -*- */
// Thanks to https://gist.github.com/fschr/92958222e35a823e738bb181fe045274

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>
#include "usb.h"

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

void handle_key(struct usb_gadget *g, SDL_KeyboardEvent *key, bool down)
{
	// event.key.keysym.sym
	printf("key: %x\n", key->keysym.scancode);
	usb_key_press(g, key->keysym.scancode, down);
}

void sdl_loop(struct usb_gadget *g)
{
	SDL_Event event;

	/* Poll for events. SDL_PollEvent() returns 0 when there are no  */
	/* more events on the event queue, our while loop will exit when */
	/* that occurs.                                                  */
	while (1) {
		if (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_KEYDOWN:
				printf("Key press detected\n");
				handle_key(g, &event.key, true);
				break;

			case SDL_KEYUP:
				printf( "Key release detected\n" );
				handle_key(g, &event.key, false);
				break;

			case SDL_QUIT:
				return;
			
			default:
				printf("Unknown event: %d\n", event.type);
				break;
			}
		}
	}
}

static SDL_Window *window = NULL;

int sdl_init(void)
{
	SDL_Surface *screenSurface = NULL;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
		return 1;
	}
	window = SDL_CreateWindow(
		"South Half",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH, SCREEN_HEIGHT,
		SDL_WINDOW_SHOWN
		);
	if (window == NULL) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		return 1;
	}
	screenSurface = SDL_GetWindowSurface(window);
	SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0x20, 0x20, 0x20));
	SDL_UpdateWindowSurface(window);
	return 0;
}

int sdl_done(void)
{
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
