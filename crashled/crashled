#!/usr/bin/python3

# https://hackaday.com/2019/09/19/analog-gauges-keep-an-eye-on-computer-performance/

import os
import sys
import time

class LED:
    def __init__(m, t):
        m.state = 0
        m.text = t

    def display(m):
        if m.state:
            print("%c[7m" % 0x1b, end="")
        print("["+m.text+"]", end="")
        if m.state:
            print("%c[0m" % 0x1b, end="")

class Gadget:
    def __init__(m):
    	pass

    def start(m):
        print("Starting gadget...")
        # https://events.static.linuxfound.org/sites/events/files/slides/LinuxConNA-Make-your-own-USB-gadget-Andrzej.Pietrasiewicz.pdf
        # https://openwrt.org/docs/guide-user/hardware/usb_gadget
        os.system("""
mount -t configfs none /sys/kernel/config
# load libcomposite module
modprobe libcomposite
modprobe usb_f_acm
modprobe usb_f_serial
set -e
# create a gadget
mkdir /sys/kernel/config/usb_gadget/g1
# cd to its configfs node
cd /sys/kernel/config/usb_gadget/g1
# configure it (vid/pid can be anything if USB Class is used for driver compat)
echo 0x05c6 > idVendor
echo 0x3197 > idProduct
# configure its serial/mfg/product
mkdir strings/0x409
echo s0 > strings/0x409/serialnumber
echo UCW > strings/0x409/manufacturer
echo CrashLED > strings/0x409/product
# create a config
mkdir configs/c.1
# configure it with attributes if needed
echo 120 > configs/c.1/MaxPower
# create the function (name must match a usb_f_<name> module such as 'acm')
# serial -> gser
mkdir functions/gser.0
# associate function with config
ln -s functions/gser.0 configs/c.1
# enable gadget by binding it to a UDC from /sys/class/udc
echo musb-hdrc.0.auto > UDC
""")        
        pass

    # sudo ifconfig usb0 down ?
    def stop(m):
        print("Stopping gadget...")        
        os.system("""
cd /sys/kernel/config/usb_gadget/
cd *
echo "" > UDC
rm configs/*/*
rmdir functions/*
rmdir configs/*/strings/*
rmdir configs/*
rmdir strings/*
cd ..
rmdir g1
ls -al g1
""")
        
    def set_raw(m):
        os.system("stty raw < /dev/ttyGS0")

    def run_dmesg(m):
        m.set_raw()
        os.system("cat /dev/ttyGS0")

    def display(m):
        for l in m.leds:
            l.display()
        print("\r", end="")

    def getc(m, f):
        return f.read(1)[0]        

    def handle_command(m, f, c, s):
        #print("have command ", c, s)
        if c == "c":
            m.reset()
            m.display()
            
        if c == "q":
            if s == "0":
                m.reset()
            else:
                m.leds[int(s)-1].state = 1
            m.display()
    
    def handle_control(m, f):
        #print("Have control sequence!")
        s = ""
        while True:
            c = m.getc(f)
            if chr(c) in "0123456789;":
                s = s + chr(c)
                continue
            m.handle_command(f, chr(c), s)
            break

    def reset(m):
        m.leds = [ LED("cpu0"), LED("cpu1"), LED("cpu2"), LED("cpu3"),
                   LED("swap"), LED("disk0"), LED("disk1"), LED("net"),
                   LED("crash") ]

    def run_leds(m):
        # http://ascii-table.com/ansi-escape-sequences-vt-100.php
        m.reset()
        m.display()
        m.set_raw()
        with open("/dev/ttyGS0", "r+b", buffering=0) as f:
            while True:
                sys.stdout.flush()                
                c = m.getc(f)
                if c == 0x1b:
                    #print("Have escape")
                    d = m.getc(f)
                    if d == ord(']'):
                        m.handle_control(f)
                    #else:
                    #    print("%c%c" % (c, d), end='')
                    continue
                if c == 13:
                    print()
                    m.display()
                    continue
                print("%c" % c, end='')
                print("%c[K" % 0x1b, end="")
                

g = Gadget()
#g.stop()
time.sleep(1)
#g.start()
time.sleep(1)

g.run_leds()

