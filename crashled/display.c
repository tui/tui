// -*- linux-c -*-

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

#include "/data/l/k/include/linux/compiler_attributes.h"
#include "/data/l/k/include/drm/gud.h"

#define DISPLAY
struct gud_display_descriptor_req gud_descriptor;
struct gud_display_mode_req gud_mode_req;

struct gud_set_buffer_req in_req;

#define INTERNAL
#include "usb.h"
#include "usb.c"


/* See drivers/gpu/drm/gud/gud_drv.c */

#define USB_VENDOR	0x1d50
#define USB_PRODUCT	0x614d

struct usb_device_descriptor usb_device = {
	.bLength =		USB_DT_DEVICE_SIZE,
	.bDescriptorType =	USB_DT_DEVICE,
	.bcdUSB =		__constant_cpu_to_le16(BCD_USB),
	.bDeviceClass =		0,
	.bDeviceSubClass =	0,
	.bDeviceProtocol =	0,
	.bMaxPacketSize0 =	EP_MAX_PACKET_CONTROL,
	.idVendor =		__constant_cpu_to_le16(USB_VENDOR),
	.idProduct =		__constant_cpu_to_le16(USB_PRODUCT),
	.bcdDevice =		0,
	.iManufacturer =	STRING_ID_MANUFACTURER,
	.iProduct =		STRING_ID_PRODUCT,
	.iSerialNumber =	STRING_ID_SERIAL,
	.bNumConfigurations =	1,
};

struct usb_qualifier_descriptor usb_qualifier = {
	.bLength =		sizeof(struct usb_qualifier_descriptor),
	.bDescriptorType =	USB_DT_DEVICE_QUALIFIER,
	.bcdUSB =		__constant_cpu_to_le16(BCD_USB),
	.bDeviceClass =		0,
	.bDeviceSubClass =	0,
	.bDeviceProtocol =	0,
	.bMaxPacketSize0 =	EP_MAX_PACKET_CONTROL,
	.bNumConfigurations =	1,
	.bRESERVED =		0,
};

struct usb_config_descriptor usb_config = {
	.bLength =		USB_DT_CONFIG_SIZE,
	.bDescriptorType =	USB_DT_CONFIG,
	.wTotalLength =		0,  // computed later
	.bNumInterfaces =	1,
	.bConfigurationValue =	1,
	.iConfiguration = 	STRING_ID_CONFIG,
	.bmAttributes =		USB_CONFIG_ATT_ONE | USB_CONFIG_ATT_SELFPOWER,
	.bMaxPower =		0x32,
};

struct usb_interface_descriptor usb_interface = {
	.bLength =		USB_DT_INTERFACE_SIZE,
	.bDescriptorType =	USB_DT_INTERFACE,
	.bInterfaceNumber =	0,
	.bAlternateSetting =	0,
	.bNumEndpoints =	1,
	.bInterfaceClass =	USB_CLASS_VENDOR_SPEC,
	.bInterfaceSubClass =	1,
	.bInterfaceProtocol =	1,
	.iInterface =		STRING_ID_INTERFACE,
};

/* see dummy_hcd.c: dummy_enable */

struct usb_endpoint_descriptor usb_endpoint = {
	.bLength =		USB_DT_ENDPOINT_SIZE,
	.bDescriptorType =	USB_DT_ENDPOINT,
	.bEndpointAddress =	USB_DIR_OUT,
	.bmAttributes =		USB_ENDPOINT_XFER_BULK,
	.wMaxPacketSize =	512, // EP_MAX_PACKET_BULK,
	//.bInterval =		5,
};

char usb_hid_report[] = {
};

struct hid_descriptor usb_hid = {
	.bLength =		9,
	.bDescriptorType =	HID_DT_HID,
	.bcdHID =		__constant_cpu_to_le16(0x0110),
	.bCountryCode =		0,
	.bNumDescriptors =	1,
	.desc =			{
		{
			.bDescriptorType =	HID_DT_REPORT,
			.wDescriptorLength =	sizeof(usb_hid_report),
		}
	},
};

#define TINY
#ifdef TINY
#define SX 64*2
#define SY 48
#else
#define SX 640
#define SY 480
#endif

struct gud_display_descriptor_req gud_descriptor = {
	.magic = __constant_cpu_to_le32(0x1d50614d),
	.version = 1,
	.flags = __constant_cpu_to_le32(0),
	.compression = 0,
	.max_buffer_size =  __constant_cpu_to_le32(2*1024*1024),
	/* Max buffer size of 256 crashes the kernel */
	.min_width = __constant_cpu_to_le32(SX),
	.max_width = __constant_cpu_to_le32(SX),
	.min_height = __constant_cpu_to_le32(SY),
	.max_height = __constant_cpu_to_le32(SY),
};

struct gud_display_mode_req gud_mode_req = {
  // 640 672 736 832 350 382 385 445
  .clock = 31475000,
  .hdisplay = SX,
  .hsync_start = SX+32,
  .hsync_end = SX+100-4,
  .htotal = SX+200-8,
  .vdisplay = SY,
  .vsync_start = SY+10,
  .vsync_end = SY+12,
  .vtotal = SY+45,
  .flags = 0,
};

static SDL_Window *window = NULL;
static SDL_Surface *surf, *screenSurface;

void sdl_update(void)
{
	SDL_BlitScaled(surf, NULL, screenSurface, NULL);
	SDL_UpdateWindowSurface(window);
}

void ep_bulk_out_loop(struct usb_gadget *g) {
	struct usb_raw_int_io io;
	int size = 0;
	char *pos;

	SDL_LockSurface(surf);
	pos = surf->pixels;
	while (size < in_req.length) {
		int r;

		io.inner.ep = 1;
		io.inner.flags = 0;
		io.inner.length = 512;

		r = usb_raw_ep_read(g, (struct usb_raw_ep_io *)&io);
		printf("EP read: %d %d / %d (%d)\n", r, size+r, in_req.length, sizeof(io));

		size += r;
		memset(pos, 255, r);		
		memcpy(pos, io.data, r);
#if 0
		{
			int i;
			for (i=0; i<r; i++)
				printf("%d ", io.data[i]);
		}
#endif
		pos += r;
	}
	SDL_UnlockSurface(surf);
	sdl_update();
}

int sdl_init(void)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
		return 1;
	}
	window = SDL_CreateWindow(
		"South Half",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SX, SY,
		SDL_WINDOW_SHOWN
		);
	if (window == NULL) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		return 1;
	}
	surf = SDL_CreateRGBSurfaceWithFormat(0, SX, SY, 32, SDL_PIXELFORMAT_RGBX8888);
	screenSurface = SDL_GetWindowSurface(window);
	SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xff, 0x20, 0x20));
	SDL_FillRect(surf, NULL, SDL_MapRGB(surf->format, 0x20, 0x20, 0x60));

	sdl_update();
	SDL_Delay(2000);
	return 0;
}

int sdl_done(void)
{
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}

int main(int argc, char **argv) {
	const char *device = "dummy_udc.0";
	const char *driver = "dummy_udc";
	if (argc >= 2)
		device = argv[1];
	if (argc >= 3)
		driver = argv[2];

	struct usb_gadget gadget;
	gadget.fd = usb_raw_open();
	gadget.size_usb_hid_report = sizeof(usb_hid_report);

	sdl_init();
	usb_raw_init(&gadget, USB_SPEED_HIGH, driver, device);
	usb_raw_run(&gadget);

	while (1) {
		ep0_loop(&gadget);
		ep_bulk_out_loop(&gadget);
	}

	sdl_done();
	close(gadget.fd);

	return 0;
}
