extern crate dbus;

use dbus::*;

use dbus::arg::Array;
//use dbus::preamble::*;


fn client() -> Result<(), dbus::Error> {
    let c = Connection::get_private(BusType::Session)?;
    let m = Message::new_method_call("org.freedesktop.DBus", "/", "org.freedesktop.DBus", "ListNames").unwrap();
    let r = c.send_with_reply_and_block(m, 2000)?;
    let arr: Array<&str, _>  = r.get1().unwrap();
    for name in arr { println!("{}", name); }

    Ok(())
}

//fn server() -> Result<(), dbus::Error> {
//   let c = Connection::get_private(BusType::Session)?;
//   c.register_name("com.example.dbustest", NameFlag::ReplaceExisting as u32)?;
//   let f = Factory::new_fn::<()>();
//   let tree = f.tree(()).add(f.object_path("/hello", ()).introspectable().add(
//        f.interface("com.example.dbustest", ()).add_m(
//            f.method("Hello", (), |m| {
// 	        let n: &str = m.msg.read1()?;
// 		let s = format!("Hello {}!", n);
// 		Ok(vec!(m.msg.method_return().append1(s)))
// 	     }).inarg::<&str,_>("name")
// 	       .outarg::<&str,_>("reply")
// 	       )
// 	       ));
//    tree.set_registered(&c, true)?;
//    c.add_handler(tree);
//    loop { c.incoming(1000).next(); }
//
//    Ok(())
//}

fn main() {
   client();
//   server();
}
