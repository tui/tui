#!/usr/bin/env python3

# http://www.pygtk.org/articles/writing-a-custom-widget-using-pygtk/writing-a-custom-widget-using-pygtk.htm

# needs python3-gi-cairo or something like that.

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
from gtk import gdk
import rotatable
import cairo
import time
import pango
import hardware
import math
import compass

class CairoTest(gtk.DrawingArea):
    def __init__(m):
        gtk.DrawingArea.__init__(m)
        m.connect("draw", m.on_expose_event)

    def get_background(m):
        """Serves as a caching solution."""
        return m.__bg

    def set_background(m, pattern):
        """Serves as a caching solution."""
        m.__bg = pattern

    def get_brush(m):
        """Serves as a caching solution."""
        return m.__brush

    def set_brush(m, pattern):
        """Serves as a caching solution."""
        m.__brush = pattern

    def on_expose_event(m, widget, event):
        context = m.window.cairo_create()

        # Restrict cairo to the exposed area
        #context.rectangle(*event.area)
        #context.clip()

#        m.width, m.height = m.window.get_size()
#        m.width, m.height = widget.get_size()
        if m.hw != "pinephone":
            m.width, m.height = 400, 400
        else:
            m.width, m.height = 200, 200

        m.draw(context)

    def on_configure_event(m, widget, event):
        """Override this in case you want to do something when 
           the widget's size changes."""

        return super(CairoTest, m).on_configure_event(widget, event)

    def invalidate(m):
        """Force a re-rendering of the window."""

        rect = m.get_allocation()

        # Compensate for parent offset, if any.
        parent = m.get_parent()
        if parent:
            offset = parent.get_allocation()
            rect.x -= offset.x
            rect.y -= offset.y

        m.window.invalidate_rect(rect, False)

    def draw(m, context):
        """Override this."""

        # do custom drawing here

        raise NotImplementedError()

class Sphere(CairoTest):
    def to_rad(m, deg):
        return (2*math.pi*deg)/360.

    def to_deg(m, rad):
        return (rad*360.)/(2*math.pi)
    
    def degrees_to_pixels(m, deg):
        return (deg / 90.) * (m.width / 2.1)

    def to_pos(m, incl, az):
        h = (2*math.pi*az) / 360.
        x = math.sin(h) * m.degrees_to_pixels(incl)
        y = -math.cos(h) * m.degrees_to_pixels(incl)
        z = False
        x = m.width/2 + x
        y = m.height/2 + y
        return x, y, z
        
    def draw_background(m, context):
        fgcolor=(0, 0, 0, 1)
        bgcolor=(1, 1, 1, 1)
        sx = m.width
        sy = m.height
                    
        context.set_source_rgba(*bgcolor)
        context.rectangle(0, 0, sx, sy)
        context.fill()

        context.set_source_rgba(*fgcolor)
        context.set_line_width(1)

        color = (0.8, 0.8, 0.8)
        context.set_source_rgba(*color)             
        context.move_to(0, sy/2)
        context.line_to(sx, sy/2)
        context.stroke()
        context.move_to(sx/2, 0)
        context.line_to(sx/2, sy)
        context.stroke()

        context.arc(sx/2, sy/2, m.degrees_to_pixels(30.), 0, 2*math.pi)
        context.arc(sx/2, sy/2, m.degrees_to_pixels(60.), 0, 2*math.pi)
        context.arc(sx/2, sy/2, m.degrees_to_pixels(90.), 0, 2*math.pi)
        context.stroke()

    def draw_star(m, context, x, y):
        m.star_pixmap()
        gtk.gdk.cairo_set_source_pixbuf(context, m.pixbuf, x, y)
        context.paint()

    def star_pixmap(m):
        # load the star xpm
        #help(gtk.gdk)
        #help(gtk.gdk.pixbuf_new_from_xpm_data)

        STAR_PIXMAP = ["22 22 77 1",
                "         c None",
                ".        c #626260",
                "+        c #5E5F5C",
                "@        c #636461",
                "#        c #949492",
                "$        c #62625F",
                "%        c #6E6E6B",
                "&        c #AEAEAC",
                "*        c #757673",
                "=        c #61625F",
                "-        c #9C9C9B",
                ";        c #ACACAB",
                ">        c #9F9F9E",
                ",        c #61635F",
                "'        c #656663",
                ")        c #A5A5A4",
                "!        c #ADADAB",
                "~        c #646562",
                "{        c #61615F",
                "]        c #6C6D6A",
                "^        c #797977",
                "/        c #868684",
                "(        c #A0A19E",
                "_        c #AAAAA8",
                ":        c #A3A3A2",
                "<        c #AAAAA7",
                "[        c #9F9F9F",
                "}        c #888887",
                "|        c #7E7E7C",
                "1        c #6C6C69",
                "2        c #626360",
                "3        c #A5A5A3",
                "4        c #ABABAA",
                "5        c #A9A9A7",
                "6        c #A2A2A1",
                "7        c #A3A3A1",
                "8        c #A7A7A6",
                "9        c #A8A8A6",
                "0        c #686866",
                "a        c #A4A4A2",
                "b        c #A4A4A3",
                "c        c #A1A19F",
                "d        c #9D9D9C",
                "e        c #9D9D9B",
                "f        c #A7A7A5",
                "g        c #666664",
                "h        c #A1A1A0",
                "i        c #9E9E9D",
                "j        c #646461",
                "k        c #A6A6A4",
                "l        c #A0A09F",
                "m        c #9F9F9D",
                "n        c #A9A9A8",
                "o        c #A0A09E",
                "p        c #9B9B9A",
                "q        c #ACACAA",
                "r        c #60615E",
                "s        c #ADADAC",
                "t        c #A2A2A0",
                "u        c #A8A8A7",
                "v        c #6E6F6C",
                "w        c #787976",
                "x        c #969695",
                "y        c #8B8B8A",
                "z        c #91918F",
                "A        c #71716E",
                "B        c #636360",
                "C        c #686966",
                "D        c #999997",
                "E        c #71716F",
                "F        c #61615E",
                "G        c #6C6C6A",
                "H        c #616260",
                "I        c #5F605E",
                "J        c #5D5E5B",
                "K        c #565654",
                "L        c #5F5F5D",
                "                      ",
                "                      ",
                "          .           ",
                "          +           ",
                "         @#$          ",
                "         %&*          ",
                "        =-;>,         ",
                "        ';)!'         ",
                "  ~{{]^/(_:<[}|*1@,   ",
                "   23&4_5367895&80    ",
                "    2a4b:7c>def)g     ",
                "     2c4:h>id56j      ",
                "      {k8lmeln2       ",
                "      j8bmoppqr       ",
                "      {stusnd4v       ",
                "      ws;x@yq;/       ",
                "      zfAB {CmD{      ",
                "     rE{     FGH      ",
                "     IJ       KL      ",
                "                      ",
                "                      ",
                "                      "]

        m.pixbuf = gtk.gdk.pixbuf_new_from_xpm_data(STAR_PIXMAP)

# Font example:
# https://sites.google.com/site/randomcodecollections/home/python-gtk-3-pango-cairo-example
# Pycairo docs -- great!
# http://zetcode.com/gfx/pycairo/basicdrawing/
#

class Meters(Sphere):
    def __init__(m):
        m.tick = 0
        Sphere.__init__(m)

    def draw_compass(m, context):
        co = m.compass
        h = co.get_heading()
        print("Heading ", h)

        x, y, z = m.to_pos(85, h)
        print("Got ", x, y, z)

        color = (0, 0, 0)
        context.set_source_rgba(*color)             
        
        context.arc(x, y, 5, 0, 2*math.pi)
        context.stroke()

    def raw_scale(m, v):
        return v*70

    def draw_compass_calib(m, context):
        co = m.compass

        if co.bad:
            color = (1, .3, .3)
        else:
            color = (.3, .8, .3)
        context.set_source_rgba(*color)
        context.rectangle(m.raw_scale(co.v_min[0]) + m.width/2,
                          m.raw_scale(co.v_min[1]) + m.height/2,
                          m.raw_scale(co.v_max[0] - co.v_min[0]),
                          m.raw_scale(co.v_max[1] - co.v_min[1]))
        context.fill()

        color = (0, .5, 0)
        context.set_source_rgba(*color)
        context.arc(m.raw_scale(co.v[0]) + m.width/2,
                    m.raw_scale(co.v[1]) + m.height/2, 3, 0, 2*math.pi)
        context.stroke()

        
    def draw_accel(m, context):
        if m.hw == "pinephone":
            return
        ac = hardware.hw.accelerometer
        if m.tick == 0:
            m.pos = ac.position()
        m.tick += 1
        if m.tick == 15:
            m.tick = 0
        x, y, z = m.pos

        print("Accel ", x, y, z)
        
        m.draw_star(context, m.width/2 + x*m.width/2, m.height/2 - y*m.height/2)
        return
        color = (.5, .5, .8)
        context.set_source_rgba(*color)             
        context.arc(m.width/2 + x*m.width/2, m.height/2 + y*m.height/2, 7, 0, 2*math.pi)
        context.stroke()

        az = m.to_deg(math.atan2(y, -x))
        l = math.sqrt(x*x + y*y)
        incl = m.to_deg(math.atan2(l, z))

        x, y, z = m.to_pos(incl, az)
        print("Got ", x, y, z)
        
        color = (.5, .5, .8)
        context.set_source_rgba(*color)             
        context.arc(x, y, 7, 0, 2*math.pi)
        context.stroke()
        

    def draw(m, context):
        context.push_group()
        m.draw_background(context)

        m.draw_compass(context)
        m.draw_compass_calib(context)
        m.draw_accel(context)
        
        m.set_background(context.pop_group())

        context.set_source(m.get_background())
        context.paint()

class TestWindow(rotatable.SubWindow):
    def aux_interior(m):
        table = gtk.Table(5,4,True)

        m.meters = Meters()
        m.meters.hw = m.hw
        table.attach(m.meters, 0,4, 0,5)
        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        m.meters.compass = compass.Compass()
        _, w = m.font_button(m.big('Calibrate'))
        w.connect("clicked", lambda _: m.meters.compass.reset_calib())
        table.attach(w, 1,2, 4,5)

        #m.time_label, button = m.font_button(m.big("(time)")+"\n"+m.small("date"))
        #table.attach(button, 2,3, 4,5)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())
        table.attach(w, 3,4, 4,5)

        w = gtk.Entry()
        if m.hw != "pinephone":
            w.modify_font(pango.FontDescription("sans 48"))
        table.attach(w, 0,4, 3,4)

        scrolledwindow = gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)

        textview = gtk.TextView()
        if m.hw != "pinephone":
            textview.modify_font(pango.FontDescription("sans 36"))
        m.textbuffer = textview.get_buffer()
        m.textbuffer.set_text("Place on table. Hit 'calibrate'. Rotate 360. Now o should point north.")
        textview.set_wrap_mode(gtk.WrapMode.WORD)
        scrolledwindow.add(textview)

        table.attach(scrolledwindow, 0,4, 0,3)
        
        return table

    def tick(m):
        print("Tick tock")
        m.meters.hide()
        m.meters.show()        
        gobject.timeout_add(300, lambda: m.tick())

    def show(m):
        super().show()
        m.tick()

if __name__ == "__main__":
    test = TestWindow()
    test.hw = "pinephone"
    if test.hw == "pinephone":
        test.vertical = 1
    test.basic_main_window()
    test.tick()
    gtk.main()
        

