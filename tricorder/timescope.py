#!/usr/bin/env python3
# -*- python -*-

import sys              
sys.path += [ "/usr/share/unicsy/lib" ]

import os
import hardware
import time
import math

class TimeScope:
    def __init__(m):
        m.light = hardware.hw.light_sensor
        m.danger = 0
        m.over = 0

    def scan(m):
        r = ""
        now = time.time()
        def xlog(v):
            if v < 0.1:
                return 0
            return math.log(v)
        step = 0.02
        for i in range(100):
            now += step
            s = now-time.time()
            if s < step/2:
                m.danger += 1
            if s < 0:
                m.over += 1
            else:
                time.sleep(s)
            i = m.light.get_illuminance()
            i = xlog(i)/12
            i = int(i * 30)
            if i > 10:
                i = 10
            sym = " .,_-+=*oO#"
            r = r + sym[i]
        return r + "|"

    def run(m):
        for j in range(30):
            s = m.scan()
            print(s)
        print(m.over, "overruns", m.danger, "close.")

t = TimeScope()
t.run()

