#!/usr/bin/env python3

# http://www.pygtk.org/articles/writing-a-custom-widget-using-pygtk/writing-a-custom-widget-using-pygtk.htm

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
from gtk import gdk
import rotatable
import cairo
import time
import pango
import math

class CairoTest(gtk.DrawingArea):
    def __init__(m):
        gtk.DrawingArea.__init__(m)
        m.connect("draw", m.on_expose_event)

    def get_background(m):
        """Serves as a caching solution."""
        return m.__bg

    def set_background(m, pattern):
        """Serves as a caching solution."""
        m.__bg = pattern

    def get_brush(m):
        """Serves as a caching solution."""
        return m.__brush

    def set_brush(m, pattern):
        """Serves as a caching solution."""
        m.__brush = pattern

    def on_expose_event(m, widget, event):
        context = m.window.cairo_create()

        # Restrict cairo to the exposed area
        #context.rectangle(*event.area)
        #context.clip()

#        m.width, m.height = m.window.get_size()
#        m.width, m.height = widget.get_size()
        m.width, m.height = 400, 400

        m.draw(context)

    def on_configure_event(m, widget, event):
        """Override this in case you want to do something when 
           the widget's size changes."""

        return super(CairoTest, m).on_configure_event(widget, event)

    def invalidate(m):
        """Force a re-rendering of the window."""

        rect = m.get_allocation()

        # Compensate for parent offset, if any.
        parent = m.get_parent()
        if parent:
            offset = parent.get_allocation()
            rect.x -= offset.x
            rect.y -= offset.y

        m.window.invalidate_rect(rect, False)

    def draw(m, context):
        """Override this."""

        # do custom drawing here

        raise NotImplementedError()


class Ruler(CairoTest):
    def draw_ruler(m, context):
        ox = 5
        oy = 5
        cm = 20
        r = 107.5
        context.move_to(ox, oy)
        context.line_to(ox + cm*r, oy)
        context.stroke()

        for x in range(cm):
            context.move_to(ox + x*r, oy)
            context.line_to(ox + x*r, oy + r)
            context.stroke()
            context.move_to(ox + x*r + 3, oy + r * 1.)
            context.show_text("%d" % x)
                 
        for x in range(cm):
            context.move_to(ox + x*r + r/2, oy)
            context.line_to(ox + x*r + r/2, oy + r * 0.6)
            context.stroke()
        for x in range(cm*10):
            context.move_to(ox + x*r/10, oy)
            context.line_to(ox + x*r/10, oy + r * 0.3)
            context.stroke()

    def from_polar(m, context, angle, len):
        a = (angle * math.pi) / 180
        y = m.oy - math.sin(a)*len
        x = m.ox - math.cos(a)*len
        return x, y


    def draw_angles(m, context):
        m.ox = 400
        m.oy = 300

        for a in range(19):
            context.move_to(m.ox, m.oy)
            x, y = m.from_polar(context, a*10, 200)
            context.line_to(x, y)
            context.stroke()
            
        
    def draw_background(m, context, fgcolor, bgcolor, width, height):
        context.set_source_rgba(*bgcolor)
        context.rectangle(0, 0, width, height)
        context.fill()

        context.set_source_rgba(*fgcolor)
        context.set_line_width(1)

        sx = width
        sy = height

        color = (0, 0, 0)
        context.set_source_rgba(*color)

        context.select_font_face("Purisa", cairo.FONT_SLANT_NORMAL,
                                 cairo.FONT_WEIGHT_NORMAL)
        context.set_font_size(13)
        
        m.draw_ruler(context)
        m.draw_angles(context)

    def draw(m, context):
        context.push_group()
        m.draw_background(context, fgcolor=(0, 0, 0, 1), bgcolor=(1, 1, 1, 1),
                    width=m.width,
                    height=m.height)

        m.set_background(context.pop_group())

        context.set_source(m.get_background())
        context.paint()

# Font example:
# https://sites.google.com/site/randomcodecollections/home/python-gtk-3-pango-cairo-example
# Pycairo docs -- great!
# http://zetcode.com/gfx/pycairo/basicdrawing/
#

class TestWindow(rotatable.SubWindow):
    def interior(m):
        table = gtk.Table(5,10,True)

        m.ruler = Ruler()
        table.attach(m.ruler, 0,10, 0,4)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())
        table.attach(w, 9,10, 4,5)

        return table

if __name__ == "__main__":
    test = TestWindow()
    test.basic_main_window()
    gtk.main()
        

