#!/usr/bin/python3
# It seems calibration is really useless the next day...

import sys              
sys.path += [ "/usr/share/unicsy/lib" ]

# On pinephone, to prepare:
# cd /sys/devices/platform/soc/1c2b000.i2c/i2c-1/1-001e/iio:device3
# echo 80 | sudo tee sampling_frequency


import hardware
import math
import time
import os

class Compass(hardware.Test):
    def __init__(m):
        #m.init_droid4_kbd_open()
        #m.init_nokia()
        #m.init_droid4_kbd_closed()
        m.init_pinephone()

    def init_pinephone(m):
        # older kernel?
        #m.magn = "/sys/devices/platform/soc/1c2b000.i2c/i2c-2/2-001e/iio:device1/"
        # v5.15:
        m.magn = "/sys/devices/platform/soc/1c2b000.i2c/i2c-1/1-001e/iio:device3/"
        m.v_min = [ 0.669, -1.082, -1.706 ]
        m.v_max = [ 1.102, -1.009, -1.275 ]

    def init_nokia(m):
        m.magn = "/sys/devices/platform/68000000.ocp/48060000.i2c/i2c-2/2-000f/iio:device1/"
        # 3D calibration
        #m.v_min = [ 0.367, -1.967, -2.062 ]
        #m.v_max = [ 1.147, -1.009, -1.126 ]

        # 2D calibration
        m.v_min = [ 0.669, -1.082, -1.706 ]
        m.v_max = [ 1.102, -1.009, -1.275 ]

    def init_droid4_kbd_open(m):
        m.magn = "/sys/devices/platform/44000000.ocp/48000000.interconnect/48000000.interconnect:segment@200000/48350000.target-module/48350000.i2c/i2c-3/3-000c/iio:device3/"

        # 2D calibration
        m.v_min = [ -0.214, -2.613, -3.188 ]
        m.v_max = [  0.095, -2.277, -3.111 ]

        # 3D, 5/2020
        m.v_min = [ -0.003, -0.544, -1.905 ]
        m.v_max = [  1.308,  0.306, -0.004 ]

        # 2D, 5/2020
        m.v_min = [  0.632, -0.292, -1.905 ]
        m.v_max = [  1.019,  0.102, -0.004 ]

    def init_droid4_kbd_closed(m):
        m.magn = "/sys/devices/platform/44000000.ocp/48000000.interconnect/48000000.interconnect:segment@200000/48350000.target-module/48350000.i2c/i2c-3/3-000c/iio:device3/"

        # 2D calibration, 5/2020
        m.v_min = [ -1.091, 1.019, -1.125 ]
        m.v_max = [ -0.737, 1.397, -1.055 ]

    def read_one(m, c):
        print(m.magn)
        x = int(m.read_int(m.magn + "in_magn_"+c+"_raw")) * float(m.read(m.magn + "in_magn_"+c+"_scale"))
        return x

    def read_xyz(m):
        x = m.read_one('x')
        y = m.read_one('y')
        z = m.read_one('z')
        return x, y, z

    def paint(m, v):
        s = 10

        for i in range(3):
            for x in range(2*s):
                x1 = (x-s) / s
                if v[i] >= x1 and v[i] < x1+(1./s):
                    print("%d" % i, end='')
                else:
                    print(".", end='')
            print()
        print()

        for y in range(2*s):
            y1 = (y-s) / s
            for x in range(2*s):
                x1 = (x-s) / s
                if v[0] >= x1 and v[0] < x1+(1./s):
                    print("0", end='')
                else:
                    print(".", end='')
            print()

    def get_heading(m):
        b, a, c = list(m.read_xyz())
        # Reorder here if needed?
        if False:
            # Droid 4 with lid open
            m.v = -a, b, c
        else:
            # pinephone
            m.v = a, b, c
        m.bad = m.step_calib(m.v)
        x, y, z = m.v
        v = m.v
        magn = math.sqrt(x*x + y*y + z*z)
        #print("got %.3f %.3f %.3f " % (x, y, z), " magnitude %.3f" % magn)

        # Thanks to https://github.com/mwilliams03/BerryIMU/blob/master/compass_tutorial04_graphical_output/compass_tutorial04.c

        vh = [0,0,0]

        # Apply hard iron calibration
        for i in range(3):
            vh[i] = v[i] - (m.v_min[i] + m.v_max[i]) /2 ;

        sc = [0,0,0]

        # Apply soft iron calibration
        for i in range(3):
            sc[i]  = (v[i] - m.v_min[i]) / (m.v_max[i] - m.v_min[i]) * 2 - 1;

        m.vh = vh
        m.sc = sc
            
        #If your IMU is upside down, comment out the two lines below which we correct the tilt calculation
        # accRaw[0] = -accRaw[0];
        # accRaw[1] = -accRaw[1];

        # Compute heading
        heading = 180 * math.atan2(vh[1], vh[0]) / math.pi;
        while heading < 0:
            heading += 360

        heading_unc = heading

        # Compute heading
        heading = 180 * math.atan2(sc[1], sc[0]) / math.pi;
        heading -= 270
        while heading < 0:
            heading += 360


        m.heading = heading
        m.heading_unc = heading_unc
        
        # Normalize accelerometer raw values.
        # accXnorm = accRaw[0]/sqrt(accRaw[0] * accRaw[0] + accRaw[1] * accRaw[1] + accRaw[2] * accRaw[2]);
        # accYnorm = accRaw[1]/sqrt(accRaw[0] * accRaw[0] + accRaw[1] * accRaw[1] + accRaw[2] * accRaw[2]);

        # Calculate pitch and roll
        # pitch = asin(accXnorm);
        # roll = -asin(accYnorm/cos(pitch));

        # Calculate the new tilt compensated values
        # magXcomp = magRaw[0]*cos(pitch)+magRaw[02]*sin(pitch);
        # magYcomp = magRaw[0]*sin(roll)*sin(pitch)+magRaw[1]*cos(roll)-magRaw[2]*sin(roll)*cos(pitch);

        # heading = 180*atan2(magYcomp,magXcomp)/M_PI;
        print("calib %.3f %.3f %.3f " % (sc[0], sc[1], sc[2]), "heading bad %3.0f good %3.0f" % (heading_unc, heading))

        #m.paint(sc)
        return heading

    def run(m):
        while True:
            h = m.get_heading()
            os.system("echo arrow %d foo > /tmp/delme.comm" % h)
            time.sleep(1)

    def reset_calib(m):
        m.v_min = [ 100, 100, 100]
        m.v_max = [-100,-100,-100]

    def step_calib(m, v):
        bad = False
        for i in range(3):
            if v[i] < m.v_min[i]:
                m.v_min[i] = v[i]
                bad = True
            if v[i] > m.v_max[i]:
                m.v_max[i] = v[i]
                bad = True
        return bad

    def calibrate(m):
        m.reset_calib()
        while True:
            v = m.read_xyz()
            m.step_calib(v)

            print("got ", end='')
            for i in range(3):
                print("%.3f..%.3f..%.3f   " % ( m.v_min[i], v[i], m.v_max[i] ), end='')
            print("")

if __name__ == "__main__":
    c = Compass()
    #c.run()
    time.sleep(5)
    c.calibrate()
