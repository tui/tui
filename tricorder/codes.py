#!/usr/bin/python3

class Code:
    pass

class Lines:
    def __init__(m, nl):
        m.nl = nl
        m.lines = []
        for i in range(nl):
            m.lines += [ '' ]
            
    def append(m, l):
        max = 0
        for a in l:
            if len(a) > max:
                max = len(a)

        lines = []
        for i in range(m.nl):
            pad = ''
            if i < len(l):
                pad = l[i]
            while len(pad) < max:
                pad += ' '
            lines += [ m.lines[i] + ' | ' + pad ]
        m.lines = lines

    def fmt(m):
        s = ''
        for l in m.lines:
            s += l + "\n"
        return s

class Numbers(Code):
    def init_map(m):
        m.map = []
        for a in range(26):
            c = chr(a+65)
            m.map += [ a, c ]
            m.print_map = [ a, [ c ]]

    def fmt(m):
        l = Lines(2)
        l.append( [ 'A', '1' ] )
        l.append( [ '.-' ] )
        l.append( [ 'B', '2' ] )
        l.append( [ '-...' ] )
        return l.fmt()
        

class Spelling(Code):
  spelling = [('A','alpha'), ('B','bravo'), ('C','charlie'), ('D','delta'), ('E',"echo"),
             ('F',"foxtrot"), ('G',"golf"),  ('H',"hotel"), ('I',"india"),  ('J',"juliet"),
             ('K',"kilo"),  ('L',"lima"), ('M',"mike"),   ('N',"november"),  ('O',"oskar"),
             ('P',"papa"), ('Q',"quebec"), ('R',"romeo"),  ('S',"sierra"), ('T',"tango"),
             ('U',"uniform"),  ('V',"victor"), ('W',"whiskey"),  ('X',"x-ray"),('Y',"yankee"),
             ('Z',"zulu"),   ('.',"dit"),   ('-',"dah"),   ('CH','chronos')]

class Morse(Code):
  letters = [('A',".-"),   ('B',"-..."), ('C',"-.-."), ('D',"-.."), ('E',"."),
             ('F',"..-."), ('G',"--."),  ('H',"...."), ('I',".."),  ('J',".---"),
             ('K',"-.-"),  ('L',".-.."), ('M',"--"),   ('N',"-."),  ('O',"---"),
             ('P',".--."), ('Q',"--.-"), ('R',".-."),  ('S',"..."), ('T',"-"),
             ('U',"..-"),  ('V',"...-"), ('W',".--"),  ('X',"-..-"),('Y',"-.--"),
             ('Z',"--.."), ('CH',"----")] 


  def __init__(s):
    s.init_morse()

  def init_morse(s):
    s.morse = {}
    s.morse_back = {}
    for letter, code in s.letters:
      s.morse[code] = letter
      s.morse_back[letter] = code

    s.spell = {}
    for letter, code in s.spelling:
      s.spell[letter] = code

n = Numbers()
n.init_map()
print(n.fmt())
