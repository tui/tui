/***
  This file is part of PulseAudio.
  PulseAudio is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published
  by the Free Software Foundation; either version 2.1 of the License,
  or (at your option) any later version.
  PulseAudio is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.
  You should have received a copy of the GNU Lesser General Public License
  along with PulseAudio; if not, see <http://www.gnu.org/licenses/>.
***/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>

#define BUFSIZE 1024

#include "cdma.c"

int main(int argc, char*argv[]) {
    /* The sample type to use */
    static const pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = 4000, /* Requesting 8000 crashes pulseaudio */
        .channels = 1
    };
    static const pa_buffer_attr attr = {
      .fragsize = (uint32_t) -1,
      .maxlength = (uint32_t) -1,
      .minreq = (uint32_t) -1,
      .prebuf = (uint32_t) -1,
      .tlength = (uint32_t) -1,
      /* fragsize / tlength can be 4096 -> pulseaudio CPU drops from 33% CPU to 10%, but latency can be heard */
    };
    pa_simple *r = NULL;
    pa_simple *p = NULL;
    int ret = 1;
    int error;
    const pa_buffer_attr *p_attr = &attr;
    int opt = 0; // | PA_STREAM_ADJUST_LATENCY

    /* Create a new playback stream */
    if (!(p = pa_simple_new(NULL, argv[0], PA_STREAM_PLAYBACK | opt, NULL, "playback", &ss, NULL, p_attr, &error))) {
        fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
        goto finish;
    }
    

    /* Create the recording stream */
    if (!(r = pa_simple_new(NULL, argv[0], PA_STREAM_RECORD | opt, NULL, "record", &ss, NULL, p_attr, &error))) {
        fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
        goto finish;
    }

    for (;;) {
        uint8_t buf[BUFSIZE];

	cdma(buf, BUFSIZE/2);

        /* ... and play it */
        if (pa_simple_write(p, buf, sizeof(buf), &error) < 0) {
            fprintf(stderr, __FILE__": pa_simple_write() failed: %s\n", pa_strerror(error));
            goto finish;
        }
    }

    /* Make sure that every single sample was played */
    if (pa_simple_drain(p, &error) < 0) {
        fprintf(stderr, __FILE__": pa_simple_drain() failed: %s\n", pa_strerror(error));
        goto finish;
    }
    ret = 0;

finish:
    if (r)
        pa_simple_free(r);
    if (p)
        pa_simple_free(p);
    return ret;
}


