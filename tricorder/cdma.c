typedef short sint16_t;

//#define KEY 0xdeadbeefbe41b0b5
#define KEY 0xaaaaaaaaaaaaaaaa

void dump_buf(sint16_t *buf, int num)
{
  int i;
  for (i=0; i<80; i++) {
    char c;
    if (buf[i] < 100) c = ' ';
    else if (buf[i] < 1000) c = '.';
    else if (buf[i] < 5000) c = '_';
    else if (buf[i] < 7000) c = 'o';
    else if (buf[i] < 12000) c = 'X';
    else c = '#';
    printf("%c", c);
  }
  printf(">\n");
}

void cdma(sint16_t *buf, int num)
{
  double t;
  int i;
  unsigned long long key = KEY;

  for (i=0; i<num; i++)
    buf[i] = 0;

  for (i=0; i<64; i++) {
    int j;
    int f = 3;
    
    for (j=0; j<f; j++) {
      if (i*f+j > num) {
	fprintf(stderr, "Too big\n");
      }
      buf[i*f+j] = (key & 1) * 20000;
    }
    key >>= 1;
  }
}

double correlate(sint16_t *buf1, sint16_t *buf2, int num, int offs)
{
  int i;
  double c = 0;
  for (i=0; i<num-offs; i++) {
    if ((buf1[i] > 10000) == (buf2[i+offs] > 1000))
      c += 1;
  }
  return c;
}

double cdma_decode(sint16_t *buf, int num)
{
  double m = 0;
  int i;
  sint16_t buf1[num];

  cdma(buf1, num);

  for (i=0; i<3; i++) {
    double c = correlate(buf1, buf, num, i);
    if (c > m)
      m = c;
  }
  if (m > 450) {
    //printf("Max %f ", m);
    dump_buf(buf, num);
  }

  return m;
}



