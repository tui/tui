#include <sys/types.h>
#include <unistd.h>
#include <asm/unistd.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdio.h>
#include <errno.h>

long syscall(long number, ...) {
    va_list args;
    long result;

    register long r0 asm("r0"); // Argument 1
    register long r1 asm("r1"); // Argument 2
    register long r2 asm("r2"); // Argument 3
    register long r3 asm("r3"); // Argument 4
    register long r4 asm("r4"); // Argument 5
    register long r5 asm("r5"); // Argument 6
    register long r7 asm("r8") = number; // Syscall number in r7

    va_start(args, number);

    // Load arguments into registers
    r0 = va_arg(args, long); 
    r1 = va_arg(args, long); 
    r2 = va_arg(args, long); 
    r3 = va_arg(args, long); 
    r4 = va_arg(args, long); 
    r5 = va_arg(args, long); 

    va_end(args);
 
    // Perform the syscall using inline assembly
    asm volatile(
        "push {r7, r8}; mov r7, r8; svc 0; pop {r7, r8}"                  // Trigger the system call (SVC instruction)
        : "=r"(r0)           // Output: result in the 'result' variable
        : "r"(r7), "r"(r0), "r"(r1), "r"(r2), "r"(r3), "r"(r4), "r"(r5) // Inputs
        : "memory"               // Clobber memory to ensure correct ordering
    );

    return r0;
}

long syscall3(long number, ...) {
    va_list args;
    long result;

    register long r0 asm("r0"); // Argument 1
    register long r1 asm("r1"); // Argument 2
    register long r2 asm("r2"); // Argument 3
    register long r7 asm("r3") = number; // Syscall number in r7

    va_start(args, number);

    // Load arguments into registers
    r0 = va_arg(args, long); 
    r1 = va_arg(args, long); 
    r2 = va_arg(args, long); 

    va_end(args);

    // Perform the syscall using inline assembly
    asm volatile(
        "push {r7, r8}; mov r7, r3; svc 0; pop {r7, r8}"                  // Trigger the system call (SVC instruction)
        : "=r"(r0)           // Output: result in the 'result' variable
        : "r"(r7), "r"(r0), "r"(r1), "r"(r2) // Inputs
        : "memory"               // Clobber memory to ensure correct ordering
    );

    return r0;
}

#include <stdio.h>

char buf[10240];


size_t strlen(const char *str) {
    size_t length = 0;
    while (*str != '\0') {  // Iterate until the null terminator is encountered
        length++;           // Increment the length counter
        str++;              // Move to the next character
    }
    return length;  // Return the total length of the string
}

void *memset(void *dest, int value, size_t count) {
    unsigned char *ptr = (unsigned char *)dest; // Treat memory as an array of bytes
    unsigned char val = (unsigned char)value;   // Convert the value to unsigned char

    // Fill memory with the value
    for (size_t i = 0; i < count; i++) {
        ptr[i] = val;
    }

    return dest; // Return the pointer to the memory block
}

#include <stdio.h>

typedef struct {
    int quotient;
    int remainder;
} divmod_result;

// __aeabi_idivmod: FIXME this likely needs to return values in registers
divmod_result aeabi_idivmod(int dividend, int divisor) {
    divmod_result result;
    unsigned int quotient = 0;
    unsigned int remainder = 0;

    // If the divisor is 0, division by zero error should be handled
    if (divisor == 0) {
        // Handle division by zero error (return quotient as 0, remainder as dividend)
        result.quotient = 0;
        result.remainder = remainder;
        return result;
    }

    // Ensure we work with positive values for simplicity
    int sign = 1;
    if (dividend < 0) {
        dividend = -dividend;
        sign = -sign;
    }
    if (divisor < 0) {
        divisor = -divisor;
        sign = -sign;
    }

    // Loop over bit positions, simulating long division
    for (int i = 31; i >= 0; i--) {
        // Shift the remainder left and bring in the next bit (simulate multiplication by 2)
        remainder <<= 1;
	remainder |= !! ((1 << i) & dividend);
        int bit = (remainder >= divisor);  // Compare remainder with divisor
        remainder -= bit * divisor;  // Subtract divisor if the bit was set
        quotient |= (bit << i);  // Set the bit in the quotient
    }

    // Apply the sign to the quotient
    result.quotient = sign * quotient;
    result.remainder = sign * remainder;
    
    return result;
}

int __aeabi_idiv(int dividend, int divisor) {
	divmod_result result = aeabi_idivmod(dividend, divisor);
	return result.quotient;
}

void itoa(int num, char *str, int base) {
    int i = 0;
    int isNegative = 0;

    // Handle 0 explicitly, otherwise empty string is printed
    if (num == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return;
    }

    // Handle negative numbers only if base is 10
    if (num < 0 && base == 10) {
        isNegative = 1;
        num = -num;
    }

    // Process individual digits
    while (num != 0) {
	int rem =  aeabi_idivmod(num, base).remainder;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        num = num / base;
    }

    // Append negative sign for negative numbers
    if (isNegative) {
        str[i++] = '-';
    }

    str[i] = '\0'; // Null-terminate the string

    // Reverse the string (since the digits are in reverse order)
    int start = 0;
    int end = i - 1;
    while (start < end) {
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;
        start++;
        end--;
    }
}

int puts(const char *s) {
	int l = strlen(s);
	syscall3(__NR_write, 1, s, l);
	return 0;
}

#define SI 320*480*3
char screen[SI];

int fbflip(int fb, void *vi, char c) {
	int res;
	memset(screen, c, SI);
	res = syscall(__NR_lseek, fb, 0, SEEK_SET);
	if (res)
		puts("seek error\n");
	res = syscall(__NR_write, fb, screen, SI);
	if (res)
		puts("write error\n");
	res = syscall(__NR_ioctl, fb, FBIOPAN_DISPLAY, vi);
	if (res)
		puts("ioctl error - pan\n");
	return res;
}

int fbtest(void) {
	char *name = "/dev/graphics/fb0";
//	char *name = "/dev/null";
	int fb = syscall3(__NR_open, name, O_RDWR);  // Open framebuffer device
	puts("Open:"); itoa(fb, buf, 10); puts(buf); puts("\n");
	if (fb != 3) {
		puts("Open error\n");
		return fb;
	}
	int res;
	struct fb_var_screeninfo vinfo;
	res = syscall(__NR_ioctl, fb, FBIOGET_VSCREENINFO, &vinfo);
	if (res) {
		puts("FBIOGET_VSCREENINFO "); itoa(res, buf, 10); puts(buf); puts("\n");
		return res;
	}
	puts("Resolution: "); itoa(vinfo.xres, buf, 10); puts(buf); puts("x"); itoa(vinfo.yres, buf, 10); puts(buf); puts("\n");
	vinfo.yoffset = 0;
	{
		char c;
		for (c=0; c<255; c++) {
			res = fbflip(fb, &vinfo, c);
			if (res) {
				puts("PAN "); itoa(res, buf, 10); puts(buf); puts("\n");
				return res;
			}
		}
	}

	res = syscall(__NR_ioctl, fb, FBIOBLANK, 0);
	if (res) {
		puts("FBIOBLANK "); itoa(res, buf, 10); puts(buf); puts("\n");
		return res;
	}
	return 0;
}

#include <asm/unistd.h>
#include <linux/input.h> // For struct input_event
#include <sys/syscall.h> // For syscall numbers
#include <unistd.h>      // For ssize_t

// Minimal implementation
void test_input() {
	// event2: touchscreen + home button below
	// event3: volume up/down
	// event4: profimity sensor?
	// event6: power button (short/long)
    const char *device = "/dev/input/event2"; // Adjust for your device
    const char newline = '\n';
    struct input_event ev;

    // Open the input device
    int fd = syscall(__NR_open, device, 0 /* O_RDONLY */, 0);
    if (fd < 0) {
        syscall(__NR_exit, 1); // Exit on failure
    }

    // Loop to read events
    while (1) {
        ssize_t bytes_read = syscall(__NR_read, fd, &ev, sizeof(ev));
        if (bytes_read < (ssize_t)sizeof(ev)) {
            syscall(__NR_exit, 1); // Exit if read fails or incomplete
        }

        // Print the event type, code, and value as raw numbers
        char buf[128];
	puts("Event: type "); itoa(ev.type, buf, 10); puts(buf);
	puts(" code ");  itoa(ev.code, buf, 10); puts(buf);
	puts(" value "); itoa(ev.value, buf, 10); puts(buf);
	puts("\n");
    }
}

#include <asm/unistd.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

#define SYS_openat __NR_openat
#define SYS_write  __NR_write
#define SYS_read   __NR_read
#define SYS_close  __NR_close
#define SYS_exit   __NR_exit

// System call wrappers
static inline long sys_open(const char *pathname, int flags) {
    return syscall(SYS_openat, AT_FDCWD, pathname, flags, 0);
}

static inline long sys_write(int fd, const void *buf, size_t count) {
    return syscall(SYS_write, fd, buf, count);
}

static inline long sys_read(int fd, void *buf, size_t count) {
    return syscall(SYS_read, fd, buf, count);
}

static inline long sys_close(int fd) {
    return syscall(SYS_close, fd);
}

static inline void sys_exit(int status) {
    syscall(SYS_exit, status);
}

// Main nolibc program
void test_modem() {
//    const char *device = "/dev/ttyHS0";
	    const char *device = "/dev/ttyMSM0";
//    const char *at_cmd = "AT+CSQ\r\n"; // AT command to send
    const char *at_cmd = "AT\r\nAT\r\n"; // AT command to send
    char buffer[128];
    long fd;

    // Open the device
    fd = sys_open(device, O_RDWR | O_NOCTTY); //  | O_NONBLOCK
    if (fd < 0) {
	    puts("Can't open modem\n");
        sys_exit(1); // Exit if the device cannot be opened
    }

    // Send the AT command
    if (sys_write(fd, at_cmd, 8) != 8) { // Write "AT\r\n"
	    puts("Can't write\n");
    }

    // Wait and read the response
    for (int i = 0; i < 10000000; i++); // Simple delay loop (not ideal, but simple)
    puts("Waiting for reply\n");
    ssize_t len = sys_read(fd, buffer, 1);

    if (len > 0) {
        buffer[len] = '\0'; // Null-terminate the response
        // (Optional) Do something with the response
	puts(buffer);
    }

    // Close the device
    sys_close(fd);

    puts("modem ok?\n");
    // Exit successfully
    sys_exit(0);
}


int _start() {
	static char hello[] = "Hello world\n";
	static char second[] = "Hard is hard\n";
	static char ok[] = "All ok\n";
	long res = syscall(__NR_write, 1, hello, 12);
	if (!fbtest())
		res = syscall3(__NR_write, 1, ok, 7);
	res = syscall3(__NR_write, 1, second, 13);
	puts("all ok\n");
	if (__aeabi_idiv(333, 3) != 111)
		puts("division 333 not ok\n");
	if (__aeabi_idiv(333, 1) != 333)
		puts("division 333a not ok\n");
	if (__aeabi_idiv(333, 333) != 1)
		puts("division 333 not ok\n");
	if (333 / 3 != 111)
		puts("/ 333 not ok\n");
	if (335 % 3 != 2)
		puts("% 333 not ok\n");
	
	itoa(123, buf, 10);
	puts(buf); puts("\n");

	//test_input();
	test_modem();
	res = syscall3(__NR_exit, 0);
	return 0;
}
