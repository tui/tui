#!/usr/bin/lua

-- https://howardhinnant.github.io/date_algorithms.html

local function civil_from_days(z)
    z = z + 719468
    local era = math.floor((z >= 0 and z or z - 146096) // 146097)
    local doe = z - era * 146097
    local yoe = math.floor((doe - doe//1460 + doe//36524 - doe//146096) // 365)
    local y = math.floor(yoe + era * 400)
    local doy = doe - (365*yoe + yoe//4 - yoe//100)
    local mp = math.floor((5*doy + 2)//153)
    local d = doy - (153*mp+2)//5 + 1
    local m = mp < 10 and mp+3 or mp-9
    if m <= 2 then
      y = y + 1
    end
    return y, m, d
end

days = os.time() // (24*60*60)
y, m, d = civil_from_days(days)
print("Today is " .. y .. " " .. m .. " " .. d)