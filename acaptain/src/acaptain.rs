#![allow(dead_code)]

// run fgfs using:
// fgfs --telnet=foo,bar,1000,foo,5401,bar --httpd=5501
// fgfs --generic=file,out,20,flight.out,playback

use std::f64::consts::PI;

use std::io::Read;

use std::fs::File;

use std::rc::Rc;
use std::f64;

extern crate gtk;
extern crate cairo;
extern crate glib;
extern crate geo;
extern crate dimensioned;
extern crate curl;
extern crate srtm;

use curl::easy::Easy;

use gtk::prelude::*;
use gtk::*;

use std::io::prelude::*;
use std::net::TcpStream;

use std::sync::Mutex;

use geo::Point;
use geo::algorithm::haversine_distance::HaversineDistance;
use geo::algorithm::haversine_destination::HaversineDestination;

use dimensioned::unit_systems::si::{Meter,M,FT,MeterPerSecond,MIN};
use dimensioned::Dimensionless;

mod dgeo;
use dgeo::*;

mod egt;


pub fn size() -> i32 { 400 }


struct HUD;

trait Draw {
    fn get_center(&self) -> (f64, f64) { ( 0.5, 0.5 ) }
    fn draw_grid(&self, cr : &cairo::Context, plane : &Plane);
    fn draw_plane(&self, cr : &cairo::Context, plane : &Plane);
    fn draw_point(&self, cr : &cairo::Context, plane : & Plane, p : & Point3D);

    fn draw_world(&self, cr : &cairo::Context, s : & State, plane : & Plane) {
           cr.set_source_rgb(0.2, 0.2, 0.6);
           cr.set_line_width(0.01);
    	    
           for p in &s.world {
           	   self.draw_point(cr, plane, &p);
           }
    }

    fn set_height_color(&self, cr : &cairo::Context, plane : & Plane, alt : Meter<f64>) {
           let diff = plane.p3.alt - alt;
	   let c_ = *(diff / (1000. * M)).value();
	   let mut c = 1.-c_;
	   if c < 0.1 { c = 0.1; }
	   if c > 1.0 { c = 1.0; }
	   if diff < 30. * M {
	              cr.set_source_rgb(c, 0.0, 0.0);
     } else if diff < 300. * M {
	              cr.set_source_rgb(c, c, 0.0);
           } else {
	              cr.set_source_rgb(c, c, c);
            }
	   //println!("Altitude color : {} {}", diff, c);
    }
    fn draw_surface(&self, _ : &cairo::Context, _ : &mut State) {}
    
    fn draw(&self, cr : &cairo::Context, state : &Mutex<State> ) {
            cr.set_dash(&[3., 2., 1.], 1.);
            assert_eq!(cr.get_dash(), (vec![3., 2., 1.], 1.));
    	cr.set_line_cap(cairo::LineCap::Round);
    
            cr.scale(size() as f64, size() as f64);
    
            cr.set_source_rgb(0.0/255.0, 0.0/255.0, 0.0/255.0);
    	cr.paint();
    
    	let mut s = state.lock().unwrap();

	self.draw_surface(cr, &mut s);
        self.draw_grid(cr, &s.plane);
    	self.draw_plane(cr, &s.plane);
    	self.draw_world(cr, &s, &s.plane);
    }

    fn speed_to_y(&self, speed : MeterPerSecond<f64>) -> f64{
          let yb_ = speed / kmh();
	  let yb = *yb_.value();
	  let yb0 = 200.;
	  let yb1 = 700.;

	  let y_ = (yb-yb0) / (yb1-yb0);

	  1.0 - y_
    }

    fn draw_speed(&self, cr : &cairo::Context, plane : &Plane) {
	  let x = 0.05;
	  let y = self.speed_to_y(plane.ias);

          cr.set_source_rgb(0.2, 0.6, 0.2);
          cr.set_line_width(0.01);

	  cr.move_to(x, y);
	  cr.line_to(x+0.03, y+0.01);
	  cr.line_to(x+0.03, y-0.01);
	  cr.line_to(x, y);
	  cr.stroke();

	  cr.set_source_rgb(0.8, 0.8, 0.8);
	  let y = self.speed_to_y(250. * kmh());
	  cr.move_to(x, y);

	  let y = self.speed_to_y(300. * kmh());
	  cr.line_to(x, y);
	  cr.stroke();

	  cr.move_to(x, y);
	  cr.set_source_rgb(0.5, 0.5, 0.5);
	  let y = self.speed_to_y(350. * kmh());
	  cr.line_to(x, y);
	  cr.stroke();

	  cr.move_to(x, y);
	  cr.set_source_rgb(0.3, 0.3, 0.3);
	  let y = self.speed_to_y(450. * kmh());
	  cr.line_to(x, y);
	  cr.stroke();
    }
}

impl HUD {
    fn angle_to_x(&self, _plane : &Plane, mut angle : f64) -> f64 {
       while angle < 0. { angle += 360. }
       while angle > 360. { angle -= 360. }
       if angle > 180. { angle -= 360. }
    
       return 0.5 + angle / 48.
    }
    
    fn angle_to_y(&self, _plane : &Plane, angle : f64) -> f64 {
    //         let a = 0.5 - angle / 24.;
    //	 let a = f64::sin(to_rad(angle)).to_degrees();
    
         return 0.5 - angle / 48.
    }
}

impl Draw for HUD {
    fn draw_grid(&self, cr : &cairo::Context, plane : &Plane) {
            let mut i = -90f64;
    
            cr.set_source_rgb(0.0, 0.8, 0.0);
    	while i <= 90f64 {
    	      if i == 0. || i == 30. || i == -30. {
    	      	      cr.set_line_width(0.005);
    	      } else {
    	      	      cr.set_line_width(0.001);
    	      }
    
    	      let y = self.angle_to_y(plane, i as f64);
    	      cr.move_to(0., y);
    	      cr.line_to(1., y);
    	      cr.stroke();
    	      i += 10f64;
    	}

	let mut i = 0.;
	if plane.agl < 500. * FT { i = -8.; }
	if plane.agl < 100. * FT { i = -4.; }
	if plane.agl < 500. * FT { i = *(-plane.agl / (7. * FT)).value(); }

	if i < 0. {
		cr.set_source_rgb(0.8, 0.3, 0.3);
    		cr.set_line_width(0.01);

    	      let y = self.angle_to_y(plane, i as f64);
    	      cr.move_to(0., y);
    	      cr.line_to(1., y);
    	      cr.stroke();
	}
    }

    fn draw_plane(&self, cr : &cairo::Context, plane : &Plane) {
          let up = plane.pitch;
          let right = plane.roll;
          let xb = 0.5;
          let yb = self.angle_to_y(plane, up);
    
          cr.set_line_width(0.005);
          cr.set_source_rgb(0.8, 0.8, 0.8);
          let (x, y) = to_polar(270.+right, 0.2); cr.move_to(x+xb, y+yb);
          let (x, y) = to_polar(90.+right, 0.2);  cr.line_to(x+xb, y+yb);
          cr.stroke();
    
          cr.move_to(xb, yb);
          let (x, y) = to_polar(0.+right, 0.05);  cr.line_to(x+xb, y+yb);
          cr.stroke();
    
          let xb = self.angle_to_x(plane, plane.sideslip);
          let yb = self.angle_to_y(plane, plane.gs);
          cr.arc(xb, yb, 0.03, 0., 2.*PI);
          cr.stroke();
	  self.draw_speed(cr, plane);
    }
    
    fn draw_point(&self, cr : &cairo::Context, plane : & Plane, p : & Point3D) {
           let angle = get_bearing(&plane.p3.point, &p.point) - plane.heading;
           let x = self.angle_to_x(plane, angle);
           let dist = plane.p3.point.haversine_distance(&p.point) * M; 
           let dalt = plane.p3.alt - p.alt;
           let d = dist / dalt;
           let a2 = f64::atan(*d.value()).to_degrees() - 90.;
           let y = self.angle_to_y(plane, a2);
    
           //println!("Angle {:?}, x {:?}, dist / dalt {:?}, a2 {:?}", angle, x, *d.value(), a2);
    
           cr.move_to(x, y);
           cr.line_to(x, y);
           cr.stroke();
    }
}


struct State {
       comms : FileComms,
       world : Vec<Point3D>,
       plane : Plane,
       surface : Surface,
}

fn new_point(lat : f64, lon : f64, alt : Meter<f64>) -> Point3D {
      Point3D { point: Point::new(lon, lat), alt : alt }
}

fn new_line(a : &Point3D, brg : f64, len : f64) -> Vec<Point3D>
{
	let mut res = vec![];

	let mut i = 0.;
	while i <= 1. {
	      let p = a.point.haversine_destination(brg, i * len);
	      println!("Have point {:?}", p);	      
	      res.push( Point3D { point : p, alt : a.alt } );
	      i += 0.1;
	}

	res
}

fn new_runway(a : Point3D, b : Point3D, width : Meter<f64>, name :&str) -> Vec<Point3D>
{
	let len = a.point.haversine_distance(&b.point);
	let brg = get_bearing(&a.point, &b.point);
	println!("Have runway {}, {}, direction {}", name, len, brg);

	let mut res = vec![];

	let width2 = *(width / (2. * M)).value();
	let left = Point3D { point : a.point.haversine_destination(brg+90., width2), alt : a.alt };
	let right = Point3D { point : a.point.haversine_destination(brg-90., width2), alt : a.alt };
	res.append(&mut new_line(&a, brg, len));
	res.append(&mut new_line(&left, brg, len));
	res.append(&mut new_line(&right, brg, len));

	res
}

fn new_world_lugano() -> Vec<Point3D> {
       let a = 915.*FT;
       new_runway( new_point( 46.008171, 8.912616, a ),
       		   new_point( 45.999466, 8.908367, a ), 10. * M, "Lugano" )
}

fn new_world_zurich() -> Vec<Point3D> {
       let a = 1416.*FT;
       new_runway( new_point( 47.445503, 8.556691, a ),
        	   new_point( 47.475594, 8.535950, a ), 30. * M, "Zurich" )

}

fn new_world_emmen() -> Vec<Point3D> {
       let a = 1398.*FT;
       new_runway( new_point( 47.102863, 8.316994, a ),
        	   new_point( 47.081665, 8.293047, a ), 20. * M, "Emmen" )
}

fn new_world_hardcoded() -> Vec<Point3D> {
	let mut res = vec![];

	res.append(&mut new_world_lugano() );
	res.append(&mut new_world_zurich() );
	res.append(&mut new_world_emmen() );

	res
}       

fn new_world() -> Vec<Point3D> {
	let mut res = vec![];

	let rwy = egt::load_runways("airports.egt");
	for r in rwy {
	       let a = 1398.*FT;

	       res.append(&mut
		          new_runway( new_point( r.point.lat(), r.point.lng(), a ),
        	                      new_point( r.other.lat(), r.other.lng(), a ), 20. * M, &r.name ));
	}

	res
}       

fn new_state() -> State{
     let mut comms = connect_file();
     let plane = comms.get_plane().unwrap();
       State { comms : comms,
       	       world : new_world(),
	       plane : plane,
	       surface : Surface::new(), }
}

struct Plane {
       p3 : Point3D,
       heading : f64,
       pitch : f64,
       roll : f64,
       gs : f64,
       ias : MeterPerSecond<f64>,
       mach : f64,
       sideslip : f64,
       agl : Meter<f64>,
}

struct VSI;

impl VSI {
fn altitude_scale(&self, plane : &Plane) -> f64 {
   if plane.p3.alt < 500. * FT { return 100.; }
   if plane.p3.alt < 2000. * FT { return 20.; }
   if plane.p3.alt < 4000. * FT { return 10.; }
   if plane.p3.alt < 10000. * FT { return 4.; }
   return 1.0;
}

    fn altitude_to_y(&self, plane : &Plane, alt : Meter<f64>) -> f64
    {
    	1. - (alt / (40000. * FT)).value() * self.altitude_scale(plane)
    }
    
    fn distance_to_x(&self, plane : &Plane, dist : Meter<f64>) -> f64
    {
    	0.1 + (dist / (230. * 1000. * M)).value() * self.altitude_scale(plane)
    }
}

impl Draw for VSI {
    fn draw_plane(&self, cr : &cairo::Context, plane : &Plane) {
          let xb = self.distance_to_x(plane, 0. * M);
          let yb = self.altitude_to_y(plane, plane.p3.alt);
    
          cr.set_line_width(0.005);
          cr.set_source_rgb(0.8, 0.8, 0.8);
          cr.move_to(xb, yb);
          let (x, y) = to_polar(90.-plane.pitch, 0.2);  cr.line_to(x+xb, y+yb);
          cr.stroke();
    
          cr.move_to(xb, yb);
          let (x, y) = to_polar(0.-plane.pitch, 0.05);  cr.line_to(x+xb, y+yb);
          cr.stroke();
    
          cr.move_to(xb, yb);
          let dist = plane.ias * 1. * MIN;
          let x = self.distance_to_x(plane, dist);
          let y = self.altitude_to_y(plane, plane.p3.alt + f64::sin(plane.gs.to_radians()) * dist );
    
          cr.move_to(xb, yb);
          cr.line_to(x, y);
          cr.stroke();
    }
    
    fn draw_grid(&self, cr : &cairo::Context, plane : &Plane) {
            let mut i = 0.;
    
            cr.set_source_rgb(0.0, 0.8, 0.0);
    	while i <= 40000. {
    	      if i == 0. || i == 30. || i == -30. {
    	      	      cr.set_line_width(0.005);
    	      } else {
    	      	      cr.set_line_width(0.001);
    	      }
    	
    	      cr.move_to(0., self.altitude_to_y(plane, i*FT));
    	      cr.line_to(1., self.altitude_to_y(plane, i*FT));
    	      cr.stroke();
    	      i += 1000.;
    	}

	cr.set_source_rgb(0.8, 0.3, 0.3);
    	cr.set_line_width(0.01);
    
	{
	      let ground = plane.p3.alt - plane.agl;
    	      cr.move_to(0., self.altitude_to_y(plane, ground));
    	      cr.line_to(1., self.altitude_to_y(plane, ground));
    	      cr.stroke();
    	}

        cr.set_source_rgb(0.3, 1.0, 0.3);
    	cr.set_line_width(0.01);
    
    	let mut i = 0.;
    	while i <= 200. {
    	      let x = self.distance_to_x(plane, i * 1000. * M);
    	      let down = f64::tan( (3.0f64).to_radians() ) * i * 1000. * M;
    	      let y = self.altitude_to_y(plane, plane.p3.alt - down);
    
    	      //println!("Point at {} {}", x, y);
    	      cr.move_to(x, y);
    	      cr.line_to(x, y);
    	      cr.stroke();
    
    	      if i <= 10. { i+=1. } else { i += 10. }
    		     
    	}
    }
    
    fn draw_point(&self, cr : &cairo::Context, plane : & Plane, p : & Point3D) {
           let angle = normalize(get_bearing(&plane.p3.point, &p.point) - plane.heading);
    
           if angle > 30. && angle < 330. { return; }
    
           let dist = plane.p3.point.haversine_distance(&p.point) * M;
           let x = self.distance_to_x(plane, dist);
           let alt = p.alt;
           let y = self.altitude_to_y(plane, alt);
    
           println!("Angle {:?}", angle);
    
           cr.move_to(x, y);
           cr.line_to(x, y);
           cr.stroke();
    }

    fn draw_surface(&self, cr : &cairo::Context, s : &mut State) {
           let mut d = 0. * M;
	   let plane = &s.plane;

	   cr.set_line_width(0.01);
	   //cr.move_to(0., 1.);
	   while d < 30000. * M {
	   	let p = plane.p3.point.haversine_destination(plane.heading, * (d / (1. * M)).value() );
		let alt = s.surface.get_height(&p);

		self.set_height_color(cr, plane, alt);
		let x = self.distance_to_x(plane, d);
		let y = self.altitude_to_y(plane, alt);

 		cr.move_to(x, y);
		cr.line_to(x, y);
   		cr.stroke();		
		d += 1000. * M;
	   }


    }
}

struct HSI;

impl HSI {
    fn distance_to(&self, dist :Meter<f64>) -> f64 {
          * (dist / (60000. * M)).value()
    }
    
    fn pos_to_xy(&self, plane : &Plane, p : &Point3D) -> (f64, f64)
    {
           let dist = plane.p3.point.haversine_distance(&p.point) * M; 
           let angle = get_bearing(&plane.p3.point, &p.point);
	   let (x, y) = to_polar(angle, self.distance_to(dist));
	   let (cx, cy) = self.get_center();

	   (x+cx, y+cy)
    }
}

impl Draw for HSI {
    fn draw_plane(&self, cr : &cairo::Context, plane : &Plane) {
          let (xb, yb) = self.get_center();
	  let heading = plane.heading;
	  let size = 0.05;

	  cr.set_line_width(0.005);
          cr.set_source_rgb(0.8, 0.8, 0.8);
          let (x, y) = to_polar( 70.+heading, size); cr.move_to(x+xb, y+yb);
          let (x, y) = to_polar(-70.+heading, size); cr.line_to(x+xb, y+yb);
          cr.stroke();
    
          let (x, y) = to_polar(  0.+heading, size);     cr.move_to(x+xb, y+yb);
          let (x, y) = to_polar(180.+heading, 0.9*size); cr.line_to(x+xb, y+yb);
          cr.stroke();

          let (x, y) = to_polar(160.+heading, size);     cr.move_to(x+xb, y+yb);
          let (x, y) = to_polar(200.+heading, size);     cr.line_to(x+xb, y+yb);
          cr.stroke();
}
    
    fn draw_grid(&self, cr : &cairo::Context, _plane : &Plane) {
            cr.set_source_rgb(0.0, 0.8, 0.0);
 	    cr.set_line_width(0.001);

	    let (xb, yb) = self.get_center();
	    cr.arc(xb, yb, 0.2, 0., 2.*PI);
	    cr.stroke();
    }
    
    fn draw_point(&self, cr : &cairo::Context, plane : & Plane, p : & Point3D) {
           let (x, y) = self.pos_to_xy(plane, p);
    
           cr.move_to(x, y);
           cr.line_to(x, y);
           cr.stroke();
    }

    fn draw_surface(&self, cr : &cairo::Context, state : &mut State) {
           let ax = 0.3;
	   let ay = 0.15;
	   let s = 20.;
	   let sx = ax / s;
	   let sy = ay / s;
	   let plane = &state.plane;
    	   let x_ = plane.p3.point.x();
           let y_ = plane.p3.point.y();
	   
           let mut y = y_ - ay;

          cr.set_line_width(0.01);
	   
	   while y <= y_ + ay {
	       	   let mut x = x_ - ax;
		   while x <= x_ + ax {
		   	 let alt = state.surface.get_height(&Point::new(x, y));
			 
		   	 self.set_height_color(cr, plane, alt);
			 let p3 = new_point(y, x, 0. * M);
			 let (px, py) = self.pos_to_xy(plane, &p3);
			 cr.move_to(px, py);
			 cr.line_to(px, py);
			 cr.stroke();
			 
		   	 x += sx;
		   }
		   y += sy;
	   }
    }

}

fn test_gtk() {
    gtk::init().unwrap();
    // Create the main window.
    let window = Window::new(WindowType::Toplevel);
    let grid = Grid::new();
    //let vbox = Box::new(gtk::Orientation::Vertical, 2);
    let close = Button::new_with_label("Close");
  
    let hud = DrawingArea::new();
    hud.set_size_request(size(), size());

    let vsi = DrawingArea::new();
    vsi.set_size_request(size(), size());

    let hsi = DrawingArea::new();
    hsi.set_size_request(size(), size());

    close.connect_clicked(|_| { println!("button") });

    let state : Rc<Mutex<State>> = Rc::new(Mutex::new(new_state()));
    {
	let state_ = Rc::clone(&state);
	let dhud = HUD;
        hud.connect_draw(move |_, cr| { dhud.draw(cr, &state_); Inhibit(false) });
    }
    {
	let state_ = Rc::clone(&state);
	let dvsi = VSI;
        vsi.connect_draw(move |_, cr| { dvsi.draw(cr, &state_); Inhibit(false) });
    }
    {
	let state_ = Rc::clone(&state);
	let dhsi = HSI;
        hsi.connect_draw(move |_, cr| { dhsi.draw(cr, &state_); Inhibit(false) });
    }

    grid.add(&close);
    grid.add(&vsi);
    grid.add(&hud);
    grid.add(&hsi);
    window.add(&grid);

    // UI initialization.
    // ...
    // Don't forget to make all widgets visible.
    window.show_all();

    // Handle closing of the window.
    window.connect_delete_event(|_, _| {
         // Stop the main loop.
         gtk::main_quit();
	 // Let the default handler destroy the window.
	 Inhibit(false)
    });

    main_iteration();

    //label.set_text("ready...");
    window.show_all();

    // glib::source::timeout_add(5000, move || { drawing.queue_draw(); on_timer(); Continue(true) });

    loop {
    	  {
		let mut s = state.lock().unwrap();

		if let Ok(plane) = s.comms.get_plane() {
		          println!("Got data");
		          s.plane = plane;
			  hud.queue_draw();
			  vsi.queue_draw();
			  hsi.queue_draw();
		} else {
		          println!("No valid data");
			  std::thread::sleep(std::time::Duration::from_millis(10));
		}
     	  }

    	  main_iteration_do(false);
    }

    // Run the main loop.
    //gtk::main();
}

struct TelnetComms {
       stream : TcpStream,
}

struct HttpComms {
}

struct FileComms {
       f : File,
}

fn connect_telnet() -> TelnetComms {
      TelnetComms { stream : TcpStream::connect("127.0.0.1:5401").unwrap() }
}

fn connect_http() -> HttpComms {
      HttpComms {}
}

fn connect_file() -> FileComms {
      FileComms { f : File::open("/data/pavel/g/tui/fgame/flight.out").unwrap() }
}

fn assert_read( stream : &mut TcpStream, s : &[u8]) {
       let mut r = [0; 1];

       for i in 0..s.len() {
	     let _num = stream.read(&mut r);

	     if r[0] != s[i] {
	     	println!("wrong property returned");
	     }
      }
}

fn string_read( stream : &mut TcpStream ) -> String {
       let mut r = [0; 1];
       let mut res = [0; 256];
       let mut i = 0;

       while r[0] != 39 {
	     let _num = stream.read(&mut r);

	     if r[0] != 39 {
	          res[i] = r[0];
	     	  i += 1;
	     }
       }

       let o = std::str::from_utf8(&res[0..i]).unwrap();

       String::from(o)
}

trait Comms {
      fn get_prop_any(&mut self, _s : &[u8], _typ : &[u8]) -> String { String::from("") }
      fn get_prop_double(&mut self, s : &[u8]) -> f64 {
      	 let str = self.get_prop_any(s, b"double");

	 str.parse::<f64>().unwrap()
      }
      fn get_plane(&mut self) -> Result<Plane, &str> {
	let heading = self.get_prop_double(b"orientation/heading-deg");
	let pitch = self.get_prop_double(b"orientation/pitch-deg");
	let roll = self.get_prop_double(b"orientation/roll-deg");

	let lat = self.get_prop_double(b"position/latitude-deg");
	let lon = self.get_prop_double(b"position/longitude-deg");
	let alt = self.get_prop_double(b"position/altitude-ft");	

	let p3 = Point3D { point: Point::new(lon, lat), alt : alt * FT };

	/* FIXME */
	Ok(Plane { p3 : p3, heading : heading, pitch : pitch, roll : roll, gs : 0., ias : 0. * kt(), mach : 0., sideslip : 0., agl : 0. * M })
      }
}

impl Comms for TelnetComms {

// get orientation/heading-deg
// orientation/heading-deg = '297.7607259' (double)

      // &prop : [u8]
      fn get_prop_any(&mut self, s : &[u8], typ : &[u8]) -> String {
	 let _ = self.stream.write(b"get ");
	 let _ = self.stream.write(s);
	 let _ = self.stream.write(b"\r\n");

	 assert_read(&mut self.stream, s);
	 assert_read(&mut self.stream, b" = '");
	 let res = string_read(&mut self.stream);
	 assert_read(&mut self.stream, b" (");
	 assert_read(&mut self.stream, typ);
	 assert_read(&mut self.stream, b")\r\n/> ");
	 res
      }
}

impl Comms for HttpComms {
      fn get_prop_any(&mut self, _s : &[u8], _typ : &[u8]) -> String {
            let mut handle = Easy::new();
      	    handle.url("http://localhost:5501/orientation/heading-deg").unwrap();

	    let mut data = Vec::new();
            let mut transfer = handle.transfer();
            let _ = transfer
                .write_function(|new_data| {
	           data.extend_from_slice(new_data);
                   Ok(new_data.len())
               });
 	    transfer.perform().unwrap();

	    //let sdata = String::from(data);
	    //let lines = sdata.split("\n");
	    //let line = lines[5];

	    String::from("0.")
      }
}

fn prop_num(c : &Vec<&str>, num : usize) -> f64 {
   let s = c[num];
   
   s.parse::<f64>().unwrap()
}

impl Comms for FileComms {
      fn get_plane(&mut self) -> Result<Plane, &str> {
        let mut buf = String::new();
	if 0 == self.f.read_to_string(&mut buf).unwrap() {
	   return Err( "no data" );
	}
	
	let split = buf.split("\n");
	let c : Vec<&str> = split.collect();
	
	//println!("c {:?}", c.len());

	let line = c[c.len()-2];
	
	//println!("l1: {:?}", line);
	let split = line.split(",");
	let vals : Vec<&str> = split.collect();

	//println!("l2: {:?}", vals);

      	 /* FIXME: Better use /instrumentation/attitude-indicator and
	                      /instrumentation/gps and
			      instrumentation/heading-indicator
			      instrumentation/radar-altimeter ? */

	let heading = prop_num(&vals, 5);
	let pitch = prop_num(&vals, 4);
	let roll = prop_num(&vals, 3);

	let lat = prop_num(&vals, 0);
	let lon = prop_num(&vals, 1);
	let alt = prop_num(&vals, 2);

	let agl = prop_num(&vals, 6);
	let gs = prop_num(&vals, 7);
	let ias = prop_num(&vals, 8);
	let mach = prop_num(&vals, 9);

	let sideslip = prop_num(&vals, 10);

	let p3 = Point3D { point: Point::new(lon, lat), alt : alt * FT };
	
	Ok(Plane { p3 : p3, heading : heading, pitch : pitch, roll : roll,
		   gs : f64::atan(gs).to_degrees(), ias : ias * kt(), mach : mach,
		   sideslip : sideslip, agl : agl * FT })
      }
}

fn test_comms() {
   let mut c = connect_http();

   for _i in 0..100 {
      let res = c.get_prop_double(b"orientation/heading-deg");
      println!("{:?}", res);
   }
}


fn test_geo() {
   let p1 = Point::new(14., 50.);
   let p2 = Point::new(14.00001, 50.);
   let d = p1.haversine_distance(&p2); /* Distance in meters */

   println!("dist = {:?}", d / 60.);
   println!("bearing = {:?}", get_bearing(&p1, &p2));

   let height : Meter<f64> = 1. * FT;
   println!("1ft = {:?}", height);
}

fn test_file() {
   let mut f = File::open("/data/pavel/g/tui/fgame/flight.out").unwrap();

   let mut buf = String::new();

   loop {
   	let l = f.read_to_string(&mut buf).unwrap();
	println!("Got {} {}", l, buf.len());
	if l == 0 {
	   std::thread::sleep(std::time::Duration::from_millis(1000));
	   continue;
        }
	let split = buf.split("\n");
	let c : Vec<&str> = split.collect();
	
	println!("c {:?}", c.len());

	let line = c[c.len()-3];
	
	println!("l1: {:?}", line);
	let split = line.split(",");
	let vals : Vec<&str> = split.collect();

	println!("l2: {:?}", vals);
   }
}

struct Surface {
       map : std::collections::HashMap<(i32, i32), srtm::Tile>,
}

impl Surface {
       fn new() -> Surface { Surface{ map : std::collections::HashMap::new() }  }

       fn __get_height(&mut self, mut lat : f64, mut lon : f64) -> i16 {
          if (lat < 0.) || (lon < 0.) {
	     println!("No support for South/West of 0");
	     return 0;
	  }

	  let name = format!("/data/pavel/.srtm/N{:02}E{:03}.hgt", lat.trunc() as i32, lon.trunc() as i32);
	  //	  let tile = srtm::Tile::from_file(name.clone()).unwrap();
	  let key = ( lat.trunc() as i32, lon.trunc() as i32 );

	  if ! self.map.contains_key(&key) {
	     println!("No key");
	     self.map.insert(key, srtm::Tile::from_file(name.clone()).unwrap());
	  }

	  let mut tile = &self.map[&key];

	  let res = tile.resolution;

	  //println!("{}, high resolution {:?}", name, res == srtm::Resolution::SRTM1);

	  lat -= lat.trunc();
	  lon -= lon.trunc();

	  lat = 1.0 - lat;

	  assert!(res == srtm::Resolution::SRTM3);

	  if res == srtm::Resolution::SRTM3 {
	     lat *= 1200.;
	     lon *= 1200.;
	  }

	  lat = lat.round();
	  lon = lon.round();

	  let height = tile.get(lon as u32, lat as u32);

	  height
       }

   fn get_height(&mut self, point : & Point<f64>) -> Meter<f64> {
       let lat = point.y();
       let lon = point.x();

       let alt = self.__get_height(lat, lon) as f64;

       alt * M
  }

}

fn test_srtm() {
   let mut s = Surface::new();

   // Kopec u zernovky, mel by mit cca 450m */
   println!("{:?}", s.__get_height(50.007766, 14.751454));
}

fn main() {
    test_gtk();
//    test_comms();
//      test_file();
//      test_geo();
//	test_srtm();
}

// list.json z ~/g/srtm.py/
//   wget ...
//  for a in ../*.zip ; do unzip $a; done
