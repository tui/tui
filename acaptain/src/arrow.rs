#![allow(dead_code)]

use std::io::Read;

use std::fs::File;
//use std::sync::mpsc;
//use std::thread;
//use std::time::Duration;
//use std::io::Write;

use std::f64;
use std::net::TcpStream;
use std::sync::Arc;

extern crate gtk;
extern crate cairo;
extern crate glib;

extern crate dimensioned;
extern crate geo;

use dimensioned::unit_systems::si::M;

use std::sync::Mutex;

use gtk::prelude::*;
use gtk::*;

use geo::Point;

mod gpsc;
mod dgeo;
mod rotatable;
mod egt;

// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

struct ArrowState {
       dir : f64,
       stream : TcpStream,
       target : Point<f64>,
}

use dgeo::{to_polar};

fn to_polar_o(a : f64, b : f64) -> (f64, f64) {
   let (x, y) = to_polar(a, b);
   (x+0.5, y+0.5)
}

// https://www.cairographics.org/tutorial/
fn on_draw_arrow(cr : &cairo::Context, state : &mut ArrowState) {
	let dir = state.dir;
	
        cr.scale(200f64, 200f64);
	cr.translate(1., 1.);

        cr.set_source_rgb(250.0/255.0, 224.0/255.0, 55.0/255.0);
	cr.paint();

        cr.set_source_rgb(0.3, 0.3, 0.3);
        cr.set_line_width(0.03);

	let width = 30.;
	let a1 : f64 = dir + 180. - width;
	let a2 : f64 = dir + 180. + width;

	if true {
	  let a1r = (a2-90.).to_radians();
	  let a2r = (a1-90.).to_radians();

	  println!("{:?} {:?}", a1r, a2r);

	  cr.arc(0., 0., 0.8, a1r, a2r);
          cr.stroke();
	}

   	println!("Draw, dir {}", dir);

	let (x, y) = to_polar(a1, 0.8); cr.move_to(x, y);
	let (x, y) = to_polar(dir, 0.8); cr.line_to(x, y);
	let (x, y) = to_polar(a2, 0.8); cr.line_to(x, y);
	let (x, y) = to_polar(dir, -0.4); cr.line_to(x, y);
	cr.close_path();
	cr.stroke();

	// cairo_text_extents_t te;
	cr.set_source_rgb (0.0, 0.0, 0.0);
	cr.select_font_face ("Georgia", cairo::FontSlant::Normal, cairo::FontWeight::Normal);
	//	    CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
 	cr.set_font_size (0.1);

	let te = cr.text_extents("ahoj");
	cr.move_to (0. - te.width / 2. - te.x_bearing,
 		    0. - te.height / 2. - te.y_bearing);
	//cr.move_to(0.5, 0.5);
 	cr.show_text ("ahoj");

}

fn on_draw_state(cr : &cairo::Context, state : &Mutex<ArrowState>) {
    	let mut s = state.lock().unwrap();
	on_draw_arrow(cr, &mut s)
}


fn on_draw(cr : &cairo::Context) {

	let mut s = String::from("");
	println!("{:?}", File::open("/tmp/delme.comm").unwrap().read_to_string(&mut s));
	let mut sp = s.split(" ");
	let s1 = sp.next().unwrap();
	println!("{:?}", s1);
	if "arrow" ==  s1 {
	      println!("got arrow");
	      let s2 = sp.next().unwrap();
	      let dir = s2.parse::<f64>().unwrap();

	      println!("Fixme!");
              //let mut state = ArrowState { dir : dir };
	      //on_draw_arrow(cr, &mut state);
         }
}

fn on_timer(state : &Mutex<ArrowState>)
{
    	let mut s = state.lock().unwrap();
	println!("time");
}

fn big_format( s3 : &str, (s1, s2) : (String, String ) ) -> String {
   s3.to_string() + "\n" + &rotatable::big(&s1) + "\n" + &s2
}

fn gps_changed(s : &mut ArrowState,
   		 dist_l : &gtk::Label, speed_l : &gtk::Label, vmg_l : &gtk::Label,
		 xtrack_l : &gtk::Label, alt_l : &gtk::Label, glide_l : &gtk::Label) -> bool {
  let target = s.target;
  let d_o = dgeo::DisplayOpts {};
    let target_heading = 200.5;
    let target_alt = 279. * M;
  
   if let Some(data) = gpsc::read_gpsd(&mut s.stream) {
      if gpsc::data_valid(&data) {
      	 if let Some(gps) = dgeo::parse_gps(&data) {
	       
	      let now = gps.point;
	      let len = dgeo::get_distance(&now, &target);
	      let brg = dgeo::get_bearing(&now, &target);

	      dist_l.set_text(&big_format("distance", dgeo::format_distance(len, &d_o))); 
	      dist_l.set_use_markup(true);
	      //println!("len {:?} bearing {:?}", len, brg);

	      s.dir = brg - gps.track;

	      speed_l.set_text(&big_format("speed", dgeo::format_speed(gps.speed, &d_o))); 
	      speed_l.set_use_markup(true);

	      let vmg = f64::cos(s.dir.to_radians()) * gps.speed;

	      vmg_l.set_text(&big_format("vmg", dgeo::format_speed(vmg, &d_o))); 
	      vmg_l.set_use_markup(true);

	      let xtrack = f64::sin((target_heading - brg).to_radians()) * len;
	      xtrack_l.set_text(&big_format("xtrack", dgeo::format_distance(xtrack, &d_o))); 
	      xtrack_l.set_use_markup(true);

	      alt_l.set_text(&big_format("alt", dgeo::format_distance(gps.alt, &d_o))); 
	      alt_l.set_use_markup(true);

	      let glide = f64::atan(dgeo::get_val((gps.alt - target_alt) / len)).to_degrees();
	      glide_l.set_text(&big_format("glide", (format!("{:.2}", glide) , "deg".to_string()))); 
	      glide_l.set_use_markup(true);
	      return true;
	        }
	}
   	}
	return false;
}

fn test_gtk() {
    gtk::init().unwrap();
    // Create the main window.
    let rot = rotatable::TouchWindow::new();
    let window = rot.window;
    let (_, close) = rotatable::font_button(&rotatable::big("Close"));
    let (dist_l, dist_b) = rotatable::font_button(&rotatable::big("dist"));
    let (speed_l, speed_b) = rotatable::font_button(&rotatable::big("speed"));
    let (vmg_l, vmg_b) = rotatable::font_button(&rotatable::big("vmg"));
    let (xtrack_l, xtrack_b) = rotatable::font_button(&rotatable::big("x-track"));
    let (alt_l, alt_b) = rotatable::font_button(&rotatable::big("alt"));
    let (glide_l, glide_b) = rotatable::font_button(&rotatable::big("glide"));
    let drawing = DrawingArea::new();

    //let target = Point::new(14.+43.188/60., 50.+00.948/60.);
    //let target = Point::new(14.+41.197/60., 49.+59.980/60.);
    //let target = Point::new(14.+41.422/60., 49.+59.921/60.);
    //let target = Point::new(8.908549, 46.002686); // Lugano
    //let target = Point::new(14.685798, 49.99362); // zabka

    let r = egt::load_egt("/home/pavel/herd/wd/destination");
    let target = r[0].point;

    close.connect_clicked(|_| { gtk::main_quit(); });

    drawing.set_size_request(400, 400);

    let mut stream = gpsc::open_gpsd();
    stream.set_read_timeout(Some(std::time::Duration::new(0, 10000000)));

    let state : Arc<Mutex<ArrowState>> = Arc::new(Mutex::new(ArrowState { dir : 0., stream : stream, target : target }));
    {
	let state_ = Arc::clone(&state);
	    drawing.connect_draw(move |_, cr| { on_draw_state(cr, &state_); Inhibit(false) });
    }


    //rot.grid_main.set_orientation(Orientation::Vertical);
    //rot.grid.set_row_homogeneous(false);
    //rot.grid.set_column_homogeneous(false);

    rot.grid.attach(&drawing, 0,0, 1,6);
    rot.grid.attach(&close, 1,0, 1,1);
    rot.grid.attach(&dist_b, 1,1, 1,1);
    rot.grid.attach(&speed_b, 1,2, 1,1);
    rot.grid.attach(&xtrack_b, 1,3, 1,1);
    rot.grid.attach(&vmg_b, 1,4, 1,1);
    rot.grid.attach(&alt_b, 1,5, 1,1);
    rot.grid.attach(&glide_b, 1,6, 1,1);

    // UI initialization.
    // ...
    // Don't forget to make all widgets visible.
    window.show_all();

    // Handle closing of the window.
    window.connect_delete_event(|_, _| {
         // Stop the main loop.
         gtk::main_quit();
	 // Let the default handler destroy the window.
	 Inhibit(false)
    });

    main_iteration();

    window.show_all();

     {
       let state_ = Arc::clone(&state);
       gtk::timeout_add(300,  clone!( close => move || {
           println!("Tick tock");
           let mut s = state_.lock().unwrap();
           if gps_changed(&mut s, &dist_l, &speed_l, &vmg_l, &xtrack_l, &alt_l, &glide_l) {
	      drawing.queue_draw();
	      }
           Continue(true)
        }));
    }

    // Run the main loop.
    gtk::main();
} 

fn main() {
    test_gtk();
}

