extern crate geo;

use std::fs::File;
use std::io::Read;
use geo::Point;

#[derive(Clone, Debug)]
pub struct NamedPlace {
	pub point : Point<f64>,
	pub name : String,
}

pub struct Runway {
	pub point : Point<f64>,
	pub other : Point<f64>,
	pub name : String,
}

fn readfile(name : &str) -> String {
	let mut f = File::open(name).unwrap();
	let mut buf = String::new();
	let l = f.read_to_string(&mut buf).unwrap();

	buf
}

pub fn load_egt(name : &str) -> Vec<NamedPlace> {
	let buf = readfile(name);
	let sp : Vec<&str> = buf.split("\n").collect();
	let mut res : Vec<NamedPlace> = vec![];

	println!("{:?}", sp);
	for line in sp {
 		let spl : Vec<&str> = line.split(">").collect();
		//println!("line {:?} {}", line, spl.len());
		if spl.len() != 2 { continue; }
		println!("Got point with name {} {}", spl[0], spl[1]);

		let latlon : Vec<&str> = spl[0].split(" ").collect();
		
		let p = Point::new(latlon[1].parse::<f64>().unwrap(),
		                   latlon[0].parse::<f64>().unwrap());
 		let pl = NamedPlace { point : p, name : String::from(spl[1]) };

 		//println!("adding {:?} {:?}", res, pl);
		res.push(pl);
	}
	res
}

pub fn load_runways(name : &str) -> Vec<Runway> {
	let buf = readfile(name);
	let sp : Vec<&str> = buf.split("\n").collect();
	let mut res : Vec<Runway> = vec![];

	println!("{:?}", sp);
	for line in sp {
 		let spl : Vec<&str> = line.split(">").collect();
		//println!("line {:?} {}", line, spl.len());
		if spl.len() != 2 { continue; }
		println!("Got point with name {} {}", spl[0], spl[1]);

		let latlon : Vec<&str> = spl[0].split(" ").collect();

 		if latlon[2] != "type=runway" {
		    println!("Point is not a runway?");
		    continue;
		}
		
		let p = Point::new(latlon[1].parse::<f64>().unwrap(),
		                   latlon[0].parse::<f64>().unwrap());

		let o = Point::new(latlon[4].parse::<f64>().unwrap(),
		                   latlon[3].parse::<f64>().unwrap());

 		let pl = Runway { point : p, other : o, name : String::from(spl[1]) };

 		//println!("adding {:?} {:?}", res, pl);
		res.push(pl);
	}
	res
}