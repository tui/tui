extern crate curl;
extern crate regex;

use curl::easy::Easy;
use std::fs::File;
use std::io;
use std::io::BufWriter;
use std::io::Write;
use regex::Regex;

fn curl_get(url: &str) -> Result<Vec<u8>, io::Error> {
    let mut data = Vec::new();
    let mut handle = Easy::new();
    handle.url(url).unwrap();

    {
        let mut transfer = handle.transfer();
        transfer
            .write_function(|new_data| {
                data.extend_from_slice(new_data);
                Ok(new_data.len())
            })
            .unwrap();
        transfer.perform().unwrap();
    }

    return Ok(data);
}

pub fn get_sim_var(v : &str) -> String {
    let t = "http://localhost:5501/props/".to_string() + v;
    let r = curl_get(&t);

    match r {
    	Err(_e) => {
 	    println!("Error talking http");
	    assert!(false);
	    return "".to_string();
        }
	Ok(d) => {
	    let s = String::from_utf8(d).unwrap();
	    let l : Vec<&str> = s.split("\r").collect();

	    //println!("Getting property {} whole {}", &v, s);

	    //println!("Getting property {} line {}", &v, l[18]);

	    let re = Regex::new(r"....$").unwrap();
	    let s = re.replace_all(&l[18], "");
	    let re = Regex::new(r".*value=.").unwrap();
	    let s = re.replace_all(&s, "");

	    //println!("Getting property {} got {}", &v, s);

	    return s.to_string();
        }
    }
}

pub fn get_sim_float(v : &str) -> f64 {
    let s = get_sim_var(v);
    s.parse::<f64>().unwrap()
}

pub fn set_sim_var(v : &str, a : &str)
{
    let t = "http://localhost:5501/props/".to_string() + v + "?value=" + a + "&submit=update";
    let r = curl_get(&t);

    match r {
    	Ok(_) => {}
    	Err(_e) => {
 	    println!("Error talking http");
	    assert!(false)
        }
    }
}

