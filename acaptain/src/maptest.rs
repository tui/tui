//! # Test of map

//extern crate gio;
extern crate gtk;
extern crate osmgpsmap;
extern crate geo;
extern crate gdk_pixbuf;
//extern crate glib;
extern crate dimensioned;

use gtk::prelude::*;
use osmgpsmap::Map;
use osmgpsmap::MapExt;
use std::sync::Mutex;
use std::sync::Arc;
use std::net::TcpStream;

use std::env::args;
mod rotatable;
mod gpsc;
use gtk::*;
mod dgeo;
mod egt;

// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

struct State {
       lat : f32,
       gps : TcpStream,
}

fn add_points(map : &Map, fname : &str) {
    let r = egt::load_egt(fname);
    let pb = gdk_pixbuf::Pixbuf::from_file_at_size("../gpsim/poi.png", 24, 24).unwrap();

    println!("Adding points? {:?}", r);
    for p in r {
    	println!("Adding point {} {}", p.point.y(), p.point.x());
        let image = map.image_add(p.point.y() as f32, p.point.x() as f32, &pb);
    }
}

fn test_ui() {
    let window = gtk::Window::new(gtk::WindowType::Toplevel);
    let overlay = gtk::Overlay::new();
    let (_, close) = rotatable::font_button(&rotatable::big("Close"));
    let (_, in_b) = rotatable::font_button(&rotatable::big("+"));
    let (_, out_b) = rotatable::font_button(&rotatable::big("-"));
    let (_, test_b) = rotatable::font_button(&rotatable::big("test"));
    let (_, test_b2) = rotatable::font_button(&rotatable::big("test2"));

    let grid = gtk::Grid::new();

    let test_l = rotatable::font_label(&rotatable::big("Hello, world"));

    let map = Map::new();
    map.set_size_request(800, 480);

    grid.attach(&in_b, 0,0, 1,1);
    grid.attach(&out_b, 0,1, 1,1);

    close.set_valign(gtk::Align::Start);
    close.set_halign(gtk::Align::Center);

    test_l.set_valign(gtk::Align::End);

    grid.set_valign(gtk::Align::End);
    grid.set_halign(gtk::Align::End);

    overlay.add(&map);
    overlay.add_overlay(&close);
    overlay.add_overlay(&test_l);
    overlay.add_overlay(&grid);

    window.add(&overlay);
    window.show_all();

    window.connect_delete_event(clone!(window => move |_, _| {
        //window.destroy();
	gtk::main_quit();
        Inhibit(false)
    }));

    // https://nzjrs.github.io/osm-gps-map/docs/reference/html/OsmGpsMap.html
    close.connect_clicked(|_| { gtk::main_quit(); });
    in_b.connect_clicked(clone!(map => move |_| { map.zoom_in(); }));
    out_b.connect_clicked(clone!(map => move |_| { map.zoom_out(); }));

    let mut lon = 14.;

    //add_points(&map, "../mission/mission.egt");
    //add_points(&map, "/home/pavel/herd/wd/destination");


    //fn set_center_and_zoom(&self, latitude: f32, longitude: f32, zoom: i32);
    // fn set_keyboard_shortcut(&self, key: MapKey_t, keyval: u32);

    let stream = gpsc::open_gpsd();
    stream.set_read_timeout(Some(std::time::Duration::new(0, 10000000)));
    let state : Arc<Mutex<State>> = Arc::new(Mutex::new( State { lat : 50., gps : stream } ));


     {
	let state_ = Arc::clone(&state);
 	gtk::timeout_add(300, clone!(map => move || {
	    println!("Tick tock");
	    let mut s = state_.lock().unwrap();
 	    if let Some(data) = gpsc::read_gpsd(&mut s.gps) {
	          if gpsc::data_valid(&data) {
 		        if let Some(gps) = dgeo::parse_gps(&data) {
			     map.gps_add(gps.point.y() as f32, gps.point.x() as f32, gps.track as f32);
			}
                  }
	    }
 	    Continue(true)
        }));
    }
     {
	let state_ = Arc::clone(&state);
	test_b.connect_clicked(clone!(map => move |_| {
            let mut s = state_.lock().unwrap();
 	    map.gps_add(s.lat, lon, 50.);
 	    s.lat = s.lat+0.3;
        }));
    }
    //glib::source::io_add_watch(0, 0, clone!(map => move || {
    //	println!("New data on stdin");
    //});

    map.connect_button_release_event(clone!(map => move |_,e| {
	 	println!("button-release {:?}", e);
		let mut e2 = e.clone();
		//let point = map.get_event_location(&mut e2);
	 	//println!("button-release point {:?}", point.unwrap().get_degrees());
	         Inhibit(false)
    }));

}

fn main() {
    gtk::init().unwrap();
    test_ui();
    gtk::main();
}