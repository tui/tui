extern crate json;

use dimensioned::unit_systems::si::{Meter,MeterPerSecond,MPS,M};
use dimensioned::Dimensionless;
use dimensioned;

use geo::Point;
use geo::prelude::HaversineDistance;

pub fn kmh() -> MeterPerSecond<f64> { (1./3.6) * MPS }
pub fn kt() -> MeterPerSecond<f64> { 0.51444457477 * MPS }

type Point2D = Point<f64>;
type Degrees = f64;
type Radians = f64;

pub fn to_polar(angle : Degrees, dist : f64) -> (f64, f64) {
      let a = angle.to_radians();
      ( dist * f64::sin(a), -dist * f64::cos(a))
}

#[derive(Clone, Debug)] 
pub struct Point3D {
       pub point : Point2D,
       pub alt : Meter<f64>,
}

pub fn normalize(mut deg : Degrees) -> Degrees {
	while deg < 0. { deg += 360. }
	while deg > 360. { deg -= 360. }
	deg
}

pub fn get_bearing(origin : & Point2D, destination : & Point2D) -> Degrees {
// https://gist.github.com/geografa/1366401
	let (lon1, lat1) = ( origin.x().to_radians(), origin.y().to_radians() );
	let (lon2, lat2) = ( destination.x().to_radians(), destination.y().to_radians() );
	let dlon = lon2-lon1;

	let bearing = f64::atan2(f64::sin(dlon)*f64::cos(lat2),f64::cos(lat1)*f64::sin(lat2)-f64::sin(lat1)*f64::cos(lat2)*f64::cos(dlon));

	let deg = bearing.to_degrees();
	normalize(deg)
}

pub fn get_distance(origin : & Point2D, destination : & Point2D) -> Meter<f64> {
    origin.haversine_distance(&destination) * M
}

pub fn get_length(val : Meter<f64>, unit : Meter<f64>) -> f64 {
    get_val( val/unit )
}

pub fn get_val(val : dimensioned::si::Unitless<f64>) -> f64 {
    *val.value()
}

pub struct DisplayOpts {}

pub fn format_distance(len : Meter<f64>, _opt : &DisplayOpts) -> (String, String) {
    if len > 200_000. * M {
        let dist = format!("{:.0}", get_length(len, 1000. * M));
    	let unit = format!("km");
	return (dist, unit);
     }
     if len > 20_000. * M {
        let dist = format!("{:.1}", get_length(len, 1000. * M));
    	let unit = format!("km");
	return (dist, unit);
     }
    if len > 2000. * M {
        let dist = format!("{:.2}", get_length(len, 1000. * M));
    	let unit = format!("km");
	return (dist, unit);
     }
    if len > 20. * M {
        let dist = format!("{:.0}", get_length(len, M));
    	let unit = format!("m");
	return (dist, unit);
     }
     let dist = format!("{:.1}", get_length(len, M));
     let unit = format!("m");
     return (dist, unit);
}

pub fn format_speed(val : MeterPerSecond<f64>, _opt : &DisplayOpts) -> (String, String) {
    if val > 20. * kmh() {
        let dist = format!("{:.0}", get_val(val / kmh()));
    	let unit = format!("km/h");
	return (dist, unit);
     }
     let dist = format!("{:.1}", get_val(val / kmh()));
     let unit = format!("km/h");
     return (dist, unit);
}

pub struct GpsData {
       pub point : Point2D,
       pub alt : Meter<f64>,

       pub track : f64,
       pub speed : MeterPerSecond<f64>,
}

pub fn parse_gps(data : &json::JsonValue) -> Option<GpsData> {
    let lat : f64 = data["lat"].as_number()?.into();
    let lon : f64 = data["lon"].as_number()?.into();
    let track : f64 = data["track"].as_number()?.into();
    let speed_ : f64 = data["speed"].as_number()?.into();
    let speed = speed_ * MPS;
    let alt_ : f64 = data["alt"].as_number()?.into();
    let alt = alt_ * M;

    println!("Parse gps ok");

    let point = Point::new(lon, lat);

    Some(GpsData{ point : point, track : track, alt : alt, speed : speed })

}