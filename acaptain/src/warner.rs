extern crate curl;
extern crate regex;

mod fghttp;

use fghttp::*;

fn watch() {
    loop {
        println!("");
    	println!("AP: {} {} {}", get_sim_var("autopilot/locks/heading"),
		                 get_sim_var("autopilot/locks/altitude"),
				 get_sim_var("autopilot/locks/speed"));
    	println!("Speedup: {}", get_sim_var("sim/speed-up"));
	println!("Height: AGL {}, {}", get_sim_var("position/altitude-agl-ft"), get_sim_var("position/altitude-ft"));
	println!("Orientation: roll {}, pitch {}, track {}",
			       get_sim_var("orientation/roll-deg"),
			       get_sim_var("orientation/pitch-deg"),
			       get_sim_var("orientation/track-deg"));

 	println!("Surfaces: flaps {}, speedbrake {}",
			       get_sim_var("surface-positions/flap-pos-norm"),
			       get_sim_var("surface-positions/speedbrake-pos-norm"));

	println!("Engine: {}", get_sim_var("engines/engine/rpm"));
	println!("Speed: {} mach {} vertical {}", get_sim_var("velocities/airspeed-kt"),
			              get_sim_var("velocities/mach"),
				      get_sim_var("velocities/vertical-speed-fps"));

	println!("Fuel: {} kg", get_sim_var("consumables/fuel/total-fuel-kg"));
			
        std::thread::sleep(std::time::Duration::from_millis(1000))
    }
}

fn main() {
    watch();
}