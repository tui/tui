extern crate stick;

use stick::Port;


fn dump() {
	// Connect to all devices.
	let mut port = Port::new();

	// Loop showing state of all devices.
	loop {
		// Cycle through all currently plugged in devices.
		let id = if let Some(a) = port.poll() {
			a
		} else {
			continue;
		};

		if let Some(state) = port.get(id) {
			println!("{}: {}", id, state);
		}
	}
}

fn handle(state : &stick::Device) {
	println!(": {}", state);

	//println!("{:?}", state.joy());

	// Prava ruka
	// 0,1,2,3 -- kloboucek
	// 4 -- trigger
	// 10 -- malicek
	// 5 -- A
	// 6 -- launch
	// 11 -- fire C

	// Leva ruka
	// 12, 13 -- mode selection
	// 1, 2 -- aux1
	// 8 -- D
	for i in 0..20 {
		if let Some(on) = state.btn(i) {
			if on {
				println!("{:?} {:?}", i, on);
			}
		}	
	}		
//println!("{:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?}",
//		 state.btn(0), state.btn(6), state.btn(7), state.btn(8), state.btn(9),
//		 state.btn(10), state.btn(11), state.btn(12), state.btn(13), state.btn(14));
}

fn main() {
	let mut port = Port::new();

	loop {
		// Cycle through all currently plugged in devices.
		let id = if let Some(a) = port.poll() {
			a
		} else {
			continue;
		};

		if let Some(state) = port.get(id) {
			handle(&state);
		}

		
	}

}
