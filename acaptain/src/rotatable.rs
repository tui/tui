#![allow(dead_code)]

// This version is obsolete; rfone/ has better version.

extern crate gtk;
//extern crate glib;

use gtk::prelude::*;
use gtk::*;

pub struct TouchWindow {
       pub vertical : bool,
       pub window : Window,
       pub grid : Grid,
       pub grid_main : Grid,
       pub grid_aux : Grid,
}

impl TouchWindow {
     pub fn new() -> Self {
        let window = Window::new(WindowType::Toplevel);
	let vertical = false;
	
	if !vertical {
	   window.set_default_size(800, 400);
	} else {
	   window.set_default_size(400, 800);
	}
	window.set_title("Rotatable test");

	let grid = Grid::new();
	if vertical {
		grid.set_orientation(Orientation::Vertical);
        }
	//grid.set_row_homogeneous(true);
	//grid.set_column_homogeneous(true);	

	let grid_main = Grid::new();
	let grid_aux = Grid::new();

	//grid.add(&grid_aux);
	//grid.add(&grid_main);

	window.add(&grid);

     	TouchWindow { vertical : vertical,
		      window : window,
 		      grid : grid,
		      grid_main : grid_main,
		      grid_aux : grid_aux,
		     }
     }
}

pub fn big(s : &str) -> String { format!("<span size=\"xx-large\">{}</span>", s) }

//    def middle(m, s): return '<span size="x-large">%s</span>' % s
//    def small(m, s): return '<small>%s</small>' % s

pub fn font_label(s : &str) -> gtk::Label {
   let w = Label::new(Some(s));
   w.set_use_markup(true);
   w
}

pub fn font_button(s : &str) -> (gtk::Label, gtk::Button) {
   let w = font_label(s);
   let w1 = Button::new();
   w1.add(&w);
   ( w, w1 )
}

//    def font_entry(m, size = 32, font=""):
//         e = gtk.Entry()
//         e.modify_font(pango.FontDescription("sans "+str(size)))
//         return e

//    def big_button(m, big, small):
//         return m.font_button(m.big(big) + '\n' + m.small(small))

fn test_gtk() {
    gtk::init().unwrap();
    // Create the main window.
    let window = TouchWindow::new();
    
    let (_, close) = font_button(&big("Close"));

    let grid = window.grid_main;
    let win = window.window;
  
    close.connect_clicked(|_| { println!("button") });

    grid.add(&close);

    // UI initialization.
    // ...
    // Don't forget to make all widgets visible.
    win.show_all();

    // Handle closing of the window.
    win.connect_delete_event(|_, _| {
         // Stop the main loop.
         gtk::main_quit();
	 // Let the default handler destroy the window.
	 Inhibit(false)
    });

    main_iteration();

    //label.set_text("ready...");
    win.show_all();

    // glib::source::timeout_add(5000, move || { drawing.queue_draw(); on_timer(); Continue(true) });

    // Run the main loop.
    gtk::main();
}

fn main() {
    test_gtk();
}
