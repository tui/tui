/* cat airports.egt | grep LSZH > ~/herd/wd/destination */

extern crate dimensioned;
extern crate geo;
extern crate curl;
extern crate regex;

mod dgeo;
mod egt;
use egt::Runway;
mod fghttp;

use fghttp::*;
use geo::Point;
use geo::algorithm::haversine_distance::HaversineDistance;
use geo::algorithm::haversine_destination::HaversineDestination;

/* https://docs.rs/dimensioned/0.7.0/dimensioned/unit_systems/si/index.html */
use dimensioned::unit_systems::si::{Unitless,Meter,M,FT,MeterPerSecond,MPS,MIN,S};
use dimensioned::Dimensionless;

fn val(x : Unitless<f64>) -> f64 { *(x).value() }
fn kmh(x : f64) -> MeterPerSecond<f64> { x * MPS/3.6 }
fn knots(x : f64) -> MeterPerSecond<f64> { kmh(x) * 1.852  }
fn fpm(x : f64) -> MeterPerSecond<f64> { FT * x / MIN }

trait Plane {
    fn reasonable_speed(&self, a : Meter<f64>) -> MeterPerSecond<f64> {
        return knots(180.);
    
        if a < 30000. * FT { return knots(250.); }
        if a < 37000. * FT { return knots(220.); }
        if a < 40000. * FT { return knots(200.); }
        if a < 45000. * FT { return knots(180.); }
        return knots(150.);
    }

    /* With drop tanks, it starts to lose speed at 48000ft */
    fn ceiling(&self) -> Meter<f64> { 48000. * FT }

    fn reasonable_altitude(&self, dist : Meter<f64>) -> Meter<f64> {
       /* 5.2% corresponds to 3 degrees glideslope */

       /* 20% is too much for MiG-15 in clean config */
       let target_alt = dist * 0.07;
       let c = self.ceiling();

       if target_alt > c { return c; }
       return target_alt;
    }

    fn reasonable_vertical_speed(&self, my_alt : Meter<f64>, alt : Meter<f64>) -> MeterPerSecond<f64> {
       let mut res = fpm(0.);
       let mut max_climb = fpm(500.);
       /* 500 fpm climb is considered limit of height for airliner */
       let mut max_desc = fpm(-2500.);

       res = (alt - my_alt) / MIN;

       if my_alt < 44000. * FT { max_climb = fpm(1000.) }
       if my_alt < 41000. * FT { max_climb = fpm(2000.) }
       if my_alt < 31000. * FT { max_climb = fpm(3000.) }
       if my_alt < 15000. * FT { max_climb = fpm(4000.) }

       if my_alt < 21000. * FT { max_desc = fpm(-3500.) }

       if res > max_climb { res = max_climb }
       if res < max_desc { res = max_desc }

       res
    }

    fn reasonable_turn_diameter(&self, alt : Meter<f64>, speed : MeterPerSecond<f64>) -> Meter<f64> {
       13000.*M
    }

    fn reasonable_final(&self) -> Meter<f64> {
       10000.*M
    }

    fn reasonable_glide_altitude(&self, dist : Meter<f64>) -> Meter<f64> {
       //dist/14.2   // @250 kn.
       dist/15.7   // @180 kn. -- engine on.
    }
}

struct MiG15 {}
struct SpaceShuttle {}

impl Plane for MiG15 {}
impl Plane for SpaceShuttle {
    fn reasonable_turn_diameter(&self, alt : Meter<f64>, speed : MeterPerSecond<f64>) -> Meter<f64> {
       43000.*M
    }

    fn reasonable_final(&self) -> Meter<f64> {
       20000.*M
    }

    fn reasonable_glide_altitude(&self, dist : Meter<f64>) -> Meter<f64> {
       dist/3.
    }
}


fn rwy_heading(r : &egt::Runway) -> f64 {
    dgeo::get_bearing(&r.point, &r.other)
}

fn similar_angle(a1 : f64, a2 : f64, limit : f64) -> bool {
    let a = dgeo::normalize(a1-a2);

    a < limit || a > 360.-limit
}

fn print_point(p : &Point<f64>, s : &str) {
    println!("{} {} > {}", p.y(), p.x(), s);
}

// We should aim to the given point, and if we are at glide_alt, we can simply
// glide to the destination
struct NavData {
    target : Point<f64>,
    glide_alt : Meter<f64>,
    phase : String,
}

fn navigate_circle(plane : &impl Plane, pos : &Point<f64>, heading : f64, r : &egt::Runway) -> NavData {
     let len = r.point.haversine_distance(&r.other);
     let brg = rwy_heading(r) + 180.;

     let approach_point = r.point.haversine_destination(brg, val(plane.reasonable_final() / M));
     let turn = plane.reasonable_turn_diameter(0.*M, knots(250.));
     let center = approach_point.haversine_destination(brg + 90., val(turn / M));
     let now_center = pos.haversine_destination(heading - 90., val(turn / M));

     let to_circle = dgeo::get_distance(&center, &now_center);

     let mut turn_remaining = heading - rwy_heading(r);
     if turn_remaining < 0. { turn_remaining += 360. }
     let turn_distance = turn_remaining.to_radians() * turn;
     let sum_distance = turn_distance + plane.reasonable_final();

     let on_circle = to_circle < 3000. * M;
     if on_circle {
	 let target_angle = dgeo::get_bearing(&center, pos);
	 //println!("2: on circle? {} ( {} )", turn_distance, to_circle);
	 let target = center.haversine_destination(target_angle - 30., val(turn / M));
	 return NavData { target : target, glide_alt : plane.reasonable_glide_altitude(sum_distance), phase : "on circle".to_string() };
     }

     //println!("3: to circle {} {}", turn_distance, to_circle );
     let target = center.haversine_destination(heading+90., val(turn / M));
     //let target = center;
     //let target = r.point;
     return NavData { target : target, glide_alt : plane.reasonable_glide_altitude(sum_distance + to_circle), phase : "to circle".to_string() };
}

fn on_direct_approach(pos : &Point<f64>, heading : f64, rwy : &egt::Runway) -> bool {
    let rwy_h = rwy_heading(&rwy);
    let to_rwy = dgeo::get_bearing(pos, &rwy.point);

    similar_angle(rwy_h, to_rwy, 15.) && similar_angle(rwy_h, heading, 15.)
}

fn navigate(plane : &impl Plane, pos : &Point<f64>, heading : f64, rwy : &egt::Runway) -> NavData {
    if on_direct_approach(pos, heading, rwy) {
        let len = dgeo::get_distance(&pos, &rwy.point);
        //println!("1: on final approach {}", len);
 	return NavData { target : rwy.point, glide_alt : plane.reasonable_glide_altitude(len), phase : "final".to_string() };
    }

    navigate_circle(plane, pos, heading, rwy)
}

fn fly_to(plane : &impl Plane, rwy : &egt::Runway) {
    loop {
	let my_alt = get_sim_float("position/altitude-ft") * FT;
	let my_agl = get_sim_float("position/altitude-agl-ft") * FT;
	let now = Point::new(get_sim_float("position/longitude-deg"), get_sim_float("position/latitude-deg"));
	let heading = get_sim_float("orientation/heading-deg");
	let verbose = false;

	if verbose {
    	    println!("Position: {} ft, agl: {}", my_alt, my_agl);

	    println!("Speed: {} mach {}", get_sim_var("velocities/airspeed-kt"),
			                  get_sim_var("velocities/mach"));
	}

 	let len = dgeo::get_distance(&now, &rwy.point);
	let target = navigate(plane, &now, heading, rwy);
	let brg = dgeo::get_bearing(&now, &target.target);

	let speed = plane.reasonable_speed(my_alt);
	let speed_kt = val(speed / knots(1.));

	if false {
	    let mut alt = plane.reasonable_altitude(len);
	    let ground_diff = (3000. * FT) - my_agl;
	    if ground_diff > 0. * M {
	        let lim = ground_diff + my_alt;
	        if alt < lim { alt = lim; }
	    }
	    let alt_ft = val(alt / FT);
    	    set_sim_var("autopilot/settings/target-altitude-ft", &format!("{}", alt_ft as i32));

	    let vs = plane.reasonable_vertical_speed(my_alt, alt);
	    let vs_fpm = val(vs / fpm(1.));
	    
 	    set_sim_var("autopilot/settings/vertical-speed-fpm", &format!("{}", vs_fpm as i32));
        }

	if true {
	    let alt = target.glide_alt + 1470.*FT;
	    let alt_ft = val(alt / FT);
	    println!("Fly heading {} altitude {} phase {}", brg, alt, &target.phase);
    	    set_sim_var("autopilot/settings/target-altitude-ft", &format!("{}", alt_ft as i32));
	}

	set_sim_var("autopilot/settings/target-speed-kt", &format!("{}", speed_kt as i32));
	set_sim_var("autopilot/settings/true-heading-deg", &format!("{}", brg as i32));

        /* std::thread::sleep(std::time::Duration::from_millis(1000)) */
    }
}

// Read destination from fgame, fly there
fn fly() {
    let r = egt::load_runways("/home/pavel/herd/wd/destination");
    let plane = MiG15 {};
    fly_to(&plane, &r[0]);
}

fn test() {
    let r = egt::load_runways("/home/pavel/herd/wd/destination");

    let plane = MiG15 {};
}

fn glide() {
    // Dubendorf -- LSMD -- 47.398, 8.644, 1470 ft.

    let target = Runway { point : Point::new(8.6637, 47.3950), other : Point::new(8.6321, 47.4018), name : String::from("Dubendorf") };

    let plane = MiG15 {};

    fly_to(&plane, &target);
}

fn shuttle() {
// Vandenberg KVBG
// --aircraft=SpaceShuttle-entry --lat=0.0 --lon=-160.0 --heading=45 --timeofday=morning

// /data/fgfs/2020.3/FlightGear-2020.3.1-x86_64.AppImage --aircraft-dir=`pwd` --aircraft=SpaceShuttle-entry --lat=0.0 --lon=-160.0 --heading=45 --timeofday=morning --nmea=socket,out,1,localhost,5500,udp --fg-scenery=/data/fgfs/Scenery/ --prop:/sim/hud/path[1]=../../../../../../../../../data/pavel/g/tui/fgame/hud.xml --httpd=5501

    // Dubendorf -- LSMD -- 47.398, 8.644, 1470 ft.
    //
    let target = Runway { point : Point::new(-120.5676,34.7183),
    	       	 	  other : Point::new(-120.6024,34.7516,), name : String::from("Vandenberg AFB") };

    let plane = SpaceShuttle {};

    fly_to(&plane, &target);
}

fn main() {
//     shuttle();
     glide();
//    fly();
//    test();
}