// Rust Positioning System

extern crate json;

use std::io::prelude::*;
use std::net::TcpStream;
use std;

pub fn open_gpsd() -> TcpStream
{
    let mut stream = TcpStream::connect("127.0.0.1:2947").unwrap();

    let _ = stream.write(b"?WATCH={\"class\":\"WATCH\",\"json\":true,\"enabled\":true};");

    stream
}

fn string_read( stream : &mut TcpStream ) -> String {
       let mut r = [0; 1];
       let mut res = [0; 10240];
       let mut i = 0;

       while r[0] != 0x0d {
	     if let Ok(n) = stream.read(&mut r) {
	         if n == 0 {
		    println!("Got zero");
	         }

 		 if r[0] != 0x0d {
	            res[i] = r[0];
	     	    i += 1;
	         }
	     } else { return String::from(""); }
       }

       let o = std::str::from_utf8(&res[0..i]).unwrap();

       String::from(o)
}

pub fn read_gpsd(stream : &mut TcpStream) -> Option<json::JsonValue>
{
    let buf = &mut [0; 1024];
    loop {

	 let o = string_read(stream);
	 println!("{}", o);

	let parser = json::parse(&o[..]);
	if let Ok(data) = parser {
		println!("{}", data["class"]);
		println!("{}", data["lat"]);
		println!("{}", data["lon"]);

		return Some(data);
	} else {
	       return None;
        }
    }
}

pub fn data_valid(data : &json::JsonValue) -> bool
{
	data["class"] == "TPV" && data["mode"] == 3
}

#[cfg(test)]
mod tests {
  use super::*;
  
  #[test]
    fn it_works() {
        println!("!");

    	let mut stream = open_gpsd();
 	loop {
    	     let _ = read_gpsd(&mut stream);
 	}
    }
}