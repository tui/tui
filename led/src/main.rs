use std::env;
use std::io::prelude::*;

static class_leds: &str = "/sys/class/leds/";

fn help() {
    println!("led command [args]");
    println!();
    println!("led ls -- list LEDs");
    println!("led on|off spec -- turn LED on or off");
    println!("led set spec val -- select specific brightness");
    std::process::exit(1);
}

fn leds_list() {
    let path = class_leds;
    let entries = std::fs::read_dir(path).unwrap()
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, std::io::Error>>().unwrap();

    // .map(|res| res.map(|p| p.to_str().unwrap()[7..]))

    println!("{:?}", entries);

    for x in entries {
        // println!("{:?}", x);
	let y = &x.to_str().unwrap()[path.len()..];
        println!("{:?}", y);
    }
}

fn read_file(f: &str) -> String
{
    let path = std::path::Path::new(f);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = std::fs::File::open(&path).unwrap();

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    
    file.read_to_string(&mut s);

    s
}

fn write_file(f: &str, c: &str)
{
    let path = std::path::Path::new(f);
    let display = path.display();

    std::fs::write(f, c.as_bytes()).unwrap();

    // Open the path in read-only mode, returns `io::Result<File>`
    //let mut file = std::fs::File::open(&path).unwrap();

    //file.write_all(c.as_bytes()).unwrap();
}

fn led_set_brightness(led: &str, br: i32)
{
    let path = class_leds.to_owned() + led + "/brightness";
    let val = &format!("{}", br);
    println!("Writing to path {} {}", path, val);

    write_file(&path, "0");  // Clear any triggers, first
    write_file(&path, val);
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        help();
    }
    let cmd = &args[1][..];
    match cmd {
        "l" | "ls" | "list" => leds_list(),
	"on" => led_set_brightness(&args[2], 999999999),
	"off" => led_set_brightness(&args[2], 0),
	"set" => {
	    let br = i32::from_str_radix(&args[3], 10).unwrap();
 	    led_set_brightness(&args[2], br);
	    },
	_ => help(),
    }
}
