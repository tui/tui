#![allow(dead_code)]

const DO_FFT : bool = false;  // FFT is faster but produces worse results
const SLOW_NET : bool = false; // Intentionaly slow stuff down
const DO_CROSS : bool = true;  // Display cross ASAP
const MY_LAT : f64 = 50.;      // Default position
const MY_LON : f64 = 14.75;

extern crate png;

use std::path::Path;

use std::io;
use std::io::BufWriter;
use std::env;
// To use encoder.set()
use png::HasParameters;

use std::fs::File;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use std::io::Write;

extern crate gtk;
extern crate glib;
//extern crate cairo;

//use cairo::{Context, Format, ImageSurface};

use gtk::prelude::*;
use gtk::*;
//use glib::source::*;

// http://gtk-rs.org/docs/gtk/struct.Image.html#method.new_from_pixbuf

// gtk::Image::new_from_pixbuf()
// cairo can load/store png.
// gdk_pixmap_new

extern crate chfft;
extern crate num_complex;
use num_complex::Complex;
use chfft::CFft2D;

extern crate curl;
use curl::easy::Easy;

extern crate time;

struct Chmi {}

trait Radar {
    fn get_skip(&self) -> i32 {
        5
    }
    fn get_resolution(&self) -> i32 {
        15
    }
    fn get_url(&self, t: &time::Tm) -> String {
        return format!(
            "https://www.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/pacz23.z_max3d.{:04}{:02}{:02}.{:02}{:02}.0.png",
            1900 + t.tm_year,
            t.tm_mon + 1,
            t.tm_mday,
            t.tm_hour,
            t.tm_min
        );
    }

    // Thanks to Milan B.
    fn transform_old(&self, dlat: f64, dlon: f64) -> (isize, isize) {
        // converion of LONG LAT to picture XY,
        // picture [0,0] is the center of upper left pixel
        // gnomonic projection only
        // v.0.91 990520 ph,pn,jk

        // 230-67 pixels is cca 320km. => 1.95km per pixel

        let rad2deg = 57.29577951308232; //  180/pi - prevod z radianu na stupne
        //    rad2deg = 1.0 #  180/pi - prevod z radianu na stupne

        let geogr_lat0 = 50.008 / rad2deg;
        let geogr_lon0 = 14.447 / rad2deg;
        let geogr_x0 = 127.5;
        let geogr_y0 = 127.5;
        let cell_size = 2.0;

        let rzeme = 6378.144;
        let my_pi = 3.141592653589793;
        let my_2pi = 6.283185307179586;
        let my_pi_2 = 1.570796326794897;

        // zacatek sfer_polar(fi1,lam1,fi2,lam2,del,az)
        let lam2 = -1. * (dlon / rad2deg);
        let fi2 = dlat / rad2deg;
        let lam1 = -1. * geogr_lon0;
        let fi1 = geogr_lat0;

        let mut s = (f64::sin(fi2) * f64::sin(fi1)) + (f64::cos(fi2) * f64::cos(fi1) * f64::cos(lam2 - lam1));
        let del_ = f64::acos(s);
        s = f64::sin(fi2) - (f64::sin(fi1) * s);
        let r = f64::cos(fi1) * f64::sin(del_);
        if r != 0.0 {
            s = s / r;
        } else {
            s = 0.;
        }

        let mut az = f64::acos(s); //        Azimut v radianech
        let dlam = lam2 - lam1;
        if (lam2 > lam1) && (dlam < my_pi) {
            az = my_2pi - az;
        }

        // konec sfer_polar(fi1,lam1,fi2,lam2,del_,az)
        // zacatek azim_gnomon(psi,alfa,x,y)

        let psi = del_;
        let alfa = az;
        let mut x = 0.0;
        let mut y = 0.0;
        if psi < my_pi_2 {
            let ro = f64::tan(psi);
            x = ro * f64::sin(alfa);
            y = ro * f64::cos(alfa);
        }
        x = geogr_x0 + (x * rzeme) / cell_size;
        y = geogr_y0 - ((y * rzeme) / cell_size);

        return (x as isize + 20, y as isize + 44);
    }

    fn transform(&self, lat: f64, lon: f64) -> (isize, isize) {
        let (mut x, mut y) = self.transform_old(lat, lon);
        y -= 300;
        x *= 2;
        y *= 2;
        x += 5;
        y += 557;
        return (x, y);
    }
}

impl Radar for Chmi {
}

#[derive(Clone)]
struct Picture {
    sx: isize,
    sy: isize,
    data: Vec<u8>,
}

fn get(p: &Picture, x: isize, y: isize) -> i32 {
    if x < 0 {
        return 0;
    }
    if y < 0 {
        return 0;
    }
    if x >= p.sx {
        return 0;
    }
    if y >= p.sy {
        return 0;
    }
    return p.data[(y * p.sx + x) as usize] as i32;
}

fn diff_val(p1: &Picture, p2: &Picture, sx: isize, sy: isize) -> f64 {
    let mut sum = 0.0;

    for y in 0..p1.sy {
        for x in 0..p1.sx {
	    if true { /* OPT */
	          let mut d = get(p1, x, y) - get(p2, x + sx, y + sy);
 		  if d < 0 {
 		       d = -d;
                  }
 		  sum += (d * d) as f64;
             } else {
	          sum -= get(p1, x, y) as f64 * get(p2, x + sx, y + sy) as f64;
             }
        }
    }
    return sum;
}

fn rain_map(r: u8, g: u8, b: u8) -> u8 {
    return match (r, g, b) {
        (255, 0, 0) => 0,
        (255, 0, 255) => 0,
        (196, 196, 196) => 0,
        (0, 0, 0) => 0,
        (3, 1, 1) => 0,
        (168, 168, 168) => 0,
        (56, 0, 112) => 4,
        (48, 0, 168) => 8,
        (0, 0, 252) => 12,
        (0, 108, 192) => 16,
        (0, 160, 0) => 20,
        (0, 188, 0) => 24,
        (52, 216, 0) => 28,
        (156, 220, 0) => 32,
        (224, 220, 0) => 36,
        (252, 176, 0) => 40,
        (252, 132, 0) => 44,
        (252, 88, 0) => 48,
        (252, 0, 0) => 52,
        (160, 0, 0) => 56,
        (252, 252, 252) => 60,
        _ => panic!("Unkown color {:?}", (r, g, b)),
    };
}

fn load_fs(s: &str) -> Picture {
    let decoder = png::Decoder::new(File::open(s).unwrap());
    let (info, mut reader) = decoder.read_info().unwrap();
    let size = info.buffer_size();
    let mut buf = vec![0; size];
    reader.next_frame(&mut buf).unwrap();
    let mut buf2 = vec![0; size / 4];

    println!(
        "info= {:?} x {:?} {:?} {:?}",
        info.width,
        info.height,
        info.bit_depth,
        info.color_type
    );

    for y in 0..info.height {
        for x in 0..info.width {
            let i = (x + y * info.width) as usize;
            let r = buf[4 * i];
            let g = buf[4 * i + 1];
            let b = buf[4 * i + 2];

            /* clear_borders */
            if x >= 728 {
                continue;
            }
            if y < 81 {
                continue;
            }
            if y >= 535 {
                continue;
            }
            buf2[i] = rain_map(r, g, b);
        }
    }

    return Picture {
        sx: info.width as isize,
        sy: info.height as isize,
        data: buf2,
    };
}

struct RGBAPicture {
    sx: isize,
    sy: isize,
    data: Vec<u8>,
}

fn set(p: &mut RGBAPicture, x: isize, y: isize, v: u8) {
    if x < 0 {
        return;
    }
    if y < 0 {
        return;
    }
    if x >= p.sx {
        return;
    }
    if y >= p.sy {
        return;
    }

    let i = ((y * p.sx + x) * 4) as usize;

    p.data[i] = v;
    p.data[i + 1] = v;
    p.data[i + 2] = v;
    p.data[i + 3] = 255;
}

fn add_cross(p: &mut RGBAPicture, x: isize, y: isize, s: isize) {
    for i in s / 5..s {
        set(p, x - i, y, 255);
        set(p, x + i, y, 255);
        set(p, x, y - i, 255);
        set(p, x, y + i, 255);
    }
}

fn save_fs(po: &RGBAPicture, path : &Path) {
    let file = File::create(path).unwrap();
    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, po.sx as u32, po.sy as u32);
    encoder.set(png::ColorType::RGBA).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(&po.data[..]).unwrap(); // Save
}

fn save_result(p: &Picture, loc: (isize, isize), shift: (isize, isize), path : &Path) {
    let mut po = RGBAPicture {
        sx: p.sx,
        sy: p.sy,
        data: vec![0 as u8; (p.sx * p.sy * 4) as usize],
    };
    for y in 0..p.sy {
        for x in 0..p.sx {
            let v = (get(p, x, y) * 4) as u8;
            set(&mut po, x, y, v);
        }
    }

    let (locx, locy) = loc;
    let (sx, sy) = shift;

    add_cross(&mut po, locx, locy, 100);
    add_cross(&mut po, locx - sx, locy - sy, 75);
    add_cross(&mut po, locx - 4 * sx, locy - 4 * sy, 50);
    save_fs(&po, path);
}

fn get_shift(p : &ComputeParams, p1: &Picture, p2: &Picture) -> (isize, isize) {
    let (tx, rx) = mpsc::channel();

    let limit = 30;
    let step = limit / 2;

    let all = vec![
        (-limit,     -step),
        (-step,      0),
        (0,          step),
        (step,       limit + 1),
    ];
    for (lo, hi) in all {
        let tx1 = mpsc::Sender::clone(&tx);
        let p1c = p1.clone();
        let p2c = p2.clone();
        thread::spawn(move || for sy in lo..hi {
	    let pr = sy - lo;
            for sx in -limit..limit + 1 {
                let d = diff_val(&p1c, &p2c, sx, sy);

                let b = (d as i64, pr, (sx, sy));
                tx1.send(b).unwrap();
            }
        });
    }
    drop(tx);

    println!("Displaying progress");
    let mut best = 999999999999999;
    let mut shift = (0, 0);
    for (d, pr, sh) in rx.iter() {
        compute_print(p, &format!("{}% computing", (100 * pr) / step));
	if d < best {
	    best = d;
	    shift = sh;
	}
    }
    println!("Computing minimum");
    if false {
    let min = rx.iter().min().unwrap();
    println!("Min is {:?}", min);
    let (_best, _pr, shift) = min;
}
    println!("Computation done");

    return shift;
}

fn vec2(p :&Picture) -> Vec<Vec<Complex<f64>>> {
    let mut v = Vec::new();

    for y in 0..p.sy {
    	let mut vx = Vec::new();
        for x in 0..p.sx {
            let v = get(p, x, y) as f64;
	    let c = Complex::new(f64::exp(v/10.), 0.0);
	    //let c = Complex::new(v, 0.0);
	    vx.append(&mut vec![ c ]);
        }
	v.append(&mut vec![ vx ]);
    }

    v
}

fn mul_conj(i1: Vec<Vec<Complex<f64>>>, i2: Vec<Vec<Complex<f64>>>) -> Vec<Vec<Complex<f64>>>
{
	let sx = i1[0].len();
	let sy = i1.len();

    let mut v = Vec::new();

    for y in 0..sy {
    	let mut vx = Vec::new();
        for x in 0..sx {
	    let v1 = i1[y][x];
	    let v2 = i2[y][x];

	    let c1 = v1.conj() * v2;

	    let c = c1 / c1.norm();
	    vx.append(&mut vec![ c ]);
        }
	v.append(&mut vec![ vx ]);
    }

    v
}

fn argmax(i: Vec<Vec<Complex<f64>>>) -> (isize, isize) {
    let sx = i[0].len();
    let sy = i.len();

    let mut max = -999999999999999999999999999.0;

    let mut mx = 0;
    let mut my = 0;

    for y in 0..sy {
        for x in 0..sx {
	    let v1 = i[y][x];
	    let n = v1.norm();

	    if n > max {
	       println!("{} {} {}", x, y, n);
	       mx = x;
	       my = y;
	       max = n;
	       //	       if ((x > 0) || (y > 0)) { max = n; }
	    }
        }
    }

    let mut rx = mx as isize;
    let mut ry = my as isize;    

    if mx > sx / 2 { rx = rx - sx as isize; }
    if my > sy / 2 { ry = ry - sy as isize; }

    (rx, ry)
}

// https://en.wikipedia.org/wiki/Phase_correlation


fn get_shift_fft(p : &ComputeParams, p1: &Picture, p2: &Picture) -> (isize, isize) {
    println!("Converting...");

    let c1 = vec2(p1);
    let c2 = vec2(p2);

    println!("Transforming...");
    let mut fft = CFft2D::<f64>::with_len(c1.len(), c1[0].len());

    let o1 = fft.forward(&c1);
    let o2 = fft.forward(&c2);

    /* o1 ... complex conjugate */

    let i = mul_conj(o1, o2);

    let o = fft.backward(&i);

    //println!("{:?}", o);

    return argmax(o);
}

// fft is 0.85 sec vs. 4.2 sec

fn predict(p : &ComputeParams, f1: &str, f2: &str, x: isize, y: isize) -> (isize, isize) {
    let p1 = load_fs(&f1);
    let p2 = load_fs(&f2);

    /* OPT */
    let shift = if !DO_FFT { get_shift(p, &p1, &p2) }
    	           else    { get_shift_fft(p, &p1, &p2) };

    save_result(&p2, (x, y), shift, Path::new(r"/tmp/delme_out.png"));

    return shift;
}

fn paint_cross(f2: &str, x: isize, y: isize) {
    let p2 = load_fs(&f2);

    save_result(&p2, (x, y), (0, 0), Path::new(r"/tmp/delme_here.png"));
}

fn test_comp() -> (isize, isize) {
    let fbase = "/data/wnow/chmi/201801/pacz23.z_max3d.20180116.";
    let f1 = fbase.clone().to_owned() + "1230.0.png";
    let f2 = fbase.clone().to_owned() + "1245.0.png";
//    let p = ComputeParams {
//    	progress : tx,
//	do_fft : false,	
//    };

//    predict(&p, &f1, &f2, 321, 301)
      (0, 0)
}

fn curl_download(url: &str, fname: &str) -> Result<(), io::Error> {
    let mut data = Vec::new();
    let mut handle = Easy::new();
    handle.url(url).unwrap();
    println!("Downloading from {}", url);

    {
        let mut transfer = handle.transfer();
        transfer
            .write_function(|new_data| {
                data.extend_from_slice(new_data);
                Ok(new_data.len())
            })
            .unwrap();
        transfer.perform().unwrap();
    }

    let mut f = std::fs::File::create(fname).unwrap();
    f.write(&data).unwrap();

    if SLOW_NET {
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }

    return Ok(());
}

fn worker(p : &ComputeParams, c: &Radar) {
    println!("Working...");

    let (x, y) = c.transform(p.lat, p.lon);
    let shift = predict(&p, "/tmp/delme_ago.png", "/tmp/delme_now.png", x, y);
    println!("Best shift is {:?}", shift);
}

fn main_iteration() {
    for _i in 0..1000 {
        gtk::main_iteration_do(false);
    }
}

struct ComputeParams {
     progress : glib::Sender<ProgressMsg>,
     do_fft : bool,
     lat : f64, lon : f64,
}

enum ProgressMsg {
     Text(String),
     DownloadDone,
     CrossDone,
     ComputeDone,
}

fn compute_print(p : &ComputeParams, t : &str) {
      let e = ProgressMsg::Text(t.to_string());
      // send result to channel
      p.progress.send(e)
            	.expect("Couldn't send data to channel");
}

fn compute_thread(p : ComputeParams) {
    let c = Chmi {};

    compute_print(&p, "Downloading images...");
    download_latest(&c);
    p.progress.send(ProgressMsg::DownloadDone);

    if DO_CROSS {
        compute_print(&p, "Painting cross...");

        let (x, y) = c.transform(p.lat, p.lon);
	paint_cross("/tmp/delme_now.png", x, y);
 	p.progress.send(ProgressMsg::CrossDone);
    }

    compute_print(&p, "Computing future...");
    worker(&p, &c);

    p.progress.send(ProgressMsg::ComputeDone);
    compute_print(&p, "All done.");
}

fn display_latest() {
    gtk::init().unwrap();
    // Create the main window.
    let window = Window::new(WindowType::Toplevel);
    let vbox = Box::new(gtk::Orientation::Vertical, 2);
    let notebook = Notebook::new();
    let close = Button::new_with_label("Close");
    let label = Label::new(Some("Initializing..."));

    close.connect_clicked(|_| {
        // Stop the main loop.
        gtk::main_quit();
    });

    vbox.add(&label);
    vbox.add(&notebook);
    vbox.add(&close);
    window.add(&vbox);

    // Handle closing of the window.
    window.connect_delete_event(|_, _| {
        // Stop the main loop.
        gtk::main_quit();
        // Let the default handler destroy the window.
        Inhibit(false)
    });

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    let mut p = ComputeParams {
    	progress : tx,
	do_fft : false,
	lat : MY_LAT,
	lon : MY_LON,
    };

    {
        let args: Vec<String> = env::args().collect();
	if args.len() > 1 {
	    println!("Have argument {}", args[1]);
	    if args[1] == "brno" { p.lat = 49.25; p.lon = 16.5; }
	    if args[1] == "ruda" { p.lat = 49.13; p.lon = 13.21; }
	}
    }


    thread::spawn(move || {
    	compute_thread(p);
    });

    label.set_text("Downloading...");
    window.show_all();

    rx.attach(None, move |msg| {
        match msg {
	ProgressMsg::Text(t) => label.set_text(&t),
	ProgressMsg::DownloadDone => {
	    let image1 = Image::new_from_file("/tmp/delme_ago.png");
	    let image2 = Image::new_from_file("/tmp/delme_now.png");
	    notebook.add(&image1);
	    notebook.add(&image2);

	    window.show_all();

 	    notebook.set_tab_label_text(&image1, "prev");
	    notebook.set_tab_label_text(&image2, "now");
 	    notebook.set_current_page(Some(1));
	    }
	ProgressMsg::CrossDone => {
 	    let image3 = Image::new_from_file("/tmp/delme_here.png");
	    notebook.add(&image3);
 	    notebook.set_tab_label_text(&image3, "here");

	    window.show_all();
	    notebook.set_current_page(Some(2));
	    }
	ProgressMsg::ComputeDone => {
	    let image3 = Image::new_from_file("/tmp/delme_out.png");

	    notebook.add(&image3);
	    notebook.set_tab_label_text(&image3, "future");

	    label.set_text("ready...");
	    window.show_all();
	    notebook.set_current_page(Some(3));
	    }
	}  

        glib::Continue(true)
    });
	    
    // Run the main loop.
    gtk::main();
}

fn test_time() {
    let now = time::get_time();
    //   let t = time::Tm::new_from_timespec(now);
    println!("Time {:?} {:?}", now, time::now());
    let mut t = time::now();

    t.tm_sec = 0;
    t.tm_min = (t.tm_min / 15) * 15;

    t = t - time::Duration::minutes(10);
    println!(
        "Time {} {}",
        t.asctime(),
        (t + time::Duration::minutes(10)).asctime()
    );

}

fn download_latest(c: &Radar) {
    let mut t = time::now();
    let skip = time::Duration::minutes(c.get_skip() as i64);
    let res = c.get_resolution();
    let delta = time::Duration::minutes(res as i64);

    t = t - skip;
    t.tm_sec = 0;
    t.tm_min = (t.tm_min / res) * res;

    println!("Downloading latest picture");
    if let Err(_e) = curl_download(&c.get_url(&t), "/tmp/delme_now.png") {
        println!("Latest was too optimistic, retry");
        t = t - delta;
        curl_download(&c.get_url(&t), "/tmp/delme_now.png").unwrap();
    }
    t = t - delta;
    println!("Downloading previous picture");
    curl_download(&c.get_url(&t), "/tmp/delme_ago.png").unwrap();
    println!("Downloads done");
}

fn test_fft() {
    let input = [
            vec![
 Complex::new(2.0, 0.0),
 Complex::new(1.0, 1.0),
 Complex::new(0.0, 3.0),
 Complex::new(2.0, 4.0),
 ],
 vec![
 Complex::new(5.0, 0.0),
 Complex::new(3.0, 1.0),
 Complex::new(2.0, 3.0),
 Complex::new(2.0, 8.0),
 ],
 vec![
 Complex::new(2.0, 5.0),
 Complex::new(2.0, 3.0),
 Complex::new(3.0, 7.0),
 Complex::new(2.0, 1.0),
 ],
 vec![
 Complex::new(5.0, 4.0),
 Complex::new(1.0, 2.0),
 Complex::new(4.0, 3.0),
 Complex::new(2.0, 1.0),
 ],
 ];

    let mut fft = CFft2D::<f64>::with_len(input.len(), input[0].len());

    let output = fft.forward(&input);

    println!("the transform of {:?} is {:?}", input, output);
 }

fn main() {
    if true {
	display_latest();
    } else {
        //test_gtk();
        let a = test_comp();
        println!("{:?}", a);
        //test_curl();
        //test_time();
        //test_fft();
    }
}
