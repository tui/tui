import ledd

class Machine(Leds):
    def __init__(m):
        super().__init__()
        m.photo = machine.ADC(0)
        # photo.read()
    
class NightLight(Machine):
    def run(m):
        bri = 0.
        bri2 = 1.
        while True:
            time.sleep(0.1)
            adc = m.photo.read()
            if adc > 100:
                print("fastoff", adc, bri)
                bri = bri * 0.8
                m.white(bri)
                continue
            if bri <= 0.1:
                bri = 0.1
            if bri < 1:
                print("faston", adc, bri)                
                bri = bri * 1.4
                m.white(bri)
                bri2 = 1
                continue
            print("slowoff", adc, bri2)
            bri2 = bri2 * 0.97
            m.white(bri2)

class BlinkComm(Machine):
    def run(m):
        t = time.time()
        p = machine.Pin(12, machine.Pin.OUT)
        for i in range(1000):
            #m.white(1)
            p.value(1)
            time.sleep(.001)
            if m.photo.read() < 60:
                print("Too dark at ", i)
            #m.white(0)
            p.value(0)
            time.sleep(.001)
            if m.photo.read() > 60:
                print("Too bright at ", i)
        print(time.time()-t, "sec")

class RainbowPaper(Machine):
    def run(m, t=0.1):
        r = machine.Pin(15, machine.Pin.OUT)
        g = machine.Pin(12, machine.Pin.OUT)
        b = machine.Pin(13, machine.Pin.OUT)
        while True:
            r.value(1)
            time.sleep(t)
            r.value(0)
            time.sleep(t)            
            r.value(1)
            time.sleep(t)            
            r.value(0)
            time.sleep(t)            
            g.value(1)
            time.sleep(t)            
            g.value(0)
            time.sleep(t)            
            b.value(1)
            time.sleep(t)            
            b.value(0)

time.sleep(3)
print("Starting up")
m = NightLight()
m.run()



p.value(1)
m.photo.read()
