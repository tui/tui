import machine
import time
import network
import socket

# p r g b t1 r g b t2/n

class Leds:
    def __init__(m):
        m.led_r = machine.PWM(machine.Pin(15, machine.Pin.OUT), freq=1000, duty=0)
        m.led_g = machine.PWM(machine.Pin(12, machine.Pin.OUT), freq=1000, duty=0)
        m.led_b = machine.PWM(machine.Pin(13, machine.Pin.OUT), freq=1000, duty=0)

    def set(m, c):
        r, g, b = c
        # print("Set ", c)
        m.led_r.duty(int(r*1023.))
        time.sleep(0.001)
        m.led_g.duty(int(g*1023.))
        time.sleep(0.001)
        m.led_b.duty(int(b*1023.))
        time.sleep(0.001)

class LeddBase(Leds):
    def __init__(m):
        super().__init__()
        m.connect()

#    def connect(m):
#        m.fd = machine.UART(0)
#        m.fd.write("Ledd 0.0 on esp"+chr(13)+chr(10))

#    def getc(m):
#        c = m.fd.read(1)
#        return c

    def readln(m, s):
        if s == None:
            print("readln: None? Why?")
            s = b""
        while True:
            r = m.getc()
            if None != r:
                # print("got: ", r, " so far ", s)
                if ord(r) == 13 or ord(r) == 10:
                    print("Full command is ", s)
                    break
                s = s + r
        return s.decode('ascii')

    def run_pattern(m, cmd, args):
        return b""
        
    def run(m):
        cmd = "p"
        args = [1, 0, 0, 0, 1, 1, 0, 0]
        while True:
            r = m.run_pattern(cmd, args)
            m.set((0, 0, 0.002))
            r = m.readln(r)
            r = r.split(" ")
            cmd = r[0]
            r = r[1:]
            # print("Unprocessed args are", r)
            try:
                args = list(map(float, r))
            except:
                continue
            # print("Command and args:", cmd, args)

class LeddPattern(LeddBase):
    def run_pattern(m, cmd, args):
        print("Processing command ", cmd, "args", args)
        if args == []:
            print("no arguments")
            return b""
        if cmd == "p":
            print("Running pattern")
            return m.run_pattern_graph(args)
        print("Unknown command", cmd)
        return b""

    def interp1(m, old, new, perc):
        res = old + (float(perc) * (float(new)-old))
        return res*1.

    def interp(m, old, new, perc):
        v = [0,0,0]
        for i in range(3):
            v[i] = m.interp1(old[i], new[i], perc)
        return v

    def run_pattern_graph(m, args):
        # From notint.py: GraphTest
        graph = None
        c = (0., 0., 0.)
        while 1:
            if not graph:
                graph = list(map(float, args))
            # print("Running graph: ", graph)                
            time_,r,g,b = graph[:4]
            n = (r, g, b)
            graph = graph[4:]
            t = 0.
            while t < time_:
                r = m.getc()
                if None != r:
                    print("got key")
                    return r

                m.set(m.interp(c, n, t/time_))
                st = 0.1
                t += st
                time.sleep(st)
            c = n
        return b""

class LeddNet(LeddPattern):
    def connect(m):
        print("Connecting..."+chr(10))
        nic = network.WLAN(network.STA_IF)
        nic.active(True)
        nic.connect('openwireless.org', '')
        print(nic.ifconfig())
        addr = socket.getaddrinfo('10.0.0.6', 10007)[0][-1]
        #addr = socket.getaddrinfo('atrey.karlin.mff.cuni.cz', 10007)[0][-1]
        while True:
            sock = socket.socket()
            try:
                m.set((0.002, 0.002, 0))
                print("Connecting...")
                sock.connect(addr)
                break
            except:
                print("Connecting... failed")                
                m.set((0.002, 0, 0))
                time.sleep(60)
        m.set((0, 0.002, 0))            
        sock.setblocking(False)
        sock.send(b'Network connected led 0.18'+chr(10))
        m.fd = sock

    def getc(m):
        try:
            c = m.fd.read(1)
            if c != b'':
                return c
        except:
            pass
        print("Connection failed?")
        m.connect()
        return None
    
d = LeddNet()
d.run()
