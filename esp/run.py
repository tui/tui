#!/usr/bin/python
# Flashing does not seem to work with main.py loaded. One solution is to
# os.remove it before flashing.

# That system is junk :-(.

# Upload code/files to the esp. Files should not contain """ or \.

import os
import time

def sy(s): os.system(s)

class Upload:
    def run_prepare(m):
        sy('sudo chown pavel /dev/ttyUSB0')
        sy('echo "" > /dev/ttyUSB0')
        time.sleep(1)
        sy('echo "" > /dev/ttyUSB0')
        time.sleep(.1)

    def run_old(m):
        m.run_prepare()
        sy('cat ledd.py > /dev/ttyUSB0')
        m.run_post()

    def copy_loop(m):
        while True:
            b = m.i.read(128)
            if not b:
                break
            print("Writing...", len(b))
            m.o.write(b)
            time.sleep(.1)
            #print(m.o.read())
 
    def run(m, s):
        m.run_prepare()
        m.i = open(s)
        m.o = open('/dev/ttyUSB0', 'w+')
        m.copy_loop()
        m.o.close()
        m.run_post()

    def inp(m):
        pass

    def flash(m, s, t):
        m.run_prepare()
        m.i = open(s)
        m.o = open('/dev/ttyUSB0', 'w+')
        m.o.write("f = open('"+t+"', 'w')\n")
        m.inp()
        m.o.write('f.write("""')
        m.inp()
        m.copy_loop()
        m.inp()    
        m.o.write('""")\n')
        m.inp()
        m.o.write('f.close()\n')
        m.inp()
        m.o.close()
        m.run_post()

    def run_post(m):
        time.sleep(.1)
        sy('echo "" > /dev/ttyUSB0')

u = Upload()
#u.run('ledd.py')
u.flash('ledd.py', 'main.py')
    
