import network
import socket

def test_client():
    nic = network.WLAN(network.STA_IF)
    nic.active(True)
    nic.connect('openwireless.org', '')
    print(nic.ifconfig())

def test_ap():
    nic = network.WLAN(network.AP_IF)
    nic.config(essid='tiny AP', channel=1)
    

def test_http():
    addr = socket.getaddrinfo('ctjb.net', 80)[0][-1]
    sock = socket.socket()
    sock.connect(addr)
    sock.send(b'GET /2016 HTTP/1.1\r\nHost: ctjb.net\r\n\r\n')
    data = sock.recv(1024)
    sock.close()
    data = data.decode('ascii')
    print(data)

def test_incoming():
    sock = socket.socket()
    a = socket.getaddrinfo('10.0.0.100', 1234)[0][-1]
    sock.bind(a)
    sock.listen(0)
    s2 = sock.accept()
    conn, addr = s2
    conn.send(b'hello, world\n')
    data = conn.recv(1024)
    data = data.decode('ascii')

def test_outgoing():
    addr = socket.getaddrinfo('10.0.0.6', 1335)[0][-1]
    sock = socket.socket()
    sock.connect(addr)
    sock.send(b'GET /2016 HTTP/1.1\r\nHost: ctjb.net\r\n\r\n')
    data = sock.recv(1024)
    sock.close()
    data = data.decode('ascii')
    print(data)
    

def http_incoming():
    sock = socket.socket()
    a = socket.getaddrinfo('192.168.4.1', 80)[0][-1]
    sock.bind(a)
    sock.listen(0)
    while True:
        s2 = sock.accept()
        conn, addr = s2
        data = conn.recv(1024)
        data = data.decode('ascii')

        conn.send(b'<html><body><h1>Hello world</h1></body></html>\n')
        break

    
