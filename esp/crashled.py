import time
import machine

led_r = machine.PWM(machine.Pin(15, machine.Pin.OUT), freq=1000, duty=0)
led_g = machine.PWM(machine.Pin(12, machine.Pin.OUT), freq=1000, duty=0)
uart = machine.UART(0)
button = machine.Pin(4, machine.Pin.IN)

log = b""
c = 0
while True:
    r = uart.read()
    if r != None:
        led_g.duty(10)
        log = log + r
        log = log[-256:]
        c = 100
    if button.value() == 0:
        led_r.duty(20)
        _ = uart.write(log)
        time.sleep(1)
        led_r.duty(0)
    if c > 0:
        c = c - 1
        if c == 0:
            led_g.duty(0)

