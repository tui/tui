#!/usr/bin/python

import sys
import re
import os

global herd_base, machine

herd_base = "/home/machek/herd/"
machine = "default"

def copy_lines(inf, outf):
    while 1:
        line = inf.readline()
        if line == "": return
        if line == ".\n": return
        outf.write(line)

def paste_file(name):
    try: 
        inf = open(name, "r")
    except:
        return -1
    copy_lines(inf, sys.stdout)
    inf.close()
    return 0

def main_loop():
    global machine
    while 1:
        sys.stdout.flush()
        sys.stderr.flush()
        line = sys.stdin.readline()
        if line == "": return

        if re.match('^PING', line):
            machine = line[5:-1]
            print 'PONG herd server 0.0, welcome ' + machine
        elif re.match('^LOG', line):
            msg = line[4:]
            out = open(herd_base+"m/%s/log" % machine, "w+")
            out.write(msg)
            out.close()
            print 'OK'
        elif re.match('^STATE', line):
            out = open(herd_base+"m/%s/state" % machine, "w+")
            copy_lines(sys.stdin, out)
            out.close()
            print 'OK'
        elif re.match('^CONFIG', line):
            cfg = herd_base+"m/%s/config" % machine
            print "Config: ", cfg
            if not paste_file(cfg):
                os.unlink(cfg)
            print '.'
        elif re.match('^PRINT', line):
            cfg = herd_base+"m/%s/print" % machine
            print "Print: ", cfg
            paste_file(cfg)
            print '.'
        elif re.match('^BYE', line):
            print 'OK goodbye'
            exit(0)
        else:
            print 'ERR unknown command ' + line

print "HELO herd server"
main_loop()
