#!/usr/bin/python

#date '+%s'

import time
import datetime

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
    # Python 2.7
    # return td.total_seconds()

def to_unix(d):
    return total_seconds((d - datetime.datetime(1970, 1, 1)))

s = '930'
ah = int(s) / 100
am = int(s) % 100
print "Alarm at %d:%02d" % (ah, am)

now = datetime.datetime.now()
alarm = now.replace(hour = ah, minute = am, second = 0, microsecond = 0)

print now
print alarm
 
#print help(alarm.now())

if alarm < now:
    alarm += datetime.timedelta(days = 1)

print "Alarm in %.1f hours" % ((to_unix(alarm) - to_unix(now))/(60.*60))
print "Alarm in %.1f seconds" % ((to_unix(alarm) - to_unix(now))/(60.))
# Requires too new python?
print "Alarm in %.1f seconds" % (total_seconds(alarm - now) / 60.)

# d = datetime.fromtimestamp(time.time())
# from datetime import *
# a = timedelta(1)
#d = datetime(2013, 2, 19, 23, 19)
#>>> d
#datetime.datetime(2013, 2, 19, 23, 19)
#>>> d+a
#datetime.datetime(2013, 2, 20, 23, 19)
#>>> d+10*a
#datetime.datetime(2013, 3, 1, 23, 19)
#>>> d+9*a
#datetime.datetime(2013, 2, 28, 23, 19)


