#!/usr/bin/python3

class Machine:
    def __init__(m, name, mac = None, eth = None):
        m.name = name
        m.mac = mac
        m.eth = eth
        

herd_base = "/home/pavel/herd/"
herd_server = "pavel@atrey.karlin.mff.cuni.cz"
herd_machines = [ Machine("xo-6d-61-c0.localdomain"),
                  Machine("Nokia-N900"),
                  Machine("amd", eth = "wls3" ),
                  Machine("duo", eth = "eth2", mac = "f0:de:f1:e3:99:75" ),
                  Machine("hobit", eth = "wlan1" ),
	          Machine("half", mac = "00:27:0e:2a:43:71" ), ]
