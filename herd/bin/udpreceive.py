#!/usr/bin/python2
# Thanks to:
# http://stackoverflow.com/questions/13666789/receiving-udp-broadcast-packets-on-linux

# arecord  | nc -u -b 10.255.255.255  1999
# ./udpreceive.py | aplay

from __future__ import print_function
import socket
import sys

localHost = ''
localPort = 1999

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
s.bind((localHost, localPort))
while True:
    #print('Listening on {0}:{1} for traffic'.format(localHost, localPort))
    data = s.recv(1024)
    sys.stdout.write(data)
    #print('Received: {0}'.format(data))
s.close()
