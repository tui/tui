#!/usr/bin/python
import time
import datetime
import os
import sys
import config

def to_unix(d):
    return (d - datetime.datetime(1970, 1, 1)).total_seconds()

def copy_lines(inf, outf):
    while 1:
        line = inf.readline()
        if line == "": return
        if line == ".\n": return
        outf.write(line)

def age(a):
    d = time.time() - a
    if d < 0: return "future"
    if d < 60: return "%d sec ago" % d
    if d < 3600: return "%d min ago" % (d/60)
    if d < 3600*24: return "%d hours ago" % (d/3600)
    return "%d days ago" % (d/(3600*24))

    
for a in config.herd_machines:
    p = os.getenv("HOME")+"/herd/m/"+a.name+"/"
    print "Path ", p
    try: s = os.stat(p+"state")
    except: continue
    print "*", a.name, ":", age(s.st_mtime)
    sf = open(p+"state")
    copy_lines(sf, sys.stdout)

