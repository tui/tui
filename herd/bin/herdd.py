#!/usr/bin/python

import re
import os
import time
import android
import config

global herd_base

droid = android.Android()
herd_base = config.herd_base

class Config:
    def __init__(self):
        self.interval = 15*60
        self.position = 0
        self.debug = 0
        self.silent_until = 0
        self.gsm_pos = 1
        self.server = config.herd_server

global config

config = Config()


def pcmd(c): return os.popen(c).readline()[:-1]

machine = pcmd("uname -n")

def connect():
    global f_in, f_out, f_err
#    ( f_in, f_out ) = os.popen4(herd_base+"bin/herds.py", bufsize=1)
    ( f_in, f_out ) = os.popen4("ssh " + config.server + " -T herd/bin/herds.py", bufsize=1)
#    ( f_in, f_out ) = os.popen3("echo HELO", bufsize=1)
#    ( f_in, f_out ) = os.popen3("cat > /dev/tty& cat /dev/tty")
#    ( f_in, f_out ) = os.popen3("cat")
#    f_in = sys.stdout
#    f_out = sys.stdin

def expect(what):
    f_in.flush()
#    time.sleep(1)
    line = f_out.readline()
    if config.debug:
        print "Got reply ", line, " expecting ", what
    if re.match("^"+what, line):
        return 0
    if re.match("^ssh: connect to host", line):
        return -1
    while 1:
        print "Unexpected reply from server :" + line
        line = f_out.readline()
        if line == "": exit(1)

def handle_config():
    f_in.flush()
    while 1:
        line = f_out.readline()
        if config.debug:
            print "Got config: ", line
        if line == "":
            print "Empty config?"
            exit(1)
        if line == ".\n":
            return
        if re.match("^Interval: ", line):
            config.interval = int(line.split(" ")[1])
            print "Reconfigured, interval now ", config.interval
        if re.match("^Say: ", line):
            say = line[5:-1]
            print "Say: ", say
            droid.ttsSpeak(say)
        if re.match("^Print: ", line):
            say = line[7:-1]
            print "Print: ", say
        if re.match("^Position: ", line):
            config.position = int(line.split(" ")[1])
        if re.match("^Silent_until: ", line):
            config.silent_until = int(line.split(" ")[1])
            if time.time() < config.silent_until:
                print "Going silent"
                droid.profile('silent')
            else:
                print "Silent_until with time already in past?"
                droid.profile('general')
        if re.match("^Autoupdate", line):
            os.system("scp " +config.server+ ":~/herd/bin/herdd.py .")

def handle_print():
    f_in.flush()
    while 1:
        line = f_out.readline()
        print line[:-1]
        if line == "":
            print "Empty print?"
            exit(1)
        if line == ".\n":
            return
    exit(0)

class State:
    sms = 0
    gsmpos = (0, 0, 0, 0)
    is_phone = 0

    def __init__(self):
        if machine == 'Nokia-N900':
            self.is_phone = 1

    def update(self):
        (_, self.charging, _) = droid.batteryGetPlugType()
        (_, self.sms, _) = droid.smsGetMessageCount(1)
        stat = droid.get_registration_status()
        if stat != 0:
            ( _, lac, cid, mnc, mcc, _, _, _ ) = stat
            self.gsmpos = (mcc, mnc, lac, cid)

class Policy:
    def run(self):
        pass

class MailPolicy(Policy):
    def offlineimap(self):
        #os.system("~/bin/ff")
        pass

    def run(self):
        if state.charging:
            self.offlineimap()

class AmdPolicy(MailPolicy):
    def __init__(self):
        import gsmpos
        self.gsmpos = gsmpos
        self.gsmweb = gsmpos.CellDb()
        self.gsmweb.init_old("/data/pavel/g/tui/loc/tm_all.gcsv", 230, 2, "gsmweb.cz O2" )
        self.logf = open("log.herdd.log", 'a')

    def weather_now(self):
        os.system("cd ~/misc/meteo; ./get-radar")
        #position = pcmd("ssh " + config.server + " 'cat ~/herd/m/Nokia-N900/state | grep ^GSM:'")

        match = re.match(".*\(([0-9]*,[0-9]*,[0-9]*,[0-9]*)\).*", position)
        if not match:
            print "No position?"
            return

        (mcc, mnc, lac, cid) = map(int, match.group(1).split(","))
        id = (mcc, mnc, lac, cid)
        p1 = self.gsmpos.CellPos(self.gsmweb, id)

        tm = time.localtime()
        hour = tm.tm_hour + (tm.tm_min / 60.0)

        self.logf.write("Time %2f (%d)\n" % (hour, time.time()))
        if not p1.found:
            self.logf.write("no position\n")
            return
        self.logf.write("have position %f %f %s\n" % (p1.lat, p1.lon, p1.desc))

        #lines = os.popen("cd ~/sf/wnow; ./nowcast --wwarn=%f,%f" % (p1.lat, p1.lon)).readlines()
        #lines = "\n".join(lines)
        #self.logf.write(lines)
        # lines = map(lambda x: x[:-1], lines)
        # lines = " ".join(lines)

        if hour > 10 and hour < 20:
            os.popen("ssh " + config.server + " 'cat >> ~/herd/m/Nokia-N900/config'", "w").write(lines)

    def run(self):
        self.offlineimap()
        #self.weather_now()
        self.logf.flush()

class MaemoPolicy(MailPolicy):
    def offlineimap(self):
        os.system("debbie ~/bin/ff")

state = State()
if machine == "amd":
    policy = AmdPolicy()
elif machine == "Nokia-N900":
    policy = MaemoPolicy()
elif machine == "duo":
    policy = MailPolicy()
elif machine == "xo-6d-61-c0.localdomain":
    policy = MailPolicy()
else:
    policy = Policy()

def main_loop():
    cycle = 0
    while 1:
        print "Cycle ", cycle, " at ", time.time()
        state.update()
        if config.silent_until and time.time() >= config.silent_until:
            print "turn off silent"
            droid.profile('general')
            config.silent_until = 0
        policy.run()
        connect()
        if expect("HELO") < 0:
            time.sleep(30)
            continue
        f_in.write("PING "+machine+"\n")
        expect("PONG")
        if config.debug:
            print "Ok, server replied."
        f_in.write("STATE\n")
        f_in.write("Cycle: " + str(cycle) + "\n")
        f_in.write("Battery: %d %%\n" % droid.readBatteryPercent())
        f_in.write("Charging: %d\n" % state.charging)
        if machine == 'Nokia-N900':
            (_, sms, _) = droid.smsGetMessageCount(1)
            f_in.write("Unread SMS: %d\n" % state.sms)
        elif machine == 'amd':
            f_in.write("Mail: %s\n" % pcmd("mailcheck"))
        if config.gsm_pos or config.position:
                f_in.write("GSM: (%d,%d,%d,%d)\n" % state.gsmpos)
        if config.position:
            config.position -= 1
            (a, pos, b) = android.getLastKnownLocation()
            if "gps" in pos:
                pos = pos["gps"]
                # Google is able to parse N 50.14 E 14.50 format
                f_in.write("GPS: %f deg %f deg %f ? %f m %f km/h\n" % (pos["latitude"], pos["longitude"], pos["accuracy"], pos["altitude"], 0))
        cycle = cycle + 1
        f_in.write(".\n")
        expect("OK")
        f_in.write("CONFIG\n")
        handle_config()
        f_in.write("PRINT\n")
        handle_print()
        f_in.write("BYE\n")
        expect("OK")
        time.sleep(config.interval)

droid = android.Android()

main_loop()
