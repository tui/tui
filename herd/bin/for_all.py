#!/usr/bin/python
import time
import datetime
import os
import sys
import config

def copy_lines(inf, outf):
    while 1:
        line = inf.readline()
        if line == "": return
        if line == ".\n": return
        outf.write(line)

lines = sys.stdin.readlines()
for a in config.herd_machines:
    p = os.getenv("HOME")+"/herd/m/"+a.name+"/"
    print "Path ", p
    sf = open(p+"config", "a")
    for l in lines:
        sf.write(l)


