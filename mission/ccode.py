#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import sys
import os
import re

names = {}
country = {} 
utc_offset = {}
utc_offset_dst = {}
codes = {}
currency_code = {}

# http://en.wikipedia.org/wiki/ISO_3166-1
# copy and paste into xemacs to get ccode.txt
f = open("ccode.txt")
for l in f.readlines():
    name, c2, c3, cnum, c2a = l.split("\t")
    c2 = c2.lower()

    names[c2] = {}
    names[c2] = name
    #print c2.lower(), name

#print json.dumps(countries)
print len(names)

def generate():
    import os
    import re
    for c in names:
        print c, names[c]
        # https://github.com/siznax/wp
        # python ./wp_info.py "Czech republic"
        os.system('python ~/g/wp/wp_info.py "'+names[c]+'" > cdata/'+c)
        os.system('sleep 1')

#generate()

def parse_one(c, pair):
    try:
        key, val = pair.split("=")
    except:
        return

    if key[0] == '|': key = key[1:]
    if key[0] == ' ': key = key[1:]
    if val == "": return
    if val[-1] == '\n': val = val[:-1]
    if key[-1] == ' ': key = key[:-1]

    # print key, val
    #if key == "cctld":
    #    print c, val
    #if key == "time_zone":
    #    print c, val
    if key == "utc_offset":
        utc_offset[c] = val
    if key == "utc_offset_dst":
        utc_offset_dst[c] = val
    if key == "currency_code":
        currency_code[c] = val

def load():
    for c in names:
        try:
            f = open("cdata/"+c, "r")
        except:
            continue
        country[c] = {}
        for l in f.readlines():
            for p in l.split("|"):
                parse_one(c, p)

load()

for c in names:
    codes[names[c]] = c
#print country
