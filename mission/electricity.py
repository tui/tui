#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import ccode

# http://en.wikipedia.org/w/index.php?title=Mains_electricity_by_country&action=edit&section=6
# http://creativecommons.org/licenses/by-sa/3.0/
wiki_data = """
|-
|[[Afghanistan]]|| C, F || ||align=center| 220 [[Volt|V]] ||align=center| 50&nbsp;[[Hertz|Hz]] || 
|-
|[[Albania]]|| C, F,|| ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Algeria]]|| C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[American Samoa]]|| A, B, F, I || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Andorra]]|| C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Angola]]|| C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Anguilla]]|| A || ||align=center| 110 V ||align=center| 60&nbsp;Hz ||
|-
|[[Antigua and Barbuda]]|| A, B || ||align=center| 230 V ||align=center| 60&nbsp;Hz || 
|-
|[[Argentina]] || C ?,<ref name="World Plugs Ambiguity"/> I || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||Line/neutral reversed compared to Chinese and Australian/NZ Type I   
|-
|[[Armenia]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Aruba]] || A, B, F || ||align=center| 127 V ||align=center| 60&nbsp;Hz ||  
|-
|[[Australia]] || I ||[[AS/NZS 3112|AS/NZS&nbsp;3112]]||align=center| 230 V<ref>AS60038-2000 [[Standards Australia]] – ''Standard Voltages''</ref><ref>[http://electricalconnection.com.au/article/10017796/when-voltage-varies When voltage varies]. Electrical connection (2012-10-22). Retrieved on 2014-05-24.</ref>|| align=center| 50&nbsp;Hz ||  Bathrooms in hotels will often have a type I, C and A socket marked "for shavers only".

Line/neutral reversed compared to Argentinian Type I
|-
|[[Austria]] || C<br/>F ||ÖVE-IG/EN&nbsp;50075<br/>ÖVEÖNORM&nbsp;E&nbsp;8620||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Azerbaijan]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Bahamas]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||  
|-
|[[Bahrain]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Bangladesh]] || C, D, G, K || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Barbados]] || A, B || ||align=center| 115 V ||align=center| 50&nbsp;Hz ||
|-
|[[Belarus]]|| C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Belgium]] || C, E ||NBN&nbsp;C&nbsp;61&nbsp;112-1 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Belize]] || A, B, G || ||align=center| 110 V <br/> 220 V ||align=center| 60&nbsp;Hz ||
|-
|[[Benin]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Bermuda]]  || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Bhutan]] || C, D, F, G, M || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Bolivia]] || A, C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 115 V <br/> 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Bosnia and Herzegovina]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Botswana]] || D, G, M || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Brazil]] || C, N ||NBR&nbsp;14136||align=center| 127 V <br/> 220 V ||align=center| 60&nbsp;Hz || Since Jan 1st 2010 all devices and new buildings must comply with NBR 14136. 
|-
|[[British Virgin Islands]] || A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz || 
|-
|[[Brunei]] || G || ||align=center| 240 V  ||align=center| 50&nbsp;Hz ||
|-
|[[Bulgaria]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Burkina Faso]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Burundi]]  || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Cambodia]] || A, C ?,<ref name="World Plugs Ambiguity"/> G || ||align=center| 230 V ||align=center| 50&nbsp;Hz || Power cords with type A plugs which are rated at only 125 V should not be used.
|-
|[[Cameroon]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Canada]]|| A<br/>B ||NEMA&nbsp;1-15&nbsp;P<br/>NEMA&nbsp;5-15&nbsp;P ||align=center| 120 V ||align=center| 60&nbsp;Hz || 
|-
|[[Cape Verde]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Caribbean Netherlands]]<br/>{{citation needed|date=November 2013}}|| A, B, C || ||align=center| 127 V <br/> 220 V ||align=center| 50&nbsp;Hz, 60&nbsp;Hz || No reliable source found. IEC World Plugs lists only the defunct Netherlands Antilles of which these islands were part.<br/>[[Bonaire]] 127 V, 50&nbsp;Hz, Receptacle is combination of A and C; [[Saba]] and [[St. Eustatius]] 110 V, 60&nbsp;Hz, A, maybe B
|-
|[[Cayman Islands]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Central African Republic]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Chad]] || C, D, E, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Chile]] || C, L || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[China]] || A, I<br/>C ||GB&nbsp;1002 <br/>(?)||align=center| 220 V ||align=center| 50&nbsp;Hz || Most wall outlets simultaneously support Types A and I. Some outlets support Type C as well (the holes in the outlets are flat in the middle and round on the sides) so that either a Type A, a Type C or a Type I (unearthed) plug can be used.{{citation needed|date=December 2013}} (See photo at right.) A second outlet only type I (Earthed) is next to the unearthed multi Type A/C/I outlet. Voltage in China is always 220 V; power cords with type A plugs which are rated at only 125 V should not be used.  Line/neutral reversed compared to Argentinian Type I
|-
|[[Colombia]] || A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz || 
|-
|[[Comoros]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Congo, Republic of the]]|| C, E || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Congo, Democratic Republic of the]]<br/>{{citation needed|date=November 2013}}|| C, D, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Cook Islands]] || I || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Costa Rica]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Côte d'Ivoire]]|| C, E || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Croatia]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Cuba]] || A, B, C || ||align=center| 110 V ||align=center| 60&nbsp;Hz ||
|-
|[[Curaçao]] || A, B,<ref name=curacao>{{cite web|title=Curaçao Electricity|url=http://www.curacao.com/Plan-Your-Trip/What-You-Should-Know/Electricity|publisher=Curaçao Tourist Board|accessdate=13 May 2014}}</ref> C{{citation needed|date=May 2014}} || ||align=center| 127 V ||align=center| 50&nbsp;Hz ||
|-
|[[Cyprus]] || G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Czech Republic]] ||C, E ||ČSN&nbsp;35&nbsp;4516 ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Denmark]]|| C<br/>E, F, K || DS/EN&nbsp;50075<br/>DS&nbsp;60884-2-D1<ref>[http://webshop.ds.dk/groupproduct/18331/M256855/29-electrical-engineering-29-120-electrical-accessories-29-120-30-plugs-socket-outlets-couplers/ds-60884-2-d12011.aspx DS 60884-2-D1:2011]</ref> ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Djibouti]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Dominica]] || D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Dominican Republic]] || A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz ||
|-
|[[Ecuador]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Egypt]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[El Salvador]] || A, B || ||align=center| 115 V ||align=center| 60&nbsp;Hz || 
|-
|[[Equatorial Guinea]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Eritrea]] || C, L || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Estonia]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Ethiopia]] || C, E, F, L || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Falkland Islands]]|| G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Faroe Islands]] || C, E, F, K || ||align=center| 230 V ||align=center| 50&nbsp;Hz || <!-- Same as in Denmark --> 
|-
|[[Fiji]]|| I || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Finland]] || C<br/>F ||SFS-EN&nbsp;50075<br/>SFS&nbsp;5610 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[France]] || C<br/>E ||NF&nbsp;EN&nbsp;50075<br/>NF&nbsp;C&nbsp;61-314||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[French Guiana]] || C, D, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[French Polynesia]]<br/>{{citation needed|date=November 2013}}|| A, B, E || ||align=center| 110 V <br/> 220 V ||align=center| 60&nbsp;Hz, 50&nbsp;Hz ||[[Marquesas Islands]] 50&nbsp;Hz<br/>No reliable source found. IEC World Plugs has no entry.
|-
|[[Gabon]] || C || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Gambia]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Georgia (country)|Georgia]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Germany]] || C<br/>F||DIN&nbsp;VDE&nbsp;0620<br/>DIN&nbsp;49441 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Ghana]] || D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Gibraltar]] || C ?,<ref name="World Plugs Ambiguity"/> G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Greece]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Greenland]] || C, E, F, K || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Grenada]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Guadeloupe]] || C, D, E || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Guam]]|| A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz ||
|-
|[[Guatemala]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Guernsey]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Guinea]] || C, F, K || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Guinea-Bissau]] || C ?<ref name="World Plugs Ambiguity"/> || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Guyana]]|| A, B, D, G || ||align=center| 110 V <br/> 220 V<ref name="History of Guyana Power and Light">[http://www.gplinc.net/about/history History of Guyana Power and Light]. Gplinc.com. Retrieved on 2014-01-01.</ref> ||align=center| 60&nbsp;Hz<br/>50&nbsp;Hz<ref name="History of Guyana Power and Light"/> ||Conversion of 50&nbsp;Hz distribution to 60&nbsp;Hz is ongoing<ref>[http://www.stabroeknews.com/2009/news/local/07/10/gpl-converting-parts-of-city-to-60-hz/ GPL Converting Parts of the City to 60 Hz, retrieved 2009 July 31]. Stabroeknews.com (2009-07-10). Retrieved on 2014-01-01.</ref>
|-
|[[Haiti]] || A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz ||
|-
|[[Honduras]] || A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz ||
|-
|[[Hong Kong]] || G || ||align=center| 220 V ||align=center| 50&nbsp;Hz || 
|-
|[[Hungary]]|| C<br/>F ||MSZ&nbsp;EN&nbsp;50075<br/>MSZ&nbsp;9781-2 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Iceland]]|| C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[India]]|| C, D, M  ||IS&nbsp;1293<ref>[https://law.resource.org/pub/in/bis/S05/is.1293.2005.pdf IS 1293]</ref> ||align=center| 230 V ||align=center| 50&nbsp;Hz ||Many power outlets are universal and accept many plugs without adapter. A combination receptacle for types C, D and M is usually present.{{citation needed|date=November 2013}}
|-
|[[Indonesia]]|| C, F, G || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Iran]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz || 
|-
|[[Iraq]]|| C ?,<ref name="World Plugs Ambiguity"/> D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Ireland]] || G  ||I.S.&nbsp;401<ref>I.S. 401, "Safety requirements for rewirable and non-rewirable 13A fused plugs for normal and rough use having insulating sleeves on live and neutral pins", NSAI (National Standards Authority of Ireland), (1997), Dublin</ref> ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Isle of Man]] || G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Israel]] || C, H, M || ||align=center| 230 V ||align=center| 50&nbsp;Hz || Type C, H, M also used in the [[Palestinian National Authority]] areas.{{citation needed|date=November 2013}}
|-
|[[Italy]]|| C<br/>F, L  ||CEI&nbsp;23-34<br/>CEI&nbsp;23-50||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Jamaica]] || A, B || ||align=center| 110 V   ||align=center| 50&nbsp;Hz ||
|-
|[[Japan]] || A, B ||JIS&nbsp;C&nbsp;8303 ||align=center| 100 V ||align=center|50&nbsp;Hz, 60&nbsp;Hz|| East Japan 50&nbsp;Hz ([[Tokyo]], [[Kawasaki, Kanagawa|Kawasaki]], [[Sapporo, Hokkaido|Sapporo]], [[Yokohama]], and [[Sendai, Miyagi|Sendai]]); West Japan 60&nbsp;Hz ([[Okinawa]], [[Osaka]], [[Kyoto]], [[Kobe]], [[Nagoya]], [[Hiroshima]]).{{citation needed|date=November 2013}} 120 V in military facilities in Okinawa.{{citation needed|date=November 2013}}
|-
|[[Jersey]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Jordan]]|| B, C, D, F, G, J || ||align=center| 230 V ||align=center| 50&nbsp;Hz || Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Kazakhstan]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Kenya]]|| G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Kiribati]] || I || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Kosovo]]<br/>{{citation needed|date=November 2013}}|| C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Kuwait]] || C ?<ref name="World Plugs Ambiguity"/> G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Kyrgyzstan]] || C, F ||  ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Laos]] || C, E, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Latvia]]|| C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Lebanon]]|| A, B, C ?<ref name="World Plugs Ambiguity"/> D, G || ||align=center| 220 V||align=center| 50&nbsp;Hz || Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Lesotho]]|| M || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Liberia]] || A, B, C, E, F || ||align=center| 120 V <br/> 240 V ||align=center|60&nbsp;Hz <br/> 50&nbsp;Hz || Now officially 50&nbsp;Hz{{citation needed|date=November 2013}} Local supplies may vary and may not match the usual voltage for a particular wall socket.{{citation needed|date=November 2013}}
|-
|[[Libya]] || C, D, F, L || ||align=center| 127 V ||align=center| 50&nbsp;Hz || [[Barca]], [[Benghazi]], [[Derna, Libya|Derna]], [[Sabha, Libya|Sabha]] & [[Tobruk]] 230 V.{{citation needed|date=November 2013}}
|-
|[[Lithuania]]|| C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Liechtenstein]] || C, J || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Luxembourg]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Macau]]  || D, F, G, M || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Republic of Macedonia|Macedonia]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Madagascar]] || C, D, E, J, K || ||align=center| 127 V <br/> 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Malawi]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Malaysia]]|| G<ref>[http://www.st.gov.my/images/article/polisi/guidelines/guidelines%20for%20electrical%20wiring%20in%20residential%20buildings%20.pdf] – Energy Commission of Malaysia.</ref> ||MS&nbsp;589 ||align=center| 240 V ||align=center| 50&nbsp;Hz || 
|-
|[[Maldives]]|| D, G, J, K, L || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Mali]] || C, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Malta]] || G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Martinique]] || C, D, E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Mauritania]] || C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Mauritius]]|| C?<ref name="World Plugs Ambiguity"/> G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Mexico]]|| A, B ||NMX-J-163-ANCE ||align=center| 127 V ||align=center| 60&nbsp;Hz || 
|-
|[[Federated States of Micronesia|Micronesia]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Moldova]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Monaco]] || C, D, E, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Mongolia]] || C, E || ||align=center| 220&nbsp;V ||align=center| 50&nbsp;Hz ||
|-
|[[Montenegro]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Montserrat]] || A, B || ||align=center| 120 V <br/> 230 V ||align=center| 60&nbsp;Hz ||Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Morocco]] || C, E || ||align=center| 127&nbsp;V <br/>220&nbsp;V ||align=center| 50&nbsp;Hz ||  
|-
|[[Mozambique]] || C, F, M || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Myanmar]] || C, D, F, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Namibia]] || D, M || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Nauru]] || I || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Nepal]] || C?<ref name="World Plugs Ambiguity"/> D, M || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Netherlands]] || C<br/>F ||EN&nbsp;50075<br/>NEN&nbsp;1020 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[New Caledonia]] || C, F|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[New Zealand]]|| I ||[[AS/NZS 3112|AS/NZS&nbsp;3112]] ||align=center| 230 V ||align=center| 50&nbsp;Hz ||Line/neutral reversed compared to Argentinian Type I
|-
|[[Nicaragua]]|| A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||
|-
|[[Niger]] || A, B, C, D, E, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Nigeria]] || D, G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[North Korea]] || A, C, F || ||align=center| 110 V<br/>220 V  ||align=center| 60&nbsp;Hz <br/>50&nbsp;Hz||
|-
|[[Norway]] || C<br/>F ||NEK&nbsp;EN&nbsp;50075<br/>NEK&nbsp;502 ||align=center| 240 V <br/> ||align=center| 50&nbsp;Hz || 
|-
|[[Oman]] || C?<ref name="World Plugs Ambiguity"/> G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Pakistan]] || C, D, G, M || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Palau]] || A, B ||  || style="text-align:center;"| 120 V || style="text-align:center;"| 60&nbsp;Hz ||
|-
|[[Panama]] || A, B ||  || style="text-align:center;"| 110 V <br/> 120 V ||align=center| 60&nbsp;Hz ||   
|-
|[[Papua New Guinea]] || I || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Paraguay]] || C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Peru]]|| A, B, C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 60&nbsp;Hz || [[Talara]] 110/220 V; some areas 50&nbsp;Hz<ref>Dilwyn Jenkins, ''The Rough Guide to Peru'' 2003 Rough Guides, ISBN 1-84353-074-0, p. 57</ref>{{verify credibility|date=November 2013}}
|-
|[[Philippines]]|| A, B, C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 60&nbsp;Hz  || Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Poland]] || C, E ||BN-88/3064 ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Portugal]]|| C, F  ||NP&nbsp;1260 ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|{{Anchor|Puerto Rico}}[[Puerto Rico]]|| A, B || ||align=center| 120&nbsp;V ||align=center| 60&nbsp;Hz || 
|-
|[[Qatar]]|| D, G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Réunion]] || E || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Romania]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Russia]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||USSR (along with much of Eastern Europe) used [[GOST]] sockets with 4.0&nbsp;mm pins similar to Type C plugs and the 4.8&nbsp;mm standard used by Type E & F.<ref>[http://elec.ru/library/gosts_e71/gost_7396_1-89.pdf ГОСТ 7396.1–89]. Elec.ru (2013-01-30). Retrieved on 2013-02-05.</ref>  
|-
|[[Rwanda]] || C, J || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Saint Helena, Ascension and Tristan da Cunha]]<br/>{{citation needed|date=November 2013}}|| G || ||align=center| 220-240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Saint Martin|St. Martin]] || C, F || ||align=center| 120 V <br/> 220 V ||align=center| 60&nbsp;Hz ||Dutch [[Sint Maarten]] 120 V, 60&nbsp;Hz; French [[Saint-Martin]] 230 V, 60&nbsp;Hz; 
|-
|[[St. Kitts and Nevis]] ||A, B, D, G || ||align=center| 110 V <br/> 230 V ||align=center| 60&nbsp;Hz || 
|-
|[[St. Lucia]] || G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Saint Pierre and Miquelon]]<br/>{{citation needed|date=November 2013}} || E || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[St. Vincent and the Grenadines]] || C, E, G, I, K || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Samoa]] || I || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[San Marino]] || C, F, L || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[São Tomé and Príncipe]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Saudi Arabia]] || A, B<br/>F<br/>G ||SASO&nbsp;2204<br/><br/>SASO&nbsp;2203 ||align=center| 127 V <br/> 220 V ||align=center| 60&nbsp;Hz || 
|-
|[[Senegal]] || C, D, E, K || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Serbia]] || C<br/>F ||JUS&nbsp;N.E3.552<br/>JUS&nbsp;N.E3.553||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Seychelles]] || G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Sierra Leone]] || D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Singapore]] || C ?<ref name="World Plugs Ambiguity"/><br/>G<br/>M ||-<br/>SS&nbsp;145<br/>SS&nbsp;472||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Slovakia]] || C, E ||STN&nbsp;34&nbsp;4516 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Slovenia]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Solomon Islands]] || I, G ||  || style="text-align:center;"| 220 V || style="text-align:center;"| 50&nbsp;Hz ||
|-
|[[Somalia]] || C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[South Africa]] || C, F, M, N ||SANS&nbsp;164 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[South Korea]] || C, F ||KSC&nbsp;8305 ||align=center| 220 V ||align=center| 60&nbsp;Hz ||  
|-
|[[Spain]]  || C, F ||UNE&nbsp;20315 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Sri Lanka]] || D, G, M || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Sudan]] || C ?<ref name="World Plugs Ambiguity"/> D || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Suriname]] || C, F || ||align=center| 127 V ||align=center| 60&nbsp;Hz ||
|-
|[[Swaziland]] || M || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Sweden]] || C<br/>F ||SS-EN&nbsp;50075<br/>SS&nbsp;428&nbsp;08&nbsp;34 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Switzerland]]|| C, J<ref>[http://www.esti.admin.ch/files/elektrische_erzeugnisse/Info_SEV1011_de-fr-it-en.pdf Referenzübersicht des Schweizerischen Starkstrominspektorats] (Information by Swiss Federal Administration)</ref><ref>[http://www.biaonline.com/catalog/pdfs/09/0911.pdf New standard for plugs in Switzerland starting from 2013] (PDF, 191&nbsp;KiB, German/English)</ref>  ||SS&nbsp;SEV&nbsp;1011 ||align=center| 230 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Syria]] || C, E, L || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Taiwan]]  || A, B || ||align=center| 110 V ||align=center| 60&nbsp;Hz || Sockets in older buildings are often unearthed and accept only Type A plugs.
|-
|[[Tajikistan]] || C, F, I || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Tanzania]] || D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Thailand]]|| A, B<br/>C, F<br/>Thai&nbsp;plug ||-<br/>-<br/>TIS&nbsp;166-2549 ||align=center| 220 V ||align=center| 50&nbsp;Hz || Newer buildings and installation use TIS116-2549 sockets.<ref>[https://law.resource.org/pub/th/ibr/th.cs.166.e.2549.pdf TIS 166-2549 (2006): Plugs and socket-outlets for household and similar purposes : plugs and socket-outlets with rated voltage not exceeding 250 ]  (English translation)</ref><ref>[http://app.tisi.go.th/notices/pdf/TIS166-2549.pdf TIS 166-2549 (2006)]  (Original Thai)</ref> Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Timor-Leste]] (East Timor) || C, E, F, I || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[Togo]] || C ?<ref name="World Plugs Ambiguity"/>|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Tonga]] || I || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Trinidad & Tobago]] || A, B || ||align=center| 115 V ||align=center| 60&nbsp;Hz ||
|-
|[[Tunisia]] || C, E || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Turkey]] || C, F || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Turkmenistan]] || B, C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz || Power cords with type A or B plugs which are rated at only 125 V should not be used.
|-
|[[Tuvalu]] || I ||  || style="text-align:center;"| 220 V || style="text-align:center;"| 50&nbsp;Hz ||
|-
|[[Uganda]] || G || ||align=center| 240 V ||align=center| 50&nbsp;Hz ||
|-
|[[Ukraine]] || C, F || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
|-
|[[United Arab Emirates]] || C ?<ref name="World Plugs Ambiguity"/> D, G || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[United Kingdom]] || G ||BS&nbsp;1363 ||align=center| 230 V ||align=center| 50&nbsp;Hz || A "shaver unit"<ref>[http://shop.bsigroup.com/en/ProductDetail/?pid=000000000030189926 BS EN 61558-2-5:2010, standard covering shaver supply units]</ref> accepting [[BS&nbsp;4573|BS 4573]], Type C and 2-pin Type I, (sometimes also type A) is often found in bathrooms. 
|-
|[[United States]] || A<br/>B||NEMA&nbsp;1-15&nbsp;P<br/>NEMA&nbsp;5-15&nbsp;P||align=center| 120 V ||align=center| 60&nbsp;Hz || 
|-
|[[US Virgin Islands]]|| A<br/>B||NEMA&nbsp;1-15&nbsp;P<br/>NEMA&nbsp;5-15&nbsp;P||align=center| 110 V ||align=center| 60&nbsp;Hz || 
|-
|[[Uruguay]] || C, F, I, L  || ||align=center| 230 V ||align=center| 50&nbsp;Hz || 
|-
|[[Uzbekistan]] || C ?<ref name="World Plugs Ambiguity"/> I|| ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Vanuatu]] || C ?<ref name="World Plugs Ambiguity"/> G, I || ||align=center| 220 V ||align=center| 50&nbsp;Hz || 
|-
|[[Venezuela]] || A, B || ||align=center| 120 V ||align=center| 60&nbsp;Hz ||  
|-
|[[Vietnam]] || A, C ?<ref name="World Plugs Ambiguity"/> G || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||  
|-
|[[Yemen]] || A, D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||Power cords with type A plugs which are rated at only 125 V should not be used.
|-
|[[Zambia]] || C ?<ref name="World Plugs Ambiguity"/> D, G || ||align=center| 230 V ||align=center| 50&nbsp;Hz ||
|-
|[[Zimbabwe]] || D, G || ||align=center| 220 V ||align=center| 50&nbsp;Hz ||
"""

success = 0

voltage = {}
hz = {}
style = {}

for line in wiki_data.split("\n"):
    line = re.sub("<br/>", ", ", line)
    #print line
    m = re.match("^\|\[\[([a-zA-Z ]*)\]\] \|\|([A-Z, ]*)[ ]*\|\|.*\|\|align=center\| ([0-9]*) V .*align=center\| ([0-9]*)\&nbsp.Hz.*", line)

    if m:
        #print line
        #print  m.group(1), m.group(2), m.group(3), m.group(4)
        if not m.group(1) in ccode.codes:
            print "Unknown country:", m.group(1)
            continue
        c = ccode.codes[m.group(1)]
        style[c] = m.group(2).rstrip().lstrip()
        voltage[c] = int(m.group(3))
        hz[c] = int(m.group(4))
        success += 1

print "Total ", success
