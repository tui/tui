#!/usr/bin/python
# -*- coding: utf-8 -*-

import ccode

# Schengen area
# http://en.wikipedia.org/wiki/Schengen_area

wiki_schengen_area = """Austria
Belgium
Czech Republic
Denmark
Estonia
Finland
France
Germany
Greece
Hungary
Iceland
Italy
Latvia
Liechtenstein
Lithuania
Luxembourg
Malta
Netherlands
Norway
Poland
Portugal
Slovakia
Slovenia
Spain
Sweden
Switzerland"""

schengen_area = {}
for l in wiki_schengen_area.split("\n"):
    schengen_area[ccode.codes[l]] = 1
