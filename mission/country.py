#!/usr/bin/python

import ccode
import cdata
import electricity
import sys

verbose = 0

def is_power_eu(c1):
    # Plugs around europe are not really same, but most devices in european
    # union are designed for both standards, and there's no need to warn.
    if electricity.style[c1] == "C, E":
        return 1
    if electricity.style[c1] == "C, F":
        return 1
    return 0

def power_diff(c1, c2):
    diff = abs(electricity.voltage[c1] - electricity.voltage[c2])
    if diff > 20 or (verbose and diff):
        print "Different voltage by ", diff, " V."
        return 1
    if electricity.style[c1] != electricity.style[c2]:
        if is_power_eu(c1) and is_power_eu(c2):
            if verbose > 0:
                print "Slightly different power plug, EU-style in both" 
        else:
            print "Different power plug style ('%s', '%s')." % ( electricity.style[c1], electricity.style[c2] )
            return 1
    if electricity.hz[c1] != electricity.hz[c2]:
        print "Different frequency."
    return 0

# The idea is that this should warn about differences between countries.
# If in my home country tap water is generally safe to drink, but not
# in target country, I'd like a message.
def travel(c1, c2):
    need = []
    print "Traveling from ", ccode.names[c1], " to ", ccode.names[c2]
    if not (c1 in ccode.utc_offset and c2 in ccode.utc_offset):
        print "Timezone information not available"
    elif ccode.utc_offset[c1] != ccode.utc_offset[c2]:
        print "You'll cross timezones."
        print ccode.names[c1], " is in ", ccode.utc_offset[c1], "."
        print ccode.names[c2], " is in ", ccode.utc_offset[c2], "."

    if cdata.need_passport(c1, c2):
        need += ["passport"]

    if not (c1 in ccode.currency_code and c2 in ccode.currency_code):
        print "Currency information not available"
    elif ccode.currency_code[c1] != ccode.currency_code[c2]:
        need += ["change money"]

    need += cdata.need_visa(c1, c2)

    if power_diff(c1, c2):
        need += ["power adapter"]

    print "You need", need

def run():
    if len(sys.argv) >= 3:
        travel(sys.argv[1], sys.argv[2])
        return
    if len(sys.argv) == 2:
        travel("cz", sys.argv[1])
        return
    travel("cz", "de")

run()
