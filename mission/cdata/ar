{{Infobox country
|conventional_long_name = Argentine Republic{{efn-ua|name=altnames|Article 35 of the [[Argentine Constitution]] gives equal recognition to the names "United Provinces of the River Plate", "Argentine Republic" and "Argentine Confederation" and authorizes the use of "Argentine Nation" in the making and enactment of laws.{{sfn|Constitution of Argentina|loc=art. 35}}}}
|native_name = {{native name|es|República Argentina}}
|common_name = Argentina
|image_flag = Flag of Argentina.svg
|image_coat = Coat of arms of Argentina.svg
|national_motto = {{native phrase|es|"[[En unión y libertad]]"|nolink=on}}<br/>{{small|"In Unity and Freedom"}}
|national_anthem = {{native name|es|[[Argentine National Anthem|Himno Nacional Argentino]]|nolink=on}}<br/>{{small|Argentine National Anthem}}<br/><center>[[File:United States Navy Band - Himno Nacional Argentino.ogg]]</center>
|image_map = Argentina orthographic.svg
|map_width = 220px
|map_caption = {{resize|110%|Mainland Argentina shown in dark green, with [[#Foreign relations|territorial claims]] shown in light green}}
|capital = [[Buenos Aires]]
|latd=34 |latm=36 |latNS=S |longd=58|longm=23 |longEW=W
|largest_city = capital
|official_languages = [[Spanish language|Spanish]]{{ref label|note-lang|a|}}
|ethnic_groups = {{unbulleted list
}}
|ethnic_groups_year = 2013<ref name=cia/><ref name="onlinelibrary.wiley.com"/>
|demonym = {{unbulleted list
|[[Argentine people|Argentine]]
|[[Argentine people|Argentinian]]
|{{nowrap|[[Argentine people|Argentinean]] {{small|(uncommon)}}}}
}}
|government_type = [[Federal republic|Federal]] [[Presidential system|presidential]] [[constitutional republic]]
|leader_title1 = [[President of Argentina|President]]
|leader_name1 = [[Cristina Fernández de Kirchner]]
|leader_title2 = [[Vice President of Argentina|Vice President]]
|leader_name2 = [[Amado Boudou]]
|leader_title3 = [[Supreme Court of Argentina|Supreme Court President]]
|leader_name3 = [[Ricardo Lorenzetti]]
|legislature = [[Argentine National Congress|Congress]]
|upper_house = [[Argentine Senate|Senate]]
|lower_house = [[Argentine Chamber of Deputies|Chamber of Deputies]]
|sovereignty_type = [[Independence]]
|sovereignty_note = from [[Spanish Empire|Spain]]
|established_event1 = [[May Revolution]]
|established_date1 = 25 May 1810
|established_event2 = [[Argentine Declaration of Independence|Declared]]
|established_date2 = 9 July 1816
|established_event3 = {{nowrap|[[Argentine Constitution|Current constitution]]}}
|established_date3 = 1 May 1853
|area_rank = 8th
|area_magnitude = 1_E12
|area_km2 = 2780400
|area_sq_mi = 1073518
|area_footnote = {{efn-ua|name=area|Area does not include territorial claims in [[Argentine Antarctica#Argentine claim|Antarctica]] (965,597 km{{smallsup|2}}, including the [[South Orkney Islands]]), the [[Falkland Islands]] (11,410 km{{smallsup|2}}), the [[South Georgia Island|South Georgia]] (3,560 km{{smallsup|2}}) and the [[South Sandwich Islands]] (307 km{{smallsup|2}}).<ref name=totalpop>{{cite web|url=http://www.indec.gov.ar/nuevaweb/cuadros/2/e020202.xls|title=Población total por sexo, índice de masculinidad y densidad de población, según provincia. Total del país. Año 2010
|work=Censo Nacional de Población, Hogares y Viviendas 2010|publisher=Instituto Nacional de Estadística y Censos|language=Spanish}}</ref>}}
|percent_water = 1.57
|population_estimate = 41,660,417<ref name=proypop>{{cite web|url=http://www.indec.gov.ar/nuevaweb/cuadros/2/e020301.xls|title=Población por año y sexo. Total del país. Años 1950–2015|work=Serie análisis demográfico nº30|publisher=Instituto Nacional de Estadística y Censos|language=Spanish}}</ref>
|population_estimate_rank =
|population_estimate_year = 2013
|population_census = 40,117,096<ref name=totalpop/>
|population_census_year = 2010
|population_census_rank = 32nd
|population_density_km2 = 14.4
|population_density_rank = 212th
|pop_den_footnote = <ref name=totalpop/>
|GDP_PPP = $793.779 billion<ref name=imf-2>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=24&pr.y=3&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=213&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Argentina |work= World Economic Outlook Database, April 2014|publisher=International Monetary Fund|date=April 2014 |accessdate=28 June 2014}}</ref>
|GDP_PPP_rank = 22nd
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $18,917<ref name=imf-2/>
|GDP_PPP_per_capita_rank = 56th
|GDP_nominal = $404.483 billion<ref name=imf-2/>
|GDP_nominal_rank = 29th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $9,640<ref name=imf-2/>
|GDP_nominal_per_capita_rank = 69th
|Gini_year = 2010
|Gini_change = decrease <!--increase/decrease/steady-->
|Gini = 44.49 <!--number only-->
|Gini_ref = <ref name=gini>{{cite web|url=http://www.indexmundi.com/facts/argentina/gini-index|title=Argentina – GINI index|publisher=Index Mundi – World Bank, Development Research Group}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.811 <!--number only-->
|HDI_ref = <ref name=hdi>{{cite web|url=http://hdrstats.undp.org/en/countries/profiles/ARG.html|title=Argentina Country Profile: Human Development Indicators|work=Report 2013|publisher=United Nations Development Programme|year=2013}}</ref>
|HDI_rank = 45th
|currency = [[Argentine peso|Peso]] ([[Dollar sign|$]])
|currency_code = ARS
|time_zone = [[Time in Argentina|ART]]
|utc_offset = −3
|date_format = dd.mm.yyyy ([[Common Era|CE]])
|drives_on = right{{ref label|note-train|b|}}
|calling_code = [[+54]]
|cctld = [[.ar]]
|footnote_a = {{note|note-lang}}''[[De facto]]'' at all government levels.{{efn-ua|name=es|Though not declared official ''[[de jure]]'', the Spanish language is the only one used in the wording of laws, decrees, resolutions, official documents and public acts.}} In addition, some provinces have official ''[[de jure]]'' languages:
:{{,}}[[Guaraní language|Guaraní]] in [[Corrientes Province]].<ref name=gn>{{cite Argentine law|jur=CN|l=5598|date=22 de octubre de 2004}}</ref>
:{{,}}[[Kom language (South America)|Kom]], [[Moqoit language|Moqoit]] and [[Wichi language|Wichi]], in [[Chaco Province]].<ref name=kom>{{cite Argentine law|jur=CC|l=6604|bo=9092|date=28 de julio de 2010}}</ref>
|footnote_b = {{note|note-train}}Trains ride on left.
}}
