{{Infobox country
|conventional_long_name = Commonwealth of Puerto Rico
|native_name =        {{nowrap|''Estado Libre Asociado de Puerto Rico''}}
|common_name =        Puerto Rico
|image_flag =         Flag of Puerto Rico.svg<!--[?:] Do not change per [[WP:MOSNUM]]-->
|image_coat =         Coat of arms of the Commonwealth of Puerto Rico.svg
|symbol_type =        Coat of arms
|national_motto =     {{native phrase|la|"[[Coat of Arms of Puerto Rico#History|Joannes est nomen eius]]"|italics=off}}
|englishmotto =       "John is his name"
|national_anthem =    {{unbulleted list
| "[[La Borinqueña]]"<br /><center>[[File:United States Navy Band - La Borinqueña.ogg]]<center>
| {{lower|0.2em|"[[The Star-Spangled Banner]]"{{ref label|natlanthem|a|}}|group=n}}
}}
|image_map =          Puerto Rico (orthographic projection).png
|alt_map =            Projection of the Caribbean Sea with Puerto Rico in green
|capital =            [[San Juan, Puerto Rico|San Juan]]
|latd=18 | latm=27 | latNS=N |longd=66 |longm=6 |longEW=W
|largest_city =       capital
|official_languages = {{hlist |[[Puerto Rican Spanish|Spanish]]|[[English in Puerto Rico|English]]}}
|ethnic_groups =      {{unbulleted list
| 75.8% [[White Puerto Rican|White]]
| 12.4% [[African immigration to Puerto Rico|Black]]
|  3.3% [[Multiracial American|Mixed]]
|  0.5% [[Amerind peoples|Amerindian]]
|  0.2% [[Asian American|Asian]]
|  0.1% [[Pacific Islands American|Pacific Islander]]
|  7.8% [[2010 United States Census|Other]]<ref name="2010profile">{{cite web|url=http://www2.census.gov/geo/maps/dc10_thematic/2010_Profile/2010_Profile_Map_Puerto_Rico.pdf |title=2010 Census: Puerto Rico Profile |format=PDF |date= |accessdate=2014-06-26}}</ref>
}}
|demonym =            {{unbulleted list |[[Puerto Rican people|Puerto Rican]] |{{nowrap|[[Puerto Rican people#Boricua|Boricua]] {{small|(colloquial)}}}}}}
|government_type =    [[Unincorporated territories of the United States|Unincorporated territory of the United States]]{{ref label|govttype|b|}}
|leader_title1 =      [[President of the United States|President]]
|leader_name1 =       [[Barack Obama]]
|leader_title2 =      [[Governor of Puerto Rico|Governor]]
|leader_name2 =       [[Alejandro García Padilla]]
|leader_title3 =      [[Resident Commissioner of Puerto Rico|Resident Commissioner]]
|leader_name3 =       [[Pedro Pierluisi]]
|legislature =        [[Legislative Assembly of Puerto Rico|Legislative Assembly]]
|upper_house =        [[Senate of Puerto Rico|Senate]]
|lower_house =        [[House of Representatives of Puerto Rico|House of Representatives]]
|sovereignty_type =   [[Political status of Puerto Rico|Autonomy]]
|sovereignty_note =   within the [[United States]]
|established_event1 = [[Treaty of Paris (1898)|Cession from Spain]]
|established_date1 =  December 10, 1898
|established_event2 = [[Jones–Shafroth Act]]
|established_date2 =  March 2, 1917
|established_event3 = [[Puerto Rican constitutional referendum, 1952|Commonwealth status]]
|established_date3 =  July 25, 1952
|area_rank = 169th
|area_magnitude =     1 E9
|area_km2 =           9,104
|area_sq_mi =         3,515<!--Do not remove per [[WP:MOSNUM]]-->
|percent_water =      1.6
|population_estimate =      3,667,084<ref name=PopEstUS/>
|population_estimate_rank = 130th
|population_estimate_year = 2012
|population_density_km2 =   418
|population_density_sq_mi = 1082
|population_density_rank =  29th
|GDP_PPP =                     $127 billion<ref>[http://data.worldbank.org/indicator/NY.GDP.MKTP.PP.CD/countries/order%3Dwbapi_data_value_2012%20wbapi_data_value%20wbapi_data_value-last?order=wbapi_data_value_2012%20wbapi_data_value%20wbapi_data_value-last&sort=desc&display=default "Gross domestic product 2012, PPP", World Bank, accessed on 11 Aug 2013]</ref>
|GDP_PPP_rank =                72nd
|GDP_PPP_year =                2012
|GDP_PPP_per_capita =          $34,527<ref>[http://data.worldbank.org/indicator/NY.GDP.PCAP.PP.CD?order=wbapi_data_value_2012+wbapi_data_value+wbapi_data_value-last&sort=desc "GDP per capita, PPP (current international $)", World Development Indicators database], World Bank. Database updated on 8 May 2014. Accessed on 10 May 2014.</ref>
|GDP_PPP_per_capita_rank =     29th
|GDP_nominal =                 $103.5 billion<ref name="World Development Indicators">{{cite web |url=http://data.worldbank.org/data-catalog/world-development-indicators |title=World Bank World Development Indicators, July 2013 |publisher=World Bank |accessdate=August 21, 2013}}</ref>
|GDP_nominal_rank =            60th
|GDP_nominal_year =            2012
|GDP_nominal_per_capita =      $23,678<ref name="World Development Indicators"/>
|GDP_nominal_per_capita_rank = 44th
|Gini =                        53.1
|Gini_ref =                    <ref>{{cite web |url=http://www.census.gov/prod/2012pubs/acsbr11-02.pdf |title=Household Income for States: 2010
and 2011 |author=<!--Staff writer(s); no by-line.--> |date=September 2012 |website= |publisher=U.S. Census Bureau |accessdate=May 16, 2014}}</ref>
|Gini_year =                   2011
|HDI =                         0.865
|HDI_ref =                     <ref>[https://www.academia.edu/6582860/An_Approximation_of_Puerto_Ricos_Human_Development_Index_Forthcoming_ An Approximation of Puerto Rico's Human Development Index (Forthcoming)]{{dead link|date=April 2014}}, Ricardo R. Fuentes-Ramírez, Caribbean Studies Journal</ref>
|HDI_rank =                    29th
|HDI_year =                    2012
|currency =           [[United States dollar]] ($)
|currency_code =      USD
|time_zone =          [[Atlantic Standard Time|AST]]
|utc_offset =         -4
|time_zone_DST =      not observed
|utc_offset_DST =     -4
|drives_on =          right
|cctld =              [[.pr]]
|iso3166code =        PR
|calling_code =       [[Telephone numbers in Puerto Rico|+1-787, +1-939]]
|footnote_a =         {{note|natlanthem}} "The Star-Spangled Banner" serves as the national anthem for the United States of America, and its territories.
|footnote_b =         {{note|govttype}} The government of Puerto Rico is a [[Republicanism in the United States|republican]] form of government.<ref name=prconstitution>[http://www.oslpr.org/english/PDF/The%20Constitution%20of%20the%20Commonwealth%20of%20Puerto%20Rico.pdf Constitution of the Commonwealth of Puerto Rico, Article I, Section 2]</ref>
}}
