{{Infobox country
|conventional_long_name = Kingdom of Spain
|native_name = {{native name|es|Reino de España|icon=no}}
|common_name = Spain
|image_flag = Flag of Spain.svg
|image_coat = Escudo de España (mazonado).svg
|image_map = EU-Spain.svg
|map_caption = {{map caption |location_color=dark green |region=[[Europe]] |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green |legend=EU-Spain.svg}}
|image_map2 = 
|national_motto = {{native phrase|la|"[[Plus ultra (motto)|Plus Ultra]]"|italics=off}}<br />{{small|"Further Beyond"}}
|national_anthem = {{native name|es|[[Marcha Real]]}}<br />{{small|''Royal March''}}<br /><center></center>
|official_languages = [[Spanish language|Spanish]]{{efn|The official Spanish language of the State is established in the Section 3 of the [[Spanish Constitution of 1978]] to be Castilian.<ref>{{cite web|url=http://www.lamoncloa.gob.es/IDIOMAS/9/Espana/LeyFundamental/index.htm |title=The Spanish Constitution |publisher=Lamoncloa.gob.es |date= |accessdate=26 April 2013}}</ref> In some [[autonomous communities of Spain|autonomous communities]], [[Catalan language|Catalan]], [[Galician language|Galician]] and [[Basque language|Basque]] are co-official languages. [[Aragonese language|Aragonese]] and [[Asturian language|Asturian]] have some degree of official recognition.}}
|regional_languages = {{hlist |[[Basque language|Basque]] |[[Catalan language|Catalan]] |[[Galician language|Galician]] |[[Occitan language|Occitan]]}}
|languages_type = {{nobold|Partially recognised<br />languages}}
|languages = {{hlist|[[Aragonese language|Aragonese]] |[[Asturian language|Asturian]]}}
|ethnic_groups = {{unbulleted list |87.8% [[Spanish people|Spanish]] |12.2% others {{efn|87% may include Spaniards with immigrant backgrounds}} }}
|ethnic_groups_year = 2011
|demonym = {{hlist |[[Spanish people|Spanish]] |[[Spanish people|Spaniard]]}}
|capital = [[Madrid]]
|latd=40 |latm=26 |latNS=N |longd=3 |longm=42 |longEW=W
|largest_city = capital
|government_type = [[Unitary state|Unitary]]&nbsp;[[Parliamentary system|parliamentary]] [[constitutional monarchy|constitutional]]&nbsp;[[monarchy]]
|leader_title1 = [[Monarchy of Spain|King]]
|leader_name1 = [[Felipe VI]]
|leader_title2 = [[Prime Minister of Spain|Prime Minister]]
|leader_name2 = [[Mariano Rajoy]]
|legislature = [[Cortes Generales|General Courts]]
|upper_house = [[Senate of Spain|Senate]]
|lower_house = [[Congress of Deputies of Spain|Congress of Deputies]]
|sovereignty_type = [[History of Spain|Formation]]
|established_event2 = [[Dynasty|Dynastic]]
|established_date2 = [[Catholic Monarchs|1479]]
|established_event3 = ''[[De facto]]''
|established_date3 = [[Charles I of Spain|1516]]
|established_event4 = ''[[De jure]]''
|established_date4 = [[Nueva Planta Decrees|1715]]
|established_event5 = [[Nation state]]
|established_date5 = [[Spanish Constitution of 1812|1812]]
|established_event6 = {{nowrap|[[Second Spanish Republic|Constitutional democracy]]}}
|established_date6 = [[Spanish Constitution of 1931|1931]]
|established_event7 = {{nowrap|[[Spanish transition to democracy|Current democracy]]}}
|established_date7 = [[Spanish Constitution of 1978|1978]]
|EUseats = 54
|area_km2 = 504,645<ref>{{cite web|title=Anuario estadístico de España 2006. 1ª parte: entorno físico y medio ambiente|url=http://www.ine.es/prodyser/pubweb/anuario06/anu06_01entor.pdf|website=Instituto Nacional de Estadística (Spain)|accessdate=5 June 2014}}</ref>
|area_sq_mi = 195,364 <!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 52nd
|area_magnitude = 1 E11
|percent_water = 1.04
|population_estimate = 46,704,314<ref>{{cite web |url=http://www.ine.es/en/prensa/np788_en.pdf |title=Population Figures at 1 January 2013 |publisher=Instituto Nacional de Estadística (INE) |accessdate=13 August 2013}}</ref>
|population_estimate_year = 2013
|population_estimate_rank = 28th
|population_census = 46,815,916<ref>{{cite web |url=http://www.ine.es/prensa/np756.pdf |title=Censos de Población y Viviendas de 2011 |publisher=Instituto Nacional de Estadística (INE)}} {{es icon}}</ref>
|population_census_year = 2011
|population_density_km2 = 92
|population_density_sq_mi = 240 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 106th
|GDP_PPP_year = 2014
|GDP_PPP = {{nowrap|$1.425 trillion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=74&pr.y=15&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=184&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Spain |publisher=International Monetary Fund |accessdate=27 June 2014}}</ref>}}
|GDP_PPP_rank = 14th
|GDP_PPP_per_capita = $30,637<ref name=imf2/>
|GDP_PPP_per_capita_rank = 33rd
|GDP_nominal = {{nowrap|$1.415 trillion<ref name=imf2/>}}
|GDP_nominal_rank = 13th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $30,432<ref name=imf2/>
|GDP_nominal_per_capita_rank = 28th
|Gini_year = 2011
|Gini_change = 32 <!--increase/decrease/steady-->
|Gini = 34.0 <!--number only-->
|Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.885 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2013_CH.pdf |title=Human Development Report 2013 |year=2013|publisher=UN |accessdate=14 March 2013}}</ref>
|HDI_rank = 23rd
|currency = [[Euro]] ([[Euro sign|€]])
|currency_code = EUR
|time_zone = [[Central European Time|CET]]
|utc_offset = +1
|time_zone_DST = [[Central European Summer Time|CEST]]
|utc_offset_DST = +2<sup>a</sup>
|date_format = dd.mm.yyyy {{small|(Spanish; [[Common Era|CE]])}}
|drives_on = right
|calling_code = [[Telephone numbers in Spain|+34]]
|iso3166code = ES
|cctld = [[.es]]{{efn|The [[.eu]] domain is also used, as it is shared with other [[European Union]] member states. Also, the [[.cat]] domain is used in [[Països Catalans|Catalan-speaking territories]].}}
|footnote_a = Except the [[Canary Islands]], which observe UTC+0 ([[Western European Time|WET]]) and [[Western European Summer Time|UTC+1]] during summer time.
}}
