{{Infobox country
|conventional_long_name = Republic of Guinea-Bissau
|native_name = {{native name|pt|República da Guiné-Bissau|fontsize=70%}}
|common_name = Guinea-Bissau
|image_flag = Flag of Guinea-Bissau.svg
|image_coat = Coat of arms of Guinea-Bissau.svg
|symbol_type = Emblem
|image_map = LocationGuineaBissau.svg
|image_map2 = Guinea-Bissau - Location Map (2013) - GNB - UNOCHA.svg
|national_motto = <br/>{{native phrase|pt|"Unidade, Luta, Progresso"|italics=off|nolink=on}}<br/>{{small|"Unity, Struggle, Progress"}}
|national_anthem = <br/>{{native name|pt|[[Esta é a Nossa Pátria Bem Amada]]|nolink=on}}<br/>{{small|''This is Our Beloved Homeland''}}
|official_languages = [[Portuguese language|Portuguese]]
|regional_languages = [[Guinea-Bissau Creole|Crioulo da Guiné-Bissau]]
|demonym = Bissau-Guinean<ref name="usstate">{{cite web |url=http://www.state.gov/r/pa/ei/bgn/5454.htm |title=Background Note: Guinea-Bissau |date=December 2009 |publisher=US Department of State |accessdate=7 February 2010}}</ref>
|capital = [[Bissau]]
|ethnic_groups =
{{unbulleted list
| 30% [[Balanta people|Balanta]]
| 20% [[Fula people|Fula]]
| 14% [[Manjack people|Manjaca]]
| 13% [[Mandinka people|Mandinga]]
|  7% [[Papel people|Papel]]
| &gt;1% others
}}
|latd=11 |latm=52 |latNS=N |longd=15 |longm=36 |longEW=W
|largest_city = capital
|government_type = [[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]] [[republic]]
|leader_title1 = [[List of Presidents of Guinea-Bissau|President]]
|leader_name1 = [[José Mário Vaz]]
|leader_title2 = {{nowrap|{{small|[[List of Prime Ministers of Guinea-Bissau|Acting Prime Minister]]}}}}
|leader_name2 = {{small|[[Rui Duarte de Barros]]}}
|leader_title3 = ''[[List of Prime Ministers of Guinea-Bissau|Prime Minister-designate]]''
|leader_name3 = ''[[Domingos Simoes Pereira]]''
|legislature = {{nowrap|[[National People's Assembly (Guinea-Bissau)|National People's Assembly]]}}
|area_rank = 136th
|area_magnitude = 1 E9
|area_km2 = 36,125
|area_sq_mi = 13,948
|percent_water = 22.4
|population_estimate = 1,647,000<ref name=unpop>{{cite journal |url=http://www.un.org/esa/population/publications/wpp2008/wpp2008_text_tables.pdf |title=World Population Prospects, Table A.1 |version=2008 revision |format=PDF |publisher=United Nations |author=Department of Economic and Social Affairs Population Division |year=2009 |accessdate=12 March 2009}} Note: According to email information by the Instituto Nacional de Estudos e Pesquisa, Bissau, the preliminary results of the national population census in Guinea-Bissau put the figure at 1,449,230.</ref>
|population_estimate_rank = 148th
|population_estimate_year = 2010
|population_census = 1,345,479
|population_census_year = 2002
|population_density_km2 = 44.1
|population_density_sq_mi = 115.5
|population_density_rank = 154th
|GDP_PPP = $1.931 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=654&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=&pr.x=36&pr.y=17 |title=Guinea-Bissau |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
|GDP_PPP_year = 2012
|GDP_PPP_per_capita = $1,222<ref name=imf2/>
|GDP_nominal = $870 million<ref name=imf2/>
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $551<ref name=imf2/>
|sovereignty_type = [[Guinea-Bissau War of Independence|Independence]] {{nobold|from [[Portugal]]}}
|established_event1 = Declared
|established_date1 = 24 September 1973
|established_event2 = Recognized
|established_date2 = 10 September 1974
|Gini_year = 1993
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 47 <!--number only-->
|Gini_ref = |Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.364 <!--number only-->
|HDI_rank = 164th
|currency = [[West African CFA franc]]
|currency_code = XOF
|country_code =
|time_zone = [[GMT]]
|utc_offset = +0
|time_zone_DST = |utc_offset_DST =
|drives_on = right
|calling_code = [[+245]]
|cctld = [[.gw]]
}}
