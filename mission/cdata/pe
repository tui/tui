{{Infobox country
|conventional_long_name = Republic of Peru
|native_name = {{unbulleted list|item_style=font-size:88%;
|{{native name|es|República del Perú}}
|{{native name|qu|Piruw Ripuwlika}}
|{{native name|ay|Piruw Suyu}}
}}
|common_name = Peru
|image_coat = Escudo nacional del Perú.svg
|image_flag = Flag_of_Peru_%28state%29.svg|20px<!--Do not change without discussion on talk page:-->
|image_map = Peru (orthographic projection).svg
|image_map2= Peru - Location Map (2011) - PER - UNOCHA.svg|national_motto = "Firme y feliz por la unión" <small>(Spanish)<br>"Firm and Happy for the Union"<small>
|national_anthem = {{native name|es|[[National Anthem of Peru|Himno Nacional del Perú]]|nolink=on}}<br/>{{small|''National Anthem of Peru''}}<br/><center>[[File:United States Navy Band - Marcha Nacional del Perú.ogg]]</center>
|other_symbol_type = National [[Seal (device)|seal]]:
|other_symbol = <div style="padding:0.3em;">[[File:Gran Sello de la República del Perú.svg|80px|link=Great Seal of the State]]</div>{{native phrase|es|[[Coat of arms of Peru#Variants#The Great Seal of the State|Gran Sello del Estado]]|nolink=on}}<br />{{small|Great Seal of the State}}
|languages_type = [[Official language]]s<sup>a</sup>
|languages = [[Spanish language|Spanish]] (official) 84.1% [[Quechuan languages|Quechua]] (official) 13% [[Aymara language|Aymara]] (official) 1.7% (2007 Census)
|demonym = Peruvian
|ethnic_groups =  
{{unbulleted list
| 45% [[Indigenous peoples in Peru|Amerindian]]
| 37% [[Mestizo]]
| 15% [[Peruvians of European descent|White]] 
|  2% others<ref name="CIA"/>
}}
| ethnic_groups_year = 2013<ref name="CIA">{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/pe.html| title=Ethnic groups of Perú|publisher=CIA Factbook |accessdate=October 30, 2013}}</ref>
|capital = [[Lima]]
|latd=12 |latm=2.6 |latNS=S |longd=77 |longm=1.7 |longEW=W
|largest_city = [[Lima]]
|government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[Constitution of Peru|constitutional]] [[republic]]
|leader_title1 = [[List of Presidents of Peru|President]]
|leader_name1 = [[Ollanta Humala]]
|leader_title2 = [[Vice President of Peru|Vice President]]
|leader_name2 = [[Marisol Espinoza]]
|leader_title3 = [[Prime Minister of Peru|Prime Minister]]
|leader_name3 = [[René Cornejo]]
|legislature = [[Congress of the Republic of Peru|Congress]]
|sovereignty_type = [[Peruvian War of Independence|Independence]] {{nobold|from [[Spanish Empire|Spain]]}}
|established_event1 = [[Peruvian War of Independence|Declared]]
|established_date1 = July 28, 1821
|established_event2 = [[Battle of Ayacucho|Consolidated]]
|established_date2 = December 9, 1824
|established_event3 = [[Chincha Islands War|Recognized]]
|established_date3 = May 2, 1866
|area_rank = 20th
|area_magnitude = 1 E12
|area_km2 = 1,285,216
|area_sq_mi = 496,225 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 0.41 <!--CIA World Factbook-->
|population_estimate = 30,475,144 <!--UN World Population Prospects-->
|population_estimate_rank = 40th <!--UN World Population Prospects-->
|population_estimate_year = 2013 <!--UN World Population Prospects-->
|population_census = 28,220,764
|population_census_year = 2007
|population_density_km2 = 23 <!--UN World Population Prospects-->
|population_density_sq_mi = 57 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 191st
|GDP_PPP = $368.777&nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?sy=2011&ey=2018&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=52&pr1.y=3&c=273%2C228%2C233%2C293%2C238&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Peru |publisher=International Monetary Fund |accessdate=May 23, 2013}}</ref>
|GDP_PPP_rank = 
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $11,735<ref name=imf2/>
|GDP_PPP_per_capita_rank = 
|GDP_nominal = $216.674&nbsp;billion<ref name=imf2/>
|GDP_nominal_rank = 
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $6,895<ref name=imf2/>
|GDP_nominal_per_capita_rank = 
|Gini_year = 2010
|Gini_change = decrease <!--increase/decrease/steady-->
|Gini = 48.1 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=March 2, 2011}}</ref>
|Gini_rank = 35th
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.741 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2010_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=November 5, 2010}}</ref>
|HDI_rank = 77th
|currency = [[Peruvian nuevo sol|Nuevo sol]]
|currency_code = PEN
|time_zone = [[Time in Peru|PET]]
|date_format = dd.mm.yyyy ([[Common Era|CE]])
|utc_offset = −5
|drives_on = right
|calling_code = [[+51]]
|cctld = [[.pe]]
|footnote_a = [[Quechua language|Quechua]], [[Aymara language|Aymara]] and [[Languages of Peru|other indigenous languages]] are co-official in the areas where they predominate.
}}
