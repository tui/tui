{{Infobox country
|conventional_long_name = New Zealand
|native_name = ''Aotearoa''
|common_name = New Zealand
|image_flag = Flag of New Zealand.svg
|image_coat = Coat of Arms of New Zealand.svg
|image_map = NZL_orthographic NaturalEarth.svg
|map_caption = Location of New Zealand within the [[Realm of New Zealand]]
|national_anthem = 
{{unbulleted list
| "[[God Defend New Zealand]]"<br /><center>[[File:God Defend New Zealand instrumental.ogg]]<center>
| <!---Do not remove "God Save The Queen" as a national anthem (see http://www.mch.govt.nz/anthem/index.html):--->{{lower|0.2em|"[[God Save the Queen]]"{{#tag:ref|"God Save the Queen" is officially a national anthem but is generally used only on regal and vice-regal occasions.<ref>{{cite web |url=http://www.mch.govt.nz/nz-identity-heritage/national-anthems |title=New Zealand's National Anthems |publisher=[[Ministry for Culture and Heritage]] |accessdate=17 February 2008}}</ref><ref name="AnthemProtocol">{{cite web| url=http://www.mch.govt.nz/nz-identity-heritage/national-anthems |title=Protocol for using New Zealand's National Anthems |publisher=Ministry for Culture and Heritage |accessdate=17 February 2008}}</ref>|group=n}}<!--end lower:-->}}
}}
|capital = [[Wellington]]
|latd=41 |latm=17 |latNS=S |longd=174 |longm=27 |longEW=E
|largest_city = [[Auckland]]
|official_languages =
{{unbulleted list
| 95.9% [[New Zealand English|English]]{{#tag:ref|Language percentages add to more than 100% because some people speak more than one language. They exclude unusable responses and those who spoke no language (e.g. too young to talk).<ref name="SpokenLanguage"/>|group=n}}
|  4.2% [[Māori language|Māori]]
| {{nowrap|0.6% [[New Zealand Sign Language|NZ Sign Language]]}}
}}
|ethnic_groups =
{{unbulleted list
|  74.0% [[European New Zealanders|European]]
|  14.9% [[Māori people|Māori]]
|  11.8% [[Asian people|Asian]]
|  7.4% [[Pacific Islander|Pacific peoples]]
|  1.2% Middle Eastern, [[Latin American]], African
|  1.7% Other<ref>{{cite web| url=http://www.stats.govt.nz/Census/2013-census/profile-and-summary-reports/quickstats-about-national-highlights/cultural-diversity.aspx| title=2013 Census - Cultural Diversity| publisher=Statistics New Zealand}}</ref>
}}
|demonym = {{unbulleted list |[[New Zealanders|New Zealander]] |{{nowrap|[[Kiwi (people)|Kiwi]] {{small|(colloquial)}}}}}}
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional monarchy]]
|leader_title1 = [[Monarchy of New Zealand|Monarch]]
|leader_name1 = [[Elizabeth II]]
|leader_title2 = {{nowrap|[[Governor-General of New Zealand|Governor-General]]}}
|leader_name2 = [[Jerry Mateparae]]
|leader_title3 = [[Prime Minister of New Zealand|Prime Minister]]
|leader_name3 = [[John Key]]
|legislature = [[New Zealand Parliament|Parliament]]<br/>{{small|([[House of Representatives of New Zealand|House of Representatives]])}}
|sovereignty_type = [[Independence of New Zealand|Independence]]
|sovereignty_note = from the [[United Kingdom]]
|established_event1 = [[New Zealand Constitution Act 1852]]
|established_date1 = 17 January 1853
|established_event2 = [[Dominion of New Zealand|Dominion]]
|established_date2 = 26 September 1907
|established_event3 = [[Statute of Westminster Adoption Act 1947]]
|established_date3 = 25 November 1947
|established_event4 = [[Constitution Act 1986]]
|established_date4 = 13 December 1986
|area_rank = 75th
|area_magnitude = 1 E11
|area_km2 = 268,021
|area_sq_mi = 103,483 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 1.6{{#tag:ref|The proportion of New Zealand's area (excluding estuaries) covered by rivers, lakes and ponds, based on figures from the New Zealand Land Cover Database,<ref>{{cite web |url=http://www.mfe.govt.nz/issues/land/land-cover-dbase/index.html |title=The New Zealand Land Cover Database |work=New Zealand Land Cover Database 2 |publisher=New Zealand Ministry for the Environment |date=1 July 2009 |accessdate=26 April 2011}}</ref> is (357526 + 81936) / (26821559 – 92499–26033 – 19216) = 1.6%. If estuarine open water, mangroves, and herbaceous saline vegetation are included, the figure is 2.2%.|group=n}}
|population_estimate =4,537,081<ref>{{cite web |url=http://www.stats.govt.nz/tools_and_services/tools/population_clock.aspx |title= Estimated resident population of New Zealand |publisher=Statistics New Zealand |date= 30 January 2014 |accessdate=30 January 2014}}</ref>
|population_estimate_year = June&nbsp;2014
|population_estimate_rank = 123rd
|population_census = 4,242,048<ref>{{cite web |url=http://www.stats.govt.nz/browse_for_stats/population/census_counts/NumberofElectoratesandElectoralPopulations_MR2013Census.aspx |title=Number of Electorates and Electoral Populations: 2013 Census  –  Media Release |work=2013 Census |publisher=Statistics New Zealand |accessdate=7 October 2013}}</ref>
|population_census_year = 2013
|population_density_km2 = 16.5
|population_density_sq_mi = 42.7 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 205th
|GDP_PPP = {{nowrap|$122.193 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=57&pr.y=12&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=196&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=New Zealand |publisher=International Monetary Fund |accessdate=20 April 2012}}</ref><!--end nowrap:-->}}
|GDP_PPP_rank = 
|GDP_PPP_year = 2013
|GDP_PPP_per_capita = $30,493<ref name="imf2"/>
|GDP_PPP_per_capita_rank = 
|GDP_nominal = {{nowrap|$181.3 billion<ref name="imf2"/>}}
|GDP_nominal_rank = 
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $40,481<ref name="imf2"/>
|GDP_nominal_per_capita_rank = 
|Gini_year = 1997
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 36.2 <!--number only-->
|Gini_ref = <ref>{{cite web |url=http://hdrstats.undp.org/en/indicators/161.html |title=Equality and inequality: Gini index |work=[[Human Development Report]] 2009 |publisher=[[United Nations Development Programme]] |accessdate=14 April 2011}}</ref>
|Gini_rank = 
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.919 <!--number only-->
|HDI_ref = <ref>{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Summary.pdf |title=Human Development Report 2013|publisher=United Nations |accessdate=5 May 2013 |page=16}}</ref>
|HDI_rank = 6th
|currency = [[New Zealand dollar]]
|currency_code = NZD
|country_code = NZ
|time_zone = [[Time in New Zealand|NZST]]{{#tag:ref|The Chatham Islands have a [[Chatham Standard Time Zone|separate time zone]], 45 minutes ahead of the rest of New Zealand.|group=n}}
|utc_offset = +12
|time_zone_DST = [[Time in New Zealand|NZDT]]
|utc_offset_DST = +13
|DST_note = (Sep to Apr)
|date_format= dd/mm/yyyy
|drives_on = left
|calling_code = [[+64]]
|cctld = [[.nz]]
}}
