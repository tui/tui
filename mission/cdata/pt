{{Infobox country
|conventional_long_name = Portuguese Republic
|native_name = {{unbulleted list|item_style=font-size:88%;|{{native name|pt|República Portuguesa}}}}
|common_name = Portugal
|image_flag = Flag of Portugal.svg
|image_coat = Coat of arms of Portugal.svg
|national_anthem = {{native name|pt|[[A Portuguesa]]|nolink=yes}}<br/>{{small|"The Portuguese"}}<br/><center>[[File:A Portuguesa.ogg|noicon|center]]</center>
|image_map = EU-Portugal with islands circled.svg
|image_map2 = Portugal - Location Map (2013) - PRT - UNOCHA.svg
|map_caption = {{map caption |location_color=dark green |region=[[Europe]] |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green |legend=Location Portugal EU Europe.png}}
|capital = {{Coat of arms|Lisbon}}
|latd=38 |latm=46 |latNS=N |longd=9 |longm=9 |longEW=W
|largest_city = capital
|official_languages = [[Portuguese language|Portuguese]]
|regional_languages = [[Mirandese language|Mirandese]]<sup>a</sup>
|ethnic_groups = {{unbulleted list |96.87% [[Portuguese people|Portuguese]] |3.13% others}}
|ethnic_groups_year = 2007
|demonym = Portuguese
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]]<br />[[republic]]}}<ref name="constitution">{{cite web |url=http://dre.pt/comum/html/legis/crp.html |title=Constituição da República Portuguesa - D.R.E. (Constitution of the Portuguese Republic)|author= |date=2 April 1976 |publisher=[[Diário da República|Diário da República Electrónico]] |accessdate=16 August 2013 |language=[[Portuguese language|Portuguese]]}}</ref><ref>{{cite book |last=Blanco de Morais |first=Carlos |authorlink= |title=A Constituição Revista — Parte III Constituição da República Portuguesa: Semipresidencialismo "on probation?" |url=http://www.ffms.pt/upload/docs/efc2e52b-75ee-412b-9deb-04076e27c552.pdf |accessdate=16 August 2013 |year=2011 |publisher=Fundação Francisco Manuel dos Santos |location=[[Lisbon|Lisboa]], Portugal |language=[[Portuguese language|Portuguese]] |isbn=978-989-8424-28-0 |pages=65–71 }}</ref><ref name="e-book">{{cite book |last1=Araújo |first1=António |authorlink1= |last2=Blanco de Morais |first2=Carlos |authorlink2= |last3=Campos e Cunha |first3=Luís |authorlink3= |last4=M. Cardoso da Costa |first4=José Manuel |authorlink4= |last5=Costa Lobo |first5=Marina |authorlink5= |last6=Duarte |first6=David |authorlink6= |last7=Duarte |first7=Tiago |authorlink7= |last8=Freire |first8=André |authorlink8= |last9=Garoupa |first9=Nuno |authorlink9= |last10=Gonçalves |first10=Maria Eduarda |authorlink10= |last11=Lomba |first11=Pedro |authorlink11= |last12=Magalhães |first12=Pedro |authorlink12= |last13=Nogueira de Brito |first13=Miguel |authorlink13= |last14=Pita Barros |first14=Pedro |authorlink14= |last15=Sousa |first15=Luís de |authorlink15= |last16=Tavares |first16=José A. |authorlink16= |last17=Vasconcelos Vilaça |first17=Guilherme |authorlink17= |last18=Veiga |first18=Francisco José |authorlink18= |last19=Vieira de Andrade |first19=José Carlos |authorlink19= |editor1-first=Nuno |editor1-last=Garoupa |editor1-link= |editor2-first=Pedro |editor2-last=Magalhães |editor2-link= |editor3-first=Miguel |editor3-last=Poiares Maduro |editor3-link=Miguel Poiares Maduro |editor4-first=José A. |editor4-last=Tavares |editor4-link=|displayeditors=4 |title=A Constituição Revista |url=http://www.estig.ipbeja.pt/~ac_direito/ConstituicaoRevista_Total.pdf |accessdate=16 August 2013 |year=2011 |publisher=Fundação Francisco Manuel dos Santos |location=[[Lisbon|Lisboa]], Portugal |language=[[Portuguese language|Portuguese]] |isbn=978-989-8424-28-0 |page=143 }}</ref><ref>{{cite book |last=Veser |first=Ernst |authorlink= |title=Semi-Presidentialism-Duverger's Concept — A New Political System Model |url=http://www.rchss.sinica.edu.tw/publication/ebook/journal/11-01-1999/11_1_2.pdf |accessdate=20 August 2013 |year=1997 |publisher=Department of Education, School of Education, [[University of Cologne]] |location= |language=[[English language|English]] (mostly), [[Traditional Chinese characters|Traditional Chinese]] (few) |isbn= |pages=39–60 }}</ref>
|leader_title1 = [[President of Portugal|President]]
|leader_name1 = {{nowrap|[[Aníbal Cavaco Silva]]}}
|leader_title2 = {{nowrap|[[Assembly of the Republic (Portugal)|Assembly President]]}}
|leader_name2 = {{nowrap|[[Assunção Esteves]]}}
|leader_title3 = [[Prime Minister of Portugal|Prime Minister]]
|leader_name3 = {{nowrap|[[Pedro Passos Coelho]]}}
|legislature = [[Assembly of the Republic (Portugal)|Assembly of the Republic]]
|sovereignty_type = [[History of Portugal|Formation]]
|sovereignty_note = 
|established_event1 = [[County of Portugal#First county|Foundation]]
|established_date1 = 868
|established_event2 = [[County of Portugal#Second county|Re-founding]]
|established_date2 = 1095
|established_event3 = [[Battle of São Mamede|Sovereignty]]
|established_date3 = 24 June 1128
|established_event4 = [[Kingdom of Portugal|Kingdom]]
|established_date4 = 26 July 1139
|established_event5 = [[Treaty of Zamora|Recognized]]
|established_date5 = 5 October 1143
|established_event6 = [[Manifestis Probatum|Papal recognition]]
|established_date6 = 23 May 1179
|established_event7 = [[5 October 1910 revolution|Republic]]
|established_date7 = 5 October 1910
|established_event8 = [[Carnation Revolution|Redemocratization]]
|established_date8 = 25 April 1974
|established_event9 = [[Portuguese Constitution|Current constitution]]<sup>c</sup>
|established_date9 = 25 April 1976
|established_event10 = {{nowrap|[[Accession of Spain to the European Union|Joined]] the [[European Economic Community|EEC]] (now the<br/>[[European Union]])}}
|established_date10 = 1 January 1986
|area_rank = 111th
|area_magnitude = 1 E10
|area =
|area_km2 = 92,212<ref>{{pt icon}} [http://www.publico.pt/Sociedade/portugal-tem-92212-quilometros-quadrados-por-enquanto-1552831 Público, "Portugal tem 92.212 quilómetros quadrados, por enquanto..."]{{dead link|date=January 2014}}. Accessed on 2 July 2012.</ref>
|area_sq_mi = 35,603
|percent_water = 0.5
|population_estimate = 10,427,301<ref>{{pt icon}} [http://www.ine.pt/xportal/xmain?xpid=INE&xpgid=ine_destaques&DESTAQUESdest_boui=211394338&DESTAQUESmodo=2]. Accessed on 17 June 2014.</ref>
|population_estimate_rank = 83rd
|population_estimate_year = 2013
|population_census = 10,562,178<ref>{{pt icon}} [http://www.ine.pt/scripts/flex_definitivos/Main.html Portugal. Censos 2011] (ine.pt)</ref>
|population_census_year = 2011
|population_density_km2 = 115
|population_density_sq_mi = 298
|population_density_rank = 97th
|GDP_PPP = {{nowrap|$251.451 billion<ref name="imf2">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?sy=2013&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=182&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=&pr.x=68&pr.y=25 |title=Portugal |publisher=International Monetary Fund |accessdate=April 2014 }}</ref><!--end nowrap:-->}}
|GDP_PPP_rank = 51rd
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $23,671<ref name="imf2"/>
|GDP_PPP_per_capita_rank = 46th
|GDP_nominal = {{nowrap|$231.214 billion<ref name="imf2"/>}}
|GDP_nominal_rank = 47th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $21,765<ref name="imf2"/>
|GDP_nominal_per_capita_rank = 38th
|Gini_year = 2011
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 34.2 <!--number only-->
|Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|patron_saint = [[St Anthony of Padua]]<br>[[Saint George]]<br>[[Immaculate Conception]] 
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.816 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Complete.pdf |title=Human Development Report 2013 |publisher=United Nations Development Programme |date= 14 March 2013 |accessdate= 14 March 2013}}</ref>
|HDI_rank = 43rd
|currency = [[Euro]] ([[Euro sign|€]])<sup>b</sup>
|currency_code = EUR
|time_zone = [[Western European Time|WET]]
|utc_offset = +0
|time_zone_DST = [[Western European Summer Time|WEST]]
|utc_offset_DST = +1
|DST_note = Note that the [[Azores]] are in a different time zone.
|antipodes = 
|date_format = {{unbulleted list |dd/mm/yyyy |dd-mm-yyyy}}
|drives_on = [[Right- and left-hand traffic#Right hand traffic|right]]
|calling_code = [[Telephone numbers in Portugal|+351]]
|emergency_telephone_number = [[112 (emergency telephone number)|112]]
<ref>{{cite web|url=https://en.wikipedia.org/wiki/112_%28emergency_telephone_number%29}}</ref>|iso3166code = 
|cctld = [[.pt]]
|footnote_a = [[Mirandese language|Mirandese]], spoken in some villages of the municipality of [[Miranda do Douro]], was officially recognized in 1999 (''Lei n.° 7/99 de 29 de Janeiro''), awarding it an official right-of-use.{{big|<ref>[http://ec.europa.eu/languages/euromosaic/pt1_en.htm#13 The Euromosaic study, Mirandese in Portugal], europa.eu&nbsp;– [[European Commission]] website. Retrieved January 2007. Link updated December 2011</ref>}}<br/>[[Portuguese Sign Language]] is also recognized.
|footnote_b = Before 1999, the [[Portuguese escudo|escudo]].
|footnote_c = Constitution had a recent minor revision in 2004.
}}
