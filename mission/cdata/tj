{{Infobox country
|conventional_long_name = Republic of Tajikistan
|native_name = {{raise|0.1em|{{lang|tg|Ҷумҳурии Тоҷикистон}}}}<br/>''Çumhurii Toçikiston''
|common_name = Tajikistan
|image_flag = Flag of Tajikistan.svg
|image_coat = Coat of arms of Tajikistan.svg
|symbol_type = Emblem
|image_map = Tajikistan (orthographic projection).svg
|image_map2 = Tajikistan - Location Map (2013) - TJK - UNOCHA.svg
|national_motto =
|national_anthem = ''[[Surudi Milli]]''<br/>{{small|''Our beloved country''}}<br/><center>[[File:Tajikistan anthem.ogg]]</center>
|official_languages=[[Tajik language|Tajik]]<ref>''[[Constitution of Tajikistan|Constitution of the Republic of Tajikistan]]'', 6 November 1994, Article 2.</ref>
|regional_languages=[[Russian language|Russian]]<ref name="lenta.ru/news/2011/06/09">{{cite web|url=http://lenta.ru/news/2011/06/09/russian|title=В Таджикистане русскому языку вернули прежний статус|publisher=Lenta.ru|accessdate=13 September 2013}}</ref>
|ethnic_groups =
{{unbulleted list
| 79.9% [[Tajik people|Tajik]]
| 15.3% [[Uzbeks|Uzbek]]
|  1.1% [[Russians|Russian]]
|  2.6% others
}}<ref>{{cite web|title=People and Society|url=https://www.cia.gov/library/publications/the-world-factbook/geos/ti.html|work=Cia World Factbook|publisher=CIA|accessdate=18 May 2014}}</ref>
|ethnic_groups_year = 2000
|capital = [[Dushanbe]]
|latd=38 |latm=33 |latNS=N |longd=68 |longm=48 |longEW=E
|largest_city  = [[Dushanbe]]
|demonym = [[Tajik people|Tajik]]<ref name="CIAPeople"/>
|government_type = [[Dominant-party system|Dominant-party]] [[Semi-presidential system|semi-presidential]] [[republic]]
|leader_title1 = [[President of Tajikistan|President]]
|leader_name1 = [[Emomali Rahmon]]
|leader_title2 = [[Prime Minister of Tajikistan|Prime Minister]]
|leader_name2 = [[Kokhir Rasulzoda]]
|legislature = [[Supreme Assembly (Tajikistan)|Supreme Assembly]]
|upper_house = [[Supreme Assembly (Tajikistan)|National Assembly]]
|lower_house = [[Supreme Assembly (Tajikistan)|Assembly of Representatives]]
|sovereignty_type = [[Dissolution of the Soviet Union|Independence]] {{nobold|from the [[Soviet Union]]}}
|established_event1 = Declared
|established_date1 = 9 September 1991
|established_event2 = Completed
|established_date2 = 25 December 1991
|area_rank = 96th
|area_magnitude = 1 E11
|area_km2 = 143,100
|area_sq_mi = 55,251
|percent_water = 1.8
|population_estimate = 8,000,000<ref name=CIAPeople/>
|population_estimate_rank = 98th
|population_estimate_year = 2013
|population_census = 7 564 500
|population_census_year = 2010
|population_density_km2 = 48.6
|population_density_sq_mi = 125.8
|population_density_rank = 155th
|GDP_PPP = $17,555 million<ref name=imf2>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=38&pr.y=11&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=923&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=|title=Tajikistan profile at|publisher=International Monetary Fund website|accessdate=22 April 2012}}</ref>
|GDP_PPP_rank =
|GDP_PPP_year = 2011
|GDP_PPP_per_capita = $2 247<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $8,572 million<ref name=imf2/>
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $1 041<ref name=imf2/>
|Gini_year = 2004
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 33.59 <!--number only-->
|Gini_ref = |Gini_rank =
|HDI_year = 2013 <!--Please use the year to which the HDI data refers, not the publication year-->
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.622 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdrstats.undp.org/en/countries/country_fact_sheets/cty_fs_TJK.html |title=Human Development Report 2009: Tajikistan |publisher=United Nations |accessdate=2009-10-18}}</ref>
|HDI_rank = 127th
|currency = [[Somoni]]
|currency_code = TJS
|country_code =
|time_zone = [[Tajikistan Time|TJT]]
|utc_offset = +5
|time_zone_DST = |utc_offset_DST =
|drives_on = right
|calling_code = [[+992]]
|cctld = [[.tj]]
<!--
ORPHANED:
|footnote_a = Estimate from State Statistical Committee of Tajikistan, 2008; rank based on UN figures for 2005.  --->
}}
