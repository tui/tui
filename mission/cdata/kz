{{Infobox country
|conventional_long_name = Republic of Kazakhstan
|native_name = {{unbulleted list |<hr/>Қазақстан Республикасы<br/>''Qazaqstan Respublïkası''<br>|<hr/>Республика Казахстан<br/>''Respublika Kazakhstan''}}
|common_name = Kazakhstan
|image_flag = Flag of Kazakhstan.svg
|image_coat = Coat of arms of Kazakhstan.svg
|symbol_type = Emblem
|image_map = Kazakhstan (orthographic projection).svg
|image_map2 = Kazakhstan - Location Map (2013) - KAZ - UNOCHA.svg
|national_motto =
|national_anthem = Менің Қазақстаным<br/>{{transl|kz|''[[Meniñ Qazaqstanım]]''}}<br/>{{small|''My Kazakhstan''}}<br/><center>[[File:Kazakhstan National Anthem 2012.ogg]]</center>
|official_languages = {{unbulleted list |[[Kazakh language|Kazakh]] {{small|(national)}} |[[Russian language|Russian]]{{small|  (1989 - 1995 lingua franca; 1995 - official in public institutions<ref>Article 2 of the Law on languages in the Kazakh SSR (1989 - 1997) and Article 7 of the Constitutions of the Republic of Kazakhstan (1995 - )[http://adilet.zan.kz/eng/docs/K950001000_]</ref>)}}}}
|ethnic_groups =
{{unbulleted list
| 63.6% [[Kazakhs|Kazakh]]
| 23.3% [[Russians|Russian]]
|  2.9% [[Uzbeks|Uzbek]]
|  2.0% [[Ukrainians|Ukrainian]]
|  1.4% [[Uyghurs|Uyghur]]
|  1.2% [[Tatars|Tatar]]
|  1.1% [[Germans|German]]
|  4.5% others
}}
|ethnic_groups_year = 2010<ref>{{Wayback |date=20100628101359 |url=http://www.stat.kz/p_perepis/Pages/n_04_02_10.aspx |title=Итоги переписи населения Республики Казахстан 2009 года}}. stat.kz. 4 February 2010.</ref>
|capital = [[Astana]]
|latd=51 |latm=10 |latNS=N |longd=71 |longm=25 |longEW=E
|largest_city = [[Almaty]]
|demonym = [[Kazakhstanis|Kazakhstani]]<!--This is the demonym for Kazakhstan citizens. Do not change it to Kazakh, which is the demonym for the ethnic group--><ref name="CIA"/>
|government_type = [[Unitary state|Unitary]] [[Dominant-party system|dominant-party]] [[Presidential system|presidential]] [[republic]]
|leader_title1 = [[President of Kazakhstan|President]]
|leader_name1 = [[Nursultan Nazarbayev]]
|leader_title2 = [[Prime Minister of Kazakhstan|Prime Minister]]
|leader_name2 = [[Karim Massimov]]
|legislature = [[Parliament of Kazakhstan|Parliament]]
|upper_house = [[Senate of Kazakhstan|Senate]]
|lower_house = ''[[Mazhilis]]''
|area_rank = 9th
|area_magnitude = 1 E12
|area_km2 = 2,724,900
|area_sq_mi = 1,052,085 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 1.7
|population_estimate = {{nowrap|17,948,816<ref name="CIA">[https://www.cia.gov/library/publications/the-world-factbook/geos/kz.html Kazakhstan]. CIA World Factbook.</ref>}}
|population_estimate_year = {{nowrap|July 2014<ref name="CIA"/>}}
|population_estimate_rank = 62nd
|population_density_km2 = 5.94
|population_density_sq_mi = 15.39 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 227th
|GDP_PPP_year = 2014
|GDP_PPP = $264.894 billion<ref name=IMF>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=60&pr.y=13&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=916&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=Kazakhstan |publisher=International Monetary Fund |accessdate=28 June 2014}}</ref>
|GDP_PPP_rank = 52nd
|GDP_PPP_per_capita = $15,219<ref name=IMF/>
|GDP_PPP_per_capita_rank = 68th
|GDP_nominal_rank = 50th
|GDP_nominal = $216.802 billion<ref name=IMF/>
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $12,456<ref name=IMF/>
|GDP_nominal_per_capita_rank = 59th
|sovereignty_type = [[History of Kazakhstan#Sovereignty and independence|Independence]] from the [[USSR]]
|established_event1 = Declared
|established_date1 = 16 December 1991
|established_event2 = Finalized
|established_date2 = 25 December 1991
|Gini_year = 2008
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 28.8 <!--number only-->
|Gini_ref = <ref>[https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html ''CIA World Factbook: Field listing''], Distribution of family income&nbsp;– Gini index</ref>
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.754 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=15 November 2011}}</ref>
|HDI_rank = 69th
|currency = [[Kazakhstani tenge|Tenge]] ({{Unicode|₸}})
|currency_code = KZT
|country_code = KAZ
|time_zone = [[Time in Kazakhstan|West{{\}}East]]
|utc_offset = [[UTC+05:00|+5]]{{\}}[[UTC+06:00|+6]]
|time_zone_DST = |utc_offset_DST =
|drives_on = right
|calling_code = +7-6xx, +7-7xx
|cctld = {{unbulleted list |[[.kz]] |[[.қаз]]}}
}}
