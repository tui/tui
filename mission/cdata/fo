{{Infobox country
|conventional_long_name = Faroe Islands
|native_name = <!--Do not add languages without discussing on the talk page first-->
{{unbulleted list|item_style=font-size:88%; |{{native name|fo|Føroyar}} |{{native name|da|Færøerne}}}}
|common_name = Faroe Islands
|image_flag = Flag of the Faroe Islands.svg|border|100px
|image_coat = Coat of arms of the Faroe Islands.svg
|image_map = Faroe Islands in its region.svg
|map_caption = Location of the Faroe Islands in Northern Europe.
|image_map2= Faroe Islands-CIA WFB Map.png
|map2_width = 250px
|map_caption2 = Map of the Faroe Islands
|national_anthem = ''[[Tú alfagra land mítt]]''<br />{{small|''Thou, my most beauteous land''}}
|official_languages = {{hlist |[[Faroese language|Faroese]] |[[Danish language|Danish]]<ref name="Tiganes">[http://www.tinganes.fo/Default.aspx?ID= Statistical Facts about the Faroe Islands], 219, The Prime Minister's Office, accessed 13 July 2011.</ref>}}
|capital = {{Coat of arms|Tórshavn}}
|latd=62 |latm=00 |latNS=N |longd=06 |longm=47 |longEW=W
|largest_city = capital
| membership_type = Sovereign state
| membership = {{flag|Kingdom of Denmark}}
|demonym = Faroese
|government_type = [[Parliamentary system|Parliamentary]] {{nowrap|[[constitutional monarchy]]}}
|leader_title1 = [[Monarchy of Denmark|Monarch]]
|leader_name1 = [[Margrethe II of Denmark|Queen Margrethe II]]
|leader_title2 = [[List of Danish High Commissioners in the Faroe Islands|High Commissioner]]
|leader_name2 = [[Dan M. Knudsen]]
|leader_title3 = [[Prime Minister of the Faroe Islands|Prime Minister]]
|leader_name3 = [[Kaj Leo Johannesen]]
|legislature = ''[[Løgting]]''
|sovereignty_type = Formation
|established_event1 = {{nowrap|[[History of the Faroe Islands|Unified with Norway]]{{ref label|aaa|a}}}}
|established_date1 = {{circa|1035}}
|established_event2 = [[Treaty of Kiel]]<br>{{nowrap|(Ceded to Denmark){{ref label|bbb|b}}}}
|established_date2 = 14 January 1814
|established_event3 = {{nowrap|Gained [[home rule]]}}
|established_date3 = 1 April 1948
|established_event4 = Further autonomy
|established_date4 = 29 July 2005<ref>{{cite web|url=http://www.stm.dk/_a_2565.html |title=Den færøske selvstyreordning, about the ''Overtagelsesloven'' (Takeover Act) |publisher=Stm.dk |date= |accessdate=2014-03-14}}</ref>
|area_rank = 180th
|area_magnitude = 1 E9
|area_km2 = 1,399
|area_sq_mi = 540 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 0.5
|population_estimate = 49,709<ref name="CIA Factbook">{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/fo.html |title=Faroe Islands |work=The World Factbook|publisher=[[CIA]] |accessdate=July 2013 }}</ref>
|population_estimate_rank = 206th
|population_estimate_year = July 2013
|population_census = 48,351<ref name="Statistics Faroe Islands">[http://www.hagstova.fo/portal/page/portal/HAGSTOVAN/Statistics_%20Faroe_Islands/Statistics/Population%20and%20elections/Talva%20BRBGDMD_09-10_EN Statistics Faroe Islands], accessed 2 December 2012.</ref>
|population_census_year = 2011
|population_density_km2 = 35.5
|population_density_sq_mi = 91 <!--Do not remove per [[WP:MOSNUM]]-->
|GDP_PPP = $1.642 billion
|GDP_PPP_year = 2008
|GDP_PPP_per_capita = $33,700
|GDP_nominal = $2.45 billion
|GDP_nominal_year = 2008
|GDP_nominal_per_capita = $50,300
|Gini_year = |Gini_change = <!--increase/decrease/steady--> |Gini = <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2008
|HDI_change = <!--increase/decrease/steady-->
|HDI = 0.950 <!--number only-->
|HDI_ref = <ref name="UN Escap">[http://www.unescap.org/pdd/publications/workingpaper/wp_09_02.pdf Filling Gaps in the Human Development Index], United Nations ESCAP, February 2009</ref>
|HDI_rank =
|currency = [[Faroese króna]]{{ref label|ccc|c}}
|currency_code = DKK
|country_code =
|time_zone = [[Western European Time|WET]]
|utc_offset = +0
|time_zone_DST = [[Western European Summer Time|WEST]]
|utc_offset_DST = +1
|drives_on = right
|calling_code = [[+298]]
|cctld = [[.fo]]
|footnote_a = {{note|aaa}} [[Danish monarchy]] reached the Faeroes in 1380 with the reign of [[Olav IV of Norway|Olav IV]] of Norway.
|footnote_b = {{note|bbb}} The Faeroes, [[Greenland]] and [[Iceland]] were formally Norwegian possessions until 1814, as Norway was united with Denmark.
|footnote_c = {{note|ccc}} The currency, printed with Faroese motifs, is issued [[at par]] with the [[Danish krone]], uses the same sizes and standards as Danish coins and [[banknote]]s and incorporates the same security features. Faroese ''krónur'' (singular ''króna'') share the Danish [[ISO 4217]] code "DKK".
}}
