{{Infobox country
|conventional_long_name = Republic of Turkey
|native_name = {{lang|tr|''Türkiye Cumhuriyeti''}}
|common_name = Turkey
|national_motto =|national_motto = {{nowrap|{{lang|tr|''[[Sovereignty unconditionally belongs to the Nation|Egemenlik, kayıtsız şartsız Milletindir]]''}}<ref name="motto of Turkey">{{cite web|url=http://www.tbmm.gov.tr/kultursanat/milli_egemenlik.htm|title=Motto|date=23 February 2013|publisher=Gov.tr|accessdate=23 February 2013}}</ref><!--end nowrap:-->}} {{small|"Sovereignty unconditionally belongs to the Nation"}}
|national_anthem = {{lang|tr|[[İstiklal Marşı]]}}<br>{{small|''Independence March''}}<br><center>[[File:Istiklal Marsi-TSK.ogg]]</center>
|image_flag = Flag of Turkey.svg
|image_coat = TurkishEmblem.svg
|symbol_type = Emblem
|image_map = Turkey (orthographic projection).svg
|capital = [[Ankara]]
|latd=39 |latm=55 |latNS=N |longd=32 |longm=50 |longEW=E
|largest_city = [[Istanbul]]<br>{{small|{{coord|41|1|N|28|57|E|display=inline}}}}
|official_languages = [[Turkish language|Turkish]]    
|demonym = [[Demographics of Turkey|Turkish]]
|ethnic_groups =
{{Unbulleted list
| 70-75% [[Turkish people|Turks]]
| 18% [[Kurdish people|Kurds]]
| 7-12% others
}}
|ethnic_groups_year =2008<ref name="cia">{{cite web|url=https://www.cia.gov/library/publications/the-world-factbook/geos/tu.html |title=Turkey |publisher=The World Factbook |accessdate=9 February 2013}}</ref>
|government_type = [[Unitary state|Unitary]] [[parliamentary system|parliamentary]] [[Constitution of Turkey|constitutional]] [[republic]]
|leader_title1 = [[President of Turkey|President]]
|leader_name1 = [[Abdullah Gül]]
|leader_title2 = [[Prime Minister of Turkey|Prime Minister]]
|leader_name2 = [[Recep Tayyip Erdoğan]]
|leader_title3 = [[List of Speakers of the Parliament of Turkey|Speaker of the Parliament]]
|leader_name3 = [[Cemil Çiçek]]
|leader_title4 = [[List of Presidents of the Constitutional Court of Turkey|President of the Constitutional Court]]
|leader_name4 = [[Haşim Kılıç]]
|legislature = {{nowrap|[[Grand National Assembly of Turkey|Grand National Assembly]]}}
|sovereignty_type = [[Partitioning of the Ottoman Empire|Succession]] {{nobold|to the [[Ottoman Empire]]}}
|established_event1 = {{nowrap|[[Government of the Grand National Assembly|Ankara government]]}}
|established_date1 = 23 April 1920
|established_event2 = {{nowrap|[[Treaty of Lausanne]]}}
|established_date2 = 24 July 1923
|established_event3 = {{nowrap|[[History of Turkey|Declaration of Republic]]}}
|established_date3 =  29 October 1923
|established_event4 = {{nowrap|[[Constitution of Turkey|Current constitution]]}}
|established_date4 =  7 November 1982
|area_km2 = 783562 <!--http://unstats.un.org/unsd/demographic/products/dyb/DYB2003/Table03.pdf UN statistics, page 7 (PDF format) -->
|area_sq_mi = 302535 <!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 37th
|area_magnitude = 1 E11
|percent_water = 1.3
|population_estimate =  76,667,864 <ref name="Population of Turkey">{{cite web|url=http://www.turkstat.gov.tr/HbGetirHTML.do?id=15974|title=The Results of Address Based Population Registration System, 2013|publisher=[[Turkish Statistical Institute]]|date=31 December 2013|accessdate=29 January 2014}}</ref>
|population_estimate_year = 2013
|population_estimate_rank = 18th
|population_density_km2 = 100 <ref name="Population of Turkey"/>
|population_density_sq_mi = 259
|population_density_rank = 108th
|GDP_PPP_year = 2013
|GDP_PPP = $1.426 trillion <ref name="oecdppp">OECD Data [http://stats.oecd.org/index.aspx?queryid=557 GDP, US $, current prices, current PPPs, millions], [[OECD]]. Accessed on 3 May 2013.</ref><ref name="WorldBank">[http://databank.worldbank.org/data/wb/id/8b26dcff The World Bank: World Development Indicators Database. ''Turkey''] Last revised on 7 July 2013.</ref>
|GDP_PPP_rank = 15th
|GDP_PPP_per_capita = $19,080<ref name="WorldBank"/>
|GDP_PPP_per_capita_rank = 54th
|GDP_nominal = $820.827 billion <ref>[http://databank.worldbank.org/data/download/GDP.pdf The World Bank: World Development Indicators Database. ''Gross Domestic Product 2012.''] Last revised on 1 July 2013.</ref>
|GDP_nominal_rank = 17th
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $11,277 <ref name="WorldBank"/>
|GDP_nominal_per_capita_rank = 64th
|Gini_year = 2010
|Gini_change = increase <!--increase/decrease/steady-->
|Gini =  40.2 <!--number only-->
|Gini_ref =<ref name="wb-gini">{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/rankorder/2172rank.html |title=Gini Index |publisher=CIA |accessdate=28 May 2014}}</ref>
|Gini_rank = 56th
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.722 <!--number only-->
|HDI_category = <span style="color:#090;white-space:nowrap;">high</span>
|HDI_ref = <ref name="UNDP">{{cite web|url=http://hdr.undp.org/en/reports/global/hdr2013/|title= The 2013 Human Development Report – "The Rise of the South: Human Progress in a Diverse World"|publisher=[[Human Development Report|HDRO (Human Development Report Office)]] [[United Nations Development Programme]]|pages=144–147|accessdate=2 March 2013}}</ref>
|HDI_rank = 90th
|currency = [[Turkish lira]] ({{Image | Turkish_lira_symbol_black.svg | 10px | link = Turkish lira sign}})
|currency_code = TRY
|time_zone = [[Eastern European Time|EET]]
|utc_offset = +2
|time_zone_DST = [[Eastern European Summer Time|EEST]]
|utc_offset_DST = +3
|date_format = dd/mm/yyyy ([[Anno Domini|AD]])
|drives_on = right
|calling_code = [[Telephone numbers in Turkey|+90]]
|cctld = [[.tr]]
|website = www.turkiye.gov.tr
}}
