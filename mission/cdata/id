{{Infobox country
|conventional_long_name = Republic of Indonesia
|native_name = ''Republik Indonesia''
|common_name = Indonesia
|image_flag = Flag of Indonesia.svg
|image_coat = National emblem of Indonesia Garuda Pancasila.svg
|symbol_type = National emblem
|image_map = Indonesia (orthographic projection).svg
|map_caption = <!--{{map caption |location_color=green |region=[[ASEAN]] |region_color=dark grey |legend=Location Indonesia ASEAN.svg}}-->
|national_motto = "[[Bhinneka Tunggal Ika]]" {{small|([[Old Javanese]])}}<br />{{small|"Unity in Diversity"}}
<br />'''[[Ideology|National ideology]]:''' [[Pancasila (politics)|Pancasila]]<ref>{{cite journal |url=http://countrystudies.us/indonesia/86.htm |publisher=US Library of Congress |title=Indonesia |edition=Country Studies}}</ref><ref name="Vickers">[[#Vickers|Vickers]], p. 117</ref>
|national_anthem = ''[[Indonesia Raya]]''<br />{{small|''Great Indonesia''}}
|official_languages = [[Indonesian language|Indonesian]]
|demonym = [[Demographics of Indonesia|Indonesian]]
|capital = [[Jakarta]]
|latd=6 |latm=10.5 |latNS=S |longd=106 |longm=49.7 |longEW=E
|largest_city = capital
|government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[constitutional republic]]
|leader_title1 = [[President of Indonesia|President]]
|leader_name1 = {{nowrap|[[Susilo Bambang Yudhoyono]]}}
|leader_title2 = [[Vice President of Indonesia|Vice President]]
|leader_name2 = [[Boediono]]
|legislature = [[People's Consultative Assembly]]
|upper_house = [[Regional Representative Council]]
|lower_house = [[People's Representative Council]]
|area_rank = 15th
|area_magnitude = 1 E+12
|area_km2 = 1,904,569 <!--http://unstats.un.org/unsd/demographic/products/dyb/DYB2004/Table03.pdf-->
|area_sq_mi = 735,358 <!--Do not remove per [[WP:MOSNUM]]-->
|area_label = [[Land area|Land]]
|area_label2 = [[Water area|Water (%)]]
|area_data2 = 4.85
|percent_water =
|population_census = 237,424,363<ref name="imf2"/>
|population_census_year = 2011
|population_census_rank = 4th
|population_density_km2 = 124.66
|population_density_sq_mi = 322.87
|population_density_rank = 84th
|GDP_PPP_year = 2013
|GDP_PPP = $1.285 trillion<ref name = "imf2">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/02/weodata/weorept.aspx?pr.x=70&pr.y=2&sy=2013&ey=2013&scsm=1&ssd=1&sort=country&ds=.&br=1&c=536&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=Indonesia |publisher=International Monetary Fund |accessdate=27 October 2013}}</ref>
|GDP_PPP_rank = 15th
|GDP_PPP_per_capita = $5,182<ref name=imf2/>
|GDP_PPP_per_capita_rank = 124th
|GDP_nominal_year = 2013
|GDP_nominal = $867.468 billion<ref name=imf2/>
|GDP_nominal_rank = 16th
|GDP_nominal_per_capita = $3,499<ref name=imf2/>
|GDP_nominal_per_capita_rank = 115th
|Gini_year = 2010
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 35.6 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank =
|HDI_year = 2012
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.629 <!--number only-->
|HDI_ref = <ref name="UNDP">{{cite web |title=Indonesia Country Profile: Human Development Indicators |accessdate=27 April 2013 |year=2012 |url=http://hdrstats.undp.org/en/countries/profiles/IDN.html}}</ref>
|HDI_rank = 121st
|sovereignty_type = [[Indonesian National Revolution|Independence]]
|sovereignty_note = from the [[Netherlands]]
|established_event2 = [[Proclamation of Indonesian Independence|Declared]]
|established_date2 = 17 August 1945
|established_event3 = [[Dutch–Indonesian Round Table Conference|Acknowledged]]
|established_date3 = 27 December 1949
|currency = [[Indonesian rupiah|Rupiah]] (Rp)
|currency_code = IDR
|time_zone = various
|utc_offset = +7 to +9
|time_zone_DST = |utc_offset_DST =
|drives_on = left <!--Note that this refers to the side of the road used, not the seating of the driver-->
|calling_code = [[+62]]
|cctld = [[.id]]
|website = [http://www.indonesia.go.id/en.html indonesia.go.id]
}}
