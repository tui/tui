{{Infobox country
| conventional_long_name = Hong Kong Special Administrative Region of the People's Republic of China<!---- NOTE: This is the established decision on the talk page and is clearly substantiated by numerous Hong Kong government documents. This is also the convention in use in all other languages on Wikipedia. Please refrain from editing. ---->
| native_name ={{nobold|{{lang|zh-hk|中華人民共和國香港特別行政區}}}}
| common_name = Hong Kong
| image_flag = Flag of Hong Kong.svg
| alt_flag = A flag with a white 5-petalled flower design on solid red background
| image_coat = Hong Kong SAR Regional Emblem.svg
| alt_coat = A red circular emblem, with a white 5-petalled flower design in the centre, and surrounded by the words "Hong Kong" and "中華人民共和國香港特別行政區"
| symbol_type = Emblem
| national_anthem = 《義勇軍進行曲》<br />{{small|''[[March of the Volunteers]]''}}
| p1 = Qing dynasty
| flag_p1 = Flag of Hong Kong 1959.svg
| image_map = Hong Kong Skyline viewed from Victoria Peak.jpeg
| map_caption = The Hong Kong evening skyline from [[Victoria Peak]]
| alt_map = A panorama overlooking the skyscrapers of Hong Kong at night, with Victoria Harbour in the background
| image_map2 = LocationHongKong.png
| map2_width = 250px
| alt_map2 = Location of Hong Kong
| languages_type = '''[[Official language]]s'''
| languages = <!---- DO NOT ADD CANTONESE, AND DON'T EVER CHANGE THE POSITION OF THE OFFICIAL LANGUAGES ---> {{hlist|style=white-space:nowrap; |[[Chinese language|Chinese]]<ref>Section 3(1) of the Official Languages Ordinance (Cap 5) provides that the "English and Chinese languages are declared to be the official languages of Hong Kong." The Ordinance does not explicitly specify the standard for "Chinese". While [[Standard Chinese|Mandarin]] and [[Simplified Chinese character]]s are used as the spoken and written standards in [[mainland China]], [[Standard Cantonese|Cantonese]] and [[Traditional Chinese character]]s are the long-established ''de facto'' standards in Hong Kong.</ref> |English}}
| languages_sub = yes
|languages2_type = [[Writing system]]s
|languages2 = {{unbulleted list | |[[Traditional Chinese characters|Traditional Chinese]]|[[Latin alphabet]] }}
|languages2_sub = yes
| demonym = [[Hong Konger]] / 香港人
| latd=22 |latm=16.7 |latNS=N |longd=114 |longm=09.533 |longEW=E
| leader_title1 = [[Chief Executive of Hong Kong|Chief Executive]]<!--- DO NOT insert Head of State of PRC here; HK is not a sovereign state and the Hong Kong Government's website (http://www.gov.hk) indicates there is no such position as Head of State of Hong Kong. --->
| leader_name1 = [[CY Leung|Leung Chun-ying]]
| leader_title2 = {{nowrap|[[Chief Secretary for Administration|Chief Secretary <br />for Administration]]}}
| leader_name2 = [[Carrie Lam (politician)|Carrie Lam]]
| leader_title3 = [[Financial Secretary (Hong Kong)|Financial Secretary]]
| leader_name3 = [[John Tsang]]
| leader_title4 = [[Secretary for Justice (Hong Kong)|Secretary for Justice]]
| leader_name4 = [[Rimsky Yuen]]
| legislature = [[Legislative Council of Hong Kong|Legislative Council]]
| government_type = [[Special administrative region|Special Administrative Region of the People's Republic of China]]
| location_of_the_government_headquarter = [[Central, Hong Kong|Central]]
| sovereignty_type = [[History of Hong Kong|Establishment]]
| established_event1 = [[Treaty of Nanking]]
| established_date1 = 29 August 1842
| established_event2 = {{nowrap|[[Japanese occupation of Hong Kong|Japanese occupation]]}}
| established_date2 = {{nowrap|25 December 1941 <br />to 15 August 1945}}
| established_event3 = [[Transfer of sovereignty over Hong Kong|Transfer from British to Chinese sovereignty]]
| established_date3 = <br />1 July 1997
| area_magnitude = 1 E9
| area_km2 = 1,104
| area_rank = 179th
| area_sq_mi = 426 <!-- Do not remove as per WP:MOSNUM -->
| percent_water = 4.58 (50&nbsp;km{{smallsup|2}}; 19&nbsp;mi{{smallsup|2}})<ref name="cia"/>
|population_estimate = 7,184,000<ref>{{cite news|url=http://censtatd.gov.hk/press_release/pressReleaseDetail.jsp?charsetID=1&pressRID=3159 |title=Mid-year Population for 2013 |work=[[Census and Statistics Department (Hong Kong)]]|date=13 August 2013}}</ref>
|population_estimate_rank = 100th
|population_estimate_year = 2013
| population_census =
| population_density_rank =
| population_census_year =
| population_density_km2 = 6,544<ref name="census1"/>
| population_density_sq_mi = 17,024 <!-- Do not remove as per WP:MOSNUM -->
| GDP_PPP = $404.892 billion<ref name="imf2"/>
| GDP_PPP_rank = 35th
| GDP_PPP_year = 2014
| GDP_PPP_per_capita = $55,383<ref name="imf2"/>
| GDP_PPP_per_capita_rank = 7th
| GDP_nominal = $302.814 billion<ref name="imf2"/>
| GDP_nominal_rank = 39th
| GDP_nominal_year = 2014
| GDP_nominal_per_capita = $41,421<ref name="imf2"/>
| GDP_nominal_per_capita_rank = 25th
| Gini_year = 2007
| Gini_change =  <!-- increase/decrease/steady -->
| Gini = 43.4 <!-- number only --><!--- DO NOT USE CIA World Factbook. The Gini index is a parameter in calculating the HDI so the Gini index and the HDI should be from the same source, i.e. Human Development Report 2009. --->
| Gini_ref = <ref>{{cite web |url=http://hdrstats.undp.org/en/indicators/161.html|title=Human Development Report 2009 – Gini Index |publisher=[[United Nations Development Programme]] |accessdate=10 November 2009}}</ref>
| Gini_rank =
| HDI_year = 2013
| HDI_change = steady <!-- increase/decrease/steady -->
| HDI = 0.906 <!-- number only -->
| HDI_ref = <ref name="2013 Complete">{{cite web|url=http://hdr.undp.org/en/media/HDR_2013_EN_complete.pdf |title=&#124; Human Development Reports |publisher=Hdr.undp.org |date= |accessdate=2014-05-14}}</ref>
| HDI_rank = 13th
| currency = [[Hong Kong dollar]]
| currency_code = HKD
| drives_on = left
| driver's_seat_on = right
| time_zone = [[Hong Kong Time|HKT]]
| utc_offset = +8
| date_format = {{unbulleted list |{{nowrap|yyyy年m月d日 {{small|(Chinese)}}}} |dd-mm-yyyy {{small|(English)}}}}
| cctld = {{unbulleted list |[[.hk]] |[[.香港]]}}
| calling_code = [[+852]]
| nicknames = "HK", "The 852"
}}
