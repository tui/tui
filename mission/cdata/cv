{{Infobox country
|conventional_long_name = Republic of Cabo Verde
|native_name = ''República de Cabo Verde''
|common_name = Cape Verde<!--This field is not visible to the reader, it provides codes for page links. It should not be changed to Cabo Verde unless the actual pages are moved.-->
|image_flag = Flag of Cape Verde.svg
|image_coat = Coat of arms of Cape Verde.svg
|symbol_type = National emblem
|image_map = Cape Verde (orthographic projection).svg
|map_caption = Location of Cape Verde (circled).
|image_map2 = Cape Verde - Location Map (2013) - CPV - UNOCHA.svg
|national_anthem = {{native name|pt|[[Cântico da Liberdade]]}}<br/>{{small|''Song of Freedom''}}
|official_languages = [[Portuguese language|Portuguese]]
|regional_languages = [[Cape Verdean Creole]]
|capital = [[Praia]]
|latd=14 |latm=55 |latNS=N |longd=23 |longm=31 |longEW=W
|largest_city = capital
|demonym = Cape Verdean
|ethnic_groups =
{{unbulleted list
|80% Mixed [[Creole peoples|Creole]]
|15% [[Black African]]
|5% White
}}
|government_type = [[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]] [[republic]]<ref name="SP-L">{{cite web |author=Octávio Amorim Neto; Marina Costa Lobo |year= 2010 |url=http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1644026 |title=Between Constitutional Diffusion and Local Politics: Semi-Presidentialism in Portuguese-Speaking Countries |publisher=[[Social Science Research Network]] |pages= |accessdate=June 6, 2014}}</ref>
|leader_title1 = [[President of Cape Verde|President]]
|leader_name1 = [[Jorge Carlos Fonseca]]
|leader_title2 = [[National Assembly (Cape Verde)|Assembly President]]
|leader_name2 = [[Basílio Ramos]]
|leader_title3 = [[Prime Minister of Cape Verde|Prime Minister]]
|leader_name3 = [[José Maria Neves]]
|legislature = [[National Assembly (Cape Verde)|National Assembly]]
|area_rank = 172nd
|area_magnitude = 1 E9
|area_km2 = 4033
|area_sq_mi = 1,557 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = negligible
|population_estimate = 512,096
|population_estimate_rank = 167th <!--UN WPP-->
|population_estimate_year = 2013
|population_density_km2 = 123.7
|population_density_sq_mi = 325.0 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 89th
|GDP_PPP = {{nonwrap|$2.305 billion}}<ref name=IMF_GDP>{{cite web http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=42&pr.y=15&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=624&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title = Cape Verde |publisher = International Monetary Fund |accessdate = 2013-04-17 }}</ref>
|GDP_PPP = $2.270 billion
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $4,482.595
|GDP_nominal = $2.071 billion
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $4,089.532
|sovereignty_type = Independence
|established_event1 = from [[Portugal]]
|established_date1 = 5 July 1975
|Gini_year = 2002
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 50.5
|Gini_ref = <ref name=gini-index>{{cite web |title = GINI index |url = http://data.worldbank.org/indicator/SI.POV.GINI?order=wbapi_data_value_2002+wbapi_data_value+wbapi_data_value-last&sort=asc&page=2 |publisher = World Bank |accessdate = 26 July 2013 }}</ref>
|Gini_rank = 
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.586 <!--number only-->
|HDI_ref = <ref name=HDI>{{cite web |url = http://hdr.undp.org/en/media/HDR_2011_EN_Summary.pdf |title = Human Development Report 2011 - Summary |page = 19 |publisher = The United Nations |accessdate = 2011-11-03 }}</ref>
|HDI_rank = 132nd
|currency = [[Cape Verdean escudo]]
|currency_code = CVE
|country_code = CPV
|time_zone = [[Cape Verde Time|CVT]]
|utc_offset = -1
|time_zone_DST = ''not observed''
|utc_offset_DST = -1
|drives_on = right
|calling_code = [[+238]]
|cctld = [[.cv]]
}}
