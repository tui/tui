{{Infobox country
|conventional_long_name = {{lang|en|Democratic Socialist Republic of {{nowrap|Sri Lanka}}}}
|native_name = {{lang|si|ශ්‍රී ලංකා ප්‍රජාතාන්ත්‍රික සමාජවාදී ජනරජය}}<br /><small>{{lang|ta|இலங்கை ஜனநாயக சோசலிசக் குடியரசு}}</small>
|common_name = Sri Lanka
|image_flag = Flag of Sri Lanka.svg
|image_coat = Emblem of Sri Lanka.svg
|symbol_type = Emblem
|national_motto =
|national_anthem = ''[[Sri Lanka Matha]]''<br />{{small|''Mother Sri Lanka''}}
|image_map = Sri Lanka (orthographic projection).svg
|image_map2 = Sri Lanka relief map.jpg
|capital = {{nowrap|[[Sri Jayawardenepura Kotte]] <br /> {{small|(Administrative)}}}} <br> {{nowrap|[[File:Colombo MC logo.png|15px|link=Colombo]] [[Colombo]] {{small|(Commercial)}}}}
|latd=6 |latm=56 |latNS=N |longd=79 |longm=50 |longEW=E
|largest_city = [[File:Colombo MC logo.png|15px|link=Colombo]] [[Colombo]]
|official_languages = {{hlist |[[Sinhala language|Sinhala]] |[[Tamil language|Tamil]]}}
|languages_sub = yes
|languages2_type = Recognized
|languages2 = [[English language|English]]
|demonym = Sri Lankan
|ethnic_groups = |ethnic_groups_year =
|government_type = [[Unitary state|Unitary]] [[semi-presidential]] [[Constitution of Sri Lanka|constitutional republic]]
|leader_title1 = [[President of Sri Lanka|President]]
|leader_name1 = [[Mahinda Rajapaksa]]
|leader_title2 = [[Prime Minister of Sri Lanka|Prime Minister]]
|leader_name2 = [[D. M. Jayaratne]]
|leader_title3 = [[Speaker of the Parliament of Sri Lanka|Speaker of the Parliament]]
|leader_name3 = [[Chamal Rajapaksa]]
|leader_title4 = [[Chief Justice of Sri Lanka|Chief Justice]]
|leader_name4 = [[Mohan Peiris]]<ref name=BBCCJ>{{cite news |title=New Sri Lanka chief justice Mohan Peiris sworn |url=http://www.bbc.co.uk/news/world-asia-21022854 |publisher=BBC News |accessdate=27 January 2013 |date=15 January 2013}}</ref>
|legislature = [[Parliament of Sri Lanka|Parliament]]
|sovereignty_type = [[History of Sri Lanka|Independence]]
|sovereignty_note = from the [[United Kingdom]]
|established_event2 = [[Dominion of Ceylon|Dominion]]
|established_date2 = 4 February 1948
|established_event3 = [[Republic]]
|established_date3 = 22 May 1972
|established_event4 = {{nowrap|[[Constitution of Sri Lanka|Current constitution]]}}
|established_date4 = 7 September 1978
|area_rank = 122nd
|area_magnitude = 1 E10
|area_km2 = 65,610
|area_sq_mi = 25,332 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 4.4
|population_estimate = |population_estimate_rank = |population_estimate_year =
|population_census = 20,277,597<ref name="popsl">{{cite web |url=http://www.statistics.gov.lk/PopHouSat/CPH2011/Pages/sm/CPH%202011_R1.pdf |format=PDF |title=Census of Population and Housing 2011 Enumeration Stage February – March 2012 |work=Department of Census and Statistics – Sri Lanka}}</ref>
|population_census_rank = 57th
|population_census_year = 2012
|population_density_km2 = 323
|population_density_sq_mi =
|population_density_rank = 40th
|GDP_PPP = $142.719 billion<ref name="imfsl">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=48&pr.y=18&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=524&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Sri Lanka |work=International Monetary Fund}}</ref>
|GDP_PPP_rank = 64th
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $7,046<ref name="imfsl"/>
|GDP_PPP_per_capita_rank = 99th
|GDP_nominal = $70.966 billion<ref name="imfsl"/>
|GDP_nominal_rank = 68th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $3,385<ref name="imfsl"/>
|GDP_nominal_per_capita_rank = 122nd
|Gini_year = 2010
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 36.4 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank =
|HDI_year = 2012 <!--Please use the year to which the HDI year data refers, not the publication year-->
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.715 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2013_EN_complete.pdf |format=PDF |page=216 |title=Human Development Report 2013. Human development index trends: Table 1 |work=[[United Nations Development Programme]]}}</ref>
|HDI_rank = 92nd
|currency = [[Sri Lankan rupee]]
|currency_code = LKR
|time_zone = [[Sri Lanka Standard Time|SLST]]
|utc_offset = +5:30
|time_zone_DST = |DST_note = |utc_offset_DST =
|date_format = dd/mm/yyyy ([[Anno Domini|AD]])
|drives_on = left
|calling_code = [[Telephone numbers in Sri Lanka|+94]]
|cctld = {{ubl |[[.lk]] |{{raise|0.1em|[[.ලංකා]]}} |[[.இலங்கை]]}}
}}
