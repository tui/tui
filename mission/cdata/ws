{{Infobox country
| conventional_long_name = Independent State of Samoa
| native_name = ''Malo Sa{{okina}}oloto Tuto{{okina}}atasi o Sāmoa''
| common_name = Samoa
| image_flag = Flag of Samoa.svg
| image_coat = Coat of arms of Samoa.svg
| symbol_type = Coat of arms
| image_map = Samoa on the globe (Polynesia centered).svg
| national_motto = "Fa'avae i le [[Atua]] Sāmoa"<br>{{small|"Samoa is founded on God"}}
| national_anthem = ''[[The Banner of Freedom]]''
| official_languages = {{unbulleted list |[[Samoan language|Samoan]] |English}}
| demonym = [[Samoans|Samoan]]
| ethnic_groups =
{{unbulleted list
|  92.6% [[Samoans]]
|  7.0% [[Euronesian]]s
|  0.4% [[Europeans]]
}}
| ethnic_groups_year = 2001<ref name=cia/>
| capital = [[Apia]]
| latd=13 |latm=50 |latNS=S |longd=171 |longm=45 |longEW=W
| largest_city = Apia
| government_type = [[Unitary state|Unitary]] [[parliamentary democracy]]
| leader_title1 = {{nowrap|''[[O le Ao o le Malo]]''&nbsp;<sup>a</sup>}}
| leader_name1 = [[Tufuga Efi]]
| leader_title2 = [[Prime Minister of Samoa|Prime Minister]]
| leader_name2 = [[Tuilaepa Aiono Sailele Malielegaoi]]
| legislature = [[Legislative Assembly of Samoa|Legislative Assembly]]
| area_rank = 174th
| area_magnitude =
| area_km2 = 2,842
| area_sq_mi = 1097
| percent_water = 0.3
| population_estimate = 194,320<ref name=cia>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/ws.html |title=CIA – The World Factbook  |work=cia.gov |year=2012 |accessdate=9 August 2012}}</ref>
| population_estimate_rank = 166th
| population_estimate_year = 2012
| population_census = 179,186
| population_census_year = 2006
| population_density_km2 = 63.2
| population_density_sq_mi = 163.7
| population_density_rank = 144th
| GDP_PPP = $1.090 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=66&pr.y=14&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=862&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Samoa |publisher=International Monetary Fund |accessdate=20 April 2012}}</ref>
| GDP_PPP_rank =
| GDP_PPP_year = 2011
| GDP_PPP_per_capita = $5,965<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $630 million<ref name=imf2/>
| GDP_nominal_year = 2011
| GDP_nominal_per_capita = $3,451<ref name=imf2/>
| sovereignty_type = [[Independence]]
| established_event1 = {{nowrap|from [[New Zealand]]}}
| established_date1 = 1 January 1962<ref>{{cite web |url=http://www.nzhistory.net.nz/politics/samoa/towards-independence |title=Towards independence – NZ in Samoa |author=New Zealand Ministry for Culture and Heritage |work=nzhistory.net.nz |date=19 July 2010 |accessdate=29 April 2011}}</ref>
| Gini_year = | Gini_change =  <!--increase/decrease/steady--> | Gini =  <!--number only--> | Gini_ref = | Gini_rank =
| HDI_year = 2013
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.702 <!--number only-->
| HDI_ref =
| HDI_rank = 96th
| currency = [[Samoan tala|Tala]]
| currency_code = WST
| country_code =
| time_zone = [[UTC+13:00]]
| utc_offset = +13<sup>b</sup>
| time_zone_DST = [[UTC+14:00]]
| utc_offset_DST = +14
| drives_on = left<sup>c</sup>
| calling_code = [[+685]]
| cctld = [[.ws]]
| footnote_a = Head of state.
| footnote_b = Since 31 December 2011.<big><ref name="Samoa Changes Timezone">{{cite news |last=Staff/Agencies |title=Samoa skips Friday in time zone change |url=http://www.abc.net.au/news/2011-12-30/samoa-skips-friday-in-time-zone-change/3753350 |accessdate=16 January 2012 |newspaper=ABC Australia |date=31 December 2011 |archiveurl=http://www.webcitation.org/64jx2ob6X |archivedate=16 January 2012}}</ref></big>
| footnote_c = Since 7 September 2009.<big><ref>{{cite news |url=http://wheels.blogs.nytimes.com/2009/09/08/in-samoa-drivers-switch-to-left-side-of-the-road/ |work=The New York Times |title=In Samoa, Drivers Switch to Left Side of the Road |first=Richard S. |last=Chang |date=8 September 2009 |accessdate=23 May 2010}}</ref></big>
}}
