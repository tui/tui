{{Infobox country
| conventional_long_name = {{Nowrap|People's Democratic Republic of Algeria}}
| native_name = {{Native name|ar|جمهورية الجزائر الديمقراطية الشعبية}}<br />{{Native name|ber|Tagduda Tadzayrit Tugduyant Tagherfant}}
| common_name = Algeria
| national_anthem = "[[Kassaman]]"<br/>{{small|"We Pledge"}} by: [[Moufdi Zakaria]] <br/> {{small|[http://www.algeria-un.org/audio/algeria.mp3 External audio file]}}
| national_motto = {{native phrase|ar|{{big|بالشّعب وللشّعب}}|italics=off}}<br/>{{small|"By the people and for the people"}}{{lower|0.2em|<ref name="CONST-AR">{{cite web|url=http://www.el-mouradia.dz/arabe/symbole/textes/constitution96.htm |title=Constitution of Algeria, Art. 11 |language=Arabic |publisher=El-mouradia.dz |date= |accessdate=17 January 2013}}</ref><ref name="CONST-EN">{{cite web|url=http://www.apn-dz.org/apn/english/constitution96/titre_01.htm |title=Constitution of Algeria; Art. 11 |publisher=Apn-dz.org |date=28 November 1996 |accessdate=17 January 2013}}</ref>}}
| image_flag = Flag of Algeria.svg
| image_coat = Algeria emb (1976).svg
| symbol_type = Emblem
| image_map = Algeria (orthographic projection).svg
| image_map2 = Algeria - Location Map (2013) - DZA - UNOCHA.svg
| official_languages = [[Arabic]]<ref name="constitution">{{cite web|url=http://www.apn-dz.org/apn/english/constitution96/titre_01.htm |title=Constitution of Algeria; Art. 3 |publisher=Apn-dz.org |date=28 November 1996 |accessdate=17 January 2013}}</ref>
| languages_type = Other languages
| languages = {{vunblist |[[Berber language]] ([[Tamazight]])<ref name="AlgeriaFactbook">{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/ag.html |title=The World Factbook - Algeria |publisher=[[Central Intelligence Agency]] |date=4 December 2013 |accessdate=24 December 2013}}</ref> {{small|(constitutionally national)}} | [[French language|French]] (business and education)<ref name="AlgeriaFactbook"/>}}
| official_religion = [[Sunni Islam]]
| ethnic_groups =
{{vunblist
| 99% {{nowrap|Arab-Berber<ref name="AlgeriaFactbook"/>{{ref label|Amazigh|a|}}<!--end nowrap:-->}}
| 1% European and others<ref name="AlgeriaFactbook"/>}}
| capital = [[Algiers]]
| latd=36 |latm=42 |latNS=N |longd=3 |longm=13 |longEW=E
| largest_city = capital
| government_type = [[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]] [[republic]]
| leader_title1 = [[President of Algeria|President]]
| leader_name1 = [[Abdelaziz Bouteflika]]
| leader_title2 = [[Prime Minister of Algeria|Prime Minister]]
| leader_name2 = [[Abdelmalek Sellal]]
| legislature = [[Parliament of Algeria|Parliament]]
| upper_house = [[Council of the Nation]]
| lower_house = {{nowrap|[[People's National Assembly]]}}
| sovereignty_type = [[Algerian War|Independence]] {{nobold|from [[France]]}}
| established_event1 = Recognized
| established_date1 = 3 July 1962
| established_event2 = Declared
| established_date2 = 5 July 1962
| area_rank = 10th
| area_magnitude = 1 E12
| area_km2 = 2381741
| area_sq_mi = 919595
| percent_water = negligible
| population_estimate = 38,700,000<ref name="ONS">{{cite web |url=http://www.ons.dz/-Demographie-.html |title=Démographie (ONS) |publisher=ONS |date=19 January 2014 |accessdate=19 January 2014}}</ref>
|population_estimate_year = 2014
|population_estimate_rank = 34th
| population_census = 37,900,000<ref name="ONS"/>
| population_census_year = 2013
| population_density_km2 = 15.9
| population_density_sq_mi = 37.9<!-- Do not remove per [[WP:MOSNUM]] -->
| population_density_rank = 208th
| GDP_PPP_year = 2014
| GDP_PPP = $302.476 billion<ref name="imf2">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=55&pr.y=15&sy=2012&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=612&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Algeria |publisher=International Monetary Fund |accessdate=26 April 2014}}</ref>
| GDP_PPP_rank = 
| GDP_PPP_per_capita =$7,816<ref name="imf2" />
| GDP_PPP_per_capita_rank =
| GDP_nominal =$219.453 billion<ref name="imf2" />
| GDP_nominal_rank =
| GDP_nominal_year = 2014
| GDP_nominal_per_capita = $5,670<ref name="imf2" />
| GDP_nominal_per_capita_rank = 
| Gini_year = 1995
| Gini_change =<! -- increase/decrease/steady -->
| Gini = 35.3
| Gini_ref =<ref>{{cite web |author=Staff |date=undated |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of Family Income&nbsp;– Gini Index |work=[[The World Factbook]] |publisher=[[Central Intelligence Agency]] |accessdate=1 September 2009 |archiveurl=http://www.webcitation.org/5rRcwIiYs |archivedate=23 July 2010 |deadurl=no}}</ref>
| HDI_year = 2013
| HDI_change = increase <!-- increase/decrease/steady -->
| HDI = 0.713 <!-- number only -->
| HDI_ref =<ref name="UN">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011. Human Development Index Trends |publisher=United Nations |accessdate=2 November 2011}}</ref>
| HDI_rank = 93rd
| currency = [[Algerian dinar]]
| currency_code = DZD
| time_zone = [[Central European Time|CET]] ([[UTC+01]])
| time_zone_DST =
| demonym = [[Demographics of Algeria|Algerian]]
| drives_on = right<ref>{{cite news|last=Geoghegan |first=Tom |url=http://news.bbc.co.uk/2/hi/8239048.stm |title=Could the UK drive on the right? |publisher=BBC News |date=7 September 2009 |accessdate=14 January 2013}}</ref>
| calling_code = [[+213]]
| iso3166code = DZ
| cctld = {{vunblist |[[.dz]] |[[الجزائر.]]}}
| footnote_a = {{note|arabberberbox}} The [[The World Factbook|CIA World Factbook]] states that about 15% of Algerians, a minority, identify as Berber even though almost all Algerians have Berber origins instead of Arab origins. The Factbook explains that of the approximately 15% who identify as Berber, most live in [[Kabylie]], more closely identify with Berber heritage instead of Arab heritage, and are Muslim.
}}
