{{Infobox country
|conventional_long_name = Republic of Cyprus
|native_name = {{native name|el|Κυπριακή Δημοκρατία|italics=no}}<br />{{native name|tr|Kıbrıs Cumhuriyeti}}
|common_name = Cyprus
|image_flag = Flag of Cyprus.svg
|image_coat = Coat of Arms of Cyprus.svg
|image_map = EU-Cyprus.svg
|map_caption = Cyprus proper shown in dark green; areas outside of Cypriot control shown in light green. {{map caption |location_color =dark green |region =Europe |region_color =dark grey | subregion =the [[European Union]] |subregion_color=dark green |legend =EU-Cyprus.svg}}
|national_motto =
|national_anthem = [[Hymn to Liberty]]
|official_languages = {{plainlist|
* [[Greek language|Greek]]
* [[Turkish language|Turkish]]
}}
|languages_type = Minority languages
|languages = {{plainlist|
* [[Armenian language|Armenian]]
* [[Cypriot Arabic]]
}}
|demonym = Cypriot
|ethnic_groups = {{plainlist|
* [[Greek Cypriots]]
* [[Turkish Cypriots]]
}}
|ethnic_groups_year =
|capital = [[Nicosia]]
|latd=35 |latm=10 |latNS=N |longd=33 |longm=22 |longEW=E
|government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[constitutional republic]]
|leader_title1 = [[President of Cyprus|President]]
|leader_name1 = [[Nicos Anastasiades]]
|legislature = {{nowrap|[[House of Representatives (Cyprus)|House of Representatives]]}}
|accession EU date = 1 May 2004
|area_rank = 168th
|area_magnitude = 1_E9
|area_label= Total<ref group="i" name="island">Including [[Northern Cyprus]], the [[United Nations Buffer Zone in Cyprus|UN buffer zone]] and [[Akrotiri and Dhekelia]].</ref>
|area_km2 = 9,251
|area_sq_mi = 3,572 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 9
|population_estimate = 1,117,000<ref group="i" name="island"/><ref>{{cite paper |author=United Nations, Department of Economic and Social Affairs, Population Division |year=2011 |title=World Population Prospects: The 2010 Revision, Highlights and Advance Tables (ESA/P/WP.220) |page=80 |publication-place=New York |url=http://esa.un.org/unpd/wpp/Documentation/pdf/WPP2010_Highlights.pdf}}</ref>
|population_estimate_year = 2011
|population_estimate_rank =
|population_census = 838,897<ref group="i" name="census">Excluding Northern Cyprus.</ref><ref>{{cite web |url=http://www.cystat.gov.cy/mof/cystat/statistics.nsf/All/732265957BAC953AC225798300406903?OpenDocument&sub=2&sel=1&e= |title=Statistical Service – Population and Social Conditions – Population Census – Announcements – Preliminary Results of the Census of Population, 2011 |language=Greek |publisher=Statistical Service of the Ministry of Finance of the Republic of Cyprus |date=29 December 2011 |accessdate=29 January 2012}}</ref>
|population_census_year = 2011
|population_density_km2 = 90.7
|population_density_sq_mi = 234.85 <!-- Do not remove per [[WP:MOSNUM]] -->
|population_density_rank = 114th
|GDP_PPP = $23.613 billion<ref name="imf2">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=39&pr.y=3&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=423&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Report for Cyprus |work=World Economic Outlook Database |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
|GDP_PPP_rank =
|GDP_PPP_year = 2013
|GDP_PPP_per_capita = $27,085<ref name="imf2"/>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $23.006 billion<ref name="imf2"/>
|GDP_nominal_rank =
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $26,389<ref name="imf2"/>
|GDP_nominal_per_capita_rank =
|Gini_year = 2011
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 29.1 <!--number only-->
| Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|Gini_rank = 19th
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.848 <!--number only-->
|HDI_rank = 31st
|HDI_ref =<ref>{{cite web |url=http://hdr.undp.org/en/reports/global/hdr2013/download/ |title=2013 Human Development Report |publisher=United Nations Publications |date=14 March 2013 |accessdate=14 March 2013}}</ref>
|sovereignty_type = Independence {{nobold|from the United Kingdom}}
|established_event1 = [[Zürich and London Agreement]]
|established_date1 = 19 February 1959
|established_event2 = Independence proclaimed
|established_date2 = 16 August 1960
|established_event3 = {{nowrap|[[Independence Day (Cyprus)|Independence Day]]}}
|established_date3 = 1 October 1960
|established_event4 = {{nowrap|[[2004 enlargement of the European Union|Joined]] the [[European Union|EU]]}}
|established_date4 = 1 May 2004
|currency = [[Euro]]
|currency_code = EUR
|time_zone = [[Eastern European Time|EET]]
|utc_offset = +2
|time_zone_DST = [[Eastern European Summer Time|EEST]]
|utc_offset_DST = +3
|drives_on = left
|calling_code = [[Telephone numbers in Cyprus|+357]]
|cctld = [[.cy]]<ref group="i">The [[.eu]] domain is also used, shared with other [[European Union]] member states.</ref>
|footnotes = {{reflist|group="i"}}
}}
