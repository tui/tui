{{Infobox country
|common_name = the Bahamas
|conventional_long_name = Commonwealth of the Bahamas
|image_flag = Flag of the Bahamas.svg
|image_coat = Coat of arms of the Bahamas.svg
|image_map = LocationBahamas.svg
|image_map2 = The Bahamas - Location Map (2013) - BHS - UNOCHA.svg
|national_motto="Forward, Upward, Onward, Together"
|national_anthem = ''[[March On, Bahamaland]]''
|royal_anthem = ''[[God Save the Queen]]''
|official_languages = [[English language|English]]
|ethnic_groups = {{nowrap|{{vunblist |90% [[Afro-Bahamian|Black]] |8% [[White Bahamian|White]] or [[Multiracial|Mixed]]|0.7% Not Stated |0.6% [[Asian people|Asian]] | 0.3% [[Other races|Other]] }}}}
|ethnic_groups_year = {{lower|0.4em|<ref name="statistics">[http://statistics.bahamas.gov.bs/download/095261300.pdf Bahamas Department of Statistics]. (PDF) . Retrieved on 20 April 2014.</ref>}}
|demonym = Bahamian
|capital = [[Nassau, Bahamas|Nassau]]
|latd=25 |latm=4 |latNS=N |longd=77 |longm=20 |longEW=W
|largest_city = capital
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional monarchy]]<ref name="A12">{{cite web |first= |last= |title=•GENERAL SITUATION AND TRENDS |work=Pan American Health Organization |url=http://www.paho.org/english/dd/ais/cp_044.htm}}</ref><ref name="A13">{{cite news |first= |last= |title=Mission to Long Island in the Bahamas |work=Evangelical Association of the Caribbean |url=http://www.caribbeanevangelical.org/newsevents/oldarticles.htm?id=82}}</ref>
|leader_title1 = [[Monarchy of the Bahamas|Monarch]]
|leader_name1 = [[Elizabeth II]]
|leader_title2 = {{nowrap|[[List of Governors-General of The Bahamas|Governor-General]]}}
|leader_name2 = [[Arthur Foulkes]]
|leader_title3 = [[List of heads of government of the Bahamas|Prime Minister]]
|leader_name3 = [[Perry Christie]]
|legislature = [[Parliament of the Bahamas|Parliament]]
|upper_house = [[Senate of the Bahamas|Senate]]
|lower_house = [[House of Assembly of the Bahamas|House of Assembly]]
|area_rank = 160th
|area_magnitude = 1 E10
|area_km2 = 13878
|area_sq_mi = 5358 <!-- Do not remove [[WP:MOSNUM]]-->
|percent_water = 28%
|population_estimate = 319,031 <ref name="cia.gov">[https://www.cia.gov/library/publications/the-world-factbook/geos/bf.html Bahamas, The]. CIA World Factbook.</ref>
|population_estimate_year = 2013
|population_estimate_rank = 177th
|population_census = 254,685
|population_census_year = 1990
|population_density_km2 = 23.27
|population_density_sq_mi = 60<!-- Do not remove [[WP:MOSNUM]]-->
|population_density_rank = 181st
|GDP_PPP_year = 2012
|GDP_PPP = $11.055 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=313&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=&pr.x=73&pr.y=10 |title=The Bahamas |publisher=International Monetary Fund |accessdate=18 April 2013}}</ref>
|GDP_PPP_rank =
|GDP_PPP_per_capita = $31,382<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal_year = 2012
|GDP_nominal = $8.043 billion<ref name=imf2/>
|GDP_nominal_per_capita = $22,832<ref name=imf2/>
|Gini_year = 2001
|Gini_change = <!--increase/decrease/steady-->
|Gini = 57
|Gini_ref = <ref name=blcs>{{cite web|title=Bahamas Living Conditions Survey 2001|url=http://www.centralbankbahamas.com/download/BLCS%202001%20poverty.pdf#page=4|publisher=Department of Statistics|accessdate=4 October 2013}}</ref> 
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.794 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=30 November 2011}}</ref>
|HDI_rank = 49th
|sovereignty_type = [[Independence]]
|established_event1 = from the [[United Kingdom]]
|established_date1 = 10 July 1973<ref name="bbc_Bahamas">{{cite news |date=9 July 1973 |url=http://news.bbc.co.uk/onthisday/hi/dates/stories/july/9/newsid_2498000/2498835.stm |title=1973: Bahamas' sun sets on British Empire |publisher=BBC News |accessdate=1 May 2009 |last= |quote=}}</ref>
|currency = [[Bahamian dollar]] (BSD) (Although [[USD]] is widely accepted)
|country_code = BAH
|time_zone = [[Eastern Time Zone|EST]]
|utc_offset = −5
|time_zone_DST = [[Eastern Daylight Time|EDT]]
|utc_offset_DST = −4
|drives_on = left
|iso3166code = BS
|cctld = [[.bs]]
|calling_code = [[Area code 242|+1-242]]
}}
