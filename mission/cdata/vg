{{Infobox country 
|conventional_long_name = Virgin Islands<br>{{small|{{nobold|''[[British Overseas Territories|Overseas territory]] of the [[United Kingdom]]''}}}}
|common_name = British Virgin Islands
|image_flag = Flag of the British Virgin Islands.svg|125px
|image_coat = Coat of Arms of the British Virgin Islands.svg|100px
|image_map = British Virgin Islands in United Kingdom.svg
|national_motto = {{native phrase|la|"Vigilate"|italics=off}}<br>{{small|"Be Vigilant"}}
|national_anthem = {{nowrap|''[[God Save the Queen]]''{{nbsp|2}}{{small|(official)}}<br>'''Territorial song:''' ''[[Oh, Beautiful Virgin Islands]]''{{nbsp|2}}{{small|(official)}}}}
|official_languages = English
|ethnic_groups =
{{vunblist
| {{nowrap|83.36% [[Afro-Caribbean]]}}
|  7.28% White<sup>a</sup>
|  5.38% [[multiracial]]<sup>b</sup>
|  3.14% [[East India]]n
|  0.84% others
}}
|capital = [[Road Town]]
|latd=18 |latm=25.883 |lats= |latNS=N |longd=64 |longm=37.383 |longs= |longEW=W
|largest_city = [[Road Town]]
|demonym = Virgin Islander
|sovereignty_type = [[British Overseas Territory]]
|government_type = [[British Overseas Territories|British Overseas Territory]]<sup>c</sup>
|leader_title1 = [[Monarchy of the United Kingdom|Monarch]]
|leader_name1 = [[Elizabeth II]]
|leader_title2 = [[Governor of the British Virgin Islands|Governor]]
|leader_name2 = [[William Boyd McCleary]]
|leader_title3 = [[Deputy Governor of the British Virgin Islands|Deputy Governor]]
|leader_name3 = [[V. Inez Archibald]]
|leader_title4 = [[Premier of the British Virgin Islands|Premier]]
|leader_name4 = [[Orlando Smith]]
|leader_title5 = Responsible Minister<sup>d</sup> ([[UK government|UK]])
|leader_name5 = [[Mark Simmonds]] [[Member of Parliament|MP]]
|legislature = [[Legislative Council of the British Virgin Islands|House of Assembly]]
|established_event1 = Separate
|established_date1 = 1960
|established_event2 = Autonomous&nbsp;territory
|established_date2 = 1967
|area_rank = 216th
|area_magnitude = 1 E8
|area_km2 = 153
|area_sq_mi = 59
|percent_water = 1.6
|population_estimate = 27,800<ref>http://www.bviplatinum.com/news.php?page=Article&articleID=1331602904</ref>
|population_estimate_rank = 
|population_estimate_year = 2012
|population_census = 27,000<ref>http://www.fco.gov.uk/en/travel-and-living-abroad/travel-advice-by-country/country-profile/north-central-america/british-virgin-islands/</ref>
|population_census_year = 2005
|population_census_rank = 212th
|population_density_km2 = 260
|population_density_sq_mi = 673
|population_density_rank = 68th
|GDP_PPP = $853.4 million<ref>https://www.cia.gov/library/publications/the-world-factbook/geos/vi.html</ref>
|GDP_PPP_rank = 
|GDP_PPP_year = 
|GDP_PPP_per_capita = $43,366
|GDP_PPP_per_capita_rank = 
|HDI_year = |HDI_change =  <!--increase/decrease/steady--> |HDI =  <!--number only--> |HDI_ref = |HDI_rank = 
|currency = [[United States dollar]]
|currency_code = USD
|country_code = 
|time_zone = [[Atlantic Standard Time|AST]]
|utc_offset = -4
|time_zone_DST = not observed
|utc_offset_DST = -4
|drives on = left
|calling_code = [[+1-284]]
|cctld = [[.vg]]
|footnote_a = Mostly British and [[Portuguese people|Portuguese]].
|footnote_b = Mostly [[Puerto Rican people|Puerto Ricans]].
|footnote_c = [[Parliamentary system|Parliamentary]] [[Representative democracy|democratic]] [[Dependent territory|dependency]] under [[constitutional monarchy]].
|footnote_d = For the [[British Overseas Territories|Overseas Territories]].
}}
