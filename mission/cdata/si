{{Infobox country
|conventional_long_name = Republic of Slovenia
|native_name = {{native name|sl|Republika Slovenija|icon=no}}
|common_name = Slovenia
|image_flag = Flag of Slovenia.svg
|image_coat = Coat of Arms of Slovenia.svg
|image_map = EU-Slovenia.svg
|map_caption = {{map caption |location_color=dark green |region=[[Europe]] |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green}}
|national_anthem = {{nowrap|''[[Zdravljica]]''&nbsp;<br/>{{small|''A Toast''}}{{efn-lr|Melody by [[Stanko Premrl]].{{#tag:ref|As defined by the Act Regulating the Coat-of-Arms, Flag and Anthem of the Republic of Slovenia and the Flag of the Slovene Nation ("Zakon o grbu, zastavi in himni Republike Slovenije ter o slovenski narodni zastavi") from 1994 and published on the web page of the [[National Assembly (Slovenia)|National Assembly of Slovenia]].<ref>{{cite web |url=http://www.pisrs.si/Predpis.aspx?id=ZAKO365&pogled=osnovni |title=PisRS |publisher=Pisrs.si |accessdate=2 June 2012}}</ref><ref name="Bozic">{{cite news |url=http://www.delo.si/clanek/127578 |title=Katero kitico č'mo kot himno zapet' |language=Slovene |trans_title=Which Stanza Should We As the Anthem Sing? |first=Dragan |last=Božič |date=3 November 2010 |accessdate=14 February 2011 |issn=1854-6544}}</ref> The question whether the entire ''Zdravljica'' or only its seventh stanza constitutes the Slovenian national anthem, remains unresolved. Whereas the [[Constitution of Slovenia]] determines the title of the poem, the act about the anthem specifically determines its seventh stanza. It has been argued that the act contradicts the constitution and that the question should be resolved by the [[Constitutional Court of Slovenia|Slovenian Constitutional Court]].<ref>{{cite news |url=http://www.delo.si/novice/politika/zdravljica-v-politicnem-in-pravnem-primezu.html |title=Zdravljica v političnem in pravnem primežu |first=Klara |last=Škrinjar |newspaper=Delo.si |date=3 September 2012 |language=Slovene |trans_title=Zdravljica in the Political and Legal Vice}}</ref>|group=Note}}}}}}<br/><center>[[File:Zdravljica.ogg]]</center>
|official_languages = [[Slovene language|Slovene]]
|demonym = [[Slovene (disambiguation)]]
|capital = {{Coat of arms|Ljubljana}}
|latd=46 |latm=03 |latNS=N |longd=14 |longm=30 |longEW=E
|largest_city = capital
|ethnic_groups =
{{unbulleted list
| 83% [[Slovenes]]
|  2% [[Serbs in Slovenia|Serbs]]
|  2% [[Croats of Slovenia|Croats]]
|  1% [[Bosniaks]]
| {{nowrap|12% others{{\}}unspecified}}
}}
|ethnic_groups_year = 2002<ref name="2002census">{{cite web |url=http://www.stat.si/popis2002/en/rezultati/rezultati_red.asp?ter=SLO&st=7 |title=Census 2002: 7. Population by ethnic affiliation, Slovenia, Census 1953,  1961, 1971, 1981, 1991 and 2002 |publisher=Statistical Office of the Republic of Slovenia |accessdate=2 February 2011}}</ref>
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Parliamentary system|parliamentary]]}} [[constitutional republic]]
|leader_title1 = [[President of Slovenia|President]]
|leader_name1 = [[Borut Pahor]]
|leader_title2 = [[Prime Minister of Slovenia|Prime Minister]]
|leader_name2 = [[Alenka Bratušek]]
|legislature = [[Slovenian Parliament|Parliament]]
|upper_house = [[National Council (Slovenia)|National Council]]
|lower_house = [[National Assembly (Slovenia)|National Assembly]]
|established_event1 = {{nowrap|[[State of Slovenes, Croats and Serbs|State of Slovenes,<br/>Croats and Serbs]]<br/>becomes independent}}
|established_date1 = <br/>29 October 1918
|established_event2 = {{nowrap|[[Creation of Yugoslavia|Kingdom of Serbs,<br/>Croats and Slovenes<br/>(Kingdom of Yugoslavia)]]}}
|established_date2 = <br/>4 December 1918
|established_event3 = [[Democratic Federal Yugoslavia|Yugoslavia]] becomes a republic
|established_date3 = 29 November 1945
|established_event4 = Independence from Yugoslavia
|established_date4 = 25 June 1991<ref name="Škrk1999"/>
|established_event5 = {{nowrap|[[2004 enlargement of the European Union|Joined]] the [[European Union]]}}
|established_date5 = 1 May 2004|area_rank = 153rd
|area_magnitude = 1_E10
|area_km2 = 20,273
|area_sq_mi = 7,827 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 0.7<ref name="Surface2005">{{cite web |url=http://www.stat.si/letopis/2010/01_10/01-03-10.htm |title=Površina ozemlja in pokrovnost tal, določena planimetrično, 2005 |trans_title=Surface area and land cover determined planimetrically, 2005 |language=Slovene, English |publisher=Statistical Office of the Republic of Slovenia |accessdate=2 February 2011}}</ref>
|population_estimate = 2,061,085<ref name="Statistical Office of Slovenia">http://www.stat.si/eng/novica_prikazi.aspx?id=6200</ref>
|population_estimate_year = 2014
|population_estimate_rank = 144th
|population_census = 1,964,036
|population_census_year = 2002
|population_density_km2 = 101<ref>{{cite web |url=http://www.stat.si/letopis/2010/30_10/30-09-10.htm |title=Gostota naseljenosti, 1. 7. |language=Slovene, English |trans_title=Population density, 1 July |publisher=Statistical Office of the Republic of Slovenia |accessdate=2 February 2011}}</ref>
|population_density_sq_mi = 262 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 106th
|GDP_PPP_year = 2014
|GDP_PPP = $58.509&nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=17&pr.y=9&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=961&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Slovenia |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
|GDP_PPP_rank =
|GDP_PPP_per_capita = $28,373<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $48.876&nbsp;billion<ref name=imf2/>
|GDP_nominal_rank =
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $23,702<ref name=imf2/>
|GDP_nominal_per_capita_rank =
|Gini_year = 2011
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 23.8 <!--number only-->
|Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = steady <!--increase/decrease/steady-->
|HDI = 0.892 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=2 November 2011}}</ref>
|HDI_rank = 21st
|currency = [[Euro]] ([[Euro sign|€]]){{efn-lr|[[Slovenian tolar]] prior to 2007.}}
|currency_code = EUR
|time_zone = [[Central European Time|CET]]
|utc_offset = +1
|time_zone_DST = [[Central European Summer Time|CEST]]
|utc_offset_DST = +2
|date_format = dd.mm.yyyy
|drives_on = right
|calling_code = [[Telephone numbers in Slovenia|+386]]
|ISO_3166-1_alpha2=SI |ISO_3166-1_alpha3=SVN |ISO_3166-1_num=705
|alt_sport_code=SLO |vehicle_code=SLO |aircraft_code=S5
|cctld = [[.si]]{{efn-lr|Also [[.eu]], shared with other [[European Union]] member states.}}
|footnotes = {{notelist-lr}}
}}
