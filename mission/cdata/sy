{{Infobox country
|conventional_long_name = Syrian Arab Republic
|native_name = {{lang|ar|{{big|الجمهورية العربية السورية}}}}<br />''{{transl|ar|Al-Jumhūrīyah Al-ʻArabīyah As-Sūrīyah}}''
|common_name = Syria
|image_flag = Flag of Syria.svg
|image_coat = Coat of arms of Syria.svg
|image_map = Syria (orthographic projection).svg
|image_map2= Syria - Location Map (2013) - SYR - UNOCHA.svg
|national_anthem = {{native phrase|ar|"حماة الديار"|italics=off}}<br />{{small|''[[Humat ad-Diyar|Guardians of the Homeland]]''}}<br><center></center>
|official_languages = [[Arabic language|Arabic]]
|capital = [[Damascus]]
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Dominant-party system|dominant-party]]<br />[[Semi-presidential system|semi-presidential]] [[republic]] <br />under [[Authoritarianism|authoritarian rule]] <ref>{{cite web |url=http://www.scribd.com/doc/81771718/Qordoba-Translation-of-the-Syrian-Constitution-Modifications-15-2-2012 |title=Constitution of Syria 2012 |publisher=Scribd.com |date=2012-02-15 |accessdate=2013-01-25}}</ref>{{efn-ua|1=Government has limited control across the country. See [[Syrian civil war]]}}<!--end nowrap:-->}}
|leader_title1 = [[List of heads of state of Syria|President]]
|leader_name1 = [[Bashar al-Assad]]
|leader_title2 = [[List of Prime Ministers of Syria|Prime Minister]]
|leader_name2 = [[Wael Nader al-Halqi]]
|leader_title3 = [[List of Speakers of the Parliament of Syria|Speaker of the People's Council]]
|leader_name3 = [[Mohammad Jihad al-Laham]]
|legislature = [[People's Council of Syria|People's Council]]
|largest_city = [[Aleppo]]
|latd=33 |latm=30 |latNS=N |longd=36 |longm=18 |longEW=E
|area_rank = 89th
|area_magnitude = 1 E11
|area_km2 = 185180 <ref>{{cite web |url=http://www.mofa.gov.sy/cweb/MOEX_NEW/syria/Overview.htm|title=Syrian ministry of foreign affairs}}</ref>
|area_sq_mi = 71479 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 1.1
|population_estimate = 22,530,746<ref name="CIA" />
|population_estimate_rank = 54th
|population_estimate_year = July 2012
|population_census =
|population_census_year =
|population_density_km2 = 118.3
|population_density_sq_mi = 306.5 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 101st
|GDP_PPP = {{nowrap|$107.831 billion<ref name=CIA>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?sy=2009&ey=2010&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=59&pr1.y=13&c=463&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Syria |publisher=International Monetary Fund |accessdate=22 April 2012}}</ref><!--end nowrap:-->}}
|GDP_PPP_rank =
|GDP_PPP_year = 2012
|GDP_PPP_per_capita = $5,100<ref name="CIA" />
|GDP_PPP_per_capita_rank =
|GDP_nominal_year = 2010
|GDP_nominal = {{nowrap|$59.957 billion<ref name="CIA" />}}
|GDP_nominal_rank =
|GDP_nominal_per_capita = $2,802<ref name="CIA" />
|GDP_nominal_per_capita_rank =
|established_event1 = Dissolution of [[Ottoman Syria]]; French occupation
|established_date1 = 1 September 1918
|established_event2 = Proclamation of Arab Kingdom of Syria
|established_date2 = 8 March 1920
|established_event3 = [[State of Syria]] established under French Mandate
|established_date3 = 1 December 1924
|established_event4 = [[Syrian Republic (1930–1958)|Syrian Republic]] established by merger of States of Jabal Druze, Alawites and Syria
|established_date4 = 1930
|established_event5 = [[Independence]] from France
|established_date5 = 17 April 1946
|established_event6 = {{nowrap|[[Secession]] from the<br />[[United Arab Republic]]}}
|established_date6 = 28 September 1961
|established_event7 = [[Ba'ath party]] [[1963 Syrian coup d'état|takes]] power
|established_date7 = 8 March 1963
|Gini_year = 2004
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 35.8 <!--number only-->
|Gini_ref = <ref>{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=World Bank GINI index |publisher=Data.worldbank.org |accessdate=2013-01-22}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.648 <!--number only-->
|HDI_ref = <ref>{{cite web|url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Index and its components. Human Development Report 2011 |format=PDF |accessdate=2013-01-25}}</ref>
|HDI_rank = 116th
|currency = [[Syrian pound]]
|currency_code = SYP
|country_code = SY
|time_zone = [[Eastern European Time|EET]]
|utc_offset = +2
|time_zone_DST = [[Eastern European Summer Time|EEST]]
|utc_offset_DST = +3
|drives_on = right
|calling_code = [[Telephone numbers in Syria|+963]]{{efn-ua|1=02 from [[Lebanon]]}}
|cctld = [[.sy]], [[سوريا.]]
|iso3166code = SY
| footnotes = {{notelist-ua}}
}}
