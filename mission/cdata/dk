{{Infobox country
|conventional_long_name = Kingdom of Denmark
|native_name = {{native name|da|Kongeriget Danmark|icon=no}}
|common_name = Denmark
|image_flag = Flag of Denmark.svg
|alt_flag = Red with a white cross that extends to the edges of the flag; the vertical part of the cross is shifted to the hoist side
|image_coat = National Coat of arms of Denmark.svg
|national_anthem =<br/>''[[Der er et yndigt land]]''<br/>''{{small|There is a lovely country}}''
<center>[[File:United States Navy Band - Der er et yndigt land.ogg]]
<br />''[[Kong Christian stod ved højen mast]]''{{#tag:ref|''Kong Christian'' has equal status as a [[national anthem]] but is generally used only on [[royal anthem|royal]] and military occasions.<ref>{{cite web|title=Not one but two national anthems|url=http://denmark.dk/en/quick-facts/national-anthems/|work=Denmark.dk|publisher=[[Ministry of Foreign Affairs (Denmark)|Ministry of Foreign Affairs of Denmark]]|accessdate=18 May 2014}}</ref>|group="N"}}<br/>''{{small|King Christian stood by the lofty mast}}''
|image_map = EU-Denmark.svg
|map_width = 260px
|map_caption = {{map caption |location_color=dark green |country='''Denmark'''<ref name="proper" group="N" /> |region=Europe |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green |legend=EU-Denmark.svg}}
|image_map2 = Kingdom of Denmark (orthographic projection).svg
|map2_width = 255px
|map_caption2 = '''{{Fontcolor|#346733|[[Danish Realm|Kingdom of Denmark]]}}''': Greenland, the Faroe Islands (circled), and Denmark.
|official_languages = [[Danish language|Danish]]
|regional_languages= {{hlist |[[Faroese language|Faroese]]|[[Greenlandic language|Greenlandic]]|[[German language|German]]<ref group="N">Faroese is co-official with Danish in the Faroe Islands. Greenlandic is the sole official language in Greenland. German is recognised as a protected minority language in the South Jutland area of Denmark.</ref>}}
|demonym = {{hlist |[[Danes|Danish]] |[[Danes|Dane]]}}
|capital = {{Coat of arms|Copenhagen}}
|latd = 55
|latm = 43
|latNS = N
|longd = 12
|longm = 34
|longEW = E
|largest_city = capital
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Parliamentary system|parliamentary]]<br>[[constitutional monarchy]]}}
|leader_title1 = [[Monarchy of Denmark|Monarch]]
|leader_name1 = [[Margrethe II of Denmark|Queen Margrethe II]]
|leader_title2 = [[Prime Minister of Denmark|Prime Minister]]
|leader_name2 = [[Helle Thorning-Schmidt]]
|legislature = [[Folketing]]
|area_rank = 133rd
|area_label = Denmark<ref name="proper" group="N" />
|area_km2 = 42,915.7
|area_sq_mi = (16,562.1)<!--Do not remove per [[WP:MOSNUM]]-->
|area_footnote = <ref>[http://www.statistikbanken.dk/ Statistics Denmark]</ref>
|area_label2 = Greenland
|area_data2 = {{convert|2,166,086|km2|sqmi|abbr=on|sigfig=6}}
|area_label3 = Faroe Islands
|area_data3 = {{convert|1,399|km2|sqmi|abbr=on|sigfig=5}}
|population_estimate_year = {{nowrap|Jan 2014}}
|population_estimate = 5,627,235<ref>[http://www.noegletal.dk/ Danish Ministry for Economic Affairs and the Interior]</ref> ([[List of countries by population|113th]])
|population_label2 = Greenland
|population_data2 = 56,370<ref name="Greenland pop">[http://www.stat.gl/publ/en/GF/2013/pdf/Greenland%20in%20Figures%202013.pdf ''"Greenland in Figures 2013,"''] [[Statistics Greenland]]. Retrieved 2 September 2013</ref><ref name="pop est" group="N">2013 estimate</ref>
|population_label3 = Faroe Islands
|population_data3 = 49,709<ref name="faroes pop">[https://www.cia.gov/library/publications/the-world-factbook/geos/fo.html "Faroe Islands"] – ''The World Factbook''. Retrieved 6 June 2012</ref><ref name="pop est" group="N" />
|population_density_km2 = 130
|FR_foot5 = &nbsp;<small>(Denmark)</small>
|GDP_PPP_year = 2013
|GDP_PPP = $211.321&nbsp;billion<ref name=imf2>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2013/02/weodata/weorept.aspx?pr.x=36&pr.y=13&sy=2011&ey=2018&scsm=1&ssd=1&sort=country&ds=.&br=1&c=238%2C128%2C944&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Denmark |publisher=International Monetary Fund |accessdate=16 March 2014}}</ref><ref name="denonly" group="N">This data is for Denmark  [[wikt:proper#Adjective|proper]] only. For data relevant to [[Greenland]] and the [[Faroe Islands]] see their respective articles.</ref>
|GDP_PPP_rank = 52nd
|GDP_PPP_per_capita = $37,794<ref name=imf2/>
|GDP_PPP_per_capita_rank = 19th
|GDP_nominal = $324.293&nbsp;billion<ref name=imf2/><ref name="denonly" group="N" />
|GDP_nominal_rank = 34th
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $57,998<ref name=imf2/>
|GDP_nominal_per_capita_rank = 6th
|Gini_year = 2012
|Gini_change = increase <!--increase/decrease/steady-->
|Gini = 28.1 <!--number only-->
|Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.901
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=8 December 2012}}</ref><ref name="denonly" group="N" />
|HDI_rank = 15th
|established_event1 = [[History of Denmark#Middle Ages|Consolidation]]
|established_date1 = {{circa|10th century}}
|established_event2 =  {{nowrap|Democratisation <br>([[Constitution of Denmark|Constitutional Act]])}}
|established_date2 = 5 June 1849
|established_event3 =  [[Danish Realm]]
|established_date3 = 24 March 1948<ref group="N">Faroe Islands became the first territory to be granted [[home rule]] on 24 March 1948. Greenland also gained autonomy on 1 May 1979.</ref>
|currency = [[Danish krone]]<ref group="N">In the Faroe Islands the currency has a separate design and is known as the [[Faroese króna|króna]], but is not a separate currency.</ref>
|currency_code = DKK
|time_zone = [[Central European Time|CET]]
|utc_offset = +1
|time_zone_DST = [[Central European Summer Time|CEST]]
|utc_offset_DST = +2
|drives_on = right
|calling_code = [[Telephone numbers in Denmark|+45]]<ref group="N">The Faroe Islands ([[Telephone numbers in the Faroe Islands|+298]]) and Greenland ([[Telephone numbers in Greenland|+299]]) have their own country calling codes.</ref>
|cctld = [[.dk]]<ref group="N">The [[Top-level domain|TLD]] [[.eu]] is shared with other [[European Union]] countries. Greenland ([[.gl]]) and the Faroe Islands ([[.fo]]) have their own TLDs.</ref>
}}
