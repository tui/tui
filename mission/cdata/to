{{Infobox country
|conventional_long_name = Kingdom of Tonga
|native_name = ''Pule{{okina}}anga Fakatu{{okina}}i {{okina}}o Tonga''
|common_name = Tonga
|image_flag = Flag of Tonga.svg
|image_coat = Coat of arms of Tonga.svg
|national_motto = "Ko e {{okina}}Otua mo Tonga ko hoku tofi{{okina}}a"<br/>{{small|"God and Tonga are my Inheritance"}}
|national_anthem = ''[[Ko e fasi ʻo e tuʻi ʻo e ʻOtu Tonga|Ko e fasi {{okina}}o e tu{{okina}}i {{okina}}o e {{okina}}Otu Tonga]]''<br/>{{small|''The Song of the King of the Tongan Islands''}}
|image_map = Tonga on the globe (Polynesia centered).svg
|image_map2 = Tonga.jpg
|official_languages = {{unbulleted list |[[Tongan language|Tongan]] |[[English language|English]]}}
|demonym = Tongan
|capital = [[Nukuʻalofa|Nuku{{okina}}alofa]]
|latd=21 |latm=08 |latNS=S |longd=175 |longm=12 |longEW=W
|largest_city = capital
|government_type = [[Unitary state|Unitary]] [[parliamentary system|parliamentary]] [[constitutional monarchy]]
|leader_title1 = [[List of monarchs of Tonga|Monarch]]
|leader_name1 = [[Tupou VI|King Tupou VI]]
|leader_title2 = [[Prime Minister of Tonga|Prime Minister]]
|leader_name2 = {{nowrap|[[Sialeʻataongo Tuʻivakanō]]}}
|legislature = [[Legislative Assembly of Tonga|Legislative Assembly]]
|area_km2 = 748
|area_sq_mi = 289 <!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 186th
|area_magnitude = 1 E12
|percent_water = 4.0
|population_estimate_rank = 189th
|population_census = 103,036<ref>[http://web.archive.org/web/20120105120058/http://www.pmo.gov.to/press-releases/3220-tonga-national-population-census-2011-preliminary-count Tonga National Population Census 2011; Preliminary Count]. pmo.gov.to (22 December 2011).</ref>
|population_census_year = 2011
|population_density_km2 = 139
|population_density_sq_mi = 360 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 76th<sup>a</sup>
|GDP_PPP_year = 2011
|GDP_PPP = $763 million<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=64&pr.y=0&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=866&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Tonga |publisher=International Monetary Fund |accessdate=22 April 2012}}</ref>
|GDP_PPP_rank =
|GDP_PPP_per_capita = $7,344<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $439 million<ref name=imf2/>
|GDP_nominal_year = 2011
|GDP_nominal_per_capita = $4,220<ref name=imf2/>
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.710 <!--number only-->
|HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2010_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=5 November 2010}}</ref>
|HDI_rank = 95th
|sovereignty_type = Independence
|established_event1 = {{nowrap|from [[United Kingdom|British]] [[Protectorate|protection]]}}
|established_date1 = 4 June 1970
|currency = [[Tongan paʻanga|Pa{{okina}}anga]]
|currency_code = TOP
|utc_offset = [[UTC+13:00|+13]]
|DST_note = {{nowrap|[[Daylight saving time|DST]] not observed}}
|drives_on = left
|calling_code = [[+676]]
|cctld = [[.to]]
|footnote_a = Based on 2005 figures.
}}
