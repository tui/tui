{{Infobox country
|conventional_long_name =Romania
|native_name = {{native name|ro|România|icon=no}}
|common_name = Romania
|image_flag = Flag of Romania.svg
|image_coat = Coat of arms of Romania.svg
|image_map = EU-Romania.svg
|map_caption = Location of '''Romania''' (dark green): {{legend|#A9A9A9|on the European continent}} {{legend|#90EE90|in the [[European Union]]}}
|image_map2 = Romania - Location Map (2013) - ROU - UNOCHA.svg
|national_anthem = ''[[Deșteaptă-te, române!]]''<br/>{{small|''Awaken thee, Romanian!''}}<br/><center>[[File:Desteapta-te, romane!.ogg]]</center>
|official_languages = [[Romanian language|Romanian]]<ref>{{cite web|url=http://www.cdep.ro/pls/dic/site.page?den=act2_2&par1=1#t1c0s0a13 |title=Constitution of Romania |publisher=Cdep.ro |accessdate=2 October 2013}}</ref>
|ethnic_groups =
{{unbulleted list
| 88.9% [[Romanians]]
|  6.5% [[Hungarians in Romania|Hungarians]]
|  3.3% [[Roma minority in Romania|Roma]]
|  1.3% [[Minorities of Romania|other minorities]]
}}
|ethnic_groups_year = 2011<ref name="CensusRef">{{cite web |url=
http://www.recensamantromania.ro/wp-content/uploads/2013/07/REZULTATE-DEFINITIVE-RPL_2011.pdf|title=Romanian 2011 census (final results) |publisher=INSSE |accessdate=28 August 2012}} {{ro icon}}</ref>
|demonym = [[Romanians|Romanian]]
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]]<br>[[republic]]}}
|capital = {{Coat of arms|Bucharest}}
|latd=44 |latm=25 |latNS=N |longd=26 |longm=06 |longEW=E
|largest_city = capital
|leader_title1 = [[President of Romania|President]]
|leader_name1 = [[Traian Băsescu]]
|leader_title2 = [[Prime Minister of Romania|Prime Minister]]
|leader_name2 = [[Victor Ponta]]
|leader_title3 = [[President of the Senate of Romania|President of the Senate]]
|leader_name3 =  [[Călin Popescu-Tăriceanu]]
|leader_title4 = [[President of the Chamber of Deputies of Romania|President of the Chamber of Deputies]]
|leader_name4 = [[Valeriu Zgonea]]
|legislature = [[Parliament of Romania|Parliament]]
|upper_house = [[Senate (Romania)|Senate]]
|lower_house = [[Chamber of Deputies (Romania)|Chamber of Deputies]]
|area_rank = 83rd
|area_magnitude = 1_E+11
|area_km2 = 238,391
|area_sq_mi = 92,043 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 3
|population_census = 20,121,641<ref name="CensusRef"/>
|population_census_year = 20 October 2011
|population_census_rank = 58th
|population_density_km2 = 84.4
|population_density_sq_mi = 218.6 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 118th
|GDP_PPP_year = 2013
|GDP_PPP = $285.131 billion<ref>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?sy=2012&ey=2019&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=109&pr1.y=18&c=968&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=GDP, PPP (current international $) &#124; Data &#124; Table |publisher=International Monetary Fund |accessdate=13 June 2014}}</ref>
|GDP_PPP_rank =
|GDP_PPP_per_capita = $13,395<ref>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?sy=2012&ey=2019&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=109&pr1.y=18&c=968&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=GDP per capita, PPP (current international $) &#124; Data &#124; Table |publisher=International Monetary Fund |accessdate=13 June 2014}}</ref>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $189.659 billion<ref>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?sy=2012&ey=2019&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=109&pr1.y=18&c=968&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=GDP (current international $) &#124; Data &#124; Table |publisher=International Monetary Fund |accessdate=13 June 2014}}</ref>
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $8,910<ref>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?sy=2012&ey=2019&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=109&pr1.y=18&c=968&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=GDP per capita (current international $) &#124; Data &#124; Table |publisher=International Monetary Fund |accessdate=13 June 2014}}</ref>
|Gini_year = 2011
|Gini_change =  steady<!--increase/decrease/steady-->
|Gini = 33.2 <!--number only-->
|Gini_ref =<ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.786 <!--number only-->
|HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Summary.pdf |title=Human Development Report 2013 |year=2013 |publisher=United Nations |accessdate=28 August 2013}}</ref>
|HDI_rank = 56th
|sovereignty_type = [[History of Romania|Formation]]
|established_event1 = [[United Principalities]]<sup>a</sup>
|established_date1 = 24 January 1859
|established_event2 = {{nowrap|[[Romanian War of Independence|Independence]] from<br/>the [[Ottoman Empire]]}}
|established_date2 = 1877{{\}}1878<sup>b</sup>
|established_event3 = [[Kingdom of Romania]]
|established_date3 = 14 March 1881
|established_event4 = [[Union of Transylvania with Romania|Great Union]]<sup>c</sup>
|established_date4 = 1 December 1918
|established_event5 = [[Monarchy]] abolished and proclamation of the [[Romanian People's Republic]]
|established_date5 = 30 December 1947
|established_event6 = [[1965 Constitution of Romania|Constitution of the Socialist Republic of Romania]]
|established_date6 = 21 August 1965
|established_event7 = [[Romanian Revolution]]
|established_date7 = 16–27 December 1989
|established_event8 = [[Constitution of Romania|Current constitution of the third republic]]
|established_date8 = 21 November 1991 
|established_event9 = {{nowrap|[[2007 enlargement of the European Union|Accession]] to the [[European Union]]}}
|established_date9 = 1 January 2007
|currency = [[Romanian leu]]
|currency_code = RON
|time_zone = [[Eastern European Time|EET]]
|utc_offset = +2
|time_zone_DST = [[Eastern European Summer Time|EEST]]
|utc_offset_DST = +3
|drives_on = right
|calling_code = [[Telephone numbers in Romania|+40]]
| iso3166code  = RO
|cctld = [[.ro]]<sup>d</sup>
|footnote_a = The double election of [[Alexandru Ioan Cuza]] in [[Moldavia]] and [[Wallachia]] (respectively, 5 and 24 January 1859).
|footnote_b = Independence proclaimed on 9 May 1877, internationally recognised in 1878.
|footnote_c = The union of Romania with [[Bessarabia]], [[Bukovina]] and [[Transylvania]] in 1918.
|footnote_d = Also [[.eu]], shared with other [[European Union]] member states.
}}
