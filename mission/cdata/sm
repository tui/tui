{{Infobox country
|conventional_long_name = Most Serene Republic of San Marino
|native_name = {{native name|it|Serenissima Repubblica di San Marino|icon=no}}
|common_name = San Marino
|image_flag = Flag of San Marino.svg
|alt_flag = Flag of San Marino
|image_coat = Coat of arms of San Marino.svg
|alt_coat = Coat of arms of San Marino
|symbol_type = Coat of arms
|national_motto = {{native phrase|la|"Libertas"|italics=off}}<br/>{{small|"Freedom"}}
|national_anthem = ''[[Inno Nazionale della Repubblica]]''<br/>{{small|''National Anthem of the Republic''}}
|image_map = Location San Marino Europe.png
|alt_map = Location of San Marino in Europe
|map_caption = {{map caption |location_color=green |region=[[Europe]] |region_color=dark grey |legend=Location San Marino Europe.png}}
|image_map2 = San Marino - Location Map (2013) - SMR - UNOCHA.svg
|capital = [[City of San Marino]]
|latd=43| latm=56 |latNS=N |longd=12 |longm=26 |longEW=E <!--capital's latitude and longitude in deg/min/dir deg/min/dir-->
|largest_city = [[Dogana]]
|official_languages = [[Italian language|Italian]]
|ethnic_groups = [[Italians]]
|demonym = Sammarinese
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional republic]]
|leader_title1 = [[List of Captains Regent of San Marino|Captains Regent]]
|leader_name1 = [[Luca Beccari]]<br>[[Valeria Ciavatta]]
|legislature = [[Grand and General Council]]
|sovereignty_type = [[Independence]]
|established_event1 = from the [[Roman Empire]]
|established_date1 = 3 September 301<sup>a</sup>
|established_event2 = [[Constitution of San Marino|Constitution]]
|established_date2 = 8 October 1600
|area_rank = 227th
|area_magnitude =
|area = {{convert|61.2|km2|sqmi|abbr=on}}
|area_km2 = 61.2
|area_sq_mi =
|area_footnote = <ref name="cia"/>
|percent_water = 0
|population_estimate = 32,576<ref name="smpop">[http://www.statistica.sm/on-line/Home/DatiStatistici/Popolazione.html?Categoria=2&nodo=/on-line/Home/DatiStatistici/Popolazione/Strutturademografica.html Popolazione], Upeceds, Government of San Marino. Accessed on 30 April 2013.</ref>
|population_estimate_ran = 218th
|population_estimate_year = 2012 (31 July)
|population_census = |population_census_year =
|population_density_km2 = 520
|population_density_sq_mi =
|population_density_rank =
|GDP_PPP = {{nowrap|$1.17 billion}}<ref name="smgdp">[http://www.statistica.sm/contents/instance15/files/document/14001294ContoGenRedNaz_2.pdf Conto della Generazione dei Redditi Nazionali], Upeceds, Government of San Marino. Accessed on 6 June 2010.</ref><ref name="wdi">[http://data.worldbank.org/country/san-marino an Marino]. [[World Bank]]. Note: "PPP conversion factor, GDP (LCU per international $)" for Italy was used.</ref>
|GDP_PPP_rank = 177th
|GDP_PPP_year = 2008
|GDP_PPP_per_capita = $35,928<ref name="smgdp"/><ref name="wdi"/>
|GDP_PPP_per_capita_rank = 24th
|GDP_nominal = {{nowrap|{{US$|1.44}} billion}}<ref name="smgdp"/><ref name="wdi"/>
|GDP_nominal_rank = 163rd
|GDP_nominal_year = 2008
|GDP_nominal_per_capita = {{US$|44,208}}<ref name="smgdp"/><ref name="wdi"/>
|GDP_nominal_per_capita_rank = 15th
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2013
|HDI_change =  <!--increase/decrease/steady-->
|HDI = 0.875 <!--number only-->
|HDI_ref = <ref>[http://www.unescap.org/pdd/publications/workingpaper/wp_09_02.pdf Filling Gaps in the Human Development Index], United Nations ESCAP, February 2009</ref>
|HDI_rank = 26th
|currency = [[Euro]]
|currency_code = EUR
|time_zone = [[Central European Time|CET]]
|utc_offset = +1
|time_zone_DST = [[Central European Summer Time|CEST]]
|utc_offset_DST = +2
|DST_note =
|date_format =
|drives_on = right
|patron_saint = [[Saint Agatha]]
|calling_code = [[+378]] (+39 0549 calling via Italy)
|cctld = [[.sm]]
|footnote_a = By tradition.
|footnote_b = [[List of countries by Human Development Index#UN member states (latest UNDP data)]].
|footnotes = Sources: {{lower|0.4em|{{big|<ref name="cia"/><ref name="unece">{{cite web |url=http://www.unece.org/stats/profiles2009/san_marino.pdf |title=San Marino |work=UNECE Statistics Programme |year=2009 |publisher=[[UNECE]] |accessdate=13 March 2010}}</ref><!--end big:-->}}}}
}}
