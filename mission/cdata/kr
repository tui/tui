{{Infobox country
|conventional_long_name = {{collapsible list |titlestyle = background:transparent;text-align:center;font-size:90%; |title = {{big|Republic of Korea}} |liststyle = text-align:center;font-size:90%;font-weight:normal; |1 = {{unbulleted list|{{nobold|{{lang|ko|대한민국}}}}|{{nobold|{{lang|ko|大韓民國}}}}|''Daehan Minguk''}}}}
|common_name = South Korea
|image_flag = Flag of South Korea.svg
|alt_flag = Centered taegeuk on a white rectangle inclusive of four black trigrams
|image_coat = Emblem of South Korea.svg
|alt_coat = Centered taegeuk on a hibiscus syriacus surrounded by five stylized petals and a ribbon
|symbol_type = Emblem
|national_motto = {{native phrase|ko|"홍익인간"|italics=off}} {{small|(''[[de facto]]'')}}<ref>{{cite journal |url=https://web.archive.org/web/20140116074324/http://www.fssp.uaic.ro/argumentum/Numarul%2010%20%282%29/Articol%20Cozmiuc.pdf|page=6 |format=PDF |title=A New Way of Seeing Country Social Responsibility |work=Faculty of Philosophy and Social-Political Sciences |publisher=[[Alexandru Ioan Cuza University]] |accessdate=January 16, 2014}}</ref><br />{{small|"Benefit Broadly the Human World"}}
|national_anthem = {{native phrase|ko|"[[Aegukga|애국가]]"|italics=off}} {{small|(''[[de jure]]'')}}<br />{{small|"Patriotic Song"}}<br/><center>[[File:The National Anthem of the Republic of Korea.ogg]]</center>
|image_map = South Korea (orthographic projection).svg
|image_map2= South Korea - Location Map (2013) - KOR - UNOCHA.svg
|alt_map = Projection of Asia with South Korea in green
|map_width = 220px
|capital = [[Seoul]]
|latd = 37 |latm = 33 |lats = 59.53 |latNS = N |longd = 126 |longm = 58 |longs = 40.69 |longEW = E
|largest_city = capital
|official_languages = [[Korean language|Korean]]
|languages_type = [[Official script]]s
|languages = [[Hangul]]
|ethnic_groups =
|ethnic_groups_year = 
|demonym = {{nowrap|{{hlist|[[Demographics of South Korea|South Korean]]|[[Koreans|Korean]]}}}}
|government_type = {{nowrap|[[Unitary state|Unitary]] [[presidential system|presidential]]<br />[[constitutional republic]]}}
|leader_title1 = [[President of South Korea|President]]
|leader_name1 = [[Park Geun-hye]]
|leader_title2 = [[Prime Minister of South Korea|Prime Minister]]
|leader_name2 = [[Jung Hong-won]]
|legislature = [[National Assembly of South Korea|National Assembly]]
|sovereignty_type = [[Korean independence movement|Independence]]
|sovereignty_note = from [[Empire of Japan]]
|established_event1 = [[Division of Korea|Division]]
|established_date1 = August 15, 1945
|established_event2 = [[Constitution of the Republic of Korea|Constitution]]
|established_date2 = July 17, 1948
|established_event3 = [[First Republic of South Korea|Republic]]
|established_date3 = August 15, 1948
|area_rank = 109th
|area_magnitude = 1 E11
|area_km2 = 100,210
|area_sq_mi = 38,691
|percent_water = 0.3 (301&nbsp;km{{smallsup|2}}{{\}}116&nbsp;mi{{smallsup|2}})
|population_estimate = 50,219,669<ref>{{cite web |url=http://kosis.kr/gen_etl/install_miplatform_etc.jsp?orgId=101&amp;tblId=DT_1B35001&amp;scrId=&amp;seqNo=&amp;lang_mode=en&amp;obj_var_id=&amp;itm_id=&amp;path=&amp;vw_cd=MT_ETITLE&amp;list_id=&amp;url_param=http://kosis.kr:80/gen_etl/install_miplatform.jsp?orgId=101&amp;tblId=DT_1B35001&amp;vw_cd=MT_ETITLE&amp;list_id=&amp;empId=&amp;scrId=&amp;seqNo=&amp;dbkind=ETLDB&amp;lang_mode=en&amp;dsu=nsi&amp;conn_path=A6&amp;conn_path=A6 |title=2013 Estimate: Population of the Republic of Korea |publisher=Korean Statistical Information Service |language=[[Korean language|Korean]] |date=July 1, 2013 |accessdate=September 16, 2013}}</ref>
|population_estimate_rank = 26th
|population_estimate_year = 2013
|population_density_sq_km = 501.1
|population_density_sq_mi = 1,297.8
|population_density_rank = 13th
|GDP_PPP = {{nowrap|$1.755 trillion}}
|GDP_PPP_rank = 12th
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $34,777
|GDP_PPP_per_capita_rank = 26th
|GDP_PPP_per_capita_year = 2014
|GDP_nominal = {{nowrap|$1.271 trillion}}
|GDP_nominal_rank = 15th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $25,931
|GDP_nominal_per_capita_rank = 33rd
|GDP_nominal_per_capita_year = 2014
|Gini_year = 2011
|Gini = 31.1
|Gini_ref = <ref>{{cite encyclopedia |url=https://www.cia.gov/library/publications/the-world-factbook/rankorder/2172rank.html?countryname=Korea,%20South&countrycode=ks&regionCode=eas&rank=51#ks |title=Country Comparison :: Distribution of family income - Gini index |encyclopedia=[[The World Factbook]] |publisher=[[Central Intelligence Agency]] |location=[[Langley, Virginia|Langley]] |accessdate=November 11, 2013}}</ref>
|Gini_rank = 109th
|HDI_year = 2013
|HDI_change = increase
|HDI = 0.909
|HDI_ref = <ref>{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Statistics.pdf |format=PDF|title=2013 Human Development Index and its components – Statistics|publisher=[[United Nations Development Programme]] |page=6 |year=2013 |accessdate=September 16, 2013}}</ref>
|HDI_rank = 12th
|currency = {{nowrap|[[South Korean won]] (₩)<br /><code>([[ISO 4217|KRW]])</code>}}
|time_zone = [[Korea Standard Time]]
|utc_offset = +9
|time_zone_DST = {{nowrap|not observed}}
|utc_offset_DST = +9
|date_format = {{unbulleted list|yyyy년 mm월 dd일|yyyy/mm/dd ([[Common Era|CE]])}}
|drives_on = right
|calling_code = [[Telephone numbers in South Korea|+82]]
|cctld = {{hlist|[[.kr]]|[[.한국]]}}
}}
