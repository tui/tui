{{Infobox country
|conventional_long_name = Lao People's Democratic Republic
|native_name = {{unbulleted list |{{nobold|{{lang|lo|ສາທາລະນະລັດ ປະຊາທິປະໄຕ ປະຊາຊົນລາວ}}}} |''Sathalanalat Paxathipatai Paxaxon Lao''}}
|common_name = Laos
|image_flag = Flag of Laos.svg
|image_coat = Coat of arms of Laos.svg
|symbol_type = Emblem
|image_map = Location Laos ASEAN.svg
|map_caption = {{map caption |location_color=green |region=[[ASEAN]] |region_color=dark grey |legend=Location Laos ASEAN.svg}} 
|image_map2 = Laos - Location Map (2013) - LAO - UNOCHA.svg
|national_motto = ສັນຕິພາບ ເອກະລາດ ປະຊາທິປະໄຕ ເອກະພາບ ວັດທະນາຖາວອນ<br/>{{small|"Peace, independence, democracy, unity and prosperity"}}
|national_anthem = ''[[Pheng Xat Lao]]''<br/>{{small|''Lao National Anthem''}}<br/><center>[[File:National Anthem of Laos.ogg]]</center>
|official_languages = [[Lao language|Lao]]
|languages_type = Spoken languages
|languages = {{hlist|[[Lao language|Lao]]|[[Thai language|Thai]]|[[Hmong languages|Hmong]]}}
|demonym = [[Lao people|Laotian<br/>Lao]]
|ethnic_groups =
{{unbulleted list
| 55% [[Lao people|Lao]]
| 11% [[Khmu people|Khmu]]
|  8% [[Hmong people|Hmong]]
| 26% others<sup>a</sup>
}}
|ethnic_groups_year = 2005<ref name="cia.gov"/>
|capital = [[Vientiane]]
|latd=17 |latm=58 |latNS=N |longd=102 |longm=36 |longEW=E
|largest_city = capital
|government_type = [[Single-party state|Single-party]] [[Marxist-Leninist]] [[socialist republic]]
|leader_title1 = [[President of Laos|President]]
|leader_name1 = [[Choummaly Sayasone]]
|leader_title2 = [[Prime Minister of Laos|Prime Minister]]
|leader_name2 = [[Thongsing Thammavong]]
|leader_title3 = [[National Assembly of Laos|President of the<br/>National Assembly]]
|leader_name3 = [[Pany Yathotu]]
|leader_title4 = [[Lao Front for National Construction|President of<br/>Construction]]
|leader_name4 = [[Sisavath Keobounphanh]]
|legislature = [[National Assembly (Laos)|National Assembly]]
|sovereignty_type = Independence {{nobold|from [[France]]}}
|established_event1 = Autonomy
|established_date1 = 19 July 1949
|established_event2 = Declared
|established_date2 = 22 October 1953
|area_rank = 84th
|area_magnitude = 1 E11
|area_km2 = 236,800
|area_sq_mi = 91,428.991 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 2
|population_estimate = 6,695,166<ref name="Background notes - Laos">{{cite web |url=http://www.state.gov/r/pa/ei/bgn/2770.htm |title=Background notes – Laos |publisher=US Department of State |accessdate=20 January 2012}}</ref>
|population_estimate_year = 2013
|population_estimate_rank = 104th
|population_census = 4,574,848
|population_census_year = 1995
|population_density_km2 = 26.7
|population_density_sq_mi = <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 177th
|GDP_PPP_year = 2013
|GDP_PPP = $20.78 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/02/weodata/weorept.aspx?sy=2010&ey=2017&scsm=1&ssd=1&sort=country&ds=.&br=1&c=544&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=&pr.x=40&pr.y=6 |title=Report for Selected Countries and Subjects |work= World Economic Outlook Database |publisher=[[International Monetary Fund]] |date= October 2012 |accessdate=22 January 2013}}</ref> <!--Do not edit!-->
|GDP_PPP_rank =
|GDP_PPP_per_capita = $3,100<ref name=imf2/> <!--Do not edit!-->
|GDP_PPP_per_capita_rank =
|GDP_nominal_rank =
|GDP_nominal = $10.1 billion<ref name=imf2/> <!--Do not edit!-->
|GDP_nominal_year = 2013
|GDP_nominal_per_capita_rank =
|GDP_nominal_per_capita = $1,509<ref name=imf2/> <!--Do not edit!-->
|Gini_year = 2008
|Gini_change =  <!--increase/decrease/staedy-->
|Gini = 36.7 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank =
|HDI_year = 2013 <!--Please use the year in which the HDI data refers to and not the publication year-->
|HDI_change = increase <!--increase/decrease/staedy-->
|HDI = 0.543 <!--number only-->
|HDI_ref = <ref name="UN">{{cite web |url= http://hdr.undp.org/en/media/HDR2013_EN_Statistics.pdf |title= 2013 Human Development Report Statistics |work=Human Development Report 2013 |publisher=United Nations Development Programme |date= 14 March 2013 |accessdate= 10 April 2013}}</ref>
|HDI_rank = 138th
|currency = [[Lao kip|Kip]]
|currency_code = LAK
|time_zone =
|utc_offset = +7
|time_zone_DST = |utc_offset_DST =
|drives_on = right
|calling_code = [[+856]]
|iso3166code = LA
|cctld = [[.la]]
|footnote_a = Including [[List of ethnic groups in Laos|over 100 smaller ethnic groups]].
}}
