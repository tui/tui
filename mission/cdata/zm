{{Infobox country
| conventional_long_name = Republic of Zambia
| common_name = Zambia
| image_flag = Flag of Zambia.svg
| image_coat = Coat of Arms of Zambia.svg
| image_map = Zambia (orthographic projection).svg
| national_motto = <br/>"One Zambia, One Nation" <!--speechmarks, as it's a quotation-->
| national_anthem = <br/>{{nowrap|''[[Stand and Sing of Zambia, Proud and Free]]''}} <!--italics, as it's a title / first line-->
| official_languages = English
| regional_languages = {{hlist |[[Bemba language|Bemba]] |[[Tonga language (Zambia)|Tonga]] |[[Lozi language|Lozi]] |[[Lunda language|Lunda]] |[[Luvale language|Luvale]] |[[Kaonde]] |[[Nyanja]] |[[Chewa language|Chewa]]}}
| demonym = Zambian
| ethnic_groups =
{{unbulleted list
| 22% [[Bemba people|Bemba]]
| 11% [[Tonga people of Zambia and Zimbabwe|Tonga]]
|  5.2% [[Lozi people|Lozi]]
|  5.1% [[Nsenga]]
|  4.3% [[Tumbuka people|Tumbuka]]
|  3.8% [[Ngoni people|Ngoni]]
|  3% [[Chewa people|Chewa]]
|  1% [[White Africans of European ancestry|White]]
| 45% others<ref name="Time04">{{cite book | editor = Brunner, Borgna | title = TIME Almanac 2004|edition= 2004|pages= 875–905 | publisher = Pearson Education, Inc.| isbn= 1-931933-78-2}}</ref>
}}
| ethnic_groups_year = 2003
| capital = [[Lusaka]]
| latd=15 |latm=25 |latNS=S | longd=28 |longm=17 |longEW=E
| largest_city = capital
| government_type = [[Presidential system|Presidential]] [[republic]]
| leader_title1 = [[List of Presidents of Zambia|President]]
| leader_name1 = [[Michael Sata]]
| leader_title2 = [[List of Vice Presidents of Zambia|Vice-President]]
| leader_name2 = [[Guy Scott]]
| legislature = [[National Assembly (Zambia)|National Assembly]]
| area_km2 = 752,618
| area_sq_mi = 290,587 <!--Do not remove per [[WP:MOSNUM]]-->
| area_rank = 39th
| area_footnote =<ref name="area">{{cite web |url=http://unstats.un.org/unsd/demographic/products/dyb/DYB2004/Table03.pdf |title=Population by sex, rate of population increase, surface area and density |format=PDF |author=United Nations Statistics Division |accessdate=9 November 2007}}</ref>
| area_magnitude = 1 E11
| percent_water = 1
| population_estimate = 14,309,466<ref>{{cite web|url=http://www.zamstats.gov.zm/ |title=Welcome – Central Statistical Office, Zambia – national statistics from the Government of the Republic of Zambia |publisher=Zamstats.gov.zm |date= |accessdate=18 December 2012}}</ref>
| population_estimate_year = 2012
| population_estimate_rank = 70th
| population_census = 13,092,666<ref name="censubibpibjnii">{{cite web |url=http://www.zamstats.gov.zm/census.php |archiveurl=http://web.archive.org/web/20120121010214/http://www.zamstats.gov.zm/census.php |archivedate=2012-01-21 |title=2010 Census Population Summaries |author=Central Statistical Office, Government of Zambia |accessdate=13 November 2012}}</ref>
| population_census_year = 2010
| population_density_km2 = 17.2
| population_density_sq_mi = 44.5 <!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 191st
| GDP_PPP_year = 2012
| GDP_PPP = $23.967 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=41&pr.y=19&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=754&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Zambia |publisher=International Monetary Fund |accessdate=18 April 2013}}</ref>
| GDP_PPP_rank = 
| GDP_PPP_per_capita = $1,721<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $20.517 billion<ref name=imf2/>
| GDP_nominal_year = 2012
| GDP_nominal_per_capita = $1,473<ref name=imf2/>
| Gini_year = 2010
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 57.5 <!--number only-->
| Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
| Gini_rank =
| HDI_year = 2011
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.430 <!--number only-->
| HDI_ref =
| HDI_rank = 164th
| sovereignty_type = Independence
| established_event1 = from the [[United Kingdom]]
| established_date1 = 24 October 1964
| established_event2 = Current constitution
| established_date2 = 24 August 1991
| currency = [[Zambian kwacha]]
| currency_code = ZMW
| time_zone = [[Central Africa Time|CAT]]
| utc_offset = +2
| time_zone_DST = not observed
| utc_offset_DST = +2
| drives_on = [[Right- and left-hand traffic|left]]
| calling_code = [[+260]]
| cctld = [[.zm]]
}}
