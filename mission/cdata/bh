{{Infobox country
|conventional_long_name = Kingdom of Bahrain
|native_name = {{lang|ar|{{big|مملكة البحرين}}}}<br/>''{{transl|ar|Mamlakat al-Baḥrayn}}''
|common_name = Bahrain
|image_flag = Flag of Bahrain.svg
|alt_flag =
|image_coat = Coat of arms of Bahrain.svg
|symbol_type = Coat of arms
|national_motto = 
|national_anthem = ''[[Bahrainona]]''<br/>{{small|''Our Bahrain''}}<br/><center>[[File:Bahraini Anthem.ogg]]</center>
|royal_anthem = <!--''[[name of/link to anthem]]''-->
|image_map = Map of Bahrain.svg
|image_map2 = Bahrain - Location Map (2013) - BHR - UNOCHA.svg
|alt_map = 
|map_caption = {{map caption |location_color=green |region=Middle East |region_color=grey |legend=LocationBahrain.png}}
|capital = [[Manama]]
|latd=26 |latm=13 |latNS=N |longd=50 |longm=35 |longEW=E
|largest_city = capital
|official_languages = [[Arabic language|Arabic]]
|demonym = [[Bahraini (disambiguation)|Bahraini]]
|ethnic_groups =
{{unbulleted list
| 46% Bahraini 
| 4.7% other Arabs 
| 45.5% Asian 
| 1.6% African 
| 1% European 
| 1.2% Other }}
|ethnic_groups_year=2010<ref name="CIA"/>
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional monarchy]]
|leader_title1 = [[King of Bahrain|King]]
|leader_name1 = [[Hamad bin Isa Al Khalifa]]
|leader_title2 = [[Line of succession to the Bahraini throne|Crown Prince]]
|leader_name2 = [[Salman bin Hamad bin Isa Al Khalifa]]
|leader_title3 = [[Prime Minister of Bahrain|Prime Minister]]
|leader_name3 = [[Khalifa bin Salman Al Khalifa]]
|legislature = [[National Assembly of Bahrain|National Assembly]]
|upper_house = [[Consultative Council of Bahrain|Consultative Council]]
|lower_house = [[Council of Representatives of Bahrain|Council of Representatives]]
|sovereignty_type = [[Independence]]
|established_event1 = from [[Persia]]
|established_date1 = 1783
|established_event2 = [[History of Bahrain#Independent Bahrain|End of treaties with]] the [[United Kingdom]]
|established_date2 = 15 August 1971
|area_magnitude = 
|area_km2 = 780
|area_sq_mi = 304.5
|area_rank = 187th
|percent_water = 0
|population_estimate = 1,317,827<ref>http://data.worldbank.org/indicator/SP.POP.TOTL</ref>
|population_estimate_rank = 155th
|population_estimate_year = 2012
|population_census = |population_census_year = 
|population_density_km2 = 1,626.6   <!--Area in 2010 was 759 sq_km-->
|population_density_sq_mi = 4,212.8 <!--Area in 2010 was 293 sq_mi-->
|population_density_rank = 7th      <!--See [[List of countries by population density]]-->
|GDP_PPP = {{nowrap|$31.101 billion<ref name=imf2>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=419&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=&pr.x=86&pr.y=0 |title=Bahrain |publisher=International Monetary Fund |accessdate=17 April 2012}}</ref><!--end nowrap:-->}}
|GDP_PPP_rank = 91st
|GDP_PPP_year = 2012
|GDP_PPP_per_capita = $28,691<ref name="imf2" />
|GDP_PPP_per_capita_rank = 34th
|GDP_nominal = {{nowrap|$26.108 billion<ref name="imf2" />}}
|GDP_nominal_rank = 91st
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $23,555<ref name="imf2" />
|GDP_nominal_per_capita_rank = 32nd
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank = 
|HDI_year = 2013
|HDI_change = decrease <!--increase/decrease/steady-->
|HDI = 0.796<!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=19 January 2012}}</ref>
|HDI_rank = 48th
|currency = [[Bahraini dinar]]
|currency_code = BHD
|time_zone = AST
|utc_offset = +3
|time_zone_DST = 
|utc_offset_DST =
|DST_note = |utc_offset_DST = <!-- +N, where N is number of hours -->
|date_format = 
|drives_on = [[Right- and left-hand traffic|right]]
|calling_code = [[Telephone numbers in Bahrain|+973]]
|cctld = [[.bh]]
|footnote_a = 
}}
