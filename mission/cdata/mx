{{Infobox country
|conventional_long_name = United Mexican States
|native_name = {{unbulleted list|item_style=font-size:88%;
|{{native name|es|Estados Unidos Mexicanos}}}}
|common_name = Mexico
|image_flag = Flag of Mexico.svg
|alt_flag =
|image_coat = Coat of arms of Mexico.svg
|alt_coat =
|symbol_type = Coat of arms
|national_motto = None<ref>{{cite web|url=http://blogexperto.com/don_juan_zarlene/2009/06/17/el-lema-de-una-nacion/ |title=El lema de una nación |publisher=Blogexperto.com |date=2009-06-17 |accessdate=2013-07-17}}</ref>{{Verify credibility|date=July 2013}}
|national_anthem = ''[[Himno Nacional Mexicano]]''[[File:Himno Nacional Mexicano instrumental.ogg]]
|other_symbol_type = National [[Seal (device)|seal]]:
|other_symbol = <div style="padding:0.3em;">[[File:Seal of the Government of Mexico.svg|80px|link=Seal of the United Mexican States]]</div>{{native phrase|es|[[Seal of the United Mexican States|Sello de los Estados Unidos Mexicanos]]|nolink=on}}<br />{{small|Seal of the United Mexican States}}
|image_map = MEX orthographic.svg
|map_width = 220px
|alt_map =
|capital = [[Mexico City]]
|latd=19 |latm=26 |latNS=N |longd=99 |longm=08 |longEW=W
|largest_city = Mexico City
|languages_type = [[National language]]s
|languages = [[Spanish language|Spanish]] (98.8%),<ref>{{cite web|url=http://web.archive.org/web/20080102103605/http://www.inegi.gob.mx/prod_serv/contenidos/espanol/bvinegi/productos/censos/poblacion/2000/definitivos/Nal/tabulados/00li01.pdf}}</ref> 68 [[Languages of Mexico|native language groups]] are also legally recognized.<ref>{{cite web|url=http://www.inali.gob.mx/clin-inali/}}</ref>
|demonym = Mexican
|government_type = {{nowrap|[[Federalism|Federal]] [[Presidential system|presidential]]<br />{{raise|0.3em|[[constitutional republic]]<ref>{{cite web |format=PDF |location=MX |url=http://www.scjn.gob.mx/SiteCollectionDocuments/PortalSCJN/RecJur/BibliotecaDigitalSCJN/PublicacionesSupremaCorte/Political_constitucion_of_the_united_Mexican_states_2008.pdf |archiveurl=http://wayback.archive.org/web/20110511194922/http://www.scjn.gob.mx/SiteCollectionDocuments/PortalSCJN/RecJur/BibliotecaDigitalSCJN/PublicacionesSupremaCorte/Political_constitucion_of_the_united_Mexican_states_2008.pdf |archivedate=2011-05-11 |title=Political Constitution of the United Mexican States, title 2, article 40 |publisher=SCJN |accessdate=August 14, 2010}}</ref><!--end raise:-->}}<!--end nowrap:-->}}
|leader_title1 = [[President of Mexico|President]]
|leader_name1 = [[Enrique Peña Nieto]] [[Institutional Revolutionary Party|(PRI)]]
|leader_title2 = [[Senate of Mexico|President of the Senate]]
|leader_name2 = [[Ernesto Cordero Arroyo]] [[National Action Party (Mexico)|(PAN)]]
|leader_title3 = [[Chamber of Deputies (Mexico)|President of the Chamber of Deputies]]
|leader_name3 = [[Ricardo Anaya Cortés]] [[National Action Party (Mexico)|(PAN)]]
|leader_title4 = [[Supreme Court of Justice of the Nation|Supreme Court President]]
|leader_name4 = [[Juan N. Silva Meza|Juan Silva Meza]]
|leader_title5 = [[Secretariat of the Interior (Mexico)|Secretary of the Interior]]
|leader_name5 = [[Miguel Ángel Osorio Chong]] [[Institutional Revolutionary Party|(PRI)]]
|legislature = [[Congress of the Union|Congress]]
|upper_house = [[Senate of the Republic (Mexico)|Senate]]
|lower_house = [[Chamber of Deputies (Mexico)|Chamber of Deputies]]
|sovereignty_type = [[Mexican War of Independence|Independence]]
|sovereignty_note = from [[Spain]]
|established_event1 = [[Grito de Dolores|Declared]]
|established_date1 = September 16, 1810
|established_event2 = [[Declaration of Independence of the Mexican Empire|Consummated]]
|established_date2 = September 27, 1821
|established_event3 = [[Spanish American wars of independence#New Spain and Central America|Recognized]]
|established_date3 = December 28, 1836
|established_event4 = [[1824 Constitution of Mexico|First constitution]]
|established_date4 = October 4, 1824
|established_event5 = [[Federal Constitution of the United Mexican States of 1857|Second constitution]]
|established_date5 = February 5, 1857
|established_event6 = [[Constitution of Mexico|Current constitution]]
|established_date6 = February 5, 1917
|area_rank = 14th
|area_magnitude = 1 E12
|area =
|area_km2 = 1,972,550
|area_sq_mi = 761,606
|area_footnote =
|percent_water = 2.5
|population_estimate = 118,395,054<ref>{{cite web|url=http://www.conapo.gob.mx/es/CONAPO/Proyecciones |title=México, Proyecciones de Población |publisher=Consejo Nacional de Población (CONAPO) |accessdate=2013-09-09}}</ref>
|population_estimate_year = 2013
|population_estimate_rank = 11th
|population_density_km2 = 57
|population_density_sq_mi = 142
|population_density_rank = 142nd
|GDP_PPP = {{nowrap|$1.927 trillion<ref name="imf-mx">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=46&pr.y=1&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=273&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Mexico |publisher=International Monetary Fund |accessdate=June 29, 2014}}</ref><!--end nowrap:-->}}
|GDP_PPP_rank = 10th
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $16,111<ref name="imf-mx"/>
|GDP_PPP_per_capita_rank = 65th
|GDP_nominal = {{nowrap|$1.288 trillion<ref name="imf-mx"/>}}
|GDP_nominal_rank = 15th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $10,767<ref name="imf-mx"/>
|GDP_nominal_per_capita_rank = 65th
|Gini_year = 2010
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 47.2 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=[[World Bank]] |accessdate=2012-05-23}}</ref>
|Gini_rank =
|HDI_year = 2012
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.775 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url= http://hdr.undp.org/en/media/HDR2013_EN_Statistics.pdf |title= 2013 Human Development Report Statistics |work=Human Development Report 2013 |publisher=United Nations Development Programme |date= 14 March 2013 |accessdate= 16 March 2013}}</ref>
|HDI_rank = 61st
|currency = [[Mexican peso|Peso]]
|currency_code = MXN
|time_zone = ''See'' [[Time in Mexico]]
|utc_offset = −8 to −6
|time_zone_DST = varies
|DST_note =
|utc_offset_DST = −7 to −5
|antipodes =
|date_format =
|drives_on = right
|calling_code = [[+52]]
|iso3166code =
|cctld = [[.mx]]
|footnote_a = Article 4.° of the [[General Law of Linguistic Rights of the Indigenous Peoples]].<ref>{{cite web |url=http://www.inali.gob.mx/pdf/LGDLPI.pdf |title=General Law of Linguistic Rights of the Indigenous Peoples |accessdate=April 11, 2010 |author=INALI |date=March 13, 2003 |accessdate=November 7, 2010}}</ref>
}}
