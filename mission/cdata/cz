{{Infobox country
| conventional_long_name = Czech Republic
| native_name = ''{{lang|cs|Česká republika}}''
| common_name = Czech Republic
| linking_name = the Czech Republic<!--to link to "[Topic] of the Czech Republic" articles-->
| image_flag = Flag of the Czech Republic.svg
| image_coat = Coat of arms of the Czech Republic.svg
| image_map = EU-Czechia.svg
| map_caption = {{map_caption |countryprefix=the |location_color=dark green |region=Europe |region_color=dark grey |subregion=the European Union |subregion_color=green |legend=EU-Czechia.svg}}
| image_map2 = Czech Republic - Location Map (2013) - CZE - UNOCHA.svg
| national_motto = {{native phrase|cs|"[[Truth prevails|Pravda vítězí]]"|italics=off}}<br/>{{small|"Truth prevails"}}
| national_anthem = {{vunblist |{{native phrase|cs|[[Kde domov můj?]]|nolink=yes}} |{{raise|0.35em|{{small|''Where is my home?''}}&nbsp;{{lower|0.2em|<sup>a</sup>}}}} |{{center|[[File:Czech anthem.ogg]]}} }}
| official_languages = [[Czech language|Czech]]<ref>{{cite web |url=http://www.czech.cz/en/67019-czech-language |title=Czech language |work=Czech Republic – Official website |publisher=[[Ministry of Foreign Affairs (Czech Republic)|Ministry of Foreign Affairs of the Czech Republic]] |accessdate=14 November 2011}}</ref>
| languages_type = {{raise|0.3em |{{nobold|Officially recognised<ref name="languages">Citizens belonging to minorities, which traditionally and on long-term basis live within the territory of the Czech Republic, enjoy the right to use their language in communication with authorities and in front of the courts of law (for the list of recognized minorities see [http://www.vlada.cz/en/pracovni-a-poradni-organy-vlady/rnm/historie-a-soucasnost-rady-en-16666/ National Minorities Policy of the Government of the Czech Republic], Belorussian and Vietnamese since 4 July 2013, see [http://zpravy.idnes.cz/vietnamci-oficialni-narodnostni-mensinou-fiq-/domaci.aspx?c=A130703_133019_domaci_jj Česko má nové oficiální národnostní menšiny. Vietnamce a Bělorusy]). The article 25 of the Czech [[Charter of Fundamental Rights and Basic Freedoms]] ensures right of the national and ethnic minorities for education and communication with authorities in their own language. Act No. 500/2004 Coll. (''The Administrative Rule'') in its paragraph 16 (4) (''Procedural Language'') ensures, that a citizen of the Czech Republic, who belongs to a national or an ethnic minority, which traditionally and on long-term basis lives within the territory of the Czech Republic, have right to address an administrative agency and proceed before it in the language of the minority. In the case that the administrative agency doesn't have an employee with knowledge of the language, the agency is bound to obtain a translator at the agency's own expense. According to Act No. 273/2001 (''About The Rights of Members of Minorities'') paragraph 9 (''The right to use language of a national minority in dealing with authorities and in front of the courts of law'') the same applies for the members of national minorities also in front of the courts of law.</ref><ref>Slovak language may be considered an official language in the Czech Republic under certain circumstances, which is defined by several laws – e.g. law 500/2004, 337/1992. Source: http://portal.gov.cz. Cited: "Například Správní řád (zákon č. 500/2004 Sb.) stanovuje: "V řízení se jedná a písemnosti se vyhotovují v českém jazyce. Účastníci řízení mohou jednat a písemnosti mohou být předkládány i v jazyce slovenském&nbsp;..." (§16, odstavec 1). Zákon o správě daní a poplatků (337/1992 Sb.) "Úřední jazyk: Před správcem daně se jedná v jazyce českém nebo slovenském. Veškerá písemná podání se předkládají v češtině nebo slovenštině&nbsp;..." (§ 3, odstavec 1). http://portal.gov.cz</ref>}} }}
| languages =
{{collapsible list |titlestyle=background:none;text-align:left;font-weight:normal; |title=[[minority language]]s
|[[Slovak language|Slovak]] |[[German language|German]] |[[Polish language|Polish]] |[[Belarusian language|Belarusian]] |[[Bulgarian language|Bulgarian]] |[[Croatian language|Croatian]] |[[Greek language|Greek]] |[[Hungarian language|Hungarian]] |[[Romani language|Romani]] |[[Russian language|Russian]] |[[Rusyn language|Rusyn]] |[[Serbian language|Serbian]] |[[Ukrainian language|Ukrainian]] |[[Vietnamese language|Vietnamese]]
}}
| demonym = [[Czechs|Czech]]
| ethnic_groups =
{{vunblist
| 64% [[Czechs]]
|  26% unspecified
|  5% [[Moravians (ethnic group)|Moravians]]
|  1.4% [[Slovaks]]
|  0,4% [[Poles]]
}}
| ethnic_groups_year = 2014<ref>{{cite web|title=Czech Republic Population 2014|url=http://worldpopulationreview.com/countries/czech-republic-population/|publisher=World Population Review|accessdate=2014-04-14}}</ref> 
| religion = {{vunblist |{{longitem|style=line-height:1.15em|80% non-declared or non-religious}} |10.3% [[Catholic Church|Roman Catholic]]}}
| capital = [[Prague]]
| latd=50 |latm=05 |latNS=N |longd=14 |longm=28 |longEW=E
| largest_city = capital
| government_type = [[Parliamentary republic]]
| leader_title1 = [[President of the Czech Republic|President]]
| leader_name1 = [[Miloš Zeman]]
| leader_title2 = [[List of Prime Ministers of the Czech Republic|Prime Minister]]
| leader_name2 = [[Bohuslav Sobotka]]
| legislature = [[Parliament of the Czech Republic|Parliament]]
| upper_house = [[Senate of the Parliament of the Czech Republic|Senate]]
| lower_house = [[Chamber of Deputies of the Parliament of the Czech Republic|Chamber of Deputies]]
| sovereignty_type = [[History of the Czech Lands|Formation]]
| established_event1 = {{nowrap|[[History of the Czech lands in the Middle Ages|Principality of Bohemia]]}}
| established_date1 = [[Circa|c.]] 870
| established_event2 = [[Kingdom of Bohemia]]
| established_date2 = 1198
| established_event3 = [[Czechoslovakia]]
| established_date3 = 28 October 1918
| established_event4 = {{nowrap|[[Czech Socialist Republic]]}}
| established_date4 = 1 January 1969
| established_event5 = [[Dissolution of Czechoslovakia|Czech Republic]]
| established_date5 = 1 January 1993
|established_event6 = {{nowrap|[[2004 enlargement of the European Union|Joined]] the [[European Union]]}}
|established_date6 = 1 May 2004
| EUseats = 24
| area_rank = 116th
| area_magnitude = 1_E10
| area_km2 = 78,866
| area_sq_mi = 30,450 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = 2
| population_estimate = 10,513,209<ref>{{cite web |title=Population|url=http://www.czso.cz/eng/redakce.nsf/i/population |publisher=Czech Statistical Office |date=19 October 2012 |accessdate=19 December 2012}}</ref>
| population_estimate_year = Sep 2012
| population_estimate_rank = 81st
| population_census = 10,436,560<ref>[http://www.scitani.cz/sldb2011/eng/redakce.nsf/i/census_2011_basic_final_results/$File/SLDB-ZAKL-CR_en.xls Census of Population and Housing 2011: Basic final results]. [http://www.czso.cz/eng/redakce.nsf/i/home Czech Statistical Office]. Retrieved on 19 December 2012.</ref>
| population_census_year = 2011
| population_density_km2 = 134
| population_density_sq_mi = 341 <!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 84th
| GDP_PPP_year = 2014
| GDP_PPP = $295.891 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=26&pr.y=13&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=935&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Czech Republic |publisher=International Monetary Fund |accessdate=26 April 2014}}</ref>
| GDP_PPP_rank =
| GDP_PPP_per_capita = $28,086<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $198.537 billion<ref name=imf2/>
| GDP_nominal_rank =
| GDP_nominal_year = 2014
| GDP_nominal_per_capita = $18,846<ref name=imf2/>
| GDP_nominal_per_capita_rank =
| Gini_year = 2012
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 24.9 <!--number only-->
| Gini_ref = <ref name=eurogini>{{cite web |title=Gini coefficient of equivalised disposable income (source: SILC) |url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12 |publisher=Eurostat Data Explorer |accessdate=13 August 2013}}</ref>
| Gini_rank =
| HDI_year = 2013
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.873 <!--number only-->
| HDI_ref = <ref name="HDI">{{cite web |url=http://hdrstats.undp.org/en/countries/profiles/CZE.html |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=5 November 2011}}</ref>
| HDI_rank = 28th
| currency = [[Czech koruna]]
| currency_code = CZK
| time_zone = [[Central European Time|CET]]
| utc_offset = +1
| time_zone_DST = [[Central European Summer Time|CEST]]
| utc_offset_DST = +2
| drives_on = right
| calling_code = [[Telephone numbers in the Czech Republic|+420]]<sup>b</sup>
| cctld = [[.cz]]<sup>c</sup>
| footnote_a = The [[Rhetorical question|question is rhetorical]], implying "those places where my homeland lies".
<!--ORPHANED: 30 June 2010 (see {{Wayback |date=20101114043947 |url=http://www.czso.cz/eng/csu.nsf/informace/aoby091310.doc |title=Population changes}}).-->
<!--ORPHANED: Rank based on 2009 IMF data.-->
| footnote_b = Code 42 was shared with [[Slovakia]] until 1997.
| footnote_c = Also [[.eu]], shared with other European Union member states.
| patron_saint = [[Wenceslaus I, Duke of Bohemia|St Wenceslaus]]
}}
