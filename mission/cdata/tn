{{Infobox country
| conventional_long_name = Tunisian Republic
| native_name = {{ublist |<hr/>{{lang|ar|{{big|الجمهورية التونسية}}}} |''{{transl|ar|al-Jumhūriyyah at-Tūnisiyyah}}''}}
| common_name = Tunisia
| image_flag = Flag of Tunisia.svg
| image_coat = Coat of arms of Tunisia.svg
| symbol_type = Coat of arms
| image_map = Tunisia in its region.svg
| map_caption = Location of Tunisia in [[northern Africa]].
| national_motto = {{lang|ar|{{big|حرية، نظام، عدالة}}}}<br/>"{{transl|ar|Ḥurriyyah, Niẓām, ‘Adālah}}"<br/>{{small|"Liberty, Order, Justice"}}<ref name="art4">{{cite web |title=Tunisia Constitution, Article 4 |url=http://www.chambre-dep.tn/a_constit1.html |archiveurl=//web.archive.org/web/20060406143842/http://www.chambre-dep.tn/a_constit1.html |archivedate=6 April 2006 |publication-date=1957-07-25 |accessdate=23 December 2009}}</ref>
| national_anthem = ''[[Humat al-Hima]]''<br/>{{small|''Defenders of the Homeland''}}''<br/>{{center|[[File:Humat al-Hima.ogg]]}}
| official_languages = [[Arabic language|Arabic]]<ref name="art1">{{cite web |title=Tunisia Constitution, Article 1 |url=http://www.chambre-dep.tn/a_constit1.html|archiveurl=//web.archive.org/web/20060406143842/http://www.chambre-dep.tn/a_constit1.html|archivedate=6 April 2006| publication-date=1957-07-25 |accessdate=23 December 2009}} Translation by the University of Bern: "Tunisia is a free State, independent and sovereign; its religion is the Islam, its language is Arabic, and its form is the Republic."</ref>
| languages_type = Spoken languages
| languages = {{nowrap|[[Tunisian Arabic]] {{hlist |French<ref group="Notes" name="French">Commercial and [[lingua franca]]; see {{cite web |title=Tunisia|url=https://www.cia.gov/library/publications/the-world-factbook/geos/ts.html |publisher=CIA World Factbook |accessdate=15 October 2012 |archiveurl=http://www.webcitation.org/6BRmFRbow |archivedate=15 October 2012}}</ref> |[[Berber languages|Berber]]}}}}
| official_religion = [[Sunni Islam]]
| ethnic_groups = {{ublist |97% [[Arab-Berber]] |3% others}}
| ethnic_groups_year = 2003
| demonym = Tunisian
| capital = [[Tunis]]
| latd=36 |latm=50 |latNS=N |longd=10 |longm=9 |longEW=E
| largest_city = capital
| government_type = {{nowrap|[[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]]}} [[republic]]<ref name="art1"/>
| leader_title1 = [[President of Tunisia|President]]
| leader_name1 = [[Moncef Marzouki]]
| leader_title2 = [[Prime Minister of Tunisia|Prime Minister]]
| leader_name2 = [[Mehdi Jomaa]]
| legislature = [[Constituent Assembly of Tunisia|Constituent Assembly]]
| area_rank = 93rd
| area_magnitude = 1 E11
| area_km2 = 163610
| area_sq_mi = 63170
| percent_water = 5.0
| population_estimate = 10,777,500<ref name="ins">{{cite web |url=http://www.ins.nat.tn/indexen.php |title=National Institute of Statistics-Tunisia |publisher=National Institute of Statistics-Tunisia|date= |accessdate=13 September 2013}}</ref>
| GDP_PPP = $105.347 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=38&pr.y=8&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=744&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Tunisia |publisher=International Monetary Fund |accessdate=18 April 2013}}</ref>
| GDP_PPP_rank = 
| GDP_PPP_year = 2012
| GDP_PPP_per_capita = $9,774<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $45.611 billion<ref name=imf2/>
| GDP_nominal_year = 2012
| GDP_nominal_per_capita = $4,232<ref name=imf2/>
| GDP_nominal_per_capita_rank = 
| population_estimate_rank = 77th
| population_estimate_year = 2012
| population_census = | population_census_year =
| population_density_km2 = 63 <!--pop est 2011 (164,418 sq km) / land area(163610) = 63,482 sq mi-->
| population_density_sq_mi = 163
| population_density_rank = 133rd
| sovereignty_type = [[History of Tunisia|Formation]]
| established_event1 = [[Husainid Dynasty]] inaugurated
| established_date1 = 15 July 1705
| established_event2 = Independence from [[French Fourth Republic|France]]
| established_date2 = 20 March 1956
| established_event3 = [[History of modern Tunisia|Republic declared]]
| established_date3 = 25 July 1957
| established_event4 = [[Tunisian Revolution|Revolution Day]]
| established_date4 = 14 January 2011
| Gini_year = 2010
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 36.1 <!--number only-->
| Gini_ref = <ref>{{cite web|url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=GINI index |publisher=World Bank |date= |accessdate=19 January 2013}}</ref>
| HDI_year = 2013
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.712<!--number only-->
| HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=5 November 2011}}</ref>
| HDI_rank = 94th
| currency = [[Tunisian dinar]]
| currency_code = TND
| country_code = +216
| time_zone = [[Central European Time|CET]]
| utc_offset = +1
| time_zone_DST = | utc_offset_DST = 
| drives_on = right
| calling_code = [[+216]]
| cctld = {{ublist |[[.tn]] |{{rtl-lang|ar|[[.تونس]]}}<ref name="iana">{{cite web |url=http://www.iana.org/reports/2010/tunis-report-16jul2010.html |title=Report on the Delegation of تونس. |year=2010 |publisher=Internet Corporation for Assigned Names and Numbers |accessdate=8 November 2010}}</ref>}}
}}
