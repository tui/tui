{{Infobox country
|conventional_long_name = Republic of Namibia
|native_name = <!--{{unbulleted list |{{native name|af|Republiek van Namibië}} |{{native name|de|Republik Namibia}}}}-->
|common_name = Namibia
|image_flag = Flag of Namibia.svg
|image_coat = Coat of Arms of Namibia.svg
|image_map = Location Namibia AU Africa.svg
|image_map2= Namibia - Location Map (2013) - NAM - UNOCHA.svg
|map_caption = {{map caption|countryprefix=|location_color=dark blue|region=Africa|region_color=dark grey|subregion=the [[African Union]]|subregion_color=light blue}}
|national_motto = "Unity, Liberty, Justice"
|national_anthem = ''[[Namibia, Land of the Brave]]''
|official_languages = [[English language|English]]
|regional_languages = {{unbulleted list |[[Afrikaans]] |[[German language|German]] |[[Kwangali language|Rukwangali]] |[[Silozi]] |[[Setswana]] |[[Khoekhoe language|Damara/Nama]] |[[Herero language|Herero]] |[[Oshiwambo language|Oshiwambo]]}}
|ethnic_groups =
{{unbulleted list
| 49.8% [[Ovambo people|Ovambo]]
|  9.3% [[Kavango people|Kavango]]
|  7.5% [[Damara (people)|Damara]]
|  7.5% [[Herero people|Herero]]
|  6.4% [[Whites in Namibia|White]]
|  4.8% [[Nama people|Nama]]
|  4.1% [[Coloureds|Coloured]]
|  3.7% [[Caprivi Strip|Caprivian]]
|  2.9% [[San people|San]]
|  2.5% [[Basters]]
|  0.6% [[Tswana people|Tswana]]
|  0.9% others
}}
|ethnic_groups_year = 2000
|demonym = Namibian
|capital = [[Windhoek]]
|latd=22 |latm=34.2 |latNS=S |longd=17 |longm=5.167 |longEW=E
|largest_city = capital
|government_type = [[Unitary state|Unitary]] [[presidential system|presidential]] [[Constitution of Namibia|constitutional]] [[republic]]
|leader_title1 = [[List of Presidents of Namibia|President]]
|leader_name1 = [[Hifikepunye Pohamba]]
|leader_title2 = [[Prime Minister of Namibia|Prime Minister]]
|leader_name2 = [[Hage Geingob]]
|area_rank = 34th
|area_magnitude = 1 E11
|area_km2 = 825,615
|area_sq_mi = 318,696 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = negligible
|population_estimate = |population_estimate_rank = |population_estimate_year =
|population_census = 2,113,077<ref name=geo>{{cite web |url=http://www.geohive.com/cntry/namibia.aspx |title=GeoHive – Namibia population |publisher=GeoHive |accessdate=12 December 2013}}</ref>
|population_census_year = 2011
|population_density_km2 = 2.54
|population_density_sq_mi = 6.6 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 235th
|GDP_PPP = $18.800&nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=19&pr.y=1&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=728&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Namibia |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
|GDP_PPP_rank =
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $8,577<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $13.064&nbsp;billion<ref name=imf2/>
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $5,961<ref name=imf2/>
|legislature = [[Parliament of Namibia|Parliament]]
|upper_house = [[National Council (Namibia)|National Council]]
|lower_house = [[National Assembly (Namibia)|National Assembly]]
|sovereignty_type = Independence
|established_event1 = from [[South Africa]]
|established_date1 = 21 March 1990
|established_event2 = [[Constitution of Namibia|Constitution]]
|established_date2 = 12 March 1990
|Gini_year = 2009
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 59.7 <!--number only-->
|Gini_ref =<ref name=undppov>{{cite web|title=Economic Policy and Poverty Unit|url=http://www.undp.org.na/economic-policypoverty.aspx|publisher=UNDP Namibia|accessdate=10 September 2013}}</ref> 
|Gini_rank = 
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.608 <!--number only-->
|HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2010_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=5 November 2010}}</ref>
|HDI_rank = 128th
|currency = [[Namibian dollar]]
|currency_code = NAD
|country_code =
|time_zone = [[West Africa Time|WAT]]
|utc_offset = +1
|time_zone_DST = [[West Africa Summer Time|WAST]]
|utc_offset_DST = +2
|drives_on = left
|calling_code = [[+264]]
|cctld = [[.na]]
}}
