{{Infobox country
| conventional_long_name = Republic of Estonia
| native_name = {{native name|et|Eesti Vabariik|icon=no}}
| common_name = Estonia
| national_anthem =
{{unbulleted list
| ''[[Mu isamaa, mu õnn ja rõõm]]''
| {{small|''My Fatherland, My Happiness and Joy''}}
| <center>[[File:US Navy band - National anthem of Estonia.ogg]]</center>
}}
| image_flag = Flag of Estonia.svg
| image_coat = Coat of arms of Estonia.svg
| image_map = EU-Estonia.svg
| map_caption = {{map_caption |location_color=dark green |region=Europe |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green |legend=EU-Estonia.svg}}
| image_map2 = Estonia - Location Map (2013) - EST - UNOCHA.svg
| capital = [[File:Coat of arms of Tallinn (small).svg|x24px]] [[Tallinn]]
| latd=59 |latm=25 |latNS=N |longd=24 |longm=45 |longEW=E
| largest_city = capital
| official_languages = [[Estonian language|Estonian]]<sup>a</sup>
| regional_languages = {{flatlist|{{unbulleted list |[[Võro language|Võro]] |[[Setu language|Setu]]}}}}
| ethnic_groups =
{{unbulleted list
| 68.7% [[Estonians]]<sup>b</sup>
| 24.8% [[Russians in Estonia|Russians]]
|  1.7% [[Ukrainians]]
|  1.0% [[Belarusians]]
|  0.6% [[Finns]]
|  0.2% [[Tatars]]
|  0.2% [[Jews]]
|  0.1% [[Latvians]]
|  2.7% [[Demographics of Estonia|Others]]
}}
| ethnic_groups_year = 2013<ref>{{cite web|title=Population and housing census&nbsp;– Population and Housing Census 2011&nbsp;– Preliminary data|url=http://www.stat.ee/sdb-update?db_update_id=13545|work=Database Update 17|publisher=Statistics Estonia|accessdate=16 January 2013}}</ref>
| demonym = Estonian
| government_type = [[Parliamentary republic]]
| leader_title1 = [[President of Estonia|President]]
| leader_name1 = {{nowrap|[[Toomas Hendrik Ilves]]}}
| leader_title2 = [[Prime Minister of Estonia|Prime Minister]]
| leader_name2 = [[Taavi Rõivas]]
| legislature = ''[[Riigikogu]]''
| sovereignty_type = [[History of Estonia|Independence]]
| established_event4 = {{nowrap|[[Autonomous Governorate of Estonia|Autonomy declared]]}}
| established_date4 = 12 April 1917
| established_event5 = {{nowrap|[[Estonian Declaration of Independence|Independence declared]]<br/>then [[History of Estonia#Road to Republic|recognised]]}}
| established_date5 = 24 February 1918<br/>2 February 1920
| established_event6 = {{nowrap|[[Occupation of the Baltic states|Foreign occupation]]}}
| established_date6 = 1940–1991
| established_event7 = {{nowrap|[[History of Estonia#Regaining independence|Independence restored]]}}
| established_date7 = 20 August 1991
|established_event8 = {{nowrap|[[2004 enlargement of the European Union|Joined]] the [[European Union]]}}
|established_date8 = 1 May 2004
| area_km2 = 45,227
| area_sq_mi = 17,413<!--Do not remove per [[WP:MOSNUM]]-->
| area_rank = 132nd<sup>d</sup>
| area_magnitude = 1 E10
| percent_water = 4.45%
| population_estimate = 1,315,819<ref>{{cite web |url=http://www.stat.ee/34277 |title=Statistics Estonia |publisher=Stat.ee |date=1 January 2014}}</ref>
| population_estimate_year = 2014
| population_estimate_rank = 154th
| population_census = 1,294,486<ref>{{cite web|title=PHC 2011 RESULTS|url=http://www.stat.ee/phc2011|publisher=Statistics Estonia|accessdate=16 January 2011}}</ref>
| population_census_year = 2011
| population_density_km2 = 29
| population_density_sq_mi = 75 <!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 181st
| GDP_PPP_year = 2014
| GDP_PPP = $29.944 billion<ref name=imf2>*[http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=43&pr.y=13&sy=1980&ey=2018&scsm=1&ssd=1&sort=country&ds=.&br=1&c=512%2C668%2C914%2C672%2C612%2C946%2C614%2C137%2C311%2C962%2C213%2C674%2C911%2C676%2C193%2C548%2C122%2C556%2C912%2C678%2C313%2C181%2C419%2C867%2C513%2C682%2C316%2C684%2C913%2C273%2C124%2C868%2C339%2C921%2C638%2C948%2C514%2C943%2C218%2C686%2C963%2C688%2C616%2C518%2C223%2C728%2C516%2C558%2C918%2C138%2C748%2C196%2C618%2C278%2C522%2C692%2C622%2C694%2C156%2C142%2C624%2C449%2C626%2C564%2C628%2C565%2C228%2C283%2C924%2C853%2C233%2C288%2C632%2C293%2C636%2C566%2C634%2C964%2C238%2C182%2C662%2C453%2C960%2C968%2C423%2C922%2C935%2C714%2C128%2C862%2C611%2C135%2C321%2C716%2C243%2C456%2C248%2C722%2C469%2C942%2C253%2C718%2C642%2C724%2C643%2C576%2C939%2C936%2C644%2C961%2C819%2C813%2C172%2C199%2C132%2C733%2C646%2C184%2C648%2C524%2C915%2C361%2C134%2C362%2C652%2C364%2C174%2C732%2C328%2C366%2C258%2C734%2C656%2C144%2C654%2C146%2C336%2C463%2C263%2C528%2C268%2C923%2C532%2C738%2C944%2C578%2C176%2C537%2C534%2C742%2C536%2C866%2C429%2C369%2C433%2C744%2C178%2C186%2C436%2C925%2C136%2C869%2C343%2C746%2C158%2C926%2C439%2C466%2C916%2C112%2C664%2C111%2C826%2C298%2C542%2C927%2C967%2C846%2C443%2C299%2C917%2C582%2C544%2C474%2C941%2C754%2C446%2C698%2C666&s=NGDPDPC&grp=0&a= April 2014 Edition, Gross Domestic Product per capita, current prices, U.S. dollars.]</ref>
| GDP_PPP_rank =
| GDP_PPP_per_capita = $23,213<ref name=imf2/>
| GDP_PPP_per_capita_rank = 47
| GDP_nominal = $24.284 billion<ref name=imf2/>
| GDP_nominal_rank =
| GDP_nominal_year = 2014
| GDP_nominal_per_capita = $20,179<ref name=imf2/>
| GDP_nominal_per_capita_rank = 41
| Gini_year = 2012
| Gini_change = increase <!--increase/decrease/steady-->
| Gini = 32.5 <!--number only-->
| Gini_ref =<ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=5 January 2014}}</ref>
| Gini_rank =
| HDI_year = 2013
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.846 <!--number only-->
| HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/reports/global/hdr2011/ |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=14 August 2011}}</ref>
| HDI_rank = 33rd
| currency = [[Euro]] ([[Euro sign|€]])<sup>e</sup>
| currency_code = EUR
| time_zone = [[Eastern European Time|EET]]
| utc_offset = +2
| time_zone_DST = [[Eastern European Summer Time|EEST]]
| utc_offset_DST = +3
| drives_on = right
| calling_code = [[Telephone numbers in Estonia|+372]]
| ISO_3166-1_alpha2 = EE
| ISO_3166-1_alpha3 = EST
| ISO_3166-1_numeric = ?
| alt_sport_code = EST
| vehicle_code = EST
| aircraft_code = EST
| cctld = [[.ee]]<sup>f</sup>
| footnote_a = According to the [[Constitution of Estonia|Constitution]], Estonian is the sole official language.<ref>[http://www.president.ee/en/republic-of-estonia/the-constitution/index.html#1 Constitution of the Republic of Estonia], 6th article</ref> In [[Võrumaa|southern counties]], [[Võro language|Võro]] and [[Seto language|Seto]] are spoken along with it. Russian is still unofficially spoken in [[Ida-Virumaa]] and Tallinn, due to the [[Demographics of Estonia|Soviet Union's program promoting mass immigration of urban industrial workers]] during the post-war period.
| footnote_b = Including 5.4% [[Võros]] and 0.93% [[Setos]].{{big|<ref>[http://www.postimees.ee/906060/vorokesed-ees-setod-jarel Võrokesed ees, setod järel]. postimees.ee (13 July 2012).</ref>}}
| footnote_c = [[Social Democratic Party (Estonia)|SDE]] member but [[nonpartisan]] while in office.
| footnote_d = {{convert|47549|km²|0|abbr=on}} were defined according to the [[Treaty of Tartu (Russian–Estonian)|Treaty of Tartu]] in 1920 between Estonia and Russia. Today, the remaining {{convert|2323|km²|0|abbr=on}} are still occupied and part of Russia. The ceded areas include most of the former [[Petseri County]] and areas behind the [[Narva river]] including [[Ivangorod]] (Jaanilinn).{{big|<ref>[[Territorial changes of the Baltic states#Actual territorial changes after World War II|Territorial changes of the Baltic states]] Soviet territorial changes against Estonia after World War II</ref>}} [[Pechory]] remains under Russian control.
| footnote_e = [[Estonian kroon]] (EEK) before 2011.
| footnote_f = Also [[.eu]], shared with other member states of the European Union.
}}
