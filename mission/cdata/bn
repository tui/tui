{{Infobox country
|conventional_long_name = Nation of Brunei, Abode of Peace
|native_name = {{vunblist |''{{lang|ms|Negara Brunei Darussalam}}'' |{{lower|0.1em|{{lang|ms-Arab|{{big|نڬارا بروني دارالسلام}}}}}}}}
|common_name = Brunei
|image_flag = Flag of Brunei.svg
|image_coat = Coat of arms of Brunei.svg
|symbol_type = Crest
|image_map = Brunei on the globe (Brunei centered).svg
|map_caption = {{map caption |location_color=red |region=none |region_color=none}}
|image_map2 = Brunei - Location Map (2013) - BRN - UNOCHA.svg
|national_motto = {{vunblist |{{big|الدائمون المحسنون بالهدى}} |{{nowrap|''Sentiasa membuat kebajikan dengan petunjuk Allah''}} |{{small|"Always in service with God's guidance"}}}}
|national_anthem = {{vunblist |''[[Allah Peliharakan Sultan]]'' |{{small|''God Bless the Sultan''}}}}
|official_languages = [[Malay language|Malay]]{{ref label|Malay|a|}}
|languages_type = Recognised
|languages = [[English language|English]]{{ref label|English|b|}}
|languages_sub = yes
|languages2_type = Other languages<ref>{{cite web |author= |url=https://www.ethnologue.com/country/BN/languages |title=Brunei |publisher=Ethnologue |date=1999-02-19 |accessdate=2013-12-30}}</ref><ref>{{cite web |url=http://borneobulletin.brunei-online.com/index.php/2013/10/25/call-to-add-ethnic-languages-as-optional-subject-in-schools/ |title=Call to add ethnic languages as optional subject in schools |accessdate=19 November 2013}}</ref>
|languages2 = {{hlist |[[Brunei Malay]] |[[Tutong language|Tutong]] |[[Kedayan]] |[[Belait language|Belait]] |[[Lun Bawang language|Murut]] |[[Dusun language|Dusun]] |[[Brunei Bisaya language|Bisaya]] |[[Melanau language|Melanau]] |[[Iban language|Iban]] |[[Penan language|Penan]]}}<!--begin hack-->
|- class=none
| colspan="2" | '''[[Official script]]s'''|| style="vertical-align:middle;" |{{vunblist |[[Malay alphabet]] |[[Jawi alphabet]]<ref>[http://www.bt.com.bn/news-national/2010/10/22/writing-contest-promotes-usage-history-jawi-script Writing contest promotes usage, history of Jawi script]. The Brunei Times (22 October 2010)</ref>}}<!--end hack-->
| ethnic_groups =
{{vunblist
| 66.3% [[Malay people|Malays]]
| 11.2% [[Chinese people|Chinese]]
| 3.4% [[Indigenous peoples|Indigenous]]
| 19.1% other
}}
| ethnic_groups_year = 2004<ref name=WorldFactbook>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/bx.html |title=The World Factbook |publisher=CIA}}</ref>
|demonym = Bruneian
|capital = [[Bandar Seri Begawan]]
|largest_city = [[Bandar Seri Begawan]]
|latd=4 |latm=53.417 |lats= |latNS=N |longd=114 |longm=56.533 |longs= |longEW=E
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Islamic state|Islamic]] [[Absolute monarchy|absolute<br />monarchy]]}}
|leader_title1 = [[List of Sultans of Brunei|Sultan]]
|leader_name1 = [[Hassanal Bolkiah]]
|leader_title2 = [[Crown Prince]]
|leader_name2 = [[Al-Muhtadee Billah]]
|legislature = [[Legislative Council of Brunei|Legislative Council]]
|sovereignty_type = [[History of Brunei|Formation]]
|established_event1 = [[Muhammad Shah of Brunei|Sultanate]]
|established_date1 = 14th century
|established_event2 = {{nowrap|[[British protectorate]]}}
|established_date2 = 1888
|established_event3 = {{nowrap|[[Independence]] from<br />the [[United Kingdom]]}}
|established_date3 = 1 January 1984
|area_rank = 172nd
|area_magnitude = 1 E9
|area_km2 = 5,765
|area_sq_mi = 2,226 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 8.6
|population_estimate = 415,717<ref name="cia.gov">{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/bx.html |title=The World Factbook |publisher=Cia.gov |date= |accessdate=2013-12-30}}</ref>
|population_estimate_rank = 175th
|population_estimate_year = {{nowrap|Jul 2013<ref name="cia.gov"/>}}
|population_density_km2 = 67.3
|population_density_sq_mi = 174.4 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 134th
|GDP_PPP = $21.907 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=62&pr.y=18&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=516&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Brunei |publisher=International Monetary Fund |accessdate=18 April 2012}}</ref>
|GDP_PPP_rank =
|GDP_PPP_year = 2012
|GDP_PPP_per_capita = $50,440<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal_year = 2012
|GDP_nominal = $17.092 billion<ref name=imf2/>
|GDP_nominal_rank =
|GDP_nominal_per_capita = $39,355<ref name=imf2/>
|GDP_nominal_per_capita_rank =
|Gini_year =
|Gini_change = <!--increase/decrease/steady-->
|Gini = <!--number only-->
|Gini_ref =
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.855 <!--number only-->
|HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Tables.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=5 November 2011}}</ref>
|HDI_rank = 30th
|currency = [[Brunei dollar]]
|currency_code = BND
|country_code =
|time_zone = BDT
|utc_offset = +8
|time_zone_DST = |utc_offset_DST =
|drives_on = left
|calling_code = [[+673]]{{ref label|Calling Code|c|}}
|cctld = [[.bn]]<ref name=hb>{{cite book |title=Brunei Sultan Haji Hassanal Bolkiah Mu'izzaddin Waddaulah Handbook |year=2011 |publisher=IBP USA |page=10 |url=http://books.google.com/?id=9q0_LcWREVMC&printsec=frontcover&dq=brunei#v=onepage&q=brunei&f=false |isbn=9781433004445 |accessdate=14 October 2013}}</ref>
|footnote_a = {{note|Malay}} Under Article 82: "Official language" of the Constitution of Brunei, [[Malay language|Malay]] is the official language.
|footnote_b = {{note|English}} Under Article 82: "Official language" of the Constitution of Brunei, [[English language|English]] is used in official documents (official documents are bilingual; Malay and English).<ref>{{cite web|url=http://www.agc.gov.bn/agc1/images/LOB/cons_doc/constitution_i.pdf|title=CONSTITUTION OF BRUNEI DARUSSALAM|publisher=Attorney General's Chambers Brunei Darussalam|accessdate=15 October 2013}}</ref>
|footnote_c = {{note|Calling Code}} Also 080 from [[East Malaysia]].
}}
