{{Infobox country
|conventional_long_name = Netherlands<!--Do not change from "Netherlands"; the Netherlands is only part of the Kingdom of the Netherlands (which has its own article).-->
|native_name            = {{native name|nl|Nederland|icon=no}}<!--Do not change from "Nederland"; the Netherlands is only part of the Kingdom of The Netherlands (which has its own article).-->
|common_name            = the Netherlands
|image_flag             = Flag of the Netherlands.svg
|image_coat             = Royal Coat of Arms of the Netherlands.svg
|image_map              = EU-Netherlands.svg
|map_width              = 250px
|map_caption            = {{map caption |countryprefix=the |country=[[European Netherlands]] |location_color=dark green |region=Europe |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green }}
|image_map2             = BES islands location map.svg
|map2_width             = 250px
|map_caption2           = {{map caption |countryprefix=the |country= [[Caribbean Netherlands|Dutch special municipalities]] |location_color=green}}
|national_motto         = {{native phrase|fr|"Je maintiendrai"|italics=off}}<br/><!--
-->{{native phrase|nl|"Ik zal handhaven"|italics=off}}<br/><!--
-->{{small|"I will uphold"}}{{#tag:ref|The official motto is in French. The literal translation into English is "I will maintain"; a better translation, however, is "I will hold firm" or "I will uphold" (namely, the integrity and independence of the territory).{{or|date=January 2013}}|group="nb"}}
|national_anthem        = {{native name|nl|"[[Wilhelmus]]"|nolink=yes|italics=off}}<br/>{{small|"'William"}}<br/><center>[[File:Wilhemus koor.ogg|noicon|center]]</center>
|official_languages     = [[Dutch language|Dutch]]
| regional_languages =
{{unbulleted list
| {{nowrap|[[West Frisian language|West Frisian]] {{small|([[Friesland]])}}}}
| {{nowrap|[[Limburgish language|Limburgish]] {{small|([[Limburg (Netherlands)|Limburg]])}}}}
| {{nowrap|[[Dutch Low Saxon]] {{Small|(North-east)}}}}
| {{nowrap|[[English language|English]] {{small|([[Sint Eustatius]]/[[Saba]])}}}}
| {{nowrap|[[Papiamento]] {{small|([[Bonaire]])}}{{#tag:ref|[[West Frisian language|Frisian]] ([[Friesland]]),<big><ref>{{cite web |url=http://wetten.overheid.nl/BWBR0002219/tekst_bevat_Fries/geldigheidsdatum_25-10-2010 |title=Wet gebruik Friese taal in het rechtsverkeer |publisher=wetten.nl |accessdate=25 October 2010 |language=Dutch}}</ref></big> [[Papiamento]] ([[Bonaire]])<big><ref name=languages>{{cite web|url=http://wetten.overheid.nl/BWBR0028063/tekst_bevat_taal%2Bin%2Bhet%2Bbestuurlijk%2Bverkeer/geldigheidsdatum_01-01-2011 |title=Invoeringswet openbare lichamen Bonaire, Sint Eustatius en Saba |language=Dutch |publisher=wetten.nl |accessdate=1 January 2011}}</ref></big> and English ([[Sint Eustatius]] and [[Saba]])<big><ref name=languages/></big> have a formal status in certain parts of the country. [[Dutch Low Saxon]] and [[Limburgish language|Limburgish]] are recognised as [[European Charter for Regional or Minority Languages|regional languages]] by the European Charter for Regional or Minority Languages.|group="nb"}}}}
}}
|ethnic_groups          =
{{unbulleted list
|  80.7% [[Dutch people|Dutch]]
|  5% other [[European Union|EU]]
|  2.4% [[Overseas Indonesian|Indonesians]]
|  2.2% [[Turks in the Netherlands|Turks]]
|  2% [[Dutch-Moroccans|Moroccans]]
|  2% [[Surinamese people in the Netherlands|Surinamese]]
|  0.8% [[Dutch Caribbean|Caribbean]]
|  4.8% others
}}
|ethnic_groups_year = 2008<ref name="cia-worldfactbook-nl"/>
|demonym = Dutch
|capital = [[Amsterdam]]<!--Do not change this without broad consensus-->{{#tag:ref|While [[Amsterdam]] is the constitutional capital, [[The Hague]] is the seat of the government.|group="nb"}}
|largest_city = capital
|latd=52 |latm=22 |latNS=N |longd=4 |longm=53 |longEW=E
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[democracy]] under [[constitutional monarchy]]
|leader_title1 = [[Monarchy of the Netherlands|Monarch]]
|leader_name1 = [[Willem-Alexander of the Netherlands|Willem-Alexander]]
|leader_title2 = [[Prime Minister of the Netherlands|Prime Minister]]
|leader_name2 = [[Mark Rutte]]
|membership_type = Sovereign state
|membership = {{flag|Kingdom of the Netherlands}}
|legislature = [[States General of the Netherlands|States General]]
|upper_house = [[Senate of the Netherlands|Senate]]
|lower_house = [[House of Representatives of the Netherlands|House of Representatives]]
|area_rank = 134th
|area_magnitude = 1 E10
|area_km2 = 41,543
|area_sq_mi = 16,039 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 18.41
|population_estimate = 16,819,595<ref name="pop-cbs">{{cite web |url=http://statline.cbs.nl/StatWeb/publication/?VW=T&DM=SLEN&PA=37943eng&LA=EN |title=Population and population dynamics; month, quarter and year |work=Centraal Bureau voor de Statistiek |accessdate=31 October 2013}}</ref>
|population_estimate_year = {{CURRENTYEAR}}
|population_estimate_rank = 63rd
|population_census =
|population_census_year =
|population_density_km2 = {{#expr:{{Data Netherlands|poptoday}} / 41526 round 1}}
|population_density_sq_mi = {{#expr:{{Data Netherlands|poptoday}} / 16033 round 1}} <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 24th
|GDP_PPP_year = 2014
|GDP_PPP = $717.146 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=22&pr.y=9&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=138&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=Netherlands |publisher=International Monetary Fund |accessdate=26 April 2014}}</ref>
|GDP_PPP_rank = 23rd
|GDP_PPP_per_capita = $42,586
|GDP_PPP_per_capita_rank = 12th
|GDP_nominal = $838.036 billion<ref name=imf2/>
|GDP_nominal_rank = 17th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $49,765
|GDP_nominal_per_capita_rank = 13th
|sovereignty_type = [[Independence]] {{nobold|from the [[Spanish Empire]]}}
|established_event1 = [[Act of Abjuration|Declared]]
|established_date1 = <!--Euro style dates per [[WP:MOSNUM]]:-->26 July 1581
|established_event2 = [[Peace of Westphalia|Recognised]]
|established_date2 = 30 January 1648
|established_event3 = [[Kingdom of the Netherlands]]
|established_date3 = 16 March 1815
|established_event4 = [[Charter for the Kingdom of the Netherlands|Formation of constituent&nbsp;country]]
|established_date4 = 15 December 1954
|established_event5 = {{nowrap|[[Enlargement of the European Union#Founding members|Founded]] the [[European Economic Community|EEC]]}} {{nowrap|(now the [[European Union|EU]])}}
|established_date5 = 1 January 1958
|Gini_year = 2011
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 25.8 <!--number only-->
|Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|Gini_rank = 111th
|HDI_year = 2012
|HDI_change = steady <!--increase/decrease/steady-->
|HDI = 0.921 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Statistics.pdf |title=Human Development Report 2013 |year=2013 |publisher=United Nations |accessdate=15 March 2013}}</ref>
|HDI_rank = 4th
|currency = {{unbulleted list |[[Euro]] ([[ISO 4217|EUR]]) |{{nowrap|[[US dollar]] (USD){{#tag:ref|The euro is used in the [[European Netherlands]] and replaced the [[Dutch guilder]] in 2002. The US dollar is used in the [[Caribbean Netherlands]] and replaced the [[Netherlands Antillean guilder]] in 2011.<ref>{{cite web |url=http://wetten.overheid.nl/BWBR0028551 |title=Wet geldstelsel BES |publisher=Dutch government |date=30 September 2010 |accessdate=11 January 2014}}</ref>|group="nb"}}}}}}
|country_code = [[NLD]]
|time_zone = [[Central European Time|CET]] ([[UTC]]+1){{#tag:ref|CET and CEST are used in the European Netherlands, and AST is used in the Caribbean Netherlands.|group="nb"}}<br>[[Atlantic Standard Time|AST]]
|utc_offset =-4
|time_zone_DST = [[Central European Summer Time|CEST]] ([[UTC]]+2)<br>[[Atlantic Standard Time|AST]]
|utc_offset_DST =-4
|drives_on = right
|calling_code = {{unbulleted list |[[Telephone numbers in the Netherlands|+31]] |[[Telephone numbers in Curaçao and the Caribbean Netherlands|+599]]{{#tag:ref|599 was the country code designated for the now dissolved [[Netherlands Antilles]]. The Caribbean Netherlands still use 599-7 (Bonaire), 599-3 (Sint Eustatius) and 599-4 (Saba).|group="nb"}}}}
|iso3166code = NL
|date_format = dd-mm-yyyy
|cctld = [[.nl]]{{#tag:ref|The [[.eu]] domain is also used, as it is shared with other [[European Union]] member states. [[.bq]] is designated, but not in use, for the [[Caribbean Netherlands]].|group="nb"}}
}}
