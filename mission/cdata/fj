{{Infobox country
|conventional_long_name = Republic of Fiji
|native_name = 
{{unbulleted list |item_style=font-size:88%;
| {{native name|fj|Matanitu Tugalala o Viti}}
| {{nobold|रिपब्लिक ऑफ फीजी}}{{nbsp|2}}{{small|{{nobold|([[Fiji Hindi]])}}}}
| ''Ripablik ăph Phījī''{{nbsp|2}}{{small|{{nobold|([[transliteration]])}}}}
}}
|common_name = Fiji
|image_flag = Flag of Fiji.svg
|image_coat = Coat of Arms Fiji.svg
|image_map = FJI orthographic NaturalEarth.svg
|image_map2 = Fiji - Location Map (2013) - FJI - UNOCHA.svg
|national_motto = {{native phrase|fj|"Rerevaka na Kalou ka Doka na Tui"|italics=off|nolink=yes}}<br/>{{small|"Fear God and honour the Queen"}}
|national_anthem = ''[[God Bless Fiji]]''{{small|}}<br/><center>[[File:Fiji National Anthem.ogg|God Bless Fiji]]</center>
|languages_type = [[Official language]]s<ref>{{cite web |url=http://www.servat.unibe.ch/icl/fj00000_.html |title=Section 4 of Fiji Constitution |publisher=www.servat.unibe.ch|accessdate=3 May 2009}}</ref>
|languages = {{unbulleted list|[[English language|English]]|[[Fijian language|Fijian]]|[[Fiji Hindi]]}}
|demonym = Fijian
|capital = [[Suva]]
|latd = 18
|latm = 10
|latNS = S
|longd = 178
|longm = 27
|longEW = E
|largest_city = [[Suva]]
|government_type = {{nowrap|[[Parliamentary system|Parliamentary republic]] run by<br/>[[Military junta]]}}
|leader_title1 = [[President of Fiji|President]]
|leader_name1 = [[Epeli Nailatikau]]
|leader_title2 = [[Prime Minister of Fiji|Prime Minister]]
|leader_name2 = [[Frank Bainimarama]]
|legislature = [[Parliament of Fiji|Parliament]]
|upper_house = [[Senate (Fiji)|Senate]]
|lower_house = [[House of Representatives (Fiji)|House of Representatives]]
|area_rank = 155th
|area_magnitude = 1 E10
|area_km2 = 18,274
|area_sq_mi = 7,056 <!-- Do not remove per [[WP:MOSNUM]] -->
|percent_water = negligible
|population_estimate = 858,038<ref name=unpop>{{cite journal |url=http://web.archive.org/web/20111113144319/http://www.statsfiji.gov.fj/Key%20Stats/Population/1.2%20pop%20by%20ethnicity.pdf |title=Annual official estimate |version=2008 revision |format=PDF |publisher=United Nations |year=2012 |accessdate= 31 May 2013}}</ref>
|population_estimate_rank = 161st
|population_estimate_year = 2012
|population_density_km2 = 46.4
|population_density_sq_mi = 120.3 <!-- Do not remove per [[WP:MOSNUM]] -->
|population_density_rank = 148th
|GDP_PPP = $4.250 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=26&pr.y=14&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=819&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Fiji |publisher=International Monetary Fund |accessdate=18 April 2012}}</ref>
|GDP_PPP_year = 2012
|GDP_PPP_per_capita = $4,728<ref name=imf2/>
|GDP_PPP_per_capita_rank = 
|GDP_nominal = $3.671 billion<ref name=imf2/>
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $4,083<ref name=imf2/>
|sovereignty_type = Independence
|established_event1 = from the [[United Kingdom]]
|established_date1 = 10 October 1970
|established_event2 = Republic
|established_date2 = 7 October 1987
|Gini_year = 2009
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 42.8 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank = 
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.702 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdrstats.undp.org/en/countries/profiles/FJI.html |title=Human Development Index – Fiji |year=2013 |publisher=United Nations |accessdate=31 May 2013}}</ref>
|HDI_rank = 96th
|currency = [[Fijian dollar]]
|currency_code = FJD
|country_code = 679
|time_zone = FJT
|utc_offset = +12
|time_zone_DST = FJST<ref>{{cite web |url=http://www.timeanddate.com/library/abbreviations/timezones/pacific/fjst.html |title=FJST &#8211; Fiji Summer Time |publisher=www.timeanddate.com |accessdate=24 October 2012}}</ref>
|utc_offset_DST = +13<ref>{{cite web |url=http://www.timeanddate.com/news/time/fiji-advances-clocks-nov-29.html|title=Fiji Reignites Daylight Saving Time on November 29 |publisher=timeanddate.com |date=10 November 2009 |accessdate=2 May 2010}}</ref>
|drives_on = left
|calling_code = [[+679]]
|cctld = [[.fj]]
}}
