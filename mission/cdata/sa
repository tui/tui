{{Infobox country
|conventional_long_name = Kingdom of Saudi Arabia
|native_name = {{lang|ar|{{big|المملكة العربية السعودية}}}}<br />''Al-Mamlakah al-Arabiyah as-Sa'ūdiyah''
|common_name = Saudi Arabia
|image_flag = Flag of Saudi Arabia.svg
|image_coat = Coat of arms of Saudi Arabia.svg
|symbol_type = Emblem
|image_map = Saudi Arabia (orthographic projection).svg
|national_motto = {{nowrap|{{big|لا إله إلا الله، محمد رسول الله}}}}<br />"{{transl|ar|Lā ʾilāha ʾillā l–lāh, Muḥammadun rasūlu l–lāh}}" <br />{{small|"There is no god but God; Muhammad is the messenger of God."<!--This phrasing is taken from the following Saudiembassy.net reference. Do not change it unless you have a better source. --><ref>{{cite web |url=http://www.saudiembassy.net/about/country-information/facts_and_figures/ |title=About Saudi Arabia: Facts and figures |publisher=The royal embassy of Saudi Arabia, Washington, D.C., [[USA]] |accessdate=6 June 2011}}</ref>{{efn|The [[Shahada]] is sometimes translated into English as "There is no god but Allah", using [[romanization]] of the [[Arabic language|Arabic]] word "''[[Allah]]''" instead of its translation. The Arabic word "''Allah''" literally translates as ''the God'', as the initial "Al-" is the definite article.<ref>{{cite web|url=http://www.pbs.org/empires/islam/faithgod.html |title=God |work=Islam: Empire of Faith |publisher=PBS|accessdate=18 December 2010}}</ref><ref>"Islam and Christianity", ''Encyclopedia of Christianity'' (2001): Arabic-speaking Christians and Jews also refer to God as ''Allāh''.</ref><ref>{{Cite encyclopedia | title=Allah | encyclopedia=Encyclopaedia of Islam Online | author=Gardet, L. }}</ref>}} ([[Shahada]])}}
|national_anthem = {{lang|ar|السلام الملكي}} (as an instrumental)<br />"[[National Anthem of Saudi Arabia|as-Salām al-Malakiyy]]" <br />{{small|"The Royal Salute"}}<br /><center>[[File:Aash Al Maleek instrumental.ogg|Aash Al Maleek (instrumental)]]</center>
|official_languages = [[Arabic language|Arabic]]<ref name="CIA World Factbook"/>
|demonym = {{unbulleted list |Saudi Arabian |Saudi {{small|(informal)}}}}
|religion = [[Islam]] {{small|(official)}}<ref name=irf2010/>
|official_religion = [[Sunni Islam]]<ref name=irf2010/>
|capital = [[Riyadh]]
|latd=24 |latm=39 |latNS=N |longd=46 |longm=46 |longEW=E
|largest_city = [[Riyadh]]
|government_type = [[Unitary state|Unitary]] [[Islamic state|Islamic]] [[absolute monarchy]]
|leader_title1 = [[King of Saudi Arabia|King]]
|leader_name1 = [[Abdullah of Saudi Arabia|Abdullah bin Abdulaziz]]
|leader_title2 = [[Crown Prince]]
|leader_name2 = [[Salman bin Abdulaziz Al Saud|Salman bin Abdulaziz]]
|leader_title3 = [[Crown Prince|Deputy Crown Prince]]<ref>{{cite news| url=http://www.economist.com/news/middle-east-and-africa/21600180-king-abdullah-appoints-second-line-throne-next-after-next | work=The Economist | title=Next after next… | date=5 April 2014}}</ref>
|leader_name3 = [[Muqrin bin Abdulaziz Al Saud|Muqrin bin Abdulaziz]]
|legislature = [[#Monarchy and royal family|None]]{{ref label|Consultative Assembly|a}}
|sovereignty_type = [[History of Saudi Arabia|Establishment]]
|established_event1 = [[Saudi Arabia#From the foundation of the State to the present|Kingdom founded]]
|established_date1 = 23 September 1932<ref>{{cite web |url=http://www.saudia-online.com/saudi_arabia.htm |title=Saudi Arabia the country in Brief |publisher=www.saudia-online.com |accessdate=3 March 2012}}</ref>
|area_rank = 13th
|area_magnitude = 1 E12
|area_km2 = 2,149,690<ref name="CIA World Factbook" />
|area_sq_mi = 870,000 <!-- Do not remove per [[WP:MOSNUM]] -->
|percent_water = 0.7
|population_estimate = 29,994,272<ref>{{cite web|url=http://www.cdsi.gov.sa/english/|title=Kingdom of Saudi Arabia – Central Department of Statistics & Information|accessdate=3 April 2014|year=2013}}</ref>
|population_estimate_year = 2013
|population_estimate_rank = 43rd
|population_census = |population_census_year =
|population_density_km2 = 12.3
|population_density_sq_mi = 31 <!-- Do not remove per [[WP:MOSNUM]] -->
|population_density_rank = 216th
|GDP_PPP_year = 2013
|GDP_PPP = $927.762 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/02/weodata/weorept.aspx?pr.x=49&pr.y=9&sy=2013&ey=2013&scsm=1&ssd=1&sort=country&ds=.&br=1&c=456&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Saudi Arabia |publisher=International Monetary Fund |accessdate=27 October 2013}}</ref>
|GDP_PPP_rank = 19th
|GDP_PPP_per_capita = $31,309<ref name=imf2/>
|GDP_PPP_per_capita_rank = 28th
|GDP_Real_Command-basis = 330.831 billion 1999 USD<ref name="KAPSARC" />
|GDP_Real_Command-basis_rank =
|GDP_Real_Command-basis_year = 2010
|GDP_Real_Command-basis_per_capita = 12,190 1999 USD<ref name="KAPSARC"/>
|GDP_Real_Command-basis_per_capita_rank =
|GDP_nominal = $718.472 billion<ref name=imf2/>
|GDP_nominal_rank = 19th
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $24,246<ref name=imf2/>
|GDP_nominal_per_capita_rank = 31st
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.782 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Tables.pdf |title=HDRO (Human Development Report Office) United Nations Development Programme |year=2011 |publisher=United Nations |accessdate=2 November 2011}}</ref>
|HDI_rank = 57th
|currency = [[Saudi riyal]] (SR)
|currency_code = SAR
|country_code = SAU
|utc_offset=+3
|time_zone=[[Arabia Standard Time|AST]]
|drives_on = Right
|calling_code = [[Telephone numbers in Saudi Arabia|+966]]
|cctld = {{unbulleted list |[[.sa]] |[[السعودية.]]}}
|footnote_a = {{note|Consultative Assembly}} Legislation is [[Rule by decree|by king's decree]]. The [[Consultative Assembly of Saudi Arabia|Consultative Assembly]] exists to advise the king.
}}
