{{Infobox country
|conventional_long_name = Republic of Angola
|native_name = {{native name|pt|República de Angola}}
|common_name = Angola
|image_flag = <!---DO NOT ADD the proposed flag; you WILL be reverted and warned!---> Flag of Angola.svg
|image_coat = Coat of arms of Angola.svg
|symbol_type = Emblem
|national_anthem = {{native name|pt|[[Angola Avante]]!}}<br/>{{small|''Forward Angola!''}}
|image_map = Angola (orthographic projection).svg
|image_map2 = Angola - Location Map (2013) - AGO - UNOCHA.svg
|capital = [[Luanda]]
|latd=8 |latm=50 |latNS=S |longd=13 |longm=20 |longEW=E
|largest_city = capital
|official_languages = [[Portuguese language|Portuguese]]
|national_languages = {{vunblist |[[Kongo language|Kikongo]] |[[Chokwe language|Chokwe]] |[[South Mbundu language|Umbundu]] |[[North Mbundu language|Kimbundu]] |[[Ganguela language|Ganguela]] |[[Kwanyama dialect|Kwanyama]]}}
|regional_languages = |languages_type = |languages =
|ethnic_groups =
{{vunblist
| 36% [[Ovimbundu]]
| 25% [[Northern Mbundu people|Ambundu]]
| 13% [[Bakongo]]
| {{nowrap|22% other African}}
|  2% [[Mestico|Mestiço]]
|  1% Chinese
|  1% [[White Africans of European ancestry|European]]
}}
|ethnic_groups_year = 2000
|demonym = Angolan
|government_type = {{nowrap|[[Unitary state|Unitary]] [[Dominant-party system|dominant-party]]<br/>[[Presidential system|presidential]] [[republic]]}}
|leader_title1 = [[President of Angola|President]]
|leader_name1 = {{nowrap|[[José Eduardo dos Santos]]}}
|leader_title2 = [[Vice President of Angola|Vice President]]
|leader_name2 = [[Manuel Vicente]]
|legislature = [[National Assembly (Angola)|National Assembly]]
|sovereignty_type = [[Angolan War of Independence|Independence]]
|established_event1 = from [[Portugal]]
|established_date1 = 11 November 1975
|area_rank = 23rd
|area_magnitude = 1 E12
|area_km2 = 1246700
|area_sq_mi = 481354
|percent_water = negligible
|population_estimate = 18,498,000<ref name=unpop>{{cite journal | url=http://www.un.org/esa/population/publications/wpp2008/wpp2008_text_tables.pdf | title=World Population Prospects, Table A.1| version=2008 revision | format=PDF | publisher=United Nations | author=Department of Economic and Social Affairs Population Division | year=2009 | accessdate=12 March 2009| archiveurl= http://web.archive.org/web/20090318041906/http://www.un.org/esa/population/publications/wpp2008/wpp2008_text_tables.pdf| archivedate= 18 March 2009 | deadurl= no}}</ref><ref>[http://www.google.com/publicdata/explore?ds=n4ff2muj8bh2a_&ctype=l&strail=false&nselm=h&met_y=POP&hl=en&dl=en#ctype=l&strail=false&nselm=h&met_y=POP&fdim_y=scenario:1&scale_y=lin&ind_y=false&rdim=world&idim=country:AO&hl=en&dl=en Population Forecast to 2060 by International Futures hosted by Google Public Data Explorer]</ref>
|population_estimate_rank =
|population_estimate_year = 2009
|population_census = |population_census_year =
|population_density_km2 = 14.8
|population_density_sq_mi = 38.4
|population_density_rank = 199th
|GDP_PPP = $139.059 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=74&pr.y=0&sy=2012&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=614&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Angola |publisher=International Monetary Fund |accessdate=26 April 2014}}</ref>
|GDP_PPP_rank = 64th
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $6,484<ref name=imf2/>
|GDP_PPP_per_capita_rank = 107th
|GDP_nominal = $129.785 billion<ref name=imf2/>
|GDP_nominal_rank = 61st
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $6,052<ref name=imf2/>
|GDP_nominal_per_capita_rank = 91st
|Gini_year = 2009
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 42.7 <!--number only-->
|Gini_ref =<ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.508 <!--number only-->
|HDI_rank = 148th
|currency = [[Angolan kwanza|Kwanza]]
|currency_code = AOA
|time_zone = [[West Africa Time|WAT]]
|utc_offset = +1
|time_zone_DST = not observed
|utc_offset_DST = +1
|drives_on = right
|calling_code = [[+244]]
|iso3166code =
|cctld = [[.ao]]
|footnote_a = <!--Orphaned: [[Kongo language|Kikongo]], [[Kimbundu]] and [[Umbundu]] languages.-->
}}
