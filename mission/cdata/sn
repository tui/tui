{{Infobox country
| conventional_long_name = Republic of Senegal
| native_name = {{native name|fr|République du Sénégal|fontsize=68%}}
| common_name = Senegal
| image_flag = Flag of Senegal.svg
| image_coat = Coat of arms of Senegal.svg
| image_map = Location Senegal AU Africa.svg
|map_caption = {{map caption|countryprefix=|location_color=dark blue|region=Africa|region_color=dark grey|subregion=the [[African Union]]|subregion_color=light blue}}
| national_motto = {{native phrase|fr|"Un Peuple, Un But, Une Foi"|italics=off}}<br>{{small|"One People, One Goal, One Faith"}}
| national_anthem = <br>{{nowrap|''[[Pincez Tous vos Koras, Frappez les Balafons]]''}}<br>{{small|''Everyone strum your [[Kora (instrument)|kora]]s, strike the [[balafon]]s''}}
| official_languages = French
| languages_type = {{nobold|Recognised regional languages}}<ref>"La langue officielle de la République du Sénégal est le Français. Les langues nationales sont le Diola, le Malinké, le Pulaar, le Sérère, le Soninké, le Wolof et toute autre langue nationale qui sera codifiée." [http://www.gouv.sn/textes/const_detail.cfm?numero=TITREPREMIER Official website of the Senegalese government]</ref>
| regional_languages = {{hlist |[[Wolof language|Wolof]] |[[Soninke language|Soninke]] |[[Serer language|Serer]] |[[Fula language|Fula]] |[[Maninka language|Maninka]] |[[Jola languages|Jola]]}}
| ethnic_groups =
{{unbulleted list
| 43.3% [[Wolof people|Wolof]]
| 23.8% [[Fula people|Fula]]
| 14.7% [[Serer people|Serer]]
|  3.7% [[Jola people|Jola]]
|  3.0% [[Mandinka people|Mandinka]]
|  1.1% [[Soninke people|Soninke]]
| {{nowrap|1.0% [[White Africans of European ancestry|European]]{{\}}[[Lebanese people|Lebanese]]}}
|  9.4% others
}}
| ethnic_groups_year = {{lower|0.4em|<ref name=cia/>}}
| demonym = Senegalese
| capital = [[Dakar]]
| latd=14 |latm=40 |latNS=N |longd=17 |longm=25 |longEW=W
| largest_city = capital
| government_type = {{nowrap|[[Semi-presidential system|Semi-presidential republic]]}}
| leader_title1 = [[List of Presidents of Senegal|President]]
| leader_name1 = [[Macky Sall]]
| leader_title2 = [[Prime Minister of Senegal|Prime Minister]]
| leader_name2 = [[Aminata Touré]]
| legislature = [[Parliament of Senegal|Parliament]]
| upper_house = [[Senate (Senegal)|Senate]]
| lower_house = [[National Assembly (Senegal)|National Assembly]]
| area_rank = 87th
| area_magnitude =
| area_km2 = 196,712<ref name=ANSD>{{fr icon}} [http://www.ansd.sn/senegal_indicateurs.html ANSD] Retrieved 2013-12-10.</ref>
| area_sq_mi = 76,000
| percent_water = 2.1
| population_estimate = 13,567,338<ref name="ANSD"/>
| population_estimate_rank = 67th
| population_estimate_year = 2013
| population_census = 9,967,299
| population_census_year = 2002
| population_density_km2 = 68.0
| population_density_sq_mi = 169.1
| population_density_rank  = 134th
| GDP_PPP = $26.574&nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=36&pr.y=16&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=722&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Senegal |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
| GDP_PPP_rank =
| GDP_PPP_year = 2012
| GDP_PPP_per_capita = $2,026<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal_rank =
| GDP_nominal = $13.864&nbsp;billion<ref name=imf2/>
| GDP_nominal_year = 2012
| GDP_nominal_per_capita = $1,057<ref name=imf2/>
| GDP_nominal_per_capita_rank =
| sovereignty_type = [[History of Senegal|Independence]]
| established_event1 = from France<sup>a</sup>
| established_date1 = 20 June 1960
| established_event2 = Withdrawal from<br/>the [[Mali Federation]]
| established_date2 = 20 August 1960
| Gini_year = 2011
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 40.3 <!--number only-->
| Gini_ref =<ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank =
| HDI_year = 2012
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.470 <!--number only-->
| HDI_ref =<ref name="HDI">{{cite web|url=http://hdr.undp.org/en/reports/global/hdr2013/|title= The 2013 Human Development Report – "The Rise of the South: Human Progress in a Diverse World"|publisher=[[Human Development Report|HDRO (Human Development Report Office)]] [[United Nations Development Programme]]|pages=144–147|accessdate=28 November 2013}}</ref>
| HDI_rank = 154th
| currency = [[West African CFA franc|CFA franc]]
| currency_code = XOF
| country_code =
| time_zone = [[Coordinated Universal Time|UTC]]
| utc_offset =
| time_zone_DST = | utc_offset_DST =
| drives_on = right
| calling_code = [[+221]]
| cctld = [[.sn]]
| footnote_a = With [[French Sudan]], as the [[Mali Federation]].
}}
