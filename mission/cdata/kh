{{Infobox country
|conventional_long_name = Kingdom of Cambodia
|native_name = {{unbulleted list |{{nobold|{{lang|km|ព្រះរាជាណាចក្រកម្ពុជា}}}}|''Preăh Réachéanachâk Kâmpŭchéa''}}
|common_name = Cambodia
|image_flag = Flag of Cambodia.svg
|image_coat = Royal Arms of Cambodia.svg
|symbol_type = Royal Arms
|image_map = Location Cambodia ASEAN.svg
|map_caption = {{map caption |location_color=green |region=[[ASEAN]] |region_color=dark grey |legend=Location Cambodia ASEAN.svg}}
|image_map2 = Cambodia - Location Map (2013) - KHM - UNOCHA.svg
|national_motto = <br/>[[File:CambodiaMotto.svg|160px]]<br/>"Nation, Religion, King"
|national_anthem = <br/>''[[Nokor Reach]]''<br/>{{small|''Majestic Kingdom''}} <center>[[File:United States Navy Band - Nokoreach.ogg]]</center>
|official_languages = [[Khmer language|Khmer]] 
|demonym = [[Cambodian]]
|languages_type = [[Official script]]
|languages = [[Khmer script]]
|usual_languages = {{unbulleted list |[[Khmer language|Khmer]] |[[French language|French]]}}
|official_religion = [[Theravada]] (Buddhist)
|ethnic_groups = 
{{unbulleted list
| 82%
[[Khmer people|Khmer]]
| 8% [[Vietnamese Cambodian|Vietnamese]]
| 1% [[Chinese Cambodian|Chinese]]
| 4% Other
}}
|ethnic_groups_year = 2013<ref name="CIACB"/>
|capital = [[Phnom Penh]]
|latd=11 |latm=33 |latNS=N |longd=104 |longm=55 |longEW=E
|largest_city = capital
|religion = [[Buddhism]] {{small|(official)}}
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] {{nowrap|[[constitutional monarchy]]}} 
|leader_title1 = [[King of Cambodia|King]] 
|leader_name1 = [[Norodom Sihamoni]]
|leader_title2 = [[Prime Minister of Cambodia|Prime Minister]]
|leader_name2 = [[Hun Sen]]
|legislature = [[Parliament of Cambodia|Parliament]]
|upper_house = [[Senate (Cambodia)|Senate]]
|lower_house = [[National Assembly (Cambodia)|National Assembly]]
|sovereignty_type = [[History of Cambodia|Formation]]
|established_event1 = [[Kingdom of Funan|Funan Kingdom]]
|established_date1 = 68
|established_event2 = [[Chenla|Chenla Kingdom]]
|established_date2 = 550
|established_event3 = [[Khmer Empire]]
|established_date3 = 802
|established_event4 = {{nowrap|[[Kingdom of Cambodia (1953–70)|Independence]] <br> from [[France]]}}
|established_date4 = 9 November 1953
|established_event5 = {{nowrap|[[1991 Paris Peace Accords|Paris Peace Accords]]}}
|established_date5 = 23 October 1991 
|established_event6 = {{nowrap|[[Modern Cambodia|Monarchy restored]]}}
|established_date6 = 24 September 1993
|area_rank = 88th
|area_magnitude = 
|area_km2 = 181,035
|area_sq_mi = 69,898 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 2.5
|population_estimate = 15,205,539<ref>[https://www.cia.gov/library/publications/the-world-factbook/geos/cb.html]</ref>
|population_estimate_year = 2013
|population_estimate_rank = 65th
|population_census = 13,388,910<ref>[http://www.nis.gov.kh/ Cambodian National Institute of Statistics], accessed 6 June 2012.</ref>
|population_census_year = 2008
|population_density_km2 = 81.8
|population_density_sq_mi = 211.8 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 118th
|GDP_PPP_year = 2013
|GDP_PPP = $43.20&nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/02/weodata/weorept.aspx?sy=2011&ey=2018&scsm=1&ssd=1&sort=country&ds=.&br=1&c=522&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=&pr.x=43&pr.y=15 |title=Cambodia |publisher=International Monetary Fund |accessdate=April 28, 2014}}</ref>
|GDP_PPP_rank = 
|GDP_PPP_per_capita = $2,776<ref name=imf2/>
|GDP_PPP_per_capita_rank = 
|GDP_nominal_rank = 
|GDP_nominal = $17.25&nbsp;billion<ref name=imf2/>
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $1,108<ref name=imf2/>
|GDP_nominal_per_capita_rank = 
|Gini_year = 2007
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 43 <!--number only-->
|Gini_ref = <ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of family income – Gini index |work=The World Factbook |publisher=CIA |accessdate=1 September 2009}}</ref>
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.543 <!--number only-->
|HDI_ref = <ref name="UN">{{cite web|url=http://hdrstats.undp.org/en/countries/profiles/khm.html |title=International Human Development Indicators – UNDP |publisher=Hdrstats.undp.org |accessdate=16 March 2013}}</ref>
|HDI_rank = 138th
|currency = [[Cambodian riel|Riel]]<sup>a</sup>
|currency_code = KHR
|time_zone = 
|utc_offset = +7
|drives_on = right
|calling_code = [[Telephone numbers in Cambodia|+855]]
|cctld = [[.kh]]
|national flower = [[Rumdul]]
|footnote_a = The [[de facto currency]] is the [[United States dollar]].{{larger|<ref name="dollar">{{cite web |author=[[Chinese University of Hong Kong]] |url=http://intl.econ.cuhk.edu.hk/exchange_rate_regime/index.php?cid=13 |title=Historical Exchange Rate Regime of Asian Countries: Cambodia |accessdate=21 February 2007}}<br/>[http://cambodia.ka-set.info/economics/news-dollar-de-dollarisation-currency-riel-huot-pum-081209.html Riel or dollar: which currency for Cambodia, in a context of crisis?]</ref>}}
}}
