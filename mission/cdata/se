{{Infobox country
| conventional_long_name = Kingdom of Sweden
| native_name = {{native name|sv|Konungariket Sverige|icon=no}}
| common_name = Sweden
| image_flag = Flag of Sweden.svg
| image_coat = Coat of Arms of Sweden.svg
| image_map = EU-Sweden.svg
| image_map2= Sweden - Location Map (2013) - SWE - UNOCHA.svg
| map_caption = {{map caption |location_color=dark green |region=Europe |region_color=dark grey |subregion=the [[European Union]] |subregion_color=green |legend=EU-Sweden.svg}}
| national_motto = {{small|[[Royal mottos of Swedish monarchs|(royal)]]}}&nbsp;"{{lang|sv|För Sverige – i tiden}}"&nbsp;{{ref label|ccc|c}}<br/>{{small|"For Sweden – With the Times"}}
| national_anthem = ''{{lang|sv|[[Du gamla, Du fria]]}}''&nbsp;{{ref label|ddd|d}}<br/>{{small|''Thou ancient, thou free''}}<br/><center>[[File:United States Navy Band - Sweden.ogg]]</center>
| royal_anthem = ''{{lang|sv|[[Kungssången]]}}''<br/>{{small|''Song of the King''}}
| official_languages = [[Swedish language|Swedish]]{{ref label|eee|e}}
| ethnic_groups = no official statistics{{ref label|fff|f}}
| demonym = {{hlist |[[Swedish people|Swedish]] |[[Swedes|Swede]]}}
| capital = {{Coat of arms|Stockholm}}
| latd=59 |latm=21 |latNS=N |longd=18 |longm=4 |longEW=E
| largest_city = capital
| government_type = {{nowrap|[[Unitary state|Unitary]] [[Parliamentary democracy|parliamentary]]<br>[[constitutional monarchy]]}}
| leader_title1 = [[Monarchy of Sweden|Monarch]]
| leader_name1 = [[Carl XVI Gustaf of Sweden|King Carl XVI Gustaf]]
| leader_title2 = [[Prime Minister of Sweden|Prime Minister]]
| leader_name2 = {{nowrap|[[Fredrik Reinfeldt]] ([[Moderate Party|M]])}}
| leader_title3 = [[Speaker of the Riksdag|Speaker of<br/>the ''Riksdag'']]
| leader_name3 = {{nowrap|[[Per Westerberg]] ([[Moderate Party|M]])}}
| legislature = ''[[Riksdag]]''
| established_event1 = [[Consolidation of Sweden|Consolidation]]
| established_date1 = Middle Ages
| EUseats = 19
| area_rank = 57th
| area_magnitude = 1 E+11
| area_km2 = 449,964
| area_sq_mi = 173,745
| percent_water = 8.7
| population = 
| population_estimate_rank = 88th
| population_census = 9,658,301<ref name="population"/>
| population_census_year = 2013
| population_density_km2 = 21.5
| population_density_sq_mi = 55.6
| population_density_rank = 195th
| GDP_PPP = {{nowrap|$393.774 billion<ref name="imf2">{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/02/weodata/weorept.aspx?pr.x=81&pr.y=7&sy=2013&ey=2013&scsm=1&ssd=1&sort=country&ds=.&br=1&c=144&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= |title=Sweden |publisher=International Monetary Fund |accessdate=27 October 2013}}</ref><!--end nowrap:-->}}
| GDP_PPP_rank = 34th
| GDP_PPP_year = 2013
| GDP_PPP_per_capita = $40,870<ref name="imf2"/>
| GDP_PPP_per_capita_rank = 14th
| GDP_nominal = {{nowrap|$552.042 billion<ref name="imf2"/>}}
| GDP_nominal_rank = 21st
| GDP_nominal_year = 2013
| GDP_nominal_per_capita = $57,297<ref name="imf2"/>
| GDP_nominal_per_capita_rank = 7th
| Gini_year = 2012
| Gini_change = increase  <!--increase/decrease/steady-->
| Gini = 24.9 <!--number only-->
| Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=4 January 2014}}</ref>
| Gini_rank = 
| HDI_year = 2013
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.916 <!--number only-->
| HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2013_EN_Table1.pdf |title=Human Development Report 2013 |year=2013 |publisher=United Nations |accessdate=14 March 2013}}</ref>
| HDI_rank = 7th
| currency = [[Swedish krona]]
| currency_code = SEK
| country_code = SWE
| time_zone = [[Central European Time|CET]]
| utc_offset = +1
| time_zone_DST = [[Central European Summer Time|CEST]]
| utc_offset_DST = +2
| date_format = yyyy-mm-dd
| drives_on = right{{ref label|ggg|g}}
| calling_code = [[Telephone numbers in Sweden|46]]
| cctld = [[.se]]{{ref label|hhh|h}}
| footnote_a = {{note|aaa}} Above the civil and state flag. Here the naval flag: <br><center>[[File:Naval Ensign of Sweden.svg|129px]]
| footnote_b = {{note|bbb}} Above the greater coat of arms. Here the lesser coat of arms:<br><center>[[File:Coat of arms of Sweden (Lesser).svg|60px]]
| footnote_c = {{note|ccc}} "{{lang|sv|För Sverige – I tiden}}" has been adopted by [[Carl XVI Gustaf of Sweden|Carl XVI Gustaf]] as his personal motto.
| footnote_d = {{note|ddd}} ''{{lang|sv|[[Du gamla, Du fria]]}}'' has never been officially adopted as national anthem, but is so by convention.
| footnote_e = {{note|eee}} Since 1 July 2009.<ref name="Swedish"/><ref name="Swedish2"/> Five other languages are [[Minority languages of Sweden|officially recognized as minority languages]]:<ref>{{cite web |url=http://www.sprakradet.se/servlet/GetDoc?meta_id=2119#item100400 |title=Är svenskan också officiellt språk i Sverige? |publisher=Språkrådet (Language Council of Sweden) |date=1 February 2008 |accessdate=22 June 2008 |language=Swedish}}</ref> [[Finnish language|Finnish]], [[Meänkieli]], [[Romani language|Romani]], [[Sami languages|Sami]], and [[Yiddish language|Yiddish]]. The [[Swedish Sign Language]] also has a special status.
| footnote_f = {{note|fff}} {{as of|2012|12|31|alt=On 31 December 2012}}, approximately 27% of the population had a full or partial foreign background.<ref name="SCB1">{{cite web |url=http://www.scb.se/Pages/TableAndChart____26041.aspx |title=Summary of Population Statistics 1960–2012 |publisher=[[Statistics Sweden]] |accessdate=9 June 2013}}</ref><ref name="NoteEthn">Note that [[Swedish-speaking Finns]] or other Swedish-speakers born outside Sweden might identify as ''Swedish'' despite being born abroad. Moreover, people born in Sweden may not be ethnic Swedes. As the [[Government of Sweden|Swedish government]] does not base any statistics on [[ethnicity]], there are no exact numbers on the [[Ethnicity|ethnic]] background of migrants and their descendants in Sweden. This is not, however, to be confused with migrants' [[Nationality|national backgrounds]], which are recorded.</ref>
| footnote_g = {{note|ggg}} Since [[Dagen H|3 September 1967]].
| footnote_h = {{note|hhh}} The [[.eu]] domain is also used, as it is shared with other [[European Union]] member states.
}}
