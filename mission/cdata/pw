{{Infobox country
|conventional_long_name = Republic of Palau
|native_name = ''Beluu er a Belau''
|common_name = Palau
|image_flag = Flag of Palau.svg
|image_coat = Seal of Palau.svg
|symbol_type = Seal
|image_map = Palau.png
|map_caption = Location of Palau (circled) in the western [[Pacific Ocean]].
|national_motto =
|national_anthem = {{lang|pau|''[[Belau rekid]]''}}<br>{{small|''Our Palau''}}</small>
|official_languages = {{unbulleted list |[[English language|English]] |[[Palauan language|Palauan]]}}
|regional_languages = {{unbulleted list |[[Japanese language|Japanese]] {{small|(in Angaur)}} |{{nowrap|[[Sonsorolese language|Sonsorolese]] {{small|(in Sonsoral)}}}} |[[Tobian language|Tobian]] {{small|(in Hatohobei)}}}}
|demonym = Palauan
|capital = [[Ngerulmud]], [[Melekeok|Melekeok State]]<sup>a</sup><ref>''"Government offices moved to a new National Capitol Building complex located at Ngerulmud, Melekeok State"'' [http://www.state.gov/outofdate/bgn/palau/119480.htm US Department of State. Palau (02/09)]. Retrieved 24 February 2013.</ref>
|latd=7 |latm=21 |latNS=N |longd=134 |longm=28 |longEW=E
|largest_city = [[Koror]]
|ethnic_groups =
{{unbulleted list
| 69.9% Palauan
| 15.3% [[Filipino people|Filipino]]
|  4.9% [[Chinese people|Chinese]]
|  2.4% [[Ethnic groups in Asia|other Asian]]
|  1.9% [[White people|White]]
|  1.4% [[Carolinian people|Carolinian]]
| {{nowrap|1.1% [[Micronesian people|other Micronesian]]}}
| {{nowrap|3.2% other{{\}}unspecified}}
}}
|ethnic_groups_year = 2000
|government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[republic]]
|leader_title1 = [[President of Palau|President]]
|leader_name1 = [[Tommy Remengesau]]
|leader_title2 = [[Vice President of Palau|Vice President]]
|leader_name2 = [[Antonio Bells]]
|legislature = [[Palau National Congress|National Congress]]
|area_rank = 196th
|area_magnitude = 1 E8
|area_km2 = 459
|area_sq_mi = 177 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = negligible
|population_estimate = 20,956
|population_estimate_rank = 218th
|population_estimate_year = 2011
|population_census = |population_census_year =
|population_density_km2 = 28.4
|population_density_sq_mi = 45.5
|population_density_rank =
|GDP_PPP = {{nowrap|$164 million<ref name="cia">2008 estimate. {{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/ps.html |title=Palau |work=CIA World Factbook |publisher=CIA |accessdate=9 August 2009}}</ref>}}
|GDP_PPP_rank = {{small|not&nbsp;ranked}}
|GDP_PPP_year = 2008
|GDP_PPP_per_capita = $8,100<ref name="cia" />
|GDP_PPP_per_capita_rank = 119th
|sovereignty_type = Independence
|established_event1 = [[Compact of Free Association]] with the [[United States]]
|established_date1 = 1 October 1994
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2011
|HDI_change =  <!--increase/decrease/steady-->
|HDI = 0.782 <!--number only-->
|HDI_ref = <ref>[http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf Human Development Index and its components]. undp.org (2011).</ref>
|HDI_rank = 49th
|currency = [[United States dollar]]
|currency_code = USD
|country_code = PW
|time_zone =
|utc_offset = +9
|time_zone_DST = |utc_offset_DST =
|drives_on = right
|calling_code = [[+680]]
|cctld = [[.pw]]
|footnote_a = On 7 October 2006, government officials moved their offices in the former capital of [[Koror]] to [[Ngerulmud]] in [[Melekeok|Melekeok State]], located {{convert|20|km|mi|0|abbr=on}} northeast of Koror on Babelthaup Island and {{convert|2|km|0|abbr=on}} northwest of Melekeok village.
<!--- ORPHANED:
|footnote_b = GDP estimate includes US subsidy (2004 estimate).
----->
}}
