{{Infobox country
|conventional_long_name = Sultanate of Oman
|native_name = {{big|سلطنة عُمان}}<br/>''{{Unicode|Salṭanat ʻUmān}}''
|common_name = Oman
|image_flag = Flag of Oman.svg
|image_coat = Coat of arms of Oman.svg
|symbol_type = National emblem
|image_map = Oman (orthographic projection).svg
|national_anthem = ''[[Nashid as-Salaam as-Sultani]]''<br/>{{small|''National anthem of Oman''}}
|official_languages = [[Omani Arabic]]
|demonym = [[Omani people|Omani]]
|capital = [[Masqat, Oman|Muscat]]
|religion = [[Ibadi|Ibadi Islam]]
|latd=23 |latm=36 |latNS=N |longd=58 |longm=33 |longEW=E
|largest_city = capital
|government_type = [[Unitary state|Unitary]] [[Islamic state|Islamic]] [[absolute monarchy]]
|leader_title1 = [[Sultan of Oman|Sultan]]
|leader_name1 = [[Qaboos bin Said al Said]]
|leader_title2 = [[Deputy Prime Minister]]
|leader_name2 = [[Fahd bin Mahmoud al Said]]<ref>{{cite web |url=http://www.omanet.om/english/government/ministers.asp?cat=gov |title=Cabinet Ministers |publisher=Government of Oman |accessdate=13 October 2010}}</ref>
|legislature = [[Council of Oman|Parliament]]
|upper_house = [[Council of State of Oman|Council of State (Majlis al-Dawla)]]
|lower_house = [[Consultative Assembly of Oman|Consultative Assembly (Majlis al-Shura)]]
|sovereignty_type1 = Establishment
|established_event1 = The [[Azd|Azd tribe]] migration
|established_date1 = Late 2nd century
|established_event2 = {{nowrap|[[Imamate]] established<ref>{{cite encyclopedia|title=Oman|url=http://encarta.msn.com/encyclopedia_761561099_7/Oman.html|publisher=MSN Encarta|archiveurl=http://www.webcitation.org/query?id=1257037559642205|archivedate=31 October 2009|quote=In 751 Ibadi Muslims, a moderate branch of the Kharijites, established an imamate in Oman. Despite interruptions, the Ibadi imamate survived until the mid-20th century.}}</ref>}}
|established_date2 = 751
|area_rank = 70th
|area_magnitude = 1 E11
|area_km2 = 309,501
|area_sq_mi = 119,498 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = negligible
|population_estimate = 4,013,391<ref>http://www.ncsi.gov.om/NCSI_website/NCSI_EN.aspx</ref> 
|population_estimate_rank = 
|population_estimate_year = April 2014
|population_census = 2,773,479<ref name="2010Census">{{cite web |url=http://www.ncsi.gov.om/documents/Census_2010.pdf |title=Final Results of Census 2010 |publisher=National Center for Statistics & Information |accessdate=7 January 2012}}</ref>
|population_census_year = 2010
|population_density_km2 = 9.2
|population_density_sq_mi = 23.8 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 220th
|GDP_PPP_year = 2012
|GDP_PPP = $90.055 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=59&pr.y=6&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=449&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Oman |publisher=International Monetary Fund |accessdate=20 April 2012}}</ref> <!--Do not edit!-->
|GDP_PPP_rank =
|GDP_PPP_per_capita = $29,166<ref name=imf2/> <!--Do not edit!-->
|GDP_PPP_per_capita_rank =
|GDP_nominal_year = 2012
|GDP_nominal = $76.464 billion<ref name=imf2/> <!--Do not edit!-->
|GDP_nominal_rank =
|GDP_nominal_per_capita = $24,764<ref name=imf2/> <!--Do not edit!-->
|GDP_nominal_per_capita_rank =
|Gini_year = |Gini_change = <!--increase/decrease/steady--> |Gini = <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2013 <!--Please use the year to which the HDI refers, not the publication year-->
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.731 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite book |title=Human Development Report 2011 |url=http://www.undp.org/content/dam/undp/library/corporate/HDR/2011%20Global%20HDR/English/HDR_2011_EN_Complete.pdf |author=UNDP |year=2011 |publisher=[[United Nations Development Programme]] |page=132 |isbn=978-0-230-36331-1}}</ref>
|HDI_rank = 85 th
|currency = [[Omani rial|Rial]]
|currency_code = OMR
|country_code = OMN
|time_zone = [[Gulf Standard Time|GST]]
|utc_offset = +4
|time_zone_DST =
|utc_offset_DST = +4
|drives_on = right
|calling_code = [[+968]]
|cctld = [[.om]], [[عمان.]]
}}
