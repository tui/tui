{{Infobox country
|conventional_long_name = Bailiwick of Jersey
|native_name = {{unbulleted list|item_style=font-size:88%; |{{native name|fr|Bailliage de Jersey}} |{{native name|nrm|Bailliage dé Jèrri}}}}
|common_name = Jersey
|image_flag = Flag of Jersey.svg
|image_coat = Jersey coa.svg
|image_map = Europe-Jersey.svg
|map_caption = {{map caption |location_color=dark green}}
|national_motto =
|national_anthem = ''[[God Save the Queen]]''{{nbsp|2}}{{small|(official)}}<br/>''Island Home''{{nbsp|2}}{{small|(official)}}&nbsp;<sup>a</sup>
|official_languages = {{unbulleted list |[[English language|English]] |[[French language|French]]}}
|regional_languages = [[Jèrriais]]<ref>{{cite web |url=http://www.statesassembly.gov.je/documents/propositions/22189-12814-1972005.htm |title=Development of a Cultural Strategy for the Island |publisher=Statesassembly.gov.je |accessdate=31 May 2011}}{{dead link|date=September 2013}}</ref>
|ethnic_groups =
{{unbulleted list
| {{nowrap|33% Jersey}}
| 31% [[British people|other Britons]]
|  14% [[Portuguese people|Portuguese]]
|  13% [[Polish people|Polish]]
|  2% [[Irish people|Irish]]
| &lt;1% [[French people|French]]
}}
|ethnic_groups_year = 2011<ref>{{cite web|url=http://www.gov.je/SiteCollectionDocuments/Government%20and%20administration/R%20CensusBulletin2%2020111214%20SU.pdf |title=Bulletin 2: Place of birth, ethnicity, length of residency, marital status. |format=PDF |accessdate=2013-09-12}}</ref>
|capital = [[Saint Helier]]
|latd=49 |latm=11.401 |latNS=N |longd=2 |longm=06.600 |longEW=W
|largest_city = capital
|government_type = [[Crown dependencies|Crown dependency]]<sup>b</sup>
|leader_title1 = [[Duke of Normandy|Monarch]]<!--masculine form retained even when holder is female-->
|leader_name1 = [[Elizabeth II|Queen Elizabeth II]]
|leader_title2 = {{nowrap|[[Lieutenant Governor of Jersey|Lieutenant Governor]]}}
|leader_name2 = [[John McColl]]
|leader_title3 = [[List of Bailiffs of Jersey|Bailiff]]
|leader_name3 = [[Michael Birt (Bailiff of Jersey)|Michael Birt]]
|leader_title4 = [[Chief Minister of Jersey|Chief Minister]]
|leader_name4 = [[Ian Gorst]]
|area_rank = 227th
|area_magnitude = 1 E8
|area_km2 = 118.2<ref name=figures />
|area_sq_mi = 45.56 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = 0
|population_estimate = 97,857<ref>{{cite web |url=http://www.gov.je/Government/Census/Census2011/Pages/2011CensusResults.aspx |title=2011 census results |publisher=Gov.je |date=8 December 2011 |accessdate=8 December 2011}}</ref>
|population_estimate_rank = 199th
|population_estimate_year = 2011
|population_density_km2 = 819
|population_density_sq_mi = 2121 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 13th<sup>c</sup>
|GDP_PPP = $5.1 billion
|GDP_PPP_rank = 166th
|GDP_PPP_year = 2005
|GDP_PPP_per_capita = $57,000
|GDP_PPP_per_capita_rank = 6th
|sovereignty_type = Status
|sovereignty_note = {{nowrap|[[Crown dependencies|British Crown dependency]]}}
|established_event1 = Administative separation from mainland [[Normandy]]
|established_date1 = <br/>1204
|established_event2 = [[Liberation Day|Liberation]] from [[Nazi Germany|German]]&nbsp;occupation
|established_date2 = <br/>9 May 1945
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2008
|HDI_change = steady <!--increase/decrease/steady-->
|HDI =  0.985<!--number only-->
|HDI_ref = <ref>[http://www.unescap.org/pdd/publications/workingpaper/wp_09_02.pdf Filling Gaps in the Human Development Index], United Nations ESCAP, February 2009</ref>
|HDI_rank = not ranked
|currency = [[Pound sterling]]<sup>d</sup>
|currency_code = GBP
|country_code =
|time_zone = [[Greenwich Mean Time|GMT]]<sup>e</sup>
|utc_offset =
|time_zone_DST =
|utc_offset_DST = +1
|drives_on = left
|patron_saint = [[Helier|St. Helier]]
|calling_code = [[+44]]
|cctld = [[.je]]
|footnote_a = by [[Gerard Le Feuvre]]; official for occasions when distinguishing anthem required.
|footnote_b = [[Parliamentary democracy]] under [[constitutional monarchy]].
<!--ORPHANED: [http://www.cab.org.je/index.php?option=com_content&task=view&id=91&Itemid=66 Jersey’s Resident Population 2007]-->
|footnote_c = Rank based on population density of Channel Islands, i.e. including the [[Guernsey|Bailiwick of Guernsey]].
|footnote_d = The States of Jersey issue their own [[Pound sterling|sterling]] notes and coins (see [[Jersey pound]]).
|footnote_e = In a referendum on 16 October 2008, voters rejected a proposal to adopt [[Central European Time]] by 72.4%.{{big|<ref>{{cite news |title=Jersey rejects time-zone change |publisher=BBC News |url=http://news.bbc.co.uk/1/hi/world/europe/jersey/7671009.stm |date=16 October 2008 |accessdate=18 October 2008 }}</ref>}}
}}
