{{Infobox country
|conventional_long_name = Grand Duchy of Luxembourg
|native_name = {{unbulleted list|item_style=font-size:88%; |{{native name|fr|Grand-Duché de Luxembourg}} |{{native name|de|Großherzogtum Luxemburg}} |{{native name|lb|Groussherzogtum Lëtzebuerg}}}}
|common_name = Luxembourg
|image_flag = Flag of Luxembourg.svg
|image_coat = Coat of Arms of Luxembourg.svg
|national_motto = {{nowrap|{{native phrase|lb|"[[Mir wëlle bleiwe wat mir sinn]]"|italics=off}}}}<br/>{{small|"We want to remain what we are"}}
|national_anthem = ''[[Ons Heemecht]]''<br/>{{small|''Our Homeland''}}<br/><center>[[File:Luxembourg National Anthem.ogg]]</center>
|royal_anthem = ''[[De Wilhelmus]]''&nbsp;<sup>a</sup>
|image_map = EU-Luxembourg.svg
|map_caption = {{map_caption |location_color=dark green |region=[[Europe]] |region_color=dark grey |subregion=the [[European Union]] 
|subregion_color=green |legend=EU-Luxembourg.svg}}
|image_map2 = Luxembourg - Location Map (2013) - LUX - UNOCHA.svg
|capital = [[Luxembourg (city)|Luxembourg City]]
|latd=49 |latm=36 |latNS=N |longd=6 |longm=7 |longEW=E
|largest_city = capital
|official_languages = {{unbulleted list|[[French language|French]] |[[German language|German]]|[[Luxembourgish language|Luxembourgish]]}}
|demonym = [[Luxembourgers|Luxembourger]]
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional monarchy]]
|leader_title1 = {{nowrap|[[Grand Duke of Luxembourg|Grand Duke]] {{small|([[List of Grand Dukes of Luxembourg|list]])}}}}
|leader_name1 = [[Henri, Grand Duke of Luxembourg|Henri]]
|leader_title2 = {{nowrap|[[Prime Minister of Luxembourg|Prime Minister]] {{small|([[List of Prime Ministers of Luxembourg|list]])}}}}
|leader_name2 = [[Xavier Bettel]]
|legislature = [[Chamber of Deputies (Luxembourg)|Chamber of Deputies]]
|sovereignty_type = [[Independence]]
|established_event1 = from [[First French Empire|French Empire]]<br/>([[Treaty of Paris (1815)|Treaty of Paris]])
|established_date1 = 9 June 1815
|established_event2 = [[Treaty of London (1839)|1st Treaty of London]]
|established_date2 = 19 April 1839
|established_event3 = [[Treaty of London (1867)|2nd Treaty of London]]
|established_date3 = 11 May 1867
|established_event4 = {{nowrap|End of [[personal union]]}}
|established_date4 = 23 November 1890
|area_km2 = 2,586.4
|area_sq_mi = 998.6 <!--Do not remove per [[WP:MOSNUM]]-->
|area_magnitude = 1 E9
|area_rank = 179th
|percent_water = 0.60%
|population_estimate = 549 680<ref>[http://www.lequotidien.lu/politique-et-societe/55384.html], Statec figures.</ref>
|population_estimate_year = 2014
|population_census = 439,539
|population_census_year = 2001
|population_estimate_rank = 170th
|population_density_km2 = 194.1
|population_density_sq_mi = 501.3 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 60th
|GDP_PPP_year = 2012
|GDP_PPP = $42.225 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=47&pr.y=10&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=137&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Luxembourg |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
|GDP_PPP_rank = 94th
|GDP_PPP_per_capita = $79,785<ref name=imf2/>
|GDP_PPP_per_capita_rank = 1st
|GDP_nominal = $56.738 billion<ref name=imf2/>
|GDP_nominal_rank = 69th
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $107,206<ref name=imf2/>
|GDP_nominal_per_capita_rank = 1st
|Gini_year = 2011
|Gini_change =  <!--increase/decrease/steady--> 
|Gini = 27.2 <!--number only--> 
|Gini_ref = <ref name=eurogini>{{cite web|title=Gini coefficient of equivalised disposable income (source: SILC)|url=http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=ilc_di12|publisher=Eurostat Data Explorer|accessdate=13 August 2013}}</ref>
|Gini_rank = 6th
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.875 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf |title=Human Development Report 2011 |year=2011 |publisher=United Nations |accessdate=5 November 2011}}</ref>
|HDI_rank = 26th
|currency = [[Euro]] ([[Euro sign|€]])<sup>b</sup>
|currency_code = EUR
|country_code = LUX
|time_zone = [[Central European Time|CET]]
|utc_offset = +1
|time_zone_DST = [[Central European Summer Time|CEST]]
|utc_offset_DST = +2
|drives_on = right
|calling_code = [[Telephone numbers in Luxembourg|+352]]
|cctld = [[.lu]]<sup>c</sup>
|footnote_a = Not the same as the ''[[Het Wilhelmus]]'' of the Netherlands.
|footnote_b = Before 1999, [[Luxembourgish franc]].
|footnote_c = The [[.eu]] domain is also used, as it is shared with other [[European Union]] member states.
|footnote_d = {{note|ddd}} {{cite web |title=CIA&nbsp;– The World Factbook&nbsp;– Field Listing&nbsp;– Distribution of family income&nbsp;– Gini index |publisher=United States government |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html#Govt |accessdate=3 May 2013}}
}}
