{{Infobox country
| conventional_long_name = Republic of Yemen
| native_name = {{big|الجمهورية اليمنية}}<br />''{{transl|ar|Al-Jumhuriya Al-Yamania}}''
| common_name = Yemen
| image_flag = Flag of Yemen.svg
| image_coat = Coat of arms of Yemen.svg
| symbol_type = Emblem
| image_map = Yemen (orthographic projection).svg
| national_motto = <div style="line-height:1.45em;white-space:nowrap;">{{native phrase|ar|{{lower|0.1em|{{big|الله، الوَطَن، الثَورة، الوَحدة}}}}|italics=off}}<br />{{transl|ar|"Allāh, al-Waṭan, aṯ-Ṯhawrah, al-Waḥdah"}}<br />{{small|"God, Country, Revolution, Unity"}}</div>
| national_anthem = <div style="line-height:1.45em;white-space:nowrap;">{{native name|ar|{{lower|0.1em|{{big|نشيد اليمن الوطني}}}}|italics=off|nolink=on}}<br />''[[National anthem of Yemen|Nashīd al-Yaman al-waṭanī]]''<br />{{small|''United Republic''}}
<center>[[File:United States Navy Band - United Republic.ogg]]</center>
| official_languages = [[Arabic]]
| demonym = [[Yemeni (disambiguation)|Yemeni]]
|capital = [[Sana'a]]
|latd=15| latm=20| latNS= N|longd=44|longm=12|longEW=E
|largest_city = capital
| government_type = [[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]] [[republic]]
| leader_title1 = [[President of Yemen|President]]
| leader_name1 = [[Abd Rabbuh Mansur Hadi]]
| leader_title2 = [[Prime Minister of Yemen|Prime Minister]]
| leader_name2 = [[Mohammed Basindawa]]
| legislature = {{nowrap|[[House of Representatives (Yemen)|House of Representatives]]}}
| area_rank = 50th
| area_magnitude = 1 E8
| area_km2 = 527,829
| area_sq_mi = 203,796 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = negligible
| population_estimate = 23,833,000<ref name=yearbook2011>{{cite web|title=Statistical Yearbook 2011|url=http://www.cso-yemen.org/publiction/yearbook2011/population.xls|publisher=Central Statistical Organisation|accessdate=24 February 2013}}</ref>
| population_estimate_year = 2011
| population_estimate_rank = 48th
| population_census = 19,685,000<ref name=yearbook2011 />
| population_census_year = 2004
| population_density_km2 = 44.7
| population_density_sq_mi = 115.7 <!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 160th
| GDP_PPP_year = 2012
| GDP_PPP = $58.202 billion<ref name=imf2>{{cite web |url=https://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=62&pr.y=1&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=474&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Yemen |publisher=International Monetary Fund |accessdate=24 April 2012}}</ref>
| GDP_PPP_rank =
| GDP_PPP_per_capita = $2,249<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $36.700 billion<ref name=imf2/>
| GDP_nominal_rank =
| GDP_nominal_year = 2012
| GDP_nominal_per_capita = $1,418<ref name=imf2/>
| GDP_nominal_per_capita_rank =
| Gini_year = | Gini_change = <!--increase/decrease/steady--> | Gini = <!--number only--> | Gini_ref = | Gini_rank =
| HDI_year = 2013 <!--Please use the year to which the HDI data refers, not the year of its publication-->
| HDI_change = decrease <!--increase/decrease/steady-->
| HDI = 0.458 <!--number only-->
| HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Complete.pdf |title=Human Development Report 2011 |publisher=The United Nations |accessdate=18 October 2011}}</ref>
| HDI_rank = 160th
| FSI = {{nowrap|93.2 {{decrease}} 3.4}}
| FSI_year = 2007
| FSI_rank = 24th
| FSI_category = <span style="color:#f00;white-space:nowrap;">Alert</span>
| sovereignty_type = Establishment
| established_event1 = [[North Yemen]] independence from the [[Ottoman Empire]]<sup>a</sup>
| established_date1 = <br />1 November 1918
| established_event2 = [[South Yemen]] independence from the [[British Empire]]<sup>b</sup>
| established_date2 = <br />30 November 1967
| established_event3 = [[Yemeni unification|Unification]]
| established_date3 = 22 May 1990
| currency = [[Yemeni rial]]
| currency_code = YER
| country_code = YEM
| time_zone =
| utc_offset = +3
| time_zone_DST = | utc_offset_DST =
| drives_on = [[Right- and left-hand traffic|right]]<ref>{{cite web |url=http://www.newssafety.org/index.php?option=com_content&view=section&layout=blog&id=28&Itemid=100385 |title=Yemen |publisher=International News Safety Institute |accessdate=14 October 2009}}</ref>
| calling_code = [[Telephone numbers in Yemen|+967]]
| cctld = [[.ye]], [[اليمن.]]
| footnote_a = From the [[Ottoman Empire]].
| footnote_b = From the [[United Kingdom]].
}}
