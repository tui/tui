{{Infobox country
|native_name = Canada
|common_name = Canada
|image_flag = Flag of Canada.svg
|alt_flag = Vertical triband (red, white, red) with a red maple leaf in the centre
|image_coat =<!-- DO NOT insert a non-free image here. Also, do not insert "Coat of Arms of Canada rendition.svg" here as it's not accurate. -->
|alt_coat =<!--alt text for coat of arms-->
|symbol_type = Coat of Arms
|national_motto = {{native phrase|la|"[[A Mari Usque Ad Mare]]"|italics=off}}<br>{{small|"From Sea to Sea"}}
|national_anthem = "[[O Canada]]"
|royal_anthem = "[[God Save the Queen]]"<ref>{{cite web |url=http://www.pch.gc.ca/pgm/ceem-cced/symbl/godsave-eng.cfm |author=[[Department of Canadian Heritage]] |title=Royal 'God Save The Queen' |publisher=Queen's Printer |accessdate=May 23, 2011}}</ref><ref>{{cite web |last=Kallmann |first=Helmut |work=Encyclopedia of Music in Canada |title=National and royal anthems |editor=Marsh, James Harley |publisher=Historica-Dominion |url=http://www.thecanadianencyclopedia.com/en/article/national-and-royal-anthems-emc/ |accessdate=November 27, 2013}}</ref>
|image_map = Canada (orthographic projection).svg
|alt_map = Projection of North America with Canada in green
|map_width = 220px
|capital = [[Ottawa]]
|latd=45 |latm=24 |latNS=N |longd=75 |longm=40 |longEW=W
|largest_city = [[Toronto]]
|official_languages = {{hlist |[[English language|English]] |[[French language|French]]}}
|ethnic_groups =
{{unbulleted list
| 76.7% [[European Canadian|European]]
| 14.2% [[Asian Canadian|Asian]]
| 4.3% [[Aboriginal peoples in Canada|Aboriginal]]
| 2.9% [[Black Canadian|Black]]
| 1.2% [[Latin American Canadian|Latin American]]
| 0.5% [[Multiracial]]
| 0.3% [[Canada 2011 Census|Other]]
}}
|demonym = Canadian
|government_type = {{nowrap|[[Federalism|Federal]] [[parliamentary system|parliamentary]]<br>[[constitutional monarchy]]<ref name=hail/>}}
|leader_title1 = [[Monarchy of Canada|Monarch]]
|leader_name1 = [[Elizabeth II]]
|leader_title2 = {{nowrap|[[Governor General of Canada|Governor General]]}}
|leader_name2 = [[David Johnston]]
|leader_title3 = [[Prime Minister of Canada|Prime Minister]]
|leader_name3 = [[Stephen Harper]]
|leader_title4 = [[Chief Justice of Canada|Chief Justice]]
|leader_name4 = [[Beverley McLachlin]]
|legislature = [[Parliament of Canada|Parliament]]
|upper_house = [[Senate of Canada|Senate]]
|lower_house = [[Canadian House of Commons|House of Commons]]
|sovereignty_type = [[Canadian Confederation|Establishment]]
|sovereignty_note = from the United Kingdom
|established_event1 = [[Constitution of Canada|Constitution Act]]
|established_date1 = July 1, 1867
|established_event2 = [[Statute of Westminster 1931|Statute of Westminster]]
|established_date2 = December 11, 1931
|established_event3 = [[Canada Act 1982|Canada Act]]
|established_date3 = April 17, 1982
|area_km2 = 9,984,670
|area_sq_mi = 3,854,085 <!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 2nd
|area_magnitude = 1 E12
|percent_water = 8.92 (891,163&nbsp;km{{smallsup|2}}{{\}}344,080&nbsp;mi{{smallsup|2}})
|population_estimate = 35,427,524<ref>{{cite web|title=Estimates of population, Canada, provinces and territories|url=http://www5.statcan.gc.ca/cansim/a26?lang=eng&retrLang=eng&id=0510005&paSer=&pattern=&stByVal=1&p1=1&p2=31&tabMode=dataTable&csid=|work=Statistics Canada|accessdate=18 June 2014}}</ref>
|population_estimate_year = 2014
|population_estimate_rank = 37th
|population_census = 33,476,688<ref name="StatsCan-2011Census">{{cite web
| author = Statistics Canada
| title = Population and dwelling counts, for Canada, provinces and territories, 2011 and 2006 censuses
| date = 2013-01-30
| url = http://www12.statcan.gc.ca/census-recensement/2011/dp-pd/hlt-fst/pd-pl/Table-Tableau.cfm?LANG=Eng&T=101&S=50&O=A
| accessdate = 2013-12-02 }}</ref>
|population_census_year = 2011
|population_density_km2 = 3.41
|population_density_sq_mi = 8.3 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 228th
|GDP_PPP = {{nowrap|$1.585 trillion<ref name=imf2/><!--end nowrap:-->}}
|GDP_PPP_rank = 13th
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $44,656<ref name=imf2/>
|GDP_PPP_per_capita_rank = 9th
|GDP_PPP_per_capita_year = 2014
|GDP_nominal = {{nowrap|$1.769 trillion<ref name=imf2/>}}
|GDP_nominal_rank = 11th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $49,838<ref name=imf2/>
|GDP_nominal_per_capita_rank = 11th
|GDP_nominal_per_capita_year = 2014
|Gini_year = 2005
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 32.1 <!--number only-->
|Gini_ref =<ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of family income – Gini index |work=World Factbook |publisher=CIA |accessdate=September 1, 2009}}</ref>
|Gini_rank = 103rd<ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/rankorder/2172rank.html|title=Country Comparison: Distribution Of Family Income – Gini Index|work=World Factbook |publisher=CIA |accessdate=May 1, 2013}}</ref>
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.911 <!--number only-->
|HDI_ref =<ref name="HDI"/>
|HDI_rank = 11th
|currency = [[Canadian dollar]] ($)
|currency_code = CAD
|utc_offset = −3.5 to −8
|utc_offset_DST = −2.5 to −7
|date_format = {{unbulleted list |dd-mm-yyyy |mm-dd-yyyy |yyyy-mm-dd ([[Common Era|CE]])}}
|drives_on = right
|calling_code = [[Telephone numbers in Canada|+1]]
|cctld = [[.ca]]
}}
