{{Infobox country
|native_name = Burkina Faso
|common_name = Burkina Faso
|image_flag = Flag of Burkina Faso.svg
|image_coat = Coat of arms of Burkina Faso.svg
|national_motto = {{native phrase|fr|"Unité–Progrès–Justice"|italics=off}}<br/>{{small|"Unity–Progress–Justice"}}
|national_anthem = {{nowrap|{{native phrase|fr|[[Une Seule Nuit]] / Ditanyè|nolink=yes}}<br/>{{small|''One Single Night / Hymn of Victory''}}}}
|image_map = Location Burkina Faso AU Africa.svg
|map_caption = {{map caption |countryprefix= |location_color=dark blue |region=[[Africa]] |region_color=dark grey |subregion=the [[African Union]] |subregion_color=light blue |legend=Location Burkina Faso AU Africa.svg}}
|image_map2 = Burkina Faso - Location Map (2013) - BFA - UNOCHA.svg
|official_languages = [[French language|French]]
|regional_languages = {{vunblist |[[Mòoré language|Mòoré]] |[[Mandinka language|Mandinka]] |[[Bambara language|Bambara]]}}
|demonym = {{hlist |Burkinabé |Burkinabè |Burkinabe}}
|ethnic_groups =
{{vunblist
| 47.9% [[Mossi people|Mossi]]
| 10.3% [[Fula people|Fulani]]
| 6.9% [[Lobi people|Lobi]]
| 6.9% [[Bobo people|Bobo]]
| 6.7% [[Mandé peoples|Mandé]]
| 5.3% [[Senufo people|Senufo]]
| 5.0% [[Gurunsi]]
| 4.8% [[Gurma people|Gurma]]
| 3.1% [[Tuareg people|Tuareg]]
}}
|ethnic_groups_year = 1995
|capital = [[Ouagadougou]]
|latd=12 |latm=20 |latNS=N |longd=1 |longm=40 |longEW=W
|largest_city = capital
|government_type = {{nowrap|[[Semi-presidential system|Semi-presidential]] [[republic]]}}
|leader_title1 = [[President of Burkina Faso|President]]
|leader_name1 = [[Blaise Compaoré]]
|leader_title2 = [[Prime Minister of Burkina Faso|Prime Minister]]
|leader_name2 = [[Luc-Adolphe Tiao]]
|legislature = [[National Assembly (Burkina Faso)|National Assembly]]
|sovereignty_type = [[Independence]]
|established_event1 = from [[France]]
|established_date1 = 5 August 1960
|area_km2 = 274,200
|area_sq_mi = 105,869 <!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 74th
|area_magnitude = 1 E11
|percent_water = 0.146%
|population_estimate = 15,730,977<ref name="INSD">{{fr icon}} [http://www.insd.bf/fr/ INSD Burkina Faso]</ref>
|population_estimate_year = 2010
|population_estimate_rank = 64th
|population_census = 14,017,262
|population_census_year = 2006
|population_density_km2 = 57.4
|population_density_sq_mi = 148.9 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 145th
|GDP_PPP_year = 2012
|GDP_PPP = $24.293&nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=66&pr.y=9&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=748&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Burkina Faso |publisher=International Monetary Fund |accessdate=17 April 2013}}</ref>
|GDP_PPP_rank = 
|GDP_PPP_per_capita = $1,399<ref name=imf2/>
|GDP_PPP_per_capita_rank = 
|GDP_nominal = $10.464&nbsp;billion<ref name=imf2/>
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $602<ref name=imf2/>
|HDI_year = 2007
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.389 <!--number only-->
|HDI_ref = 
|HDI_rank = 177th
|Gini_year = 2007
|Gini_change = <!--increase/decrease/steady-->
|Gini = 39.5 <!--number only-->
|Gini_ref = <ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of family income&nbsp;– Gini index |work=The World Factbook |publisher=CIA |accessdate=1 September 2009}}</ref>
|Gini_rank = 
|currency = [[West African CFA franc]]<ref>CFA Franc BCEAO. Codes: XOF / 952 [http://www.currency-iso.org/dam/downloads/table_a1.xml ISO 4217 currency names and code elements]. ISO.</ref>
|currency_code = XOF
|time_zone = 
|utc_offset = +0
|time_zone_DST = not observed
|utc_offset_DST = 
|drives_on = right
|calling_code = [[+226]]
|cctld = [[.bf]]
|footnotes = The data here is an estimation for the year 2005 produced by the International Monetary Fund in April 2005.
}}
