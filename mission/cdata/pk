{{Infobox country
|conventional_long_name = Islamic Republic of Pakistan
|native_name = {{native name|ur|{{nobold|{{Nastaliq|اسلامی جمہوریۂ پاكستان}}}}|italics=off}}<br><small>{{transl|Urdu|ALA-LC|''Islāmī Jumhūriyah-yi Pākistān''}}</small>
|common_name = Pakistan
|image_flag = Flag of Pakistan.svg
|image_coat = Coat of arms of Pakistan.svg
|symbol_type = Emblem
|image_map = Pakistan (orthographic projection).svg
|map_caption = Area controlled by Pakistan shown in dark green; claimed but uncontrolled territory shown in light green.
|map_width = 220px
|national_motto = {{native phrase|ur|{{Nastaliq|ایمان، اتحاد، نظم}}|italics=off|nolink=on}}<br/>{{Transl|ur|''Īmān, Ittiḥād, Naẓm''}}<br/>{{small|"Faith, Unity, Discipline"}}&nbsp;{{lower|0.2em|<ref name="The State Emblem">{{cite web |title=The State Emblem |url=http://www.infopak.gov.pk/Eemblem.aspx |archiveurl=https://web.archive.org/web/20070701023430/http://www.infopak.gov.pk/Eemblem.aspx |archivedate=2007-07-01 |publisher=Ministry of Information and Broadcasting, Government of Pakistan. |accessdate=18 December 2013}}</ref><!--end lower:-->}}
|national_anthem = {{transl|ur|ALA-LC|''[[Qaumi Taranah|Qaumī Tarānah]]''}}<br/>{{lang|ur|{{Nastaliq|قومی ترانہ}}}}<br/><center><small>"The National Anthem"</small><ref>{{cite web|title=National Symbols and Things of Pakistan|url=http://www.pakistan.gov.pk/gop/index.php?q=aHR0cDovLzE5Mi4xNjguNzAuMTM2L2dvcC8uL2ZybURldGFpbHMuYXNweD9vcHQ9bWlzY2xpbmtzJmlkPTQx|work=Government of Pakistan|accessdate=27 May 2014}}</ref></center><center>[[File:Qaumi Tarana Instrumental.ogg]]</center>
|official_languages = {{hlist |[[English language|English]]<sup>a</sup> |[[Urdu]]<ref>{{cite web |quote=The Pakistani Government states that English is the official language. It is being widely used in business, law, and government documents, as well being taught throughout schools as a medium of instruction. |url=http://www.pakistan.gov.pk/ |publisher=Government of Pakistan |title=Welcome |year=2012 |accessdate=24 October 2012}} {{cite web |url=http://heritage.pk/english-as-an-official-language-of-pakistan |publisher=Heritage Pakistan |title=English, as an Official Language of Pakistan |year=2012 |accessdate=24 October 2012}}</ref><!--end hlist:-->}}
|languages_type=Regional languages
|languages=[[Punjabi language|Punjabi]], [[Pashto language|Pashto]], [[Sindhi language|Sindhi]], [[Saraiki language|Saraiki]], [[Balochi language|Balochi]], [[Kashmiri language|Kashmiri]], [[Brahui language|Brahui]], [[Dogri language|Dogri]], [[Hindko language|Hindko]], [[Shina language|Shina]], [[Balti language|Balti]], [[Khowar language|Khowar]], [[Burushaski]] [[Yidgha language|Yidgha]], [[Dameli language|Dameli]], [[Kalash language|Kalasha]], [[Gawar-Bati language|Gawar-Bati]], [[Domaaki language|Domaaki]]<ref>{{cite web|title=Population by Mother Tongue|url=http://www.census.gov.pk/MotherTongue.htm|publisher=Population Census Organization, Government of Pakistan|accessdate=28 December 2011}}</ref><ref name="state.gov"/>
|official_religion = [[Islam]]
|demonym = Pakistani
|capital = [[Islamabad]]
|latd=33 |latm=40 |latNS=N |longd=73 |longm=10 |longEW=E
|largest_city = [[Karachi]]
|government_type = [[Federal republic|Federal]] [[Parliamentary democracy|parliamentary]] [[Parliamentary republic|republic]]
|leader_title1 = [[President of Pakistan|President]]
|leader_name1 = {{nowrap|[[Mamnoon Hussain]] ([[Pakistan Muslim League (N)|PML-N]])}}
|leader_title2 = [[Prime Minister of Pakistan|Prime Minister]]
|leader_name2 = [[Mian Nawaz Sharif]] ([[Pakistan Muslim League (N)|PML-N]])
|leader_title3 = [[Chief Justice of Pakistan|Chief Justice]]
|leader_name3 = [[Tassaduq Hussain Jillani]]
|leader_title4 = [[Chairman of the Senate of Pakistan|Chairman Senate]]
|leader_name4 = {{nowrap|[[Nayyar Hussain Bukhari]] ([[Pakistan Peoples Party|PPP]])}}
|leader_title5 = [[Speaker of the National Assembly of Pakistan|Speaker National Assembly]]
|leader_name5 = {{nowrap|[[Sardar Ayaz Sadiq|Ayaz Sadiq]] ([[Pakistan Muslim League (N)|PML-N]])}}
|legislature = ''[[Parliament of Pakistan|Majlis-e-Shoora]]''
|upper_house = [[Senate of Pakistan|Senate]]
|lower_house = [[National Assembly of Pakistan|National Assembly]]
|sovereignty_type = [[History of Pakistan|Formation]]
|established_event1 = {{nowrap|[[Allahabad Address|Conception of Pakistan]]}}<ref>{{cite web|url=http://www.allamaiqbal.com/publications/journals/review/oct83/2.htm|title= THE CONCEPT OF PAKISTAN IN THE LIGHT OF IQBAL’S ADDRESS AT ALLAHABAD | author=Ehsan Rashid| year=1977|publisher=Iqbal Memorial Talks |accessdate=5 March 2014}} Ehsan Rashid explains how concept of Pakistan and Iqbal's Allahabad address are interlinked.</ref>
|established_date1 = 29 December 1930
|established_event2 = {{nowrap|[[Lahore Resolution|Pakistan Resolution]]}}
|established_date2 = 23 March 1940
|established_event3 = [[Independence Day (Pakistan)|Independence]] and [[Dominion of Pakistan|Dominion]]
|established_date3 = 14 August 1947
|established_event4 = [[Republic Day (Pakistan)|Islamic Republic]] 
|established_date4 = 23 March 1956
|established_event5 = [[Bangladesh Liberation War|Breakup of East and West Pakistan]]
|established_date5 = 16 December 1971
|established_event6 = [[Constitution of Pakistan|Current constitution]]
|established_date6 = 14 August 1973
|area_km2 = 796,095
|area_sq_mi = 307,374
|area_footnote = {{efn|"Excludes data for Pakistani territories of Kashmir; [[Azad Kashmir]] ({{convert|13297|km2|sqmi|disp=or|abbr=on}}) and [[Gilgit–Baltistan]] ({{convert|72520|km2|sqmi|disp=or|abbr=on}}).<ref>{{cite web|url=http://www.geohive.com/cntry/pakistan.aspx |title=Pakistan statistics |publisher=Geohive |accessdate=20 April 2013}}</ref> Including these territories would produce an area figure of {{convert|881912|km2|sqmi|abbr=on}}."}}
|area_rank = 36th
|area_magnitude = 
|percent_water = 3.1
|population_estimate = 186,693,907<ref>{{cite web|url=http://www.census.gov.pk/ |title=Pakistan Census|publisher=Government of Pakistan |accessdate=21 May 2014}}</ref>
|population_estimate_year = 2014
|population_estimate_rank = 6th
|population_density_km2 = 234.4
|population_density_sq_mi = 607.4
|population_density_rank = 55th
|GDP_PPP_year = 2013
|GDP_PPP = {{nowrap|$574.068 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/02/weodata/weorept.aspx?pr.x=41&pr.y=13&sy=2013&ey=2013&scsm=1&ssd=1&sort=country&ds=.&br=1&c=564&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= | title=Pakistan | publisher=International Monetary Fund | accessdate=10 November 2013}}</ref>}}
|GDP_PPP_rank = 26th
|GDP_PPP_per_capita = $3,144<ref name=imf2/>
|GDP_PPP_per_capita_rank = 139th
|GDP_nominal = {{nowrap|$236.518 billion<ref name=imf2/>}}
|GDP_nominal_rank = 45th
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $1,295<ref name=imf2/>
|GDP_nominal_per_capita_rank = 147th
|Gini_year = 2008
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 30.0 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
|Gini_rank = 
|HDI_year = 2012
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.515 <!--number only-->
|HDI_ref = <ref>{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Summary.pdf |format=PDF |title=Human Development Report 2013. Human development index trends: Table 1 |publisher=[[United Nations Development Programme]] |page=18 |accessdate=12 July 2013}}</ref>
|HDI_rank = 146th
|currency = [[Pakistani rupee]] (₨)
|currency_code = PKR
|time_zone = [[Pakistan Standard Time|PKT]]
|utc_offset = +5
|utc_offset_DST = +6<sup>b</sup>
|drives_on = [[LHT#Pakistan|left]]<ref>{{cite news |url=http://www.dailytimes.com.pk/default.asp?page=story_28-7-2005_pg3_5 |archiveurl=http://web.archive.org/web/20120110085150/http://www.dailytimes.com.pk/default.asp?page=story_28-7-2005_pg3_5 |archivedate=2012-01-10 |title=Driving—the good, the bad and the ugly |author=Miguel Loureiro |date=28 July 2005 |work=Daily Times |accessdate=6 February 2014 |location=Pakistan}}</ref>
|calling_code = [[Telephone numbers in Pakistan|+92]]
|cctld = [[.pk]]
|footnote_a = See also [[Pakistani English]].
|footnote_b = Not always observed; see [[Daylight saving time in Pakistan]].
| website           ={{URL|http://www.pakistan.gov.pk/}}
}}
