{{Infobox country
| conventional_long_name = Togolese Republic
| native_name = ''République Togolaise''
| common_name = Togo
| image_flag = Flag of Togo.svg
| image_coat = Coat of arms of Togo.svg
| symbol_type = Coat of arms
| image_map = Location Togo AU Africa.svg
| map_caption = {{map caption|countryprefix=|location_color=dark blue|region=Africa|region_color=dark grey|subregion=the [[African Union]]|subregion_color=light blue}}
| national_motto = {{native phrase|fr|"Travail, Liberté, Patrie"<ref>{{cite web |url=http://www.africanlegislaturesproject.org/content/constitution-togo |title=Constitution of Togo |year=2002 |accessdate=20 November 2011}}</ref>|italics=off}}<br/>{{small|"Work, Liberty, Homeland"}}
| national_anthem = {{native name|fr|[[Salut à toi, pays de nos aïeux]]|nolink=on}}<br/>{{small|"Hail to thee, land of our forefathers"}}
| official_languages = French
| languages_type = [[Vernacular|Vernacular<br/>language]]s
| languages = {{unbulleted list |[[Gbe languages]]<sup>a</sup> |[[Tem language|Kotocoli]] |[[Kabiyé language|Kabiyé]]}}
| ethnic_groups =
{{unbulleted list
| {{nowrap|99% African {{small|(37 tribes)}}&nbsp;<sup>b</sup>}}
|  1% others<sup>c</sup>
}}
| demonym = Togolese
| capital = [[Lomé]]
| latd=6 |latm=7 |lats=55 |latNS=N |longd=1 |longm=13 |longs=22 |longEW=E
| largest_city = [[Lomé]]
| government_type = [[Presidential system|Presidential]] [[republic]]
| leader_title1 = [[President of Togo|President]]
| leader_name1 = [[Faure Gnassingbé]]
| leader_title2 = [[Heads of Government of Togo|Prime Minister]]
| leader_name2 = [[Kwesi Ahoomey-Zunu]]
| legislature = [[National Assembly (Togo)|National Assembly]]
| area_rank = 125th
| area_magnitude = 1 E10
| area_km2 = 56,785
| area_sq_mi = 21,925
| percent_water = 4.2
| population_estimate = 7,154,237<ref name="cia"/><ref>[https://www.cia.gov/library/publications/the-world-factbook/rankorder/2119rank.html?countryname=Togo&countrycode=to&regionCode=afr&rank=101#to Country Comparison :: Population]. CIA World Factbook</ref>
| population_estimate_rank = 101st
| population_estimate_year = 2013
| population_census = | population_census_year =
| population_density_km2 = 125.9
| population_density_sq_mi = 326.3
| population_density_rank = 93rd<sup>e</sup>
| GDP_PPP = $6.910 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=68&pr.y=11&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=742&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Togo |publisher=International Monetary Fund |accessdate=18 April 2013}}</ref>
| GDP_PPP_rank =
| GDP_PPP_year = 2012
| GDP_PPP_per_capita = $1,096<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $3.685 billion<ref name=imf2/>
| GDP_nominal_year = 2012
| GDP_nominal_per_capita = $584<ref name=imf2/>
| sovereignty_type = [[Independence]]
| established_event1 = from [[France]]
| established_date1 = 27 April 1960
| Gini_year = 2011
| Gini_change =  <!--increase/decrease/steady--> 
| Gini = 39.3 <!--number only--> 
| Gini_ref = <ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
| Gini_rank =
| HDI_year = 2010
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.459 <!--number only-->
| HDI_ref = <ref name="HDI">{{cite web|url=http://hdr.undp.org/en/reports/global/hdr2013/|title= The 2013 Human Development Report – "The Rise of the South: Human Progress in a Diverse World"|publisher=[[Human Development Report|HDRO (Human Development Report Office)]] [[United Nations Development Programme]]|pages=144–147|accessdate=28 November 2013}}</ref>
| HDI_rank = 159th
| currency = [[CFA franc]]
| currency_code = XOF
| country_code =
| time_zone = [[Greenwich Mean Time|GMT]]
| utc_offset = +0
| time_zone_DST = | utc_offset_DST =
| drives_on = right
| calling_code = [[+228]]
| cctld = [[.tg]]
| footnote_a = Such as [[Ewe language|Ewe]], [[Gen language|Mina]] and [[Aja language (Niger–Congo)|Aja]].
| footnote_b = Largest are the [[Ewe people|Ewe]], Mina, [[Tém|Kotokoli Tem]] and [[Kabye people|Kabre]].
| footnote_c = Mostly European and Syrian-Lebanese.
| footnote_d = Estimates for this country explicitly take into account the effects of excess mortality due to AIDS; this can result in lower life expectancy, higher infant mortality and death rates, lower population and growth rates, and changes in the distribution of population by age and sex than would otherwise be expected.
| footnote_e = Rankings based on 2005 figures ([https://www.cia.gov/library/publications/the-world-factbook/geos/to.html CIA World Factbook – ''Togo''])
}}
