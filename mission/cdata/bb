{{Infobox country
|conventional_long_name = Barbados
|common_name = Barbados
|image_flag = Flag of Barbados.svg
|image_coat = Barbados Coat of Arms.svg
|image_map = BRB orthographic.svg
|national_motto="Pride and Industry"
|national_anthem = ''[[National Anthem of Barbados|In Plenty and in Time of Need]]''<br/><center>[[File:In Plenty and In Time of Need instrumental.ogg]]</center>
|official_languages = English
|regional_languages = [[Bajan Creole|Bajan]]
|ethnic_groups =
{{vunblist
| 93% [[Afro-Barbadian|Black]]
| 3.2% [[White Barbadian|White]]
| 2.6% [[Multiracial]]
| 1% [[Indians in Barbados|East Indian]]
| 0.2% Other
}}
|ethnic_groups_year = 2000<ref name="cia"/>
|religion = {{vunblist |74.6% Christian |4.8% other |20.6% none{{\}}unspecified<ref name="cia"/>}}
|capital = [[Bridgetown]]
|latd=13 |latm=06 |latNS=N |longd=59 |longm=37 |longEW=W
|largest_city = capital
|demonym = {{nowrap|[[Barbadian people|Barbadian]]<br/>Bajan {{small|([[colloquial]])}}}}
|government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional monarchy]]
|leader_title1 = [[Monarchy of Barbados|Monarch]]
|leader_name1 = [[Elizabeth II]]
|leader_title2 = [[Governor-General of Barbados|Governor-General]]
|leader_name2 = [[Elliott Belgrave]]
|leader_title3 = [[List of Prime Ministers of Barbados|Prime Minister]]
|leader_name3 = [[Freundel Stuart]]
|legislature = [[Parliament of Barbados|Parliament]]
|upper_house = [[Senate of Barbados|Senate]]
|lower_house = [[House of Assembly of Barbados|House of Assembly]]
|area_km2 = 432
|area_sq_mi = 166<!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 200th
|area_magnitude = 1 E8
|percent_water = negligible
|population_census = 277,821<ref name=geo>{{cite web |url=http://www.geohive.com/cntry/barbados.aspx |title=GeoHive – Barbados population |publisher=GeoHive |accessdate=16 December 2013}}</ref>|population_census_rank = 181st
|population_census_year = 2010
|population_density_km2 = 660
|population_density_sq_mi = 1,704<!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 15th
|GDP_PPP = $7.053 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=316&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=&pr.x=52&pr.y=10 |title=Barbados |publisher=International Monetary Fund |accessdate=18 April 2013}}</ref>
|GDP_PPP_rank = 
|GDP_PPP_year = 2013
|GDP_PPP_per_capita = $25,100<ref name=imf2/>
|GDP_PPP_per_capita_rank = 
|GDP_nominal = $4.490 billion<ref name=imf2/>
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $16,151<ref name=imf2/>
|sovereignty_type = [[Independence]]
|established_event1 = from the [[United Kingdom]]
|established_date1 = 30 November 1966
|Gini_year = |Gini_change = <!--increase/decrease/steady--> |Gini = <!--number only--> |Gini_ref = |Gini_rank =
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.825 <!--number only-->
|HDI_ref =
|HDI_rank = 38th
|currency = [[Barbadian dollar]] ($) (Although USD is widely accepted)
|currency_code = BBD
|country_code =
|time_zone = [[Eastern Caribbean Time Zone|Eastern Caribbean]]
|utc_offset = -4
|time_zone_DST = not observed
|utc_offset_DST = -4
|drives_on = left<ref name=fco>{{Wayback |date=20060829184000 |url=http://web.archive.org/web/20071015163750/http://www.fco.gov.uk/servlet/Front?pagename=OpenMarket/Xcelerate/ShowPage&c=Page&cid=1007029390590&a=KCountryAdvice&aid=1013618386991 |title=Barbados }} (fco.gov.uk), updated 5 June 2006.</ref>
|calling_code = [[North American Numbering Plan|+1]] [[Area code 246|-246]]
|cctld = [[.bb]]
}}
