{{Infobox country
| conventional_long_name = Republic of El Salvador
| native_name = ''República de El Salvador''
| common_name = El Salvador
| image_flag = Flag of El Salvador.svg
| image_coat = Coats of arms of El Salvador.svg
| national_motto = {{native phrase|es|"Dios, Unión, Libertad"|italics=off}}<br />{{small|{{lang-en|"God, Unity, Freedom"}}}}
| national_anthem = ''[[National Anthem of El Salvador|Himno Nacional de El Salvador]]''<br />{{small|{{lang-en|"National Anthem of El Salvador"}}}}
| image_map = LocationElSalvador.svg 
| official_languages = [[Spanish language|Spanish]]
| ethnic_groups =<ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/es.html|title=CIA The World Factbook: People and Society - El Salavador}}</ref>
{{vunblist
| 86% [[Mestizo]]
| 12% [[White Latin American|White]]
| 1%  [[Indigenous peoples of the Americas#El Salvador|Indigenous]]
}}
| demonym = {{unbulleted list |Salvadoran |Salvadorian |Salvadorean}}
| capital = [[San Salvador]]
| latd=13 |latm=40 |latNS=N |longd=89 |longm=10 |longEW=W
| largest_city = [[San Salvador]]
| government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[constitutional republic]]
| leader_title1 = [[President of El Salvador|President]]
| leader_name1 = [[Salvador Sánchez Cerén]]
| legislature = [[Legislative Assembly of El Salvador|Legislative Assembly]]
| sovereignty_type = [[Independence]]
| established_event1 = from [[Spain]]
| established_date1 = September 15, 1821
| established_event2 = {{nowrap|Recognized by Spain}}
| established_date2 = June 24, 1865
| established_event3 = from the [[Greater Republic of Central America]]
| established_date3 = November 13, 1898
| area_rank = 153rd
| area_magnitude = 1 E8
| area_km2 = 21,040
| area_sq_mi = 8,124 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = 1.4
| population_estimate = 6,134,000<ref name="UNdata">{{cite web |url=http://data.un.org/CountryProfile.aspx?crName=El%20Salvador |title=UNdata El Salvador |publisher=UN |year=2008 |accessdate=2010-07-04}}</ref>
| population_estimate_rank = 99th
| population_estimate_year = July 2009
| population_census = 5,744,113<ref name=r1>[http://www.censos.gob.sv/util/datos/Resultados%20VI%20Censo%20de%20Poblaci%F3n%20V%20de%20Vivienda%202007.pdf Resultados Vi Censo de Poblacion V de Vivienda 2007], censos.gob.sv<!--PLEASE READ PAGE 25, OFFICIAL INFORMATION FROM EL SALVADOR CENSUS 2007--></ref>
| population_census_year = 2009
| population_density_km2 = 341.5
| population_density_sq_mi = 884.4 <!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 47th
| GDP_PPP = $44.576 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2012/01/weodata/weorept.aspx?pr.x=105&pr.y=18&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=253&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=El Salvador |publisher=International Monetary Fund |accessdate=2012-04-18}}</ref>
| GDP_PPP_year = 2011
| GDP_PPP_per_capita = $7,549<ref name=imf2/>
| GDP_nominal = $22.761 billion<ref name=imf2/>
| GDP_nominal_year = 2011
| GDP_nominal_per_capita = $3,855<ref name=imf2/>
| Gini_year = 2002
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 52.4 <!--number only-->
| Gini_ref = | Gini_rank =
| HDI_year = 2010
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.659 <!--number only-->
| HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2010_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=5 November 2010}}</ref>
| HDI_rank = 90th
| currency = [[United States dollar]]<sup>a</sup>
| currency_code = USD
| country_code = 5o3
| time_zone = [[Central Time Zone|CST]]
| utc_offset = −6
| drives_on = right
| calling_code = [[+503]]<sup>b</sup>
| cctld = [[.sv]]
| footnote_a = The [[United States dollar]] is the currency in use. Financial information can be expressed in U.S. dollars and in [[Salvadoran colón]], but the colón is out of circulation.{{big|<ref>[http://web.archive.org/web/20070708094105/http://www.bcr.gob.sv/ingles/integracion/ley.html Main Aspects of the Law]. bcr.gob.sv</ref>}}
| footnote_b = Telephone companies (market share): Tigo (45%), Claro (25%), Movistar (24%), Digicel (5.5%), Red (0.5%).
<!----ORPHANED:
| footnote_? = On the [[Coat of arms of El Salvador|Coat of Arms of El Salvador]], the country's name is written "Republica de El Salvador en la America Central", meaning "Republic of El Salvador in Central America"
----->
}}
