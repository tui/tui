{{Infobox country
|conventional_long_name = Russian Federation
|native_name = Российская Федерация <br /> ''Rossiyskaya Federatsiya''
|common_name = Russia
|image_flag = Flag of Russia.svg
|image_coat = Coat of Arms of the Russian Federation.svg
|national_anthem = <br />"[[National Anthem of Russia|Государственный гимн Российской Федерации]]"<br />"''[[National Anthem of Russia|Gosudarstvenny gimn Rossiyskoy Federatsii]]''"{{nbsp|2}}{{small|([[Romanization of Russian|transliteration]])}}<br />{{small|"State Anthem of the Russian Federation"}}
|image_map = Russian Federation (orthographic projection) - Crimea disputed.svg
|map_width = 220px
|map_caption =
Russia proper (dark green)<br>
[[Political status of Crimea|Disputed]] [[Crimea|Crimean peninsula]] ([[United Nations General Assembly Resolution 68/262|internationally viewed]] as territory of [[Ukraine]], but de facto [[Annexation of Crimea by the Russian Federation|administered]] by Russia) (light green)<ref>{{cite web|last=Taylor|first=Adam|title=Crimea has joined the ranks of the world’s ‘gray areas.’ Here are the others on that list.|url=http://www.washingtonpost.com/blogs/worldviews/wp/2014/03/22/crimea-has-joined-the-ranks-of-the-worlds-gray-areas-here-are-the-others-on-that-list/|publisher=The Washington Post|accessdate=27 March 2014}}</ref>
|capital = [[Moscow]]
|latd=55 |latm=45 |latNS=N |longd=37 |longm=37 |longEW=E
|largest_city = capital
|official_languages = [[Russian language|Russian]] official throughout the country; [[Languages of Russia|27 other languages co-official]] in various regions
|ethnic_groups =
{{unbulleted list
| 81.0% [[Russians|Russian]]
| 3.7% [[Tatars|Tatar]]
| 1.4% [[Ukrainians in Russia|Ukrainian]]
| 1.1% [[Bashkirs|Bashkir]]
| 1.0% [[Chuvash people|Chuvash]]
| 0.8% [[Chechen people|Chechen]]
| {{nowrap|11.0% others{{\}}unspecified}}
}}
|ethnic_groups_year = 2010<ref name="perepis-2010.ru"/>
|demonym = [[Rossiyane|Russians (Rossiyane)]]
|government_type = [[Federalism|Federal]] [[Semi-presidential system|semi-presidential]] [[Constitution of Russia|constitutional]] [[republic]]
|leader_title1 = [[President of Russia|President]]
|leader_name1 = [[Vladimir Putin]]
|leader_title2 = [[Prime Minister of Russia|Prime Minister]]
|leader_name2 = [[Dmitry Medvedev]]
|leader_title3 = [[Federation Council (Russia)|Chairman of the Federation Council]]
|leader_name3 = [[Valentina Matviyenko]]
|leader_title4 = [[Chairman of the State Duma]]
|leader_name4 = [[Sergey Naryshkin]]
|legislature = [[Federal Assembly (Russia)|Federal Assembly]]
|upper_house = [[Federation Council (Russia)|Federation Council]]
|lower_house = [[State Duma]]
|sovereignty_type = [[Sovereign state|Formation]]
|established_event1 = Arrival of [[Rurik]], considered as a foundation event by the Russian authorities<ref>[http://www.1150russia.ru/ukaz-prezidenta-rf-o-prazdnovanii-1150-letiya-zarozhdeniya-rossiyskoy-gosudarstvennosti.html Указ Президента РФ "О праздновании 1150-летия зарождения российской государственности"] {{Ru icon}}</ref>
|established_date1 = 862
|established_event2 = [[Kievan Rus']]
|established_date2 = 882
|established_event3 = [[Grand Duchy of Moscow]]
|established_date3 = 1283
|established_event4 = [[Tsardom of Russia]]
|established_date4 = 16 January 1547
|established_event5 = [[Russian Empire]]
|established_date5 = 22 October 1721
|established_event6 = [[Russian Soviet Federative Socialist Republic|Russian SFSR]]
|established_date6 = 6 November 1917
|established_event7 = [[Soviet Union]]
|established_date7 = 10 December 1922
|established_event8 = Russian Federation
|established_date8 = 25 December 1991
|established_event9 = Adoption of the current Constitution of Russia
|established_date9 = 12 December 1993
|area_km2 = 17,098,242 (Crimea not included)
|area_sq_mi = 6,592,800 (Crimea not included)
|area_rank = 1st
|area_magnitude = 1 E13
|percent_water = 13<ref name=gen>{{cite web |title=The Russian federation: general characteristics |url=http://www.gks.ru/scripts/free/1c.exe?XXXX09F.2.1/010000R |archiveurl=https://web.archive.org/web/20110728064121/http://www.gks.ru/scripts/free/1c.exe?XXXX09F.2.1/010000R |archivedate=28 July 2011 |work=Federal State Statistics Service |accessdate=5 April 2008}}</ref>&nbsp;{{small|(including swamps)}}
|population_estimate = 143,700,000<ref>{{cite web | url=http://www.gks.ru/bgd/free/B14_00/IssWWW.exe/Stg//%3Cextid%3E/%3Cstoragepath%3E::%7Cdk01/7-0.doc | title=демография (Demography) | year=2014 | publisher=[[Russian Federal State Statistics Service]] | language=Russian | accessdate=21 March 2014}}</ref>
|population_estimate_year = 2014
|population_estimate_rank = 9th
|population_density_km2 = 8.4
|population_density_sq_mi = 21.5
|population_density_rank = 217th
|GDP_PPP_year = 2014
|GDP_PPP = $2.630 trillion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=45&pr.y=13&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=922&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=Russia |publisher=International Monetary Fund |accessdate=30 June 2014}}</ref>
|GDP_PPP_rank = 6th
|GDP_PPP_per_capita = $18,408<ref name=imf2/>
|GDP_PPP_per_capita_rank = 58th
|GDP_nominal = $2.092 trillion<ref name=imf2/>
|GDP_nominal_rank = 9th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $14,645<ref name=imf2/>
|GDP_nominal_per_capita_rank = 51st
|Gini_year = 2011
|Gini_change = <!--increase/decrease/steady-->
|Gini = 41.7 <!--number only-->
|Gini_ref =<ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of family income – Gini index |work=The World Factbook |publisher=CIA |accessdate=5 January 2014}}</ref>
|Gini_rank = 83rd
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.788 <!--number only-->
|HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR2013_EN_Complete.pdf |title=Human Development Report 2013 |publisher=United Nations Development Programme |date= 14 March 2013 |accessdate= 14 March 2013}}</ref>
|HDI_rank = 55th
|currency = [[Russian ruble]]
|currency_code = RUB
|utc_offset = +3 to +12<sup>a</sup>
|date_format = dd.mm.yyyy
|drives_on = right
|calling_code = [[Telephone numbers in Russia|+7]]
|cctld = {{unbulleted list |[[.ru]] |[[.su]] |[[.рф]]}}
|footnote_a = Excluding +5.
}}
