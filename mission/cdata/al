{{Infobox country
| conventional_long_name = Republic of Albania
| native_name = ''{{lang|sq|Republika e Shqipërisë}}''
| common_name = Albania
| national_motto = {{small|(official)}}
{{vunblist
| {{native phrase|sq|italics=off|"Ti Shqipëri, më jep nder, më jep emrin Shqipëtar"}}
| {{nowrap|{{small|"You Albania, give me honor, give me the name Albanian"}}}}
}}
| national_anthem =
{{vunblist
| {{native name|sq|nolink=yes| [[Himni i Flamurit|Betimi mbi flamur]]}}
| {{small|''Oath on the flag''}}
| <center>[[File:Hymni i Flamurit instrumental.ogg]]</center>
}}
| image_flag = Flag of Albania.svg
| image_coat = Albania state emblem.svg
| symbol_type = Coat of arms
| image_map = Europe-Albania.svg
| map_caption = {{map caption |location_color=green |region=Europe |region_color=dark grey |legend=Location Albania Europe.png}}
| official_languages = [[Albanian language|Albanian]]<sup>a</sup>
| capital = [[Tirana]]
| latd=41 |latm=20 |latNS=N |longd=19 |longm=48 |longEW=E
| largest_city = capital
| demonym = [[Albanians|Albanian]]
| government_type = [[Unitary state|Unitary]] [[Parliamentary system|parliamentary]] [[constitutional republic]]
| ethnic_groups = {{vunblist |95% [[Albanians|Albanian]],|3-6% [[Greeks in Albania|Greek]] <ref name="RFE/RL Research Report: Weekly Analyses from the RFE/RL Research Institute">{{cite book|title=RFE/RL Research Report: Weekly Analyses from the RFE/RL Research Institute|url=http://books.google.com/books?id=RxgkAQAAIAAJ|accessdate=22 December 2012|year=1993|publisher=Radio Free Europe/Radio Liberty, Incorporated|quote=Albanian officials alleged that the priest was promoting irredentist sentiments among Albania's Greek minority&nbsp;– estimated at between 60,000 and 300,000.}}</ref><ref name="BideleuxJeffries2006">{{cite book|author1=Robert Bideleux|author2=Ian Jeffries|title=The Balkans: A Post-Communist History|url=http://books.google.com/books?id=5jrHOKsU9pEC&pg=PA49|accessdate=6 September 2013|date=15 November 2006|publisher=Routledge|isbn=978-0-203-96911-3|page=49|quote=The Albanian government claimed that there were only 60,000, based on the biased 1989 census, whereas the Greek government claimed that there were upwards of 300,000. Most Western estimates were around the 200,000 mark ...}}</ref><ref name="Ramet1998">{{cite book|author=Sabrina P. Ramet|title=Nihil Obstat: Religion, Politics, and Social Change in East-Central Europe and Russia|url=http://books.google.com/books?id=ZvMi6paTOlcC&pg=PA222|accessdate=6 September 2013|year=1998|publisher=Duke University Press|isbn=978-0-8223-2070-8|page=222|quote=that between 250,000 and 300,000 Orthodox Greeks reside in Albania}}</ref><ref name="Jeffries2002">{{cite book|author=Ian Jeffries|title=Eastern Europe at the Turn of the Twenty-first Century: A Guide to the Economies in Transition|url=http://books.google.com/books?id=L7PBtDujYt0C&pg=PA69|accessdate=6 September 2013|year=2002|publisher=Routledge|isbn=978-0-415-23671-3|page=69|quote=It is difficult to know how many ethnic Greeks there are in Albania. The Greek government, it is typically claimed, says that there are around 300,000 ethnic Greeks in Albania, but most Western estimates are around the 200,000 mark.}}</ref><ref name="cia">{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/geos/al.html |publisher=Central Intelligence Agency |title=The World Factbook: Albania |accessdate=21 June 2013}}</ref><ref name="Publications2008">{{cite book|author=Europa Publications|title=The Europa World Year Book 2008|url=http://books.google.com/books?id=Oabsu05rJNoC|accessdate=22 December 2012|date=24 June 2008|publisher=Taylor & Francis|isbn=978-1-85743-452-1|quote=...and Greece formally annulled claims to North Epirus (southern Albania), where there is a sizeable Greek minority. ... strained by concerns relating to the treatment of ethnic Greeks residing in Albania (numbering an estimated 300,000) ...}}</ref><ref name="NoStaff2007">{{cite book|author1=Author No|author2=Europa Publications Staff|title=The Europa World Year Book: 2007|url=http://books.google.com/books?id=BR8eTxcLZuoC|accessdate=22 December 2012|date=6 September 2007|publisher=Routledge|isbn=978-1-85743-413-2|quote=During the early 1990s, however, bilateral relations were severely strained by concerns relating to the treatment of ethnic Greeks residing in Albania (numbering an estimated 300,000) and to ...}}</ref><ref name="House">{{cite book|author=United States, Committee on Armed Services, General Accounting Office, Congress, National Security and International Affairs Division, House|title=Balkans security : current and projected factors affecting regional stability : briefing report to the Chairman, Committee on Armed Services, House of Representatives|url=http://books.google.com/books?id=JaZ6SMwuRjkC&pg=PA14|accessdate=22 December 2012|publisher=DIANE Publishing|isbn=978-1-4289-7030-4|page=14}}</ref> |2% other ([[Aromanians|Aromanian]], [[Macedonians of Albania|Macedonian]] etc.)<ref name="cia"/>}}
| leader_title1 = [[List of heads of state of Albania|President]]
| leader_name1 = [[Bujar Nishani]]
| leader_title2 = [[List of Prime Ministers of Albania|Prime Minister]]
| leader_name2 = [[Edi Rama]]
|leader_title3 = [[Chairman of the Parliament of Albania|Speaker of the parliament]]
|leader_name3 = [[Ilir Meta]]
| legislature = ''[[Parliament of Albania|Kuvendi]]''
| area_rank = 143rd
| area_magnitude = 1 E10
| area_km2 = 28748
| area_sq_mi = 11100 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = 4.7
|population_estimate = 3,011,405 <ref>{{cite web|url=https://www.cia.gov/library/publications/the-world-factbook/geos/al.html |title=The World Factbook |publisher=Cia.gov |date= |accessdate=2014-02-15}}</ref>
|population_estimate_year = 2013
| population_census = 2,821,977<ref name="Population and Housing Census 2011">{{cite web|title=Population and Housing Census 2011|url=http://www.instat.gov.al/en/themes/population/publications/books/2012/main-results-of-population-and-housing-census-2011.aspx|publisher=INSTAT (Albanian Institute of Statistics)}}</ref>
| population_census_year = 2011
| population_density_km2 = 98
| population_density_sq_mi = 254 <!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 63rd
| GDP_PPP_year = 2014 
| GDP_PPP = $31 billion <ref name=Min2>{{cite web |url=http://www.financa.gov.al/files/userfiles/Programimi_EkonomikoFiskal/Kuadri_Makroekonomik_dhe_Fiskal/KMF_Periudhen_2014-2016_VKM_NR.73_date_23.01.2013.pdf |title=Ministry of Economy of Albania |date= |accessdate=2014-02-15}}</ref>
| GDP_PPP_rank = 
| GDP_PPP_per_capita = $9,903<ref name=imf2>http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=79&pr.y=13&sy=2012&ey=2019&scsm=1&ssd=1&sort=country&ds=.&br=1&c=914&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=</ref>–$11,400<ref name=Min2/>
| GDP_PPP_per_capita_rank = 
| GDP_nominal_year = 2014 
| GDP_nominal = $14 billion<ref name=imf2/>
| GDP_nominal_rank = 
| GDP_nominal_per_capita = $5,000<ref name=imf2/>
| GDP_nominal_per_capita_rank = 
| Gini_year = 2008
| Gini_change = <!--increase/decrease/steady-->
| Gini = 26.7 <!--number only-->
| Gini_ref = <ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of family income&nbsp;– Gini index |work=The World Factbook |publisher=CIA |accessdate=1 September 2009 |archiveurl=http://www.webcitation.org/5rRcwIiYs|archivedate=23 July 2010 |deadurl=no}}</ref>
| Gini_rank = 
| HDI_year = 2013
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.749 <!--number only-->
| HDI_ref = <ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2010_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=5 November 2010}}</ref>
| HDI_rank = 70th
| sovereignty_type = [[History of the Albanian state|Formation]]
| established_event1 = [[Principality of Arbër]]
| established_date1 = 1190
| established_event2 = [[League of Lezhë]]
| established_date2 = 2 March 1444
| established_event3 = [[Independent Albania]]
| established_date3 = 28 November 1912
| established_event4 = [[Principality of Albania]]
| established_date4 = 29 July 1913
| established_event5 = {{nowrap|[[Constitution of Albania|Current constitution]]}}
| established_date5 = 28 November 1998
| currency = [[Albanian lek|Lek]]
| currency_code = ALL
| country_code = AL
| time_zone = [[Central European Time|CET]]
| utc_offset = +1
| time_zone_DST = [[Central European Summer Time|CEST]]
| utc_offset_DST = +2
| drives_on = right
| calling_code = [[Telephone numbers in Albania|355]]
| date_format = dd.mm.yyyy
| cctld = [[.al]]
| footnote_a = [[Greek language|Greek]], [[Vlach language|Vlach]], [[Macedonian language|Macedonian]], and other regional languages are government-recognized minority languages.
}}
