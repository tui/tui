{{Infobox country
| conventional_long_name = Republic of Botswana
| native_name = {{smaller|{{native name|tn|Lefatshe la Botswana}}}}
| common_name = Botswana
| image_flag = Flag of Botswana.svg
| image_coat = Arms of Botswana.svg
| image_map = Location Botswana AU Africa.svg
| map_caption = {{map caption |countryprefix= |location_color=dark blue |region=Africa |region_color=dark grey |subregion=the [[African Union]] |subregion_color=light blue |legend=Location Botswana AU Africa.svg}}
| image_map2 = Botswana - Location Map (2013) - BWA - UNOCHA.svg
| national_motto = {{native phrase|tn|"Pula"|italics=off|nolink=yes}}<br/>{{small|"Rain"}}
| national_anthem = {{native phrase|tn|[[Fatshe leno la rona]]|nolink=yes}}<br/>{{small|''This Land of Ours''}}
| official_languages = {{Unbulleted list|[[English language|English]] |[[Setswana language|Setswana]]}}
| demonym = {{Unbulleted list|[[Batswana]] |Motswana}}
| ethnic_groups = {{Unbulleted list|79% [[Tswana people|Tswana]] |11% [[Kalanga people|Kalanga]] |3% [[San people|Basarwa]] |3% [[Kgalagadi]] |3% [[White people in Botswana|White]] |1% others}}
| ethnic_groups_year =
| capital = [[Gaborone]]
| latd=24 |latm=39.5 |latNS=S |longd=25 |longm=54.5 |longEW=E
| largest_city = capital
| government_type = [[Unitary state|Unitary]] [[parliamentary republic]] <!--cia.gov-->
| leader_title1 = [[President of Botswana|President]]
| leader_name1 = [[Ian Khama]]
| leader_title2 = [[Vice-President of Botswana|Vice-President]]
| leader_name2 = [[Ponatshego Kedikilwe]]
| legislature = [[National Assembly]]
| sovereignty_type = [[Independence]]
| sovereignty_note = from the [[United Kingdom]]
| established_event1 = Established ([[Constitution of Botswana|Constitution]])
| established_date1 = 30 September 1966
| area_rank = 48th
| area_magnitude = 1 E11
| area_km2 = 581,730
| area_sq_mi = 224,610 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = 2.6
| population_estimate = 2,155,784<ref name="cia">{{cite web |title=Botswana |work=The World Factbook |publisher=Central Intelligence Agency |year=2014 |url=https://www.cia.gov/library/publications/the-world-factbook/geos/bc.html |accessdate=16 April 2014}}</ref>
| population_estimate_rank = 145th
| population_estimate_year = 2014
| population_census = 2,038,228 <ref>[http://www.cso.gov.bw/templates/cso/file/File/Census%202011%20Preliminary%20%20Brief%20Sept%2029%202011.pdf 2011 Population & Housing Census Preliminary Results Brief]</ref>
| population_census_year = 2011
| population_density_km2 = 3.4
| population_density_sq_mi = 8.9
| population_density_rank = 231st
| GDP_PPP = $35.978 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=113&pr.y=16&sy=2009&ey=2012&scsm=1&ssd=1&sort=country&ds=.&br=1&c=616&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Botswana |publisher=International Monetary Fund |accessdate=2012-04-17}}</ref>
| GDP_PPP_rank =
| GDP_PPP_year = 2014
| GDP_PPP_per_capita = $17,101<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $16.046 billion<ref name=imf2/>
| GDP_nominal_rank =
| GDP_nominal_year = 2014
| GDP_nominal_per_capita = $7,627<ref name=imf2/>
| GDP_nominal_per_capita_rank =
| Gini_year = 1993
| Gini_change = <!--increase/decrease/steady-->
| Gini = 63
| Gini_ref =<ref name="cia"/>
| Gini_rank =
| HDI_year = 2013
| HDI_change = decrease <!--increase/decrease/steady-->
| HDI = 0.634
| HDI_ref =<ref name="HDI">{{cite web |url=http://hdr.undp.org/en/media/HDR_2010_EN_Table1.pdf |title=Human Development Report 2010 |year=2010 |publisher=United Nations |accessdate=5 November 2010}}</ref>
| HDI_rank = 119th
| currency = [[Botswana pula|Pula]]
| currency_code = BWP
| country_code =
| time_zone = [[Central Africa Time]]
| utc_offset = +2
| time_zone_DST = not observed
| utc_offset_DST =
| drives_on = left
| calling_code = [[+267]]
| cctld = [[.bw]]
}}
