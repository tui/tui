{{Infobox country
|name = Arab Republic of Egypt
|native_name = {{lang|ar|جمهورية مصر العربية}}<br/>''{{transl|ar|DIN|Ǧumhūriyyat Miṣr al-ʿArabiyyah}}''
|common_name = Egypt
|image_flag = Flag of Egypt.svg
|image_coat = Coat of arms of Egypt (Official).svg
|image_map = Egypt (orthographic projection).svg
|map_caption = 
|image_map2 = Egypt - Location Map (2013) - EGY - UNOCHA.svg
|national_anthem = ''[[Bilady, Bilady, Bilady]]''<br/>{{small|''My country, my country, my country''}}<br/><center>[[File:Bilady, Bilady, Bilady.ogg]]</center>
|official_languages = [[Arabic]]{{ref label|offlang|a|}}
|languages_type = [[National language]]
|languages = [[Egyptian Arabic]]
|official_religion = [[Sunni Islam]]
|demonym = Egyptian
|capital = [[Cairo]]
|latd=30 |latm=2 |latNS=N |longd=31 |longm=13 |longEW=E
|largest_city = Cairo
|government_type =  {{nowrap|[[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]]<br>[[constitutional republic]]}}
|leader_title1 = [[President of Egypt|President]]
|leader_name1 = [[Abdel Fattah el-Sisi]]
|leader_title2 = {{small|[[Prime Minister of Egypt|Acting Prime Minister]]}}
|leader_name2 = {{small|[[Ibrahim Mahlab]]}}
|legislature = [[House of Representatives (Egypt)|House of Representatives]] ''(dissolved)''
|lower_house =
|established_event1 = {{nowrap|Unification of [[Upper and Lower Egypt|Upper<br/>and Lower Egypt]]<ref>{{cite book |last1=Goldschmidt |first1=Arthur |title=Modern Egypt: The Formation of a Nation-State |url=http://books.google.com/books?id=YmZyAAAAMAAJ&q=state |year=1988 |publisher=Westview Press |location=Boulder, CO |isbn=978-0-86531-182-4 |page=5 |quote=Among the peoples of the ancient Near East, only the Egyptians have stayed where they were and remained what they were, although they have changed their language once and their religion twice. In a sense, they constitute the world's oldest nation. For most of their history, Egypt has been a state, but only in recent years has it been truly a nation-state, with a government claiming the allegiance of its subjects on the basis of a common identity.}}</ref><ref name="U.S.Dept of State/Egypt">{{cite web |url=http://www.state.gov/r/pa/ei/bgn/5309.htm |title=Background Note: Egypt |date=10 November 2010 |publisher=[[United States Department of State]] [[Bureau of Near Eastern Affairs]] |accessdate=5 March 2011}}</ref><!--end nowrap:-->}}
|established_date1 = [[Circa|c.]] 3200 BC
|established_event2 = [[Muhammad Ali Dynasty]] inaugurated
|established_date2 = 9 July 1805<ref name="Crabitès">{{cite book |author=Pierre Crabitès |title=Ibrahim of Egypt |url=http://books.google.com/books?id=1NbCRckI3EoC&pg=PA1 |accessdate=10 February 2013 |year=1935 |publisher=Routledge |isbn=978-0-415-81121-7 |page=1 |quote=...&nbsp;on July 9, 1805, Constantinople conferred upon Muhammad Ali the pashalik of Cairo&nbsp;...}}</ref>
|established_event3 = {{nowrap|[[Unilateral Declaration of Egyptian Independence|Independence]] from<br/>the [[United Kingdom of Great Britain and Ireland|United Kingdom]]}}
|established_date3 = 28 February 1922
|established_event4 = [[Egyptian Revolution of 1952|Republic declared]]
|established_date4 = 18 June 1953
|established_event5 = [[Egyptian Revolution of 2011|Revolution Day]]
|established_date5 = 25 January 2011
|established_event6 = [[Egyptian Constitution of 2014|Current Constitution]]
|established_date6 = 18 January 2014
|area_rank = 30th
|area_magnitude = 1 E12
|area_km2 = 1,002,450
|area_sq_mi = 387,048
|percent_water = 0.632
|population_estimate = 86,502,500<ref name="popclock" /> 
|population_estimate_year = 2014
|population_estimate_rank = 15th
|population_census = 72,798,000<ref name="pop1882-2006"/>
|population_census_year = [[Census in Egypt#2006|2006]]
|population_density_km2 = 84
|population_density_sq_mi = 218 <!--Do not remove per [[WP:MOSNUM]] -->
|population_density_rank = 126th
|GDP_PPP_year = 2014
|GDP_PPP = {{nowrap|$576.350 billion<ref name=imf2>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=83&pr.y=14&sy=2010&ey=2013&scsm=1&ssd=1&sort=country&ds=.&br=1&c=469&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=|title=Egypt|publisher=International Monetary Fund|accessdate=18 April 2012}}</ref><!--end nowrap:-->}}
|GDP_PPP_rank =
|GDP_PPP_per_capita = $6,714<ref name=imf2/>
|GDP_PPP_per_capita_rank =
|GDP_nominal = $275.748 billion<ref name=imf2/>
|GDP_nominal_rank =
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $3,213<ref name=imf2/>
|GDP_nominal_per_capita_rank =
|Gini_year = 2008
|Gini_change = <!--increase/decrease/steady-->
|Gini = 30.8 <!--number only-->
|Gini_ref = <ref>{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=GINI index |publisher=World Bank |accessdate=2013-02-08}}</ref>
|Gini_rank =
|HDI_year = 2013
|HDI_change = steady <!--increase/decrease/steady-->
|HDI = 0.662 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web|url=http://hdr.undp.org/en/reports/global/hdr2013/|title= The 2013 Human Development Report – "The Rise of the South: Human Progress in a Diverse World"|publisher=[[Human Development Report|HDRO (Human Development Report Office)]] [[United Nations Development Programme]]|pages=144–147|accessdate=28 November 2013}}</ref>
|HDI_rank = 112th
|currency = [[Egyptian pound]]
|currency_code = EGP
|time_zone = [[Eastern European Time|EET]]
|utc_offset = +2
|time_zone_DST = [[Eastern European Summer Time|EEST]]
|utc_offset_DST = +3{{ref label|dst|b|}}
|drives_on = right
|calling_code = [[+20]]
|cctld = {{unbulleted list |[[.eg]] |[[مصر.]]}}
|footnote_a = {{note|offlang}} [[Modern Standard Arabic|Literary Arabic]] is the sole official language.{{big|<ref name="Provisional Constitution"/>}} [[Egyptian Arabic]] is the national [[spoken language]]. Other [[Languages of Egypt|dialects and minority languages]] are spoken regionally.
|footnote_b = {{note|dst}} [[Summer time]] has been reintroduced [[Daylight saving time in Egypt|in 2014]].
}}
