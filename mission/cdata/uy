{{Infobox country
|conventional_long_name = Oriental Republic of Uruguay
|native_name = {{unbulleted list|item_style=font-size:88%;|{{native name|es|República Oriental del Uruguay}}}}
|common_name = Uruguay
|image_flag = Flag of Uruguay.svg
|alt_flag =
|image_coat = Coat of arms of Uruguay.svg
|image_map = Uruguay (orthographic projection).svg
|map_width = 220px
|alt_map =|map_caption =
|national_motto = {{native phrase|es|"Libertad o Muerte"|italics=off|nolink=on}}<br>{{small|"Freedom or Death"}}
|national_anthem = ''[[National Anthem of Uruguay|Himno Nacional de Uruguay]]''<br>{{small|''National Anthem of Uruguay''}}<br><center>[[File:HimnoNacionalUruguay.ogg]]</center>
|capital = [[Montevideo]]
|latd=34|latm=53|latNS=S|longd=56|longm=10|longEW=W
|largest_city = capital
|official_languages = [[Spanish language|Spanish]]
|national_languages  =
|recognized_minority_languages = [[Portuguese language|Portuguese]]
|languages_type =|languages =
|ethnic_groups ={{unbulleted list|90.7% [[White Latin American|White]]|4.8% [[Afro-Uruguayan|Black]]|2.4% [[Indigenous peoples|Indigenous]]|0.5% [[Asian Latin American|Asian]]|1.9% [[Other/None]]}}
|ethnic_groups_year = 2011<ref name=enha_asc>{{cite web|title=Atlas Sociodemografico y de la Desigualdad en Uruguay , 2011: Ancestry |language=Spanish |format=PDF |publisher=National Institute of Statistics |url=http://www.ine.gub.uy/biblioteca/Atlas_Sociodemografico/Atlas_fasciculo_2_Afrouruguayos.pdf  |pages=15–16}}</ref>
|demonym = [[Uruguayan people|Uruguayan]]
|government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[constitutional republic]]
|leader_title1 = [[President of Uruguay|President]]
|leader_name1 = [[José Mujica]]
|leader_title2 = [[Vice President of Uruguay|Vice President]]
|leader_name2 = [[Danilo Astori]]
|legislature = [[General Assembly of Uruguay|General Assembly]]
|upper_house = [[Senate of Uruguay|Chamber of Senators]]
|lower_house = [[Chamber of Deputies of Uruguay|Chamber of Deputies]]
|sovereignty_type = [[Independence]] {{nobold|from the [[Empire of Brazil]]}}
|established_event1 = Declaration
|established_date1 = 25 August 1825
|established_event2 = [[1828 Treaty of Montevideo|Recognition]]
|established_date2 = 28 August 1828
|established_event3 = Constitution
|established_date3 = 18 July 1830
|area_rank = 91st
|area_magnitude =
|area_km2 = 176,215
|area_sq_mi = 68,037
|area_footnote =
|percent_water = 1.5
|population_estimate = 3,324,460<ref name="cia"/>
|population_estimate_rank = 133rd
|population_estimate_year = 2013
|population_census = 3,286,314<ref>[http://www.ine.gub.uy/censos2011/resultadosfinales/analisispais.pdf Resultados del Censo de Población 2011: población, crecimiento y estructura por sexo y edad] ine.gub.uy</ref>
|population_census_year = 2011
|population_density_km2 = 18.87
|population_density_sq_mi = 48.86
|population_density_rank = 198th
|GDP_PPP = $59.201 billion<ref name=imf2>{{cite web|url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?sy=2012&ey=2019&scsm=1&ssd=1&sort=country&ds=.&br=1&c=298&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=&pr.x=57&pr.y=5 |title=Report for Selected Countries and Subjects |work=World Economic Outlook |publisher=International Monetary Fund |date=8 April 2014 |accessdate=28 June 2014}}</ref>
|GDP_PPP_rank = 91st
|GDP_PPP_year = 2014
|GDP_PPP_per_capita = $17,391<ref name=imf2 />
|GDP_PPP_per_capita_rank = 61st
|GDP_nominal = $58.283 billion<ref name=imf2 />
|GDP_nominal_rank = 78th
|GDP_nominal_year = 2014
|GDP_nominal_per_capita = $17,121<ref name=imf2 />
|GDP_nominal_per_capita_rank = 45th
|Gini_year = 2010
|Gini_change = <!--increase/decrease/steady-->
|Gini = 45.3 <!--number only-->
|Gini_ref = <ref name="wb-gini">{{cite web|url=https://toolserver.org/~dispenser/cgi-bin/webchecklinks.py?page=Uruguay|title=Gini Index|publisher=World Bank|accessdate=14 April 2012}}</ref>
|Gini_rank =
|HDI_year = 2012
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.792 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web|url=http://hdr.undp.org/sites/default/files/hdr2013_en_summary.pdf|title=Human Development Report 2013|publisher=United Nations Development Programme|date=14 March 2013|accessdate=30 May 2014}}</ref>
|HDI_rank = 51st
|currency = [[Uruguayan peso]]
|currency_code = UYU
|time_zone = [[America/Montevideo|UYT]]
|utc_offset = −3
|time_zone_DST = [[Daylight saving time in Uruguay|UYST]]
|utc_offset_DST = −2
|DST_note =
|antipodes =
|date_format =
|drives_on = right
|calling_code = [[+598]]
|iso3166code =
|cctld = [[.uy]]
}}
