{{Infobox country
| conventional_long_name = Kingdom of Thailand
| native_name = {{lang|th|ราชอาณาจักรไทย}}<br/>''Ratcha Anachak Thai''
| common_name = Thailand
| image_flag = Flag of Thailand.svg
| image_coat = Garuda Emblem of Thailand.svg
| symbol_type = Emblem
| national_motto = <br/>{{small|(unofficial)}} {{native phrase|th|{{big|ชาติ ศาสนา พระมหากษัตริย์}}|italics=off}}<br/>''Chat, Satsana, Phra Maha Kasat''<br/>{{small|"Nation, Religions, King"}}
| national_anthem = ''[[Thai National Anthem|Phleng Chat Thai]]''<br/>''Thai National Anthem (instrumental)''<br/><center>[[File:Thai National Anthem - US Navy Band.ogg]]</center>
| royal_anthem = ''[[Sansoen Phra Barami]]''<br/>''Thai Royal Anthem (instrumental)''<br/><center>[[File:Thai Royal Anthem - US Navy Band.ogg]]</center>
| image_map = Thailand (orthographic projection).svg
| capital = [[Bangkok]]
| latd=13 |latm=45 |latNS=N |longd=100 |longm=29 |longEW=E
| largest_city = capital
| official_languages = [[Thai language|Thai]]<ref name=CIA>[https://www.cia.gov/library/publications/the-world-factbook/geos/th.html Thailand], CIA World Factbook.</ref><!--Thai is the official language. English is a non-official secondary language.-->
| languages_type = [[Official script]]
| languages = [[Thai alphabet]]
| ethnic_groups = 95.9% [[Demographics of Thailand|Thai origin]] <small>(incl.<br> [[Thai people|Central and Southern]],<br> [[Isan people|Northeastern]],<br> [[Northern Thai people|Northern]],<br> [[Thai Chinese]],<br> [[Northern Khmer people|Northern Khmer]],<br> [[Thai Malays]],<br> and "[[Hill tribe (Thailand)|Hill tribes]]")</small>,<br>2.0% [[Demographics of Burma|Bamar]],<br>1.3% other,<br>0.9% unspecified
|ethnic_groups_year = 2010<ref name=CIA/>
| demonym = [[Demographics of Thailand|Thai]]
| government_type = [[Constitutional monarchy]] administered by [[military junta]]
| leader_title1 = [[Monarchy of Thailand|King]]
| leader_name1 = [[Bhumibol Adulyadej]]
| leader_title2 = [[Prime Minister of Thailand|Head]] of the [[National Council for Peace and Order]]
| leader_name2 = [[Prayuth Chan-ocha]]
| legislature = [[National Assembly of Thailand]] ''(Dissolved)''
| sovereignty_type = [[History of Thailand|Formation]]
| established_event1 = [[Sukhothai Kingdom]]
| established_date1 = 1238–1448
| established_event2 = [[Ayutthaya Kingdom]]
| established_date2 = 1351–1767
| established_event3 = [[Thonburi Kingdom]]
| established_date3 = 1768–1782
| established_event4 = [[Rattanakosin Kingdom]]
| established_date4 = 6 April 1782
| established_event5 = {{nowrap|[[Siamese revolution of 1932|Constitutional monarchy]]}}
| established_date5 = 24 June 1932
| area_rank = 51st
| area_magnitude = 1 E11
| area_km2 = 513,120
| area_sq_mi = 198,115 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = {{nowrap|0.4 (2,230 km{{smallsup|2}})}}
| population_estimate = 66,720,153<ref>[https://web.archive.org/web/20110716001724/http://203.113.86.149/stat/pk/pk53/pk_53.pdf ประกาศสานักทะเบียนกลาง กรมการปกครอง เรื่อง จานวนราษฎรทั่วราชอาณาจักร แยกเป็นกรุงเทพมหานครและจังหวัดต่าง ๆ ตามหลักฐานการทะเบียนราษฎร ณ วันที่ 31 ธันวาคม 2553]. Web.archive.org (2011-07-16). Retrieved 20 May 2012.</ref>
| population_estimate_rank = 20th
| population_estimate_year = 2011
| population_census = 65,479,453<ref>{{th icon}} National Statistics Office, [http://popcensus.nso.go.th/doc/8-thailand%20census.doc "100th anniversary of population censuses in Thailand: Population and housing census 2010: 11th census of Thailand"]. popcensus.nso.go.th.</ref>
| population_census_year = 2010
| population_density_km2 = 132.1
| population_density_sq_mi = 342 <!--Do remove per [[WP:MOSNUM]]-->
| population_density_rank = 88th
| GDP_PPP = $701.554 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?sy=2011&ey=2018&scsm=1&ssd=1&sort=country&ds=.&br=1&pr1.x=49&pr1.y=10&c=578&s=NGDP_RPCH%2CNGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a= 
|title=Thailand |publisher=[[International Monetary Fund]] |accessdate=18 April 2013}}</ref>
| GDP_PPP_rank =
| GDP_PPP_year = 2013
| GDP_PPP_per_capita = $10,849<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = {{nowrap|$424.985 billion<ref name=imf2/>}}
| GDP_nominal_rank = 
| GDP_nominal_year = 2013
| GDP_nominal_per_capita = $6,572<ref name=imf2/>
| GDP_nominal_per_capita_rank = 
| Gini_year = 2010
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 39.4 <!--number only-->
| Gini_ref =<ref name="wb-gini">{{cite web |url=http://data.worldbank.org/indicator/SI.POV.GINI/ |title=Gini Index |publisher=World Bank |accessdate=2 March 2011}}</ref>
| Gini_rank =
| HDI_year =
| HDI_change = increase <!--increase/decrease/steady-->
| HDI = 0.690 <!--number only-->
| HDI_ref = <ref name="UNDP">{{cite web |url=http://hdr.undp.org/en/media/HDR_2011_EN_Tables.pdf |title=Human Development Report 2011 – Human development statistical annex |publisher=[[Human Development Report|HDRO (Human Development Report Office)]] [[United Nations Development Programme]] |pages=127–130 |accessdate=2 November 2011}}</ref>
| HDI_rank = 103rd
| currency = [[Thai baht|Baht]] (฿)
| currency_code = THB
| time_zone =
| utc_offset = +7
| time_zone_DST = | DST_note = | utc_offset_DST = <!-- +N, where N is number of hours -->
| drives_on = left
| calling_code = [[+66]]
| itu_prefix = HS, E2
| cctld = {{unbulleted list |[[.th]] |[[.ไทย]]}}
}}
