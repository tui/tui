{{Infobox country
| conventional_long_name = Republic of Colombia
| native_name = {{native name|es|República de Colombia|fontsize=70%}}
| common_name = Colombia
| image_flag = Flag of Colombia.svg
| image_coat = Coat of arms of Colombia.svg
| image_map = COL orthographic (San Andrés and Providencia special).svg
| image_map2= Colombia - Location Map (2013) - COL - UNOCHA.svg
| national_motto = {{native phrase|es|"Libertad y Orden"|italics=off|nolink=yes}}<br/>{{small|"Freedom and Order"}}
| national_anthem = {{native phrase|es|[[National Anthem of Colombia|¡Oh, Gloria Inmarcesible!]]|nolink=yes}}<br/>{{small|''O unfading glory!''}}<br/><center>[[File:United States Navy Band - ¡Oh, gloria inmarcesible!.ogg]]</center>
| capital = [[File:Bogota (escudo).svg|18px]] [[Bogotá|Bogotá D.C]]
| latd=4 |latm=35 |lats=53 |latNS=N |longd=74 |longm=4 |longs=33 |longEW=W
| largest_city = capital
| official_languages = [[Spanish language|Spanish]]<sup>a</sup>
| regional_languages = 68 ethnic languages and dialects. [[English language|English]] also official in the [[San Andrés, Providencia and Santa Catalina]] Islands.
| ethnic_groups =
{{vunblist
| 86% non-ethnic population (includes 49% [[Mestizo Colombian|Mestizo]] and 37.0% [[White Colombian|White]])
| 10.6% [[Afro-Colombian|Black]]
| {{nbsp|6}}{{small|(includes [[Mulatto]])}}
| 3.4% [[Indigenous peoples in Colombia|Amerindian]]
| 0.01% [[Romani people|Roma]]
}}
| ethnic_groups_year = 2005<ref name = "grupos étnicos">{{cite web|title= visibilización estadística de los grupos étnicos |url=http://www.dane.gov.co/files/censo2005/etnia/sys/visibilidad_estadistica_etnicos.pdf|work=Censo General 2005|publisher=Departamento Administrativo Nacional de Estadistica (DANE)|accessdate=15 June 2013}}</ref><ref name="The Society and Its Environment">Bushnell, David & Rex A. Hudson (2010) "[http://lcweb2.loc.gov/frd/cs/pdf/CS_Colombia.pdf The Society and Its Environment]"; ''Colombia: a country study'': 87. Washingtion D.C.: Federal Research Division, Library of Congress.</ref>
| demonym = Colombian
| government_type = [[Unitary state|Unitary]] [[Presidential system|presidential]] [[constitutional republic]]
| leader_title1 = [[President of Colombia|President]]
| leader_name1 = [[Juan Manuel Santos]]
| leader_title2 = [[Vice President of Colombia|Vice President]]
| leader_name2 = [[Angelino Garzón]]
| legislature = [[Congress of Colombia|Congress]]
| upper_house = [[Senate of Colombia|Senate]]
| lower_house = [[Chamber of Representatives of Colombia|Chamber of Representatives]]
| sovereignty_type = Independence {{nobold|from [[Spain]]}}
| established_event1 = [[Colombian Declaration of Independence|Declared]]
| established_date1 = 20 July 1810
| established_event2 = Recognized
| established_date2 = 7 August 1819
| established_event3 = {{nowrap|[[Colombian Constitution of 1991|Current constitution]]}}
| established_date3 = 4 July 1991
| area_rank = 26th
| area_magnitude = 1 E12
| area_km2 = 1,141,748
| area_sq_mi = 440,831 <!--Do not remove per [[WP:MOSNUM]]-->
| percent_water = 8.8 (17th)
| population_estimate = 47,425,437 <ref name="DANE clock">{{cite web |url=http://www.dane.gov.co/reloj/reloj_animado.php |title=Animated clock |publisher=Colombian State Department |accessdate=4 February 2012}}</ref>
| population_estimate_rank = 27th
| population_estimate_year = January 2014
| population_census = 42,888,592 <ref name="DANE clock"/>
| population_census_year = 2005
| population_density_km2 = 40.74
| population_density_sq_mi = 105.72<!--Do not remove per [[WP:MOSNUM]]-->
| population_density_rank = 173rd
| GDP_PPP = $559.659 &nbsp;billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2014/01/weodata/weorept.aspx?pr.x=54&pr.y=10&sy=2014&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=233&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC&grp=0&a=|title=World Economic Outlook Database |publisher=International Monetary Fund |date= April 2014|accessdate= 19 June 2014}}</ref>
| GDP_PPP_rank = 28
| GDP_PPP_year = 2014
| GDP_PPP_per_capita = $11,730<ref name=imf2/>
| GDP_PPP_per_capita_rank =
| GDP_nominal = $387.692&nbsp;billion<ref name=imf2/>
| GDP_nominal_rank = 30
| GDP_nominal_year = 2014
| GDP_nominal_per_capita = $8,125<ref name=imf2/>
| GDP_nominal_per_capita_rank =
| Gini_year = 2013
| Gini_change =  <!--increase/decrease/steady-->
| Gini = 53.9 <!--number only-->
| Gini_ref = <ref name="socio-economic policies">{{cite web |url= http://www.dane.gov.co/files/investigaciones/condiciones_vida/pobreza/bol_pobreza_13.pdf |title= socio-economic policies|publisher= dane.gov.co |accessdate= 22 March 2014}}</ref>
| Gini_rank =
| HDI_year = 2012
| HDI_change = increase
| HDI = 0.719 <!--number only-->
| HDI_ref = <ref>{{cite web|url= https://data.undp.org/dataset/Human-Development-Index-HDI-value/8ruz-shxu|title= Human Development Index (HDI) value |publisher= data.undp.org|accessdate= 2 April 2014}}</ref>
| HDI_rank = 91st
| HPI_year = 2006
| HPI = 67.2
| HPI_category = medium
| HPI_rank = 2nd
| currency = [[Colombian peso|Peso]]
| currency_code = COP
| country_code = CO
| time_zone = [[Time in Colombia|COT]]
| utc_offset = −5<sup>b</sup>
| time_zone_DST =
| utc_offset_DST =
| drives_on = right
| date_format = dd−mm−yyyy (CE)
| calling_code = [[+57]]
| cctld = [[.co]]
| footnote_a = Although the Colombian Constitution specifies Spanish (''Castellano'') as the official language in all its territory, other languages spoken in the country by ethnic groups (approximately 68 languages) are also official in their territories.<ref>Colombian Constitution of 1991 (Title I - Concerning Fundamental Principles - Article 10)</ref> English is also official in [[San Andrés, Providencia and Santa Catalina]] Islands.<ref>{{cite web|url= http://www.alcaldiabogota.gov.co/sisjur/normas/Norma1.jsp?i=2780|title = LEY 47 DE 1993 |publisher= alcaldiabogota.gov.co|language= Spanish | accessdate = 23 February 2014}}</ref>
| footnote_b = The official Colombian time <ref>{{cite web|url= http://horalegal.sic.gov.co/|title = The official Colombian time |publisher= horalegal.sic.gov.co|language= Spanish | accessdate = 23 February 2014}}</ref> is controlled and coordinated by the state agency [[Superintendency of Industry and Commerce]].<ref>{{cite web |url= http://www.alcaldiabogota.gov.co/sisjur/normas/Norma1.jsp?i=38168 |title= Decreto 2153 de 1992, articulo 20 |publisher = Presidencia de la República de Colombia |language = Spanish |accessdate= 9 October 2013}}</ref>
}}
