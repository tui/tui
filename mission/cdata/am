{{Infobox country
|conventional_long_name = Republic of Armenia
|native_name =  Հայաստանի Հանրապետություն<br />''Hayastani Hanrapetutyun''
|common_name = Armenia
|image_flag = Flag of Armenia.svg
|image_coat = Coat of arms of Armenia.svg
|image_map = Armenia (orthographic projection).svg
|image_map2 = Armenia - Location Map (2013) - ARM - UNOCHA.svg
|national_motto = 
|national_anthem = <br/>[[Mer Hayrenik|Մեր Հայրենիք]]<br/>''Mer Hayrenik''<br/>{{small|''Our Fatherland''}}<br/><center>[[File:Mer Hayrenik instrumental.ogg]]</center>
|official_languages = [[Armenian language|Armenian]]<ref name="constitution">{{cite web |title=The Constitution of the Republic of Armenia (with amendments) |url=http://www.concourt.am/english/constitutions/index.htm#1 |publisher=Constitutional Court of the Republic of Armenia |accessdate=14 October 2012 |date=5 July 1995}}</ref>
|ethnic_groups =
{{unbulleted list
| 97.9% [[Armenians|Armenian]]
|  1.3% [[Yazidi]]s
|  0.5% [[Russians|Russian]]
|  0.3% other
}}
|ethnic_groups_year = {{lower|0.4em|<ref>Asatryan, Garnik; Arakelova, Victoria (Yerevan 2002). [http://www.hra.am/file/minorities_en.pdf The Ethnic Minorities in Armenia]. Part of the [[Organization for Security and Co-operation in Europe|OSCE]].</ref>}}
|demonym = Armenian
|capital = {{Coat of arms|Yerevan}}
|latd=40 |latm=11 |latNS=N |longd=44 |longm=31 |longEW=E
|largest_city = capital
|government_type = [[Semi-presidential system|Semi-presidential]] [[republic]]<ref name="constitution"/>
|leader_title1 = [[President of Armenia|President]]
|leader_name1 = [[Serzh Sargsyan]]
|leader_title2 = [[Prime Minister of Armenia|Prime Minister]]
|leader_name2 = [[Hovik Abrahamyan]]
|legislature = [[National Assembly of Armenia|National Assembly]]
|sovereignty_type = Formation and independence
|established_event1 = Traditional date
|established_date1 = 2492 BC
|established_event2 = [[Nairi]]
|established_date2 = 1200&nbsp;BC
|established_event3 = [[Urartu|Kingdom of Ararat]]
|established_date3 = 840s BC
|established_event4 = [[Orontid Dynasty]]
|established_date4 = 560&nbsp;BC
|established_event5 = [[Kingdom of Armenia (Antiquity)|Kingdom of Armenia]]<br/>formed
|established_date5 = <br/>190&nbsp;BC
|established_event6 = {{nowrap|[[First Republic of Armenia|First Republic of<br/>Armenia]] established}}
|established_date6 = <br/>28 May 1918
|established_event7 = Independence<br/>from the [[Soviet Union]]<div style="float:right;">Declared<br/>Recognised<br/>Finalised</div>
|established_date7 = <br/><br/>23 August 1990<br/>21 September 1991<br/>21 December 1991
|area_km2 = 29,743
|area_sq_mi = 11,484 <!--Do not remove per [[WP:MOSNUM]]-->
|area_rank = 141st
|area_magnitude = 1 E10
|percent_water = 4.71<ref name=cia-fact>{{cite web |title=The World Fact Book – Armenia |url=https://www.cia.gov/library/publications/the-world-factbook/geos/am.html |publisher=Central Intelligence Agency |accessdate=2010-07-17 |archiveurl=//web.archive.org/web/20100719074837/https://www.cia.gov/library/publications/the-world-factbook/geos/am.html |archivedate=19 July 2010 <!--DASHBot-->|deadurl=no}}</ref>
|population_census = {{nowrap|{{increase}} 3,018,854<ref name="Armstat 2011 census">{{cite web |url=http://www.armstat.am/file/doc/99475033.pdf |title=Statistical Service of Armenia|publisher=Armstat |accessdate=20 February 2014}}</ref><ref name="News.am">{{cite web |url=http://news.am/eng/news/46702.html |title=News.am |work=World Economic Outlook Database, October 2009 |publisher=[[International Monetary Fund|IMF]] |accessdate=1 January 2011}}</ref><!--end nowrap:-->}}
|population_census_year = 2011
|population_census_rank = 134th
|population_density_km2 = 101.5
|population_density_sq_mi = 262.9 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 99th
|GDP_PPP_year = 2013
|GDP_PPP = {{nowrap|$20.831 billion<ref name=imf2>{{cite web |url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=49&pr.y=4&sy=2011&ey=2014&scsm=1&ssd=1&sort=country&ds=.&br=1&c=911&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a= |title=Armenia |publisher=International Monetary Fund |accessdate=2013-07-19}}</ref><!--end nowrap:-->}}
|GDP_PPP_per_capita = $6,128<ref name=imf2/>
|GDP_nominal = {{nowrap|$10.325 billion<ref name=imf2/>}}
|GDP_nominal_year = 2013
|GDP_nominal_per_capita = $3,037<ref name=imf2/>
|Gini_year = 2008
|Gini_change =  <!--increase/decrease/steady-->
|Gini = 30.9 <!--number only-->
|Gini_ref = <ref>{{cite web |url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html |title=Distribution of family income – Gini index |work=The World Factbook |publisher=CIA |accessdate=2009-09-01}}</ref>
|Gini_rank = 
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.729 <!--number only-->
|HDI_ref = <ref name="HDI">{{cite web|title=Human Development Report 2013 |year=2013 |publisher=United Nations |accessdate=21 May 2013|url=http://hdr.undp.org/en/mediacentre/humandevelopmentreportpresskits/2013report/}}</ref>
|HDI_rank = 87th
|currency = [[Armenian dram|Dram]] (դր.)
|currency_code = AMD
|time_zone = [[UTC]]
|utc_offset = +4
|patron_saint = {{nowrap|[[Gregory the Illuminator|St. Gregory the Illuminator]]}}
|drives_on = right
|calling_code = [[Telephone numbers in Armenia|+374]]
|cctld = [[.am]]
}}
