{{Infobox country
|conventional_long_name = 
{{collapsible list
|titlestyle = background:transparent;text-align:center;font-size:90%;
|title = {{big|Republic of Equatorial Guinea}}
|liststyle = margin-left:0.5em;text-align:left;font-weight:normal;
| {{nowrap|{{native name|es|{{smaller|República de Guinea Ecuatorial}}|fontsize=68%}}}}
| {{native name|fr|{{smaller|République de Guinée équatoriale}}|fontsize=68%}}
| {{native name|pt|{{smaller|República da Guiné Equatorial}}|fontsize=68%}}
}}
|common_name = Equatorial Guinea
|image_flag = Flag of Equatorial Guinea.svg
|image_coat = Coat of arms of Equatorial Guinea.svg
|symbol_type = Coat of arms
|image_map = Location Equatorial Guinea AU Africa.svg
|map_caption = {{map caption |countryprefix= |location_color=dark blue |region=Africa |region_color=dark grey |subregion=the [[African Union]] |subregion_color=light blue}}
|national_motto = {{unbulleted list |{{native name|es|"Unidad, Paz, Justicia"|italics=off|nolink=on}} |{{small|"Unity, Peace, Justice"}}}}
|national_anthem = {{native name|es|[[Caminemos pisando las sendas de nuestra inmensa felicidad]]|nolink=yes}}<br/>{{small|''Let us walk the paths of our immense happiness''}}
|official_languages = [[Spanish language|Spanish]]<br>[[French language|French]]<br>[[Portuguese Language|Portuguese]]
|languages_type = [[National language]]
|languages = [[Spanish Language|Spanish]]
|regional_languages = {{collapsible list |titlestyle=background:transparent;text-align:left;font-weight:normal;
|title = 5 languages<ref>{{cite web |title=World Directory of Minorities and Indigenous Peoples – Equatorial Guinea : Overview |publisher=[[United Nations High Commissioner for Refugees|UNHCR]] |date=20 May 2008 |url=http://www.unhcr.org/refworld/country,,MRGI,,GNQ,,4954ce2a2,0.html |accessdate=18 December 2012}}</ref><!--
--><ref>{{cite book |title=Africa 2012 |first=James Tyler |last=Dickovick |publisher=Stryker Post |year=2012|isbn= 1610488822 |page=180 |url=http://books.google.com/books?id=NdyYjQxd7uEC&pg=PA180 |accessdate=18 December 2012}}</ref>|[[Fang language|Fang]] |[[Bube language|Bube]] |[[Igbo language|Igbo]] |{{nowrap|[[West African Pidgin English|Pidgin English]]}} |[[Annobonese language|Annobonese]]}}
|ethnic_groups =
{{unbulleted list
| 85.7% [[Fang people|Fang]]
|  6.5% [[Bubi people|Bubi]]
|  3.6% [[Demographics of Equatorial Guinea#Peoples considered as natives|Ndowe]]
|  1.6% [[Annobon]]
|  1.1% [[Kwasio people|Bujeba {{small|(Kwasio)}}]]
|  1.4% others<sup>a</sup>
}}
|ethnic_groups_year = 1994<ref name=CIA>[https://www.cia.gov/library/publications/the-world-factbook/geos/ek.html Equatorial Guinea]. Cia World Factbook.</ref>
|demonym = {{unbulleted list |Equatoguinean |{{nowrap|Equatorial Guinean}}}}
|capital = [[Malabo]]<sup>b</sup>
|latd=3 |latm=45 |lats=7.43 |latNS=N |longd=8 |longm=47 |longs=5.13 |longEW=E
|largest_city = [[Bata, Equatorial Guinea|Bata]]
|government_type = [[Unitary state|Unitary]] [[Semi-presidential system|semi-presidential]] [[republic]]
|leader_title1 = [[List of Presidents of Equatorial Guinea|President]]
|leader_name1 = [[Teodoro Obiang Nguema Mbasogo]]
|leader_title2 = [[List of Prime Ministers of Equatorial Guinea|Prime Minister]]
|leader_name2 = [[Vicente Ehate Tomi]]
|legislature = [[Chamber of People's Representatives]]
|area_rank = 144th
|area_magnitude = 1 E10
|area_km2 = 28,050
|area_sq_mi = 10,830 <!--Do not remove per [[WP:MOSNUM]]-->
|percent_water = negligible
|population_estimate = 1,622,000 <ref name=unpop>[http://www.guineaecuatorialpress.com/estadistica.php GUINEA ECUATORIAL, Página Oficial del Gobierno de la República de Guinea Ecuatorial]. Evolución de la población de Guinea Ecuatorial</ref>
|population_estimate_rank = 150th
|population_estimate_year = 2012 <!--Do not remove http://data.worldbank.org/indicator/SP.POP.TOTL-->
|population_census = |population_census_year = 2010
|population_density_km2 = 24.1
|population_density_sq_mi = 62.4 <!--Do not remove per [[WP:MOSNUM]]-->
|population_density_rank = 187th
|GDP_PPP_year = 2010
|GDP_PPP = $19.286 billion<ref name=weo>{{cite web|title=World Economic Outlook Database, April 2013|url=http://www.imf.org/external/pubs/ft/weo/2013/01/weodata/weorept.aspx?pr.x=49&pr.y=9&sy=2010&ey=2013&scsm=1&ssd=1&sort=country&ds=.&br=1&c=642&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=|publisher=IMF|accessdate=21 July 2013}}</ref> 
|GDP_PPP_per_capita = $25,929<ref name=weo/>
|GDP_PPP_per_capita_rank = <!--Do not edit!-->
|GDP_nominal = $17.206 billion<ref name=weo/>
|GDP_nominal_year = 2012
|GDP_nominal_per_capita = $23,133<ref name=weo/>
|sovereignty_type = Independence
|established_event1 = from [[Spain]]
|established_date1 = 12 October 1968
|Gini_year = |Gini_change =  <!--increase/decrease/steady--> |Gini =  <!--number only--> |Gini_ref = |Gini_rank = 
|HDI_year = 2013
|HDI_change = increase <!--increase/decrease/steady-->
|HDI = 0.554 <!--number only-->
|HDI_ref = <ref name="UN">{{cite web |url=http://hdr.undp.org/en/media/HDR_2009_EN_Complete.pdf |format=PDF |title=Human Development Report 2009. Human development index trends: Table G |publisher=[[United Nations]] |accessdate=10 October 2009}}</ref>
|HDI_rank = 136th
|currency = [[Central African CFA franc]]
|currency_code = XAF
|country_code = 
|time_zone = [[West Africa Time|WAT]]
|utc_offset = +1
|time_zone_DST = not observed
|utc_offset_DST = +1
|drives_on = right
|calling_code = [[+240]]
|cctld = [[.gq]]
|footnote_a = {{nowrap|Including [[Equatoguinean Spanish]] (''Español ecuatoguineano'').}}
|footnote_b = [[Oyala]] (under construction).
}}
