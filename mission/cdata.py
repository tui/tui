#!/usr/bin/python
# -*- coding: utf-8 -*-

import european_union

visa = {}
# More values, like "formal" and really hard to get? Is it possible to get
visa[ ("cz", "tr") ] = None
visa[ ("cz", "ie") ] = None

def need_passport(c1, c2):
    if c1 in european_union.schengen_area and c2 in european_union.schengen_area:
        return 0
    return 1
    
def need_visa(c1, c2):
    if not need_passport(c1, c2):
        return []
    if not ((c1, c2) in visa):
        return ["check for visa"]
    else:
        if visa[ c1, c2 ]:
            return ["visa"]
    return []

