// rustc -C opt-level=3 debayer.rs

use std::fs::File;
use std::io::{Read, Write};
use std::env;

fn debayer_to_rgb(bayer: &[u8], width: usize, height: usize, n: usize, m: u32) -> Vec<u8> {
    let mut rgb = Vec::new();
    
    for y in (0..height).step_by(2 * n) {
        for x in (0..width).step_by(2 * n) {
	    if m == 0 {  	        // GRBG
	        let g1 = bayer[y * width + x];
            	let r = bayer[y * width + x + 1];
 		let b = bayer[(y + 1) * width + x];
 		let g2 = bayer[(y + 1) * width + x + 1];

		rgb.push(r);
                rgb.push((g1 as u16 / 2 + g2 as u16 / 2) as u8);
                rgb.push(b);
	    }
	
	    if m == 1 {  	        // RGGB
	        let r = bayer[y * width + x];
            	let g1 = bayer[y * width + x + 1];
 		let g2 = bayer[(y + 1) * width + x];
 		let b = bayer[(y + 1) * width + x + 1];

		rgb.push(r);
                rgb.push((g1 as u16 / 2 + g2 as u16 / 2) as u8);
                rgb.push(b);
	    }
        }
    }
    
    rgb
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 6 {
        eprintln!("Usage: {} <input file> <output file> <width> <height> <n>", args[0]);
        return;
    }

    let input_path = &args[1];
    let output_path = &args[2];
    let width: usize = args[3].parse().expect("Invalid width");
    let height: usize = args[4].parse().expect("Invalid height");
    let n: usize = args[5].parse().expect("Invalid n");

    let mut file = File::open(input_path).expect("Failed to open input file");
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).expect("Failed to read file");
    
    if buffer.len() < width * height {
        eprintln!("File size does not match given width and height.");
        return;
    }

    let rgb = debayer_to_rgb(&buffer, width, height, n, 0);
    
    let new_width = width / (2 * n);
    let new_height = height / (2 * n);
    
    let mut out_file = File::create(output_path).expect("Failed to create output file");
    write!(out_file, "P6\n{} {}\n255\n", new_width, new_height).expect("Failed to write PPM header");
    out_file.write_all(&rgb).expect("Failed to write output file");
    
    println!("Conversion complete. Output saved to {}", output_path);
}
