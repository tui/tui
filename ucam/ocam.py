#!/usr/bin/env python3
"""
# Obscura camera

Test scripts for libobscura.

Copyright 2025 Pavel Machek, GPLv3
"""

import os
import time
import grw2dng
import subprocess
import glob
import shutil

class grwBase:
    def init(m, dir):
        m.dir = dir
        m.slen = len(m.suffix)
        m.start_time = 0
        m.scan()
        print("Movie", len(m.list))

    def scan(m):
        m.list = os.listdir(m.dir)
        m.list.sort(reverse=True) # FIXME -- this reverses the list
        m.length = len(m.list)

    def get_path(m):
        s = m.get_name()
        if s: return m.dir + s
        return s

    def get_name(m):
        m.scan()
        #print("Get path -- ")
        while True:
            if (len(m.list)) == 0:
                return None
            #print("Get path: ", m.list[0], m.suffix)
            if m.list[0][-m.slen:] != m.suffix:
                m.pop()
                continue
            return m.list[0]

    def get_time(m):
        s = m.get_name()
        s = s[:-m.slen]
        t = int(s)
        res = t * 1000 - m.start_time
        t = t / (1000*1000.)
        while (time.time() - t < 1):
            print("Too fast: ", time.time(), t, file=sys.stderr)
            print("Message: WA")
            sys.stdout.flush()
            time.sleep(.1)
        return res

    def pop(m):
        m.list = m.list[1:]

    def progress(m):
        i = len(m.list)
        print("Message: %d" % i)
        sys.stdout.flush()

class SVBayer(grwBase):
    suffix = ".1052x780-GRBG.sv"
    def __init__(m, dir):
        m.init(dir)

class Obscura:
    path = "/home/purism/g/libobscura/"
    rundir = "/tmp/delme.sm/"
    resdir = "/tmp/delme.out/"

    def __init__(m):
        m.grw2dng = grw2dng.Grw2Dng()

    def cleanup(m):
        for file in glob.glob(m.rundir + "*.sv"):
            if False:
                os.remove(file)

    def maybe_unlink(m, p):
        if os.path.exists(p) or os.path.islink(p):
            os.unlink(p)

    def debayer_good(m, base):
        # FIXME: Doing preview like this is not great. When the image is dark,
        # dcraw adjusts brightness anyway, so you get bright image with a lot
        # of noise on preview. Ungood.
        print("sv->dng")
        time.sleep(.05) # FIXME Wait for full file, hack!
        m.grw2dng.run(base+".sv", base+".dng")
        m.cleanup()
        print("dng->ppm")
        subprocess.run(['dcraw',
                        '-w',      # -w Use camera white balance
                        '+M',      # +M use embedded color matrix
                        '-H', '2', # -H 2 Recover highlights by blending them
                        '-o', '1', # -o 1 Output in sRGB colorspace
                        '-q', '0', # -q 0 Debayer with fast bi-linear interpolation
                        '-f',      # -f Interpolate RGGB as four colors
                        base+".dng"])
#        print("tiff->jpeg")
#        subprocess.run(['convert', base+'.tiff', base+'.jpeg'])

    def debayer_fast(m, base):
        print("sv->ppm")
        time.sleep(.05) # FIXME Wait for full file, hack!
        subprocess.run(['./debayer', base+".sv", base+".ppm", '1052', '780', '1'])
    
    def process(m):
        b = SVBayer(m.rundir)
        grw = b.get_name()
        if not grw:
            time.sleep(.1)
            return
        print(grw)
        base = m.rundir+grw[:-3]
        t1 = time.time()
        m.debayer_fast(base)
        m.maybe_unlink(m.rundir+"cur.jpeg")
        m.maybe_unlink(m.rundir+"cur.ppm")
        os.symlink(base+'.ppm', m.rundir+"cur.ppm")
        #m.maybe_unlink(base+".sv")
        print("ok ", time.time() -t1)
        photo = glob.glob(m.rundir+"*.photo.mark")
        if photo:
            os.remove(photo[0])
            print("Taking photo %s... \a" % base)
            m.maybe_unlink(m.rundir+"cur.ppm")
            m.maybe_unlink(m.rundir+"cur.jpeg")
            for f in glob.glob(base+".*"):
                shutil.move(f, m.resdir)

    def run(m):
        subprocess.run(['killall', 'obscura_getfram'])
        if not os.path.exists(m.rundir):
            os.mkdir(m.rundir)
        if not os.path.exists(m.resdir):
            os.mkdir(m.resdir)

        m.ucam = subprocess.Popen(["./ucam.py"], stdin=subprocess.DEVNULL, stdout=None, stderr=None)
        m.obscura = subprocess.Popen([m.path + "target/debug/obscura_getframes", "imx7-csi:imx-media:776794edba9cf34e:s5k3l6xx 3-002d"],
                                     env={**os.environ, "LIBOBSCURA_DEVICES_DIR": "crates/vidi/config/"},
                                     stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=None, cwd = m.path)
        while True:
            if not m.ucam.poll() is None:
                break
            if not m.obscura.poll() is None:
                break
            m.process()
        print("Terminating children")
        m.cleanup()
        m.ucam.terminate()
        m.obscura.terminate()
        m.ucam.wait()
        m.obscura.wait()
        
o = Obscura()
o.run()
