#!/usr/bin/env python3
# Thanks to https://brettviren.github.io/pygst-tutorial-org/pygst-tutorial.html#sec-9
# Needs to be ran as GDK_SYNCHRONIZE=1 GDK_BACKEND=x11 ./cam.py

import sys, os, time
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GObject, Gtk, GLib, GdkPixbuf, Gdk
import controls

class UnicsyCamera:
    rundir = "/tmp/delme.sm"

    def timestamp(m):
        return "%d" % (time.time() * 1000000)

    def create_mark(m, name):
        with open(m.rundir + "/" + m.timestamp() + "." + name, 'w') as fp:
            pass

class MainWin(UnicsyCamera):
    do_bench = False
    def __init__(m):
        window = Gtk.Window(type = Gtk.WindowType.TOPLEVEL)
        window.set_title("Unicsy Camera")
        window.set_decorated(False)
        # PinePhone is 720x1440, scaled to 2x.
        window.set_default_size(360, 680)
        window.connect("destroy", Gtk.main_quit, "WM destroy")
        vbox = Gtk.VBox()
        window.add(vbox)

        m.status = Gtk.Label(label = "Status...")
        vbox.add(m.status)
        
        m.movie_window = Gtk.Image()
        m.movie_window.set_hexpand(False)
        m.movie_window.set_vexpand(False)
        m.movie_window.set_size_request(160, 120)

        # Create an EventBox to capture clicks
        event_box = Gtk.EventBox()
        event_box.set_events(Gdk.EventMask.ALL_EVENTS_MASK)
        event_box.add(m.movie_window)
        vbox.add(event_box)
        #vbox.add(m.movie_window)

        # Connect touch or click event
        event_box.connect("motion-notify-event", m.on_image_clicked)
        
        hbox = Gtk.HBox()
        vbox.pack_start(hbox, False, False, 0)
        hbox.set_border_width(10)

        m.button2 = Gtk.Button(label = "Quit")
        m.button2.connect("clicked", m.exit)
        hbox.pack_start(m.button2, False, False, 0)
        m.button3 = Gtk.Button(label = "...")
        m.button3.connect("clicked", m.test)
        hbox.pack_start(m.button3, False, False, 0)

        m.button4 = Gtk.Button(label = "Wow")
        m.button4.connect("clicked", lambda x: m.create_mark("wow.mark"))
        hbox.pack_start(m.button4, False, False, 0)
        m.button5 = Gtk.Button(label = "Photo")
        m.button5.connect("clicked", lambda x: m.create_mark("photo.mark"))
        hbox.pack_start(m.button5, False, False, 0)
        m.b_video = Gtk.Button(label = "Video")
        m.b_video.connect("clicked", m.video)
        hbox.pack_start(m.b_video, False, False, 0)

        m.label = Gtk.Label(label = "Starting up...")
        hbox.pack_start(m.label, False, False, 0)
        
        hbox.add(Gtk.Label())
        window.show_all()

        m.is_recording = None
        m.count = 0
        GLib.idle_add(lambda: m.idle())
        m.label.set_text("ok")

        m.controls = controls.Librem5()
        #m.controls = controls.Controls()

    def on_image_clicked(m, widget, event):
        xlim = 360
        ylim = 640 # Screen is 720 but we don't have whole screen
        x = event.x
        y = event.y / ylim
        #print(f"Image touched at ({x}, {y})")
        if x < xlim/3:
            m.controls.set_exposure(y * 1/3)
            return
        if x > 2*xlim/3:
            m.controls.set_focus(25*y)
            return
        m.controls.set_gain(48*y)

    def display(m, path):
        #print("Displaying:", path, "@", time.time())
        m.movie_window.set_from_file(path)
        pixbuf = m.movie_window.get_pixbuf()
        if pixbuf:
            pixbuf = pixbuf.rotate_simple(270)
            pixbuf = pixbuf.scale_simple(357, 560, 0)
            m.movie_window.set_from_pixbuf(pixbuf)
        else:
            m.label.set_text("no preview?")

    def display_raw(m, path):
        pixels = open(path, 'rb').read()
        depth = 8
        width = 640
        height = 480
        stride = width * 3
        pixbuf = GdkPixbuf.Pixbuf.new_from_data(pixels, GdkPixbuf.Colorspace.RGB,
                                                False, depth, width, height, stride)
        pixbuf = pixbuf.rotate_simple(270)
        pixbuf = pixbuf.scale_simple(357, 560, 0)        
        m.movie_window.set_from_pixbuf(pixbuf)

    def video(m, _):
        if not m.is_recording:
            m.create_mark("start.mark")
            m.is_recording = time.time()
        else:
            m.create_mark("stop.mark")
            m.is_recording = None

    def test(m, _):
        for i in range(1000):
            m.display(m.rundir+"/image-%06d.jpeg" % i)

    def exit(m, widget, data=None):
        Gtk.main_quit()

    def update_buttons(m):
        if not m.is_recording:
            m.b_video.set_label("Video")
        else:
            t = time.time() - m.is_recording
            m.b_video.set_label("%d:%02d" % (t/60, t%60))
        
    def idle(m):
        if m.do_bench:
            m.idle_benchmark()
            return
        m.display(m.rundir+"/cur.ppm")
        if not m.count % 2:
            m.update_buttons()
        m.count += 1
        m.status.set_text(m.controls.get_summary())
        GLib.idle_add(lambda: m.idle())

    def idle_benchmark(m):
        m.display(m.rundir+"/image%06d.jpeg" % m.count)
        m.count += 1
        if m.count == 200:
            # On x220: 640x480 jpeg, 2.06 sec (scale before rotate)
            # On x220, 640x480 raw, 1.28 sec (scale before rotate)
            # On x220, 640x480 raw, 0.95 sec
            Gtk.main_quit()
        GLib.idle_add(lambda: m.idle())

w = MainWin()
#GObject.threads_init()
Gtk.main()

