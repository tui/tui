#!/usr/bin/python3

import os
import shutil
import time

class Rec:
    dir = "/tmp/delme.sm"
    
    def init(m):
        os.system("mkdir "+m.dir)

    def cploop(m):
        while True:
            if os.path.exists("cur_fresh.jpeg"):
                n = "%d.jpeg.sv" % (int(time.time() * 1000000))
                os.rename("cur_fresh.jpeg", n)
                os.symlink(n, "cur_new.jpeg")
                os.rename("cur_new.jpeg", "cur.jpeg")
                time.sleep(0.01)

    def run(m):
        r.init()
        os.chdir(m.dir)
        s = "gst-launch-1.0 -v "
        s += "droidcamsrc mode=2 camera-device=0 "
        # works: camera-device=0 back camera
        #        camera-device=1 front camera
        # camera-device=1 4608,height=3456
        # camera-device=2 2592, height=(int)1940
	# video-torch=1 mode=2 
	# works: mode=2, !mode (1920, 1080, 30)
	# works: (640, 480, 30)
        # works: (1024, 768, 30)
	# ?: (1920, 1080, 30)
	# works: (1920, 1440, 30)
	# does not: (2560, 1440, 30)
        s += "\! video/x-raw,width=%d,height=%d,format=NV21,framerate=%d/1 " % (1920, 1080, 30)
        s += "\! jpegenc"
        #s += "\! image/jpeg,width=4608,height=3456,framerate=30/1 "
        #s += "\! image/jpeg,width=1920,height=1080,framerate=30/1 "
        #s += "\! fakesink"        
        s += "\! multifilesink location=cur_fresh.jpeg"
        os.system(s + " &")
        m.cploop()

r = Rec()
r.run()

