#include <gst/gst.h>
#include <fcntl.h>
#include <cstdio>
#include <unistd.h>

/* Thanks to https://stackoverflow.com/questions/29205920/push-images-into-gstreamer-pipeline */

/*

   build/src/cam/cam  -c /base/i2c-csi/rear-camera@4c -spixelformat=JPEG,width=2592,height=1944 --sdl -C300 -F/tmp/delme.#

 -> delme.cam0-stream0-000007
*/
static GMainLoop *loop;

#define SIZE (1920*1080)
static guchar data[SIZE];
static int count = 0;

static void
cb_need_data (GstElement *appsrc,
	      guint       unused_size,
	      gpointer    user_data)
{
  static GstClockTime timestamp = 0;
  GstBuffer *buffer;
  GstFlowReturn ret;
  char name[1024];

  (void) unused_size;
  (void) user_data;

  sprintf(name, "/tmp/delme.cam0-stream0-%06d", count);
  count++;

  {
    int fd = open(name, O_RDONLY);
    if (fd < 0) {
      printf("%s does not exist\n", name);
      g_main_loop_quit (loop);
    } else
      printf("Reading %s\n", name);
    read(fd, data, SIZE);
    close(fd);
  }

  buffer = gst_buffer_new_memdup (data, SIZE);
  GST_BUFFER_PTS (buffer) = timestamp;
  GST_BUFFER_DURATION (buffer) = gst_util_uint64_scale_int (1, GST_SECOND, 30);

  timestamp += GST_BUFFER_DURATION (buffer);

  g_signal_emit_by_name (appsrc, "push-buffer", buffer, &ret);
  gst_buffer_unref(buffer);

  if (ret != GST_FLOW_OK) {
    printf("Something wrong pushing flow\n");
    /* something wrong, stop pushing */
    g_main_loop_quit (loop);
  }
}

gint
main (gint   argc,
      gchar *argv[])
{
  GstElement *pipeline, *appsrc, *conv, *videosink;
  gboolean res;
  gboolean preview = FALSE;

  /* init GStreamer */
  gst_init (&argc, &argv);
  loop = g_main_loop_new (NULL, FALSE);

  /* setup pipeline */
  pipeline = gst_pipeline_new ("pipeline");
  appsrc = gst_element_factory_make ("appsrc", "source");
  if (preview) {
    conv = gst_element_factory_make ("jpegdec", "conv");
    videosink = gst_element_factory_make ("autovideosink", "videosink");
  } else {
    conv = gst_element_factory_make ("matroskamux", "conv");
    videosink = gst_element_factory_make ("filesink", "videosink");
    // location= sync=
    g_object_set (G_OBJECT (videosink), "location", "/tmp/delme.mkv", NULL);
  }
  
  /* setup */
  g_object_set (G_OBJECT (appsrc), "caps",
		gst_caps_new_simple ("image/jpeg",
				     "width", G_TYPE_INT, 1920,
				     "height", G_TYPE_INT, 1080,
				     "framerate", GST_TYPE_FRACTION, 30, 1,
				     NULL), NULL);
  res = gst_bin_add (GST_BIN (pipeline), appsrc);
  if (!res) printf("Could not add\n");
  res = gst_bin_add (GST_BIN (pipeline), conv);
  if (!res) printf("Could not add\n");
  res = gst_bin_add (GST_BIN (pipeline), videosink);
  if (!res) printf("Could not add\n");
  res = gst_element_link_many (appsrc, conv, videosink, NULL);
  if (!res) printf("Could not link\n");
  
  /* setup appsrc */
  g_object_set (G_OBJECT (appsrc),
		"stream-type", 0,
		"format", GST_FORMAT_TIME, NULL);
  g_signal_connect (appsrc, "need-data", G_CALLBACK (cb_need_data), NULL);

  /* play */
  GstStateChangeReturn ns;
  ns = gst_element_set_state (pipeline, GST_STATE_PLAYING);
  if (ns == GST_STATE_CHANGE_FAILURE) {
    printf("Can't start pipeline\n");
    return 1;
  }
  g_main_loop_run (loop);

  /* clean up */
  ns = gst_element_set_state (pipeline, GST_STATE_NULL);
  if (ns == GST_STATE_CHANGE_FAILURE) {
    printf("Can't stop pipeline\n");
    return 1;
  }
  gst_object_unref (GST_OBJECT (pipeline));
  g_main_loop_unref (loop);

  return 0;
}

