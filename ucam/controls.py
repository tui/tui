#!/usr/bin/python3
import os
import fcntl
import struct
import glob

def get_stable_subdev_names():
    devices = {}
    for dev in glob.glob("/dev/v4l-subdev*"):
        realpath = os.path.realpath(dev)  # Resolve symlinks
        with open(f"/sys/class/video4linux/{os.path.basename(realpath)}/name") as f:
            name = f.read().strip()
        devices[name] = dev
    return devices

devices = get_stable_subdev_names()
print(devices)

class Controls:
    # V4L2 IOCTL codes (from Linux headers)
    VIDIOC_G_CTRL = 0xC008561B
    VIDIOC_S_CTRL = 0xC008561C

    CID_BRIGHTNESS = 0x00980900  # V4L2_CID_BRIGHTNESS
    CID_EXPOSURE = 0x00980911    # 2 max 3118
    CID_AGAIN = 0x009e0903       # 32 max 512
    CID_DGAIN = 0x009f0905       # 256 max 768
    CID_FOCUS = 0x009a090a	 # 0 max 1023 inf 250?
    
    def get_v4l2_control(m, device, control_id):
        fd = os.open(device, os.O_RDWR)
        # Prepare the request structure (control_id, value placeholder)
        request = struct.pack("Ii", control_id, 0)
        response = fcntl.ioctl(fd, m.VIDIOC_G_CTRL, request)
        _, value = struct.unpack("Ii", response)
        os.close(fd)
        return value

    def set_v4l2_control(m, device, control_id, value):
        fd = os.open(device, os.O_RDWR)
        # Pack the control ID and new value into the request structure
        request = struct.pack("Ii", control_id, int(value))
        fcntl.ioctl(fd, m.VIDIOC_S_CTRL, request)
        os.close(fd)

    def get_summary(m):
        return "(not available)"
        
class ADGain(Controls): # For machines with separate AGAIN and DGAIN
    def set_gain(m, val): # 1 .. ISO100
        maxa = m.AGAIN_MAX/m.AGAIN_MIN
        if val < maxa:
            m.set_v4l2_control(m.sensor, m.CID_DGAIN, m.DGAIN_MIN)
            m.set_v4l2_control(m.sensor, m.CID_AGAIN, int(val * m.AGAIN_MIN))
        else:
            val /= maxa
            m.set_v4l2_control(m.sensor, m.CID_DGAIN, int(val * m.DGAIN_MIN))
            m.set_v4l2_control(m.sensor, m.CID_AGAIN, m.AGAIN_MAX)

    def get_gain(m):
        r = 1.0
        r = r * m.get_v4l2_control(m.sensor, m.CID_AGAIN) / m.AGAIN_MIN
        r = r * m.get_v4l2_control(m.sensor, m.CID_DGAIN) / m.DGAIN_MIN
        return r

    def get_summary(m):
        e = m.get_exposure()
        g = m.get_gain()
        f = m.get_focus()
        if f < 0.001: f = 0.001
        return "1/%.0f iso%.0f %.2fm" % (1/e, g*100, 1/f)
    
class Librem5(ADGain):
    def __init__(m):
        global devices
        m.sensor = devices['s5k3l6xx 3-002d']
        m.focus = devices['dw9714 3-000c']

    def set_exposure(m, val): # in seconds
        m.set_v4l2_control(m.sensor, m.CID_EXPOSURE, int(val / 0.0001))

    def get_exposure(m):
        return 0.0001 * m.get_v4l2_control(m.sensor, m.CID_EXPOSURE)
        #return 0.001

    EXPOSURE_MAX = 3118
    DGAIN_MIN = 256
    DGAIN_MAX = 768
    AGAIN_MIN = 32
    AGAIN_MAX = 512
    FOCUS_MIN = 0
    FOCUS_MAX = 1023
    FOCUS_INF = 250
    FOCUS_STEP = 30

    def get_focus(m): # in diopters, aka 1/m
        v = m.get_v4l2_control(m.focus, m.CID_FOCUS)
        v -= m.FOCUS_INF
        v /= m.FOCUS_STEP
        return v

    def set_focus(m, val):
        # Guesstimate minimal focus is 4cm aka 25 diopters
        v = m.FOCUS_INF + val * m.FOCUS_STEP
        #print("Focus:", v)
        m.set_v4l2_control(m.focus, m.CID_FOCUS, v)

if __name__ == "__main__":
    m = Librem5()
    print(m.get_summary())

    print("ISO 100")
    m.set_gain(1)
    print(m.get_summary())

    print("ISO 1600")
    m.set_gain(16)
    print(m.get_summary())

    print("ISO 4800")
    m.set_gain(48)
    print(m.get_summary())

    print("Focus inf")
    m.set_focus(0)
    print(m.get_summary())

    print("Focus 25di")
    m.set_focus(25)
    print(m.get_summary())

    print("1/30")
    m.set_exposure(1/30)
    print(m.get_summary())

    print("1/125")
    m.set_exposure(1/125)
    print(m.get_summary())

    print("1/1000")
    m.set_exposure(1/1000)
    print(m.get_summary())

    print("Reset")
    m.set_exposure(1/30)
    m.set_gain(48)
    m.set_focus(0)
    print(m.get_summary())
