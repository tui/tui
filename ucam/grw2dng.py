#!/usr/bin/env python3                                                          
#                                                                               
# Test scripts for libobscura.                                                  
# pip3 install pidng

from pidng.core import RAW2DNG, DNGTags, Tag
from pidng.defs import *
import numpy as np
import struct

class Grw2Dng:
    def run(m, grw, dng):
        # image specs
        width = 1052
        height = 780
        bpp= 8

        # load raw data into 16-bit numpy array.
        numPixels = width*height
        rawFile = grw
        rf = open(rawFile, mode='rb')
        if bpp > 8:
            rawData = struct.unpack("H"*numPixels,rf.read(2*numPixels))
            rawFlatImage = np.zeros(numPixels, dtype=np.uint16)
        else:
            rawData = struct.unpack("B"*numPixels,rf.read(numPixels))
            rawFlatImage = np.zeros(numPixels, dtype=np.uint16)

        rawFlatImage[:] = rawData[:] 
        rawImage = np.reshape(rawFlatImage,(height,width))
        if bpp > 8:
            rawImage = rawImage >> (16 - bpp)

        # uncalibrated color matrix, just for demo. 
        ccm1 = [[19549, 10000], [-7877, 10000], [-2582, 10000],	
                [-5724, 10000], [10121, 10000], [1917, 10000],
                [-1267, 10000], [ -110, 10000], [ 6621, 10000]]

        # set DNG tags.
        t = DNGTags()
        t.set(Tag.ImageWidth, width)
        t.set(Tag.ImageLength, height)
        t.set(Tag.TileWidth, width)
        t.set(Tag.TileLength, height)
        t.set(Tag.Orientation, Orientation.Horizontal)
        t.set(Tag.PhotometricInterpretation, PhotometricInterpretation.Color_Filter_Array)
        t.set(Tag.SamplesPerPixel, 1)
        t.set(Tag.BitsPerSample, bpp)
        t.set(Tag.CFARepeatPatternDim, [2,2])
        t.set(Tag.CFAPattern, CFAPattern.GRBG)
        t.set(Tag.BlackLevel, (4096 >> (16 - bpp)))
        t.set(Tag.WhiteLevel, ((1 << bpp) -1) )
        t.set(Tag.ColorMatrix1, ccm1)
        t.set(Tag.CalibrationIlluminant1, CalibrationIlluminant.D65)
        t.set(Tag.AsShotNeutral, [[1,1],[1,1],[1,1]])
        t.set(Tag.BaselineExposure, [[-150,100]])
        t.set(Tag.Make, "Camera Brand")
        t.set(Tag.Model, "Camera Model")
        t.set(Tag.DNGVersion, DNGVersion.V1_4)
        t.set(Tag.DNGBackwardVersion, DNGVersion.V1_2)
        t.set(Tag.PreviewColorSpace, PreviewColorSpace.sRGB)

        # save to dng file.
        r = RAW2DNG()
        # 0.84sec with compression, 0.77sec without, with "sleeping" l5
        r.options(t, path="", compress=False)
        r.convert(rawImage, filename=dng)
