#!/usr/bin/python3

import sys

def hex_dump(file_path):
    try:
        with open(file_path, 'rb') as file:
            byte = file.read(1)
            while byte:
                hex_value = format(ord(byte), '02x')
                print(hex_value)
                byte = file.read(1)
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")

print('mm.b 0x88800000')
hex_dump(sys.argv[1])

