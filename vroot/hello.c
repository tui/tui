/* -*- linux-c -*- */

#include <stdint.h>

static char stack[4096];

void sbi_first(void)
{
	asm volatile (
		"mv sp, %0\n\t"
		"j main\n\t"
		:: "r"(stack+4096):);
}

/*
Extension: Console Putchar (EID #0x01)
long sbi_console_putchar(int ch)
Write data present in ch to debug console.
*/

void sbi_call(unsigned long eid, unsigned long fid, unsigned long a0) {
	asm volatile (
		"mv a7, %0\n\t"    // Move the address of the string to register a0
		"mv a6, %1\n\t"      // SBI console print function code
		"mv a0, %2\n\n"
		"ecall\n\t"
		:
		: "r"(eid), "r"(fid), "r"(a0)
		: "a0", "a6", "a7"
		);
}

int putchar(int c)
{
	sbi_call(0x01, c, c);
	return 0;
}

int puts(const char *s)
{
	while (*s) {
		putchar(*s++);
	}
	return 0;
}

void poweroff(void)
{
	int *syscon = 0x100000;

	*syscon = 0x5555; // 0x7777 for reboot
	puts("Poweroff failed?\n");
	while(1);
}

typedef volatile struct __attribute__((packed)) {
	uint32_t MagicValue;
	uint32_t Version;
	uint32_t DeviceID;
	uint32_t VendorID;
	uint32_t DeviceFeatures;
	uint32_t DeviceFeaturesSel;
	uint32_t _reserved0[2];
	uint32_t DriverFeatures;
	uint32_t DriverFeaturesSel;
	uint32_t _reserved1[2];
	uint32_t QueueSel;
	uint32_t QueueNumMax;
	uint32_t QueueNum;
	uint32_t _reserved2[2];
	uint32_t QueueReady;
	uint32_t _reserved3[2];
	uint32_t QueueNotify;
	uint32_t _reserved4[3];
	uint32_t InterruptStatus;
	uint32_t InterruptACK;
	uint32_t _reserved5[2];
	uint32_t Status;
	uint32_t _reserved6[3];
	uint32_t QueueDescLow;
	uint32_t QueueDescHigh;
	uint32_t _reserved7[2];
	uint32_t QueueAvailLow;
	uint32_t QueueAvailHigh;
	uint32_t _reserved8[2];
	uint32_t QueueUsedLow;
	uint32_t QueueUsedHigh;
	uint32_t _reserved9[21];
	uint32_t ConfigGeneration;
	uint32_t Config[0];
} virtio_regs;

void dump_virtio()
{
}

#define VIRTIO_MAGIC   0x74726976
#define VIRTIO_VERSION 0x2
#define VIRTIO_DEV_NET 0x1
#define VIRTIO_DEV_BLK 0x2
#define wrap(x, len)   ((x) & ~(len))

#define READ32(a) (a)

static int virtio_dev_init(uint32_t virt, uint32_t intid)
{
	virtio_regs *regs = (virtio_regs *) virt;

	puts("Virtio device\n");

	if (READ32(regs->MagicValue) != VIRTIO_MAGIC) {
		puts("error: virtio at 0x%x had wrong magic value 0x%x, expected 0x%x\n");
		//virt, regs->MagicValue, VIRTIO_MAGIC);
		return -1;
	}
	if (READ32(regs->Version) != VIRTIO_VERSION) {
		puts("error: virtio at 0x%x had wrong version 0x%x, expected 0x%x\n");
		//virt, regs->Version, VIRTIO_VERSION);
		return -1;
	}
	return -1;       
	if (READ32(regs->DeviceID) == VIRTIO_DEV_NET) {
		puts("type: net\n");
	}
	if (READ32(regs->DeviceID) == VIRTIO_DEV_BLK) {
		puts("type: block\n");
	}
	
	if (READ32(regs->DeviceID) == 0) {
		/*On QEMU, this is pretty common, don't print a message */
		/*printf("warn: virtio at 0x%x has DeviceID=0, skipping\n", virt);*/
		return -1;
	}

	/* ... to be continued ... */
}

void virtio_init(void)
{
	/* TODO: we know these addresses due to manually reading device tree,
	 * but we should automate that */
	void *page_virt = 0x10001000;
	
	for (int i = 0; i < 8; i++)
		virtio_dev_init(page_virt + 0x1000 * i, i + 1);
}

void main(void)
{
	putchar('H');
	puts("ello world\r\n");
	virtio_init();
	poweroff();
}

