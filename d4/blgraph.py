#!/usr/bin/python

import math

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable
import matplotlib.pyplot as plt

# Tex support is at
# http://matplotlib.org/users/usetex.html

class Painter:
    def __init__(m):
        pass

    def d4_to_n900_p3(m, val):
        val -= 50.
        val = val/28.
        return pow(val, 3)

    def d4_to_n900_p4(m, val):
        val -= 50.
        val = val/40.
        return pow(val, 4)
    
    def d4_to_n900_e1(m, val):
        return pow(2, val /23.) / 3

    def d4_to_n900(m, val):
        return pow(2, (val-50) /20.)

    def n900_to_d4(m, val):
        r = math.log(val) / math.log(2)
        r = r*20 + 50
        return r

    def paint(m, ax):
        ax.axis([0, 255, 1, 255])

        plt.ylabel('N900')
        plt.xlabel('D4')
        
        ax.set_title("backlight")
        ax.titlesize = 'small'
        ax.title.set_visible(True)
        ax.grid(True)

        #ax.set_yscale('log')

        plotx = [ 50, 70, 100, 120,  130 ,  140 ,  150 ,  180 ,  190 ,  200 ,  210 ,  220 ]
        ploty = [  1,  2,   6,   8,   20 ,   30 ,   40 ,  100 ,  128 ,  150 ,  200 ,  255 ]
        lines = ax.plot(plotx, ploty, 'bo', ms=4, lw=1, ls='-')

        ix = []
        iy = []
        for i in range(50, 255):
            ix += [ i ]
            iy += [ m.d4_to_n900(i) ]
        lines = ax.plot(ix, iy, 'ro', ms=1, lw=1, ls='-')
            
        ix = []
        iy = []
        for i in range(1, 255):
            ix += [ m.n900_to_d4(i) ]
            iy += [ i ]
        lines = ax.plot(ix, iy, 'go', ms=1, lw=1, ls='-')
            
    def run(m):
        plt.rcParams.update({'axes.labelsize': 'small'})                
        plt.rcParams.update({'axes.titlesize': 'small'})        
        plt.figure(2)
        ax = plt.subplot(1, 1, 1)
        m.paint(ax)
        plt.savefig("foo.png")
        
        plt.show()

p = Painter()
p.run()
    

