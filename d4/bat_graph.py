#!/usr/bin/python2

from __future__ import print_function

import math
import sys
sys.path += [ "../gtracks" ]
import egt

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable
import matplotlib.pyplot as plt

# Tex support is at
# http://matplotlib.org/users/usetex.html

class Painter:
    def __init__(m):
        pass

    def paint(m, ax1, ax2, ax3):
        #plt.ylabel('voltage/%/current')
        plt.xlabel('time')

        ax = (ax1, ax2, ax3)
        
        ax1.set_title("voltage")
        ax2.set_title("%")
        ax3.set_title("current")

        for a in ax:
            a.grid(True)
            a.title.set_visible(True)
        
        ax1.axis([0, m.t, 3.0, 4.4])
        ax2.axis([0, m.t, 0, 120.0])
        ax3.axis([0, m.t, -800, +300])

        #ax1.set_yscale('log')

        ix = []
        iv1 = []
        iv2 = []
        ip1 = []
        ip2 = []
        ic1 = []
        ic2 = []        
        ic3 = []
                      
        for s in m.data:
            t, v1, v2, p1, p2, c1, c2, c3 = s
            t = (t-m.t1) / 3600.
            ix += [ t ]
            iv1 += [ v1 ]
            iv2 += [ v2 ]
            ip1 += [ p1 ]
            ip2 += [ p2 ]
            ic1 += [ c1 ]
            ic2 += [ c2 ]
            ic3 += [ c3 ]

        lines = ax1.plot(ix, iv1, 'go', ms=1, lw=1, ls='-')
        lines = ax1.plot(ix, iv2, 'bo', ms=1, lw=1, ls='-')

        lines = ax2.plot(ix, ip1, 'go', ms=1, lw=1, ls='-')
        lines = ax2.plot(ix, ip2, 'bo', ms=1, lw=1, ls='-')

        lines = ax3.plot(ix, ic1, 'ro', ms=1, lw=1, ls='-')
        lines = ax3.plot(ix, ic2, 'go', ms=1, lw=1, ls='-')
        lines = ax3.plot(ix, ic3, 'bo', ms=1, lw=1, ls='-')
            
    def run(m):
        plt.rcParams.update({'axes.labelsize': 'small'})                
        plt.rcParams.update({'axes.titlesize': 'small'})        
        plt.figure(2)
        ax1 = plt.subplot(3, 1, 1)
        ax2 = plt.subplot(3, 1, 2)
        ax3 = plt.subplot(3, 1, 3)
        m.paint(ax1, ax2, ax3)
        plt.savefig("foo.png")
        
        plt.show()

    def read_log(m, path):
        t1 = 0
        m.data = []
        #path = "bat_1524564800.log"
        #path = "bat_1524552292.log"
        #path = "bat_n900.log"
        for l in open(path, "r").readlines():
            l = l[1:-2]
            s = l.split(",")
            s = map(float, s)
            m.data += [ (s) + (0, 0) ]
            t, v1, v2, p1, p2, c = s
            if t1 == 0: t1 = t
        m.t1 = t1
        m.t = (t-t1)/3600.
        print(m.t, " hours of recording")

    def read_egt(m, path):
        t1 = 0
        m.data = []
        path = "bat_1528051121.egt"
        for l in open(path, "r").readlines():
            p = egt.parse_string(l)
            if not "batacur" in p: p["batacur"] = "0."
            if not "bataacur" in p: p["bataacur"] = "0."            
            s = ( p["utime"], p["batvolt"], p["bativolt"], p["batperc"], p["batiperc"], p["batcur"], p["batacur"], p["bataacur"] )
            s = map(float, s)
            m.data += [ s ]
            t, v1, v2, p1, p2, c, c2, c3 = s
            if t1 == 0: t1 = t
        m.t1 = t1
        m.t = (t-t1)/3600.
        print(m.t, " hours of recording")

    def read(m, path):
        e = path[-4:] 
        if e == ".egt":
            return m.read_egt(path)
        if e == ".log":
            return m.read_log(path)
        else:
            print("Unknown path extension", e)
            sys.exit(1)

m = Painter()
m.read(sys.argv[1])
m.run()
    

