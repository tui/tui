#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

/* This catches call begining and incoming SMS. But it does NOT catch
   call termination... Also, modem sends "RING" ... "RING"... first
   one is received, subsequent ones are not.
 */

int main(int argc, char *argv[])
{
  int gt = open("/dev/gsmtty1", O_RDONLY);
  int ut = open("/dev/ttyUSB0", O_WRONLY);

  while (1) {
    char buf[1024];
    int r;

    r = read(gt, buf, sizeof(buf));
    if (r < 0) {
      printf("Can't read from gsmtty1: %m\n");
      exit(1);
    }
    buf[0] = '\n';
    r = write(ut, buf, 1);
    if (r != 1) {
      printf("Can't write to ttyUSB: %m\n");
      exit(1);
    }
  }
}
