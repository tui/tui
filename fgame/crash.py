#!/usr/bin/python
# https://www.reddit.com/r/flying/comments/u1lod/whats_the_maximum_gs_on_a_c172_before_structural/

# Normal +3.8, -1.5 Utility +4.4, -1.8 Aerobatic... Refer to the POH
# for limitations. Most aerobatic trainers are around +6.0, -3.0

from __future__ import print_function

import os
import sys
sys.path += [ "../lib", "../zpells" ]
import re
import time

def sy(s): os.system(s)

class Launcher:
    def __init__(m):
        m.old_version = " --control=joystick"
        m.local = "--prop:/sim/rendering/texture-compression=off --prop:/sim/traffic-manager/enabled=0 --prop:/sim/rendering/shaders/quality-level=0 --disable-real-weather-fetch --enable-clouds3d"
        m.position = "--timeofday=dawn"
        m.extra = ""
        #                                  visib.
        m.weather = '"--metar=METAR XXXX 012345Z 12SM SCT060 FEW100"'
        # Telnetd... 1000 helps somehow (I get 10 properties a second).. but not nearly enough.
        m.nmea="--nmea=socket,out,1,localhost,5500,udp --httpd=5501 --telnet=1000,1000,1000,1000,5401,1000 --generic=file,out,20,flight.out,acaptain"
        m.scenery = "--fg-scenery=/data/fgfs/Scenery/"

    def run_fgfs(m):
        binary = "fgfs" # 24fps with MiG-15
        if m.version > 2018.99:
            binary = "/data/fgfs/2020.3/FlightGear-2020.3.1-x86_64.AppImage" # 20fps
        if m.version > 2020.99:
            binary = "/data/fgfs/2021/FlightGear-latest-x86_64.AppImage" # 7fps with MiG-15
        sy(" ".join([binary, m.local, m.aircraft, m.nmea, m.position, m.weather, m.extra, m.scenery])+ " > fgfs.log 2>&1 &")

    def run_gpsd(m):
        sy("sudo systemctl stop gpsd.socket")
        sy("sudo systemctl stop gpsd")
        sy("sudo gpsd -G udp://127.0.0.1:5500")
        time.sleep(.5)

class SimVars:
    def __init__(m):
        m.num_landing = 0
        m.url = "http://localhost:5501/props/"
        
    def pipe_command(m, cmd):
        return os.popen(cmd).readlines()

    def set_sim_var(m, var, value):
        sy("wget '"+m.url+var+"?value="+value+"&submit=update' -O /dev/null > /dev/null 2>&1")

    def get_sim_var(m, var):
        l = m.pipe_command("wget -O - '%s'" % m.url+var)
        l = l[0]
        l = re.sub('.*value="', '', l)
        l = re.sub('" /.../form.*', '', l)
        return l

    def get_sim_float(m, var):
        return float(m.get_sim_var(var))

    def get_accel(m, path):
        #return m.get_sim_float(path) / 32.26900156
        # Needs nasal script to work?
        max = m.get_sim_float(path + "-max")
        m.set_sim_var(path + "-clear", str(max + 0.01))
        return max / 32.26900156

    def check_accels(m):
        return
    
        v = m.get_accel("accelerations/pilot/z-accel-fps_sec")
        #if v < -3 or v > 8:
        #    m.game.say(m.voice_god, "You are overstressing the aircraft")

        if v < 0.5 or v > 1.5:
            m.display(str(v))

        # sim/crashed
        
    def score(m, score, msg):
        m.display(msg)

    def check_landing(m, num):
        base = "landing[%d]/" % num
        landing_fpm = m.get_sim_float(base + "down-relground-fps")
        landing_fpm *= -60
        if landing_fpm < 50:
            m.score(10, "nice landing -- for cessna")
        elif landing_fpm < 150:
            m.score(10, "very good landing")
        elif landing_fpm < 200:
            m.score(9, "good landing")
        elif landing_fpm < 250:
            m.score(5, "marginal landing")            
        elif landing_fpm < 600:
            m.score(3, "hard landing")            
        elif landing_fpm < 1500:
            m.score(2, "over aircraft limit")
        elif landing_fpm < 4000:
            m.score(1, "aircraft destroyed")
        else:
            m.score(0, "crash")

        pitch = m.get_sim_float(base + "pitch-deg")
        if pitch < -1:
            m.score(2, "bad nosewheel landing")            
        elif pitch < 0.5:
            m.score(3, "nosewheel landing")
        elif pitch > 10:
            m.score(3, "tailstrike")
        else:
            m.score(10, "good pitch at landing")
            
        m.display("touchdown at %d fpm" % landing_fpm)

        roll = m.get_sim_float(base + "roll-deg")
        if roll < 0:
            roll *= -1

        if roll < 2:
            m.score(10, "very good roll")
        elif roll < 5:
            m.score(10, "good roll")
        elif roll < 10:
            m.score(5, "marginal roll")            
        elif roll < 20:
            m.score(3, "wingstrike")            
        elif roll < 45:
            m.score(1, "aircraft destroyed")
        else:
            m.score(0, "crash")

        speed = m.get_sim_float(base + "airspeed-kt")
        vref = 47	# 47 kn is cessna 172 stall speed in flaps down, no power config
        if speed < vref - 5:
            m.score(4, "below Vref")
        elif speed < vref - 2:
            m.score(6, "slightly below Vref")
        elif speed < vref + 2:
            m.score(10, "perfect slow landing")
        elif speed < vref + 5:
            m.score(9, "good landing")
        elif speed < vref + 10:
            m.score(7, "marginal landing")
        elif speed < vref + 15:
            m.score(5, "fast landing")
        elif speed < vref + 35:
            m.score(3, "aircraft damaged -- too fast landing")
        else:
            m.score(2, "more crash than landing")

    def check_landings(m):
        # hack?
        return
        v = m.get_sim_float("landing/num")
        if v != m.num_landing:
            while m.num_landing < v:
                m.num_landing += 1                
                m.display("Landing %d" % m.num_landing)
                m.check_landing(m.num_landing)


    def display(m, msg):
        m.set_sim_var("/sim/messages/copilot", msg)

    def run(m):
        while True:
            m.check_accels()
            m.check_landings()
            time.sleep(2)

# Useful variables:
# /gear/gear/wow
# /position/altitude-agl-ft
# /velocities/airspeed-kt
            
if __name__ == "__main__":
    l = SimVars()
    l.run()
