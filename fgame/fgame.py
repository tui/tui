#!/usr/bin/python3
# gps module does not seem to be available for python3.

# sudo apt install python3-gps python3-geopy python3-lxml

from __future__ import print_function

import os
import sys
sys.path += [ "../lib", "../zpells" ]
import textui
import util
import util_geo
import select
import time
import gps
import formulate
import math
import crash
import random
import heliport

from aircraft import *
from airport import *

# 38.0 N is limit of default scenery
# 122 W is _not_ a limit
# There's pretty cool mountain at 37.883671/-121.926728, Mt.diablo, 1173meters.
#
# https://skyvector.com/ contains maps usable for navigation.
# http://opentopomap.org/#map=15/37.86384/-121.95193 is useful for terrain
# foxtrotgps is useful
# settings:
# http://a.tile.opentopomap.org/%d/%d/%d.png
# /data/pavel/Maps/OpenTopo


def sy(s): os.system(s)

class GameOver(BaseException):
    pass

class Voice:
    pass

class Places:
    def __init__(m):
        m.lkln = FixPos(49 + 40./60, 13 + 16/60., name = "Plzen")
        m.lkvm = FixPos(49.9272, 16.1856, name = "Vysoke myto") # Invisible in fgfs
        m.ekrn = FixPos(55.064804, 14.754007, name = "Bornholms airport")
        m.lkkb = FixPos(50 + 7.5/60., 14+ 33/60., name = "Kbely")
        m.lszs = FixPos(46 + 32./60., 9+ 53./60., name = "Samedan")
        m.air_bari = FixPos(41.135742, 16.756210, name = "Bari")

        p = FixPos(-37.008056, 174.791667)
        p.name = "Auckland"
        m.nzaa = p

        p = FixPos(40.639722, -73.778889)
        p.name = "JFK New York"
        m.kjfk = p

        p = FixPos(37.518169, -122.506493)
        p.name = "khaf"
        p.track = 130
        m.khaf = p

        p = FixPos(37 + 37.09/60., -122 - 22.40/60.)
        p.name = "ksfo"
        m.ksfo = p

        p = FixPos(50.1068, 14.2441)
        p.name = "lkpr"
        m.lkpr = p

        p = FixPos(47.4847, 9.5570)
        p.name = "lzsr"
        m.lzsr = p

        p = FixPos(47 + 4.68/60, 9 + 3.90/60.)
        p.name = "lsmf"
        m.lsmf = p

        p = FixPos(45.4278611, -116.6938889)
        p.name = "25u"
        m._25u = p

        # 1st most dangerous airport
        p = FixPos(27.395445, 89.429475)
        p.name = "vqpr"
        m.vqpr = p

        # 200km from 1st most dangerous
        p = FixPos(27.253718, 91.519250)
        p.name = "vqty"
        p.track = 300
        m.vqty = p

        # 4th most dangerous, in steep mountain
        p = FixPos(27.684665, 86.733462)
        p.name = "vlnk"
        p.track = 240
        m.vlnk = p

        p = FixPos(26.708763, 85.923318)
        p.name = "vnjp"
        p.track = 240
        m.vnjp = p

        p = FixPos(45.990525, 63.289207)
        p.name = "Baikonur"
        m.baikonur = p

        p = FixPos(47.260278, 11.343889)
        p.name = "Innsbruck Airport"
        m.lowi = p

        p = FixPos(43 + 15.42/60, 5 + 46.62/60)
        p.name = "lfmq"
        m.lfmq = p

        # Saba airport
        # 400m runway on island
        p = FixPos(17.644047, -63.219698)
        p.name = "Saba airport"
        m.tncs = p

        # Something nearby. (17 + 38/60., -63 + 13/60.)

        p = FixPos(47 + 27.8/60., 8 + 33/60.)
        p.name = "Kloten Airport"
        m.lszh = p

        p = FixPos(47 + 27.8/60., 8 + 33/60.)
        p.name = "St. Vincent Airport"
        m.tvsv = p

        p = FixPos(42.561389, 18.268333)
        p.name = "Dubrovnik Airport"
        m.lddu = p

        p = FixPos(43.538889, 16.298056)
        p.name = "Split Airport"
        m.ldsp = p

        p = FixPos(16.264167, -61.525833)
        p.name = "Pointe-a-Pitre Airport"
        m.tffr = p

        p = FixPos(59.349917, -2.953818)
        p.name = "egew"
        p.track = 85
        m.egew = p

        p = FixPos(59.351667, -2.900278)
        p.name = "Papa Westray Airport"
        m.egep = p

        p = FixPos(39.221944, -106.868333)
        p.name = "Sardy field"
        m.kase = p

        p = FixPos(38.784065, -9.134102)
        p.name = "Lisboa"
        m.lppt = p

        p = FixPos(35.765278, 140.385556)
        p.name = "Tokyo"
        m.rjaa = p

        p = FixPos(43.097222, 6.146111)
        p.name = "Toulon"
        m.lfth = p

        p = FixPos(21.318611, -157.9225)
        p.name = "Honolulu"
        m.phnl = p

        p = FixPos(44.140, -103.098)
        p.name = "Ellsworth Air Force Base"
        m.krca = p

        p = FixPos(44.045278, -103.057222)
        p.name = "Rapic City Regional"
        m.krap = p

        p = FixPos(37.778056, -89.251944)
        p.name = "Southern Illinois Airport"
        m.kmdh = p

        p = FixPos(36.894722, -76.201111)
        p.name = "Norfolk International"
        m.korf = p

        p = FixPos(58.355, -134.576389)
        p.name = "Juneau International"
        m.pajn = p

        p = FixPos(-14.267000, -70.467003)
        # The airport is very strangely modeled. Not sure if PAPI can be trusted.
        p.name = "San Rafael"
        m.sprf = p

        p = FixPos(-16.340278, -71.566667)
        p.name = "Ballon international"
        m.spqu = p

        p = FixPos(63.6450994, -148.7981306)
        p.name = "Denali airport"
        m.ak06 = p

        p = FixPos(42.77, -1.647)
        p.name = "Pamplona airport"
        m.lepp = p

        p = FixPos(35.877778, -78.7875)
        p.name = "Raleigh airport"
        m.krdu = p

        p = FixPos(33.411522, 36.515558)
        p.name = "osdi"
        m.osdi = p

        p = FixPos(64.13, -21.940556)
        p.name = "Reykjavik"
        m.birk = p

        p = FixPos(65.661111, -18.072222)
        p.name = "biar"
        m.biar = p

        # not visible on ground :-(.
        p = FixPos(49.09643, 19.73385)
        p.name = "mokrad"
        m.mokrad = p

        # Poprad airport: visible, pretty big. LZTT.
        p = FixPos(49.073611, 20.241111)
        p.name = "Poprad"
        m.lztt = p

        p = FixPos(51.4775, -0.461389)
        p.name = "Heathrow"
        m.egll = p

        # Short, big buildings around
        p = FixPos(18.040833, -63.109444)
        p.name = "Saint Maarten"
        m.tncm = p

        p = FixPos(-0.1133, -78.3586)
        p.name = "Quito"
        m.seqm = p

        p = FixPos(46.912222, 7.499167)
        p.name = "Bern"
        m.lszb = p

        p = FixPos(43.2490, 6.1215)
        p.name = "cuers"
        m.af_cuers = p

        p = FixPos(44.413333, 8.8375)
        p.name = "Genova"
        m.limj = p

        # Suitable for MiG15, nice mountains around, not easy to land from west
        p = FixPos(43 + 14.9/60, 6 + 7.7/60)
        p.name = "L'aerodrome de Cuers-Pierrefeu"
        m.lftf = p

        p = FixPos(49 + 18./60, 17 + 1.7 /60.)
        p.name = "Vyskov"
        m.lkvy = p

        p = FixPos(49 + 18./60, 17 + 1.7 /60.)
        p.name = "Smolensk"
        m.uubs = p
        # Invisible in the flightgear :-(

        p = FixPos(27 + 41.8/60, 85 + 21.5 /60.)
        p.name = "Kathmandu"
        m.vnkt = p

        p = FixPos(41 + 47.95/60, 12 + 35.8 /60.)
        p.name = "Rome Ciampino"
        m.lira = p

        # 35 46'33.9"N 050 49'36.1"E
        p = FixPos(35 + 46.56/60, 50 + 49.62 /60.)
        p.name = "Payam International"
        m.oiip = p

        # Challenging airport in colorado
        # 37 57.38' / W107 55.24'
        p = FixPos(37 + 57.38/60, -107 - 55.24 /60.)
        p.name = "Telluride Regional"
        m.ktex = p

        # Highest airport in colorado
        # 39 13'13"N 106 19'0

        p = FixPos(39 + 13.2/60, -106 - 19.0 /60.)
        p.name = "Lake County airport"
        m.klxv = p

        # Highest civilian airport in the world
        # 29 19 23 N 100 03 12 E
        # Not available in fgfs.
        p = FixPos(29 + 19.33/60, 100 + 3.16/60.)
        p.name = "Hanyu Pinyin"
        m.zudc = p

        # Very high, quite big
        # 16 30 48"S 068 11 32"W
        p = FixPos(-16 - 30.75/60, -68 - 11.53/60.)
        p.name = "El Alto"
        m.sllp = p

        p = FixPos(37.811024, -122.414970)
        p.name = "San Francisco helipad"
        m.san_francisco_heli = p

        p = FixPos(37.825871, -122.421509)
        p.name = "Alcatraz helipad"
        m.alcatraz_heli = p

        p = FixPos(40.701096, -74.009026)
        p.name = "Downtown Manhattan heliport"
        m.manhattan_heli = p

        p = FixPos(40.742256, -73.972099)
        p.name = "FDR drive heliport"
        m.fdr_drive_heli = p
        

        p = FixPos(40.689163, -74.044289)
        p.name = "Liberty statue"
        m.liberty_statue = p

        p = FixPos(25.090515, 55.149845)
        p.name = "Dubai university"
        m.dubai_uni_heli = p
        # altitude-ft 723.2718101
        # ground-elev-ft 717.1309692
        # ground-elev-m 218.5815194
        

        p = FixPos(25.140982, 55.185802)
        p.name = "Mahara tower heliport"
        m.mahara_heli = p

        p = FixPos(34.05107, -118.25442)
        p.name = "Bank Tower"
        m.bank_tower = p
        
        p = FixPos(34.058079, -118.235619)
        p.name = "Los angeles station heliport"
        m.los_angeles_heli = p

        p = FixPos(38.887360, -77.023300)
        m.washington_heli = p

        p = FixPos(38.8977, -77.0365)
        p.name = "White House"
        m.white_house = p

        p = FixPos(35.68919, 139.69180)
        p.name = "Tokyo Metropolitan Government"
        m.tokyo_metropolitan = p

class Option:
    def __init__(m, name, text):
        m.name = name
        m.text = text

class Level(textui.BaseLoop):
    def __init__(m, game):
        textui.BaseLoop.__init__(m)
        m.game = game
        m.voice_boss = Voice()
        m.voice_pilot = Voice()
        m.voice_god = Voice()
        m.voice_atc = Voice()
        m.voice_steward = Voice()
        m.enable_terrasync = False
        m.pos_airport = None
        m.launcher = crash.Launcher()
        m.places = Places()
        m.set_aircraft(Seneca())
        m.sim = crash.SimVars()
        m.said = []

        # 38.0 is limit of default scenery
        m.pos_start = FixPos(37.99293, -122.055103)
        m.pos_start.altitude_ft = None
        m.pos_start.track = 220
        m.pos_waypoint = []
        # Hayward executive
        m.pos_target = FixPos(37.6583, -122.1119)
        m.callsign = "golf juliet sierra uniform"
        m.takeoff_ft = 2000
        m.intro_text = """Fly, somewhere. I guess."""
        m.level_options = [ Option("wait", "Wait a moment, something is not right here") ]
        m.seed = random.randint(0, 1000)
        m.balloons = False
        random.seed(m.seed)
        m.init()

    def set_aircraft(m, a):
        m.aircraft = a
        m.launcher.aircraft = " --aircraft="+a.id+" "+a.extra_options
        m.launcher.version = a.version

    def get_launcher(m):
        l = m.launcher
        if m.pos_airport:
            l.position += " --airport="+m.pos_airport
        else:
            l.position += " --lat=%f --lon=%f --heading=%f" % (m.pos_start.lat, m.pos_start.lon, m.pos_start.track)
        if m.pos_start.altitude_ft: l.position += " --altitude=%d --vc=150" % m.pos_start.altitude_ft

        if m.enable_terrasync:
            l.extra += " --enable-terrasync"
        return l

    def get_key(m):
        return sys.stdin.read(1)

    def say_once(m, v, s):
        if s in m.said:
            return False
        m.game.say(v, s)
        m.said += [ s ]
        return True

    def print_question(m, options):
        i = 1
        print()
        for o in options:
            o.key = i
            print(i, o.text)
            i += 1

    def get_answer(m, options):
        ( rlist, wlist, xlist ) = select.select(m.watch, [], [], 1)
        m.game.update_pos()
        if sys.stdin in rlist:
            key = m.get_key()
            if key > '0' and key < '9':
                key = int(key)
                for o in options:
                    if key == o.key:
                        return o.name
        return ""

    def end_game(m, s):
        m.game.say(m.voice_god, s)
        m.end_game_hook()
        raise GameOver()

    def wait_for_sim(m):
        m.new_phase("Simulator ready")
        m.game.say(m.voice_god,
                   """Is the simulator ready?""")

        menu = [ Option("ok", "Yes, the simulator is ready.") ]
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            if answer == "ok":
                break

        m.aircraft.sim = m.sim
        m.aircraft.setup()

    def new_phase(m, s):
        m.phase_start = time.time()
        m.phase_name = s

    def phase_len(m):
        return time.time() - m.phase_start

    def handle_answer(m, question, answer):
        if answer == "wait":
            m.end_game("""Everything seems okay to me.""")
            return 1
        return 0

    def takeoff_speed(m, speed, phase, say, menu):
        if speed > 0:
            while True:
                answer = m.get_answer(menu)
                if answer:
                    if m.handle_answer(phase, answer):
                        return
                print(m.game.pos.speed_kn)
                m.flight_checks()
                if m.phase_len() > 4*60:
                    m.end_game("""You did not accelerate in time""")
                    return True
                if m.game.pos.speed_kn > speed:
                    break

            m.game.say(m.voice_god, say)
        return False

    def takeoff(m):
        m.game.say(m.voice_atc,
                   """Clear for takeoff %s.""" % m.callsign)

        m.new_phase("Takeoff")
        menu = [] + m.level_options
        m.print_question(menu)

        if m.takeoff_speed(m.aircraft.v_1, "takeoff", "V 1", menu): return
        if m.takeoff_speed(m.aircraft.v_r, "takeoff", "Rotate", menu): return
        if m.takeoff_speed(m.aircraft.v_2, "V 1", "V 2", menu): return

        time.sleep(3)
        m.game.say(m.voice_god, "Now climb to %d feet" % m.takeoff_ft)
        while True:
            answer = m.get_answer(menu)
            if m.handle_answer("V2", answer):
                return
            m.game.update_pos()
            m.flight_checks()
            print(m.game.pos.speed_kn, m.game.pos.altitude_ft)
            if m.game.pos.altitude_ft > m.takeoff_ft-100:
                break
            if m.phase_len() > 10*60:
                m.end_game("""You did not climb in time""")
                return


    def flight_hook(m):
        pass

    def flight_checks(m):
        m.sim.check_accels()
        m.sim.check_landings()
        m.flight_hook()

    def move_balloon(m, p):
        if m.balloons:
            m.sim.set_sim_var('ai/models/tanker/position/latitude-deg', '%f'%p.lat)
            m.sim.set_sim_var('ai/models/tanker/position/longitude-deg', '%f'%p.lon)
            if p.altitude_ft:
                alt = p.altitude_ft
            else:
                alt = m.game.pos.altitude_ft + 100
            m.sim.set_sim_var('ai/models/tanker/position/altitude-ft', '%f'%alt)

    def goto_waypoint(m, pos):
        try:
            pos.write_waypoint()
        except:
            pass
        menu = [ Option("navigate", "Tell me where to go") ] + m.level_options
        t = 18
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            m.move_balloon(pos)
            if m.handle_answer("waypoint", answer):
                return
            if True:
                tt = TravelTo(m.game.pos, pos)
                text = tt.formulate()
                if t > 20 or answer == "navigate":
                    m.game.say(m.voice_god, text)
                    t = 0
                #print("speed", m.game.pos.speed_kn)
                agl = tt.distance * 171
                if agl < 3000:
                    agl = 3000
                agl = int(agl)
                agl += tt.distance / 1000.
                m.sim.set_sim_var("autopilot/settings/target-agl-ft", "%.3f"%agl)
                m.sim.set_sim_var("autopilot/settings/true-heading-deg", "%d"%tt.bearing)

            m.flight_checks()
            m.game.update_pos()
            if util.distance(m.game.pos, pos) < pos.target_size:
                return "waypoint"
            if m.game.pos.speed_kn < 2:
                return "landed"
            t += 1

    def follow_waypoints(m):
        m.new_phase("waypoint")
        for wp in m.pos_waypoint:
            if m.goto_waypoint(wp) != "waypoint":
                m.end_game("You managed to land? How did you manage to land? Where?")
            if wp.message:
                m.game.say(m.voice_pilot, wp.message)
            else:
                m.game.say(m.voice_pilot, """Ok, next waypoint.""")
            if wp.action:
                wp.action()

    def cruise(m):
        m.new_phase("Cruise")
        m.goto_waypoint(m.pos_target)

    def landing(m):
        m.new_phase("Landing")

        if util.distance(m.game.pos, m.pos_target) > 2:
            m.end_game("""You are now on the ground, good, but you are not on the destination
            airport.""")

        m.game.say(m.voice_god, """You are now on the ground, good. You even seem to be on the destination airport.""")

        menu = [ Option("crashed", "But I crashed"),
                 Option("damage", "But I damaged the airplane"),
                 Option("intact", "I did not even damage the plane"),
        	 Option("ok", "I even stayed reasonably within the regulations") ]
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            if answer == "crashed":
                 m.end_game("""You are not supposed to crash planes.""")
            if answer == "damage":
                 m.end_game("""Try better next time.""")
            if answer == "intact":
                 m.end_game("""Well, walking away from the landing is important, undamaged plane is nice bonus.""")
            if answer == "ok":
                 m.end_game("""Congratulations, you have made it.""")

    def end_game_hook(m):
        pass

    def landing_survive(m):
        m.new_phase("Landing")

        if util.distance(m.game.pos, m.pos_target) > 2:
            m.end_game("""You are now on the ground, good, but you are not on the destination
            airport. Any airport is a pretty good result. """)

        m.game.say(m.voice_god, """You are now on the ground, good. You even seem to be on the destination airport.""")

        menu = [ Option("crashed", "But I crashed"),
                 Option("damage", "But I damaged the airplane"),
                 Option("ok", "I did not even damage the plane") ]
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            if answer == "crashed":
                 m.end_game("""You are not supposed to crash planes.""")
            if answer == "damage":
                 m.end_game("""This was pretty bad situation.""")
            if answer == "ok":
                 m.end_game("""Congratulations, you have made it.""")

    def intro(m):
        m.new_phase("Before takeoff check")
        menu = [ Option("takeoff", "I am ready for takeoff") ] + m.level_options
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            if m.handle_answer("waypoint", answer):
                return
            if answer:
                if answer == "takeoff":
                    break
                # 707 startup is quite long.
            if m.phase_len() > 7*60:
                m.end_game("""You did not decide in time""")
                return

    def set_fuel(m, f1, f2, f3 = "0", f4 = "0"):
        m.sim.set_sim_var("consumables/fuel/tank/level-lbs", f1)
        m.sim.set_sim_var("consumables/fuel/tank%5B1%5D/level-lbs", f2)
        m.sim.set_sim_var("consumables/fuel/tank%5B2%5D/level-lbs", f3)
        m.sim.set_sim_var("consumables/fuel/tank%5B3%5D/level-lbs", f4)
        m.sim.set_sim_var("consumables/fuel/tank%5B4%5D/level-lbs", "0")
        m.sim.set_sim_var("consumables/fuel/tank%5B5%5D/level-lbs", "0")
        m.sim.set_sim_var("consumables/fuel/tank%5B6%5D/level-lbs", "0")
        m.sim.set_sim_var("consumables/fuel/tank%5B7%5D/level-lbs", "0")

    def fail_engines(m):
        m.fail_engine(0)
        m.fail_engine(1)

    def fail_engine(m, num = 0):
        if num == -1:
            random.randint(0, m.aircraft.engines-1)
        p = "sim/failure-manager/engines/engine"
        q = "engines/engine"
        if num > 0:
            p += "[%d]" % num
            q += "[%d]" % num
        m.sim.set_sim_var(p+"/serviceable", "false")
        m.sim.set_sim_var(q+"/on-fire", "true")

    def fail_system(m, s):
        all = [ "static", "pitot", "vacuum", "electrical", "rudder", "rudder-hardover" ]
        if s == "random":
            s = random.choice(all)
        if not s in all:
            print("!!! Unknown system to fail")
            exit(0)
        if s in [ "rudder", "rudder-hardover" ]:
            if s == "rudder-hardover":
                m.sim.set_sim_var("controls/flight/rudder", "0.64")
            m.sim.set_sim_var("sim/failure-manager/controls/flight/rudder/serviceable", "false")
            if s == "rudder-hardover":
                m.sim.set_sim_var("controls/flight/rudder", "0.64")
            return
            
        if s == "vacuum":
            m.sim.set_sim_var("systems/vacuum/serviceable", "false")
            m.sim.set_sim_var("systems/vacuum%5B1%5D/serviceable", "false")
            # Not much efect in seneca...
            m.sim.set_sim_var("instrumentation/attitude-indicator/serviceable", "false")
            return
        if s in [ "static", "pitot", "electrical" ]:
            m.sim.set_sim_var("systems/%s/serviceable" % s, "false")
            return
        print("!!! Internal error in system failing")
        exit(0)

    def fail_instrumentation(m, s):
        all = [ "adf", "airspeed-indicator", "altimeter", "attitude-indicator",
                "dme", "heading-indicator", "vertical-speed-indicator"]
        # "nav", "nav[1],  "slip-skid-ball", "turn-indicator", "magnetic-compass"
        if s == "random":
            s = random.choice(all)
        if not s in all:
            print("!!! Unknown system to fail")
            exit(0)
        m.sim.set_sim_var("systems/instrumentation/serviceable" % s, "false")
        

    def fail_static(m):
        m.fail_system("static")

    def fail_pitot(m):
        m.fail_system("pitot")

    def fail_vacuum(m):
        m.fail_system("vacuum")

    def set_time(m, val):
        m.sim.set_sim_var("sim/time/cur-time-override", str(val))

    def intro_hook(m):
        pass
        # We used to set fuel here:
        # m.set_fuel("20.5", "20.7")

    def cruise_hook(m):
        pass

    def run(m):
        m.game.say(m.voice_boss, m.intro_text)

        m.intro_hook()
        m.intro()
        m.takeoff()

        m.game.say(m.voice_god,
                   """You have reached %d feet. Fly V F R, descent when appropriate.""" % m.takeoff_ft)
        m.cruise_hook()

        m.follow_waypoints()
        m.cruise()
        m.landing()

    def set_wind_fps(m, north, east, down):
        m.sim.set_sim_var("environment/local-weather-lift-fps", str(down))
        m.sim.set_sim_var("environment/wind-from-down-fps", str(down))
        m.sim.set_sim_var("environment/wind-from-east-fps", str(east))
        m.sim.set_sim_var("environment/wind-from-north-fps", str(north))

    def set_wind(m, headwind, down):
        m.set_wind_fps(0.7*headwind, -0.7*headwind, down)

    def alt_scenery(m):
        m.launcher.scenery="--fg-scenery=/data/fgfs/Scenery/"

    def enable_balloons(m):
        m.balloons = True
        m.launcher.extra += ' "--ai-scenario=balloon_demo"'

class Trap:
    pass

class LevelTrap(Level):
    def init_trap(m):
        m.trap = Trap()
        r = random.random()
        r -= 0.1
        if r < 0:
            m.trap.when = "takeoff"
            m.trap.what = "engine"
            m.trap.speed = random.random()*m.aircraft.v_2 + 30
            return
        r -= 0.2
        if r < 0:
            m.trap.when = "takeoff"
            m.trap.what = "system"
            m.trap.speed = random.random()*m.aircraft.v_2 + 50
            return
        r -= 0.1
        if r < 0:
            m.trap.when = "takeoff"
            m.trap.what = "engine"
            m.trap.speed = random.random()*m.aircraft.v_1
            return
        r -= 0.2
        if r < 0:
            m.trap.when = "landing"
            m.trap.what = "engine"
            m.trap.speed = random.random()*20 + m.aircraft.v_2
            return
        r -= 0.2
        if r < 0:
            m.trap.when = "landing"
            m.trap.what = "system"
            m.trap.speed = random.random()*50 + m.aircraft.v_2
            return
            
        m.trap.when = "no"
        m.trap.what = "never"
        m.trap.speed = 0

    def end_game_hook(m):
        print("Trap was ", m.trap.when, m.trap.what, m.trap.speed)

    def run_trap(m):
        if m.trap.what == "engine":
            m.fail_engine(-1)
        if m.trap.what == "system":
            m.fail_system("random")

    def flight_hook(m):
        # if m.trap.when == "takeoff-alt" and 
        if m.trap.when == "takeoff" and m.game.pos.speed_kn > m.trap.speed:
            m.run_trap()
        if m.trap.when == "landing" and m.game.pos.speed_kn < m.trap.speed \
            and util.distance(m.game.pos, m.pos_target) < 10:
            m.run_trap()

class TravelTo(formulate.TravelTo):
    pass

class LevelAfterRepairs(Level):
    spoiler = "Flying on empty"
    # Inspired by:
    # http://asndata.aviation-safety.net/reports/2013/20130610-3_BE10_C-GJSU.pdf

    def init(m):
        m.pos_start = FixPos(37.50879, -122.49538)
        m.pos_start.track = 300
        m.pos_target = FixPos(37.51442, -122.25216)

    def sabotage_fuel(m):
        # On seneca2, whole flight is possible with 8.6 lbs in each tank. 1.4 lbs is needed for go around
        #                                       ... with 8.1 lbs in each tank. If I'm careful.
        # Big airport is to the north of the flight path, can be reached with 5.8 lbs of fuel.
        # FIXME: this changed in new flightgear.
#        m.set_fuel("7.9", "8.1")
        m.set_fuel("22.3", "22.5")

    def handle_answer(m, question, answer):
        if answer == "wait":
            m.end_game("""You are right, there is not enough fuel. But that is not how this story was supposed to end.""")
            return 1
        return 0

    def run(m):
        m.callsign = "golf juliet sierra uniform"

        m.sabotage_fuel()
        m.game.say(m.voice_boss,
                   """We have performed some maintainance on this Seneca. Now we would
like you to fly it to its owner at San Carlos, approximately 33km east
from here. Check the aircraft and fly there. Fly heading of 90
degrees at maximum altitude of 3000 feet.""")

        m.intro()
        m.takeoff()

        m.game.say(m.voice_god,
                   """You have reached 2000 feet. Turn towards 90 degrees and climb to 3000 feet. Fly V F R, descent when appropriate.""")

        m.cruise()
        m.landing()

    def landing(m):
        m.new_phase("Landing")

        if util.distance(m.game.pos, m.pos_target) > 2:
            m.end_game("""You are now on the ground, good, but you are not on the destination
            airport. If you are on any airport, that is still pretty
            good result. If you made survivable emergency landing,
            that is good, too.""")

        m.game.say(m.voice_god, """You are now on the ground, good. You even seem to be on the destination airport.""")

        menu = [ Option("crashed", "But I crashed"),
                 Option("damage", "But I damaged the airplane"),
                 Option("ok", "I did not even damage the plane") ]
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            if answer == "crashed":
                 m.end_game("""Well, with so little fuel, only safe variant was not to fly.""")
            if answer == "damage":
                 m.end_game("""If you only damaged the plane, that is pretty good result, only safe variant was not to fly.""")
            if answer == "ok":
                 m.end_game("""Congratulations, you have made it.""")

# Unfinished / untested. FIXME: needs to switch the aircraft.
class LevelSeaplane(Level):
    def init(m):
        # KSFO, 28R
        m.launcher.aircraft = " --aircraft=dhc2F"
        m.pos_start = FixPos(37.61390, -122.35860)
        m.pos_start.track = 280
        # Hmm. Nice place. For seaplanes.
        m.pos_target = FixPos(37.87993, -122.51427)

    def run(m):
        m.callsign = "golf juliet sierra uniform"

        m.game.say(m.voice_boss,
                   """Please fly this XXX to small airport near Mill Valley. Airport is to the north of you. You should be able to get to the Golden Gate bridge, then follow highway there. Climb to 2000 feet.""")

        m.intro()
        m.takeoff()

        m.game.say(m.voice_god,
                   """You have reached 2000 feet. Fly over San Francisco city, then follow the highway north.. Fly V F R, descent when appropriate.""")

        m.cruise()
        m.landing()

class LevelShortRunway(Level):
    def init(m):
        # 38.0 is limit of default scenery
        # Small airport in mountains
        m.pos_start = FixPos(37.068848, -122.123958)
        m.pos_start.track = 300
        # Tiny strip on beach
        m.pos_target = FixPos(37.08766, -122.27283)

    def run(m):
        m.callsign = "golf juliet sierra uniform"

        m.game.say(m.voice_boss,
                   """Please fly this Seneca to small airport near shore. Airport is to the west of you. Climb to 2000 feet.""")

        m.intro()
        m.takeoff()

        m.game.say(m.voice_god,
                   """You have reached 2000 feet. Fly V F R, descent when appropriate.""")

        m.cruise()
        m.landing()

class LevelLeak(Level):
    # Emergency landing on different airport is possible with 4lb left.
    # Cross-feed makes it trivial.
    # Plus seneca has two engines..?
    def init(m):
        m.right_fuel = 20.7

    def steal_fuel(m):
        m.right_fuel -= 0.3
        fuel = m.right_fuel
        if fuel < 0:
            fuel = 0
            m.sim.set_sim_var("consumables/fuel/tank%5B1%5D/level-lbs", str(fuel))

    def flight_hook(m):
        m.steal_fuel()

class LevelImbalance(Level):
    def init(m):
        m.pos_airport = "LSMM"
        m.pos_target = m.places.lszb
        m.takeoff_ft = 2500
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Quite a long flight at this speed..."

        # lsmm, lszb, lszw are suitable for landing. Airport near Bad Durrheim is nice, too.

    def intro_hook(m):
        m.set_fuel("1.1", "100.7")

class LevelOnTop(Level):
    def init(m):
        # Inspired by: https://www.youtube.com/watch?v=R-EkSaw1Fqw
        # Near mt diablo
        #m.pos_start = FixPos(37+54/60., -121-50/60.)

        #m.set_aircraft(MiG15()) -- not suitable; can not start in air.
        m.pos_start = FixPos(37+31/60., -122-25/60.)
        m.pos_start.track = 120
        m.pos_start.altitude_ft = 5000

        # It wants "basic weather"...
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 1SM OVC003 SCT018"'

    def intro_hook(m):
        return
        m.sim.set_sim_var('environment/clouds/layer/visibility-m', '350')
        m.sim.set_sim_var('environment/clouds/layer/coverage', 'overcast')
        m.sim.set_sim_var('environment/clouds/layer/elevation-ft', '200')
        m.sim.set_sim_var('environment/clouds/layer/thickness-ft', '1600')

    def fly_rnav(m, dest, course):
        menu = [ Option("wait", "Wait a moment, something is not right here"),
                 Option("navigate", "Tell me where to go") ]

        # Normal glide path is 3 degrees.

        m.print_question(menu)
        t = 0
        while True:
            answer = m.get_answer(menu)
            if t > 7 or answer == "navigate":
                dist = util.distance(m.game.pos, dest)
                # ideal position
                ipos = util_geo.get_point_at(dest, course+180, (dist * 0.9 * 1000))
                iheight = dest.altitude_ft + math.tan(3/180. * math.pi) * 3280 * dist
                tt = TravelTo(m.game.pos, ipos)
                m.game.say(m.voice_god, tt.formulate() + " height " + str(int(iheight)))
                t = 0
            m.flight_checks()
            m.game.update_pos()
            if m.game.pos.speed_kn < 2:
                return "landed"
            t += 1


    def run(m):
        m.intro_hook()

        # FIXME: retract the landing gear, disengage parking brake
        # starting the engine for user would be nice, too.

        m.game.say(m.voice_pilot, """This is not good. I am over the
        hills, about 5 nautical miles south west of San Francisco
        International Airport, north east of Half Moon bay airport. I
        was hoping for weather to get better, but it got worse,
        instead. I am now on top of cloud. That is not looking
        good. Not at all. The cloud seems to be from 300 feet above
        see level to 1800 feet. It seems I have few options
        here. There is Bonny Doon airport, about 30 nautical miles
        south from my current position. With some luck, it is above
        clouds. I could get there using G P S. But if the weather gets
        worse, or if it is in cloud, I am in trouble. I could also
        head for the San Francisco international airport. There is
        water north from it, I could descent over the bay, using G P S
        for navigation, then head south, and land on their big
        runways. I would have to declare mayday, and I am sure to have
        some fun with National Transport Safety Board. Pretty sure I
        am going to get to front pages of newspapers, too. Half Mono
        bay airport is near the Ocean, too. Much smaller and thus much
        harder to find, but I guess I would avoid at least the
        newspapers that way.""")

        menu = [ Option("khaf", "Head for the Half Moon bay"),
                 Option("ksfo", "Cause some excitement on San Francisco International Airport"),
                 Option("doon", "Sneak onto Bonny Doon airport"),
                 Option("ksfo_28r", "Ask for ILS approach on San Francisco International Airport"),
                 Option("khaf_30", "Ask for RNAV approach to Half Moon bay") ]


        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            m.game.update_pos()
            if answer == "khaf":
                wp6 = FixPos(37 + 22.00/60., -122 - 38.06/60.)
                wp6.target_size = 1
                wp7 = FixPos(37 + 24.02/60., -122 - 31.69/60.)
                wp7.target_size = .8
                wp8 = FixPos(37 + 26.58/60., -122 - 28.74/60.)
                wp8.target_size = .6
                wp8.message = "You should see ground soon"
                wp9 = FixPos(37 + 28.90/60., -122 - 27.38/60.)
                wp9.target_size = .3
                wp9.message = "You should see ground now, and soon see the airport"
                m.pos_waypoint = [ wp7, wp8, wp9 ]
                m.pos_target = m.places.khaf
                m.game.say(m.voice_pilot,
                           """Ok, small airport sounds like a lot of fun.""")
                break
            if answer == "ksfo":
                m.pos_target = m.places.ksfo
                m.pos_waypoint = [ FixPos(37 + 41.45/60., -122 - 19.56/60.) ]
                m.pos_waypoint[0].target_size = 1
                m.game.say(m.voice_pilot,
                           """Ok, lets go to the big airport.""")
                break
            if answer == "ksfo_28r":
                # https://skyvector.com/files/tpp/1610/pdf/00375I28RSAC1.PDF
                m.game.say(m.voice_pilot,
                           """nav1 = 111.7, dme = 111.7 go to 3000 feet, east. Capture localizer,
                           stay at 3000 until DME 12 or so. When glideslope is captured, you can descent.""")
                break
            if answer == "khaf_30":
                # 14 E deviation.
                m.pos_target = FixPos(37.515766, -122.503733)
                m.pos_target.altitude_ft = 40
                m.fly_rnav(m.pos_target, 302 + 14.)
                m.landing()
                return

            if answer == "doon":
                m.pos_target = FixPos(37 + 4.18/60., -122 - 7.72/60.)
                m.pos_target.name = answer
                m.pos_waypoint = []
                m.game.say(m.voice_pilot,
                           """Ok, I hope I decided well.""")
                break


        m.follow_waypoints()
        m.game.say(m.voice_pilot,
                   """Ok, I should be in the runway centerline at the moment. Now all I
                   need to do is descent above water level, and head in the direction of
                   the runway. Should be easy, right?""")

        m.cruise()
        m.landing_survive()

class LevelMountains(Level):
    # Inspired by:
    # https://www.youtube.com/watch?v=bLmzy8ZPgtc&index=11&list=PLCC59953860B62145
    def init(m):
        # Possible with MiG-15, too... but the destination runway is quite tricky with that.
        #m.set_aircraft(MiG15())

        # 38.0 is limit of default scenery
        m.pos_start = FixPos(37.99293, -122.055103)
        m.pos_start.track = 220
        # Small airport over the mountains
        m.pos_target = FixPos (37.823186, -121.62835)
        m.enable_balloons()

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = .5
        m.pos_waypoint = wp

        m.takeoff_ft = 600

        m.intro_text = "Let us go to the mountain trip."


    def get_waypoints_orig(m):
        # Fly between fossil hill and haystack hill
        wp  = [ FixPos(37.87716, -122.00678) ]
        wp[-1].message = "Turn left into the mountains"
        wp += [ FixPos(37.85195, -121.92661) ]
        wp[-1].message = "Mountain pass"
        wp += [ FixPos(37.86920, -121.87417) ]
        wp += [ FixPos(37.87956, -121.86782) ]
        wp[-1].message = "Now turn sharply right, follow valley"
        wp += [ FixPos(37.83141, -121.80619) ]
        wp += [ FixPos(37.8242,  -121.7337) ]
        wp[-1].message = "You should see the level ground now"
        return wp

    def get_waypoints(m):

        wp  = [ FixPos(37.905945, -122.026830) ]
        wp[-1].message = "Turn left, head for crossing of road and power lines."
        wp += [ FixPos(37.899202, -121.990190) ]
        wp[-1].message = "Follow the road to the mountains."
        wp += [ FixPos(37.867243, -121.942657) ]
        wp[-1].message = "Getting to the higher ground now. Follow the road to the curves around the Artist point."
        wp += [ FixPos(37.846653, -121.922802) ]
        # 2000 is useful here.
        wp[-1].message = "Turn left from the road, follow the walley to the east. Cross the power lines.."
        wp += [ FixPos(37.860643, -121.890753) ]
        # 1800 ?
        wp[-1].message = "Follow the walley to north east to the village."
        wp += [ FixPos(37.87760, -121.86512) ]
        # 1400
        wp[-1].message = "Follow the walley to the north, meet the power lines there."
        wp += [ FixPos(37.910723, -121.883475) ]
        # Az sem dobry. Tady je jednoduchy se ztratit.
        # 1200
        wp[-1].message = "Do a near u-turn to the right, follow the walley to the south east."
        wp += [ FixPos(37.889890, -121.846577) ]
        wp[-1].message = "That's the walley. Follow it."
        wp += [ FixPos(37.886428, -121.823003) ]
        wp[-1].message = "Still there. Continue."
        # 800?
        wp += [ FixPos(37.893108, -121.798288) ]
        wp[-1].message = "Turn right, follow the walley to the east.."
        # 500?
        wp += [ FixPos(37.869292, -121.749997) ]
        wp[-1].message = "See the powerlines east of you? Get there."
        wp += [ FixPos(37.865885, -121.699033) ]
        wp[-1].message = "Now follow the powerlines to the right. Cross the river."
        wp += [ FixPos(37.817272, -121.656235) ]
        wp[-1].message = "Turn left to heading 60 degrees. There's an airport nearby."

        m.pos_target = FixPos(37.824415, -121.624888)

        return wp


    def intro_hook(m):
        m.game.say(m.voice_boss,
                   """Nice day to fly. Lets fly through the mountains, to get some view. Head south, for a church under the Fossil hill.""")

class LevelWeather(LevelMountains):
    def init(m):
        LevelMountains.init(m)
        m.visibility = 25000
        m.cloud_bottom = 2000

    def flight_hook(m):
        m.visibility -= 100
        v = m.visibility
        if v > 250:
            m.sim.set_sim_var('environment/clouds/layer/visibility-m', str(v))


    def intro_hook(m):
        m.sim.set_sim_var('environment/clouds/layer/coverage', 'overcast')
        m.sim.set_sim_var('environment/clouds/layer/elevation-ft', '2000')
        m.sim.set_sim_var('environment/clouds/layer/thickness-ft', '1700')
        m.flight_hook()

        m.game.say(m.voice_boss,
                   """I heard your flight through the mountains last week was very nice, can we repeat it it today?""")

class LevelNightmare20k(Level):
    # Inspired by https://en.wikipedia.org/wiki/Nightmare_at_20,000_Feet
    # Link to the guys that killed both engines in learjet and crashed?
    def init(m):
        m.pos_start = FixPos(37.937, -122.90)
        m.pos_start.track = 120
        m.pos_start.altitude_ft = 20000

    def intro_hook(m):
        m.game.say(m.voice_boss,
                   """They say this plane can fly up to 25000 feet, and you decided to try.
                      Now that both engines gave up, you regret that decision a bit.
                      But you have enough altitude, and thus lot of options...""")

class LevelFun20k(Level):
    def init(m):
        m.pos_start = FixPos(37.937, -122.90)
        m.pos_start.track = 120
        m.pos_start.altitude_ft = 20000
        m.launcher.aircraft = " --aircraft=ask13"

    def intro_hook(m):
        m.game.say(m.voice_boss,
                   """How did you get here? Well, good question. Can you make it to Doon
                   Village? Easy...""")
        # Doon is way too easy.

class LevelStormRace(Level):
    # https://en.wikipedia.org/wiki/Delta_Air_Lines_Flight_191
    # http://libraryonline.erau.edu/online-full-text/ntsb/aircraft-accident-reports/AAR86-05.pdf
    def init(m):
        m.pos_airport = "KNUQ"
#        m.pos_target = FixPos(37.415981, -122.044373)
        m.pos_target = FixPos(37.458778, -122.112869)
        m.takeoff_ft = 1000

#        wp8 = FixPos(37 + 26.58/60., -122 - 28.74/60.)
        wp8 = FixPos(37.442154, -122.095535)
        wp8.target_size = .6
        wp8.message = "Airport is to your right."
        m.pos_waypoint = [ wp8 ]

    def intro_hook(m):
        m.sim.set_sim_var("environment/realwx/enabled", "false")
        m.sim.set_sim_var("environment/config/enabled", "false")
        # FIXME: ?
        # local-weather-lift-fps seems to work -- with some delay
        #m.sim.set_sim_var("environment/terrain/enabled", "false")
        m.set_wind(20, 0)

    def microburst(m):
        # They encountered:
        # 10 knots headwind.
        # 20 seconds of downflow, 6 to 24 knots. headwind 27 knots.
        # 26 seconds of 40 knots tailwind.
        m.game.say(m.voice_god,
                   """I have bad feeling about this.""")
        m.set_wind(54, -24)
        time.sleep(20)
        m.game.say(m.voice_god,
                   """We are losing speed fast.""")
        m.set_wind(-80, 0)
        time.sleep(20)
        m.game.say(m.voice_god,
                   """It seems to be over.""")
        m.intro_hook()

    def run(m):
        m.game.say(m.voice_boss,
                   """Fly to Mossfet Federal Airfield, short hop north-west of here. There is
                   an thunderstorm approaching, so hurry up.""")

        m.intro_hook()
        m.intro()
        m.takeoff()

        m.game.say(m.voice_god,
                   """You have reached %d feet. Fly at that level until you are lined up with the airport.""" % m.takeoff_ft)
        m.follow_waypoints()

        m.new_phase("trap")
        menu = [ Option("wait", "Wait a moment, something is not right here") ]
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            m.flight_checks()
            m.game.update_pos()
            if m.game.pos.speed_kn < 80 and m.game.pos.altitude_ft < 1200:
                m.microburst()
                break

        m.cruise()
        m.landing()

class LevelLowFlight(Level):
    note = "This is pretty hard. Seneca does not seem to climb at all, and perhaps the best speed with engine failure is not blue line. Probably not realistic with seneca. With lightning, flight in ground effect is possible. Getting out of ground effect is not possible, unless you accelerate at low altitude, anticipating engine failure."

    def init(m):
        m.pos_start = FixPos(37.458778, -122.112869)
        m.pos_start.heading = 315
        m.pos_target = FixPos(37.424164, -122.054596)
        m.takeoff_ft = 200
        # Takeoff runway is too short for citation?
        #m.set_aircraft(CitationX())
        # Very hard :-(
        #m.set_aircraft(Beechcraft())
        # Failed engines produce thrust on 777
        #m.set_aircraft(Boeing777())
        m.set_aircraft(Lightning())
        m.intro_text = "Do the engines sound the same way they did yesterday?"

    def flight_hook(m):
        if m.game.pos.altitude_ft > 100:
            m.fail_engine()

class LevelLowFlight2(Level):
    def init(m):
        m.pos_start = FixPos(37.424164, -122.054596)
        m.pos_start.heading = 315
        m.pos_target = m.places.khaf
        m.takeoff_ft = 200
        # Lightning can climb. Seneca can keep altitude.
        #m.set_aircraft(Lightning())
        m.intro_text = "Do the engines sound the same way they did yesterday?"

    def flight_hook(m):
        if m.game.pos.altitude_ft > 600:
            m.fail_engine()


class LevelUpAndAway(Level):
    def init(m):
        m.pos_start = FixPos(37.458778, -122.112869)
        m.pos_start.heading = 315
        m.pos_target = FixPos(37.424164, -122.054596)
        m.takeoff_ft = 500

    def run(m):
        m.game.say(m.voice_boss,
                   """There are some high winds expected, lets move to hangar at nearby airport.""")

        m.intro()
        m.set_wind(10, 40)
        m.takeoff()

        m.game.say(m.voice_god,
                   """What is going on? Was it supposed to be hurricane today?""" )
        m.set_wind(20, 100)
        time.sleep(30)
        m.game.say(m.voice_god,
                   """It seems to be getting even stronger...""" )
        m.set_wind(30, 150)
        time.sleep(130)

        m.set_wind(30, -5)

        m.follow_waypoints()
        m.cruise()
        m.landing()

class LevelEvilTurbines(Level):
    def init(m):
        m.pos_airport = "KRHV"
        # Big one, close
        m.pos_target = FixPos(37.354332, -121.918716)
        m.takeoff_ft = 500
        m.set_aircraft(Beechcraft())

class LevelFailLanding(Level):
    def init(m):
        m.pos_airport = "KSQL"
        m.pos_target = m.places.khaf
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 5000 BKN010 OVC015"'
        m.takeoff_ft = 1000
        # Very, very hard with beechcraft
        #m.set_aircraft(Beechcraft())
        #m.set_aircraft(CitationX())
        m.intro_text = "We will fly to Half Moon Bay. There are some nice mountains along the way..."
        m.fail_altitude = 500

    # fail at landing
    def flight_hook(m):
        if m.game.pos.altitude_ft < m.fail_altitude and util.distance(m.game.pos, m.pos_target) < 3:
            m.fail_engine()

class LevelBeechFun(LevelFailLanding):
    def init(m):
        LevelFailLanding(m).init()
        m.fail_altitude = 1500

class LevelEvilJets(Level):
    def init(m):
        m.pos_airport = "KRHV"
        m.pos_target = FixPos(37.354332, -121.918716)
        m.takeoff_ft = 500
        m.set_aircraft(CitationX())
        #m.set_aircraft(MiG15())
        m.intro_text = "Ready for very short flight? You probably will not even want to retract flaps..."


class LevelEvilCopters(Level):
    def init(m):
        m.pos_airport = "KRHV"
        m.pos_target = FixPos(37.354332, -121.918716)
        m.takeoff_ft = 500
        m.launcher.aircraft = " --aircraft=bo105"

class LevelFranceHop(Level):
    def init(m):
        # N823GA mishap, gulfstram
        # cruise at 16000
        m.pos_airport = "LFTH"
        # LFMN : quite nice, but far
        m.pos_target = m.places.lfmq
        m.takeoff_ft = 2500
        m.set_aircraft(CitationX())
        # Quite nice with 707. Not sure if realistic.
        #m.set_aircraft(Boeing707())
        #m.set_aircraft(Tu154())
        #m.set_aircraft(Boeing777())
        #m.set_aircraft(MiG15())

        m.alt_scenery()
        m.intro_text = "This should be quite short."

        # /environment/config/aloft/entry/turbulence/magnitude-norm == 100
        # ... a pekne se to klepe.
        # /environment/config/aloft/entry/turbulence/magnitude-norm

class LevelFranceHop2(Level):
    def init(m):
        m.pos_airport = "LFTH"
        m.pos_target = m.places.af_cuers
        m.takeoff_ft = 2500
        m.set_aircraft(Boeing707())

        m.alt_scenery()
        m.intro_text = "This should be quite short."

class LevelFranceBack(Level):
    def init(m):
        m.pos_airport = "LFMQ"
        m.pos_target = m.places.lfth
        m.takeoff_ft = 2500
        m.alt_scenery()
        m.intro_text = "Back in smaller plane.."

        # Fail elevator at 3000ft?

        # Elevator failure -- easy with long runway.
        # Failed ailerons -- well that's hard.

class LevelTest(Level):
    def init(m):
        # N823GA mishap, gulfstram
        # cruise at 16000
        m.pos_airport = "TFFR"
        # December 22: TWA Flight 842, a 707-331 (N18701), crashed on landing at Milan-Malpensa, Italy, no casualties.[1]
        # On March 31, 1992 Trans Air Services 707-321C was damaged beyond repair at Istres, France when the No 3 engine separated in flight, taking with it the No 4 engine. An emergency landing was made but the aircraft was damaged beyone repair by fire.[1][60]

        # Dubrovnik, split
        m.pos_target = m.places.lddu
        m.takeoff_ft = 1500
        #m.set_aircraft(CitationX())
        #m.set_aircraft(CRJ900())
        #m.set_aircraft(CRJ200())
        #m.set_aircraft(Pilatus())
        #m.set_aircraft(Lightning())
        m.set_aircraft(Beechcraft())
        m.alt_scenery()
        m.intro_text = "Fill this space."

    def init_ideas(m):
        # Yandell ranch airport.
        # small airport, in fgfs.
        #m.pos_start = FixPos(37.641843, -121.165213)
        # KRHV
        #m.pos_start = FixPos(37 + 19/60.+47.1/(60.*60), -121 - 48/60. - 58.3/(60.*60))
        #m.pos_start.track = 323
        m.pos_airport = "KRHV"
        # Flying bull airport: Not there?
        #m.pos_target = FixPos(37.6188, -121.1717)
        # Small airport westley
        m.pos_target = FixPos(37 + 33.26/60., -121 - 12.76/60.)
        # Should be very short hop
        m.takeoff_ft = 500

class LevelFoggyDeparture(Level):
    def init(m):
        m.pos_airport = "OIII"

        # Dubrovnik, split
        m.pos_target = m.places.lkpr
        m.takeoff_ft = 1500
        m.set_aircraft(Boeing777())
        #m.set_aircraft(Tu154())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 27030KT 2SM FEW002 OVC004"'
        m.alt_scenery()
        m.intro_text = "Long trip north."

class LevelDevilVacation(Level):
    def init(m):
        m.pos_airport = "KOPF"
        # Dubrovnik, split
        m.pos_target = m.places.lddu
        # To: knqx1
        m.takeoff_ft = 1500
        #m.set_aircraft(Beechcraft())
        m.set_aircraft(Boeing777())
        m.alt_scenery()
        m.intro_text = "He lost his wings."

class LevelIcySpain(Level):
    def init(m):
        # http://www.fomento.es/NR/rdonlyres/B2086E95-45BE-4179-850E-1A7FCA9F5150/142188/2015_005_ENG.pdf
        # LEPP -> LEMD
        # In -> LEPP direction, autoland is possible. Runway is quite short for not-quite-empty airplane. Recommended in IMC weather.
        # Insbruck: LOWI
        m.pos_airport = "LEVT"
        m.pos_target = m.places.lepp
        m.takeoff_ft = 2500
        #m.set_aircraft(Beechcraft())
        #m.set_aircraft(CitationX())
        #m.set_aircraft(Boeing777())
        #m.set_aircraft(Tu154())
        #m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "It can be icy in Spain."

class LevelCrossCrisis(Level):
    def init(m):
        #https://www.youtube.com/watch?v=_wsa3vhnowk&list=PLCC59953860B62145
        #visibility 2nm, ceiling 1000, 20 minutes of fuel.
        #To: Raleigh, North Carolina.
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 2SM 00580G99KT FEW008 OVC012"'
        m.pos_airport = "KIGX"
        m.pos_target = m.places.krdu
        m.takeoff_ft = 2000
        m.alt_scenery()
        m.intro_text = "It can be icy in Spain."

class LevelNonDirectional(Level):
    def init(m):
        m.pos_airport = "pakt"
        m.pos_target = m.places.pajn
        m.takeoff_ft = 1500
        "http://wiki.flightgear.org/index.php?title=Places_to_fly&redirect=no"
        """
Park your aircraft on PAKT, Ketchikan International, runway 11. The wind is coming from South, change the weather if needed (SE is ok too).
Fly North towards NDB Fredericks Point on 372.0 for 94 NM.
Fly towards NDB Five Fingers on 295.0 for 39 NM.
Fly towards NDB Gustavus on 219.0 for 77 NM.
Fly towards NDB Coghlan Island on 212.0 for 32 NM.
Set the ILS on 109.9 and land your aircraft (after 4 NM) on PAJN, Juneau International, runway 08.
"""
        m.set_aircraft(Boeing707())
        m.alt_scenery()
        m.intro_text = "What about some non-directional beacon training? Read the sources!"

class LevelLongShift(Level):
    def init(m):
        # https://dms.ntsb.gov/public/59500-59999/59551/600216.pdf
        m.pos_airport = "KCDR"
        m.pos_target = m.places.krca
        m.takeoff_ft = 4000
        m.set_aircraft(Boeing777())
        m.alt_scenery()
        m.intro_text = "Okay, this is going to be a long day. Fly over Summerset at 10000 feet."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 2.5
        m.pos_waypoint = wp

    def get_waypoints(m):
        wp = []

        wp += [ FixPos(44.221, -103.377) ]
        wp[-1].message = "Turn right, descent to 5500 feet."

        wp += [ FixPos(44.219, -103.206) ]
        wp[-1].message = "Turn to heading 140, prepare to land at Rapid City Regional Airport."
        return wp

class LevelTooLongShift(Level):
    def init(m):
        # https://dms.ntsb.gov/public/59500-59999/59551/600216.pdf
        m.pos_airport = "KRCA"
        m.pos_target = m.places.krap
        m.takeoff_ft = 4000
        #m.set_aircraft(MiG15())
        #m.set_aircraft(Boeing707())
        m.set_aircraft(Boeing777())
        m.alt_scenery()
        m.intro_text = "Hmm. Lets pretend this never happened."

class LevelShort(LevelTrap):
    def init(m):
        # http://www.amusingplanet.com/2013/08/worlds-shortest-commercial-flight-is.html
        # DID YOU KNOW: The world’s shortest commercial flight is in Scotland, which connects Westray to Papa Westray. The flight can be finished in 47 seconds, and it spans a distance of just 1.7 miles. (130kt)

        m.pos_start = m.places.egew
        m.pos_target = m.places.egep
        m.takeoff_ft = 150
        #m.set_aircraft(CitationX())
        #m.set_aircraft(CRJ900())
        #m.set_aircraft(CRJ200())
        #m.set_aircraft(Pilatus())
        #m.set_aircraft(JT5B())
        #m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "Ready for a very short trip?"
        m.init_trap()

class LevelNotCapable(Level):
    def init(m):
        m.pos_airport = "TFFF"
        # https://en.wikipedia.org/wiki/Air_France_Flight_117
        m.pos_target = m.places.tffr
        m.takeoff_ft = 1500
        m.set_aircraft(Boeing707())
        m.alt_scenery()
        m.intro_text = "I did not believe the captain capable of qualifying in the 707."

class LevelCroatia(Level):
    def init(m):
        # N823GA mishap, gulfstram
        # cruise at 16000
        m.pos_airport = "LDSP" # LDRI
        # http://www.planes.cz/cs/article/201644/letecke-stripky-3-dubrovnik
        m.pos_target = m.places.ldsp
        m.takeoff_ft = 1500
        #m.set_aircraft(Boeing777())
        #m.set_aircraft(Lockheed1049h())
        m.set_aircraft(Tu154())
        m.alt_scenery()
        m.intro_text = "Croatia is nice, even from air."

class LevelSaba(Level):
    def init(m):
        m.pos_airport = "TNCS"
        m.pos_target = m.places.tncs
        m.takeoff_ft = 1500
        #m.set_aircraft(Pilatus())
        m.alt_scenery()
        m.intro_text = "Let us do some sight-seeing. Circle the island, then land back here."

class LevelTrijetIntro(Level):
    def init(m):
        m.pos_airport = "LFMN"
        m.pos_target = m.places.lfmq
        m.takeoff_ft = 1500
        #m.set_aircraft(DC10())
        m.set_aircraft(Tu154())
        m.alt_scenery()
        m.intro_text = "Short hop, three-engined aircraft. Wanna play?"

class LevelTrijetIntro2(Level):
    def init(m):
        m.pos_airport = "LFMN"
        m.pos_target = m.places.limj
        m.takeoff_ft = 1500
        #m.set_aircraft(MiG15())
        m.set_aircraft(Tu154())
        #m.set_aircraft(Boeing777())
        m.alt_scenery()
        m.intro_text = "Short hop, three-engined aircraft. Wanna play?"

# Mig 15 LFMN -> Marseille city? Nebo tech asi 5 letist okolo?
# MiG 15, pristani ze severo-zapadu na Aerodrome de Cuers... neni lehky

class LevelFranceMountains(Level):
    def init(m):
        m.pos_airport = "LFTH"
        m.pos_target = m.places.lftf
        m.takeoff_ft = 1500
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Short hop, challenging aircraft aircraft.?"

class LevelLightning(Level):
    def init(m):
        # This actually happened. Lighting stroke and killed all
        # (electronic) static-based systems.
        m.pos_start = m.places.khaf
        m.pos_target = m.places.ksfo
        m.takeoff_ft = 1500
        #m.set_aircraft(Beechcraft())
        m.intro_text = "That lightning was near..."
        # Citation X : even with static failure, vario shows. There's no radar altimeter. Mach speed is displayed even with failed static.

        # Tune nav to 115.80, rwy 1L. Its heading is 14deg.
        # Nebo ILS 19L, 108.9

    def intro_hook(m):
        m.sim.set_sim_var('environment/clouds/layer/visibility-m', '800')
        m.sim.set_sim_var('environment/clouds/layer/coverage', 'overcast')
        m.sim.set_sim_var('environment/clouds/layer/elevation-ft', '600')
        m.sim.set_sim_var('environment/clouds/layer/thickness-ft', '1000')


class LevelBattery(Level):
    def init(m):
        # http://code7700.com/mishap_air_illinois_710.html
        if False:
            # Flat, not really interesting area
            m.pos_airport = "KSPI"
            m.pos_target = m.places.kmdh
            m.takeoff_ft = 1500
        else:
            m.pos_airport = "KTGI"
            m.pos_target = m.places.korf
            m.takeoff_ft = 1500


        # Nicely modelled electrical system, but the aircraft does not appear
        # to use electricity .. at all.
        #m.set_aircraft(Lockheed1049h())

        # Electrical system behaves strange. Autopilot works without electricity
        #m.set_aircraft(Beechcraft())
        # Cessna 172p -- electrical systems can be failed.

        # Seneca: radar altimeter works without electricity. Hmm. This plane nees no electricity...
        # Actually... it looks like it has a battery, just a very big one.
        # systems/electrical/bus/element
        # http://localhost:5501/systems/electrical/bus/element capacity-norm 0.278 jeste nastartujes
        # Limit for cranking is 0.27878
        # controls/anti-ice/{pitot-heat,window-heat,wing-heat}
        # controls/deice/prop-deice
        # controls/electric/engine/genarator
        # controls/electric/engine[1]/genarator

        # Cessna: reasonable simulation... but it needs no electricity.

        # 777: some simulation is present. But it has too many backups and no
        # battery meter.
        #m.set_aircraft(Boeing707())

        m.alt_scenery()
        m.intro_text = "I had problems starting the engines yesterday. Strange."

    def intro_hook(m):
        m.sim.set_sim_var("systems/electrical/bus/element/capacity-norm", "0.295")
        m.sim.set_sim_var("controls/electric/engine/generator", "false")
        m.sim.set_sim_var("controls/electric/engine%5B1%5D/generator", "false")
        m.sim.set_sim_var("controls/deice/prop-deice", "true")
        m.sim.set_sim_var("controls/deice/pitot-heat", "true")
        m.sim.set_sim_var("controls/deice/window-heat", "true")
        m.sim.set_sim_var("controls/deice/wing-heat", "true")


class LevelPaintJob(Level):
    def init(m):
        """May Day: Flying Blind, Cineflix, Episode 4, Season 1, 17 September 2003 (AeroPeru 603)

Peru Ministry of Transport, Aircraft Accident Report, Accident of the Boeing 757-200 Aircraft, Operated by Empressa de Transporte Aereo Del Peru, Off the coast of Lima, 2 October 1996

SPIM -> SCEL.

ATC knows ground speed, but lies about altitude. (constant 9800)"""

        # Happened at night.

        # TNCS is a nice place, but..
        m.pos_airport = "SPIM"
        m.pos_target = m.places.tncs
        m.takeoff_ft = 2000
        m.intro_text = "The aircraft has beatiful new paint. Do not scratch it."
        m.set_aircraft(Boeing777())
        m.alt_scenery()

        # Too long, fuel actually runs out.

    def intro_hook(m):
        # set_time somehow breaks our monitoring :-(.
        #m.set_time(18*60*60)
        pass


    def flight_hook(m):
        if m.game.pos.altitude_ft > 500:
            m.fail_static()


class LevelSmokingLavatory(Level):
    # Skoncil jsem na "Tingo maria" airport. Probably not suitable for boeing?
    # "SPGM". Pekny letiste v kopcich, skoda toho sloupu uprostred runwaye.
    # 2098 meters runway.

    def init(m):
        m.pos_airport = "SPIM"
        m.pos_target = m.places.tncs
        m.takeoff_ft = 500
        m.intro_text = "No smoking in the lavatory."
        m.set_aircraft(Boeing777())
        m.alt_scenery()

    def b60(m, deg, min):
        return deg * 60. + min

    def handle_speech(m, t):
        if t > m.b60(-9, 14):
            m.say_once(m.voice_pilot, "Hmm. Circuit breakers for rear lavatory just tripped.")
        if t > m.b60(0, 0):
            m.say_once(m.voice_pilot, "I smell something funny.")
        if t > m.b60(2, 40):
            m.say_once(m.voice_steward, "Excuse me, there is a fire in the washroom in back, they are just... went back to go to put it out.")
        if t > m.b60(5, 35):
            m.say_once(m.voice_pilot, "Electricity problems just got worse.")
        if t > m.b60(6, 52):
            m.say_once(m.voice_steward, "Captain, the smoke is clearing.")
        if t > m.b60(6, 53):
            m.say_once(m.voice_pilot, "Ok, guess it was just thrashcan.")
        if t > m.b60(8, 12):
            m.say_once(m.voice_pilot, "Mayday, Mayday, Mayday. We have a fire, we need to land as soon as possible.")
        if t > m.b60(20, 9):
            m.say_once(m.voice_pilot, "Hmm. This is where we should really be on the ground.")

    def flight_hook(m):
        m.handle_speech(m.phase_len() - 15*60)

class LevelRoudnice(Level):
    def init(m):
        m.pos_airport = "LKRO"
        m.pos_target = m.places.lkpr
        m.takeoff_ft = 500
        m.alt_scenery()

        """Lets do sightseeing flight over the Rip mountain. You have the controls, take us there."""

        """Take us to Hazmburk mountain"""

        # hazmburk .. 50 + 26/... , 14 + 00/
        # Rip 50 + 23/ 14 + 17
        # letiste nymburk: 50 10 09", 15 03'10"
        # LKKB? 50 7 26, 14 33 05

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 1
        m.pos_waypoint = wp

        """Lets do scenic flight over mountains."""

        """Set your altimeter to 1337 feet."""

        """1.Right after takeoff you turn to a south-eastern course
        (ca. 130 deg), until
        you meet a motorway (the A3) about half a minute later."""


    def get_waypoints(m):
        wp = []
        wp += [ FixPos(50 + 26/60., 14 + 0.) ]
        wp[-1].message = "Ok, now turn East towards Rip"

        wp += [ FixPos(50 + 23/60., 14 + 17./60.) ]
        wp[-1].message = "Well, I'm feeling somehow sleepy. Bring us to some airport somewhere...."
        return wp

class LevelTutorial(Level):
    def init(m):
        m.pos_airport = "LSPV"
        m.pos_target = m.places.lzsr
        m.takeoff_ft = 2000
        m.alt_scenery()
        m.set_aircraft(Cessna())

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 1.5
        m.pos_waypoint = wp

        m.enable_balloons()
        m.intro_text = "Let us do a scenic flight over mountains. Set your altimeter to 1337 feet. Right after takeoff you turn to a south-eastern course -- about 130 degrees, until you meet a motorway -- the A3 -- about half a minute later."

    def get_waypoints(m):
        # Thanks to Jonathan Schellhase

        wp = []
        wp += [ FixPos(47.192772, 8.856797) ] # FIXME
        wp[-1].message = "With a slight left turn to about 110 degrees you follow the A3 to the valley in front of you. According to the mandatory minimum safe altitude we have to maintain an altitude of at least 2000 feet, but I recommend climbing to 3000 feet"

        wp += [ FixPos(47.148390, 9.041153) ]
        wp[-1].message = "When passing through the valley, you will see an airfield to your right. That's Mollis, a former military airfield of the Swiss Air Force. In case of an emergency like an engine fire or something similar this would be an emergency landing field."

        wp += [ FixPos(47.136192, 9.065015) ]
        wp[-1].message = "To your left front there is a lake, the Walensee This is where you now head for."

        wp += [ FixPos(47.134790, 9.102295) ]
        wp[-1].message = "As soon as you are over the western shore of the lake you start a steep climb an turn to a course of 10-20 deg. Climb steeply."

        wp += [ FixPos(47.148077, 9.103227) ]
        wp[-1].message = "In order to pass the mountain in front of you need to reach at least 5500 ft, better 6000. To obtain such a steep ascent you have to set full power (just neglect the possibly overspeeding engine) and maybe apply one step of flaps."

        wp += [ FixPos(47.173505, 9.132270) ]
        wp[-1].message = "After crossing the mountain pass you maintain a heading of 10-20 deg for about 17 miles until you reach the motorway A1 which runs more or less parallel to a railway track. With a cruising speed approximately 140 knots this will take somewhat between five and six minutes. On that leg -- in pilot's language a leg is a straight part of a flight route -- you can descent to 4000 to 4500 feet."

        wp += [ FixPos(47.417320, 9.223072) ]
        wp[-1].message = "Now you turn eastwards to a heading of about 100 deg to follow the A1."

        wp += [ FixPos(47.446225, 9.397825) ]
        wp[-1].message = "When flying over the city of Saint Gallen the A1 makes a long drawn curve to the left (northwards). At some pint the shore of the Bodensee or Lake of Constance will become visible."

        wp += [ FixPos(47.468540, 9.470932) ]
        wp[-1].message = "As soon as you leave St. Gallen behind, the A1 makes a relative sharp turn to the east. Now you don't continue following the A1, but maintain heading and start descending to 3000 feet. Now you should also be able to spot you destination airport to the right front of you. As soon as you line up with the runway -- presumably quite accurate above the shore -- you turn right and start the approach. The rest -- final approach and landing -- shouldn't be new for you so you can do this on your own. In an unlikely event of go around, climb to 2500 feet, and make a right-hand circuit pattern to 190 degrees. Beware of the hills to the south of the airport."

        return wp

class LevelHighest(Level):
    # Vyletet na nejvyssi horu v okoli, tam engine failure.
    # Docela sila, i se senecou.
    #
    def init(m):
        m.pos_airport = "LSMF"
        m.pos_target = m.places.lsmf
        m.takeoff_ft = 2000
        m.alt_scenery()

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 1
        m.pos_waypoint = wp

        m.intro_text = "Take me to the highest mountain."

    def get_waypoints(m):
        wp = []
        wp += [ FixPos(46.924505, 9.255180) ]
        wp[-1].action = lambda: m.fail_engine()
        return wp


class LevelTest3(Level):
    def init(m):
        m.pos_airport = "LSZC"
        m.pos_target = m.places.lzsr
        m.takeoff_ft = 2000
        m.alt_scenery()
        m.set_aircraft(B1b())

class LevelSupersonic(Level):
    def init(m):
        m.pos_airport = "EGWU"
        m.pos_target = m.places.lzsr
        m.takeoff_ft = 2000
        m.alt_scenery()
        #m.set_aircraft(B1b())
        #m.set_aircraft(B36())
        m.set_aircraft(BoeingB52())

class LevelPower(Level):
    def init(m):
        # FIXME
        m.pos_airport = "LSZC"
        m.pos_target = m.places.lzsr
        m.takeoff_ft = 2000
        m.pos_target = FixPos(47.079313, 9.065023)
        # Cessna() -- it is possible to control power with mixture
        # CitationX() -- lot of fun, it is impossible to shutdown the engine
        # VelocityXL() -- bad landing detection, otherwise quite fun (but power can be controlled with mixture)
        # Caravan() -- bad landing detection, no way to cut fuel, behaves strangely
        # ShootingStar -- no way to cut fuel. Interesting. Not realistic.
        #m.set_aircraft(ShootingStar())
        m.set_aircraft(Cessna())
        #m.set_aircraft(Cessna182())
        m.alt_scenery()
        m.intro_text = "They say Cessna has not enough power, but I disagree."

    def flight_hook(m):
        if time.time() - m.phase_start > 30:
            m.sim.set_sim_var("controls/engines/engine/throttle", "1")

class LevelHell(Level):
    def init(m):
        m.pos_airport = "KGIC"
        m.pos_target = m.places._25u
        m.takeoff_ft = 2000
        m.alt_scenery()
        m.intro_text = "How does visit to Hells Canyon sound? Runway is quite short, in north-south direction, and views should be nicer if we approach from the south."

    def flight_hook(m):
        if util.distance(m.game.pos, m.pos_target) < 8:
            m.fail_vacuum()

class LevelDanger7(Level):
    # 7th most dangerous airport
    # Easy with seneca.
    def init(m):
        m.pos_airport = "LFKA"
        m.pos_target = Airport("LFLJ")
        m.takeoff_ft = 2000
        m.alt_scenery()

        "Going skiing? skying?"

class LevelDanger1(Level):
    # 1st most dangerous airport
    # Reasonably looking.
    # Challenging for big aircraft?

    # 4th most dangerous to most dangerous :-)
    # Long flight.

    def init(m):
        m.pos_start = m.places.vlnk
        m.pos_target = m.places.vqpr
        m.takeoff_ft = 10000
        m.alt_scenery()
        m.intro_text = "We are flying from Mount Everest to the worlds most dangerous airport. But do not worry, it is only dangerous for big aircraft. You will need to lean the engines to even start it."

class LevelRealDanger1(LevelDanger1):
    # It is possible to make it to Janakpur (VNJP).
    def cruise_hook(m):
        m.pos_target = m.places.vnjp
        m.fail_engine()

class LevelVostok(Level):
    def init(m):
        m.pos_start = m.places.baikonur
        m.pos_start.track = 55
        m.takeoff_ft = 10000
        m.alt_scenery()
        m.set_aircraft(Vostok())
        m.space = False
        m.intro_text = "Do you want to be the first one in space?"

    def flight_checks(m):
        if m.game.pos.altitude > 1000:
            m.say_once(m.voice_pilot, "kilo meter")
        if m.game.pos.altitude > 3000:
            m.say_once(m.voice_pilot, "30 degrees down")
        if m.game.pos.altitude > 6000:
            m.say_once(m.voice_pilot, "wind loading")
        if m.game.pos.altitude > 10000:
            m.say_once(m.voice_pilot, "10 kilo meters")
        if m.game.pos.altitude > 35000:
            m.say_once(m.voice_pilot, "check g force")
        if m.game.pos.altitude > 49000:
            m.say_once(m.voice_pilot, "49 kilo meters")
        if m.game.pos.altitude > 100000 and not m.space:
            m.game.say(m.voice_pilot, "space")
            m.space = True
        if m.game.pos.altitude < 100000 and m.space:
            m.game.say(m.voice_pilot, "atmosphere")
            m.space = False
        acc = m.sim.get_sim_float("accelerations/pilot/z-accel-fps_sec")
        acc /= 32.26900156
        if acc > 3.8:
            m.game.say(m.voice_pilot, "g force")
        spd = m.sim.get_sim_float("velocities/vertical-speed-fps")
        spd *= 0.3048
        if spd > 500:
            m.say_once(m.voice_pilot, "vertical speed high")

    def run(m):
        m.game.say(m.voice_boss, m.intro_text)

        m.new_phase("Takeoff")
        menu = [ Option("wait", "Wait a moment, something is not right here") ]
        m.print_question(menu)
        while True:
            answer = m.get_answer(menu)
            if answer:
                if answer == "wait":
                    m.end_game("""Everything is just fine.""")
                    return
            m.flight_checks()
            if m.game.pos.speed > 27000:
                break

        m.game.say(m.voice_god,
                   """You have reached orbital speed""")

class LevelJetMountains(LevelTrap):
    def init(m):
        m.pos_airport = "EDDM"
        m.pos_target = m.places.lowi
        m.alt_scenery()
        #m.set_aircraft(Boeing707())
        m.set_aircraft(A320())
        m.intro_text = "Lets take these people skiing..."
        m.init_trap()

class LevelTestMountains(Level):
    def init(m):
        m.pos_airport = "EDDM"
        m.pos_target = m.places.lowi
        m.alt_scenery()
        m.set_aircraft(Beechcraft())
        m.intro_text = "Lets take these people skiing..."

class LevelFrozen(Level):
    def init(m):
        """
        http://code7700.com/mishap_helios_522.html
        geo:37.6685,24.4837?z=14  .....
        8:45:50 left engine flameout, fl340
        8:54:18 Mayday Mayday Mayday Helios airways flight 522
        8:59:45 There is F-16 nearby. How nice.
        8:59:47 right engine flameout
        https://www.youtube.com/watch?v=Go3K0UUt2Us
        """
        m.pos_start = FixPos(37.6685, 24.4837)
        m.pos_start.altitude_ft = 34000
        m.pos_start.track = 110
        # NATO air base
        m.pos_target = FixPos(37.929981, 23.939552)

        # left = 0, right = .09

        m.alt_scenery()
        m.set_aircraft(Boeing777())
        m.intro_text = "Its cold up here..."

    def intro_hook(m):
        m.set_fuel("0", "500")

    def run(m):
        m.intro_hook()
        m.cruise()
        m.landing_survive()

class LevelGroundContact(Level):
    def init(m):
        # http://code7700.com/mishap_crossair_3597.html
        m.pos_airport = "EDNY"
        m.pos_target = m.places.lszh
        m.alt_scenery()
        #m.set_aircraft(Beechcraft())
        # Rwy 28 approach
        m.intro_text = "The weather is not quite good, lets fly under instrument rules. Please see sources for maps and instructions."

        # http://code7700.com/images/lszh_vor_dme_aaib_1793_appendix_8_rwy_28_page_1.png

        wp = m.get_waypoints()
        for w in wp:
            if w.target_size < 0:
                w.target_size = 1
        m.pos_waypoint = wp

    def get_waypoints(m):
        wp = []
        wp += [ FixPos(47.683040, 9.552190) ]
        wp[-1].target_size = 2
        wp[-1].message = "Turn south after takeoff. Intercept 89th radial of ZUE beacon 110.05, fly west. Climb to 9000 feet."

        wp += [ FixPos(47.616967, 9.152268) ]
        wp[-1].message = "You should be at 9000 feet by now, 10 miles to the beacon. You will be turning left to follow 178 degree radial at the beacon."

        wp += [ FixPos(47.591308, 8.815747) ]
        wp[-1].message = "After reaching ZUE beacon, follow 178 degree radial for about 6.3 miles, descending to 5000 feet. Airport beacon is 114.85, when you reach its 81th radial, descend to 4000 feet and turn right to heading 275 degrees."

        wp += [ FixPos(47.485378, 8.826035) ]
        wp[-1].message = "You have reached the radial. Descend to 4000 feet and turn right to heading 275 degrees. Follow the 275 radial to the airport. You should be at 4000 feet at 6 miles from the airport."

        wp += [ FixPos(47.448597, 8.691822) ]
        wp[-1].message = "At DME 6 miles, begin descent to 3360 feet."

        wp += [ FixPos(47.452342, 8.644387) ]
        wp[-1].message = "At DME 4 miles, begin descent to decision height of 2390 feet. Then land at runway 28, elevation 1416 feet."

        return wp

class LevelImpossible(Level):
    def init(m):
        # http://code7700.com/approach_impossible.html
        m.pos_airport = "TVSB"
        # Runway 7 NDB approach
        m.pos_target = m.places.tvsv
        m.alt_scenery()
        #m.set_aircraft(Beechcraft())
        # Rwy 28 approach
        m.intro_text = "The weather is not quite good."

        """After takeoff, head south. Tune NDB joshua, 403 SV"""

class LevelTail(Level):
    def init(m):
        m.pos_airport = "LPMA"
        m.pos_target = m.places.lppt

        # Wow. Possible. Quite interesting. ... for basically random choice.
        # With 777 -- Has problems -- engines still provide power after failure
        m.takeoff_ft = 1500
        m.set_aircraft(CitationX())
        m.alt_scenery()
        m.intro_text = "Nice airport, right?"

    def flight_hook(m):
        if m.game.pos.altitude_ft > 13782:
            m.fail_engine()

class LevelEvening(Level):
    def init(m):
        m.pos_airport = "RJTT"
        m.pos_target = m.places.rjaa

        # Wow. Possible. Quite interesting. ... for basically random choice.
        # Has problems -- engines still provide power after failure
        m.takeoff_ft = 1500
        m.set_aircraft(Boeing777())
        m.alt_scenery()
        m.intro_text = "Nice airport, right?"

class LevelWindyMountains(Level):
    def init(m):
        # https://reports.aviation-safety.net/2014/20140105-1_CL60_N115WF.pdf
        m.pos_airport = "KEGE"
        m.pos_target = m.places.kase
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 1SM 31009G28KT FEW112 BKN123 M11/M20 -HZ"'
        m.takeoff_ft = 8000
        #m.set_aircraft(MiG15())
        m.set_aircraft(CitationX())
        #m.set_aircraft(Boeing777())
        #m.set_aircraft(Seneca())
        # CitationX -- pretty hard
        # Actually easier with 777 :-)
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "You are pretty new to this aircraft, right?"

        "After takeoff, turn left to heading 246, and climb to 14000 feet"
        "Tune SNOW VOR 'SXW' at 109.2. Follow its 246 radial. It is less than 6 NM."
        "Upon reaching the VOR, turn south, follow its 196 degree radial. It is less than 6NM."
        "Tune RED TABLE VOR 'DBL', 113.0. When you reach its 117th radial at 6NM DME. follow it. You should be 10 miles away from it."
        "In 3 miles, you should reach ASE localizer. Tune localizer ASE at 111.15. Follow its 151 degree radial."
        "At 13.1 DME, you should be no less than 13400 feet, then you can descent to 12900."
        "At 10.9 DME, you can descent to 12300 feet."
        "At 9 DME, you can descent to 11700 feet."

        "Localizer is at 111.15"

class LevelKeepingInAir(Level):
    def init(m):
        # Inspired by https://en.wikipedia.org/wiki/Pan_Am_Flight_843
        m.pos_airport = "KSFO"

        m.pos_target = m.places.phnl
        m.takeoff_ft = 500
        m.set_aircraft(Boeing707())
        m.alt_scenery()
        m.intro_text = "Flying over ocean should be nice."
        # Ouch. This is pretty hard even without engine failure.
        m.failed = True

    def intro_hook(m):
        m.sim.set_sim_var("consumables/fuel/tank/level-lbs", "2348")
        m.sim.set_sim_var("consumables/fuel/tank%5B1%5D/level-lbs", "12432")
        m.sim.set_sim_var("consumables/fuel/tank%5B2%5D/level-lbs", "21764")
        m.sim.set_sim_var("consumables/fuel/tank%5B3%5D/level-lbs", "54560")
        m.sim.set_sim_var("consumables/fuel/tank%5B4%5D/level-lbs", "21764")
        m.sim.set_sim_var("consumables/fuel/tank%5B5%5D/level-lbs", "12433")
        m.sim.set_sim_var("consumables/fuel/tank%5B6%5D/level-lbs", "2348")
        m.sim.set_sim_var("consumables/fuel/tank%5B7%5D/level-lbs", "0")

        m.sim.set_sim_var("payload/weight%5B2%5D/weight-lb", "9940")
        m.sim.set_sim_var("payload/weight%5B3%5D/weight-lb", "13000")
        m.sim.set_sim_var("payload/weight%5B5%5D/weight-lb", "10000")
        m.sim.set_sim_var("payload/weight%5B6%5D/weight-lb", "10000")

    def flight_hook(m):
        if not m.failed and  m.game.pos.altitude_ft > 800:
            #m.sim.set_sim_var("engines/engine%5B3%5D/on-fire", "true")
            # This should set engine on fire:
            m.sim.set_sim_var("b707/generator/gen-drive%5B3%5D", "false")
            time.sleep(1)
            m.sim.set_sim_var("b707/generator/gen-drive%5B3%5D", "true")
            m.sim.set_sim_var("sim/failure-manager/controls/flight/flaps/serviceable", "false")
            m.sim.set_sim_var("sim/failure-manager/engines/engine%5B3%5D/serviceable", "false")

            m.game.say(m.voice_pilot, "I do not know whether I can keep it in the air or not.")
            m.failed = True

class LevelThinAir(Level):
    def init(m):
        # http://wiki.flightgear.org/index.php?title=Places_to_fly&redirect=no
        # Copacabana
        m.pos_airport = "SLCC"
        m.pos_target = m.places.sprf
        m.takeoff_ft = 14000
        # Too high for Cessna. Basically does not climb.
        m.set_aircraft(Lightning())
        m.alt_scenery()
        m.intro_text = "We are quite high."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 2.5
        m.pos_waypoint = wp

        "After taking off, fly heading 311. Follow 115.5, radial 311. Tune NAV to 113.7, and capture radial of 212 after 100 or so miles of flight or so... Airport is at the radial, at DME 140 miles."

    def get_waypoints(m):
        wp = []

#        wp += [ FixPos(44.221, -103.377) ]
#        wp[-1].message = "Turn right, descent to 5500 feet."
        return wp

class LevelAmazon(Level):
    def init(m):
        # http://wiki.flightgear.org/index.php?title=Places_to_fly&redirect=no
        # Amazon river origin
        m.pos_airport = "SPRF"
        m.pos_target = m.places.spqu
        m.takeoff_ft = 16000
        # Too high for Cessna. Basically does not climb.
        # Too high for Seneca, too? -- It is possible to takeoff in east direction.
        # Cilove letiste je spis na boeinga...
        #m.set_aircraft(Lightning())
        m.alt_scenery()
        m.intro_text = "We are still quite high. See sources."

        "Set NAV1 to Arequipa VOR-DME at 113.7 with a radial of 176 (magnetic). Set NAV2 to Cusco VOR-DME at 114.9 also with a radial of 176. Set QNH and during flight keep correcting it, it's a bad idea to use Pressure altitude during this flight. Set the heading bug to 250. Arm the autopilot and set the initial altitude to 16000 feet."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 2.5
        m.pos_waypoint = wp

        "Fly course 250"

    def get_waypoints(m):
        wp = []

#        wp += [ FixPos(44.221, -103.377) ]
#        wp[-1].message = "Turn right, descent to 5500 feet."
        return wp

class LevelDateLine(Level):
    def init(m):
        # http://cestovani.idnes.cz/beringova-uzina-cesky-pilot-sam-nad-aljaskou-f3z-/kolem-sveta.aspx?c=A170605_081546_kolem-sveta_job
        m.pos_airport = "PAHV" #PATK : > 100km
        m.pos_target = m.places.ak06
        # Too long trip for seneca
        m.takeoff_ft = 1800
        #m.set_aircraft(Lightning())
        m.set_aircraft(Viggen())
        m.alt_scenery()
        m.intro_text = "Ready to visit date line?."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 2.5
        m.pos_waypoint = wp

        "Fly course 250"

    def get_waypoints(m):
        wp = []

#        wp += [ FixPos(44.221, -103.377) ]
#        wp[-1].message = "Turn right, descent to 5500 feet."
        return wp

class LevelAnotherWorld(Level):
    def init(m):
        # http://www.planes.cz/cs/article/201735/letecke-stripky-8-prg-dam-kwi
        m.pos_airport = "LOWI"
        m.pos_target = m.places.osdi
        m.takeoff_ft = 2500
        #m.set_aircraft(Boeing777())
        # Can be done with wiggen, with 14% fuel remaining (w/o drop tank at the begining)
        m.set_aircraft(Viggen())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 27030KT 2SM FEW002 OVC004"'
        m.alt_scenery()
        m.intro_text = "Long trip south."

class LevelFullAfterburner(Level):
    def init(m):
        m.pos_airport = "LSMM"
        m.pos_target = m.places.lowi
        m.takeoff_ft = 2500
        m.set_aircraft(Viggen())
        # Can be done in 16:03 stop-to-stop. Hmm. 16:40 when I tried harder. 16:25 with 14g landing.
        # 13:31. 11:48, but did not manage to stop on runway..

        #m.set_aircraft(MiG15())
        # Hmm. Don't accelerate time if you want to compare speeds.
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 27030KT 2SM FEW002 OVC004"'
        m.alt_scenery()
        m.intro_text = "Quick trip to Austria."

class LevelLost(Level):
    def init(m):
        m.pos_start = FixPos(random.random()*35+30, random.random()*25)
        #m.pos_start = FixPos(62.9502, 1.966553)
        # Actually survivable from 25000 with gps,, go to Floro airport at 61.58189, 5.014229
        m.pos_start.altitude_ft = 15000
        m.pos_target = m.places.lowi
        # Not suitable: Canopy gets torn off, can not autostart in the air.
        m.set_aircraft(MiG15())
        #m.set_aircraft(Ask13())
        # Hmm. Not easy to start in the air.
        #m.set_aircraft(Boeing707())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 10SM FEW010"'
        m.alt_scenery()

        m.intro_text = "You are lost, are you?"

class LevelJetTraining(Level):
    # Not easy at all
    def init(m):
        m.pos_airport = "LSMM"
        m.pos_target = m.places.lszb
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        #m.set_aircraft(Boeing777())
        #m.set_aircraft(Tu154())
        m.alt_scenery()
        m.intro_text = "This is not easy plane to fly. But flight over lakes is beatiful."

        # lsmm, lszb, lszw are suitable for landing. Airport near Bad Durrheim is nice, too.

class LevelJetTraining2(Level):
    def init(m):
        m.pos_airport = "LSMD"
        m.pos_target = m.places.lszh
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        #m.set_aircraft(Boeing707())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 27030KT 2SM FEW002 OVC004"'
        m.alt_scenery()
        m.intro_text = "This is not easy plane to fly. Let us try short hops with long runways."

class LevelAgent(Level):
    # Not easy at all
    def init_agent(m, airports):
        # LSML -- mozne i bez ubrani paliva, ale je to na metry.

        # Aeroporto di Calcinate del Pesce "Paolo Contri" -- pravdepodobne mozne. -- 45.809509, 8.770308
        # 200m (!)
        m.pos_airport = random.choice( airports )
        #m.pos_target = random.choice( [ m.places.lszb, m.places.lszh, m.places.lszs ] )
        m.pos_target = Airport(random.choice(airports))
        print("Flying ", m.pos_airport, "to", m.pos_target.name)
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        #m.set_aircraft(JT5B())
        #m.set_aircraft(Chinook())
        #m.set_aircraft(F16())
        m.alt_scenery()
        m.intro_text = "Somewhere in Switzerland, there is dangerous fuzz bomb on the loose."

        # lsmm, lszb, lszw are suitable for landing. Airport near Bad Durrheim is nice, too.

    # Letiste pobliz alpnachu je pekne... a jsou tam tri.
    # Ambri airport taky nevypada spatne.
    # Letiste u Maienfeld-u ... uslo.
    # Aeroport de Sion -- celkem dlouhe, ale kolem celkem hory.
    # Da se doletet na letiste Pardubice
    # Da se doletet na Gagliary a pak jeste 40 km
    # Lodrino air base -- tak akorat -- LSML
    # St Stephan a pak Zweisin... kousek severne. Obe tezky.
    # letiste u Gundelfigen -- plo a kratke.
    # Bolzano -- trochu dlouhe letiste. Zato pekne vymodelovane.
    # Da se doletet do piestan -- 726 km, jen nedelat chyby. Hmm. A melo by to jit i dal.
    # Da se doletet do sarajeva -- 788 km, zbyva 130lb paliva. -> melo by jit 832km
    # Hamburg je 833km. Pristal jsem s 75lb left. (a teda ponicenej podvozek).
    # Zere to min kdyz se nezrychluje cas, da se doletet az na Ostersund, kolem 1926km. Melo by jit kolem 1926+150+70
    # Marakesh je 2172.
    # Khouribga -- palivo by akorat tak vystacilo, ale letiste neni ve fgfs!
    # Rixheim -- da se pristat, tak akorat...

class LevelAgentCSA(LevelAgent):
    # LFHZ: melo by byt tezke ale mozne.
    def init(m):
        airports = [ "LSMM", "LSZB", "LSZW",
                     "LSTS", "LSGK", "LFSM",
                     "LSGC", "LILE", "LIDT", # Pretty hard? Limit fuel
                     "LSZG", # Hard even with limited fuel
                     "LIMW", # Exactly right size?
                     "LFLI", "LFHO",
                     "LSZS", # Easy, nice model
                     "LSZA", # pekne a bez podrazu.
                     "LILN", # spatne videt - k nalezeni, ale lehky

                     # Autogenerated -- near LSPM and >1.2 km
                     "LSPM", "LSMA", "LSMF", "LSME", "LILN",
                     "LSGK", "LSZR", "EDTD", "EDSN", "EDTF", "LFSM", "LFLI",
                     "LFGA", # 204 km from LSPM.
                     "LSGS", # Long rwy but interesting mountains around
                     "LSZC", "LSMD",
                     
                     "LIDA"  # New additions 11/2021
        ]
        m.init_agent(airports)

class LevelAgentHard(LevelAgent):
    def init(m):
        airports = ["LSZW", "LSGR", "LSZO", "LSPG", "LSPU", "LSZN", # -- even takeoff is interesting, landing is possible with minimal fuel
                    "LSMD",
                    "LILC", "LSZL", "LSML", "LILB", "LSZE", "LILO" # -- unrealistic in mountainside.
        ]
        m.init_agent(airports)

class LevelAgentCessna(Level):
    # Not easy at all
    def init(m):
        # "Paolo Contri" -- LILC -- na Cessnu akorat.
        #m.pos_airport = "LSZA"
        #m.pos_target = Airport( "LSMO" )
        #m.pos_airport = "LSGE"
        #m.pos_target = Airport( "LSGT" )

        airports = [ "LSZG", # 3200 ft
                     "LSPL", # 3000 ft
                     "LSGC", # 3700 ft
                     "LSGN", # 2200 ft
                     "LSPN", # 1400 ft
                     "LSGE", # 2600 ft
                     "LFGB", # 3200 ft
                     "LSGR", # 3000 ft
                     "LSZF", # 2100 ft
                     "LSTZ", # 2700 ft
                     "LSPG",
                     "LSZN", # 1800 ft
                     "LFSP", # 3200 ft
                     "LSGL", # 3000
                     "LSPU", # 3200 ft
                     "LSPV", # 1600 ft
                     "LSZX", # 1700 ft
                     "LFGY", # 2800 ft
                     "EDTS", # 2000 ft
                     "LSGT", "LSPN", "LSZA"
        ]
        m.pos_airport = random.choice( airports )
        m.pos_target = Airport(random.choice(airports))
        
        print("Flying ", m.pos_airport, "to", m.pos_target.name)
        m.takeoff_ft = 2500
        m.set_aircraft(Cessna())
        #m.set_aircraft(Cessna182())
        #m.set_aircraft(Pilatus())
        m.alt_scenery()
        m.intro_text = "Short hop in high mountains."

        # Lodrino -> Ascona?
        # LSMC -> LSPU
        # LSGE -> LSGT -> Saanen

class LevelJetShort(Level):
    def init(m):
        m.pos_airport = "LSZW"
        m.pos_target = m.places.lszb
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "This is not easy plane to fly. Takeoff at 250 kilometers per hour."


class LevelJetSurprise(Level):
    def init(m):
        m.pos_airport = "LSZL"
        m.pos_target = m.places.lszs
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 27030KT 2SM FEW002 OVC004"'
        m.alt_scenery()
        m.intro_text = "This is not easy plane to fly. Takeoff at 250 kilometers per hour."

class LevelJetLeak(Level):
    def init(m):
        m.pos_airport = "LQMO"
        m.pos_target = m.places.air_bari
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "We will fly to Messina today, around heading 190 degrees. It will be rather long flight."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 10
        m.pos_waypoint = wp

        # FOggia: deadstick landing, w/o damage

    def get_waypoints(m):
        wp  = [ FixPos(41.82, 17.20) ]
        wp[-1].message = "Lets cross the sea"
        return wp

    def flight_hook(m):
        def steal_fuel(n):
            m.sim.set_sim_var(tank, str(fuel-n))
        d = util.distance(m.game.pos, m.pos_target)
        tank = "consumables/fuel/tank%5B1%5D/level-lbs"
        fuel = m.sim.get_sim_float(tank)
        print(d, " km ", fuel, "lb")
        if d < 150 and fuel > 220:
            steal_fuel(20)
        if d < 100 and fuel > 140:
            steal_fuel(10)
        if d < 1 and fuel > 20:
            steal_fuel(1)
        pass

    def run(m):
        m.game.say(m.voice_boss, m.intro_text)

        m.intro_hook()
        m.intro()
        m.set_fuel("20", "2000")
        m.takeoff()

        m.game.say(m.voice_god,
                   """Climb to 13 kilometers.""")
        m.cruise_hook()

        m.follow_waypoints()
        m.game.say(m.voice_god,
                   """Watch the fuel. We must have leak somewhere. Let us divert to Bari.""")
        m.cruise()
        m.landing()

class LevelJetEscape(Level):
    # https://technet.idnes.cz/mig-15-mig-15bis-lim-2-franciszek-jarecki-bornholm-fq7-/vojenstvi.aspx?c=A180110_142722_vojenstvi_erp
    def init(m):
        m.pos_airport = "EPSK"
        m.pos_target = m.places.ekrn
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "You should fly to Szczecin and back. But..."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 3
        m.pos_waypoint = wp

    def intro_hook(m):
        m.set_fuel("20", "1000")
        # Doable with 30lb left.


    def get_waypoints(m):
        wp  = [ FixPos(53.654, 14.567) ]
        wp[-1].message = "Now turn back to your home base"

        wp += [ FixPos(53.988, 15.238) ]
        wp[-1].message = "Now turn north. Living in communist country ceased to be fun few years ago. There should be airport on Bornholm island, 100 kilometers north of here."

        wp += [ FixPos(54.942, 15.067) ]
        wp[-1].message = "You should see the island now. Rumors say there is big airport here."

        return wp

class LevelGearTrap(Level):
    def init(m):
        m.pos_airport = "LKCV"
        m.pos_target = m.places.lkln
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "There were some problems in last few days, but we checked your aircraft, and it is safe."

    def run(m):
        m.game.say(m.voice_boss, m.intro_text)

        m.intro_hook()
        m.intro()
        m.takeoff()

        m.game.say(m.voice_god,
                   """You have reached %d feet. Fly V F R, descent when appropriate.""" % m.takeoff_ft)
        m.cruise_hook()

        m.follow_waypoints()
        m.cruise()
        m.landing()

class LevelRaceTrack(Level):
    def init(m):
        # https://reports.aviation-safety.net/2013/20130805-0_BE20_TF-MYX.pdf
        m.pos_airport = "BIHN"
        m.pos_target = m.places.biar
        m.takeoff_ft = 700
        #m.set_aircraft(Beechcraft())
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "Lets see the racetrack."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = .5
        m.pos_waypoint = wp

    def get_waypoints(m):
        wp  = [ FixPos(65 + 40/60. + 30.624/3600., -18 -9/60. - 8.687/3600.) ]
        wp[-1].message = "Nice race. Now fly to the airport"
        return wp

class LevelJetHigh(Level):
    # Not finished -- too far
    def init(m):
        # vnbp ... invisible in flightgear?
        # verl ... not known to fgfs.
        # vnpk works
        # zurk -- not in fgfs
        m.pos_airport = "zwww"
        m.pos_target = m.places.vnkt
        m.takeoff_ft = 5000
        m.set_aircraft(MiG15())
        #m.launcher.weather = '"--metar=METAR XXXX 012345Z 27030KT 2SM FEW002 OVC004"'
        m.alt_scenery()
        m.intro_text = "This is not easy plane to fly. Takeoff at 250 kilometers per hour."

class LevelLocked(Level):
    def init(m):
        # http://code7700.com/accident_giii_n303ga.htm
        m.pos_airport = "BIHU"
        m.pos_target = m.places.biar
        m.takeoff_ft = 700
        #m.set_aircraft(Cessna())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "There is something funny going on with the throttles."
        # Fail elevator/ airleons

class LevelTatry(Level):
    def init(m):
        # EPNT: not visible on ground
        m.pos_airport = "EPKK"
        m.pos_target = m.places.lztt
        m.takeoff_ft = 900
        m.set_aircraft(Spin())
        # set fuel to 47.56-3.71 pounds
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "Lets fly over Tatry."
        # Fail elevator/ airleons

class LevelTalesLondon(Level):
    def init(m):
        # https://www.youtube.com/watch?v=9NFq2eZSDJE
        # Flight management provides completely crazy way, not flyable.
        m.pos_airport = "EHAM"
        # Flying to rwy27 has advantage of pre-set ILS... and "nice" surprise if you try to autoland
        m.pos_target = m.places.egll
        m.takeoff_ft = 1500
        #m.set_aircraft(Boeing777())
        m.set_aircraft(Tu154())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "This will be short hop. Plan for heathrow 0 9 left."

class LevelTalesShort(Level):
    def init(m):
        # https://www.youtube.com/watch?v=7GZ3fSDFevE
        m.pos_airport = "TQPF"
        m.pos_target = m.places.tncm
        m.takeoff_ft = 1500
        m.set_aircraft(Boeing777())
        #m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "There are tourists, buildings and trees on final."

class LevelTalesMountains(Level):
    def init(m):
        # https://www.youtube.com/watch?v=Uh4MYnxubqA
        # A\ Routine\ Landing\ Instantly\ Turns\ into\ a\ Disaster\ \(With\ Real\ Video\)-k4WvPPukoAA
        # Would be beatiful ... destination airport was not invisible in old scenery ;-)
        m.pos_airport = "SEQM"
        m.pos_target = m.places.seqm
        m.takeoff_ft = 1500
        #m.set_aircraft(Boeing777())
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "Destination is quite high."

class LevelTalesLong(Level):
    def init(m):
        # https://www.youtube.com/watch?v=fwqEe0XkggI
        m.pos_airport = "EHAM"
        m.pos_target = m.places.kjfk
        m.takeoff_ft = 1500
        # Trying max payload, 44% fuel -- no chance.
        m.set_aircraft(Boeing777())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "This will be rather long flight."

class LevelQuietAlps(Level):
    def init(m):
        # Start at 47.5N, 8.5, 15999ft, 200kt
        #m.pos_start = FixPos(47.5, 8.5) doable, winnable
        #m.pos_start.altitude_ft = 7999
        m.pos_start = FixPos(47.5, 11.5)
        m.pos_start.altitude_ft = 7999
        m.pos_target = m.places.kjfk
        m.set_aircraft(Boeing707())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        m.alt_scenery()
        m.intro_text = "This will be rather short flight."

class LevelLongest777(Level):
    def init(m):

        m.pos_airport = "OMDB" # "OHTT"
        m.pos_target = m.places.nzaa
        m.takeoff_ft = 1500
        # Trying max payload, 44% fuel -- no chance.
        m.set_aircraft(Boeing777())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT048 OVC062"'
        # https://wayback.archive-it.org/all/20120906032711/http://mswia.datacenter-poland.pl/FinalReportTu-154M.pdf
        m.alt_scenery()
        m.intro_text = "This will be rather long flight."

class LevelJetLong(Level):
    def init(m):
        m.pos_airport = "LKKB"
        m.pos_target = m.places.lkkb
        m.takeoff_ft = 2500
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 13510KT 1SM OVC010 FEW100"'
        m.alt_scenery()
        m.intro_text = "This is not easy plane to fly. "
        # It is possible to travel to Lvov / Lviv, 11% fuel left.
        # Saint-Exupery airport, Lyon, is quite challenging.

class LevelFlock(Level):
    def init(m):
        # https://reports.aviation-safety.net/2008/20081110-0_B738_EI-DYG.pdf
        #m.pos_airport = "EDFH"
        m.pos_airport = "LIRE" # LIRU runway is too short for reasonable takeoff.
        m.pos_target = m.places.lira
        m.takeoff_ft = 2500
        m.set_aircraft(Boeing707())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT140"'
        m.alt_scenery()
        m.intro_text = "Runway is short at Rome."

    # fail at landing
    def flight_hook(m):
        if m.game.pos.altitude_ft < 427+180 and util.distance(m.game.pos, m.pos_target) < 3:
            m.say_once(m.voice_pilot, "Ahi, ahi, ahi, ahi, ahi, ahi, ahi, ahi, ahi, ahi, ahi!")
            m.fail_engine()

class LevelConfused(Level):
    def init(m):
        # https://aviation-safety.net/wikibase/wiki.php?id=220796
        #m.pos_airport = "UAFM" # also ucfm
        m.pos_airport = "OIID" # also ucfm
        m.pos_target = m.places.oiip
        m.takeoff_ft = 2500
        #m.set_aircraft(Boeing707())
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR OIIP 140300Z 12010KT 3000 SN RA FEW025 SCT035 OVC080 M00/M01 Q1012"'
        m.alt_scenery()
        m.intro_text = "Let's deliver some meat."

class LevelColorado(Level):
    def init(m):
        # https://www.youtube.com/watch?v=SNt6gZ1I_Kk
        # V pocasi "low pressure region" je dost nizky strop -> velka legrace.
        m.pos_airport = "KMTJ"
        m.pos_target = m.places.ktex
        m.takeoff_ft = 7500
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT140"'
        m.alt_scenery()
        m.intro_text = "We are flying high."

class LevelColorado2(Level):
    def init(m):
        m.pos_airport = "KBJC"
        m.pos_target = m.places.klxv
        m.takeoff_ft = 7500
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT140"'
        m.alt_scenery()
        m.intro_text = "We are flying even higher."


class LevelHighLanding(Level):
    def init(m):
        # SLLP is highest in the list (#5) that is available in fgfs. Quite big.
        # SLPO is other available one. More suitable for MiG-15?
        # https://en.wikipedia.org/wiki/Highest_airports
        m.pos_airport = "SLPO"
        m.pos_target = m.places.sllp
        m.takeoff_ft = 15000
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT140"'
        m.alt_scenery()
        m.intro_text = "We are flying even higher."

class LevelCircleToLand(Level):
    def init(m):
        m.pos_airport = "MHSC"
        m.pos_target = Airport("MHTG")
        # There's something evil with runway 02 with mig-15. Front gear _always_ collapses.
        # Seems it is runway being inclined.
        # It is possible to land with minimum fuel.
        m.takeoff_ft = 4200
        #m.set_aircraft(Boeing777())
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Circle to land. Not your usual airport."

class LevelDamascus(Level):
    def init(m):
        # http://www.planes.cz/cs/article/201735/letecke-stripky-8-prg-dam-kwi
        m.pos_airport = "OLRA"
        m.pos_target = Airport("OSDI")
        m.takeoff_ft = 3200
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Some mountains around...."

class LevelTivat(Level):
    def init(m):
        # https://www.youtube.com/watch?v=mDGY_UjcMT8
        # https://www.youtube.com/watch?v=zj5DfmDRcT8
        m.pos_airport = "LDDU"
        m.pos_target = Airport("LYTV")
        m.takeoff_ft = 3200
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Mountains and sea around...."

class LevelMadeira(Level):
    def init(m):
        # https://www.youtube.com/watch?v=VKBii0JVZPY 3:49
        # https://www.youtube.com/watch?v=zzrN9P3y4TQ
        m.pos_airport = "LPPS"
        m.pos_target = Airport("LPMA")
        m.takeoff_ft = 1000
        #m.set_aircraft(MiG15()) # Quite easy
        m.set_aircraft(Boeing777())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Mountains and sea around...."

class LevelRhodes(Level):
    def init(m):
        # https://www.youtube.com/watch?v=VKBii0JVZPY 3:49
        # https://www.youtube.com/watch?v=zzrN9P3y4TQ
        m.pos_airport = "LTBS"
        m.pos_target = Airport("LGRP")
        m.takeoff_ft = 1000
        #m.set_aircraft(MiG15()) # Quite easy
        m.set_aircraft(Boeing777())
        #m.set_aircraft(Boeing707())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Welcome to empty island...."

class LevelTroubleDeep(Level):
    def init(m):
        m.pos_airport = "VQPR"
        m.pos_target = Airport("LGRP")
        m.takeoff_ft = 1000
        #m.set_aircraft(MiG15()) # Quite easy
        #m.set_aircraft(Boeing777())
        #m.set_aircraft(Boeing707())
        m.set_aircraft(Cessna()) # Quite hard
        #m.set_aircraft(DiamondDA42())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Welcome to empty island...."

class LevelKhorog(Level):
    def init(m):
        # http://wiki.flightgear.org/Suggested_Flights
        m.pos_airport = "OPCH"
        m.pos_target = Airport("UT1C")
        m.takeoff_ft = 7000
        #m.set_aircraft(MiG15())
        m.set_aircraft(Seneca())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Welcome to empty mountains...."

class LevelTibet(Level):
    def init(m):
        #m.pos_airport = "ZPPP" ~1500km, na hranici doletu
        m.pos_airport = "VNSI" # VNSI? # VNJP: ok
        m.pos_target = Airport("VNKT")
        m.takeoff_ft = 11000
        #m.set_aircraft(MiG15())
        m.set_aircraft(Seneca())
        #m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Welcome to empty mountains...."

class LevelIcon(Level):
    # https://www.youtube.com/watch?v=NZeeZ2BRNk4
    # https://www.openstreetmap.org/search?query=lake%20berryessa#map=11/38.5960/-122.2463
    def init(m):
        m.pos_airport = "KVCB"
        m.pos_target = Airport("2O3")         # Hawell mountain je vyrazne vic zabavy...
                                     # Pope Valley -- na hrane pro mig-15
        # KSTS? vetsi

        m.takeoff_ft = 1200
        m.set_aircraft(MiG15())
        #m.set_aircraft(Seneca())
        #m.set_aircraft(Cessna())
        #m.launcher.weather = '"--metar=METAR XXXX 012345Z SCT120"'
        m.alt_scenery()
        m.intro_text = "Aircraft is not a toy."

class LevelHeart(Level):
    # Heart attack on first officer, then they made very quick landing
    # https://www.youtube.com/watch?v=GXDLfsBCcGI
    # Wind was quite strong
    # rwy 29. wind 280/18 gusting 24.
    # je visual nekde nad 3000'

    # Umim to za nejakych 10:25 do zastaveni. Slo by to za 8.

    def init(m):
        m.pos_airport = "CYCH"
        m.pos_target = Airport("CYQM")

        m.takeoff_ft = 1200
        m.set_aircraft(MiG15())
#        m.set_aircraft(Boeing777())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 28018G24KT SCT040 OVC030"'
        m.alt_scenery()
        m.intro_text = "Hearts can be broken."

        # Head to Saint-Ignace
        wp1 = FixPos(46.7108, -65.0605)
        wp1.target_size = 4
        wp1.message = "You should climb to 40 thousand feet, accelerate to 472 knots ground speed."
        # Turn of 123, North of 116 and 123 crossing
        wp2 = FixPos(46.3063, -65.7774)
        wp2.target_size = 4
        wp2.message = "You should land real fast now. Airport is 60 miles, 7 o'clock. Aim for runway 26."
        m.pos_waypoint = [ wp1, wp2 ]

class LevelHit(Level):
    def init(m):
        m.pos_airport = "LSMA"
        m.pos_target = Airport("LFLI")

        m.takeoff_ft = 1500
        m.set_aircraft(MiG15())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 35010G15KT SCT040 OVC030"'
        m.enable_balloons()
        m.alt_scenery()
        m.intro_text = "Hit or run."

        delta = 0.004
        size = 0.14

        p1 = FixPos(46.97 + delta, 8.28789)
        p1.altitude_ft =  3100
        p1.agl_ft = 200
        p1.target_size = size
        p1.message = "Congratulations!"

        p = FixPos(46.97, 8.28789)
        p.altitude_ft =  2500
        p.agl_ft = 200
        p.target_size = size
        p.message = "Keep good work!"

        p2 = FixPos(46.97 - delta, 8.28789)
        p2.altitude_ft =  1900
        p2.agl_ft = 200
        p2.target_size = size
        p.message = "Second."

        p3 = FixPos(46.97 - 2*delta, 8.28789)
        p3.altitude_ft =  1600
        p3.agl_ft = 200
        p3.target_size = size
        p3.message = "Got first one."

        m.pos_waypoint = [ p3, p2, p, p1 ]

    def run_test(m):
        m.game.say(m.voice_boss, m.intro_text)

        delta = 0.002

        p1 = FixPos(46.95602 + delta, 8.28789)
        p1.altitude_ft =  1900
        p1.agl_ft = 200

        p = FixPos(46.95602, 8.28789)
        p.altitude_ft =  1600
        p.agl_ft = 200

        p2 = FixPos(46.95602 - delta, 8.28789)
        p2.altitude_ft =  1600
        p2.agl_ft = 200

        p3 = FixPos(46.95602 - 2*delta, 8.28789)
        p3.altitude_ft =  1600
        p3.agl_ft = 200

        while True:
            print("p1")
            m.move_balloon(p1)
            m.move_balloon(p1)
            m.move_balloon(p1)
            time.sleep(4)
            print("p")
            m.move_balloon(p)
            m.move_balloon(p)
            m.move_balloon(p)
            time.sleep(4)
            print("p2")
            m.move_balloon(p2)
            m.move_balloon(p2)
            m.move_balloon(p2)
            time.sleep(4)
            print("p3")
            m.move_balloon(p3)
            m.move_balloon(p3)
            m.move_balloon(p3)
            time.sleep(4)

class LevelVacation(Level):
    def init(m):
        # https://www.youtube.com/watch?v=JycvVgLSEnA
        # https://en.wikipedia.org/wiki/Nature_Air_Flight_9916

        # MRIA is not visible in ws2.

        # Lepsi zacinat v Tamboru. -- MRTR
        m.pos_airport = "MRNS"
        m.pos_target = Airport("MRTR")

        m.takeoff_ft = 500
        m.set_aircraft(Cessna())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 35010G15KT SCT090 OVC130"'
        m.alt_scenery()
        m.intro_text = "Fly to the waypoint. Cross the waypoint at heading of 30, minimum altitude, 60 knots and flaps 1."

        m.enable_balloons()

        delta = 0.004
        size = 0.14

        p1 = FixPos(9.856944, -85.370833)
        p1.altitude_ft =  100
        p1.agl_ft = 200
        p1.target_size = size
        p1.message = "Head here."

        m.pos_waypoint = [ p1 ]

class LevelBarn(Level):
    def init(m):
        # MRIA is not visible in ws2.

        # Lepsi zacinat v Tamboru. -- MRTR
        m.pos_airport = "PHNL"
        m.pos_target = Airport("PHNL")

        m.takeoff_ft = 150
        #m.set_aircraft(Cessna())
        #m.set_aircraft(YardStik())
        m.set_aircraft(Rascall())
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 35010G15KT SCT090 OVC130"'
        m.alt_scenery()
        m.intro_text = "Did you ever want to try barn storming?"

        m.enable_balloons()

        delta = 0.004
        size = 0.14

        # On runway
        p = FixPos(21.317923, -157.921371)
        p.altitude_ft =  100
        p.target_size = size
        p.message = "Turn sharply left to buzz the tower."
        m.pos_waypoint += [ p ]

        # On runway
        p = FixPos(21.321299, -157.925491)
        p.altitude_ft =  100
        p.target_size = size
        p.message = "Did you notice the cars? Lets visit military aircraft at nearby base."
        m.pos_waypoint += [ p ]

        p = FixPos(21.328697, -157.935394)
        p.altitude_ft =  100
        p.target_size = size
        p.message = "Did you notice the chopters? See those barns?"
        m.pos_waypoint += [ p ]

        p = FixPos(21.328136, -157.947403)
        p.altitude_ft =  100
        p.target_size = size
        p.message = "See those barns?"
        m.pos_waypoint += [ p ]

        # 21.327457, -157.952591 # bigger?
        # 21.313814, -157.885071 # jerab 1 ... zda se ze by bylo mozne vzit je postupne
        # 21.320892, -157.872955 # 2 anteny, snadne.
        # 21.332890, -157.889526 # 2 anteny, tezsi.
        # 21.318594, -157.914352 # mala striska -- mega tezke?

        # 21.344278, -157.907898 # mezi panelaky, velmi lehke.
        # 21.335941, -157.905655 # pod dalnici, vypada efektne

        # 21.318932, -157.953918 # Small, small stuff.
        # 21.330753, -157.960693 # Quite big.- sized.
        # 21.333593, -157.958420 # Who put doors to the barn?!

        # 21.333433, -157.925934 # Velmi dlouhy hangar. Prilis maly na c170.
        #    369017,  157.942780 # Krasnej dlouhej most.

class Level28sec(Level):
    def init(m):
        # Yep, not easy to make it through the mountains:
        # https://vuelo518.files.wordpress.com/2008/07/infografiasb.jpg
        m.pos_airport = "SVMD --runway=24"
        m.pos_target = Airport("SVVL") # Pretty hard. SVVG?

        m.takeoff_ft = 0 # Below elevation, we want "immediate" cruise
        m.set_aircraft(MiG15())
        # takeoff to NE is impossible
        #m.launcher.weather = '"--metar=METAR XXXX 012345Z 35010G15KT SCT090 OVC130"'
        # takoff to SW?
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 04510G15KT SCT090 OVC130"'
        m.alt_scenery()
        m.intro_text = "You are in a hurry."

    def limit_engine(m):
        rpm = m.sim.get_sim_float("engines/engine/rpm")
        #rpm = m.sim.get_sim_float("engines/engine/egt-degc")
        if rpm > 7000:
            m.sim.set_sim_var("controls/engines/engine/throttle", "0.52")

        pass

    def flight_hook(m):
        if m.phase_name == "Cruise" and time.time() - m.phase_start > 8:
            m.limit_engine()

class LevelJail(Level):
    def init(m):
        # 37.811024, -122.414970 San Francisco helipad.
        m.pos_start = m.places.san_francisco_heli
        m.pos_target = m.places.alcatraz_heli
        m.takeoff_ft = 150
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "Want to go to jail?"

class LevelNimitz(Level):
    def init(m):
        m.pos_start = m.places.khaf
        m.pos_target = m.places.egep
        m.takeoff_ft = 150
        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "You should not really land on aircraft carriers."

class LevelParo(Level):
    # -> PARO ... fun even with MiG-15...
    # https://www.youtube.com/watch?v=t-9Jp8dBGXQ
    # https://www.youtube.com/watch?v=3tga6fn6nXI
    # Nearby: VE44 hashimara. -- boring :-(.
    #         VQ10
    def init(m):
        m.pos_start = Airport("VQPR")
        m.pos_target = Airport("VQ10")
        m.takeoff_ft = 15000
        #m.set_aircraft(Boeing707()) # Not too bad. Maybe realistic weight would be more fun?
        #m.set_aircraft(Boeing777()) # Not too bad. Maybe realistic weight would be more fun?
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "High mountains and interesting airports."

class LevelMeltdown(Level):
    def init(m):
        m.pos_start = FixPos(27.708458, 85.364624)
        m.pos_start.track = 200

        m.pos_target = Airport("VNKT")
        m.takeoff_ft = 4600
        m.set_aircraft(MiG15())
        #m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "You definitely should not fly like that."

        m.enable_balloons()
        wp = m.get_waypoints()
        for w in wp:
            w.target_size = .5
        m.pos_waypoint = wp

    def get_waypoints(m):

        wp  = [ FixPos(27.707924, 85.372269) ]
        wp[-1].message = "Turn sharp left, fly over runway threshold."
        wp += [ FixPos(27.708458, 85.364624) ]
        wp[-1].message = "Now buzz the tower at low altitude.."
        # wp += [ FixPos(27.699930, 85.356873) ]  Too hard?
        wp += [ FixPos(27.705872, 85.360939) ] # Quite hard, doable at 1/4 speed.
        wp[-1].message = "Now land. Good luck."
        return wp

class LevelCliff(Level):
    def init(m):
        # Dangerous airport
        # Caused in-flight breakup... 57knots wind
        # https://www.youtube.com/watch?v=-nMg9mqJ-lc
        # ENVY -- Destination airport no longer exists :-(.
        # ENRS -- tak akorat dlouhy
        # Leknes airport taky neni k zahozeni.
        m.pos_airport = "ENBO"
        m.pos_target = Airport("ENRS")
        m.takeoff_ft = 1200
        m.set_aircraft(MiG15())
        #m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "You definitely should not fly like that."

class LevelLiberty(Level):
    def init(m):
        m.pos_start = m.places.manhattan_heli
        m.pos_target = m.places.fdr_drive_heli
        m.takeoff_ft = 150
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "Take a look at Liberty statue, 2 kilo meters south west of you."

class LevelRio(Level):
    def init(m):
        # They\ Were\ Flying\ Towards\ A\ Mountain\ And\ NO\ ONE\ Noticed\ _\ Lufthansa\ Cargo\ Flight\ 527--2uPhTmTURY.mp4
        m.pos_airport = "SBGL" # Takeoff from rwy 27.
        m.pos_target = Airport( "GOBD" ) # Does not work?
        m.takeoff_ft = 1000
        m.set_aircraft(Boeing707())
        m.alt_scenery()
        m.intro_text = "There is a lot of traffic around. After takeoff, climb 2000 feet and turn right towards caxias ( charlie alfa exray ) V O R."

        # Turn right heading 0 4 0 and maintain 2000 feet until further advice. Increase your speed if feasible.

        # Near the crash, 10nm out: "turn 1 6 0 and climb immediately to 10000 feet at 3000 fpm.

        # CAX vor, https://ourairports.com/navaids/CAX/Caxias_VOR-DME_BR/
        # -22.776300, -43.333199
        # 113 MHz

        m.pos_waypoint = m.get_waypoints()

    def get_waypoints(m):
        # Fly between fossil hill and haystack hill
        wp  = [ FixPos(-22.776300, -43.333199) ]
        wp[-1].message = "Continue with heading 0 4 0 at 2000 feet."
        wp[-1].target_size = .5        
        wp += [ FixPos(-22.582778, -43.215833) ]
        wp[-1].message = "Turn to 1 6 0 and climb immediately to 10000 feet at 3000 fpm."
        wp[-1].target_size = 2
        
        wp += [ FixPos(-22.0, -43.0) ]
        wp[-1].message = "Continue transantlantic crossing."
        wp[-1].target_size = 10
        return wp
        
class LevelMahara(Level):
    # sim/current-view/x-offset-m 0, z-offset-m 20
    def init(m):
        m.pos_start = m.places.dubai_uni_heli
        m.pos_target = m.places.mahara_heli
        m.takeoff_ft = 1050
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.launcher.position += " --altitude=724"
        m.intro_text = "This is an interesting building."

class LevelBank(Level):
    def init(m):
        #m.pos_start = Airport("KLAX")
        #m.pos_start = Airport("KHHR")
        m.pos_start = m.places.los_angeles_heli
        m.pos_target = m.places.bank_tower
        m.takeoff_ft = 500
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "This is an interesting building."
        
class LevelWhiteHouse(Level):
    def init(m):
        m.pos_start = m.places.washington_heli
        # Airport("KDCA") # DC12 is closer but does not work like this?
        # KIAD is bigger but too far away.
        # 38.887360, -77.023300 heliport.
        # 38.904209, -77.034401 dalsi heliport.
        # 38.928944, -77.016342 heliport pobliz zajimavyho vysilace.
        # 38.8977_N_77.0365 bily dum.
        m.pos_target = m.places.white_house
        m.takeoff_ft = 500
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "Land at white house's lawn."

class LevelTokyo(Level):
    def init(m):
        # Heliport at building, 35.660694 139.729370
        # Second one nearby, 35.663250, 139.725586 
        #m.pos_start = Airport("RJTT")
        m.pos_start = FixPos(35.663250, 139.725586)
        m.pos_target = m.places.tokyo_metropolitan
        m.takeoff_ft = 500
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "Land at white house's lawn."

        # LFLJ?
        
class LevelOil(Level):
    def init(m):
        m.pos_start = Airport("EGOW")
        #m.pos_start = FixPos(35.663250, 139.725586)
        m.pos_target = FixPos(53.63305, -3.17547)
        m.takeoff_ft = 500
        m.set_aircraft(Chinook())
        m.alt_scenery()
        m.intro_text = "Land at white house's lawn."

        # EGHE -> EGBR -- lighthouse
        # EGLH -- jinej majak, pekny
        # Na 49.973816, -6.312536 je lod, da se na ni pristat.

        # BIVM -- docela zajimavy letiste.

        # Eddystone lighthouse je hodne slavnej:
        # https://www.youtube.com/watch?v=QmrjoKJ4s58

class LevelRegional(LevelTrap):
    def init(m):
        m.pos_airport = "LFMD"
        m.pos_target = Airport("LFMC")
        m.takeoff_ft = 1500
        m.set_aircraft(CRJ())
        m.alt_scenery()
        m.intro_text = "Short hop, two-engined aircraft. Wanna play?"
        m.init_trap()

class LevelAgentRegional(LevelTrap):
    # Not easy at all
    def init(m):
        airports = [ "LILE", "LSMA", "LSMF", "EDTM", "LIMW",
                     "LFGA", "ETHL", "LSTS", "LSZS",
                     "LSPM", "LOWI", "LSGS", "ETSA", "LSMM",
                     "LIMZ", "LSMC", "LIMP", "LFQP",
                     "LFGJ", "LSTA", "EDMO", "LFSD", "LFYL",
                     "LFST", "LFYD", "LIPH", "LFSX",
                     "LFSC", "ETSL", "LIML", "LSME"]
        m.pos_airport = random.choice( airports )
        m.pos_target = Airport(random.choice(airports))
        print("Flying ", m.pos_airport, "to", m.pos_target.name)
        m.takeoff_ft = 2500
        #m.set_aircraft(CRJ())
        #m.set_aircraft(P166())
        #m.set_aircraft(CitationII())
        m.set_aircraft(A320())
        m.alt_scenery()
        m.intro_text = "Fly around, I would say."
        m.init_trap()

class LevelTower(Level):
    def init(m):
        # It is possible to fly under eifel tower, then la defense, back to tower.
        # la defense: 48.891624, 2.238593
        # eifel should be at: 48.858250, 2.294447
        m.pos_airport = "LFPB"
        m.pos_target = Airport("LFPV")
        m.takeoff_ft = 1050
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "This is an interesting building."

class LevelMother(Level):
    def init(m):
        m.pos_airport = "LKVO"
        m.pos_target = Airport("LKTC")
        m.takeoff_ft = 1050
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Prague is mother of all cities."
        # But poorly modeled in fgfs :-(.

class LevelScotland(LevelTrap):
    def init(m):
        m.pos_airport = "EGET" #792m
        m.pos_target = Airport("EGPB")
        m.takeoff_ft = 2500
        #m.set_aircraft(CRJ())
        m.set_aircraft(Seneca())
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Weather is not good today."
        m.init_trap()

class LevelHotAspen(LevelTrap):
    def init(m):
        # https://www.youtube.com/watch?v=8PBUVMCbmFQ
        m.pos_airport = "KASE"
        m.pos_target = Airport("KLXV")
        # Moc velke, radeji 2CO9
        m.takeoff_ft = 8000
        m.set_aircraft(Seneca())
        m.alt_scenery()
        m.intro_text = "It is nice hot day today."
        m.init_trap()

class LevelCirclingProhibited(Level):
# https://www.youtube.com/watch?v=S71maBUL5wM        
    def init(m):
        # https://www.youtube.com/watch?v=8PBUVMCbmFQ
        m.pos_airport = "KNKX"
        m.pos_target = Airport("KSEE")
        m.launcher.position = "--timeofday=evening"
        m.launcher.weather = '"--metar=METAR XXXX 280355Z VRB05KT 3SM BR SCT007 BKN009 OVC011 11/09 A2996"'
        
        # Maybe clouds at 600ft?
        m.takeoff_ft = 1000
        m.set_aircraft(MiG15())
        #m.set_aircraft(F16())
        m.alt_scenery()
        m.intro_text = "It is nice hot day today."
        # Approach 17, circle to land 27R.

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = .5
        m.pos_waypoint = wp

    def get_waypoints(m):
        wp  = [ FixPos(32.876, -116.972) ]
        wp[-1].message = "Begin instrument approach to runway 17."
        wp += [ FixPos(32.827, -116.972) ]
        wp[-1].message = "Break the approach at 500 feet, declare VFR, circle to land runway 27 right."
        wp += [ FixPos(32.825, -116.959) ]
        wp[-1].message = "Land visually."
        return wp

class LevelForgot(Level):
    # https://www.youtube.com/watch?v=pt5zYQ5jdG4
    def init(m):
        m.pos_airport = "CYWG"
        m.pos_target = Airport("CYYQ")
        m.takeoff_ft = 1500
        m.set_aircraft(Beechcraft())
        m.alt_scenery()
        m.intro_text = "Did you forget something?"
        #m.init_trap()
        # Approach 17, circle to land 27R.

class LevelOrange(LevelTrap):
    # https://www.youtube.com/watch?v=7Ru6DQFaiXw
    def init(m):
        if False:
            m.pos_airport = "KSNA"
            m.pos_target = Airport("KSLI")
            m.takeoff_ft = 1500            
        else:
            m.pos_airport = "LSTZ --runway=17"
            m.pos_target = Airport("LSTS")
            m.takeoff_ft = 3500

        m.set_aircraft(Seneca())
        m.alt_scenery()
        m.intro_text = "Did you forget something?"
        m.init_trap()
        m.trap.when = "takeoff"
        m.trap.what = "engine"
        m.trap.speed = random.random()*30 + m.aircraft.v_2

class LevelSwaids(Level):
    # https://www.youtube.com/watch?v=qGB-BCLYv3Q
    def init(m):
        m.pos_airport = "3J1"
        m.pos_target = Airport("2GA2")
        m.takeoff_ft = 1500            

        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "Climb to 4500 feet."
    
class LevelWatson(Level):
    # https://www.youtube.com/watch?v=qGB-BCLYv3Q
    def init(m):
        m.pos_airport = "KWVI"
        m.pos_target = Airport("KWVI")
        m.takeoff_ft = 1500            

        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "Try to deal with stuck on full throttle."
        
class LevelUnlicensed(LevelTrap):
    # https://www.youtube.com/watch?v=Bv9M9Sqwixw
    def init(m):
        m.pos_airport = "LFST"
        m.pos_target = Airport("EKRN")
        m.takeoff_ft = 1500

        # Total: 2291. Mam 760, a uz jsem mel byt daaavno na zemi. Pristavam se 743, zda se ze na volnobeh nezere _nic_.

        m.set_aircraft(CitationX())
        m.alt_scenery()
        m.intro_text = "You are doing something really interesting."
        m.init_trap()

    def intro_hook(m):
        m.set_fuel("350", "410", "390")

class LevelChina(LevelTrap):
    # http://avherald.com/h?article=4f64be2f&opt=0
    # Air china crash; impossible to simulate
    def init(m):
        m.pos_airport = "LFST"
        m.pos_target = Airport("EKRN")
        m.takeoff_ft = 1500

        # Total: 2291. Mam 760, a uz jsem mel byt daaavno na zemi. Pristavam se 743, zda se ze na volnobeh nezere _nic_.

        m.set_aircraft(B738())
        m.alt_scenery()
        m.intro_text = "You are doing something really interesting."
        m.init_trap()

    def intro_hook(m):
        m.set_fuel("350", "410", "390")

class LevelSniktau(LevelTrap):
    # https://www.youtube.com/watch?v=jR5-v5g3LqE
    
    # Air china crash; impossible to simulate
    def init(m):
        m.pos_airport = "KBJC"
        m.pos_target = Airport("KLXV") # KGJT
        m.takeoff_ft = 6500

        # Total: 2291. Mam 760, a uz jsem mel byt daaavno na zemi. Pristavam se 743, zda se ze na volnobeh nezere _nic_.

        #m.set_aircraft(Seneca())
        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "Fly low to give passangers better view. Fly south, then follow highway through the mountains."
        m.init_trap()

class Level7tries(Level):
    """
7 pokusu o pristani, na konci uz delaj slusny silenosti, Mentour 8/22

https://www.youtube.com/watch?v=KUg0hmskWFw
https://www.youtube.com/watch?v=o-IuOnDBoGA

(start) -> 3 approaches at VOCI Cochin -> then 4 approaches at VOTV. Initially weather .. night, visibility 3km, overcast 1500ft. At 6:30 local, visibility 2.5km. 14 VOR approach. 1.5km. 
[visibility 1500 meter haze, winds 290/3 kts, scattered cloud at
1500 ft and at 2500 ft]
 At 25nm "minimum fuel". 2km visibility. 7:08 local they have ~10 minutes fuel.

https://www.aviation-accidents.net/report-download.php?id=649

"""

    def init(m):
        m.pos_airport = "VOCI"
        m.pos_target = Airport("VOTV")
        m.takeoff_ft = 3000

        m.set_aircraft(Boeing777())
        # Too easy with MiG
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Visibility is not good today."
        # Limit fuel for "more fun".
        # It wants "basic weather"...
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 1SM SCT015 OVC035"'
        m.launcher.position = "--timeofday=night"

class LevelEngineProjectile(Level):
    def init(m):
        m.pos_airport = "ELLX"
        m.pos_target = Airport("DNKN")

        # https://en.wikipedia.org/wiki/Trans-Air_Service_Flight_671
        # Flight was ELLX -> DNKN.
        # Somewhere around Valence, at 32000 feet, both engines failed.
        # Fire after extending flaps.
        # They headed to LFML, but ended up at LFMI

        # 38 t of cargo -- but sim does not allow that?
        # Takeoff is possible with 38t fuel, 17t load.

        m.takeoff_ft = 3500
        m.intro_text = "Prepare for long and boring flight. Climb to 32000 feet."
        m.set_aircraft(Boeing707())

        m.alt_scenery()

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 5
        m.pos_waypoint = wp

    def get_waypoints(m):
        wp  = [ FixPos(44.350, 5.167) ]
        wp[-1].message = "Climb to 32000 feet"
        return wp

    def flight_hook(m):
        if util.distance(m.game.pos, FixPos(43.39, 5.28)) < 50:
            m.say_once(m.voice_pilot, "Two bangs, what was that?")
            m.fail_engine(2)
            m.fail_engine(3)

class LevelSwim(Level):
    """
    https://en.wikipedia.org/wiki/Transair_Flight_810
    https://www.youtube.com/watch?v=aPFJvSDbzVc

    2100 feet -> first engine fail.
    +4 minutes -> second engine fail

    27km/h wind
"""
    def init(m):
        m.pos_airport = "PHNL"
        m.pos_target = Airport("PHJR")

        m.takeoff_ft = 1000
        # m.set_aircraft(Boeing777()) # not suitable, engines work after failure
        m.set_aircraft(CRJ())
        m.alt_scenery()
        m.intro_text = "This should be quite short."

    def flight_hook(m):
        if m.game.pos.altitude_ft > 2100:
            m.say_once(m.voice_pilot, "Ugh oh. That is not good.")
            m.fail_engine(0)
            m.new_phase("damaging engine")
        if m.phase_name == "damaging engine" and m.phase_len() > 3*60:
            m.say_once(m.voice_pilot, "Right engine is overheating.")
        if m.phase_name == "damaging engine" and m.phase_len() > 4*60:
            m.say_once(m.voice_pilot, "We should have expected that.")
            m.fail_engine(1)

class LevelNoRunway(Level):
    """
Letadlo pred nim havaruje a zablokuje drahu

Pristavaj s asi 100kg paliva.

https://www.youtube.com/watch?v=_59b-HRu2YI

-> LevelNoRunway

Fuel remaining: 45, 247
"""
    def init(m):
        m.pos_airport = "LSGS"
        m.pos_target = Airport("LSGS")

        m.takeoff_ft = 1000
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Fly to Saint Stephan Airport. You don not have much fuel."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 5
        m.pos_waypoint = wp

    def intro_hook(m):
        # S 45/430 je mozny tak udelat G/A na Saint Stephan a pristat znovu. Zbyde 40.
        # 
        m.set_fuel("45", "430", "0")

    def get_waypoints(m):
        wp  = [ FixPos(46.441, 7.454) ]
        wp[-1].message = "Prepare for landing."

        wp += [ FixPos(46.486, 7.425) ]
        wp[-1].message = "Oh no, plane before you crashed. You can not land here. Perform touch and go!"
        
        return wp

class LevelFastandLow(Level):
    def evaluation(m):
        print("Your time was ", time.time() - m.phase_start)
        m.end_game("Congratulation, you finished in %d seconds." % int(time.time() - m.phase_start))

        
    def flight_hook(m):
        print(m.phase_name, "-- time", time.time() - m.phase_start, "alt", m.game.pos.altitude_ft)
        if m.phase_name == "Race":
            if m.game.pos.altitude_ft > 3650:
                m.end_game("You are too high, you failed")

            if m.game.pos.altitude_ft > 3550:
                m.game.say(m.voice_god, "Descent!")

class LevelMaverick(LevelFastandLow):
    def init(m):
        #m.pos_airport = "ENAL"         wp  = [ FixPos(62.466667, 7.666667) ]
        m.pos_airport = "L41" # "KGCN" #UT47?
        m.pos_target = Airport("LSGS")

        m.takeoff_ft = 3800
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "You are best of the best. You will need to be fast and fly low."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 1.5
        m.pos_waypoint = wp

    def get_waypoints_long(m):
        # 1st try: 428 seconds, without effective altitude restrictions. -- lets make it shorter?
        wp  = [ FixPos(36.802753, -111.643921) ]
        wp[-1].message = "Follow the canyon."

        wp += [ FixPos(36.786121, -111.646584) ]
        wp[-1].message = "Descent to 300 feet above water."

        wp += [ FixPos(36.765636, -111.661438) ]
        wp[-1].message = "Stay fast and stay low."
        wp[-1].action = lambda: m.new_phase("Race")
        
        wp += [ FixPos(36.339741, -111.863480) ]
        wp[-1].message = "Congratulations, you made it so far."
        wp[-1].action = lambda: m.evaluation()
        return wp
    
    def get_waypoints(m):
        wp  = [ FixPos(36.647448, -111.759796) ]
        wp[-1].message = "Follow the canyon."

        wp += [ FixPos(36.603954, -111.765289) ]
        wp[-1].message = "Descent to 300 feet above water."

        wp += [ FixPos(36.576939, -111.792068) ]
        wp[-1].message = "Stay fast and stay low."
        wp[-1].action = lambda: m.new_phase("Race")
        
        wp += [ FixPos(36.339741, -111.863480) ]
        wp[-1].message = "Congratulations, you made it so far."
        wp[-1].action = lambda: m.evaluation()
        return wp
    	# 295 seconds can be done, without exceeding altitudes. Wow. Not easy!
    	# 216 seconds, and then successful landing


class LevelMaverickTrain(LevelFastandLow):
    def init(m):
        m.pos_airport = "L41" # "KGCN" #UT47?
        m.pos_target = Airport("LSGS")

        m.takeoff_ft = 3750
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Train to become the best."

        wp = m.get_waypoints()
        for w in wp:
            w.target_size = 1
        m.pos_waypoint = wp

    def get_waypoints(m):
        # Possible, record is 185 seconds so far
        # Next was 183 seconds (and crash during landing)
        # Then 148.43 seconds (and crash before landing)
        # Then 147.89 seconds (and good landing).
        wp  = [ FixPos(36.818283, -111.634224) ]
        wp[-1].message = "Follow the canyon north."

        wp += [ FixPos(36.826736, -111.626671) ]
        wp[-1].message = "Descent to 300 feet above water."

        wp += [ FixPos(36.839104, -111.616798) ]
        wp[-1].message = "Stay fast and stay low."
        wp[-1].action = lambda: m.new_phase("Race")
        
        wp += [ FixPos(36.935352, -111.483246) ]
        wp[-1].message = "Congratulations, you made it so far."
        wp[-1].action = lambda: m.evaluation()
        return wp


class LevelOverEdge(Level):
    # Going\ Over\ The\ Edge\ \(Atlantic\ Airways\ Flight\ 670\)\ -\ DISASTER\ BREAKDOWN-tmV3hJg5_5A
    def init(m):
        m.pos_airport = "ENZV"
        m.pos_target = Airport("ENSO")
        m.takeoff_ft = 500
        m.set_aircraft(MiG15())
        #m.set_aircraft(Tu154())
        m.alt_scenery()
        m.intro_text = "Short hop."

class LevelAntelope(Level):
    def init(m):
        m.pos_airport = "9OR1"
        m.pos_target = Airport("2OR1")
        m.takeoff_ft = 500
        m.set_aircraft(MiG15())
        #m.set_aircraft(Tu154())
        m.alt_scenery()
        m.intro_text = "Short hop."

class LevelAgentHeli(LevelAgent):
    def init(m):
        # FIXME: Many beatiful heliports around, select randomly!
        airports = [ "SPIF", "SSAS", "SDKS", "SDMU", "SDNU", "SJBG", "SWDX", "SIDT", "SDDO", "SIJP", "SDUY", "SIGP", "SNUO", "SIMN", "SIGNJ", "SDFQ", "SDHU", "SDCX", "SWZK", "SWIR", "SDDM", "SDRI", "SNRG", "SDHD", "SITG", "SNEQ", "SDQD", "SNVF", "SJPM", "SIQT", "SDRJ", "SDHL", "SIYM", "SDQJ", "SJSY", "SDCN", "SJFD", "SIGA", "SIHH", "SDPU", "SIVM", "SIYV" ]

        m.pos_airport = random.choice(airports)
        m.pos_target = heliport.Heliport(random.choice(airports))
        print("Flying ", m.pos_airport, "to", m.pos_target.name)
        m.takeoff_ft = 500
        #m.set_aircraft(Chinook())
        m.set_aircraft(Alouette())
        m.alt_scenery()
        m.intro_text = "Feel free to visit Jesus es statue."

class LevelBadCircle(Level):
    def init(m):
        # Yemenia flight 626,
        # A\ Routine\ Landing\ Approach\ Quickly\ Turns\ into\ a\ Disaster\ \(Terrifying\ Moments\ on\ Tape\)-iikG8Pt5WB0.mp4
        m.pos_airport = "FMCI"
        m.pos_target = Airport("FMCH")
        m.takeoff_ft = 150
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Approach runway 20, circle to land runway 02."
        m.launcher.weather = '"--metar=METAR XXXX 012345Z 02040KT 3SM OVC070 SCT018"'

class LevelGoldenGate(Level):
    def init(m):
        m.pos_airport = "KOAK"
        m.takeoff_ft = 550
        m.pos_target = FixPos(47.079313, 9.065023)
        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "Land on golden gate bridge."

class LevelLot(Level):
    def init(m):
        # Nice video: yt
        # Heartbreaking Plane Crash So Close To Airport (LOT Polish Airlines Flight 5055) - DISASTER BREAKDOWN-Vi9Tfs5H-mU
        # https://en.wikipedia.org/wiki/LOT_Polish_Airlines_Flight_5055
        m.pos_airport = "EPWA"
        m.pos_target = Airport("EPWA")
        m.takeoff_ft = 550
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Flying passangers to San Francisco."

        # Head to, Grudziądz%20#map=12/53.4725/18.7785, climb to FL180.
        # At Lipniky, FL269, 10:41:28
        # Half engines, No Elevator,
        # 11:09 -- Pitch trim fails.

        # 52.274, 20.742 -- 3 min do EPWA.
        
class LevelBridges(Level):
    def init(m):
        # Inspired by "Bridges at Toko-Ri" -- https://en.wikipedia.org/wiki/The_Bridges_at_Toko-Ri
        # https://www.historynet.com/real-bridges-toko-ri/
        # They say it was really Sokho-Ri -- https://www.openstreetmap.org/node/3514985382#map=10/38.7257/125.7784
        # Not too great -- North Korea is not interesting
        m.pos_airport = "ZKPY"
        m.pos_target = Airport("EPWA")
        m.takeoff_ft = 550
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Flying passangers to San Francisco."

class LevelBusters(Level):
    def init(m):
        # 60 ft, 390 km/h
        # Möhne Dam -- 51.48971/8.05944 (blizko Dusseldorf)
        # Eder Dam -- 51.1826/9.0600 (Waldeck airport?)
        # Sorpe -- ?
        # Ennepe Dams -- 51.24114/7.40877 (Wellringrade airport).
        m.pos_airport = "EDFQ"
        m.pos_target = Airport("ETHF")
        m.takeoff_ft = 550
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Flying passangers to San Francisco."

class LevelTurn(Level):
    def init(m):
        m.pos_airport = "WSSL"
        #m.pos_airport = "WMKJ"
        m.pos_target = Airport("WMBT")
        m.takeoff_ft = 550
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Flying passangers to San Francisco."

class LevelSlavkovskyStit(Level):
    def init(m):
        m.pos_airport = "LZTT"
        m.pos_target = Airport("LZTT")
        m.takeoff_ft = 1550
        m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Fly low over Slavkovsky stit."

class LevelRoundValley(Level):
    def init(m):
        m.pos_airport = "O09"
        m.pos_target = Airport("O16")
        m.takeoff_ft = 1550
        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "Fly into the sunset."

class LevelDoor(Level):        
    # American Airlines flight 96
    # Some tri-motor, cargo door bust.
    # Engine failure middle engine, rudder hardover.

    def init(m):
        m.pos_airport = "LSZC"
        m.pos_target = Airport("LSME")
        m.takeoff_ft = 3500
        # Fun in Seneca.
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Those cargo doors were hard to close."

    def flight_hook(m):
        if m.game.pos.altitude_ft > 3000:
            m.fail_system("rudder-hardover")

class LevelRockies(Level):
    # https://www.youtube.com/watch?v=eJ1OkL5YMj8
    def init(m):
        m.pos_airport = "KBDU" # KMTJ
        m.pos_target = Airport("KASE")
        m.takeoff_ft = 5500
        # Fun in Seneca.
        m.set_aircraft(Cessna())
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Cruise at 16 500 feet."

        wp  = [ Airport("KMTJ") ]
        wp[-1].message = "Ugh ogh. Something is wrong."
        wp[-1].target_size = 128
        wp[-1].action = lambda: m.fail_engines()
        m.pos_waypoint = wp

class LevelSightSeeing(Level):
    # Blancorio Covelo\ A-36\ NTSB\ Preliminary\ Report\ \[ogUnaE3U40Y
    # 
    def init(m):
        m.pos_airport = "KPVU" # "KSPK" # Spanish fork kspk? -- unavailable?!
        m.pos_target = Airport("KASE")
        m.takeoff_ft = 5000
        # Fun in Seneca.
        m.set_aircraft(Cessna())
        #m.set_aircraft(MiG15())
        m.alt_scenery()
        m.intro_text = "Lets fly to the mountains."

        #wp  = [ FixPos(40 + 13.5/60, -111-36.75/60) ]
        #wp  = [ FixPos(40.22488, -111.609) ]
        wp  = [ FixPos(40.22488, -111.000) ]
        wp[-1].message = "Ugh ogh. Something is wrong."
        wp[-1].target_size = 0.5
        wp[-1].action = lambda: m.fail_engines()
        m.pos_waypoint = wp

class LevelDownhill(Level):        
    def init(m):
        m.pos_airport = "TNCM"
        m.pos_target = Airport("TFFJ")
        m.takeoff_ft = 500
        m.set_aircraft(Cessna())
        m.alt_scenery()
        m.intro_text = "We'll go to famous airport."

    def flight_hook(m):
        if m.game.pos.altitude_ft > 3000:
            m.fail_system("rudder-hardover")

class Game(textui.TextUI):
    def __init__(m, arg):
        m.level = m.prepare_level(arg)

        m.pos = util.FakePos()
        m.pos.speed_kn = 0
        m.pos.track = 0
        m.pos.altitude_ft = 0

    def prepare_level(m, arg):
        if len(arg) > 1:
            if arg[1] == "agent":
                return LevelAgentCSA(m)
            if arg[1] == "cessna":
                return LevelAgentCessna(m)
            if arg[1] == "maverick":
                return LevelMaverick(m)
            if arg[1] == "maverick2":
                return LevelMaverickTrain(m)
            if arg[1] == "barn":
                return LevelBarn(m)
            if arg[1] == "lost":
                return LevelLost(m)

            # helicopter
            if arg[1] == "jail":
            	return LevelJail(m)
            if arg[1] == "liberty":
            	return LevelLiberty(m)
            if arg[1] == "mahara":
            	return LevelMahara(m)
            if arg[1] == "oil":
            	return LevelOil(m)
            if arg[1] == "heli":
            	return LevelAgentHeli(m)
            if arg[1] == "vostok":
                return LevelVostok(m)
            if arg[1] == "circle":
                return LevelCircleToLand(m)
            
            print("Unknown game level: ", arg[1])
            sys.exit(1)
                

        # Pretty much done
        #return LevelAfterRepairs(m)
        #return LevelLowFlight(m)
        #return LevelLowFlight2(m)
        #return LevelEvilTurbines(m)
        #return LevelEvilJets(m)	# hmm. quite busy flight.
        #return LevelStormRace(m)
        #return LevelTutorial(m)
        #return LevelDanger1(m)
        #return LevelRealDanger1(m)
        #return LevelBeechFun(m)
        #return LevelHell(m)		# simple, done
        #return LevelFranceHop(m)	# simple
        #return LevelFranceHop2(m)
        #return LevelFranceBack(m)
        #return LevelFrozen(m)	# Okay, nice... Not sure if 737 is suitable for this, it flies ok well below the stall speed.
        #return LevelFun20k(m)
        #return LevelLeak(m) # A bit easy? Should it use different airports?

        #return LevelOnTop(m) # FIXME: does not work. espeak takes too long to speak.
        #return LevelMountains(m)
        #return LevelWeather(m)
        #return LevelNightmare20k(m)
        #return LevelUpAndAway(m)
        #return LevelSeaplane(m)
        #return LevelEvilCopters(m)
        #return LevelGroundContact(m) # Chtelo by doladit. ILS, chce pripravu.

        #return LevelShortRunway(m)
        #return LevelImbalance(m)

        #return LevelRoudnice(m)
        #return LevelHighest(m)
        #return LevelJetMountains(m)
        #return LevelSmokingLavatory(m)
        #return LevelBattery(m)

        #return LevelTestMountains(m)
        #return LevelPaintJob(m) # Narazi na limity simulace: zblaznil se mi pitch trim, s tim je tezky letet.

        # Experiments/ needs more work
        #return LevelNonDirectional(m)
        #return LevelThinAir(m)
        #return LevelAmazon(m)
        #return LevelDateLine(m)

        #return LevelDevilVacation(m)
        #return LevelIcySpain(m)
        #return LevelCrossCrisis(m)

        #return LevelImpossible(m) # Needs fixing? Way too long flight.
        #return LevelTest(m)
        #return LevelCroatia(m)
        #return LevelNotCapable(m)
        #return LevelSaba(m)
        #return LevelTrijetIntro(m)
        #return LevelTrijetIntro2(m)
        #return LevelFranceMountains(m)
        #return LevelTail(m)
        #return LevelShort(m)
        #return LevelEvening(m)

        #return LevelLightning(m)
        # Fixme: should do ILS cat I min (?) weather, should fail pitot static etc.
        #return LevelPower(m)
        # Good tutorial for Cessna is Buochs -> Alpnach -> Sarnen
        #return LevelTest3(m)
        #return LevelSupersonic(m)

        #return LevelKeepingInAir(m)
        #return LevelWindyMountains(m) # Nice, and pretty hard. Would be good to make it say the instructions.

        #return LevelFoggyDeparture(m)
        #return LevelAnotherWorld(m) # Too long flight.
        #return LevelFullAfterburner(m)
        #return LevelJetTraining(m)
        #return LevelJetTraining2(m)
        #return LevelJetShort(m)
        #return LevelJetSurprise(m) # add some surprise?
        #return LevelJetLeak(m)
        #return LevelJetEscape(m)
        #return LevelJetHigh(m)
        #return LevelGearTrap(m) # FIXME: Add trap
        #return LevelRaceTrack(m)
        #return LevelLocked(m) # FIXME:
        #return LevelTatry(m)
        #return LevelTalesLondon(m)
        #return LevelTalesShort(m)
        #return LevelTalesMountains(m)
        #return LevelTalesLong(m)
        #return LevelLongest777(m)
        #return LevelQuietAlps(m)
        #return LevelFlock(m)
        #return LevelConfused(m)
        #return LevelColorado(m)
        #return LevelColorado2(m)
        #return LevelHighLanding(m)

        #return LevelLost(m)
        #return LevelDanger7(m)

        #return LevelLongShift(m)
        #return LevelTooLongShift(m)

        #return LevelJetLong(m)
        #return LevelCircleToLand(m)
        #return LevelDamascus(m)
        #return LevelTivat(m)
        #return LevelMadeira(m)
        #return LevelRhodes(m)
        #return LevelTroubleDeep(m)
        #return LevelKhorog(m)
        #return LevelTibet(m)
        #return LevelIcon(m)
        #return LevelHeart(m)
        #return LevelHit(m)
        #return LevelVacation(m)
        #return LevelAgentCessna(m)
        #return LevelBarn(m)
        #return Level28sec(m)
        #return LevelNimitz(m)
        #return LevelAgentCSA(m)
        #return LevelParo(m)
        #return LevelMeltdown(m)
        #return LevelCliff(m)
        #return LevelFoggyDeparture(m)
        #return LevelRio(m)
        #return LevelBank(m)
        #return LevelWhiteHouse(m)
        #return LevelTokyo(m)
        #return LevelOil(m)
        #return LevelRegional(m)
        #return LevelAgentRegional(m)
        #return LevelTower(m)
        #return LevelMother(m)
        #return LevelScotland(m)

        # WZ38->WX15.
        # MPMG->MPHO
        # MROC->MRPV

        #return LevelHotAspen(m)
        #return LevelCirclingProhibited(m)
        #return LevelForgot(m)
        #return LevelOrange(m)
        #return LevelSwaids(m)
        #return LevelWatson(m)
        #return LevelUnlicensed(m)
        #return LevelChina(m)
        #return LevelSniktau(m)
        #return Level7tries(m)
        #return LevelEngineProjectile(m)
        #return LevelSwim(m)
        #return LevelNoRunway(m)
        #return LevelOverEdge(m)
        #return LevelAntelope(m)
        #return LevelBadCircle(m)
        #return LevelGoldenGate(m)
        #return LevelLot(m)
        #return LevelBridges(m)
        #return LevelBusters(m)
        #return LevelTurn(m)
        #return LevelSlavkovskyStit(m)
        #return LevelRoundValley(m)
        #return LevelDoor(m)
        #return LevelRockies(m)
        #return LevelSightSeeing(m)
        return LevelDownhill(m)

    # https://www.youtube.com/watch?v=-0wh2pZHSjc
    # Hayden, CO. KHDN -> KMIA

    def say(m, voice, s):
        #sy("killall espeak")
        print(s)
        m.level.sim.display(s)
        #sy("echo '"+s+"' | espeak &")

    def update_pos(m):
        while m.gpsd.waiting():
            report = m.gpsd.next()
            #print(report)
            if report['class'] == 'TPV':
                m.pos.eph = 10
                m.pos.lat = report["lat"]
                m.pos.lon = report["lon"]
                if report["speed"] != 0:
                    m.pos.speed = report["speed"]  # apparently in m/sec
                    m.pos.speed_kn = 1.94384 * m.pos.speed
                if "track" in report:
                    m.pos.track = report["track"]
                if "alt" in report:
                    m.pos.altitude = report["alt"]
                    m.pos.altitude_ft = m.pos.altitude * 3.28084

    def run(m):
        launcher = m.level.get_launcher()
        launcher.run_gpsd()
        launcher.run_fgfs()
        m.adjust_termios()
        m.gpsd = gps.gps()
        m.gpsd.stream(1)
        m.level.wait_for_sim()
        try:
            m.level.run()
        except GameOver:
            print("*** Game ends here")

# http://wiki.flightgear.org/Multiplayer_protocol

# mknod /tmp/fgfs.nmea p

g = Game(sys.argv)
g.run()
