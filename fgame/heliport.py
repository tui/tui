#!/usr/bin/python3

# https://ourairports.com/help/data-dictionary.html

import sys
sys.path += [ "../lib", "../zpells" ]
import util

class Heliport:
    def __init__(m, name):
        m.list = {}
        m.read()
        x = m.list[name]
        m.lat = x.lat
        m.lon = x.lon
        m.name = name
        m.target_size = 0.05

    def read(m):
        for l in open("airports.csv").readlines()[1:]:
            s = l.split(",")
            code, lat, lon = s[1], s[4], s[5]
            if code[0] == '"':
                code = code[1:]
            if code[-1] == '"':
                code = code[:-1]
            try:
                m.list[code] = util.FixPos(float(lat), float(lon))
            except:
                pass

if __name__ == "__main__":
    h = Heliport("SNUO")
    print(h.name, h.lat, h.lon)
