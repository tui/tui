# http://kwynn.com/t/4/07/FG/Nasal/1016/accel_score.nas
# GPLv3

var verbose = 1;
var verboseT = 0;

var reinitL = nil;
var frameL = nil;
var maxZ = 0;
var frameCount = 0;
var maxZPr = 0;
var actFrame = 0;

var one_acc = func(s) {
        base = "/accelerations/pilot/" ~ s ~ "-accel-fps_sec";

	var a = abs(getprop(base));
	var max = abs(getprop(base ~ "-max") or 0);
	var clear = getprop(base ~ "-clear") or 0;

	if (a > max) {
	   setprop(base ~ "-max", a);
	   setprop(base ~ "-clear", a);
	   max = a;
	}
	if (clear > max)
	   setprop(base ~ "-max", 0.0);
}

var was_wow = 0;
var landings = 0;

var copy_one = func(from, to, from2, arg) {
        if (from == "")
	        from = from2;
	p = getprop(from ~ arg);
	if (p)
		setprop(to ~ arg, p);
}

var copy_parameters = func(from, to) {
        copy_one(from, to, "/orientation", "/pitch-deg");
        copy_one(from, to, "/orientation", "/roll-deg");
        copy_one(from, to, "/orientation", "/track-deg");
        copy_one(from, to, "/velocities", "/airspeed-kt"); 	
        copy_one(from, to, "/velocities", "/down-relground-fps");
}

var frame_listener_func = func() {
        one_acc("x");
        one_acc("y");
        one_acc("z");

	if (!getprop("/gear/gear/wow"))
	        copy_parameters("", "/last");

	if (getprop("/gear/gear/wow") and !was_wow) {
	   	copy_parameters("/last", "/landing[" ~ landings ~ "]");
		setprop("/landing/num", landings);
	        landings = landings + 1;
        }

	was_wow = getprop("/gear/gear/wow");
}

var init = func {

        print('Initializing...');
	setprop("/landing/num", 0);
	frameL = setlistener("/sim/signals/frame", frame_listener_func, 0, 1);
}

print('Initializing...');
if (reinitL == nil) reinitL = setlistener("/sim/signals/reinit", init, 1,1);
