#!/usr/bin/python

from __future__ import print_function

import sys
sys.path += [ "../lib", "../zpells" ]
import util

from lxml import etree

class FixPos(util.FixPos):
    def __init__(m, lat, lon, name = None):
        m.lat = lat
        m.lon = lon
        m.altitude_ft = None
        m.track = 0
        m.target_size = -1
        m.name = None
        m.message = None
        m.action = None
        m.name = name

fgfs_path = "/data/fgfs/"

class Airport(FixPos):
    def __init__(m, name):
        FixPos.__init__(m, None, None)

        fn = fgfs_path + "Scenery/Airports/" + name[0] + "/" + name[1] + "/" + name[2] + "/" + name + ".threshold.xml"
        try:
            f = open(fn, "r")
        except:
            return None

        m.name = name
        m.root = etree.parse(f)
        #print(etree.tostring(m.root))
        for threshold in m.root.iter("threshold"):
            #print(etree.tostring(threshold))
            if not (m.lat is None):
                m.other = FixPos(m.lat, m.lon, "Other end of " + name)
            for e in threshold:
                #print(e.tag, e.text)
                if e.tag == "rwy":
                    m.rwy = str(e.text)
                if e.tag == "lat":
                    m.lat = float(e.text)
                if e.tag == "lon":
                    m.lon = float(e.text)
        
    def details(m):
        len = util.distance(m, m.other)
        print("%f %f type=runway %f %f  len_km=%.3f icao=%s > %s" % (m.lat, m.lon, m.other.lat, m.other.lon, len, m.name, m.name))

class Airports():
    def __init__(m):
        fn = fgfs_path + "Scenery/Airports/index.txt"
        m.all = []
        for l in open(fn, "r").readlines():
            sp = l.split("|")
            a = FixPos(float(sp[2]), float(sp[1]), sp[0])
            #print(sp[0])
            m.all += [ a ]

def print_airports_near(n):
    pos = Airport(n)
    all = Airports()
    human = False
    for a in all.all:
        d = util.distance(pos, a)
        if d < 100:
            if human:
                print(d, "km to ", end='')
            afull = Airport(a.name)
            if afull.name:
                afull.details()
            else:
                print(a.name, "... no runways available?")

def cmd_line():
    if len(sys.argv) == 2:
        a = Airport(sys.argv[1])
        #print(a.lat, a.lon, "type=airport >", a.name)
        # sort -k 6
        a.details()
        return

    print_airports_near("LSPM")

if __name__ == "__main__":
    cmd_line()
