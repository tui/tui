class Aircraft:
    v_1 = 0
    v_r = 0
    v_2 = 0
    extra_options = ""
    version = 2018.0
    engines = 1

    def setup(m):
        pass

class AddonAircraft(Aircraft):
    fgaddon = "/data/fgfs/flightgear-fgaddon/"
    def __init__(m):
        m.extra_options = "--aircraft-dir="+m.fgaddon+"Aircraft/"+m.path+"/"

class Ask13(AddonAircraft):
    # Glider
    id = "ask13"
    
class Camel(AddonAircraft):
    # camel gives very strange data to the gps, so nothing works?
    # not very good look from the cockpit
    # Did not figure out how to take off with it.
    id = "sopwithCamel"

class YardStik(AddonAircraft):
    id = "YardStik"
    path = "YardStik"

class Rascall(AddonAircraft):
    id = "Rascal110-JSBSim"
    path = "Rascal"

class Cessna(Aircraft):
    id = "c172p"
    v_1 = 40
    v_r = 55
    v_2 = 80
    # Needs ~800 to 1550 ft runway to takeoff, according to conditions
    # https://www.quora.com/What-is-the-minimum-runway-length-for-a-Cessna-172

class Cessna182(AddonAircraft):
    id = "c182t"
    path = "c182s"
    version = 2021
    # 2020 version: No garmin g1000. -> bad
    # 2021 version: g1000 works, but result is 1fps.

class Ufo(Aircraft):
    # Not even too much fun to fly
    id = "ufo"
    
class Seneca(AddonAircraft):
    # Seneca checklists:
    # http://www.atlasaviation.com/checklists/piper/Seneca-Checklist.pdf
    # manual:
    # http://www.ehfc.net/SenecaIIManual.pdf
    # panel reference
    # http://wiki.flightgear.org/Seneca_II_Panel_Reference
    # https://www.flygoodyear.com/images/downloads/Piper%20PA34-200T%20Turbo%20Seneca%20II%20POH.pdf

    # Recomendation : turn off yokes and windshield deice in View| menu.
    # SOP: According to them, V_1 is after rotate. Too complex for me.
    # http://www.langleyflyingschool.com/PDF%20Documents/LFS%20PA-34%20SOPs%20Dec%2008%20(2).pdf
    path = "SenecaII"
    id = "SenecaII"
    v_1 = 68
    v_r = 70
    v_2 = 91
    engines = 2

    # Seneca has very poor single-engine performance in simulator. With lighter load
    # it should be closer to realistic.
    def min_load(m):
        for i in range(9):
            m.sim.set_sim_var("fdm/jsbsim/inertia/pointmass-weight-lbs[%d]" % i, "0")


    def setup(m):
        m.min_load()

class DiamondDA42(AddonAircraft):
    path = "Diamond-Da42"
    id = "da42"

class Beechcraft(AddonAircraft):
    # https://en.wikipedia.org/wiki/Beechcraft_1900
    # At least fgaddon version works. Below captain's yoke, enable battery ignition. Turn condition away from "cutoff". Then start...
    # Crazy behaviour with engines idle -- stalls very easily (?). Be sure to maintain airspeed.

    path = "b1900d"
    id = "b1900d"
    v_1 = 105
    v_r = 110
    v_2 = 120
    cruise_speed = 230
    v_ne = 247
    service_ceiling = 25000

class CitationX(AddonAircraft):
    id = "CitationX"
    path = "CitationX"
    v_1 = 120
    v_r = 130
    v_2 = 140

class CitationII(AddonAircraft):
    id = "Citation-II-Sierra"
    path = "Citation"
    v_1 = 120
    v_r = 130
    v_2 = 140

class Vostok(AddonAircraft):
    # english docs is wrong: 4km for bigger parachute
    # warning: need to hold C to operate re-entry engine. 
    id = "Vostok-1"
    path = "Vostok-1"

class Param:
    # lbs, int, knots
    def __init__(m, _wmax, _flaps, _speed):
        m.wmax = _wmax
        m.flaps = _flaps
        m.speed = _speed

class SpeedLim:
    def __init__(m, knots, mach=None):
        m.knots = knots
        m.mach = mach

class Boeing707(AddonAircraft):
    # 707-420/430
    # Works fine.
    # Autostart takes long time. Watch the speed, or engines fail.
    # Run the checklists after takeoff, or there are surprises.

    # Balancing fuel steals it
    # Spooling up the engines a bit too fast.

    # http://www.boeing.com/assets/pdf/commercial/airports/acaps/707sec3.pdf

    id = "707"
    path = "707"
    runway_mtow = 11400 # feet, 707-320b
    runway_land = 5900
    v_1 = 120
    v_r = 150
    v_2 = 160
    # Cruise at 977km/h.
    # Can't find good data :-(
    # v_ne =
    # m_ne =
    def init(m):
        m.takeoff = [ Param( 215000, 14, 130),
                      Param( 280000, 25, 135),
                      Param( 999999, 25, 155) ]
        m.landing = [ Param( 170000, 50, 125),
                      Param( 190000, 50, 130),
                      Param( 999999, 50, 140) ]
        # Touchdown at 120, max 130
        m.gear = SpeedLim(320, mach=0.83)
        m.gear_move = SpeedLim(270, mach=0.83)
        m.flaps = {}
        m.flaps[14] =  SpeedLim(223)
        #m.flaps[25] =  SpeedLim(223)
        #m.flaps[50] =  SpeedLim(223)

class Boeing777(AddonAircraft):
    # Very nice cockpit. Can be autostarted. Very easy to fly.
    # Don't try to fail engines to this one. It seems that failed engines still
    # provide power (a lot of power). They eat fuel but fuel consumption indicates zero.
    # Too easy to fly: don't fly below stall speed. Simulation will happily climb
    # 100 knots below stall warning.
    # Performance with empty aircraft is better than MiG-15.
    id = "777-300"
    path = "777"
    v_1 = 120
    v_r = 130
    v_2 = 140
    mtow = 775000
    mlw = 554000
        
class BoeingB52(AddonAircraft):
    id = "B-52F"
    path = "B-52F"

class B1b(AddonAircraft):
    # Quite nice toy. Goes supersonic, flies far...
    id = "B-1B"
    path = "B-1B"

class B36(AddonAircraft):
    id = "b36d"
    path = "B-36D-Peacemaker"

class VelocityXL(AddonAircraft):
    # Small, strange airplane. Quite stable
    id = "velocity"
    path = "Velocity-XL"

class Lockheed1049h(AddonAircraft):
    # Big beast, old, nice simulation. "Constellation"
    id = "Lockheed1049h"
    path = "Lockheed1049h"

class Tu154(AddonAircraft):
    # Unfamiliar units, unfamilar language. Fun :-).
    # Very detailed / nicely done.
    # Can keep altitude on single engine. Realistic?
    id = "tu154b"
    path = "tu154b"

class ShootingStar(AddonAircraft):
    # I goes supersonic without anything bad happening; I don't think that is realistic
    id = "F80C"
    path = "F80C"

class X15(AddonAircraft):
    # X-15
    # Rocket powered; normally dropped from B-52. I don't think simulation is complete
    id = "X15-new"
    path = "X15"

class DC10(AddonAircraft):
    # Nice plane. Not too old, not too new.
    # No speedbrakes, AFAICT. Refuses to go down, pretty much.
    id = "DC-10-30"
    path = "DC-10-30"

class Pilatus(AddonAircraft):
    # Taildragger. Quite hard to land in one piece
    id = "PC6-B2H4"
    path = "PC-6"

class Lightning(AddonAircraft):
    # Stall speed 105mph
    id = "P-38L"
    path = "P-38-Lightning"

class ATR72(AddonAircraft):
    # No directional control on ground.
    # Crashes after engine fail.
    id = "ATR-72-500"
    path = "ATR-72-500"

class Vanguard(AddonAircraft):
    id = "vanguard"
    path = "Vickers-Vanguard"

class Spin(AddonAircraft):
    id = "Fokker-Spin"
    path = "Fokker-Spin"

class Viggen(AddonAircraft):
    id = "AJS37-Viggen"
    path = "JA37"

class MiG15(AddonAircraft):
    id = "MiG-15bis"
    path = "MiG-15"
    v_r = 108
    # lszw -- 792m -- pretty much minimum for takeoff at max weight; landing is hard but possible (at reduced weight)?
    # http://www.migflug.com/jetflights/mig-15-fagot-the-70-year-old-65-year-old-jet-still-going-strong.html
    # 5000 feet runway at sea level ... 1524 meters.
    # 8000 feet runway at 6000 feet elevation ... 2438 meters.

    #       cca 720 km ... full + drop tanks, 12km, mach .69
    # Range cca 450 km ... full + drop tanks, cruise at 10km, 750km/h ground speed,
    #       cca 170 km? .. full + drop tanks, low altitude, 300-400km/h speed
    # Range cca 850 km ...full + drop tanks, above 12km, .69 mach
    # Ladovisko Tornow -- from LKKB, in range?

    # Descent from 13km, 100km to airport, 10%..344pounds fuel, idle
    #              10km   40km             8%.. 292
    #               9km   20km             7%
    #               8.5km  0                 .. 238

class JT5B(AddonAircraft):
    id = "JT-5B"
    path = "JT-5B"

# 47 is nice and tame, 53e is more of challenge
class Chinook(AddonAircraft):
    id = "ch47"
    path = "ch47"

    def setup(m):
        m.sim.set_sim_var("sim/current-view/x-offset-m", "0")
        m.sim.set_sim_var("sim/current-view/y-offset-m", "2.5")
        m.sim.set_sim_var("sim/current-view/z-offset-m", "20")

class Alouette(AddonAircraft):
    id = "Alouette-III"
    path = "Alouette-III"

    def setup(m):
        m.sim.set_sim_var("sim/current-view/x-offset-m", "0")
        m.sim.set_sim_var("sim/current-view/y-offset-m", "2.5")
        m.sim.set_sim_var("sim/current-view/z-offset-m", "20")

class Stallion(AddonAircraft):
    id = "ch53eSuperStallion"
    path = "ch53e"

    def setup(m):
        m.sim.set_sim_var("sim/current-view/x-offset-m", "0")
        m.sim.set_sim_var("sim/current-view/y-offset-m", "2.5")
        m.sim.set_sim_var("sim/current-view/z-offset-m", "20")
        
class F16(AddonAircraft):
    id = "f16-block-60"
    path = "f16"
    version = 2020

class CRJ(AddonAircraft):
    v_1 = 130
    v_r = 135
    v_2 = 145
    engines = 2
    # http://krepelka.com/fsweb/learningcenter/aircraft/flightnotesbombardiercrj700.htm
    # At full load, needs 5500 feet (1676m) for takeoff and 4850 feet (1478) for landing.
    id = "CRJ700"
    path = "CRJ700-family"
    version = 2020
    
    def mlw_load(m):
        # Sets up mlw + a bit, with goal of having mlw after short flight
        m.sim.set_sim_var("sim/config/allow-autothrottle", "true")        
        m.sim.set_sim_var("consumables/fuel/tank/level-lbs", "4733")
        m.sim.set_sim_var("consumables/fuel/tank[1]/level-lbs", "4733")
        m.sim.set_sim_var("sim/weight[1]/weight-lb", "12000")
        m.sim.set_sim_var("sim/weight[2]/weight-lb", "3739")
        m.sim.set_sim_var("sim/weight[3]/weight-lb", "961")

    def setup(m):
        m.mlw_load()

class P166(AddonAircraft):
    v_1 = 130
    v_r = 135
    v_2 = 145
    engines = 2
    id = "p166"
    path = "Piaggio-P166"
    version = 2020

class A320(AddonAircraft):
    v_1 = 130
    v_r = 135
    v_2 = 145
    engines = 2
    version = 2020.0
    id = "A320neo-PW"
    path = "A320-family"

class B738(AddonAircraft):
    v_1 = 130
    v_r = 135
    v_2 = 145
    engines = 2
    version = 2021
    id = "738"
    path = "737-800"

class B744(AddonAircraft):
    v_1 = 130
    v_r = 135
    v_2 = 145
    engines = 2
    version = 2021
    id = "747-400"
    path = "747-400"
