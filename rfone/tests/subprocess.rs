extern crate subprocess;

#[cfg(test)]
mod tests {
    use subprocess::Exec;

    #[test]
    fn hello() -> Result<(), String> {
        println!("hello rust");
        Exec::shell("echo hello shell").join().unwrap();
	Ok(())
    }

    #[test]
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
 	    Ok(())
 	} else {
            Err(String::from("two plus two does not equal four"))
        }
    }
}
