extern crate subprocess;

use self::subprocess::Exec;

use std::io::{BufReader, BufRead};
use std::collections;
use std::fmt;

#[derive(Debug, Clone)]
pub struct Contact {
    pub name : String,
    pub number : String,
}

impl fmt::Display for Contact {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    // The `f` value implements the `Write` trait, which is what the
    // write! macro is expecting. Note that this formatting ignores the
    // various flags provided to format strings.
        write!(f, "{}	{}", self.number, self.name)
    }
}

impl Contact {
    fn matches(&self, query: &str) -> bool {
       self.name.contains(query)
    }
}

#[derive(Debug)]
pub struct Book {
    data : collections::BTreeMap<String, Contact>,
}

// FIXME: need to remove "strange" characters from key, too.
fn get_key(num : &str) -> String {
    let mut start = 0;
    let le = num.len();
    if le > 8 {
        start = le-8;
    }

    num[start..].to_string()
}

impl fmt::Display for Book {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
         for (_key, c) in &self.data {
	 	writeln!(f, "{}", c)?;
	 }
	 Ok(())
    }
}

impl Book {
    pub fn lookup(&self, number: &str) -> Option<&Contact> {
        let key = get_key(number);

	Some(self.data.get(&key)?)
    }
    
    fn empty() -> Book {
        Book{ data : collections::BTreeMap::new() }
    }

    pub fn filter(&self, query: &str) -> Book{
       let mut b = Book::empty();

       for (key, contact) in &self.data {
           if contact.matches(query) {
 	       b.data.insert(key.to_string(), contact.clone());
	   }
       }

       b
    }
    
    pub fn new() -> Book {
        let mut b = Book::empty();
	let cmd = "tests/ph-book";
	let cmd = "/usr/share/unicsy/ofone/contactsdb.py";
    
        let x = Exec::cmd(cmd).stream_stdout().unwrap();
        let br = BufReader::new(x);
        for (_, line) in br.lines().enumerate() {
            let l = line.unwrap();
 	    let v: Vec<&str> = l.split("\t").collect();
	    if v.len() < 2 {
	       println!("Suspicious contact: {}", l);
	       continue;
 	    }
 	    let num = v[0].to_string();
 	    let name = v[1].to_string();
	    let key = get_key(&num);
    
	    println!("number {} name {}", num, name);
    	    b.data.insert(key, Contact{ number: num, name: name });
        }

	b
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hello() -> Result<(), String> {
        println!("hello book module");
 	Book::new();
	Ok(())
    }
}