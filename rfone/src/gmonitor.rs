extern crate gtk;
extern crate glib;

use std::sync::Arc;
use std::sync::Mutex;

use std::cell::RefCell;
use std::rc::Rc;
use std::collections;

use gtk::prelude::*;
use gtk::*;
use rotatable::*;

use alert::*;
use phone::*;

struct GtkListener {
       widgets : PhoneWidgets,
       alert : Option<MusicAlert>,
       calls : collections::HashMap<String, CallWindow>,
       tx : glib::Sender<PhoneCmd>,
}

impl PhoneListener for GtkListener {
    fn netreg_changed(&mut self, n: NetReg) {
	let s = n.get_pretty().unwrap_or("0% (error getting signal)".to_string());
	let new_signal = n.have_signal().unwrap_or(false);

	self.widgets.signal_label.set_text(&s);
    }

    fn incoming_sms(&mut self, sms : SMS) {
        win_message(&format!("New SMS: {}", sms.text));
    }

    fn call_changed(&mut self, call : Call) {
        let id = self.format_number(call.props.get_str("LineIdentification"));
        let s = format!("call {} {} {}", call.path,
		      call.props.get_str("State").unwrap_or("(?!)"),
 		      id);
        self.printl(&s);

	let c = win_call(&call, id, &self.tx);

	self.calls.insert(call.path, c);
    }    

    fn call_ended(&mut self, path : &str) {
        self.printl(&format!("call {} ended", path));
    }
}

impl GtkListener {
    fn new(pw :PhoneWidgets, tx : glib::Sender<PhoneCmd>) -> GtkListener {
       GtkListener{widgets : pw,
                   alert : None,
		   calls : collections::HashMap::new(),
		   tx : tx}
    }
}

struct PhoneWidgets {
    signal_label : gtk::Label,
}

struct State {
       listener : Mutex<GtkListener>,
}

impl State {
    fn tick(&self, phone : &Phone) {
        println!("tik tak");
 	let (event, str) = phone.message_poll();
	let mut l = self.listener.lock().unwrap();

        match (event) {
 	PhoneMsg::NetRegChanged(n) => l.netreg_changed(n),
 	PhoneMsg::CallChanged(s) => l.call_changed(s),
 	PhoneMsg::CallEnded(s) => l.call_ended(&s),
 	PhoneMsg::IncomingSMS(s) => l.incoming_sms(s),
 	PhoneMsg::Note(s) => l.printl(&s),
 	PhoneMsg::Nomsg => {},
 	}
    }

    fn new(pw :PhoneWidgets, tx : glib::Sender<PhoneCmd>) -> State {
       State {
	   listener : Mutex::new(GtkListener::new(pw, tx)),
       }
    }
}

pub fn win_message(text : &str) {
    // Create the main window.
    let window = TouchWindow::new("Message");

    let grid = window.grid;
    let win = window.window;

    let label = font_label(&big(text));
    grid.attach(&label, 0, 0, 5, 5);

    let (_, close) = font_button(&big("Close"));
    grid.attach(&close, 5, 5, 1, 1);

    win.show_all();
}

struct CallWindow {}

fn call_button(model : Rc<RefCell<MusicAlert>>, tx : &glib::Sender<PhoneCmd>, cmd : PhoneCmd) {
    (*model.borrow_mut()).stop();
    tx.send(cmd);
}

fn win_call(call : &Call, id : String, tx : &glib::Sender<PhoneCmd>) -> CallWindow {
    let text = "Ahoj";
    // Create the main window.
    let window = TouchWindow::new(&format!("Call from {}", id));

    let grid = window.grid;
    let win = window.window;

    let label = font_label(&big(text));
    grid.attach(&label, 0, 0, 5, 5);

    let mut a = MusicAlert::new();
    let model = Rc::new(RefCell::new(a));
    let model2 = model.clone();
    let model3 = model.clone();

    let path = call.path.clone();
    let path2 = call.path.clone();
    let path3 = call.path.clone();

    let tx1 = tx.clone();
    let tx2 = tx.clone();
    let tx3 = tx.clone();

    let (_, answer) = font_button(&big("Reject"));
    answer.connect_clicked(move |_| { call_button(model3.clone(), &tx3, PhoneCmd::RejectCall(path3.clone())); });
    grid.attach(&answer, 2, 5, 1, 1);

    let (_, answer) = font_button(&big("Answer"));
    answer.connect_clicked(move |_| { call_button(model2.clone(), &tx2, PhoneCmd::AnswerCall(path2.clone())); });
    grid.attach(&answer, 3, 5, 1, 1);

    let (_, mute) = font_button(&big("Mute"));
    mute.connect_clicked(move |_| { call_button(model.clone(), &tx1, PhoneCmd::Nomsg); });
    grid.attach(&mute, 4, 5, 1, 1);

    let win2 = win.clone();
    let (_, close) = font_button(&big("Close"));
    close.connect_clicked(move |_| { win2.destroy(); });
    grid.attach(&close, 5, 5, 1, 1);

    win.show_all();

    CallWindow {}
}

pub fn win_monitor() {
    gui_init();

    // Create the main window.
    let window = TouchWindow::new("Unicsy Phone");

    // Handle closing of the window.
    window.set_main();

    let grid = window.grid;
    let win = window.window;

    let label = font_label(&big("Startup...."));
    grid.attach(&label, 1, 0, 5, 1);

    //win_message("Hello world!");

    let close = quit_button();
    grid.attach(&close, 5, 5, 1, 1);

    let pw = PhoneWidgets{ signal_label : label };

    // Don't forget to make all widgets visible.
    win.show_all();

    let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
    let state : Arc<State> = Arc::new(State::new(pw, tx));
    let phone = Phone::new();
    let phone2 = Phone::new();

    {
        let state_ = Arc::clone(&state);
 	gtk::timeout_add(50, move || {
 	     let s = &state_;
	     s.tick(&phone);
	     Continue(true)
	});
    }

    {
        let state_ = Arc::clone(&state);
	rx.attach(None, move |msg| {
	match msg {
	    _ => { println!("got some message should do something"); phone2.one_command(msg); }
	}
        glib::Continue(true)
    });
    }


    // Run the main loop.
    gtk::main();
}

fn main() -> Result<(), i32> {
    win_monitor();
    Ok(())
}
