extern crate gtk;
extern crate glib;
extern crate subprocess;
#[macro_use]
extern crate lazy_static;

mod phone;
mod rotatable;

use std::sync::Arc;
use std::sync::Mutex;


use gtk::prelude::*;
use gtk::*;
use rotatable::*;

use phone::*;
use subprocess::*;

use std::cell::RefCell;
use std::rc::Rc;

struct State {
       audioplay : Option<subprocess::Popen>,       
}

impl State {
       fn new() -> State {
       	  State { audioplay : None }
       }
}

lazy_static! {
    static ref state: Mutex<State> = Mutex::new( State::new() );
}

pub fn answer_call() {
    let s = state.lock();
}

pub fn win_call() {
    let text = "Ahoj";
    // Create the main window.
    let window = TouchWindow::new("Message");

    let grid = window.grid;
    let win = window.window;

    let label = font_label(&big(text));
    grid.attach(&label, 0, 0, 5, 5);

    let x = Exec::cmd("/usr/bin/mpg123").arg("/data/mp3/The-Star-of-the-County-Down-.MP3").popen().unwrap();
    let model = Rc::new(RefCell::new(x));
    let model2 = model.clone();

    let (_, answer) = font_button(&big("Answer"));
    answer.connect_clicked(move |_| {
    			      //println!("Could kill pid {:?}", x );
			      (*model2.borrow_mut()).terminate();
			      answer_call();
 			 });
    grid.attach(&answer, 3, 5, 1, 1);

    let (_, mute) = font_button(&big("Mute"));
    mute.connect_clicked(move |_| {
    			      //println!("Could kill pid {:?}", x );
			      (*model.borrow_mut()).terminate();
 			 });
    grid.attach(&mute, 4, 5, 1, 1);

    let (_, close) = font_button(&big("Close"));
    close.connect_clicked(|_| { gui_quit() });
    grid.attach(&close, 5, 5, 1, 1);

    win.show_all();
}

fn main() {
    gtk::init().unwrap();

    win_call();

    // Run the main loop.
    gtk::main();
}
