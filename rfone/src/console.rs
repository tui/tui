extern crate termios;

// Thanks to
// https://stackoverflow.com/questions/26321592/how-can-i-read-one-character-from-stdin-without-having-to-hit-enter

use std::io;
use std::io::Read;
use std::io::Write;
use self::termios::{Termios, TCSANOW, ECHO, ICANON, tcsetattr};

fn set_termios(f : fn(u32) -> u32) {
    let stdin = 0; // couldn't get std::os::unix::io::FromRawFd to work
                   // on /dev/stdin or /dev/tty
    let termios = Termios::from_fd(stdin).unwrap();
    let mut new_termios = termios.clone();   // make a mutable copy of termios
    // that we will modify
    //new_termios.c_lflag &= !(ICANON | ECHO); // no echo and canonical mode
    new_termios.c_lflag = f(new_termios.c_lflag);
    tcsetattr(stdin, TCSANOW, &mut new_termios).unwrap();
}

pub fn tui_init() {
    set_termios(|x| { x & !(ICANON | ECHO) });
}

pub fn getchar() -> char {
    let stdout = io::stdout();
    let mut reader = io::stdin();

    let mut buffer = [0;1];  // read exactly one byte

    //print!("");
    //stdout.lock().flush().unwrap();
    
    reader.read_exact(&mut buffer).unwrap();
    //println!("You have hit: {:?}", buffer);

    buffer[0] as char
}

pub fn tui_done() {
    set_termios(|x| { x | (ICANON | ECHO) });
}

pub fn main() {
    tui_init();
    getchar();
    tui_done();
}