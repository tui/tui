#![allow(unused)]
#![allow(unused_must_use)]
#[macro_use(lazy_static)]

extern crate lazy_static;

extern crate gtk;
extern crate glib;
extern crate subprocess;

mod phone;
mod rotatable;
mod gmonitor;
mod gcompose;
mod gphone;
mod alert;

fn help() {
    println!("No help yet");
}

fn main() -> Result<(), i32> {
    let args : Vec<_> = ::std::env::args().collect();
    if args.len() == 1 {
        help();
	return Err(1);
    }
    let cmd = &args[1];

    match &cmd[..] {
    "monitor" => {
    	      	     gmonitor::win_monitor();       
		     return Ok(());
    }
    "compose" => {
    	      	     gcompose::win_compose();       
		     return Ok(());
    }
    "help" => {
    	             println!("Help requested.");
		     help();
		     return Ok(());
    }
    _ => {
                     println!("Unknown command");
                     help();
		     return Err(2);
         }
    }	 

    Ok(())
}
