extern crate gtk;
extern crate glib;

use std::sync::Arc;
use std::sync::Mutex;

use gtk::prelude::*;
use gtk::*;
use rotatable::*;

use phone::*;

// https://stackoverflow.com/questions/21788876/what-is-the-maximum-sms-message-length
// 160 7-bit characters
// 70 16-bit characters

// 153 7-bit if there's more than one sms.

struct Compose {
       number : gtk::Entry,
       text : gtk::Entry,
       counter : gtk::Label,
       send : gtk::Button,
}


// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

impl Compose {
     pub fn new() -> Compose {
         let counter = font_label(&big("150/1"));

	 let entry = font_entry();
	 let number = font_entry();

 	 entry.connect_changed(clone!(counter => move |x| {
 		println!("entry changed {:?}", x.get_text());
		let t = format!("{}/150?", x.get_text().unwrap().len());
		counter.set_text(&t);
 	 }));

	 let (_, send) = font_button(&big("Send"));
	 send.connect_clicked(clone!(entry, number => move |_| {
	        println!("send sms, text {:?}", entry.get_text());
		let p = Phone::new();
		//let p = &single_phone.unwrap();
		p.send_sms(&number.get_text().unwrap(), &entry.get_text().unwrap());
         }));

	 Compose { number: number, text: entry, counter: counter, send: send }
     }
}

pub fn win_compose() {
    gui_init();

    // Create the main window.
    let window = TouchWindow::new("SMS compose");

    // Handle closing of the window.
    window.set_main();

    let grid = window.grid;
    let win = window.window;

    let compose = Compose::new();
    grid.attach(&compose.counter, 5, 0, 1, 1);
    grid.attach(&compose.number, 1, 0, 4, 1);
    grid.attach(&compose.text, 0, 1, 6, 4);

    let close = quit_button();
    grid.attach(&compose.send, 4, 5, 1, 1);    
    grid.attach(&close, 5, 5, 1, 1);


    // Don't forget to make all widgets visible.
    win.show_all();

    // Run the main loop.
    gtk::main();
}

fn main() -> Result<(), i32> {
    win_compose();
    Ok(())
}
