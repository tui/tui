#![allow(unused)]
#![allow(unused_must_use)]

extern crate subprocess;

mod phone;
mod book;
mod alert;
mod console;

use phone::*;
use book::Book;
use alert::*;

fn help() {
    println!("Usage: ph command");
    println!();
    println!("	monitor");
    println!("	d|dial");
    println!("	h|hangup");
    println!("	a|answer");
    println!("	s|sms number text");
    println!("	tests");
    println!("	test2");
    println!("	help");
}

fn get_number( num : &str ) -> String {
    return num.to_string();
}

struct BookListener {
    book : Book,
}

impl PhoneListener for BookListener {
    fn lookup_number(&self, number: &str) -> Option<String> {
        Some(self.book.lookup(number)?.name.to_string())
    }
}

impl BookListener {
    fn new() -> BookListener {
       BookListener{ book : Book::new() }
    }
}

fn book_isearch(b: Book) {
    console::tui_init();
    let mut s = "".to_string();
    loop {
    	  let f = b.filter(&s);
    	  println!("{}> {}", f, s);
	  let c = console::getchar();
	  if c == 127 as char { println!("Backspace"); s = s[..s.len()-1].to_string(); continue; }
	  if c == '\n' { break; }
	  s.push(c);
    }

    console::tui_done();
}

fn main() -> Result<(), i32> {
    let args : Vec<_> = ::std::env::args().collect();
    if args.len() == 1 {
        help();
	return Err(1);
    }
    let cmd = &args[1];

    match &cmd[..] {
    "book" => {
		     let b = book::Book::new();
		     println!("{}", b);
		     book_isearch(b);
		     return Ok(());
    }
    "alert" => {
		     console_alert();
		     return Ok(());
    }
    "alert2" => {
		     let a = MusicAlert::new();
		     loop {}
		     return Ok(());
    }
    "help" => {
    	             println!("Help requested.");
		     help();
		     return Ok(());
    }
    _ => {}
    }

    let mut l = BookListener::new();
    let p = Phone::new();

    match &cmd[..] {
    "monitor" => {
 		     p.modem_online();
 		     p.monitor(&mut l);
    }
    "d" | "dial" => {
		     p.dial(&get_number(&args[2]));
		     p.signal_loop(&mut l, "CallRemoved");
    }
    "h" | "hangup" => {
                     p.hangup_all();
    	       }
    "a" | "answer" => {
                     p.answer_all();
		     p.signal_loop(&mut l, "CallRemoved");
    	       }
    "s" | "sms" => {
		     p.send_sms(&get_number(&args[2]), &args[3]);
		     p.signal_loop(&mut l, "MessageRemoved");
    }
    "tests" => {
 		     p.tests(&mut l);
    }
    "test2" => {
    }
    "help" => {
		     help();
		     return Ok(());
    }
    _ => {
                     println!("Unknown command");
                     help();
		     return Err(2);
         }
    }	 

    Ok(())
}
