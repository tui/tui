use std::thread;
use std::time::Duration;
use std::io::prelude::*;
use std::io;

use subprocess::*;

fn beep(t :u64) {
   print!("\x07");
   io::stdout().flush();
   thread::sleep(Duration::from_millis(t));
}

pub fn console_alert() {
    let handle = thread::spawn(|| {
        for j in 1..4 {
 	    for i in 1..4 {
	        beep(180);
	    }
 	thread::sleep(Duration::from_millis(300));
        } });
    handle.join().unwrap();
}

pub struct MusicAlert {
    proc : Popen,
}

impl MusicAlert {
    pub fn new() -> MusicAlert {
    	MusicAlert {
	    proc : Exec::cmd("/usr/bin/mpg123").arg("/data/mp3/The-Star-of-the-County-Down-.MP3").popen().unwrap()
        }
    }

    pub fn stop(&mut self) {
        self.proc.terminate();
    }
}
