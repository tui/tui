#![allow(dead_code)]

extern crate gtk;
extern crate glib;

use gtk::prelude::*;
use gtk::*;

pub struct TouchWindow {
       pub vertical : bool,
       pub window : Window,
       pub grid : Grid,
}

pub struct RotatableWindow {
       pub vertical : bool,
       pub window : Window,
       pub grid : Grid,

       pub grid_main : Grid,
       pub grid_aux : Grid,
}

trait AnyTouchWindow {
}

impl TouchWindow {
     pub fn new(title : &str) -> Self {
        let window = Window::new(WindowType::Toplevel);
	let vertical = false;
	
	if !vertical {
	   window.set_default_size(800, 400);
	} else {
	   window.set_default_size(400, 800);
	}
	window.set_title(title);

	let grid = Grid::new();
	//grid.set_row_homogeneous(true);
	//grid.set_column_homogeneous(true);	

	grid.set_hexpand(true);
	grid.set_vexpand(true);
	grid.set_row_homogeneous(true);
	grid.set_column_homogeneous(true);

	window.add(&grid);

     	TouchWindow { vertical : vertical,
		      window : window,
 		      grid : grid,
		     }
     }

     pub fn set_main(&self) {
     	 self.window.connect_delete_event(|_, _| {
             // Stop the main loop.
             gtk::main_quit();
	     // Let the default handler destroy the window.
	     Inhibit(false)
         });
     }
}

impl RotatableWindow {
     pub fn new(title : &str) -> Self {
     	 let tw = TouchWindow::new(title);

	let grid_main = Grid::new();
	let grid_aux = Grid::new();

	grid_main.set_hexpand(true);
	grid_main.set_vexpand(true);
	grid_main.set_row_homogeneous(true);
	grid_main.set_column_homogeneous(true);

	grid_aux.set_hexpand(true);
	grid_aux.set_vexpand(true);
	grid_aux.set_row_homogeneous(true);
	grid_aux.set_column_homogeneous(true);

	if tw.vertical {
	    tw.grid.set_orientation(Orientation::Vertical);
        }

	tw.grid.add(&grid_main);
	tw.grid.add(&grid_aux);
	
	RotatableWindow {
	    vertical : tw.vertical,
 	    window : tw.window,
 	    grid : tw.grid,
	    grid_main : grid_main,
	    grid_aux : grid_aux,
        }
    }
}

pub fn big(s : &str) -> String { format!("<span size=\"xx-large\">{}</span>", s) }

//    def middle(m, s): return '<span size="x-large">%s</span>' % s
//    def small(m, s): return '<small>%s</small>' % s

pub fn font_label(s : &str) -> gtk::Label {
   let w = Label::new(Some(s));
   w.set_use_markup(true);
   w
}

pub fn font_button(s : &str) -> (gtk::Label, gtk::Button) {
   let w = font_label(s);
   let w1 = Button::new();
   w1.add(&w);
   ( w, w1 )
}

pub fn quit_button() -> gtk::Button {
    let (_, close) = font_button(&big("Quit"));
    close.connect_clicked(|_| { gui_quit() });
    close
}

pub fn font_entry() -> gtk::Entry {
   let e = Entry::new();
   // https://mmstick.gitbooks.io/rust-programming-phoronix-reader-how-to/content/chapter16.html
   //e.modify_font(pango.FontDescription("sans 28");
   e
}

//    def big_button(m, big, small):
//         return m.font_button(m.big(big) + '\n' + m.small(small))

pub fn gui_init() {
    gtk::init().unwrap();
}

pub fn gui_quit() {
    gtk::main_quit();
}

