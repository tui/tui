#![allow(unused)]
#![allow(unused_must_use)]
#![allow(non_snake_case)]

// SHould have used thsi instead? https://github.com/antoyo/gdbus-rs

extern crate dbus;
extern crate chrono;

use self::dbus::*;
use self::dbus::arg::RefArg;

use self::chrono::prelude::*;

use std::io::prelude::*;
use std::io;
use std::collections;

pub trait PhoneListener {
    fn pretty_now(&self) -> String {
        let local: DateTime<Local> = Local::now();

        format!("{:2}:{:02}", local.hour(), local.minute())
    }

    fn format_any(&self, p : &str) -> String {
        let l = 70;
        let mut s = p.to_owned();
    	while s.len() < l {
    	      s = s + " ";
            }
    	format!("{} {}\r", self.pretty_now(), s)
    }
    
    fn printt(&self, p : &str)  {
    	print!("{}", self.format_any(p));
    	io::stdout().flush().ok().expect("Could not flush stdout");
    }
    
    fn printl(&self, p : &str)  {
    	println!("{}", self.format_any(p));
    }

    fn netreg_changed(&mut self, n: NetReg) {
	let s = n.get_pretty().unwrap_or("0% (error getting signal)".to_string());
	let new_signal = n.have_signal().unwrap_or(false);

	self.printt(&s);
    }

    fn lookup_number(&self, number: &str) -> Option<String> {
        None
    }

    fn format_number(&self, number: Option<&str>) -> String {
        if let Some(n) = number {
	    if let Some(name) = self.lookup_number(n) {
	        return format!("{} <{}>", name, n);
	    }
 	    return n.to_string();
	}

	return "(unknown)".to_string();
    }

    fn incoming_sms(&mut self, sms : SMS)  {
        let s = format!("From: {} msg: {}",
 			       self.format_number(sms.props.get_str("Sender")),
			       sms.text);
        self.printl(&s);
    }

    fn call_changed(&mut self, call : Call) {
        let s = format!("call {} {} {}", call.path,
		      call.props.get_str("State").unwrap_or("(?!)"),
 		      self.format_number(call.props.get_str("LineIdentification"))
 		);
        self.printl(&s);
    }

    fn call_ended(&mut self, path : &str) {
        self.printl(&format!("call {} ended", path));
    }
}

pub enum PhoneMsg {
     Nomsg,
     IncomingSMS(SMS),
     CallChanged(Call),
     CallEnded(String),
     NetRegChanged(NetReg),
     Note(String),
}

pub enum PhoneCmd {
     Nomsg,
     AnswerCall(String),
     RejectCall(String),
     HangupCall(String),
}

pub struct Props {
    props : collections::HashMap<String, arg::Variant<Box<arg::RefArg>>>,
}

impl Props {
    pub fn get(&self, s : &str) -> Option<&arg::RefArg> { Some(self.props.get(s)?) }
    pub fn get_str(&self, s : &str) -> Option<&str> {     self.get(s)?.as_str() }
    pub fn get_int(&self, s : &str) -> Option<i64>  {     self.get(s)?.as_i64() }
}

pub struct NetReg {
    props : Props,
}

impl NetReg {
    pub fn have_signal(&self) -> Option<bool> {
       if self.props.get_str("Status")? != "registered" &&
          self.props.get_str("Status")? != "roaming" { return Some(false); }
       let i = self.props.get_int("Strength")?;

       Some(i > 0)
    }
    pub fn get_pretty(&self) -> Option<String> {
       if !self.have_signal()? { return Some(format!("0% (no signal)")); }
       let i = self.props.get_int("Strength")?;
       let mut s = format!("{}% {}", i, self.props.get_str("Name")?);

       Some(s)
    }
}

pub struct SMS {
    pub text : String,
    pub props : Props,
}

pub struct Call {
    pub path : String,
    pub props : Props,
}

pub struct Phone {
    pub conn: Connection,
    first_modem_path: String,
}

fn print_refarg(value: &arg::RefArg) {
 // We don't know what type the value is. We'll try a few and fall back to
 // debug printing if the value is more complex than that.
	if let Some(s) = value.as_str() { println!("{}", s); }
 	else if let Some(i) = value.as_i64() { println!("{}", i); }
 	else { println!("{:?}", value); }
}

fn arg_to_Props(res : collections::HashMap<String, arg::Variant<Box<arg::RefArg>>>) -> Props {
        //println!("Netreg props {:?}", r);

        //println!("Complete reply: {:?}", res);
        //println!("First property: {:?}", res["Status"]);
	//print_refarg(&res["Status"]);
        //println!("First property: {:?}", &res["Status"].as_str().unwrap());

        Props { props : res }
}

fn message_to_Props(r : dbus::Message) -> Props {
        let res: collections::HashMap<String, arg::Variant<Box<arg::RefArg>>> = r.get1().unwrap();

	arg_to_Props(res)
}

fn message_to_SMS(msg : &dbus::Message) -> SMS {
    let res: Box<arg::RefArg> = msg.get1().unwrap();

    //println!("get1 -- {:?}", res);
    let (res1, res2): (Option<Box<arg::RefArg>>, Option<collections::HashMap<String, arg::Variant<Box<arg::RefArg>>>>) = msg.get2();

    //println!("get2 -- {:?}", res2);

    SMS{props: arg_to_Props(res2.unwrap()), text: res.as_str().unwrap().to_owned()}
}

fn message_debug(msg : &dbus::Message)  {
    let res: Box<arg::RefArg> = msg.get1().unwrap();

    println!("get1 -- {:?}", res);
    let (res1, res2): (Option<Box<arg::RefArg>>, Option<collections::HashMap<String, arg::Variant<Box<arg::RefArg>>>>) = msg.get2();

    println!("get2 -- {:?}", res2);

    ()
}

impl Phone {
    fn method_call_at(&self, path: &str, pa: &str, me: &str) -> dbus::Message {
        let m = Message::new_method_call(
            "org.ofono",
            path,
            "org.ofono".to_owned() + pa,
            me,
        ).unwrap();
        m
    }

    fn method_call(&self, pa: &str, me: &str) -> dbus::Message {
        self.method_call_at(&self.first_modem_path, pa, me)
    }

    fn send_and_block(&self, m: dbus::Message) -> dbus::Message {
        /* FIXME: these can fail. Easily. */
        let r = self.conn.send_with_reply_and_block(m, 2000).unwrap();
        r
    }

    pub fn modem_online(&self)  {
        let m = self.method_call(".Modem", "SetProperty").append2(
            "Powered",
            arg::Variant(true),
        );
        self.send_and_block(m);

        let m = self.method_call(".Modem", "SetProperty").append2(
            "Online",
            arg::Variant(true),
        );
        self.send_and_block(m);
    }

    pub fn dial(&self, num: &str)  {
        let m = self.method_call(".VoiceCallManager", "Dial").append2(
            num,
            "default",
        );
        self.send_and_block(m);
    }

    pub fn hangup_all(&self)  {
        let m = self.method_call(".VoiceCallManager", "HangupAll");
        self.send_and_block(m);
    }

    pub fn list_calls(&self) {
    /* see answer_all */
    }

    pub fn answer(&self, path : &str) {
        let m = self.method_call_at(path, ".VoiceCall", "Answer");
        self.send_and_block(m);
    }

    pub fn hangup(&self, path : &str) {
        let m = self.method_call_at(path, ".VoiceCall", "Hangup");
        self.send_and_block(m);
    }

    pub fn answer_all(&self) {
        let m = self.method_call(".VoiceCallManager", "GetCalls");
        let r = self.send_and_block(m);
        let res: Box<arg::RefArg> = r.get1().unwrap();
	println!("Call list:");
	for c in res.as_iter().unwrap() {
	    let p = c.as_iter().unwrap().next().unwrap().as_str().unwrap();
	    println!("Path: {:?}", p);
	    /* FIXME: should check that the call is incoming */
	    self.answer(p);
	}
    }

    pub fn send_sms(&self, num : &str, message : &str) {
        let m = self.method_call(".MessageManager", "SetProperty").append2(
	    "UseDeliveryReports",
	    arg::Variant(true),
	);
        let r = self.send_and_block(m);

        let m = self.method_call(".MessageManager", "SendMessage").append2(
	    num,
	    message,
	);
        let r = self.send_and_block(m);

	let res: Box<arg::RefArg> = r.get1().unwrap();
	let path = res.as_str().unwrap().to_owned();

	println!("Message path {}", path);
	//self.signal_loop("MessageRemoved");
    }

    fn get_netreg_props(&self) -> NetReg {
        let m = self.method_call(".NetworkRegistration", "GetProperties");
        let r = self.send_and_block(m);

	NetReg{ props: message_to_Props(r) }
    }

    fn get_call_props(&self, path : String) -> Call {
        let m = self.method_call_at(&path, ".VoiceCall", "GetProperties");
        let r = self.send_and_block(m);

	Call{ props: message_to_Props(r), path: path }
    }

    pub fn translate_message(&self, msg: dbus::Message) -> (PhoneMsg, String) {
            //println!("Have incoming message... {:?}", msg);
	    let iface = &*msg.interface().unwrap();
 	    let signal = &*msg.member().unwrap();

            //println!("... interface {:?} / {:?}", iface, signal);
 	    /* 0123456789
	       org.ofono. */
	    if &iface[..10] != "org.ofono." { return (PhoneMsg::Nomsg, "(not ofono)".to_string()); }
	    let iface = &iface[10..];
	    if iface == "NetworkRegistration" && signal == "PropertyChanged" {
		return (PhoneMsg::NetRegChanged(self.get_netreg_props()), signal.to_string());
	    }
	    if iface == "MessageManager" && signal == "IncomingMessage" {
	        return (PhoneMsg::IncomingSMS(message_to_SMS(&msg)), signal.to_string());
	    }
	    if iface == "VoiceCallManager" {
 		let res: Box<arg::RefArg> = msg.get1().unwrap();
		let path = res.as_str().unwrap().to_owned();

		if signal == "CallAdded" || signal == "PropertyChanged" {		
		    let s = self.get_call_props(path);
 		    return (PhoneMsg::CallChanged(s), signal.to_string());
		} else {
 		    return (PhoneMsg::CallEnded(path.to_string()), signal.to_string());
		}
	    }
	    if iface == "Message" {
	        println!("Message, signal: {}", signal);
	        if signal == "PropertyChanged" {
 		    return (PhoneMsg::Note(format!("Message property changed.")), signal.to_string());
		}
	    }
	    if iface == "MessageManager" {
	        if signal == "MessageRemoved" {
 		    return (PhoneMsg::Note(format!("Message removed, probably sent?")), signal.to_string());
		}
	    }

	    (PhoneMsg::Nomsg, signal.to_string())
    }

    pub fn handle_one(&self, msg: dbus::Message, listener : &mut impl PhoneListener) -> String {
    	let (event, note) = self.translate_message(msg);

	match (event) {
	PhoneMsg::NetRegChanged(n) => listener.netreg_changed(n),
	PhoneMsg::CallChanged(s) => listener.call_changed(s),
	PhoneMsg::CallEnded(s) => listener.call_ended(&s),
	PhoneMsg::IncomingSMS(s) => listener.incoming_sms(s),
	PhoneMsg::Note(s) => listener.printl(&s),
	PhoneMsg::Nomsg => {},
	}

	return note;
    }

    pub fn get_initial_state(&self, listener : &mut impl PhoneListener) {
 	let n = self.get_netreg_props();
 	listener.netreg_changed(n);
    }

    pub fn signal_loop(&self, listener : &mut impl PhoneListener, end_on : &str)  {
        let c = &self.conn;

        listener.printt("Connecting (signal handlers)");

	self.get_initial_state(listener);

        // Wait for the signal to arrive.
        for msg in c.incoming(1000000) {
	    let signal = self.handle_one(msg, listener);
	    if signal == end_on {
	        return;
	    }
        }
    }

    pub fn message_poll(&self) -> (PhoneMsg, String) {
         let c = &self.conn;

	 for r in c.iter(1) {
 	     match r {
	     	 ConnectionItem::Nothing  => { println!("got nothing"); break; }
		 ConnectionItem::MethodCall(msg) |
		 ConnectionItem::Signal(msg) |
		 ConnectionItem::MethodReturn(msg) => { return self.translate_message(msg); }
 		 _ => { println!("Got something else? Should not happen"); }
 	     }
	 }

	 return (PhoneMsg::Nomsg, "(no message ready)".to_string());
    } 

    pub fn signal_poll(&self, l : &mut impl PhoneListener) {
         let c = &self.conn;

	 // Thanks to:
 	 // https://github.com/diwic/dbus-rs/issues/84

	 for r in c.iter(1) {
	     match r {
	     	 ConnectionItem::Nothing  => { println!("got nothing"); break; }
		 ConnectionItem::MethodCall(msg) |
		 ConnectionItem::Signal(msg) |
		 ConnectionItem::MethodReturn(msg) => { self.handle_one(msg, l); break; }
 		 _ => { println!("Got something else? Should not happen"); }
 	     }
	 }
    }

    pub fn tests(&self, l : &mut impl PhoneListener) -> Result<(), dbus::Error> {
        self.modem_online();
        self.get_netreg_props();

        for i in 0..100 {
	     self.signal_poll(l);
	}

	self.signal_loop(l, "");
        Ok(())
    }

    pub fn one_command(&self, cmd : PhoneCmd) {
    }

    pub fn monitor(&self, listener : &mut impl PhoneListener)  {
        listener.printt("Ready.");
	self.signal_loop(listener, "");
    }

    pub fn connect_phone() -> Result<Phone, dbus::Error> {
        let c = Connection::get_private(BusType::System)?;
    
        let m = Message::new_method_call("org.ofono", "/", "org.ofono.Manager", "GetModems").unwrap();
        let r = c.send_with_reply_and_block(m, 2000)?;
        let res: Box<arg::RefArg> = r.get1().unwrap();
        //println!("complete reply: {:?}", res);
    
        //let mut ai = res.as_iter().unwrap();
        //println!("path?: {:?}", ai.next());
    
        let first_modem = res.as_iter().unwrap().next().unwrap();
        let first_modem_path = first_modem
            .as_iter()
            .unwrap()
            .next()
            .unwrap()
            .as_str()
            .unwrap();

        //    let mstr = IR::match_str(Some(&"org.ofono.MessageManager".into()), None);
        //    let mstr = IA::match_str(Some(&"org.ofono.VoiceCallManager".into()), None);
        //    let mstr = "interface='org.ofono.VoiceCallManager',member='CallRemoved'";
        //    let mstr = "interface='org.ofono.VoiceCallManager'";
        let mstr = "";
    	c.add_match(&mstr).unwrap();
    
        Ok(Phone {
            conn: c,
            first_modem_path: first_modem_path.to_string(),
        })
    }

    pub fn new() -> Phone {
    	Phone::connect_phone().unwrap()
    }
}

//lazy_static! {
//    static ref single_phone : Option<& 'static Phone> = None;
//}

//pub static single_phone : Mutex<Option<Phone>> = Mutex::new(None);

//pub fn init_phone() {
//    let mut p = single_phone.lock().unwrap();
//    *p = Some(Phone::new());
//}

//pub fn phone() -> & 'static Phone {
//    &single_phone.unwrap()
//}