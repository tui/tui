Distribute under GPLv2+

# Why?

We already have a kernel running on cellphones (Linux, Nokia N900 is
nicely supported, Motorola Droid 4 is getting there), operating system
(Devuan) and graphical user interface suitable for phone (Maemo Leste
-- 1). We even have a modem driver (Ofono). What we don't have is user
interface for Ofono. Which means you can do calls and send and receive
text messages ... from command line.

I'm trying to change that.

# Goals

One day, this should turn into "Unicsy Phone", phone support that
integrates well with the rest of the Unix system. Phone functionality
should be available both from command-line and from simple
GUI. Important data should be stored in plain-text formats that are
easy to access with common software.

SMS messages should be handled in similar way e-mails currently are --
stored in maildir or mailbox formats, and accessible by common email
clients. Access by two or more clients at the same time should be
possible. Of course, special GUI client should be provided to work
with SMS (using same maildir backend).

Simple text format should be supported for contacts -- probably as
simple as number '\t' name. Many users will want something a bit more
advanced, I'd prefer hierarchical structure offered by emacs "org"
mode, allowing me to group contacts together. We should call external
program to list contacts for us, allowing easy merging of contacts
from multiple sources. "ph book" should provide the merged list.

# Design

Non-GUI component should be responsible for maintaining call log and
incoming SMSes. History plugin in ofono can probably do this. Ideally,
GSM should go offline when this component fails, so that SMSes are
queued by the network.

There should be "ph primary" component responsible for calls and
incoming SMSes -- mostly for actions not initiated by local user. On
incoming events, it should play a melody. For calls, it should set up
mixers, and display a window allowing user to manipulate volume,
answer, reject and terminate the call. For incoming message,
notification window should be displayed.

"ph monitor" should be usable for monitoring phone status from the
command line, displaying current network and signal status, any
incoming calls and messages.

There should be a GUI component for doing actions for local user. It
should allow basic control (online/airplane mode, starting data
connection), allow placing phone calls (but audio will be controlled
and call window will still be displayed by "ph primary"), and sending
SMS messages.