extern crate sdl2;
extern crate rand;

use sdl2::event::Event;
use sdl2::pixels;
use sdl2::keyboard::Keycode;

use sdl2::gfx::primitives::DrawRenderer;

use sdl2::pixels::Color;

use sdl2::rect::Rect;
use sdl2::rect::Point;

use std::time::Duration;
use std::collections::HashSet;

use rand::Rng;

const SCREEN_WIDTH: u32 = 1016;
const SCREEN_HEIGHT: u32 = 690;

struct Thing {
       posx: f32,   /* Horse */
       posy: f32,
       angle: f32,
       speed: f32,
}

pub fn to_polar(angle : f32, dist : f32) -> (f32, f32) {
      let a = angle.to_radians();
      ( dist * f32::sin(a), -dist * f32::cos(a))
}

impl Thing {
    fn convert_pos(&self) -> (f32, f32) {
        (500. + self.posx, 350. + self.posy)
    }

    fn to_polar(&self, angle : f32, dist : f32) -> (i16, i16) {
        let (x, y) = to_polar(angle, dist * 30.);
	let (xo, yo) = self.convert_pos();
	((x+xo) as i16, (y+yo) as i16)
    }
    
    fn paint_arrow(&mut self, canvas : &mut sdl2::render::Canvas<sdl2::video::Window>) {
        let dir = self.angle;

	let width = 30.;
	let a1 : f32 = dir + 180. - width;
	let a2 : f32 = dir + 180. + width;
	let color = pixels::Color::RGB(255, 200, 200);

	let (x1, y1) = self.to_polar(a1, 0.8); 
	let (x2, y2) = self.to_polar(dir, 0.8);  
	let (x3, y3) = self.to_polar(a2, 0.8);   
	let (x4, y4) = self.to_polar(dir, -0.4); 

	canvas.line(x1, y1, x2, y2, color);
	canvas.line(x2, y2, x3, y3, color);
	canvas.line(x3, y3, x4, y4, color);
 	canvas.line(x4, y4, x1, y1, color);
    }

    fn paint(&mut self, canvas : &mut sdl2::render::Canvas<sdl2::video::Window>) {
        self.paint_arrow(canvas);
    }

    fn tick(&mut self, tick: f32) {
        if self.speed < 0. { self.speed = 0.; }
        if self.speed > 1. { self.speed = 1.; }	
    
        let (x, y) = to_polar(self.angle, self.speed * 1000. * tick * 5.);
        self.posx += x;
	self.posy += y;
    }
}

struct State {
       stx: f32,    /* Stability */
       sty: f32,

       cmdx: f32,
       cmdy: f32,

       horse: Thing,

       keys_down: HashSet<Keycode>,
}

impl State {
    fn convert_stability(&self, x : f32, y : f32) -> (i32, i32) {
        ( ((x*200.)+220.) as i32, ((y*200.)+220.) as i32 )
    }

    fn input_mouse(&mut self, x : i32, y : i32) {
        if x < 20 || x > 420 || y < 20 || y > 420 {
	   self.cmdx = 0.;
	   self.cmdy = 0.;
	} else {
	   self.cmdx = ((x as f32)-220.)/200.;
	   self.cmdy = ((y as f32)-220.)/200.;
	}
    }
    
    fn paint_base(&mut self, canvas : &mut sdl2::render::Canvas<sdl2::video::Window>) {
        canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
    	canvas.clear();

        canvas.set_draw_color(Color::RGB(30, 30, 30));
	let (x1, y1) = self.convert_stability(-1., -1.);
	let (x2, y2) = self.convert_stability(1., 1.);
	let r = Rect::new(x1, y1, (x2-x1) as u32, (y2-y1) as u32);
    	canvas.fill_rect(r);

	canvas.set_draw_color(Color::RGB(255, 0, 0));
	let (x1, y1) = self.convert_stability(self.stx, self.sty);
    	canvas.fill_rect(Rect::new(x1, y1, 20, 20));

	canvas.set_draw_color(Color::RGB(0, 255, 0));
	let (x1, y1) = self.convert_stability(self.cmdx, self.cmdy);
    	canvas.fill_rect(Rect::new(x1, y1, 20, 20));
    }

    fn paint(&mut self, canvas : &mut sdl2::render::Canvas<sdl2::video::Window>) {
        self.paint_base(canvas);
	self.horse.paint(canvas);
	canvas.present();
    }

    fn stability_tick(&mut self, tick: f32) {
	let scale = 10.;
	let x: f32 = rand::thread_rng().gen_range(-tick*scale, tick*scale);
	let y: f32 = rand::thread_rng().gen_range(-tick*scale, tick*scale);

	self.stx += self.stx * tick * scale;
	self.sty += self.sty * tick * scale;

	self.stx -= self.cmdx * tick * scale * 3.;
	self.sty -= self.cmdy * tick * scale * 3.;

	self.stx += x;
	self.sty += y;
    }

    fn tick(&mut self, tick: f32) {
        self.stability_tick(tick);
	self.key_tick(tick);
	self.horse.tick(tick);
    }

    fn key_tick(&mut self, tick: f32) {
        if self.keys_down.contains(&Keycode::F) { self.horse.speed -= 10. * tick; }
        if self.keys_down.contains(&Keycode::R) { self.horse.speed += 10. * tick; }
        if self.keys_down.contains(&Keycode::D) { self.horse.angle -= 1000.0 * tick; }
        if self.keys_down.contains(&Keycode::G) { self.horse.angle += 1000.0 * tick; }
    }
}

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsys = sdl_context.video().unwrap();
    let window = video_subsys.window("Horsim", SCREEN_WIDTH, SCREEN_HEIGHT)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    let mut state = State { stx: 0.0, sty: 0.0, cmdx: 0.0, cmdy: 0.0, horse : Thing {
    	                    posx: 0.0, posy: 0.0, angle: 0.0, speed: 0.0 },
			    keys_down: HashSet::new()};

    state.paint(&mut canvas);

    let mut timer = sdl_context.timer().unwrap();

    let mut lastx = 0;
    let mut lasty = 0;

    let mut events = sdl_context.event_pump().unwrap();

    'main: loop {
        for event in events.poll_iter() {

            match event {

                Event::Quit {..} => break 'main,

                Event::KeyDown {keycode: Some(keycode), ..} => {
                    if keycode == Keycode::Escape {
                        break 'main
                    } else {
		        state.keys_down.insert(keycode);
		    }
                }

                Event::KeyUp {keycode: Some(keycode), ..} => {
 		    state.keys_down.remove(&keycode);
		}

		Event::MouseMotion {x, y, ..} => {
		    state.input_mouse(x, y);
		}

                _ => { println!("Unhandled event {:?}", event); }
            }
        }

	//let ticks = timer.ticks() as i32;

	let tick = 0.001;
	state.tick(tick);
 	state.paint(&mut canvas);

	std::thread::sleep(Duration::from_millis((tick * 1000. * 5.) as u64));
    }
}
