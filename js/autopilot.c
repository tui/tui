// -*- linux-c -*- 
// gcc autopilot.c -l hidapi-hidraw && sudo ./a.out
// sudo apt install libhidapi-dev

// xinput disable XX

#include <stdio.h> // printf
#include <wchar.h> // wchar_t
#include <string.h>
#include <stdbool.h>

#include <hidapi/hidapi.h>

#include <time.h>

#define MAX_STR 255

static int one;

int msleep(int v)
{
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec = v * 1000000;

	nanosleep(&ts, NULL);
	return 0;
}

unsigned char map(char c)
{
	switch (c) {
	case ' ': return 0xf;
	case '-': return 0xe;
	}
	return c - '0';
}

void display(hid_device *handle, char *msg, char leds)
{
	int res, i;
	unsigned char buf[65];

	/* Use msg = "          " to completely clear the display */

	buf[0] = 0; /* 0; 1+ immediately sets first digits and shift the rest. Useless? */
	/* digits: high bits ignored, 0xe -, 0xf " " */
	 /* ALT, 5 digits, VS, 5 digits */
	for (i=0; i<10; i++)
		buf[i+1] = map(msg[i]);
	buf[11] = leds; /* button light, 1..ap, 2..hdg, 4..nav, .., 32..vs, 64..??, 128..rev */
	buf[12] = 0;
	buf[13] = 0;
	buf[14] = 0;
	buf[15] = 0;

	//res = hid_write(handle, buf, 65);
	/* 16 seems to be right length */
	/* buf[0] = 1, 3 accepts 4 bytes */
	res = hid_send_feature_report(handle, buf, 12); 
}

struct state {
	int mode;
#define TIME 0
#define WORK 1
#define CD 2
#define STW 3
#define BLANK 4
	int timeout;
	int blink;
	int expire;
	int alarm;
	int stopwatch;
	int stoptime;
	int worktot;
	int workstart;
	int work_up;
	int work_down;
} state;

int countdown_active(void)
{
	if (!state.expire)
		return 0;
	int act = state.expire - time(NULL);
	if (act < state.timeout - 3)
		return act;
	return 0;
}

int get_leds(void)
{
	int r = 0;
	if (countdown_active())
		r |= 8 * !!state.blink;
	if (state.stopwatch)
		r |= 2 * !!state.blink;
	if (state.stoptime)
		r |= 2;
	if (state.workstart)
		r |= 32;
	return r | state.alarm;
}

void fmt_time(char *buf)
{
	// Get the current time
	time_t currentTime = time(NULL);

	// Convert to local time format
	struct tm *localTime = localtime(&currentTime);

	// Extract hours and minutes
	int hour = localTime->tm_hour;
	int minute = localTime->tm_min;
	int sec = localTime->tm_sec;

	// Print the result
	sprintf(buf, "%2d%02d -- %02d", hour, minute, sec);
}

void puttime(char *s1, char *s2)
{
	// Get the current time
	time_t currentTime = time(NULL);

	// Convert to local time format
	struct tm *localTime = localtime(&currentTime);

	// Extract hours and minutes
	int hour = localTime->tm_hour;
	int minute = localTime->tm_min;
	int sec = localTime->tm_sec;

	// Print the result
	printf("%s%02d%02d%s", s1, hour, minute, s2);
}

void disp_time(hid_device *handle)
{
	char buf[12];
	fmt_time(buf);
	display(handle, buf, get_leds());
}

void but_read(hid_device *handle)
{
	char buf[256];
	static char mask[256];
	const int debug = 0;

	int res = hid_read_timeout(handle, buf, sizeof(buf), 1000);
        if (res < 0) {
		fprintf(stderr, "Failed to read from device\n");
		return;
        }
	if (res == 0)
		return;

	if (debug)
		printf("%d ", res);

	for (int i = 0; i < 3; i++) {
		if (debug)
			printf("%02x ", buf[i] & 0xff);
		mask[i] &= buf[i];
		buf[i] &= ~mask[i];
	}

	if (debug)
		printf("\n");

	/* buf[0] ~ 0x1f: left select knob
	            0x60: +- knob
	            0x80: AP button
	   buf[1] ~ 0x7f: HDG ... REV buttons
	            0x80: AUTO throttle switch
	   buf[2] ~ 0x0c: pitch trim
	            0x03: flaps up/down
	*/
	// Optional: check for scroll wheel in buf[3] if present

	if (buf[0] & 0x01)
		state.mode = TIME;
	if (buf[0] & 0x02)
		state.mode = WORK;
	if (buf[0] & 0x04)
		state.mode = CD;
	if (buf[0] & 0x08)
		state.mode = STW;
	if (buf[0] & 0x10)
		state.mode = BLANK;
	if (buf[1] & 0x04)
		state.alarm &= ~0x08;
	if (buf[1] & 0x01) {
		if (!state.stopwatch)
			state.stopwatch = time(NULL);
		else {
			if (state.stoptime) {
				state.stopwatch = 0;
				state.stoptime = 0;
			} else
				state.stoptime = time(NULL);
		}
	}
	if (buf[1] & 0x10) {
		if (!state.workstart) {
			state.workstart = time(NULL);
			puttime("", "-\n");
		} else {
			state.worktot += time(NULL) - state.workstart;
			state.workstart = 0;
			puttime("-", "\n");
		}
	}
	if (state.mode == CD) {
		int reset = 0;
		if (buf[0] & 0x20) {
			state.timeout+=10;
			mask[0] |= 0x20;
			reset = 1;
		}
		if (buf[0] & 0x40) {
			state.timeout-=10;
			mask[0] |= 0x40;
			reset = 1;
		}
		if (buf[2] & 0x04) {
			state.timeout+=60;
			mask[2] |= 0x04;
			reset = 1;
		}
		if (buf[2] & 0x08) {
			state.timeout-=60;
			mask[2] |= 0x08;
			reset = 1;
		}
		if (buf[2] & 0x01) {
			state.timeout+=1;
			reset = 1;
		}
		if (buf[2] & 0x02) {
			state.timeout-=1;
			reset = 1;
		}
		if (state.timeout < 0)
			state.timeout = 0;
		if (reset)
			state.expire = time(NULL) + state.timeout;
	} else {
		if (buf[2] & 0x01) {
			state.work_up+=1;
			puttime("  ", " up\n");
		}
		if (buf[2] & 0x02) {
			state.work_down+=1;
			puttime("  ", " down\n");
		}
	}
}

char fmt_w(int num)
{
	if (num == 0)
		return ' ';
	if (num < 8) {
		return num + '0';
	}
	return '9';
}

int main(int argc, char* argv[])
{
	int res;
	unsigned char buf[65];
	wchar_t wstr[MAX_STR];
	hid_device *handle;
	int i;
	unsigned long t = 0;
	unsigned long u = 0;
	const int debug = 0;

	// Initialize the hidapi library
	res = hid_init();

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	handle = hid_open(0x6a3, 0x0d06, NULL);
	if (!handle) {
		printf("Unable to open device\n");
		hid_exit();
 		return 1;
	}

	// Read the Manufacturer String
	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	if (debug)
		printf("Manufacturer String: %ls\n", wstr);

	// Read the Product String
	res = hid_get_product_string(handle, wstr, MAX_STR);
	if (debug)
		printf("Product String: %ls\n", wstr);

	// Read the Serial Number String
	res = hid_get_serial_number_string(handle, wstr, MAX_STR);
	if (debug)
		printf("Serial Number String: (%d) %ls\n", wstr[0], wstr);

	// Read Indexed String 1
	res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	if (debug)
		printf("Indexed String 1: %ls\n", wstr);

	msleep(20);

	while (1) {
		char buf[16];
		//display(handle, "0000000000", u);
		switch (state.mode) {
		case TIME:
			disp_time(handle);
			break;
		case WORK: {
			char buf[12];
			int act = time(NULL) - state.workstart;
			if (!state.workstart)
				fmt_time(buf);
			else
				sprintf(buf, "%2d%02d", act/60, act%60);

			act = state.worktot;
			sprintf(buf+4, "%c%2d%02d%c", fmt_w(state.work_up), act/60, act%60, fmt_w(state.work_down));
			display(handle, buf, get_leds());
			break;
		}
		case CD: {
			int act = countdown_active();
			if (!act)
				sprintf(buf, "%3d%02d     ", state.timeout/60, state.timeout%60 );
			else
				sprintf(buf, "%3d%02d     ", act/60, act%60);
			display(handle, buf, get_leds());
			break;
		}
		case STW: {
			int act = time(NULL) - state.stopwatch;
			if (state.stoptime)
				act = state.stoptime - state.stopwatch;
			if (!state.stopwatch)
				sprintf(buf, "         0");
			else
				sprintf(buf, "%3d%02d     ", act/60, act%60);
			display(handle, buf, get_leds());
			break;
		}
		case BLANK:
			sprintf(buf, "          ");
			display(handle, buf, get_leds());
			break;
		}

		if (debug)
			printf("t %d, res: %d\n", t, res);
		t++;
		u = ~u;

		but_read(handle);
		state.blink = time(NULL) & 1;
		if (countdown_active() && time(NULL) > state.expire) {
			printf("\aAlarm timeout\n");
			state.expire = 0;
			state.alarm |= 8;
		}
	}

	// Close the device
	hid_close(handle);

	// Finalize the hidapi library
	res = hid_exit();

	return 0;
}
