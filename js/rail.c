// -*- linux-c -*-
// Driver for
// PI Engineering RailDriver Modern Desktop

// gcc rail.c -l hidapi-hidraw && sudo ./a.out
// sudo apt install libhidapi-dev

//https://github.com/openrails/openrails/blob/master/Source/ORTS.Common/Input/RailDriverDevice.cs


#include <stdio.h> // printf
#include <wchar.h> // wchar_t
#include <string.h>

#include <hidapi/hidapi.h>

#include <time.h>

#define MAX_STR 255

static int one;

int msleep(int v)
{
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = v * 1000000;

    nanosleep(&ts, NULL);
    return 0;
}


void display(hid_device *handle, unsigned char v1, unsigned char v2, unsigned char v3)
{
	int res;
	unsigned char buf[65];

	buf[0] = 0;
	buf[1] = 134;
	buf[2] = v1;
	buf[3] = v2;
	buf[4] = v3;
	res = hid_write(handle, buf, 5);
	msleep(30);

	// Read requested state
//	res = hid_read(handle, buf, 65);
}

int main(int argc, char* argv[])
{
	int res;
	unsigned char buf[65];
	wchar_t wstr[MAX_STR];
	hid_device *handle;
	int i;
	unsigned long t = 0;
	const int debug = 0;

	// Initialize the hidapi library
	res = hid_init();

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	//handle = hid_open(0x4f2, 0x111, NULL);
	handle = hid_open(0x5f3, 0x00d2, NULL);
	if (!handle) {
		printf("Unable to open device\n");
		hid_exit();
 		return 1;
	}

	// Read the Manufacturer String
	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	if (debug)
		printf("Manufacturer String: %ls\n", wstr);

	// Read the Product String
	res = hid_get_product_string(handle, wstr, MAX_STR);
	if (debug)
		printf("Product String: %ls\n", wstr);

	// Read the Serial Number String
	res = hid_get_serial_number_string(handle, wstr, MAX_STR);
	if (debug)
		printf("Serial Number String: (%d) %ls\n", wstr[0], wstr);

	// Read Indexed String 1
	res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	if (debug)
		printf("Indexed String 1: %ls\n", wstr);

	msleep(20);
	display(handle, 0xff, 0xff, 0xff);

	while (1) {
		display(handle, t >> 16, t >> 8, t);
		t++;
		msleep(10);
		// Read requested state
		res = hid_read(handle, buf, 65);

		// Print out the returned buffer.
		// 0..3: throttles
		// 4..: ind brake left-right movement
		// 5..6: wiper/light selectors, analog!
		// 7..10: blue buttons, left to right
		// 10..11: blue app/down,arrows
		// 11..12: alert/sand block
		for (i = 0; i < 13; i++)
			printf("buf[%d]: %d\n", i, buf[i]);
	}

	// Close the device
	hid_close(handle);

	// Finalize the hidapi library
	res = hid_exit();

	return 0;
}
