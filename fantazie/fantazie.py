#!/usr/bin/env python3

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time
import copy

import rotatable

class AppsWindow(rotatable.Rotatable):
    def __init__(m):
        rotatable.Rotatable.__init__(m)

    def interior(m):
        table = gtk.Table(1,2,True)
        b = {}
        for i in range(3):
            button = gtk.Button()

            name = "/data/picture/horses/zernovka112/DCP_1419.JPG"
            pixbuf = gtk.gdk.pixbuf_new_from_file(name)
            pixbuf = pixbuf.scale_simple(232, 600, gtk.gdk.INTERP_BILINEAR)
            image = gtk.image_new_from_pixbuf(pixbuf)
            #image.set_from_file(name)
            button.add(image)
                        
            table.attach(button, 5*i,5*i+5,1,13)
            b[i] = button

        b[0].connect("clicked", lambda w: sys.exit(100))
        b[1].connect("clicked", lambda w: sys.exit(101))
        b[2].connect("clicked", lambda w: sys.exit(102))

        close = gtk.Button("Close")
        close.connect("clicked", lambda w: sys.exit(200))
        table.attach(close, 13,15, 0,1)

        return table


if __name__ == "__main__":
    test = AppsWindow()
    test.basic_main_window()
    gtk.main()
