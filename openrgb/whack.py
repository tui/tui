#!/usr/bin/python3

import sys
import termios
import signal
import random

def getchar():
    old = termios.tcgetattr(sys.stdin)
    cbreak = old.copy()
    cbreak[3] &= ~(termios.ECHO|termios.ICANON)
    cbreak[6][termios.VMIN] = 1
    cbreak[6][termios.VTIME] = 0
    termios.tcsetattr(sys.stdin,termios.TCSADRAIN,cbreak)
    char = sys.stdin.read(1)
    termios.tcsetattr(sys.stdin,termios.TCSADRAIN,old)
    return char

import openrgb

class ORGB:
    def __init__(m):
        o = openrgb.OpenRGB('localhost')
        o.client_name("start")
        oc = o.controller_data()
        print(oc.leds[0].name)
        m.leds = oc.leds

    def set_all(m, c):
        for l in m.leds:
            l.set(c)

    def print_keys(m):
        for l in m.leds:
            print(l.name)

class Whack:
    def __init__(m):
        print("Init...")
        m.o = ORGB()
        m.o.set_all((0,0,0))
        m.keys = "`1234567890-=QWERTYUIOP[]ASDFGHJKL;'ZXCVBNM,./"
        m.leds = {}
        m.state = {}
        for c in m.keys:
            found = False
            for l in m.o.leds:
                if l.name == "Key: "+c:
                    m.leds[c] = l
                    m.state[c] = 0
                    found = True
            if not found:
                print("Don't have led for", c)
        for l in m.leds:
            m.leds[l].set((0,0,0))

    def scale(m, v):
        if v == 1: return 10
        if v == 2: return 20
        if v == 3: return 40
        if v == 4: return 80
        if v == 5: return 255
        return 0

    def grow(m):
        r = random.choice(m.keys)
        m.state[r] += 1
        s = m.state[r]
        if s > 5:
            print("Game over")
            m.over = 1
        if s < 6:
            m.leds[r].set((0, m.scale(s), 0))

    def play(m):
        m.score = 0
        m.over = 0
        while not m.over:
            signal.setitimer(signal.ITIMER_REAL, 0.2)
            try:
                c = getchar()
            except TimeoutException:
                c = ""
                print("Timeout")
            print("Whack: ", c)
            if c == '\n':
                break
            if c == "":
                m.grow()
                continue
            if not m.state[c]:
                for i in range(10): m.grow()
            else:
                m.score += 1
            m.state[c] = 0
            m.leds[c].set((0, 0, 0))

        print("Score", m.score)
        m.o.set_all((0,50,0))
            


if 0:        
    o = ORGB()
    o.set_all((25,0,0))
    o.leds[0].set((40,40,40))
    o.print_keys()

class TimeoutException(Exception):
    pass

def timeout_handler(signum, frame):
    raise TimeoutException

# Set the timeout handler for the alarm signal
signal.signal(signal.SIGALRM, timeout_handler)

w = Whack()
w.play()
