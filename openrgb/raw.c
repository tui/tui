// -*- linux-c -*- 
// gcc raw.c -l hidapi-hidraw && sudo ./a.out
// sudo apt install libhidapi-dev


#include <stdio.h> // printf
#include <wchar.h> // wchar_t
#include <string.h>

#include <hidapi/hidapi.h>

#include <time.h>

static int one;

int msleep(int v)
{
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = v * 1000000;

    nanosleep(&ts, NULL);
    return 0;
}


#define MAX_STR 255

static unsigned char keys[] = {0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14,
                              0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x20, 0x21, 0x22,
                              0x23, 0x24, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30,
                              0x31, 0x32, 0x33, 0x34, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3E, 0x3F, 0x41,
                              0x44, 0x45, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x51, 0x54, 0x55,
                              0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5E, 0x5F, 0x61, 0x64, 0x65, 0x68, 0x69, 0x6A,
                              0x6B, 0x6C, 0x6E, 0x6F, 0x74, 0x75, 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E,
                              0x7F, 0x81, 0x84, 0x85, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x91,
                              0x94, 0x95 };

void send_effect(hid_device *handle)
{
	unsigned char buf[264];
	unsigned char   profile = 1;
	unsigned char   mode = 0x01;  // 0x01 HYPERX_ALLOY_ELITE_MODE_STATIC
	unsigned char   direction = 0;
	unsigned char   reactive_mode = 0xff; // NONE
	unsigned char   speed = 0;
	unsigned char   color_mode = 0x00; // HYPERX_ALLOY_ELITE_COLOR_MODE_SINGLE
	unsigned char   red_single = 0;
	unsigned char   grn_single = 0;
	unsigned char   blu_single = 0;
	unsigned char   red_dual_1 = 0;
	unsigned char   grn_dual_1 = 0;
	unsigned char   blu_dual_1 = 0;
	unsigned char   red_dual_2 = 0;
	unsigned char   grn_dual_2 = 0;
	unsigned char   blu_dual_2 = 0;

	/*-----------------------------------------------------*\
	  | Zero out buffer                                       | 
	  \*-----------------------------------------------------*/
	memset(buf, 0x00, sizeof(buf));

	/*-----------------------------------------------------*\ 
	  | Set up Effect packet                                  | 
	  \*-----------------------------------------------------*/
	buf[0x00]   = 0x07;
	buf[0x01]   = 0x02; // HYPERX_ALLOY_ELITE_PACKET_ID_SET_EFFECT;
	buf[0x02]   = profile;

	/*-----------------------------------------------------*\ 
	  | Set mode                                              | 
	  \*-----------------------------------------------------*/
	buf[0x09]   = 0x01;
	buf[0x0A]   = mode;

	/*-----------------------------------------------------*\                                     
	  | Set direction                                         |                                     
	  \*-----------------------------------------------------*/
	buf[0x0D]   = direction;
	buf[0x0E]   = direction;

	/*-----------------------------------------------------*\                                     
	  | Set reactive mode                                     |                                     
	  \*-----------------------------------------------------*/
	buf[0x1B]   = reactive_mode;
	buf[0x1C]   = reactive_mode;
	buf[0x1D]   = reactive_mode;
	buf[0x1E]   = reactive_mode;
	buf[0x1F]   = reactive_mode;
	buf[0x20]   = reactive_mode;
	buf[0x21]   = reactive_mode;
	buf[0x22]   = reactive_mode;

	/*-----------------------------------------------------*\                                     
	  | Set mode-specific colors                              |                                     
	  \*-----------------------------------------------------*/
	buf[0x29]   = red_single;
	buf[0x41]   = grn_single;
	buf[0x59]   = blu_single;
	buf[0x2A]   = red_dual_1;
	buf[0x42]   = grn_dual_1;
	buf[0x5A]   = blu_dual_1;
	buf[0x2B]   = red_dual_2;
	buf[0x43]   = grn_dual_2;
	buf[0x5B]   = blu_dual_2;

	buf[0x6B]   = 0x09;
	buf[0x6C]   = 0x09;
	buf[0x6D]   = 0x05;
	buf[0x6E]   = 0x05;
	buf[0x6F]   = 0x06;
	buf[0x70]   = 0x05;

	/*-----------------------------------------------------*\                                     
	  | Set speed                                             |                                     
	  \*-----------------------------------------------------*/
	buf[0x71]   = speed;

	buf[0x72]   = 0x09;

	/*-----------------------------------------------------*\                                     
	  | Set color mode                                        |                                     
	  \*-----------------------------------------------------*/
	buf[0x73]   = color_mode;
	buf[0x74]   = color_mode;
	buf[0x75]   = color_mode;
	buf[0x76]   = color_mode;
	buf[0x77]   = color_mode;
	buf[0x78]   = color_mode;
	buf[0x79]   = color_mode;
	buf[0x7A]   = color_mode;

	printf("send_feature_report: 264\n"); fflush(stdout);

	/*-----------------------------------------------------*\ 
	  | Send packet                                           | 
	  \*-----------------------------------------------------*/
	hid_send_feature_report(handle, buf, 264);
}

void set_direct_color(hid_device *handle, int color, int val)
{
	unsigned char buf[264];

	/*-----------------------------------------------------*\ 
	  | Zero out buffer                                       | 
	  \*-----------------------------------------------------*/
	memset(buf, 0x00, sizeof(buf));

	/*-----------------------------------------------------*\ 
	  | Set up Direct packet                                  | 
	  \*-----------------------------------------------------*/

	for(int i = 0; i < sizeof(keys); i++)
	{
		buf[keys[i]] = val;
	}
	if (one && color==1)
		buf[keys[one]] = 0xff;
    
	buf[0x00]   = 0x07;
	buf[0x01]   = 0x16; // HYPERX_ALLOY_ELITE_PACKET_ID_DIRECT;
	buf[0x02]   = color; // HYPERX_ALLOY_ELITE_COLOR_CHANNEL_GREEN
	buf[0x03]   = 0xA0;

	printf("Set feature report -- direct\n"); fflush(stdout);
	hid_send_feature_report(handle, buf, 264);
}

int main(int argc, char* argv[])
{
	int res;
	unsigned char buf[65];
	wchar_t wstr[MAX_STR];
	hid_device *handle;
	int i;

	// Initialize the hidapi library
	res = hid_init();

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	//handle = hid_open(0x4f2, 0x111, NULL);
	handle = hid_open(0x951, 0x16be, NULL);
	if (!handle) {
		printf("Unable to open device\n");
		hid_exit();
 		return 1;
	}

	// Read the Manufacturer String
	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	printf("Manufacturer String: %ls\n", wstr);

	// Read the Product String
	res = hid_get_product_string(handle, wstr, MAX_STR);
	printf("Product String: %ls\n", wstr);

	// Read the Serial Number String
	res = hid_get_serial_number_string(handle, wstr, MAX_STR);
	printf("Serial Number String: (%d) %ls\n", wstr[0], wstr);

	// Read Indexed String 1
	res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	printf("Indexed String 1: %ls\n", wstr);

#if 0	
	// Toggle LED (cmd 0x80). The first byte is the report number (0x0).
	buf[0] = 0x0;
	buf[1] = 0x80;
	res = hid_write(handle, buf, 65);

	// Request state (cmd 0x81). The first byte is the report number (0x0).
	buf[0] = 0x0;
	buf[1] = 0x81;
	res = hid_write(handle, buf, 65);

	// Read requested state
	res = hid_read(handle, buf, 65);

	// Print out the returned buffer.
	for (i = 0; i < 4; i++)
		printf("buf[%d]: %d\n", i, buf[i]);
#endif

//	send_effect(handle);
#if 1
	while(1) {
	set_direct_color(handle, 1, 0);
	msleep(100);
	set_direct_color(handle, 2, 0);
	msleep(100);
	set_direct_color(handle, 3, 0);
	msleep(100);
	set_direct_color(handle, 4, 0);
	msleep(100);
	set_direct_color(handle, 1, 0);
	msleep(100);
	set_direct_color(handle, 2, 0);
	msleep(100);
	set_direct_color(handle, 3, 0);
	msleep(100);
	set_direct_color(handle, 4, 0);
	msleep(100);
	one++;
	if (one >= sizeof(keys))
		one = 0;
	}

#endif

	// Close the device
	hid_close(handle);

	// Finalize the hidapi library
	res = hid_exit();

	return 0;
}
