#!/usr/bin/python3

import sys
import termios
import signal
import random
import time

import openrgb

class ORGB:
    def __init__(m):
        o = openrgb.OpenRGB('localhost')
        o.client_name("start")
        oc = o.controller_data()
        print(oc.leds[0].name)
        m.leds = oc.leds
        m.sym = "`-=[];',./"
        m.alpha = "QWERTYUIOPASDFGHJKLZXCVBNM"
        m.num = "1234567890"

    def set_all(m, c):
        for l in m.leds:
            l.set(c)

    def print_keys(m):
        for l in m.leds:
            print(l.name)

    def set_name(m, c, co):
        found = False
        for l in m.leds:
            if l.name == "Key: "+c:
                l.set(co)
                found = True
        if not found:
            print("Don't have led for", c)

    def set_set(m, se, co):
        for c in se:
            m.set_name(c, co)

class Set:
    def __init__(m):
        print("Init...")
        m.o = ORGB()

        # Bad keys: in my case  Caps, asdfcvbn.
        # red: Shift/ End / C / F / 4like to blink
        # red: R blinks if num keys are lit
        # blue: 3 / X / del like to blink.
        # blue: caps / asdvbnm like to blink red when blue is used.

    def scan_bad(m):
        m.o.set_all((0, 0, 0))
        for l in m.o.leds:
            print(l.name)
            l.set((255, 0, 0))
            time.sleep(3)
            l.set((0, 255, 0))
            time.sleep(3)
            l.set((0, 0, 255))
            time.sleep(3)
            l.set((0, 0, 0))

    def run(m):
        m.o.set_all((255, 0, 0))
        c = (0, 255, 0)
        m.o.set_set(m.o.sym, c)
        m.o.set_set(m.o.num, c)
        c = (0, 0, 0)
        m.o.set_set(m.o.alpha, c)
        m.o.set_name("Caps Lock", c)        
        c = (0, 255, 0)
        m.o.set_name("Left Shift", c)
        m.o.set_name("Right Shift", c)
        m.o.set_name("Left Control", c)
        m.o.set_name("Right Control", c)
        c = (0, 255, 0)
        m.o.set_name("Tab", c)
        m.o.set_name("Space", c)        
        c = (0, 255, 0)
        m.o.set_name("Left Alt", c)
        m.o.set_name("Right Alt", c)
        m.o.set_name("Left Windows", c)
        m.o.set_name("Right Windows", c)
        c = (0, 255, 0)
        m.o.set_name("F1", c)
        m.o.set_name("F2", c)
        m.o.set_name("F3", c)
        m.o.set_name("F4", c)
        m.o.set_name("F5", c)
        m.o.set_name("F6", c)
        m.o.set_name("F7", c)
        m.o.set_name("F8", c)
        m.o.set_name("F9", c)
        m.o.set_name("F10", c)
        m.o.set_name("F11", c)
        m.o.set_name("F12", c)


s = Set()
#s.run()
s.scan_bad()
