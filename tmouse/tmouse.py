#!/usr/bin/python2
# -*- python -*-
# Needs "pip install evdev"
#
# On pmos, needs:
# apk add py2-pip linux-headers gcc g++ python2-dev
# pip install evdev

# DISPLAY=:0.0 xinput disable  "Atmel maXTouch Touchscreen"
# Does not work remotely?

# https://bigasterisk.com/darcs/?r=xtest;a=headblob;f=/xtest.pyx

from __future__ import print_function

import sys
sys.path += [ "/usr/share/unicsy/lib" ]

import os
from evdev import InputDevice, list_devices, ecodes
import time
import watchdog
import hardware
import select

import xtest

class InputHandler:
    def __init__(m):
        #m.devs = map(InputDevice, ('/dev/input/event7'))
        m.devs = [ InputDevice('/dev/input/event2') ]
        m.devs = {dev.fd: dev for dev in m.devs}
        print(m.devs)

        m.xt = xtest.XTest()
        m.reset()
        m.is_drag = False
        m.t_wheel = 0
        
    def reset(m):
        m.x = None
        m.y = None
        m.t = time.time()
        m.is_special = False

    def probe_devices(m):
        devices = [InputDevice(fn) for fn in list_devices()]
        for dev in devices:
            print(dev.fn, dev.name, dev.phys)

    def handle_x(m, x):
        if m.x:
            v = x - m.x
            if not m.is_special:
                m.xt.fakeRelativeMotionEvent(v, 0)
        else:
            print("First x", x)
            if x > 970:
                m.is_special = True
                m.scroll_y = m.y
        m.x = x

    def handle_y(m, y):
        if m.y:
            v = y - m.y
            if not m.is_special:
                m.xt.fakeRelativeMotionEvent(0, v)
            else:
                if time.time() - m.t_wheel > 0.4:
                    m.t_wheel = time.time()
                    if v > 0:
                        print("Wheel 4!")
                        m.xt.fakeButtonEvent(5, 1)
                        m.xt.fakeButtonEvent(5, 0)
                    if v < 0:
                        print("Wheel 5!")
                        m.xt.fakeButtonEvent(4, 1)
                        m.xt.fakeButtonEvent(4, 0)
        else:
            m.scroll_y = y
                        

        m.y = y
            
    def handle_event(m, event):
        if event.type == ecodes.EV_SW:
            m.handle_switch(event)

        if event.type == ecodes.EV_KEY:
            if event.code == ecodes.BTN_TOUCH:
                if event.value == 0:
                    if m.is_drag:
                        m.xt.fakeButtonEvent(1, 0)
                        m.is_drag = False
                    if time.time() - m.t < 0.1:
                        print("Click!")
                        m.xt.fakeButtonEvent(1, 1)
                        m.xt.fakeButtonEvent(1, 0)
                else:
                    if time.time() - m.t < 0.2:
                        print("Drag!")
                        m.xt.fakeButtonEvent(1, 1)
                        m.is_drag = True

                m.reset()

        if event.type == ecodes.EV_ABS:
            if event.code == ecodes.ABS_X:
                m.handle_y(-event.value)
            if event.code == ecodes.ABS_Y:
                m.handle_x(event.value)

    def run(m):
        # Thanks to https://python-evdev.readthedocs.io/en/latest/tutorial.html#reading-events-from-multiple-devices-using-select
        while True:
            r, w, x = select.select(m.devs, [], [])
            for fd in r:
                for event in m.devs[fd].read():
                    m.handle_event(event)

ih = InputHandler()
ih.run()
#print(ecodes.KEY[30])
#print(ecodes.bytype[ecodes.EV_KEY][30])
