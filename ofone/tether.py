#!/usr/bin/python3

import hardware
import os

def sy(s):
    os.system(s)

def tether_home_net():
#hardware.enable_access("/proc/sys/net/ipv4/ip_forward")
    hardware.enable_access("/etc/resolv.conf")
    sy("sudo ifconfig wlan0 10.0.0.251 up")
    sy("echo 'nameserver 8.8.8.8' > /etc/resolv.conf")

def enable_tether(iface):
    # On olpc it is enough to do "route add default gw..."
    # sudo route add default gw usb
    # echo 'nameserver 8.8.8.8' > /etc/resolv.conf
    sy("sudo bash -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'")
    sy("sudo iptables -t nat -A POSTROUTING -o %s -j MASQUERADE" % iface)

enable_tether("gprs0") # n900
enable_tether("wwan0") # d4

# wifi:
# sudo NetworkManager
# nm-applet

# on amd:
# 8.8.8.8 > resolv.conf
# sudo route add default gw usb


