#!/usr/bin/env python3

from gi.repository import Gtk, GLib
from gi.repository import AppIndicator3 as appindicator
from gi.repository import Notify as notify

class MyIndicator:
  def __init__(m, root):
    notify.init("Test")

    m.app = root
    m.ind = appindicator.Indicator.new(
                m.app.name,
                "indicator-messages",
                appindicator.IndicatorCategory.APPLICATION_STATUS)
    m.ind.set_status (appindicator.IndicatorStatus.ACTIVE)
    m.menu = Gtk.Menu()
    item = Gtk.MenuItem()
    item.set_label("Main Window")
    item.connect("activate", m.app.main_win.cb_show, '')
    m.menu.append(item)

    item = Gtk.MenuItem()
    item.set_label("Configuration")
    item.connect("activate", m.app.conf_win.cb_show, '')
    m.menu.append(item)

    item = Gtk.MenuItem()
    item.set_label("Test")
    item.connect("activate", m.cb_test, '')
    m.menu.append(item)
    
    item = Gtk.MenuItem()
    item.set_label("Exit")
    item.connect("activate", m.cb_exit, '')
    m.menu.append(item)

    m.menu.show_all()
    m.ind.set_menu(m.menu)

    m.ind.set_label("hello", "world... this si ")
    m.ind.set_label("foo bar baz hello", "world... this si ")        
    

    m.refresh()

  def refresh(m):
    GLib.timeout_add_seconds(1, m.refresh)
    print("refresh")

  def cb_test(m, w, data):
    icon = "./delme.svg"
    m.ind.set_icon(icon)
                
    notify.Notification.new("<b>Vagrant - %s</b>" % "foo", "bar", None).show()
    print("small test")
 
  def cb_exit(m, w, data):
     Gtk.main_quit()

class MyConfigWin(Gtk.Window):
  def __init__(m, root):
    super().__init__()
    m.app = root
    m.set_title(m.app.name + ' Config Window')

  def cb_show(m, w, data):
    m.show()

class MyMainWin(Gtk.Window):
  def __init__(m, root):
    super().__init__()
    m.app = root
    m.set_title(m.app.name)

  def cb_show(m, w, data):
    m.show()

class MyApp(Gtk.Application):
  def __init__(m, app_name):
    super().__init__()
    m.name = app_name
    m.main_win = MyMainWin(m)
    m.conf_win = MyConfigWin(m)
    m.indicator = MyIndicator(m)

  def run(m):
    Gtk.main()

if __name__ == '__main__':
  app = MyApp('Scaffold')
  app.run()
