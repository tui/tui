#!/usr/bin/env python3

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time
import watchdog

import rotatable

class TimeWindow(rotatable.SubWindow):
    def __init__(m):
        rotatable.SubWindow.__init__(m)
        m.wd = watchdog.LocationWatchdog()
        m.track_wd = watchdog.TrackWatchdog()

    def aux_interior(m):
        table = gtk.Table(5,4,True)

        m.time_label, w = m.font_button(m.big("(time)")+"\n"+m.small("date"))
        table.attach(w, 3,4,0,1)

        m.loc_label, w = m.font_button(m.big("(position)")+"\n"+m.small("date"))
        table.attach(w, 0,2,0,1)

        m.track_label, w = m.font_button(m.big("(track)")+"\n"+m.small("date"))
        table.attach(w, 0,2,2,4)

        _, w = m.font_button(m.big("Record"))
        w.connect("clicked", lambda _: os.system("../gpsim/trackd.py &"))
        table.attach(w, 2,3,2,3)
        
        _, w = m.font_button(m.big("stop"))
        w.connect("clicked", lambda _: os.system("killall trackd.py"))
        table.attach(w, 3,4,2,3)
        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())

        table.attach(w, 3,4, 4,5)
        return table

    def tick_time(m):
        if m.time_label:
            dt = time.localtime()
            t = "%d:%02d" % (dt.tm_hour, dt.tm_min)
            t = m.big(t)
            t += "\n"
            t += "%d.%d.%d" % (dt.tm_mday, dt.tm_mon, dt.tm_year)
            t += "\n(impl)"
            m.time_label.set_text(t)
            m.time_label.set_use_markup(True)
            
    def tick_loc(m):
        m.wd.read_loc()
        t = m.big(m.wd.pos.name)
        t += "\n"
        t += "%f %f" % (m.wd.pos.lat, m.wd.pos.lon)
        t += "\n(impl)"
        m.loc_label.set_text(t)
        m.loc_label.set_use_markup(True)

    def tick_track(m):
        t = m.track_wd.read()
        if t == None:
            m.track_label.set_text(m.big("not recording"))
        else:
            m.track_label.set_text("\n".join(t))
        m.track_label.set_use_markup(True)

    def tick(m):
        print("Tick tock")
        m.tick_loc()        
        m.tick_time()
        m.tick_track()
        gobject.timeout_add(30000, lambda: m.tick())

    def show(m):
        rotatable.SubWindow.show(m)
        m.tick()

if __name__ == "__main__":
    test = TimeWindow()
    test.basic_main_window()
    test.tick()
    gtk.main()
