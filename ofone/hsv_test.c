#include <stdio.h>

typedef unsigned int u32;
#define printk printf

#define DIV_ROUND_CLOSEST(x, divisor)(                  \
 {                                                       \
 typeof(x) __x = x;                              \
 typeof(divisor) __d = divisor;                  \
 (((typeof(x))-1) > 0 ||                         \
 ((typeof(divisor))-1) > 0 ||                   \
 (((__x) > 0) == ((__d) > 0))) ?                \
 (((__x) + ((__d) / 2)) / (__d)) :       \
 (((__x) - ((__d) / 2)) / (__d));        \
 }                                                       \
 )


struct led_hsv {
  u32 hue;
  u32 saturation;
  u32 value;
};

struct led_rgb {
  u32 red;
  u32 green;
  u32 blue;
};

struct led_rgb led_hsv_to_rgb(struct led_hsv hsv)
{
	unsigned int h = hsv.hue >> 24;
	unsigned int s = hsv.saturation >> 24;
	unsigned int v = hsv.value >> 24;
	unsigned int f, p, q, t, r, g, b;
	struct led_rgb res;

	if (!v) {
		res.red = 0;
		res.green = 0;
		res.blue = 0;
		return res;
	}
	if (!s) {
		res.red = v << 24;
		res.green = v << 24;
		res.blue = v << 24;
		return res;
	}

	f = DIV_ROUND_CLOSEST((h % 42) * 255, 42);
	p = v - DIV_ROUND_CLOSEST(s * v, 255);
	q = v - DIV_ROUND_CLOSEST(f * s * v, 255 * 255);
	t = v - DIV_ROUND_CLOSEST((255 - f) * s * v, 255 * 255);

	switch (h / 42) {
	case 0:
		r = v; g = t; b = p; break;
	case 1:
		r = q; g = v; b = p; break;
	case 2:
		r = p; g = v; b = t; break;
	case 3:
		r = p; g = q; b = v; break;
	case 4:
		r = t; g = p; b = v; break;
	case 5:
		r = v; g = p; b = q; break;
	}

#if 0
	printk("hsv_to: h %5d, s %5d, v %5d -> %5d, %5d, %5d\n",
	       h*390, s*390, v*390, r*390, g*390, b*390);
#endif
	
	res.red = r << 24;
	res.green = g << 24;
	res.blue = b << 24;
	return res;
}

#define ALL 0x1000000

static char avail[ALL];

void main(void)
{
  struct led_hsv hsv;
  struct led_rgb rgb;
  unsigned int i;
  unsigned int j;

  for (i = 0; i < ALL; i++) {
  
    hsv.hue = ((i & 0xff0000) >> 16) << 24;
    hsv.saturation = ((i & 0xff00) >> 8) << 24;
    hsv.value  = ((i & 0xff) >> 0) << 24;

    rgb = led_hsv_to_rgb(hsv);

    rgb.red >>= 24;
    rgb.green >>= 24;
    rgb.blue >>= 24;

    //    printf("rgb: %x %x %x\n", rgb.red, rgb.green, rgb.blue);
    j = (rgb.red << 16) + (rgb.green << 8) + rgb.blue;

    if (j > ALL) {
      printf("Overrun: %x\n", j);
      exit(1);
    }
    avail[j] = 1;
  }

  j = 0;
  for (i = 0; i < ALL; i++) {
    if (!avail[i]) {
      //printf("Unavailable: %x\n", i);
      j++;
    }
  }
  printf("%d / %d unavailable\n", j, ALL);
}
