#!/usr/bin/python2

from __future__ import print_function

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time
import watchdog

import rotatable

class AppsWindow(rotatable.SubWindow):
    def __init__(m):
        rotatable.SubWindow.__init__(m)

    def aux_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big("10f")+"\n"+m.small("alarm, media"))
        w.connect("clicked", lambda _: os.system("../10f/10fmenu.py"))
        table.attach(w, 0,1,0,1)
        
        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())

        table.attach(w, 3,4, 4,5)
        return table

    def show(m):
        rotatable.SubWindow.show(m)

if __name__ == "__main__":
    test = AppsWindow()
    test.basic_main_window()
    gtk.main()
