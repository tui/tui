#!/bin/bash
# Thanks to Tony Lindgren
# Date: Tue, 9 Feb 2016 09:24:04 -0800
# From: Tony Lindgren <tony@atomide.com>
# Subject: Re: N900 sleep mode (in 4.5-rc0, if that matters)

# Change for your UDC controller
phy_module=phy-twl4030-usb
udc_glue=omap2430
udc_module=musb_hdrc
udc_name=musb-hdrc.0.auto

start_usb_gadgets() {
	vendor=0x1d6b
	product=0x0106
	file=$1
  
	mount -t configfs none /sys/kernel/config
	mkdir /sys/kernel/config/usb_gadget/g1
	old_pwd=$(pwd)
	cd /sys/kernel/config/usb_gadget/g1
	
	echo $product > idProduct
	echo $vendor > idVendor
	mkdir strings/0x409
	echo 123456789 > strings/0x409/serialnumber
	echo foo > strings/0x409/manufacturer
	echo "Multi Gadget" > strings/0x409/product
	
	mkdir configs/c.1
	echo 100 > configs/c.1/MaxPower
	mkdir configs/c.1/strings/0x409
	echo "Config 100mA" > configs/c.1/strings/0x409/configuration
	
	mkdir configs/c.5
	echo 500 > configs/c.5/MaxPower
	mkdir configs/c.5/strings/0x409
	echo "Config 500mA" > configs/c.5/strings/0x409/configuration

	mkdir functions/mass_storage.0
	echo $file > functions/mass_storage.0/lun.0/file
	ln -s functions/mass_storage.0 configs/c.1
	ln -s functions/mass_storage.0 configs/c.5

	mkdir functions/acm.0
	ln -s functions/acm.0 configs/c.1
	ln -s functions/acm.0 configs/c.5

	mkdir functions/ecm.0
	ln -s functions/ecm.0 configs/c.1
	ln -s functions/ecm.0 configs/c.5

	# Adding rndis seems to cause alignment trap or some
	# random oops on reboot after rmmod $udc_glue

	echo $udc_name > /sys/kernel/config/usb_gadget/g1/UDC
	cd $old_pwd
}

stop_usb_gadgets() {
	old_pwd=$(pwd)
	cd /sys/kernel/config/usb_gadget/g1

	echo "" > /sys/kernel/config/usb_gadget/g1/UDC

	rm configs/c.1/ecm.0
	rm configs/c.5/ecm.0
	rmdir functions/ecm.0

	rm configs/c.1/acm.0
	rm configs/c.5/acm.0
	rmdir functions/acm.0

	echo "" > functions/mass_storage.0/lun.0/file
	rm configs/c.1/mass_storage.0
	rm configs/c.5/mass_storage.0
	rmdir functions/mass_storage.0

	rmmod $udc_glue
	rmmod $phy_module
	rmmod $udc_module

	cd $old_pwd
}

case $1 in
	start)
	insmod /my/modules/libcomposite
	insmod /my/modules/$udc_module.ko
	insmod /my/modules/$phy_module.ko
	insmod /my/modules/$udc_glue.ko
	start_usb_gadgets ""
        ;;
	stop)
	stop_usb_gadgets
	;;
        *)
        ;;
esac
