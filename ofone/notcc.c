#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

struct led_pattern {
  int delta_t;
  int brightness;
};

void write_string(char *name, char *val)
{
  int f = open(name, O_RDWR);
  write(f, val, strlen(val));
  close(f);
} 

void write_hex(char *name, char *pat, int len)
{
  char buf[1024];
  int f = open(name, O_RDWR);
  int i;
  for (i=0; i<len; i++) {
    sprintf(buf+2*i, "%02x", pat[i]);
  }
  buf[2*len] = 0;
  printf("Hex code: %s\n", buf);
  write(f, buf, 2*len);
  close(f);
}

int compile_pattern(struct led_pattern *steps, int len, int repeats, char *res)
{
  int i;
  int br = 0;
  char *res_ = res;

  *res++ = 0x9d; // start
  *res++ = 0x80;

  *res++ = 0x40; // reset brightness to zero
  *res++ = 0x00;

#if 1
  for (i = 0; i < len; i++) {
    int n;
    int b = steps[i].brightness;
    int t = steps[i].delta_t;

    n = 0;  
    if (b < br) {
      n |= 0x01;
      b = br - b;
    } else b = b - br;

    t *= 1000; /* usec */
    if (b)	/* for b=0, we use it as a delay */
      t /= b;    /* per step */
    if (t > 15200) {
      n |= 0x40;  /* Selecting 15.625 msec steps (other variant is 0.488 msec) */
      t /= 15625;
      if (!t) t = 1;
      printf("Long ramp, ");
    } else {
      t /= 488;
      printf("Short ramp, ");
    }
    printf("%d\n", t);
    if (t > 31) {
      printf("Ramp way too long\n");
      if (b == 0) {
	while (t > 31) {
	  printf("Splitting delay\n");
	  t -= 31;
	  *res++ = n | 0x1f;
	  *res++ = b;
	}
      }
      t = 31;
    }

    n |= (0x1f & t) << 1;
    
    *res++ = n;
    *res++ = b;
    
    br = b;
  }
#else
  *res++ = 0x41;
  *res++ = 0x00;
#endif
#if 0
  *res++ = 0x0a;
  *res++ = 0x01;
#endif
  return res - res_;
}

static struct led_pattern quickstep[] = {
  { .delta_t = 1000, .brightness = 0, },
  { .delta_t = 1000, .brightness = 255, },
  { .delta_t = 1000, .brightness = 0, },
  { .delta_t = 0, .brightness = 255, },
  { .delta_t = 100, .brightness = 255, },
  { .delta_t = 0, .brightness = 0, },
  { .delta_t = 1000, .brightness = 0, },  
  { .delta_t = 0, .brightness = 255, },
  { .delta_t = 100, .brightness = 255, },
  { .delta_t = 0, .brightness = 0, },
  { .delta_t = 1000, .brightness = 0, },  
 };

static struct led_pattern quiet[] = {
  { .delta_t = 1000, .brightness = 0, },
  { .delta_t = 1000, .brightness = 1, },
  { .delta_t = 1000, .brightness = 0, },
 };


#define arraysize(x) (sizeof(x) / sizeof(x[0]))

void run_pattern(char *pat, int len)
{
  chdir("/sys/class/i2c-adapter/i2c-2/2-0032");  
  write_string("engine1_mode", "load");
  write_string("engine1_leds", "111111111");    
  write_string("engine1_mode", "load");
  //write_string("engine1_load", "9d804000427f0d7f7f007f0042000000");
  //write_string("engine1_load", "9d804000900290039004900590069007900890097c00400a7c00900d900e900f9010901140807c00a00b");
  write_hex("engine1_load", pat, len); 
  write_string("engine1_mode", "run");
}


void main(void)
{
  static char res[1024];
  int len;

  //len = compile_pattern(quickstep, arraysize(quickstep), 999, res);
  len = compile_pattern(quiet, arraysize(quiet), 999, res);
  if (1)
    run_pattern(res, len);
  else
    run_pattern("\x9d\x80\x40\x00\x42\x7f\x0d\x7f\x7f\x00\x7f\x00\x42\x00\x00\x00", 16);
}
