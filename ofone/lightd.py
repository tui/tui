#!/usr/bin/python3
# -*- python -*-
import hardware
import time

if __name__ == "__main__":
    la = hardware.LightAdjustment()
    while True:
        la.run(False)
        time.sleep(1)
