#!/usr/bin/env python

import sys
#sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
sys.path += [ "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time
import watchdog
import hardware

import rotatable

class AppsWindow(rotatable.SubWindow):
    def __init__(m):
        rotatable.SubWindow.__init__(m)
        m.hw = hardware.hw

    def aux_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big("loud")+"\n"+m.small(""))
        w.connect("clicked", lambda _: m.hw.audio.mixer_ringing())
        table.attach(w, 0,1,0,1)
        
        _, w = m.font_button(m.big("call")+"\n"+m.small(""))
        w.connect("clicked", lambda _: m.hw.audio.mixer_call())
        table.attach(w, 1,2,0,1)

        _, w = m.font_button(m.big("wired")+"\n"+m.small(""))
        w.connect("clicked", lambda _: m.hw.audio.mixer_headphones())
        table.attach(w, 2,3,0,1)        
        
        _, w = m.font_button(m.big("play")+"\n"+m.small(""))
        w.connect("clicked", lambda _: os.system("mpg123 -Z /data/mp3.local/*/* /data/mp3.local/*/*/* &"))
        table.attach(w, 3,4,1,2)

        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())

        table.attach(w, 3,4, 4,5)
        return table

if __name__ == "__main__":
    test = AppsWindow()
    test.basic_main_window()
    gtk.main()
