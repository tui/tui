start()
start: a = 0
b = 0
br = a
delay_long(30)
a = 1
br = a
delay_long(30)
a += 4
br = a
delay_long(30)
goto start
a += 16
br = a
delay_long(30)
a += 64
br = a
delay_long(30)
if (a == b) goto ok1
bad1: goto bad1
goto start
bad2: goto bad2
ok1: a = 5
br = a
label: a = a + b
a = a - b
br = a
ramp_up_long(a,b)
if (a<b) goto skip
goto label
skip: a = 0
