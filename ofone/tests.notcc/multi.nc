repeat: led_set(red)
br = 255
delay_long(30) # red+blue
led_set(blue)
br = 0
delay_long(30) # red
led_set(green)
br = 32
delay_long(30) # yellow
led_set(red)
br = 0
delay_long(30) # green
led_set(blue)
br = 64
delay_long(30) # blue+green
led_set(green)
br = 0
delay_long(30) # blue
goto repeat
