@engine 1, 000000100
start()
c = 1
next_number: c += 1
b = 1
next_divisor: b += 1
if (b==c) goto is_prime
a = 0
test_prime: a = a+b
if (c==a) goto not_prime
if (c<a) goto not_divisor
goto test_prime
not_divisor: goto next_divisor
not_prime: goto next_number
is_prime: trigger(2)
wait_for(2)
goto next_number
@engine 2, 000001000
start()
next_number: wait_for(1)
b = 0
show_prime: ramp_up_short(1,255)
ramp_down_short(1,255)
delay_long(5)
b += 1
if (b == c) goto next_number2
goto show_prime
next_number2: delay_long(30)
delay_long(30)
trigger(1)
goto next_number
