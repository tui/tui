#!/usr/bin/python
# sudo aptitude install python-audioread python-alsaaudio

import audioread
import alsaaudio
import random
import struct

class Player:
    def __init__(m):
        m.filename = '/data/mp3/czech/terezka/Sabat.mp3'
        m.buffer_size = 4608
        m.avg = 0
        m.max = 0
        m.silent = 0

    def read_file(m):
        with audioread.audio_open(m.filename) as f:
            m.device.setchannels(f.channels)
            m.device.setrate(f.samplerate)
            print(f.channels, f.samplerate, f.duration)
            for buf in f:
                print(type(buf), len(buf))
                d = struct.unpack("h" * (len(buf)/2), buf)
                #print(buf)
                #d.write(buf)
                #do_something(buf)
                #print(d)
                d = m.process(d)
                buf = struct.pack("h" * (len(buf)/2), *d)
                m.device.write(buf)

    def process(m, d):
        ret = []
        for i in d:
            ratio = 0.995
            m.avg = m.avg * ratio + i * (1-ratio)
            m.max = m.max * ratio
            new = 0
            m.silent += 1
            if i > m.max * 1.1 and m.silent > 100:
                new = 30000
                m.silent = 0
            if i > m.max:
                m.max = i
            
            SHRT_MAX = 32700
            SHRT_MIN = -32700
            if new > SHRT_MAX:
                new = SHRT_MAX
            if new < SHRT_MIN:
                new = SHRT_MAX
                
            ret += [new]
            
        #d = [random.random()*10000 + d[i] for i in range(len(d))]
        return ret


    def noise_callback(m):
        return [random.randint(-20000, 20000) for i in range(m.buffer_size)]

    def openaudio(m, card_name, freq, callback):
        buffer_size = p.buffer_size
        device = alsaaudio.PCM(card=card_name)

        device.setchannels(1)
        device.setrate(freq)
        device.setformat(alsaaudio.PCM_FORMAT_S16_LE)

        device.setperiodsize(buffer_size)
        m.device = device

    def run(m):
        while True:
            samples = m.noise_callback()
            data = struct.pack("h" * len(samples), *samples)
            m.device.write(data)

p = Player()
p.openaudio(card_name="default", freq=44100, callback=None)
#p.run()
p.read_file()
                                                        
