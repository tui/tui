#!/usr/bin/python

from midiutil.MidiFile import MIDIFile
import os

class ToneGen:
    letters = [('A',".-"),   ('B',"-..."), ('C',"-.-."), ('D',"-.."), ('E',"."),
             ('F',"..-."), ('G',"--."),  ('H',"...."), ('I',".."),  ('J',".---"),
             ('K',"-.-"),  ('L',".-.."), ('M',"--"),   ('N',"-."),  ('O',"---"),
             ('P',".--."), ('Q',"--.-"), ('R',".-."),  ('S',"..."), ('T',"-"),
             ('U',"..-"),  ('V',"...-"), ('W',".--"),  ('X',"-..-"),('Y',"-.--"),
             ('Z',"--.."), ('CH',"----")] 

    def __init__(m):
        m.mf = MIDIFile(1)
        m.time = 0
        m.track = 0
        m.channel = 0
        m.pitch = 70
        m.volume = 3
        m.init_morse()

    def init_morse(s):
          s.morse = {}
          s.morse_back = {}
          for letter, code in s.letters:
              s.morse[code] = letter
              s.morse_back[letter] = code

    def add_note(m, pitch, duration):
        m.mf.addNote(m.track, m.channel, m.pitch+pitch, m.time, duration, m.volume)
        m.time += duration

    def gen_sms(m):
        m.mf.addTrackName(m.track, 0, "SMS in morse")
        m.mf.addTempo(m.track, 0, 660)

        pitch = 0

        m.gen_morse(0, ".../--/...")
        m.time += 3
        m.gen_morse(1, ".../--/...")

    def gen_morse(m, pitch, s):
        for c in s:
            if c == ".": m.add_note(pitch, 1)
            elif c == "-": m.add_note(pitch, 2)
            elif c == "/": m.time += 1
            else: print("Unknown character", c)

    def gen_word(m, s):
        for c in s:
            pitch = (ord(c) % 10) - 3
            m.gen_morse(pitch, m.morse_back[c] + "/")
            

    def gen_ovecka(m):
        m.mf.addTrackName(m.track, 0, "Ovecka")
        m.mf.addTempo(m.track, 0, 180)

        # Buggy
        m.add_note(0, 1)
        m.add_note(1, 1)
        m.add_note(2, 2)                
        m.add_note(3, 2)
        m.add_note(3, 1)
        m.add_note(3, 1)
        m.add_note(3, 1)
        m.add_note(2, 1)
        m.add_note(1, 1)        
        m.add_note(0, 2)
        m.add_note(0, 1)
        m.add_note(0, 1)
        
    def play(m):
        binfile = open("midiw.mid", 'wb')
        m.mf.writeFile(binfile)
        binfile.close()

        os.system("musescore midiw.mid -o /tmp/delme.wav && aplay /tmp/delme.wav")

    def run(m):
        m.gen_word("ROLF")
        m.play()

t = ToneGen()
t.run()
