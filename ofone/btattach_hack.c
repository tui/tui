/* hacked tty attach */
#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#define HCIUARTSETPROTO _IOW('U', 200, int)
#define N_HCI 15
#define HCI_UART_NOKIA  10

int main(int argc, char **argv) {
        int fd;
        int err;
        int ldisc = N_HCI;
        int proto = HCI_UART_NOKIA;

     /* ttyO1 may be ttyS1 depending on serial driver */
        fd = open("/dev/ttyO1", O_RDWR | O_NOCTTY);
        if(fd < 0) {
                fprintf(stderr, "open failed!\n");
                return fd;
        }

        err = ioctl(fd, TIOCSETD, &ldisc);
        if(err < 0) {
                fprintf(stderr, "ldisc failed!\n");
                return err;
        }

        err = ioctl(fd, HCIUARTSETPROTO, proto);
        if(err < 0) {
                fprintf(stderr, "proto failed!\n");
                return err;
        }

        pause();

        while(1) {
                sleep(60);
        }

        return 0;
}
