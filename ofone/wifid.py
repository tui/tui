#!/usr/bin/env python3
# -*- python -*-

import sys              
sys.path += [ "/usr/share/unicsy/lib" ]

import os
import hardware
import watchdog
import re
import time
import subprocess

class Periodic:
    def __init__(m):
        m.iface_base = "wlan0"
        m.iface = m.iface_base

    def cmd_iw(m, d=""):
        if d== "":
            d=m.iface
        return "sudo iw dev "+d+ " "
    
    def iw(m, s, d=""):
        s =m.cmd_iw(d) + s
        print("cmd: ", s)
        os.system(s)
        print("ok")

    def ifconfig(m, s):
        os.system("sudo ifconfig "+m.iface+" "+s)
        
    def scan(m):
        scan = os.popen(m.cmd_iw() + "scan | grep SSID: | sed 's/.*SSID: //'").readlines()
        scan = map(lambda x: x[:-1], scan)
        scan = list(scan)
        return scan
    
    def connect_wlan(m, ssid = "openwireless.org", ipa = "10.0.0.8", gw = "10.0.0.251"):
        m.iw("connect -w '"+ssid+"'")
        m.ifconfig(ipa+" up")
        m.iw("set power_save off")
        os.system("sudo route add default gw "+gw)
        
    def connect_ibss(m, ssid = "tether.pavel", ipa = "10.42.0.2", freq = "2417"):
        print("Connecting to ibss")
        m.ifconfig("down")
        m.iw("interface add wlan0_m type ibss")
        time.sleep(.5)
        m.iface = "wlan0_m"
        m.ifconfig("up")
        m.iw("ibss join "+ssid+" "+freq)
        m.ifconfig(ipa+" up")
        #m.iw("set power_save off")
        print("Connected?")
        
    def disconnect(m):
        m.iw("disconnect")
        m.iw("set power_save on")
        m.ifconfig("down")
        m.iw("del", "wlan0_m")

    def disconnect_all(m):
        m.iface = "wlan0_m"
        m.disconnect()
        m.iface = m.iface_base
        m.disconnect()

    def is_alive(m, ipa):
        (cS,cO) = subprocess.getstatusoutput('ping -c1 -t1 %s' % ipa)
        if not cS:
            return 1
        return 0

    def test_alive(m, ipa):
        print("Alive? ", ipa, m.is_alive(ipa))

    def run_home_wifi(m):
        for i in range(10):
            if not m.is_alive("10.0.0.251"):
                return
            time.sleep(1)
        print("*** Network alive, sync mails?")
        os.system("~/bin/ff -a")
        while True:
            if not m.is_alive("10.0.0.251"):
                return
            time.sleep(1)

    def run(m):
        os.system("sudo killall NetworkManager")
        m.disconnect_all()
        while True:
            m.ifconfig("up")
            scan = m.scan()
            #m.test_alive("127.0.0.1")
            #m.test_alive("10.0.0.9")
            #m.test_alive("10.42.0.1")
            if "pavel lg" in scan:
                m.connect_wlan("pavel lg")
            if "openwireless.org" in scan:
                print("Connecting...")
                m.connect_wlan()
                print("...run...")
                m.run_home_wifi()
                print("...disconnect")
                m.disconnect()
            if "tether.pavel" in scan:
                m.connect_ibss()
                return
            if "tether.olpc" in scan:
                m.connect_ibss("tether.olpc", "10.42.43.2", "2412")
                return
            print(scan)
            time.sleep(3)

m = Periodic()
if len(sys.argv) > 1:
    m.connect_wlan(sys.argv[1])
    print("should connect")
else:
    m.run()
