#!/usr/bin/env python3

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time

import rotatable

class AppsWindow(rotatable.Rotatable):
    def __init__(m):
        rotatable.Rotatable.__init__(m)

    def aux_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big("10f")+"\n"+m.small("alarm, media"))
        w.connect("clicked", lambda _: os.system("../10f/10fmenu.py &"))
        table.attach(w, 0,1,0,1)
        
        _, w = m.font_button(m.big("monav")+"\n"+m.small("navigation"))
        w.connect("clicked", lambda _: os.system("monav &"))
        table.attach(w, 1,2,0,1)
        
        _, w = m.font_button(m.big("wnow")+"\n"+m.small("weather"))
        w.connect("clicked", lambda _: os.system("../nowcast/nowcast -x &"))
        table.attach(w, 2,3,0,1)

        _, w = m.font_button(m.big("audio"))
        w.connect("clicked", lambda _: os.system("./win_audio.py &"))
        table.attach(w, 3,4,0,1)

        _, w = m.font_button(m.big("gps &amp;")+"\n"+m.small("position"))
        w.connect("clicked", lambda _: os.system("./win_pos.py &"))
        table.attach(w, 1,2,1,2)
        
        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())

        table.attach(w, 3,4, 4,5)
        return table

if __name__ == "__main__":
    test = AppsWindow()
    test.basic_main_window()
    gtk.main()
