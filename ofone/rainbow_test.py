#!/usr/bin/env python3
# See mygtk.py for cairo package requirements.

# http://www.pygtk.org/articles/writing-a-custom-widget-using-pygtk/writing-a-custom-widget-using-pygtk.htm


import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
from gtk import gdk
import rainbow
import rotatable
import cairo
import progress
import pango

class StarPainter(progress.Rainbow):
    def __init__(m):
        progress.Rainbow.__init__(m)
        m.x = 5
        m.y = 5
    
    def draw_star(m, context, x, y):
        m.star_pixmap()
        gtk.gdk.cairo_set_source_pixbuf(context, m.pixbuf, m.x, m.y)
        context.paint()

    def star_pixmap(m):
        # load the star xpm
        #help(gtk.gdk)
        #help(gtk.gdk.pixbuf_new_from_xpm_data)

        STAR_PIXMAP = ["22 22 77 1",
                "         c None",
                ".        c #626260",
                "+        c #5E5F5C",
                "@        c #636461",
                "#        c #949492",
                "$        c #62625F",
                "%        c #6E6E6B",
                "&        c #AEAEAC",
                "*        c #757673",
                "=        c #61625F",
                "-        c #9C9C9B",
                ";        c #ACACAB",
                ">        c #9F9F9E",
                ",        c #61635F",
                "'        c #656663",
                ")        c #A5A5A4",
                "!        c #ADADAB",
                "~        c #646562",
                "{        c #61615F",
                "]        c #6C6D6A",
                "^        c #797977",
                "/        c #868684",
                "(        c #A0A19E",
                "_        c #AAAAA8",
                ":        c #A3A3A2",
                "<        c #AAAAA7",
                "[        c #9F9F9F",
                "}        c #888887",
                "|        c #7E7E7C",
                "1        c #6C6C69",
                "2        c #626360",
                "3        c #A5A5A3",
                "4        c #ABABAA",
                "5        c #A9A9A7",
                "6        c #A2A2A1",
                "7        c #A3A3A1",
                "8        c #A7A7A6",
                "9        c #A8A8A6",
                "0        c #686866",
                "a        c #A4A4A2",
                "b        c #A4A4A3",
                "c        c #A1A19F",
                "d        c #9D9D9C",
                "e        c #9D9D9B",
                "f        c #A7A7A5",
                "g        c #666664",
                "h        c #A1A1A0",
                "i        c #9E9E9D",
                "j        c #646461",
                "k        c #A6A6A4",
                "l        c #A0A09F",
                "m        c #9F9F9D",
                "n        c #A9A9A8",
                "o        c #A0A09E",
                "p        c #9B9B9A",
                "q        c #ACACAA",
                "r        c #60615E",
                "s        c #ADADAC",
                "t        c #A2A2A0",
                "u        c #A8A8A7",
                "v        c #6E6F6C",
                "w        c #787976",
                "x        c #969695",
                "y        c #8B8B8A",
                "z        c #91918F",
                "A        c #71716E",
                "B        c #636360",
                "C        c #686966",
                "D        c #999997",
                "E        c #71716F",
                "F        c #61615E",
                "G        c #6C6C6A",
                "H        c #616260",
                "I        c #5F605E",
                "J        c #5D5E5B",
                "K        c #565654",
                "L        c #5F5F5D",
                "                      ",
                "                      ",
                "          .           ",
                "          +           ",
                "         @#$          ",
                "         %&*          ",
                "        =-;>,         ",
                "        ';)!'         ",
                "  ~{{]^/(_:<[}|*1@,   ",
                "   23&4_5367895&80    ",
                "    2a4b:7c>def)g     ",
                "     2c4:h>id56j      ",
                "      {k8lmeln2       ",
                "      j8bmoppqr       ",
                "      {stusnd4v       ",
                "      ws;x@yq;/       ",
                "      zfAB {CmD{      ",
                "     rE{     FGH      ",
                "     IJ       KL      ",
                "                      ",
                "                      ",
                "                      "]

        m.pixbuf = gtk.gdk.pixbuf_new_from_xpm_data(STAR_PIXMAP)

# Font example:
# https://sites.google.com/site/randomcodecollections/home/python-gtk-3-pango-cairo-example
# Pycairo docs -- great!
# http://zetcode.com/gfx/pycairo/basicdrawing/
# 

class TestWindow(rotatable.SubWindow):
    def aux_interior(m):
        table = gtk.Table(5,4,True)

        m.time_label, button = m.font_button(m.big("(time)")+"\n"+m.small("date"))
        table.attach(button, 3,4,0,1)

        m.rainbow = progress.RainbowProgress()
        table.attach(m.rainbow, 0,4, 2,3)
        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())
        table.attach(w, 3,4, 4,5)

        w = gtk.Entry()
        #w.set_font_options(None, None)
        w.modify_font(pango.FontDescription("sans 48"))
        table.attach(w, 0,4, 3,4)

        scrolledwindow = gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)

        textview = gtk.TextView()
        textview.modify_font(pango.FontDescription("sans 48"))
        m.textbuffer = textview.get_buffer()
        m.textbuffer.set_text("This is some text inside of a Gtk.TextView. "
                                 + "Select text and click one of the buttons 'bold', 'italic', "
                                 + "or 'underline' to modify the text accordingly.")
        textview.set_wrap_mode(gtk.WrapMode.WORD)
        scrolledwindow.add(textview)


        table.attach(scrolledwindow, 0,4, 0,3)
                                                                        
        
        return table

    def tick(m):
        print("Tick tock")
        #m.rainbow.x += 1
        m.rainbow.progress += .005
        m.rainbow.hide()
        m.rainbow.show()        
        gobject.timeout_add(300, lambda: m.tick())

    def show(m):
        super().show()
        m.tick()

if __name__ == "__main__":
    test = TestWindow()
    test.basic_main_window()
    test.tick()
    gtk.main()
