* Usable applications

** With 1 hand

galculator --
 in preferences: Buttons
    button font: sans | 20
    minimal width: 100
    minimal height: 45

** With stylus

*** Midori

good enough, sometimes slow. Usable/useful web pages:

m.mobilecity.cz
m.facebook.com

These switch to full version, but with right user agent, can be used:
allows use of m.idos.cz. In midori: "Tools" | Preferences | Network | Identify as

Mozilla/5.0 (MeeGo; N9) AppleWebKit/533.3 (KHTML, like Gecko) NokiaBrowser/8.5 Mobile Safari/533.3

To get reasonable size of fonts:    "Tools" | Preferences | Fonts: Sans 18, Monospace 15

m.idos.cz
t.idnes.cz
t.jizdnirady.idnes.cz


*** accessories | alarm clock, probably "alarm-clock-applet" in debian.
- everything is way too small
- eats 3% cpu.

*** monav

*** foxtrotgps
- czech hiking maps work, enter http://tile.poloha.net/%d/%d/%d.png 

** backend support

*** gpsfeed+

http://sourceforge.net/p/gpsfeed/code/ci/master/tree/

aptitude install tcl-udp

/usr/sbin/gpsd tcp://localhost:2222

aptitude install gpsd-clients

sudo systemctl stop gpsd.socket

* Android 2

Main menu
- application list
- web
- all applications
- dialer

Dialer
- dial numbers
- recent calls list
- contacts
- starred contacts

Messages
- list of messages
- "new message"

Power button
- power off/reboot/quiet/airplane

* Nokia series 40

Standby screen
- date, time, status, battery
- can type number right away

Power button
- change profiles
- power off

Main menu
- organizer
-- alarm clock, calendar, notes, calculator
- messages
-- inbox
- phonebook
- call logs
- application
- web browser

* Neo1793

Standby screen
- dialer
- contacts
- messages
- calendar
- very big time
- application list
- settings

* Maemo ?

	
