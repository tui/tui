#!/usr/bin/env python3

# http://www.pygtk.org/articles/writing-a-custom-widget-using-pygtk/writing-a-custom-widget-using-pygtk.htm

import sys
sys.path += [ "../maemo", "../lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
from gtk import gdk
import rainbow
import rotatable
import cairo
import progress
import os

class WatchFile(rotatable.SubWindow):
    def __init__(m, name, max_size):
        super().__init__()
        m.name = name
        m.max_size = max_size

    def aux_interior(m):
        table = gtk.Table(5,4,True)

        m.name_label = gtk.Label(m.name)
        table.attach(m.name_label, 0,4,0,1)

        m.progress_label = gtk.Label("progress 10%")
        table.attach(m.progress_label, 0,4,1,2)
        
        m.rainbow = progress.HistoryProgress()
        table.attach(m.rainbow, 0,4, 2,3)
        return table

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())

        table.attach(w, 3,4, 4,5)
        return table

    def tick(m):
        stat = os.stat(m.name)
        p = stat.st_size / m.max_size
        # stat.st_ctime
        m.rainbow.set_progress(p)
        m.progress_label.set_text("progress " + str(int(p*100)) + "%")
        print("Tick tock", p)
        gobject.timeout_add(300, lambda: m.tick())

    def show(m):
        super().show()
        m.tick()

if __name__ == "__main__":
    test = WatchFile(sys.argv[1], int(sys.argv[2]))
    test.basic_main_window()
    test.tick()
    gtk.main()
