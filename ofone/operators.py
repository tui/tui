#!/usr/bin/python

class Action:
    pass

class USSD(Action):
    def __init__(m, code):
        m.ussd = code

class SMS(Action):
    pass

class Operator:
    def __init__(m, name):
        m.name = name
        m.web_main = None
        m.web_ss = None
        m.apn_inet = None

        m.get_credit = None    # Get current credit
        m.get_data = None      # Get data limit left

        m.activate_slow_data = None

        m.init()

    def init(m):
        pass

class Operators:
    def __init__(m):
        l = []
        o = Operator("Blesk?")
        o.web_main = "https://bleskmobil.blesk.cz"
        o.web_ss = "https://samoobsluha.bleskmobil.cz/selfcare/"
        l += [ o ]
        o = Operator("Mobil CZ") # T-Mobile
        o.get_credit = USSD("*145#")
        o.get_data = USSD("*146#")
        l += [ o ]
        o = Operator("TESCO Mobile") # O2
        o.web_main = "https://www.tescomobile.cz/"
        o.web_ss = "https://samoobsluha.tescomobile.cz/selfcare/"
        o.get_credit = USSD("*125*#")
        o.activate_slow_data = SMS("999346", "NA DOSAH A") # Cca 16kbit, 49Kc, 1 month
        o.activate_slow_data = SMS("999346", "MINI A") # 100MB, 99Kc, 1 month
        l += [ o ]


