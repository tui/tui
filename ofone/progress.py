#!/usr/bin/env python3

# http://www.pygtk.org/articles/writing-a-custom-widget-using-pygtk/writing-a-custom-widget-using-pygtk.htm

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
from gtk import gdk
import rainbow
import rotatable
import cairo
import time

class CairoTest(gtk.DrawingArea):
    def __init__(m):
        gtk.DrawingArea.__init__(m)
        m.connect("draw", m.on_expose_event)

    def get_background(m):
        """Serves as a caching solution."""
        return m.__bg

    def set_background(m, pattern):
        """Serves as a caching solution."""
        m.__bg = pattern

    def get_brush(m):
        """Serves as a caching solution."""
        return m.__brush

    def set_brush(m, pattern):
        """Serves as a caching solution."""
        m.__brush = pattern

    def on_expose_event(m, widget, event):
        context = m.window.cairo_create()

        # Restrict cairo to the exposed area
        #context.rectangle(*event.area)
        #context.clip()

#        m.width, m.height = m.window.get_size()
        m.width, m.height = 400, 300

        m.draw(context)

    def on_configure_event(m, widget, event):
        """Override this in case you want to do something when 
           the widget's size changes."""

        return super(CairoTest, m).on_configure_event(widget, event)

    def invalidate(m):
        """Force a re-rendering of the window."""

        rect = m.get_allocation()

        # Compensate for parent offset, if any.
        parent = m.get_parent()
        if parent:
            offset = parent.get_allocation()
            rect.x -= offset.x
            rect.y -= offset.y

        m.window.invalidate_rect(rect, False)

    def draw(m, context):
        """Override this."""

        # do custom drawing here

        raise NotImplementedError()


class Rainbow(CairoTest):
    def percent_to_color(m, p):
        wave_min = 380
        wave_max = 780
        wave = wave_max - (wave_max - wave_min) * p
        color = rainbow.wl_to_rgb_gamma(wave)
        return color

    def make_rainbow(m, context, fgcolor, bgcolor, width, height):
        context.set_source_rgba(*bgcolor)
        context.rectangle(0, 0, width, height)
        context.fill()

        context.set_source_rgba(*fgcolor)
        context.set_line_width(1)

        width = 400
        for x in range(0, width):
            p = float(x) / width
            color = m.get_color(p)
            context.set_source_rgba(*color)             
            context.move_to(x, 0)
            context.line_to(x, 30)
            context.stroke()

        #m.draw_star(context, m.x, m.y)

    def draw(m, context):
        context.push_group()
        m.make_rainbow(context, fgcolor=(0, 0, 0, 1), bgcolor=(1, 1, 1, 1),
                    width=m.width,
                    height=m.height)
        m.set_background(context.pop_group())
        #help(context.paint)

        context.set_source(m.get_background())
        context.paint()

class RainbowProgress(Rainbow):
    def __init__(m):
        Rainbow.__init__(m)
        m.progress = 0

    def get_color(m, p):
        if p <= m.progress:
            color = m.percent_to_color(p)
        else:
            color = m.percent_to_color(m.progress)            
            color = map(lambda i: i*0.3, color)
        return color

    def set_progress(m, p):
        m.progress = p
        m.hide()
        m.show()

class HistoryProgress(RainbowProgress):
    def __init__(m):
        RainbowProgress.__init__(m)
        m.start_time = time.time()
        m.ages = {}
        m.steps = 300
        m.set_range(0, m.start_time)

    def set_range(m, p, t):
        for i in range(int(p*m.steps), m.steps):
            m.ages[i] = t

    def age_to_color(m, age):
        now = time.time()
        p = (age-m.start_time) / (now-m.start_time)
        return m.percent_to_color(p)

    def get_color(m, p):
        if p <= m.progress:
            color = m.age_to_color(m.ages[int(p*m.steps)])
        else:
            color = m.percent_to_color(m.progress)            
            color = map(lambda i: i*0.3, color)
        return color

    def set_progress(m, p):
        now = time.time()
        m.set_range(p, now)
        #for i in range(m.steps):
        #    print(m.ages[i] - now, ' ', end='')
        print()

        RainbowProgress.set_progress(m, p)
