#!/usr/bin/python2
import time
import sys
sys.path += [ "../maemo", "../lib" ]
import notint
import colorsys

class HSVLed(notint.ColorDisplay):
    path = "/sys/class/leds/lp5523:r/"
    def write(m, s, v):
        f = open(m.path + s, "w")
        f.write(v)
        f.close()

    def write_float(m, s, v):
        scale = (1 << 32)-1
        m.write(s, str(int((v+0.0)*scale)))
        
    def change_color(m, val):
        (r, g, b) = val
        h, s, v = colorsys.rgb_to_hsv(r, g, b)
        m.write_float("hue", h)
        m.write_float("saturation", s)
        m.write("brightness", str(int((v+0.0)*256)))

class PatternSync(notint.Test):
    def change_color(m, c):
        m.d.change_color(c)
        m.d1.change_color(c)

    def run_basic(m):
        l = m.d
        while True:
            m.change_color((1, 0, 0))
            l.sleep(.5)
            m.change_color((0, 0, 0))
            l.sleep(.5)
            m.change_color((0, 1, 0))
            l.sleep(.5)
            m.change_color((0, 0, 0))
            l.sleep(.5)
            m.change_color((0, 0, 1))
            l.sleep(.5)
            m.change_color((0, 0, 0))
            l.sleep(.5)


            m.change_color((1, 1, 1))
            l.sleep(.5)
            m.change_color((.8, .8, .8))
            l.sleep(.5)
            m.change_color((.6, .6, .6))
            l.sleep(.5)
            m.change_color((.4, .4, .4))
            l.sleep(.5)
            m.change_color((.2, .2, .2))
            l.sleep(.5)
            m.change_color((.0, .0, .0))
            l.sleep(.5)

    def run_hsv(m):
        l = m.d
        while True:
            for i in range(360):
                m.change_color(colorsys.hsv_to_rgb(i/360., 1, 1))
                l.sleep(.01)

            for i in range(360):
                m.change_color(colorsys.hsv_to_rgb(i/360., .5, 1))
                l.sleep(.01)

    def run(m):
        m.run_basic()

if __name__ == "__main__":
    t = PatternSync(notint.LedN900())
    #t = PatternSync(HSVLed())
    t.d1 = notint.GtkColorDisplay()
    t.run()
