#!/usr/bin/python
# rsync -zavP --delete "pavel@maemo:~/MyDocs/Exported\ contacts" .

from __future__ import print_function
import vobject
import json
import glob

out = []

for f in glob.glob("Exported contacts/*.vcf"):
    s = open(f).readlines()
    s = "".join(s)
    try:
        v = vobject.readOne(s)
    except:
        print("Could not parse ", f)
        continue

    try:
        name = v.fn.value
    except:
        print("No full name for ", f)
        continue

    for a in v.getChildren():
        if a.name != 'TEL':
            continue

        try:
            out += [ [str(name), str(a.value)] ]
        except:
            print("Could not write contact details ", name, a.value)

f = open("contacts.json", "w")
f.write(json.dumps(out)) # skipkeys=True
