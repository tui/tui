#!/usr/bin/python3
#
# NOTification Compiler
#
# for LP5523 chip.
#
# Copyright 2014 Pavel Machek <pavel@ucw.cz>, GPLv3
#
# http://wiki.maemo.org/LED_patterns#Lysti_Format_Engine_Patterns_and_Commands
# http://www.ti.com/general/docs/lit/getliterature.tsp?genericPartNumber=lp5523&fileType=pdf
# http://www.ti.com/lit/ds/symlink/lp5523.pdf
#
# Seems to have A,B local variables, C global variable,
#
# 2.6.28-omap1 seems to cut execution after 16 steps.

import sys
import re
import hardware

class Compiler:
    def __init__(m):
        m.script = []
        m.script_tail = []

    def error(m, text):
        print("stdin:", m.cur_line_num, ":", text, " -- ", m.program[m.cur_line_num][:-1])

    def load(m, program):
        m.labels = {}
        m.statements = []
        for i in range(len(program)):
            m.cur_line_num = i
            l = program[i]
            match = re.split(":", l)
            if len(match) == 2:
                rest = match[1]
                label = match[0]
                m.labels[label] = i
            elif len(match) == 1:
                rest = match[0]
            else:
                m.error("Too many ':'s")
                raise

            l = rest
            if l[-1] == '\n':
                l = l[:-1]
            l = re.sub("^ ", "", l)
            l = re.sub(" *= *", "=", l)
            l = re.sub(" *\+ *", "+", l)
            l = re.sub(" *\+= *", "+=", l)
            l = re.sub(" *- *", "-", l)
            l = re.sub("#.*$", "", l)
            m.statements += [ l ]

        print(m.labels)
        print(m.statements)

    def output(m, res):
        m.out += "%04x" % res
        print("%d: %04x %s" % (m.cur_line_num, res, m.program[m.cur_line_num][:-1]))

    def add(m, left, r1, r2):
        return m.output(0x9300 + (m.var(left)<<10) + (m.var(r1)<<2) + (m.var(r2)))

    def sub(m, left, r1, r2):
        return m.output(0x9310 + (m.var(left)<<10) + (m.var(r1)<<2) + (m.var(r2)))

    def var(m, s):
        if s == "a":
            return 0
        if s == "b":
            return 1
        if s == "c":
            return 2
        m.error("unknown variable")

    def val(m, s):
        return int(s)

    def assign(m, left, right):
        match = re.split("\+", right)
        if len(match) == 2:
            return m.add(left, match[0], match[1])
        match = re.split("\-", right)
        if len(match) == 2:
            return m.sub(left, match[0], match[1])
        if left == "br":
            if re.match("[a-z]+", right):
                return m.output(0x8460 + m.var(right))
            else:
                return m.output(0x4000 + m.val(right))
        return m.output(0x9000 + (m.var(left)<<10) + m.val(right))

    def branch(m, left, cond, right, label):
        if cond == "!=":
            op = 0x8800
        elif cond == "<":
            op = 0x8a00
        elif cond == ">=":
            op = 0x8c00
        elif cond == "==":
            op = 0x8e00
        else:
            return m.error("Unknown conditional")
        dest = m.labels[label] - m.cur_line_num - 1
        if dest < 0:
            return m.error("Conditional jumps can't go bacwards")
        if dest > 0x1f:
            return m.error("Conditional jumps can't go so far forwards")
        return m.output(op + (dest<<4) + (m.var(left)<<2) + m.var(right))

    def ramp(m, up, short, time, num):
        # Valid step time is 1 to 31
        op = 0x8400
        if short == "long":
            op |= 0x20
        if up == "down":
            op |= 0x10
        return m.output(op + (m.var(time)<<2) + m.var(num))

    def ramp_im(m, up, short, time, num):
        # Valid step time is 1 to 31
        op = 0x0000
        if short == "long":
            op |= 0x4000
        if up == "down":
            op |= 0x0100
        return m.output(op + (m.val(time)<<9) + m.val(num))

    def add_im(m, var, val):
        return m.output(0x9100 + m.val(val) + (m.var(var) << 10))

    def led_set(m, val):
        # mux sel
        if val == "blue": val = "5"
        if val == "green": val = "6"
        if val == "red": val = "7"
        return m.output(0x9d00 + m.val(val))

    def compile_line(m, l):
        match = re.split("\+=", l)
        if len(match) == 2:
            return m.add_im(match[0], match[1])

        match = re.split("=", l)
        if len(match) == 2:
            return m.assign(match[0], match[1])

        match = re.match("^goto (.*)$", l)
        if match:
            target = m.labels[match.group(1)]
            return m.output(0xa000 + target)
        match = re.match("^if \(([a-z]+)([<>!=]+)([a-z]+)\) goto (.*)$", l)
        if match:
            return m.branch(match.group(1), match.group(2), match.group(3), match.group(4))
        match = re.match("^ramp_(up|down)_(short|long)\(([a-z]+),([a-z]+)\)", l)
        if match:
            return m.ramp(match.group(1), match.group(2), match.group(3), match.group(4))
        match = re.match("^ramp_(up|down)_(short|long)\(([0-9]+),([0-9]+)\)", l)
        if match:
            return m.ramp_im(match.group(1), match.group(2), match.group(3), match.group(4))
        match = re.match("^delay_(short|long)\(([0-9]+)\)", l)
        if match:
            return m.ramp_im("up", match.group(1), match.group(2), 0)
        match = re.match("start()", l)
        if match:
            # mux_map_next()
            return m.output(0x9d80)
        match = re.match("led_clear\(\)", l)
        if match:
            # mux clr
            return m.led_set("0")
        match = re.match("led_set\(([a-z0-9]+)\)", l)
        if match:
            return m.led_set(match.group(1))
        match = re.match("wait_for\(([0-9]+)\)", l)
        if match:
            return m.output(0xe000 + (m.val(match.group(1))<<7))
        match = re.match("trigger\(([0-9]+)\)", l)
        if match:
            return m.output(0xe000 + (m.val(match.group(1))<<1))
        match = re.match(".word ([0-9xabcdef]+)", l)
        if match:
            return m.output(int(match.group(1), 0))
        	
        m.error("unknown statement")

    def compile(m, program, options):
        if not program:
            return
        m.out = ""
        m.program = program
        m.load(m.program)

        for i in range(len(m.statements)):
            m.cur_line_num = i
            m.cur_line = m.statements[i]
            m.compile_line(m.cur_line)

        m.program_it(options)

class ScriptCompiler(Compiler):
    def script_print(m, s):
        m.script += [ s ]

    def script_output(m, options):
        engine = "1"
        # 0000bgr00 for N900
        leds = "111111111"
        if options != "":
            match = re.match("@engine ([1-3]), ([01]{9})", options)
            engine = match.group(1)
            leds = match.group(2)
        engine = "engine"+engine

        m.script_print("")
        m.script_print("cd /sys/class/i2c-adapter/i2c-2/2-0032")
        m.script_print("echo load > %s_mode; echo %s > %s_leds" % (engine, leds, engine))
        m.script_print("echo %s > %s_load" % (m.out, engine))
        if len(m.out)>64:
            m.script_print("### Program too long for default kernel")
        m.script_print("echo disabled > %s_mode" % engine)
        m.script_tail = [ "echo run > %s_mode" % engine ] + m.script_tail

    def program_it(m, options):
        m.script_output(options)

        print("")
        print("RAW", m.out)

    def compile_multi(m, program):
        options = ""
        start = 0
        for i in range(len(program)):
            l = program[i]
            if re.match("^@", l):
                compiler.compile(program[start:i], options)
                options = l
                start = i+1

        compiler.compile(program[start:], options)
        for l in compiler.script:
            print(l)
        for l in compiler.script_tail:
            print(l)

    def compile_string(m, s):
        return m.compile_multi(s.split("\n"))

class HwCompiler(Compiler):
    def script_print(m, s):
        m.script += [ s ]

    def program_it(m, options):
        engine = 1
        # 0000bgr00 for N900
        leds = 0x111111111
        if options != "":
            match = re.match("@engine ([1-3]), ([01]{9})", options)
            engine = int(match.group(1))
            leds = int(match.group(2), 16)

        al = hardware.hw.led
        al.program_engine(engine, leds, m.out)

    def compile_multi(m, program):
        options = ""
        start = 0
        for i in range(len(program)):
            l = program[i]
            if re.match("^@", l):
                compiler.compile(program[start:i], options)
                options = l
                start = i+1

        compiler.compile(program[start:], options)

    def compile_string(m, s):
        return m.compile_multi(s.split("\n"))

class AccelPattern:
    def __init__(m, hwc):
        m.hwc = hwc

    def translate_color(m, s):
        r, g, b = s

    def translate(m, s):
        m.pgm = []
        for i in s:
            m.pgm += [ 'delay_long(31)' ]
            if i == " ":   m.pgm += [ 'br = 0' ]
            elif i == ".": m.pgm += [ 'br = 50' ]
            elif i == "-": m.pgm += [ 'br = 100' ]
            elif i == "x": m.pgm += [ 'br = 150' ]
            elif i == "#": m.pgm += [ 'br = 200' ]
            elif i == "X": m.pgm += [ 'br = 250' ]
            elif i == "|": m.pgm += [ 'br = 250', 'delay_long(1)', 'br = 0' ]

            elif i == "r": m.translate_color([1., 0., 0.])
            elif i == "g": m.translate_color([0., 1., 0.])
            elif i == "b": m.translate_color([0., 0., 1.])
            elif i == "c": m.translate_color([0., 1., 1.])
            elif i == "m": m.translate_color([1., 0., 1.])
            elif i == "y": m.translate_color([1., 1., 0.])
            elif i == "w": m.translate_color([1., 1., 1.])

            else:
                print("unknown character ", i)
            

    def run(m):
        m.options = "@engine 1, 000000100"
        m.pgm = [ "start()",
                      "rep: br = 255",
                      "delay_long(31)",
                      "br = 0",
                      "delay_long(31)",
                      "goto rep" ]
        m.translate(" .-x#X  | |  ")
        hardware.hw.led.all_off()
        m.hwc.compile(m.pgm, m.options)

class BlinkPattern(AccelPattern):
    pass

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "--stdin":
            program = sys.stdin.readlines()
            compiler = ScriptCompiler()
            compiler.compile_multi(program)
        if sys.argv[1] == "--hw":
            program = sys.stdin.readlines()
            compiler = HwCompiler()
            compiler.compile_multi(program)
        if sys.argv[1] == "--pattern":
            compiler = HwCompiler()
            p = BlinkPattern(compiler)
            p.run()

"""
cd /sys/class/i2c-adapter/i2c-2/2-0032
echo load > engine1_mode; echo 111111111 > engine1_leds
echo load > engine1_mode; cat > engine1_load ; echo run > engine1_mode 
9d804000427f0d7f7f007f0042000000
"""

"""
echo load > engine1_mode; echo 111111111 > engine1_leds
echo load > engine1_mode; echo 9d80900191019401950184618e8498009b098e488a229b09a009a004a0029400 > engine1_load
echo load > engine2_mode; echo 98ff84627c00980084627c0095018e14a010a002 > engine2_load
echo run > engine1_mode
"""
