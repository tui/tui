#!/usr/bin/python3
# -*- python -*-

import os

def pcmd(c):
    return os.popen(c).readline()[:-1]

# i2cdump -y 1 0x55 w
#      0,8  1,9  2,a  3,b  4,c  5,d  6,e  7,f
# 00: 0521 0105 0001 ff00 37ff c037 00c0 4600 
# 08: 0246 9a02 0b9a f60b 0ef6 df0e fedf dffe 
# 10: fedf fbfe fffb cbff d8cb ffd8 7fff 007f 
# 18: 0000 0a00 000a 0a00 000a 4600 0a46 ca0a 

class I2C:
    def readb(m, a):
        s = pcmd("sudo i2cget %s %d b" % (m.addr, a))
        return int(s, 0)

    def readw(m, a):
        s = pcmd("sudo i2cget %s %d w" % (m.addr, a))
        return int(s, 0)

    def readf(m, a):
        v = m.readw(a)
        if v > 32767:
            v = v - 65536
        return v / 1000.

class I2C_bat(I2C):
    def __init__(m):
        m.addr = "-y 1 0x55"
        pass

    def readacc(m):
        v = m.readw(0x14) + 65536*m.readw(0x12)
        if v > 32767*65536:
            v = v - 65536*65536
        return v / 1000.
        
        
    def run(m):
        print("Battery:")
        if m.readw(0) != 0x521:
            print("Wrong register ID")

        print("Device id:       %04x" % m.readw(0x00))
        print("Control:         %04x" % m.readw(0x02))
        print("Config 1:        %04x" % m.readw(0x04))
        print("Config 2:        %04x" % m.readw(0x06))
        print("Status:          %04x" % m.readw(0x08))

        print("Temperature now: %.1f C" % ((m.readw(0x0a) / 10.) - 273.15))
        print("Voltage now:     %.3f V" % m.readf(0x0c))
        print("Current now:     %.3f A" % m.readf(0x0e))
        print("Current average: %.3f A" % m.readf(0x10))

        print("Accumulated cur: %.3f As" % m.readacc())

        print("Current limit:   %.3f A" % m.readf(0x26))
        print("Current hys:     %.3f A" % m.readf(0x28))

        print("Device info:     %04x" % m.readw(0x34))
        print("FW revision:     %04x" % m.readw(0x36))

        print("Boot low:        %.3f V" % m.readf(0x62))
        print("Boot always:     %.3f V" % m.readf(0x64))
        print("Boot limit:      %.3f V" % m.readf(0x66))

class I2C_charger(I2C):
    def __init__(m):
        m.addr = "-y 1 0x6b"
        pass

#  i2cdump -y 1 0x6b b
#     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
#00: 40 30 0a 51 09 2c 40 ff ff ff ff ff ff ff ff ff    @0?Q?,@.........

    # http://www.ti.com/lit/ds/symlink/bq24153.pdf
    def run(m):
        print("Charger:")
        print("Status/control:  %02x def 40" % m.readb(0x00))
        print("Control:         %02x def 30" % m.readb(0x01))
        print("Battery voltage: %02x def 0a" % m.readb(0x02))
        print("Vender:          %02x have 51 def 60" % m.readb(0x03))
        print("Charge current:  %02x have 09 def 01" % m.readb(0x04))
        print("Special voltage: %02x have 2c def 24" % m.readb(0x05))
        print("Safety limit:    %02x def 40" % m.readb(0x06))


i2c = I2C_bat()
i2c.run()

i2c = I2C_charger()
i2c.run()
