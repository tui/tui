/* 
 * script in tefone.950 is better
 */
#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

void main(void)
{
	int file;
	int adapter_nr = 2; /* probably dynamically determined */
	char filename[20];

	snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);
	file = open(filename, O_RDWR);
	if (file < 0) {
		printf("No i2c: %m\n");
		exit(1);
	}

	int addr = 0x55; /* The I2C address */

	if (ioctl(file, I2C_SLAVE, addr) < 0) {
		printf("slave: %m\n");
		exit(1);
	}

	__u8 reg = 0x0c; /* Device register to access */

	/* 0x0c, 0x62, 0x64, 0x66: milivolt limits etc */
	__s32 res;
	char buf[10];

	/* Using SMBus commands */
	res = i2c_smbus_read_word_data(file, reg);
	if (res < 0) {
		/* ERROR HANDLING: i2c transaction failed */
	} else {
		printf("Have result: %d\n");
		/* res contains the read word */
	}
}
 

