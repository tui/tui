#!/usr/bin/python3

import gsmpos
import util
import sys

class AveragePos(gsmpos.FakePos):
    def __init__(m, l):
        m.lat = 0
        m.lon = 0
        max1 = 0
        for p in l:
            m.lat += p.lat
            m.lon += p.lon
            if p.eph_m > max1:
                max1 = p.eph_m
        m.lat /= len(l)
        m.lon /= len(l)

        max2 = 0
        for p in l:
            d = util.distance(m, p)
            if d > max2:
                max2 = d

        m.eph_m = max1 + max2 * 1000

class CellPos(gsmpos.CellPos):
    pass

class CellDb(gsmpos.CellDb):
    def add_into(m, d, i, pos):
        if not i in d:
            d[i] = []
        d[i] += [pos]
        
    def approximate(m):
        m.full = {}
        m.part_lac = {}
        m.part_operator = {}        
        m.part_country = {}

        for i in m.lon:
            ( a, b, c, d ) = i
            pos = gsmpos.FakePos()
            pos.lon = m.lon[i]
            pos.lat = m.lat[i]
            pos.desc = m.desc[i]
            pos.eph_m = db.error
            m.add_into(m.full, i, pos)

            j = ( a, b, c )
            m.add_into(m.part_lac, j, pos)

            j = ( a, b )
            m.add_into(m.part_operator, j, pos)

            j = ( a )
            m.add_into(m.part_country, j, pos)
            
    def dump(m, d):
        for lac in d:
            avgpos = AveragePos(d[lac])
            print(lac, avgpos.fmt())
            
    def dump_lac(m):
        d = m.part_lac
        print(",mcc,net,area,cell,,lon,lat,range,,,,,")
        for lac in d:
            avgpos = AveragePos(d[lac])
            mcc, area, net = lac
            print(",%d,%d,%d,,,%f,%f,%f,,,,," % (mcc, area, net, avgpos.lon, avgpos.lat, avgpos.eph_m))
            
    def dump_lacs(m):
        m.dump(m.part_lac) 


if __name__ == "__main__":
    db = CellDb()
    db.init_opencellid("world.ocid")
    #print("Took %d entries from world" % len(db.lat))
    db.approximate()
    db.dump_lac()
    sys.exit(0)

    cellid = (230,2,1137,203716433)
    pos = CellPos(db, cellid)
    print(pos.lat, pos.lon, pos.eph_m)


    cellid = (230,2,1137,123456778)
    pos = CellPos(db, cellid)
    print(pos.lat, pos.lon, pos.eph_m)
    db.approximate()
    pos = CellPos(db, cellid)
    print(pos.lat, pos.lon, pos.eph_m)

    pos = AveragePos(db.part_lac[(230,2,1137)])
    print(pos.lat, pos.lon, pos.eph_m)

    pos = AveragePos(db.part_country[(230)])
    print(pos.lat, pos.lon, pos.eph_m)

#print("------------")
#    db.dump(db.full)
    
    print("------------")
    db.dump_lacs()

    print("------------")
    db.dump(db.part_operator)
    
    print("------------")
    db.dump(db.part_country)

    # Full database of Czech republic is cca 1.34MB, only LACs are cca 15kB.
