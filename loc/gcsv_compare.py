#!/usr/bin/python
# Compare gsmweb.cz and opencellids.org databases.
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import util
import gsmpos

tm = gsmpos.CellDb()
tm.init_old("tm_all.gcsv", 230, 1, "gsmweb.cz T-Mobile")
o2 = gsmpos.CellDb()
o2.init_old("o2_all.gcsv", 230, 2, "gsmweb.cz O2" )
vf = gsmpos.CellDb()
vf.init_old("vf_all.gcsv", 230, 3, "gsmweb.cz Vodafone" )

openc = gsmpos.CellDb()
openc.init_opencellids(name="mls.ocsv")

def select_all(db, cell, s_data):
    return 1

def select_gsm(db, cell, s_data):
    (mcc, mnc, lac, cid) = cell
    return cid < 65536

def select_network(db, cell, s_data):
    (mcc, mnc, lac, cid) = cell
    (s_mcc, s_mnc) = s_data
    return mcc == s_mcc and mnc == s_mnc

def select_boundary(db, cell, s_data):
    sp = util.FakePos()
    sp.lat, sp.lon, s_dist, s_inside = s_data

    cp = util.FakePos()
    cp.lat = db.lat[cell] 
    cp.lon = db.lon[cell]

    dist = util.distance(sp, cp)

#    print sp.lat, sp.lon, cp.lat, cp.lon, dist
    return (dist < s_dist) == s_inside

def select_cr(db, cell, s_data):
    return select_boundary(db, cell, (48.75, 15.33, 620.0/2, s_data))

def select_kbely_zernovka(db, cell, s_data):
    #http://www.openstreetmap.org/?lat=50.073&lon=14.683&zoom=11&layers=M
    return select_network(db, cell, s_data) and select_boundary(db, cell, (50.073, 14.683, 18.0, 1))

def select_kbely_zernovka_gsm(db, cell, s_data):
    return select_kbely_zernovka(db, cell, s_data) and select_gsm(db, cell, s_data)

def select_kbely_zernovka_umts(db, cell, s_data):
    return select_kbely_zernovka(db, cell, s_data) and not select_gsm(db, cell, s_data)

def select_network_no_bogons(db, cell, s_data):
    return select_network(db, cell, s_data) and select_cr(db, cell, 1)

def count(db, selector, s_data):
    count = 0
    all_count = 0
    for cell in db.lat:
        all_count += 1
        if selector(db, cell, s_data):
            count += 1

    print db.name, " contains ", count, " records. (", all_count, " total)"
    return count

def difference(db1, db2, selector, s_data):
    count = 0
    for cell in db1.lat:
        if selector(db1, cell, s_data) and not cell in db2.lat:
            count += 1

    print db1.name, " contains ", count, " records that are not in ", db2.name
    return count

def common(db1, db2, selector, s_data):
    count = 0
    total_dist = 0.0
    dist_10 = 0
    dist_100 = 0
    dist_1000 = 0
    for cell in db1.lat:
        if selector(db1, cell, s_data) and cell in db2.lat:
            count += 1

            p1 = util.FakePos()
            p1.lat = db1.lat[cell]
            p1.lon = db1.lon[cell]

            p2 = util.FakePos()
            p2.lat = db2.lat[cell]
            p2.lon = db2.lon[cell]

            # Distance in kilometers
            dist = util.distance(p1, p2)
            total_dist += dist

            if dist > 100:
              dist_100 += 1

            if dist > 1000:
              dist_1000 += 1

            if dist > 10:
              dist_10 += 1

            if 0 and dist > 1000:
                print "   Databases disagree: ", dist, " km"
                print cell
                print db1.name, p1.lat, p1.lon
                print db2.name, p2.lat, p2.lon

    print db1.name, " and ", db2.name, " have ", count, " records in common."
    if count > 0:
        print "   Average distance is ", total_dist / count, " km"
    print "   Errors over 10 km: ", dist_10
    print "   Errors over 100 km: ", dist_100
    print "   Errors over 1000 km: ", dist_1000
    return count

def compare(db1, db2, selector, s_data, name):
    print name
    print "------------------"
    count(db1, selector, s_data)
    count(db2, selector, s_data)
    difference(db1, db2, selector, s_data)
    difference(db2, db1, selector, s_data)
    common(db1, db2, selector, s_data)
    print

def bogus(dbs):
    print "Entries outside Czech republic:"
    for db in dbs:
        count(db, select_cr, 0)

def print_bogus(dbs):
    for db in dbs:
        for cell in db.lat:
            if not select_cr(db, cell, 1):
                print "Bogus entry ", db.name, cell, db.lat[cell], db.lon[cell], db.desc[cell]

gsmweb = (tm, o2, vf)
all=gsmweb + (openc, )

def statistics():
    compare(tm, openc, select_network_no_bogons, (230, 1), "T-Mobile")
    compare(o2, openc, select_network_no_bogons, (230, 2), "O2")
    compare(vf, openc, select_network_no_bogons, (230, 3), "Vodafone")

    bogus(all)

statistics()
print_bogus(all)

print "Prague - East:"
compare(o2, openc, select_kbely_zernovka, (230, 2), "O2")

print "Prague - East (GSM):"
compare(o2, openc, select_kbely_zernovka_gsm, (230, 2), "O2")

print "Prague - East (UMTS):"
compare(o2, openc, select_kbely_zernovka_umts, (230, 2), "O2")
