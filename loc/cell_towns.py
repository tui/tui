#!/usr/bin/python3

import sys
sys.path += [ "../lib", "../osm" ]
import towns
import nearest
import gsmpos

db = gsmpos.CellDb() 
db.init_opencellid("world-lac.ocid")
town = towns.TownData()

#cellid = (230,2,1137,None)
print(len(town.towns), "towns")
for cellid in db.lat:
    (country, _, _, _) = cellid
    if country != 230:
        continue
    lat, lon = db.lat[cellid], db.lon[cellid]

    point = nearest.Place(lat, lon, "")
    (place, distance) = town.get_nearest(point, town.towns)
    print(cellid, distance, place.fmt())

