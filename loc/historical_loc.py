#!/usr/bin/python
# Compute approximate position based on Cell ID
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import sys
import gsmpos
import re

print "Reading databases..."
old = gsmpos.CellDb()
new = gsmpos.CellDb()
old.init_opencellids("2013.01/cr.ocsv")
new.init_opencellids()


old_matches = 0
new_matches = 0
total = 0

#logf = open("loc.log", 'r')
logf = sys.stdin

for line in logf.readlines():
    match = re.match(".*\(([0-9]*,[0-9]*,[0-9]*,[0-9]*)\).*", line)
    total += 1
    if not match:
        print "No match?"
    else:
        (mcc, mnc, lac, cid) = map(int, match.group(1).split(","))
        id = (mcc, mnc, lac, cid)
        p1 = gsmpos.CellPos(old, id)
        if p1.found: old_matches += 1
        p2 = gsmpos.CellPos(new, id)
        if p2.found: new_matches += 1
        if p1.found and not p2.found: print id, " disappeared from database?"

print total, " total entries"
print "Old matched ", old_matches, " entries, that's ", (old_matches*100)/total , "%"
print "New matched ", new_matches, " entries, that's ", (new_matches*100)/total , "%"
