/* -*- linux-c -*-
 *
 * Copyright 2015,2023 Pavel Machek <pavel@ucw.cz>
 * Copyright Luke Dashjr
 *
 * Based on source from: http://dashjr.org/~luke-jr/tmp/code/gps3.c
 *
 * Distribute under GPLv3.
 *
 * Reads NMEA from stdin and feeds it to gpsd. 

 * On Librem 5:

 * sudo nc -U /var/run/gnss-share.sock | ./gpsin
 */


#define _BSD_SOURCE
#define _XOPEN_SOURCE 600
#define DEBUG

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <pty.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

#include <fcntl.h>

#define BUFFER_SIZE 4096

#define DEBUG
#ifdef DEBUG
#	define debug(...) printf(__VA_ARGS__)
#else
#	define debug(...)
#endif

int
setupGpsPty() {
	int ptyMaster;
	ptyMaster = posix_openpt(O_RDWR);
	if (ptyMaster < 0)
		return -1;
	{
		struct termios tios;
		if (tcgetattr(ptyMaster, &tios))
			return -5;
		cfmakeraw(&tios);
		if (tcsetattr(ptyMaster, TCSANOW, &tios))
			return -6;
	}
	{
		int flags;
		flags = fcntl(ptyMaster, F_GETFL);
		if (flags < 0)
			return -7;
		flags |= O_NONBLOCK;
		if (fcntl(ptyMaster, F_SETFL, flags) == -1)
			return -8;
	}
	if (grantpt(ptyMaster))
		return -2;
	if (unlockpt(ptyMaster))
		return -3;
	const char*ptySlaveLink = getenv("GPS_PTY_LINK");

	{
		const char*ptySlaveName = ptsname(ptyMaster);
		char cmd[10240];

		fprintf(stderr, "got slave name %s\n", ptySlaveName);
		snprintf(cmd, 10239, "/home/purism/g/gpsd-3.22/gpsd-3.22/gpsd/gpsd -N %s &", ptySlaveName);
		system(cmd);
	}
	if (ptySlaveLink && *ptySlaveLink)
	{
		const char*ptySlaveName = ptsname(ptyMaster);
		if (
			(!ptySlaveName)
				||
			((0 != unlink(ptySlaveLink)) && errno != ENOENT)
				||
			(0 != symlink(ptySlaveName, ptySlaveLink))
		)
		{
			close(ptyMaster);
			return -4;
		}
	}
	return ptyMaster;
}

static
void
waitForSlave(int ptyMaster) {
	// HACK, because there's no sane way to detect this :(
	fd_set rfds;
	FD_ZERO(&rfds);
	struct timeval tv;
	while (1)
	{
		write(ptyMaster, "$GP\r\n", 5);
		debug("Waiting for slave...\n");
		FD_SET(ptyMaster, &rfds);
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		if (select(ptyMaster + 1, &rfds, NULL, NULL, &tv))
			break;
	}
}

// BEGIN: Lifted from gpsd-2.32/nmea_parse.c
void nmea_add_checksum(char *sentence)
/* add NMEA checksum to a possibly  *-terminated sentence */
{
    unsigned char sum = '\0';
    char c, *p = sentence;

    if (*p == '$') {
	p++;
    } else {
#if 0
        gpsd_report(1, "Bad NMEA sentence: '%s'\n", sentence);
#endif
    }
    while ( ((c = *p) != '*') && (c != '\0')) {
	sum ^= c;
	p++;
    }
    *p++ = '*';
    (void)snprintf(p, 5, "%02X\r\n", (unsigned)sum);
}

int nmea_send(int fd, const char *fmt, ... )
/* ship a command to the GPS, adding * and correct checksum */
{
    int status;
    char buf[BUFSIZ];
    va_list ap;

    va_start(ap, fmt) ;
    (void)vsnprintf(buf, sizeof(buf)-5, fmt, ap);
    va_end(ap);
    if (buf[0] == '$') {
	strcat(buf, "*");
	nmea_add_checksum(buf);
    } else
	strcat(buf, "\r\n");
    printf("writing '%s' to gps\n", buf);
    status = (int)write(fd, buf, strlen(buf));
    if (status == (int)strlen(buf)) {
#if 0
	gpsd_report(2, "=> GPS: %s\n", buf);
#endif
	return status;
    } else {
#if 0
	gpsd_report(2, "=> GPS: %s FAILED\n", buf);
#endif
	return -1;
    }
}
//  END : Lifted from gpsd-2.32/nmea_parse.c

static
int sck;
static
char isActive = 0, nopm = 0;

static
void
sigShutdown(int signum) {
	debug("Shutdown signal received\n");
	exit(0);
}

static
void
handlePhonetPacket(int sck, int pty) {
	int fd = pty, status;
	char buf[BUFFER_SIZE];
	
	ssize_t buflen = read(sck, buf, sizeof(buf));
	assert(buflen > 0);
	debug("Read %d bytes : ", buflen);
	buf[buflen] = 0;

    status = (int)write(fd, buf, strlen(buf));
    if (status == (int)strlen(buf)) {
#if 1
	debug("=> GPS: %s\n", buf);
#endif
	return status;
    } else {
#if 0
	gpsd_report(2, "=> GPS: %s FAILED\n", buf);
#endif
	return -1;
    }


#if 0
	float lat, lon;

	if (buf[0] == 'l') {
		sscanf(buf+2, "%f %f\n", &lat, &lon);

		uint8_t latD = abs((int16_t)lat);
		float latM = (fabs(lat) - (float)latD) * 60;
		uint8_t lonD = abs((int16_t)lon);
		float lonM = (fabs(lon) - (float)lonD) * 60;
		int time_hour = 12, time_minute =34 , time_second =56 , time_ms =0;

		debug("Have %f %f\n", lat, lon);
		
		nmea_send(pty, "$GPGLL,%d%09.6f,%c,%d%09.6f,%c,%02d%02d%02d.%02d,A", latD, latM, (lat < 0) ? 'S' : 'N', lonD, lonM, (lon < 0) ? 'W' : 'E', time_hour, time_minute, time_second, time_ms / 10);
		return;
	}
	if (buf[0] == 'n') {
		nmea_send(pty, "%s", buf+1);
	}
#endif
}


void parse_opts(int argc, char *argv[]) {
	int flags, opt;
	int nsecs, tfnd;

	nsecs = 0;
	tfnd = 0;
	flags = 0;
	while ((opt = getopt(argc, argv, "d")) != -1) {
		switch (opt) {
		case 'd':
			nopm = 1;
			break;
		default: /* '?' */
			fprintf(stderr, "Usage: %s [-d]\n",
				argv[0]);
			exit(EXIT_FAILURE);
		}
	}
}

int
main(int argc, char *argv[]) {
	parse_opts(argc, argv);
  
	signal(SIGHUP, sigShutdown);
	signal(SIGINT, sigShutdown);
	signal(SIGQUIT, sigShutdown);
	signal(SIGPIPE, sigShutdown);
	
	sck = 0;
	assert(sck >= 0);
	
	int pty = setupGpsPty();
	assert(pty >= 0);
	
	int maxfd = pty;
	if (sck > maxfd)
		maxfd = sck;
	++maxfd;
	
	while (1)
	{
		int ptySlave = open(ptsname(pty), O_RDWR | O_NOCTTY);
		assert(ptySlave >= 0);
		// 1. Select on READ until gpsd probes, writing "$GP\r\n" every so often
		if (!nopm)
			waitForSlave(pty);
		// 2. Close our slave fd
		close(ptySlave);
		// 3. ACTIVATE AND RUN
		debug("TTY active, starting GPS\n");
		isActive = 1;
		while (1)
		{
			fd_set rfds;
			FD_ZERO(&rfds);
			if (!nopm)
				FD_SET(pty, &rfds);
			FD_SET(sck, &rfds);
			select(maxfd, &rfds, NULL, NULL, NULL);

			if (FD_ISSET(sck, &rfds))
				handlePhonetPacket(sck, pty);

			if (FD_ISSET(pty, &rfds))
			{
				char buf[BUFFER_SIZE];
				if (read(pty, buf, sizeof(buf)) < 1)
					// EOF or error, idle GPS
					break;
			}
		}
		// 4. Until EOF from master fd
		debug("TTY idle, shutting off GPS\n");
		isActive = 0;
		// 5. Reopen slave fd, and start over
	}
}
