#!/usr/bin/python
# Compute approximate position based on Cell ID
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import sys
import gsmpos
import re
import util

print "Reading databases..."
gsm = gsmpos.CellDb()
if 1:
    gsm.init_old("tm_all.gcsv", 230, 1, "gsmweb.cz T-mobile" )
    gsm.init_old("o2_all.gcsv", 230, 2, "gsmweb.cz O2" )
else:
    gsm.init_opencellids()

gsm_matches = 0
no_gps = 0
total = 0

num = 0
dist = 0

logf = sys.stdin

distances = []
num_zeros = 0

for line in logf.readlines():
    match = re.match(".*\(([0-9]*,[0-9]*,[0-9]*,[0-9]*)\).*", line)
    total += 1
    if not match:
        print "No match?"
    else:
        (mcc, mnc, lac, cid) = map(int, match.group(1).split(","))
        id = (mcc, mnc, lac, cid)
        p1 = gsmpos.CellPos(gsm, id)

    if not p1.found: 
        continue
    gsm_matches += 1

    match = re.match(".*bssid: ([0-9a-f]*).*", line)
    if match:
        pass

    match = re.match(".*gps ([0-9]*\.[0-9]* [0-9]*\.[0-9]*).*", line)
    if not match:
        no_gps += 1
        continue

    p = gsmpos.FakePos()
    (p.lat, p.lon) = map(float, match.group(1).split(" "))

    num += 1
    d = util.distance(p, p1) * 1000.
    dist += d

    distances = distances + [ d ]
    if d == 0:
        num_zeros += 1

print total, " total entries"
print "Gsm matched ", gsm_matches, " entries, that's ", (gsm_matches*100)/total , "%"
print "GPS unavailable ", no_gps, " entries, that's ", (no_gps*100)/total , "%"
print "Valid measurements ", num, " average distance ", dist/num, "m"

print "Distances between GPS and GSM cell (meters)"
print
gsmpos.distance_statistics(distances)

print num_zeros, " zero distances "



