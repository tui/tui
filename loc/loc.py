#!/usr/bin/python
# Log positions / cell IDs / wifi IDs.
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import android
import sys
import time
import util
import gsmpos
import gc
import agsmpos

class DbStatisticsSet():
  def __init__(self):
        self.old = 0
        self.seen = 0
        self.new = 0

  def fmt(self):
    return "%d new, %d seen, %d old" % (self.new, self.seen, self.old)

class DbStatistics():
  def __init__(self):
        self.total = DbStatisticsSet()
        self.hourly = DbStatisticsSet()

  def inc_old(self):
    self.hourly.old += 1
    self.total.old += 1

  def inc_seen(self):
    self.hourly.seen += 1
    self.total.seen += 1

  def inc_new(self):
    self.hourly.new += 1
    self.total.new += 1

  def fmt_hour(self):
    s = self.hourly.fmt() + " (total " + self.total.fmt() + ")"
    self.hourly = DbStatisticsSet()
    return s

class StatisticsSet():
  def __init__(self):
        self.scans = 0
        self.gps_on = 0
        self.gps_fix = 0

class PosStatistics:
  def stats_init(self):
    print "Initializing statistics for ", self
    self.stats = DbStatistics()
    self.done = {}
    self.seen = {}

  def new_cell(self, name):
    if self.have_gps():
        print("Ding dong. Have new cell for " + name + ".")
    else:
        self.say("Current cell not in " + name + ".")

  def have_id(self, id):
    # Don't count into statistics, already seen it this session
    if id in self.done:
      self.done[id] += 1
      return

    if self.found:
      self.stats.inc_old()
      self.done[id] = 1
      return

    self.master.use_gps = 600
    # FIXME: say we have new_cell ?

    if self.master.have_gps():
      self.done[id] = 1
      self.stats.inc_new()
    else:
      if id in self.seen:
        self.seen[id] += 1
      else:
        self.stats.inc_seen()
        self.seen[id] = 1

  def fmt_hour(self):
    return " " + self.name + " " + self.stats.fmt_hour()

  def fmt(self):
    if self.found:
        s = self.name + " " + self.fmt_loc()
        if self.master.use_gps and self.master.have_gps():
            dist = util.distance(self.master.gpspos, self)
            s = s + ("dist %1.3f km " % dist)
    else:
        s = "no " + self.name + " "
    return s

droid = agsmpos.droid

class GsmPos(agsmpos.GsmPos, PosStatistics):
  pass

class OpencPos(agsmpos.GsmPos, PosStatistics):
  def __init__(self):
    self.apos_init()
    self.stats_init()
    self.db.init_opencellids()
    self.name = "openc"

class GsmwebPos(GsmPos, PosStatistics):
  def __init__(self):
    self.apos_init()
    self.stats_init()
    #gw.init_old("tm_all.gcsv", 230, 1, "gsmweb.cz T-Mobile")
    #self.db.init_old("o2_all.gcsv", 230, 2, "gsmweb.cz O2" )
    #gw.init_old("vf_all.gcsv", 230, 3, "gsmweb.cz Vodafone" )
    self.name = "gw"

class WifiPos(agsmpos.WifiPos, PosStatistics):
  def __init__(self):
    self.apos_init()
    self.stats_init()    
    self.db.init_wifi()
    self.name = "wifi"

  def init_writer(self):
        xml = open("wifi.%d.xml" % time.time(), 'w')

        xml.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        xml.write('<logfile manufacturer="Maemo/Python" model="N900" revision="0.0.1" swid="http://sourceforge.net/projects/tui/" swver="0.0.2">\n')
        return xml

class GpsPos(agsmpos.GpsPos, PosStatistics):
  def __init__(self):
    self.name = "gps"
    self.hourly = StatisticsSet()
    self.total = StatisticsSet()
    agsmpos.GpsPos.__init__(self)

  def fmt(self):
    s = ''
    if self.master.use_gps:
                if self.master.have_gps():
                    s = s + "gps " + self.fmt_loc()
                    s = s + " %3.0f m" % (self.eph / 100.0)
                else:
                    s = s + "no gps"
    return s

  def fmt_hour(self):
    s = " scans %d (%d total)" % (self.hourly.scans, self.total.scans)
    s += " gps on %d fix %d (total on %d fix %d)" % (self.hourly.gps_on, self.hourly.gps_fix, self.total.gps_on, self.total.gps_fix)
    self.hourly = StatisticsSet()
    #print gc.get_objects()
    return s

  def sleep_for(self, sleep_time):
    self.total.scans += 1
    self.hourly.scans += 1
    self.total.gps_on += sleep_time * (self.master.use_gps > 0)
    self.hourly.gps_on += sleep_time * (self.master.use_gps > 0)
    self.total.gps_fix += sleep_time * self.master.have_gps()
    self.hourly.gps_fix += sleep_time * self.master.have_gps()
    time.sleep(sleep_time)

class GsmLogger():
  def __init__(self):
        self.use_gps = 0

        self.use_gsm = 1
        print "Reading databases..."
        self.opencpos = OpencPos()
        self.gsmwebpos = GsmwebPos()
        self.gpspos = GpsPos()

        self.use_wifi = 1
        if self.use_wifi:
            self.wifipos = WifiPos()
            self.xml = self.wifipos.init_writer()

        self.poss = [ self.opencpos, self.gsmwebpos, self.wifipos, self.gpspos ]
        for pos in self.poss:
            pos.master = self

        self.logf = open("loc.%d.log" % time.time(), 'w')

        self.new_hour()
        print "Ready"

  def have_gps(self):
    return self.gpspos.eph < 50000

  def say(self, what):
    return
    droid.ttsSpeak(what)

  def wifi_iteration(self, pos):
            s = " bssid:"
            ids = self.wifipos.sense()
            for id in ids:
                s = s + " " + id
                self.wifipos.locate(id)
                s = s + " " + self.wifipos.fmt()
                self.wifipos.have_id(id)

            if self.have_gps():
                self.xml.write('<scan>\n')
                self.xml.write('<gps lng="%f" lat="%f" accuracy="%.2f" />\n' % (pos.lon, pos.lat, pos.eph/100.0))
                for id in ids:
                    self.xml.write('<wifiap bssid="%s" />\n' % id)
                    self.xml.write('\n')
                    self.xml.write('<gps lng="%f" lat="%f" accuracy="%.2f" />\n' % (pos.lon, pos.lat, pos.eph/100.0))
                self.xml.write('</scan>\n')
                self.xml.flush()

            return s

  def gsm_iteration(self, pos):
      s = ""
      ids = self.opencpos.sense()
      for id in ids: 
            self.prev_id = self.id
            self.id = id

            s += " (%d,%d,%d,%d) " % self.id

            self.opencpos.locate(id)
            s += self.opencpos.fmt()
            self.gsmwebpos.locate(id)
            s += self.gsmwebpos.fmt()

            if self.gsmwebpos.found:
		    s += self.gsmwebpos.db.desc[self.id]

            if self.gsmwebpos.found and self.opencpos.found:
		    dist = util.distance(self.gsmwebpos, self.opencpos)
		    s = s + (" gw-openc %1.3f km " % dist)

            self.opencpos.have_id(self.id)

      return s

  def new_hour(self):
    self.hour_start = time.time()
    tm = time.localtime()
    s = "%d:%02d:" % (tm.tm_hour, tm.tm_min)
    s += self.gpspos.fmt_hour()
    s += self.opencpos.fmt_hour()
    s += self.wifipos.fmt_hour()
    print s

  def main_loop(self):
        self.prev_id = (0,0,0,0)
        self.id = (0,0,0,0)
        sleep_time = 5
        while True:
            if self.use_gps:
              droid.startLocating()
              self.gpspos.locate()
            pos = self.gpspos

            s = '%d' % time.time() + " " + self.gpspos.fmt()
            if self.use_gsm:
                s += self.gsm_iteration(pos)
            if self.use_wifi:
                s += self.wifi_iteration(pos)

            sleep_time = sleep_time * 2
            if self.prev_id != self.id:
                sleep_time = 5
            if self.use_gps:
                sleep_time = 5

            if sleep_time < 5:
                sleep_time = 5
                self.say("Moving.")
            if sleep_time > 600:
                sleep_time = 600
                self.say("Stationary.")
            if sleep_time > 5:
                s = s + ( " stationary %d " % sleep_time )

            self.logf.write(s+"\n")
            if 1:
                print s
            sys.stdout.flush()
            self.logf.flush()

            if self.use_gps:
                self.use_gps -= sleep_time
                if self.use_gps < 0:
                    if pos.eph >= 50000:
                        self.say("Could not acquire GPS signal.")
                    droid.stopLocating()
                    self.use_gps = 0

            if time.time() - self.hour_start > 360: # FIXME: 3600:
              self.new_hour()
            self.gpspos.sleep_for(sleep_time)

logger = GsmLogger()
logger.main_loop()
