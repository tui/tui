#!/usr/bin/python3
# Parse .gcsv and .ocsv files
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

class FakePos:
    def fmt(m):
        return ("%f %f eph %f m" % (m.lat, m.lon, m.eph_m))

class CellPos(FakePos):
    def __init__(self, db, id):
        self.db = db
        self.locate(id)

    def init(self):
        self.found = 0
        self.eph_m = 99999999
        self.lat = 0
        self.lon = 0

    def locate(self, id):
        db = self.db
        self.init()
        if id in db.lon:
            self.lat = db.lat[id]
            self.lon = db.lon[id]
            self.desc = db.desc[id]
            self.eph_m = db.error
            self.found = 1

class BaseDb:
    def __init__(self):
        self.lon={}
        self.lat={}
        self.desc={}
        self.error = 4780 # 95th percentile

    def locate(self, id):
        return CellPos(self, id)

class CellDb(BaseDb):
    def read_opencellids(self, name):
      f = open(name, 'r')
      for l in f.readlines():
        fi = l.split(";")
    
        # 0 1 2   3   4   5   6   7
        # ? ? lat lon mcc mnc lac cid

        if float(fi[2]) == 0.0: continue
        if float(fi[3]) == 0.0: continue

        if fi[4] == "" or fi[5] == "" or fi[6] == "" or fi[7] == "":
            print("Strange line", fi)
            continue
        id = (int(fi[4]), int(fi[5]), int(fi[6]), int(fi[7]))
        self.lat[id] = float(fi[2])
        self.lon[id] = float(fi[3])
        self.desc[id] = fi[13]

    def init_opencellids(self, name="cr.ocsv"):
        self.read_opencellids(name)
        self.name = "openccellids.org"

    def in_subset_half(self, lat, lon, mcc):
        if lat < 30: return 0
        if lat > 60: return 0
        if lon < 00: return 0
        if lon > 30: return 0
        return 1
        
    def in_subset_europe(self, lat, lon, mcc):
        if lat < 45: return 0
        if lat > 55: return 0
        if lon < 10: return 0
        if lon > 20: return 0
        return 1

    def in_subset_half(self, lat, lon, mcc):
        if mcc >= 300: return 1
        return 0

    def in_subset(self, lat, lon, mcc):
        return 1

    def read_opencellid(self, name):
      f = open(name, 'r')
      first = 1
      for l in f.readlines():
        if first:
          if l == "radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal\n":
            first = 0
            continue
          if l == ",mcc,net,area,cell,,lon,lat,range,,,,,\n":
            first = 0
            continue
        
        fi = l.split(",")

        # 0     1   2   3    4    5    6   7   8     9       10         11      12      13
        # radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal

        if fi[1] == "radio": continue
        if float(fi[6]) == 0.0: continue
        if float(fi[7]) == 0.0: continue
        mcc = int(fi[1])
        cell = None
        if fi[4] != "":
            cell = int(fi[4])

        id = (mcc, int(fi[2]), int(fi[3]), cell)
        lat, lon = float(fi[7]), float(fi[6])
        if not self.in_subset(lat, lon, mcc):
            continue
        self.lat[id] = lat
        self.lon[id] = lon
        self.desc[id] = ""
        #print("Good line", fi)

        # cat delme.txt | grep [MS],230 > cr.ocid
    def init_opencellid(self, name="cr.ocid"):
        self.read_opencellid(name)
        self.name = "openccellid.org"
        
    def read_oldstyle(self, name, mcc, mnc):
        f = open(name, 'r')
        for l in f.readlines():
          fi = l.split(";")
          #     CID    LAC    GPS    Umisteni
          #print fi[0], fi[2], fi[8], fi[7]
          id = (mcc, mnc, int(fi[2]), int(fi[0]))
          if fi[8] != '':
              try: 
                  gpsp = fi[8].split(",")
                  self.lon[id] = float(gpsp[0])
                  self.lat[id] = float(gpsp[1])
                  self.desc[id] = fi[7]
              except:
                  print("Could not parse: ", l)
      
    def init_old(self, fname, mcc, mnc, name):
        self.read_oldstyle(fname, mcc, mnc)
        self.name = name

class PosDb(CellDb):
    def init_wifi(self, name="wifi.csv"):
        self.error = 2833

        f = open(name, 'r')
        for l in f.readlines():
            fi = l.split(",")

        # 0     1   2   3     4   5   6   7
        # rem?  lon lat bssid

            id = fi[3][1:-1]
            self.lat[id] = float(fi[2])
            self.lon[id] = float(fi[1])
            self.desc[id] = fi[0]


def distance_statistics(distances):
    distances.sort()
    print(max(distances), " maximum distance ")
    print(distances[len(distances*99)/100], " 99th percentile ")
    print(distances[len(distances*95)/100], " 95th percentile ")
    print(distances[len(distances*9)/10], " 90th percentile  ")
    print(distances[len(distances*8)/10], " 80th percentile  ")
    print(distances[len(distances*7)/10], " 70th percentile  ")
    print(distances[len(distances*6)/10], " 60th percentile  ")
    print(distances[len(distances)/2], " median  ")
    print(distances[len(distances*4)/10], " 40th percentile  ")
    print(distances[len(distances*3)/10], " 30th percentile  ")
    print(distances[len(distances*2)/10], " 20th percentile  ")
    print(distances[len(distances*1)/10], " 10th percentile  ")
    print(sum(distances)/len(distances), " average distance ")

if __name__ == "__main__":
    db = CellDb()
    db.init_opencellids()
    cellid = (230,2,1137,203716433)
    pos = CellPos(db, cellid)
    print(pos.lat, pos.lon, pos.eph_m)
