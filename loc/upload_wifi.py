#!/usr/bin/python
# Upload 
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

# unused and unusable. loc.py now exports xml directly. 
# use wifi_convert for upload. 

import sys
import os
import time
import util
import gsmpos
import re

print "Reading databases..."
old = gsmpos.CellDb()
old.init_opencellids()

print '<?xml version="1.0" encoding="UTF-8"?><logfile manufacturer="nokia" model="N900" revision="0.1" swid="upload_wifi" swver="0.1"><scan time="20130318000000" distance="100" >'

no_gps = 0
for line in logf.readlines():
    match = re.match("bssid (.*)$", line)
    total += 1
    if not match:
        print "No match?"
        bugs += 1
        continue

    bssids = match.group(1).split(" ")

    match = re.match(".*gps ([0-9]*\.[0-9]* [0-9]*\.[0-9]*).*", line)
    if not match:
        no_gps += 1
        continue

    (lat, lon) = map(float, match.group(1).split(" "))

