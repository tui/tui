#!/usr/bin/python
# Fuse data from different sensors
#
# Time in seconds, distance in meters. 
#
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import fusion
import re
from shapely.geometry import Point

class Cmdline(fusion.Fusion):
  def command(self, cmd, s):
    l = len(s)
    return cmd[0:l] == s

  def intarg(self, cmd):
    return int(cmd.split(" ")[1])

  def load_logf(self, fname):
    self.logf = open(fname, 'r')

  def step_logf(self):
    line = self.logf.readline()
    print line

    # GSM
    match = re.match(".*\(([0-9]*,[0-9]*,[0-9]*,[0-9]*)\).*", line)
    if not match:
      print "### No GSM?"
      self.errors += 1
    else:
      self.got_gsm(match.group(1))

    # WIFI
    match = re.match(".*bssid: ([0-9a-f]*).*", line)
    if match:
      self.got_wifi(match.group(1))

    match = re.match(".*gps ([0-9]*\.[0-9]* [0-9]*\.[0-9]*).*", line)
    if match:
      print "Have gps: ", match.group(1)
      (lat, lon) = map(float, match.group(1).split(" "))
      p = Point(lat, lon)
      if not self.guess.pol.contains(p):
        self.outside += 1
        print "### GPS outside guess"
      else:
        print "GPS inside, ok."

    # Stationary tag
    match = re.match(".*stationary ([0-9]*).*", line)
    time = 5
    if match:
      print "Have time: ", int(match.group(1))
    self.advance_time(time, 37.)

  def bogons(self):
    return (self.errors, self.guess.teleports, self.outside)

  def interactive(self):
    self.errors = 0
    self.outside = 0
    self.distance = -1
    while True:
        print "Time ", self.time, " position ", self.guess.polygon_summary(), " err %d/tele %d/out %d " % self.bogons()

        print "Ready ]",
        cmd = raw_input()
        if self.command(cmd, "url"):
          print self.guess.polygon_url()
        elif self.command(cmd, "walk"):
          print self.advance_time(self.intarg(cmd), 2.)
        elif self.command(cmd, "car"):
          print self.advance_time(self.intarg(cmd), 37.)
        elif self.command(cmd, "wifi"):
          # 0011950530d7
          id = cmd.split(" ")[1]
          self.got_wifi(id)
        elif self.command(cmd, "gsm"):
          id = cmd.split(" ")[1]
          self.got_gsm(id)
        elif self.command(cmd, "logf"):
          id = cmd.split(" ")[1]
          self.load_logf(id)
        elif self.command(cmd, "go"):
          b = self.bogons()
          while b == self.bogons():
            self.step_logf()
        elif self.command(cmd, "finish"):
          b = self.errors
          while b == self.errors:
            self.step_logf()
        elif cmd == "":
          print "Should step from file"
          self.step_logf()
        else:
          print "unknown command"

fusion = Cmdline()
fusion.init_gsm()
fusion.init_wifi()
fusion.interactive()
