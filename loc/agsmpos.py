#!/usr/bin/python
# Compute approximate position based on Cell ID / wifi / GPS.
# gsmpos, android specific stuff.
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import android
import sys
import time
import util
import gsmpos
import gc
import getopt
import agsmpos

droid = android.Android()

class APos(gsmpos.CellPos):
  def fmt_loc(self):
    return "%3.4f %3.4f " % (self.lat, self.lon)

class DbPos(APos):
  def apos_init(self):
    self.db = gsmpos.PosDb()

class GsmPos(DbPos):
  def sense(self):
        #    print droid.get_signal_strength()
        try:
            # FIXME: That is maemo-specific droid class.
            stat = droid.get_registration_status()
            ( _, lac, cid, mnc, mcc, _, _, _ ) = stat
            id = (mcc, mnc, lac, cid)
            return [ id ]
        except:
            return []

class OpencPos(GsmPos):
  def __init__(self):
    self.apos_init()
    self.db.init_opencellids()
    self.name = "openc"

class GsmwebPos(GsmPos):
  def __init__(self):
    self.apos_init()
    self.db.init_old("tm_all.gcsv", 230, 1, "gsmweb.cz T-Mobile")
    #self.db.init_old("o2_all.gcsv", 230, 2, "gsmweb.cz O2" )
    #self.db.init_old("vf_all.gcsv", 230, 3, "gsmweb.cz Vodafone" )
    self.name = "gw"

class WifiPos(DbPos):
  def __init__(self):
    self.apos_init()
    self.db.init_wifi()
    self.name = "wifi"

  def sense(self):
    ids = []
    (a, scan, b) = droid.wifiGetScanResults()
    for a in scan:
      s = a['bssid'].lower().replace(":", "")
      ids = ids + [ s ]
    return ids

class GpsPos(APos):
  def __init__(self):
    self.name = "gps"
    self.lat = 50
    self.lon = 14
    self.eph = 100000000
    self.found = 0

  def sense(self):
    raise "just use locate"

  def locate(self):
                (a, p, b) = droid.getLastKnownLocation()
                if "gps" in p:
                    p = p["gps"]
                    sys.stderr.write("GPS: %f deg %f deg %f ? %f m %f km/h\n" % (p["latitude"], p["longitude"], p["accuracy"], p["altitude"], 0))

                self.lat = p["latitude"]
                self.lon = p["longitude"]
                self.eph = p["accuracy"]
                self.found = 1
