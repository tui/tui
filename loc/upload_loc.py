#!/usr/bin/python
# Upload 
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import sys
import os
import gsmpos
import re

print "Reading databases..."
old = gsmpos.CellDb()
old.init_opencellids()

#logf = open("loc.log", 'r')
logf = sys.stdin
total = 0
bugs = 0
upload = 0
old_matches = 0
no_gps = 0
new_no_gps = 0

for line in logf.readlines():
    match = re.match(".*\(([0-9]*,[0-9]*,[0-9]*,[0-9]*)\).*", line)
    total += 1
    if not match:
        print "No match?"
        bugs += 1
        continue

    (mcc, mnc, lac, cid) = map(int, match.group(1).split(","))
    id = (mcc, mnc, lac, cid)
    p1 = gsmpos.CellPos(old, id)
# We actually want to upload duplicates, so that averaging has chance.
#    if p1.found:
#        old_matches += 1
#        continue		# stop upload of duplicates

    match = re.match(".*gps ([0-9]*\.[0-9]* [0-9]*\.[0-9]*).*", line)
    if not match:
        if not p1.found:
            new_no_gps += 1
        no_gps += 1
        continue

    (lat, lon) = map(float, match.group(1).split(" "))
    key = "eb0ef30e-ae86-41bc-8129-2f26ec713a06"
    print "Uploading ", id, lat, lon
    os.system("cd delme.dir && wget 'http://www.opencellid.org/measure/add?key="+key+"&mcc=%d&mnc=%d&lac=%d&cellid=%d&lat=%3.8f&lon=%3.8f'" % (mcc, mnc, lac, cid, lat, lon))
    upload += 1

print total, " total entries"
print old_matches, " already known, that's ", (old_matches*100)/total , "%"
print no_gps, " gps unavailable, that's ", (no_gps*100)/total , "%"
print new_no_gps, " gps unavailable in new, that's ", (new_no_gps*100)/total , "%"
print "Uploaded ", upload, " new ", (upload*100)/total , "%"
