#!/usr/bin/python
# Fuse data from different sensors
#
# Time in seconds, distance in meters. 
#
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later
#
# Shapely manual:
# http://toblerity.github.io/shapely/manual.html


import util
import math
import gsmpos
from shapely.geometry import Point
from gsmpos import FakePos

class Guess:
  def set_position(self, pos):
    self.pol = Point(pos.lat, pos.lon)
    self.add_error(pos.eph_m)
    self.teleports = 0

  def get_error(self):
    p1 = FakePos()
    p2 = FakePos()
    (p1.lat, p1.lon, p2.lat, p2.lon) = self.pol.bounds
    return util.distance(p1, p2) * 1000. / 2

  def polygon_summary(self):
    p1 = FakePos()
    p2 = FakePos()
    (p1.lat, p1.lon, p2.lat, p2.lon) = self.pol.bounds
    return "%f %f, %.0f m %.0f m" % ((p1.lat+p2.lat)/2. , (p1.lon+p2.lon)/2., self.get_error(), math.sqrt(self.pol.area)*1000000*0.076248)

  def polygon_url(self):
    # &mode=Draw&d0p0lat=50.148957530297&d0p0lon=14.478607177735&d0p1lat=50.114625352324&d0p1lon=14.450454711915&d0p2lat=50.099211881155&d0p2lon=14.488220214844&d0p3lat=50.113304392014&d0p3lon=14.543838500977&d0p4lat=50.142357101309&d0p4lon=14.548645019532&d0p5lat=50.152477386656&d0p5lon=14.516372680665&dp_num=6&?342,89
    bounds = self.pol.bounds
    p1 = FakePos()
    p2 = FakePos()
    (p1.lat, p1.lon, p2.lat, p2.lon) = bounds
    latavg = (p1.lat+p2.lat)/2.
    lonavg = (p1.lon+p2.lon)/2.
    s = "http://ojw.dev.openstreetmap.org/StaticMap/?lat=%f&lon=%f&z=14&show=0" % (latavg, lonavg)
    s += "&w=1024&h=768"

    c = list(self.pol.simplify(0.1).boundary.coords)
    s += "&dp_num=%d" % (len(c)+1)
    i = 0
    for a in c+list( (c[0],) ):
      (lat, lon) = a
      s += "&d0p%dlat=%f&d0p%dlon=%f" % (i, lat, i, lon)
      i += 1

    return s

  def add_error(self, meters):
    # 111.120km is 1 degree latitutde
    self.pol = self.pol.buffer(meters/111120., 3)

  def intersect_with(self, pos):
    new = Guess()
    new.set_position(pos)
    print "Got new measurement ", new.polygon_summary()
    self.pol = self.pol.intersection(new.pol)
    if self.pol.is_empty:
      print "### Teleport detected"
      self.teleports += 1
      self.pol = new.pol

class Fusion:
  def __init__(self):
    ipos = FakePos()
    ipos.lat = 50.14
    ipos.lon = 14.50
    ipos.eph_m = 40000000
    self.guess = Guess()
    self.guess.set_position(ipos)
    self.time = 0
    pass

  def advance_time(self, seconds, speed):
    # speed in m/s
    self.guess.add_error(seconds*speed)
    self.time += seconds

  def init_wifi(self):
    self.wdb = gsmpos.PosDb()
    self.wdb.init_wifi()
  
  def init_gsm(self):
    self.openc = gsmpos.CellDb()
    self.openc.init_opencellids()

  def got_wifi(self, id):
    p = self.wdb.locate(id)
    if p.found:
      self.guess.intersect_with(p)

  def got_gsm(self, id):
    id = tuple(map(int, id.split(",")))
    p = self.openc.locate(id)
    if p.found:
      self.guess.intersect_with(p)
