#!/usr/bin/python
# 
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

# scp gps.py talking_gps.py pavel@maemo:~ && ssh pavel@maemo "./talking_gps.py Home at table"

import android
import sys
import time
import gps

print "Ready"
droid = android.Android()

start = time.time()

print "Starting talking_gps"
droid.ttsSpeak("Starting")

logf = open("talking_gps.log", 'a')
logf.write("Starting, situation: " + " ".join(sys.argv[1:]) + "\n")
logf.flush()
interval = 60
while True:
    pos = gps.OneShotPosition()
    pos.get_position(30000, interval)
    minutes = (time.time() - start)/60
    got_fix = (pos.eph < 30000)
    print "eph ", pos.eph, " got fix ", got_fix
    if not got_fix:
        msg = "No fix after %d minutes." % minutes
    else:
    	msg = "Hooray. Got position after %d minutes." % minutes

    droid.ttsSpeak(msg)
    print(msg)
    logf.write(msg+"\n")
    logf.write("position error %f m.\n" % (pos.eph/100.0))
    logf.flush()
    sys.stdout.flush()
    if got_fix:
        break

    interval *= 2
