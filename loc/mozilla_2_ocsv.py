#!/usr/bin/python
#
# ./mozilla_2_ocsv.py < /tmp/delme | grep ";230;[123];" > mls.ocsv

import sys
for l in sys.stdin.readlines():
	fields = l.split(",")
	radio,mcc,net,area,cell,unit,lon,lat,range,samples,changeable,created,updated,averageSignal = fields

	print "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;" % \
	       ("", "", lat, lon, mcc, net, area, cell, "", "", "", "", "" )
