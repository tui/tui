#!/usr/bin/python
# Convert data from wifi openbmap style
# cat wifi.openbmap/* | ./wifi_convert.py
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv2 or later

import sys
import re
import gsmpos
import util
import copy

logf = sys.stdin
rawf = open("wifi.wigle.csv", 'w')
rawf.write("WigleWifi-0.0\r\n")
rawf.write("MAC,SSID,AuthMode,FirstSeen,Channel,RSSI,CurrentLatitude,CurrentLongitude,AltitudeMeters,AccuracyMeters,Type\r\n")
home = gsmpos.FakePos()
home.lat = 50.14
home.lon = 14.50
pos = gsmpos.FakePos()

positions = {}
measurements = 0
combinations = 0
this_combination = 0

for line in logf.readlines():

    match = re.match('.gps .*lng="([-0-9.]*)" lat="([-0-9.]*)".*', line)
    if match:
        pos.lon = float(match.group(1))
        pos.lat = float(match.group(2))
        combinations += (this_combination * (this_combination - 1)) / 2
        this_combination = 0

    match = re.match('.wifiap bssid="([0-9a-z]*)".*', line)
    if match and util.distance(pos, home) < 400:
        ssid = match.group(1)
        print util.distance(pos, home), " km ,", pos.lon, ",", pos.lat, ",", ssid
        # for http://www.openwlanmap.org/upload.php?lang=en
        # raw SID;BSSID;ENCRYPTION;LATITUDE;LONGITUD
        rawf.write("%s,,,,,,%f,%f,,,WIFI\r\n" % (util.format_ssid(ssid), pos.lat, pos.lon))
        measurements += 1
        if ssid in positions:
            positions[ssid] += [copy.deepcopy(pos)]
        else:
            positions[ssid] = [copy.deepcopy(pos)]
        this_combination += 1

if 0:
    print measurements, " measurements "
    print len(positions), " accesspoints "
    distances = []
    num_zeros = 0
    for ap in positions:
        maxdist = -1
        for x in positions[ap]:
            for y in positions[ap]:
                if x != y:
                    dist = util.distance(x, y) * 1000.
                    if dist > maxdist: maxdist = dist
        if maxdist == -1:
            continue
        distances = distances + [ maxdist ]
        if maxdist == 0:
            num_zeros += 1

    print "access point area, meters "
    print
    gsmpos.distance_statistics(distances)
    distances.sort()
    print num_zeros, " access point of zero size "

if 1:
    print measurements, " measurements "
    print combinations, " combinations "
