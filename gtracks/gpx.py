# -*- coding: utf-8 -*-
# GPLv3, taken from gpxtools-0.2.1

"""
Parse GPX file.
"""
from lxml import etree

class GPXFile(object):
    """
    Simplifies operations on GPX files. 
    """
    _data = None

    def __init__(self, fileobj):
        """
        Parse GPX file to ElementTree instance.
        """
        self._data = etree.parse(fileobj)
        self._xmlns = self._data.getroot().nsmap[None]
        self._trkpt_path = '{%s}trk/{%s}trkseg/{%s}trkpt' % (self._xmlns, self._xmlns, self._xmlns)

    def remove_extensions(self):
        """
        Remove 'extensions' tags for GPX file.
        """
        def _remove_extensions(parent):
            ext = parent.find('{%s}extensions' % self._xmlns)
            if ext is not None:
                parent.remove(ext)
        
        metadata = self._data.find('{%s}metadata' % self._xmlns)
        if metadata is not None:
            _remove_extensions(metadata)
            
        for trkpt in self._data.findall(self._trkpt_path):
            _remove_extensions(trkpt)
    
    def remove_whitespaces(self):
        """
        Remove whitespaces between tags.
        """
        for i in self._data.getiterator():
            if i.text and i.text.isspace():
                i.text = None
            if i.tail and i.tail.isspace():
                i.tail = None

    def convert_to_egt(self):
        print(self._data)
        for trkpt in self._data.findall(self._trkpt_path):
            lat = float(trkpt.attrib['lat'])
            lon = float(trkpt.attrib['lon'])
            s = "%f %f" % (lat, lon)

            ele = trkpt.find('{%s}ele' % self._xmlns)
            if ele is not None:
                s += " ele=%s" % ele.text
            time = trkpt.find('{%s}time' % self._xmlns)
            if time is not None:
                s += " time=%s" % time.text
                
            print(s)

    def write(self, file, pretty_print=False):
        """
        Save GPX file.
        """
        return self._data.write(file, 
                                encoding=self._data.docinfo.encoding, 
                                xml_declaration=True, 
                                pretty_print=pretty_print)

class GPXWriter(GPXFile):
    def __init__(self, template):
        """
        Parse GPX file to ElementTree instance.
        """
        self._data = etree.ElementTree(etree.fromstring(template))
        self._xmlns = self._data.getroot().nsmap[None]
        self._trkpt_path = '{%s}trk/{%s}trkseg/{%s}trkpt' % (self._xmlns, self._xmlns, self._xmlns)

    def add_point(self, parse):
        for seg in self._data.findall('{%s}trk/{%s}trkseg' % (self._xmlns, self._xmlns)):
            pt = etree.Element('trkpt')
            pt.attrib['lat'] = parse['lat']
            pt.attrib['lon'] = parse['lon']
            seg.append(pt)

            #ele = etree.Element('ele')
            #ele.text = str("123.4")
            #pt.append(ele)
            break
