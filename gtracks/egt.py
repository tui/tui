#!/usr/bin/python
# See README.egt for file format description

def parse_string(l):
    res = {}
    sp = l.split(" ")
    if sp[0] != "" and sp[0][0].isdigit():
        res["lat"] = sp[0]
        sp = sp[1:]
    if sp[0] != "" and sp[0][0].isdigit():
        res["lon"] = sp[0]
        sp = sp[1:]
    for x in sp:
        kv = x.split("=")
        if len(kv) > 1:
            res[kv[0]] = kv[1]
    res["#"] = ""
    return res

def parse(l):
    l = l.rstrip("\n")
    if not l[0].isdigit():
        return None
    return parse_string(l)    
    try:
    	pass
    except:
        print("parse error on line with ", l)
        return None

def fmt(res):
    s = ""
    special = [ "#", "lat", "lon" ]
    s = "%s %s" % (res["lat"], res["lon"])
    for key in res:
        if key in special:
            continue
        s = s+" "+key+"="+str(res[key])
    return s

        
