#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Command line tools useful to manipulate GPX files.
"""
import sys
from os.path import basename
from optparse import OptionParser

from gpx import GPXFile, GPXWriter

def main():
    """
    Entry point for gpxtools.
    """
    parser = OptionParser(usage='%prog [options]')
    parser.add_option('-i', '--intput', dest='input', help='name of GPX input file, if not set stdin will be used', metavar='FILE')
    parser.add_option('-o', '--output', dest='output', help='name of GPX output file, if not set stdout will be used', metavar='FILE')
    
    (options,args) = parser.parse_args()

    input_f = sys.stdin
    if options.input:
        input_f = open(options.input, 'r')

    gpx_file = GPXFile(input_f)
    
    operations = {
        'gpx-cleanup': gpx_file.remove_extensions,
        'gpx-compress': gpx_file.remove_whitespaces,
        'egt': gpx_file.convert_to_egt,        
        }
    
    operation = 'egt'
    if operation not in operations:
        parser.error('unknown operation, should be one of: %s' % ', '.join(sorted(operations.keys())))

    print("Running operation")
    ## run choosen operation
    operations[operation]()

    output_f = sys.stdout
    if options.output:
        output_f = open(options.output, 'w')

    gpx_file.write(output_f)
    output_f.close()

#main()

template = b"""<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<?xml-stylesheet type="text/xsl" href="details.xsl"?>
<gpx
 version="1.0"
 creator="gpx-test"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns="http://www.topografix.com/GPX/1/0"
 xmlns:topografix="http://www.topografix.com/GPX/Private/TopoGrafix/0/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd http://www.topografix.com/GPX/Private/TopoGrafix/0/1 http://www.topografix.com/GPX/Private/TopoGrafix/0/1/topografix.xsd">
<trk>
<trkseg>
</trkseg>
</trk>
</gpx>
"""

gpx_file = GPXWriter(template)
gpx_file.add_point()
#gpx_file.convert_to_egt()
gpx_file.write(sys.stdout)
