#!/usr/bin/env python

from __future__ import print_function

# example notebook.py

import sys
sys.path += [ "../maemo" ]
import mygtk
mygtk.setup()
import android
import gtk
import gobject
import os
import time
import pango
import datetime
try:
    import pytz             # python-tz
    import dateutil.tz        # python-dateutil
except:
    print("Timezones not available")


class MusicAmd:
    def do(self, s): os.system("ssh media@amd '"+s+"'")
    def play(self):  self.do("mpc play")
    def pause(self): self.do("mpc pause")
    def nexts(self):  self.do("mpc next")
    def volume(self, s): self.do("mpc volume "+s)

class Music:
    def do(self, s): os.system(s)
    def play(self):  self.do("killall -CONT mpg123")
    def pause(self): self.do("killall -STOP mpg123")
    def nexts(self):  self.do("killall -INT mpg123")
    def volume(self, s): self.do("mpc volume "+s)

class MediaCtl(Music):
    def send(self, s): self.do("echo "+s+" > ~pavel/.mplayer/rc")
    def play(self):  self.send("pause")
    def volume(self, s): self.send("volume "+s)

global music
music = Music()
global mediactl
mediactl = MediaCtl()
global android
android = android.Android()

global big
big = 1

def big_label(label):
    if big:
        label.modify_font(pango.FontDescription("sans 48"))

def big_button(button):
    return
    if button.get_use_stock():
        label = button.child.get_children()[1]
    elif isinstance(button.child, gtk.Label):
        label = button.child
    else:
        raise ValueError("button does not have a label")
    big_label(label)

class Page:
    def __init__(self, notebook, handle, title):
        bufferf = title
        bufferl = handle

        self.notebook = notebook
        self.title = title

        try:
            frame = gtk.Frame(bufferf)
        except:
            frame = gtk.Frame()
        frame.set_border_width(10)
        if not big:
            frame.set_size_request(300, 200)
        else:
            frame.set_size_request(640, 350)
        frame.show()

        label = self.interior()
        frame.add(label)
        label.show()

        label = gtk.Label(bufferl)
        notebook.append_page(frame, label)

    def interior(self):
        label = gtk.Label(self.title)
        return label

class Main(Page):
    def interior(self):
        table = gtk.Table(3, 3, True)

        time_button = self.time_button = gtk.Button("(time)")
        big_button(time_button)
        table.attach(time_button, 0, 1, 0, 1)
        time_button.connect("clicked", lambda s: self.notebook.set_current_page(1))
        time_button.show()

        button = gtk.Button("Start")
        big_button(button)
        table.attach(button, 0, 1, 2, 3)
        button.connect("clicked", lambda s: self.notebook.set_current_page(5))
        button.show()

        button = gtk.Button("Film")
        big_button(button)
        table.attach(button, 1, 2, 1, 2)
        button.connect("clicked", lambda s: self.notebook.set_current_page(2))
        button.show()

        button = gtk.Button("Music")
        big_button(button)
        table.attach(button, 1, 2, 0, 1)
        button.connect("clicked", lambda s: self.notebook.set_current_page(3))
        button.show()

        table.show()

        self.tick()
        return table

    def tick(self):
        gobject.timeout_add(20000, lambda: self.tick())        
        dt = list(time.localtime())            
        t = "%d:%02d" % (dt[3], dt[4])
        return # FIXME
        self.time_button.child.set_text(t)

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6
    # Python 2.7
    # return td.total_seconds()

def to_unix(d):
    return total_seconds((d - datetime.datetime(1970, 1, 1, 0, 0, 0, 0, pytz.utc)))

class Alarm(Page):
    def update_unix(self):
        ah = self.time / 60
        am = self.time % 60
        msg = "Alarm at %d:%02d\n" % (ah, am)

        now = datetime.datetime.now(dateutil.tz.tzlocal())
        alarm = now.replace(hour = ah, minute = am, second = 0, microsecond = 0)

        if alarm < now:
            alarm += datetime.timedelta(days = 1)

        delta = to_unix(alarm) - to_unix(now)
        msg += "Alarm in %.1f hours\n" % (delta/(60.*60))
        msg += "Alarm in %.1f minutes\n" % (delta/(60.))
        msg += "Alarm really in %.1f minutes\n" % ((to_unix(alarm) - time.time())/(60.))

        if delta > 10:
            os.system("sudo rtcwake -m no -s %d" % (delta-5))

        self.alarm = to_unix(alarm)
        self.left_label.set_text(msg)

    def update(self, new, disable):
        self.time = new
        if not disable and self.disabled:
            self.disabled = disable
            self.tick()
        self.disabled = disable
        t = "a: %d:%02d" % (self.time / 60, self.time % 60)
        if disable: t = t + " (off)"
        self.label.set_text(t)
        self.update_unix()

    def update_herd(self):
        l = os.popen("ssh pavel@atrey.karlin.mff.cuni.cz 'echo Silent_until: %d | ~/herd/bin/for_all.py'" % self.alarm).readlines()
        self.left_label.set_text(' '.join(l))

    def tick(self):
        print("Tick, disabled = ", self.disabled)
        gobject.timeout_add(25000, lambda: self.tick())

        dt = list(time.localtime())
        t = "%d:%02d" % (dt[3], dt[4])
        self.time_label.set_text(t)

        if not self.disabled:
            now = to_unix(datetime.datetime.now(dateutil.tz.tzlocal()))
            if now > self.alarm: 
                print("\a\aAlarm")
                #music.play()
                os.system("amixer set Master unmute")
#                os.system("aplay alarm.wav &")
                #  -a plughw:CARD=Audio,DEV=0
                os.system("mpg123 alarm.mp3 /data/mp3.local/phone/Music-For-Relaxing-Waking-Up-Slowly.mp3 /data/mp3.local/mala_bila_vrana/tokaheya.mp3 /data/mp3/czech/mala_bila_vrana/tokaheya.mp3 &")
                #android.ttsSpeak("Alarm, wake up. This is alarm, wake up.")
                self.disabled = 1

    def alarm_off(self):
        self.update(self.time, 1)
        os.system("killall aplay mpg123")
        os.system("sudo rtcwake -m disable")
#        music.pause()

    def interior(self):
        self.disabled = 1

        vbox = gtk.VBox(False, 0)
        vbox.show()

        label = self.label = gtk.Label("(wait)")
        big_label(label)
        vbox.add(label)
        label.show()

        left_label = self.left_label = gtk.Label("(time left)")
        vbox.add(left_label)
        left_label.show()

        time_label = self.time_label = gtk.Label("(time)")
        big_label(time_label)
        vbox.add(time_label)
        time_label.show()

        self.update(9 * 60 + 45, 1)

        hbox = gtk.HBox(False, 0)
        hbox.show()
        vbox.add(hbox)

        button = gtk.Button("off")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: self.alarm_off())
        button.show()

        button = gtk.Button("--")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: self.update(self.time - 30, 0))
        button.show()
 
        button = gtk.Button("-")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: self.update(self.time - 5, 0))
        button.show()
 
        button = gtk.Button("+")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: self.update(self.time + 5, 0))
        button.show()

        button = gtk.Button("++")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: self.update(self.time + 30, 0))
        button.show()

        if False:
            button = gtk.Button("herd")
            big_button(button)
            hbox.add(button)
            button.connect("clicked", lambda s: self.update_herd())
            button.show()

        self.tick()
 
        return vbox

class Audio(Page):
    def volume_buttons(self, ctl):
        hbox = gtk.HBox(False, 0)
        hbox.show()
 
        button = gtk.Button("-")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: ctl.volume('-10'))
        button.show()

        button = gtk.Button("+")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: ctl.volume('+10'))
        button.show()

        return hbox

class Media(Audio):
    def interior(self):
        vbox = gtk.VBox(False, 0)
        vbox.show()

        hbox = gtk.HBox(False, 0)
        hbox.show()
        vbox.add(hbox)

        button = gtk.Button("<<")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: mediactl.send("seek -10"))
        button.show()

        button = gtk.Button('|| >')
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: mediactl.play())
        button.show()
 
        button = gtk.Button(">>")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: mediactl.send("seek +10"))
        button.show()

        button = gtk.Button(">|")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: mediactl.send("seek +300"))
        button.show()

        vbox.add(self.volume_buttons(mediactl))
 
        return vbox

class Music(Audio):
    def interior(self):
        vbox = gtk.VBox(False, 0)
        vbox.show()

        hbox = gtk.HBox(False, 0)
        hbox.show()
        vbox.add(hbox)

        button = gtk.Button('||')
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: music.pause())
        button.show()
 
        button = gtk.Button('>')
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: music.play())
        button.show()
 
        button = gtk.Button(">>")
        big_button(button)
        hbox.add(button)
        button.connect("clicked", lambda s: music.nexts())
        button.show()

        vbox.add(self.volume_buttons(music))
 
        return vbox

class Test(Page):
    def interior(self):
        chooser = gtk.FileChooserDialog()
        return chooser
class Herd(Page):
    def update(self):
        l = os.popen("ssh pavel@atrey.karlin.mff.cuni.cz '~/herd/bin/status.py'").readlines()
        self.status.set_text(' '.join(l))

    def interior(self):
        table = gtk.Table(3, 3, True)

        button = gtk.Button("Status")
        big_button(button)
        table.attach(button, 0, 1, 0, 1)
        button.connect("clicked", lambda s: self.update())
        button.show()

        status = self.status = gtk.Label("(status)")
        table.attach(status, 1, 4, 0, 3)
        status.show()

        button = gtk.Button("Night")
        big_button(button)
        table.attach(button, 0, 1, 3, 4)
#        button.connect("clicked", lambda s: pass)
        button.show()

        button = gtk.Button("Day")
        big_button(button)
        table.attach(button, 1, 2, 3, 4)
#        button.connect("clicked", lambda s: pass)
        button.show()

        table.show()
        return table


class Start(Page):
    def interior(self):
        table = gtk.Table(3, 3, True)

        button = gtk.Button("Herdd")
        big_button(button)
        table.attach(button, 0, 1, 0, 1)
        button.connect("clicked", lambda s: os.system("osso-xterm ~/herdd &"))
        button.show()

        button = gtk.Button("Rain")
        big_button(button)
        table.attach(button, 1, 2, 0, 1)
        button.connect("clicked", lambda s: os.system("osso-xterm 'debbie ~/g/tui/nowcast/nowcast -x' &"))
        button.show()

        button = gtk.Button("Debian")
        big_button(button)
        table.attach(button, 2, 3, 0, 1)
        button.connect("clicked", lambda s: os.system("osso-xterm debbie &"))
        button.show()

        button = gtk.Button("Loc")
        big_button(button)
        table.attach(button, 0, 1, 1, 2)
        button.connect("clicked", lambda s: os.system("osso-xterm ~/loc &"))
        button.show()


        table.show()
        return table

class MainNotebook:

    def delete(self, widget, event=None):
        gtk.main_quit()
        return False

    def __init__(self):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("delete_event", self.delete)
        window.set_border_width(10)
        #window.maximize()

        table = gtk.Table(3,6,False)
        window.add(table)

        # Create a new notebook, place the position of the tabs
        notebook = gtk.Notebook()
        notebook.set_tab_pos(gtk.POS_LEFT)
        table.attach(notebook, 0,6,0,1)
        notebook.show()
        self.show_tabs = True
        self.show_border = True

        page = Main(notebook, "main", "Main menu")
        try:
            pytz.utc
            page = Alarm(notebook, "alarm", "Alarm")
        except:
            page = Page(notebook, "alarm", "Alarm not available")
        page = Media(notebook, "film", "Film control")          # 2
        page = Music(notebook, "music", "Music control")        # 3
        page = Herd(notebook, "herd", "Herd")                   # 4
        page = Start(notebook, "start", "Start")                # 5

#        page = Test(notebook, "test", "Test file chooser")
#        page = Page(notebook, "page", "Longer page title")
      
        # Set what page to start at (page 4)
        notebook.set_current_page(3)

        # Create a bunch of buttons
        button = gtk.Button("close")
        button.connect("clicked", self.delete)
        table.attach(button, 0,1,1,2)
        button.show()

        button = gtk.Button("main")
        button.connect("clicked", lambda w: notebook.set_current_page(0))
        table.attach(button, 3,4,1,2)
        button.show()

        button = gtk.Button("next page")
        button.connect("clicked", lambda w: notebook.next_page())
        table.attach(button, 1,2,1,2)
        button.show()

        button = gtk.Button("prev page")
        button.connect("clicked", lambda w: notebook.prev_page())
        table.attach(button, 2,3,1,2)
        button.show()

        notebook.set_current_page(0)

        table.show()
        window.show()

def main():
    gtk.main()
    return 0

if __name__ == "__main__":
    MainNotebook()
    main()
