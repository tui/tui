public int main (string[] args) {
       var f = new Factory();
       f.init();
       {
              var loop = new MainLoop();
       	      var time = new TimeoutSource(200000);

	      time.set_callback(() => {
	      			   stdout.printf("Time!\n");
				   loop.quit();
				   return false;
		});

	       time.attach(loop.get_context());

	       loop.run();
       }
       return 0;
}