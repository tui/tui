
public class Watchdog {
	string mydir;
	string key;
	int timeout;
       public FileMonitor? monitor;
    
    public Watchdog(string s) {
	    mydir = GLib.Environment.get_home_dir() + "/herd/wd/";
	    key = s;
	    timeout = 45;
    }

    public DataInputStream? read() {
	    var f = File.new_for_path(get_path());
	    if (!f.query_exists())
		    return null;

 	    Posix.Stat stat;
	    if (Posix.lstat(get_path(), out stat) < 0)
	            return null;
	    var now = GLib.get_real_time() / 1000000;
	    var mtime = stat.st_mtime;

	    if (now - mtime > timeout)
	            return null;

	    if (now < mtime - 5)
	            return null;

	    var dis = new DataInputStream(f.read());	    
	    return dis;
    }

    public string get_path() {
	    return mydir+key;
    }

    public void start_monitor() {
	    var f = File.new_for_path(get_path());
	    monitor = f.monitor (FileMonitorFlags.NONE, null);
    }
}
