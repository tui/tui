class MyFactory : Factory {
        public class Maildir maildir;

	public override void my_init() {
	       stdout.printf("Initializing: my init.........................\n");
	       maildir = new Maildir();
	       maildir.prepare();
	}

	public override void incoming_message(string from, string text, string stime, string lstime) {
	     maildir.write(from, text);
	}
	
	public override void new_call(string from, string state) {
	     stdout.printf("smsd: new call\n");
	     maildir.write(from, state);
	}
}

public int main (string[] args) {
       var f = new MyFactory();
       f.init();
       {
              var loop = new MainLoop();
	      loop.run();
       }
       return 0;
}