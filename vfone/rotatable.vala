
public class TGrid : Gtk.Grid {
	public TGrid.hom() {
		this();
		column_homogeneous = true;
		row_homogeneous = true;
	}
}

public class TLabel : Gtk.Label {
	public TLabel.big(string s) {
	        this();
		set_text("<span size=\"xx-large\">"+s+"</span>");
	        use_markup = true;
        }
}

public class TButton : Gtk.Button {
       public Gtk.Label widget_label;
       
	public TButton.big(string s) {
		this();
		var l = new TLabel.big(s);
		add(l);
		widget_label = l;
	}
}

public class Rotatable {
       public Gtk.Window window;

       public Rotatable() {
       	      stdout.printf("Rotatable constructor\n");
       }

       public virtual Gtk.Widget interior() {
	       var t = new TGrid.hom();
	       t.attach(aux_interior(), 0,1,1,1);
	       t.attach(main_interior(), 1,1,1,1);
	       return t;
       }

       public virtual Gtk.Widget aux_interior() {
	       return new Gtk.Grid();
       }

       public virtual Gtk.Widget main_interior() {
	       return new Gtk.Grid();
       }
       
       public void basic_main_window(Gtk.Application app) {
	       window = new Gtk.ApplicationWindow(app);

	       var w = interior();
	       window.add(w);
	       window.set_title("");
	       window.set_default_size(800, 400);
	       window.destroy.connect(Gtk.main_quit);
	       window.show_all();
       }

       public void basic_window() {
	       window = new Gtk.Window();

	       var w = interior();
	       window.add(w);
	       window.set_title("");
	       window.set_default_size(800, 400);
	       window.destroy.connect(Gtk.main_quit);
	       window.show_all();
       }

}
