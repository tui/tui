using Posix;

class Maildir {
      string dir;
      public void prepare() {
      	     dir = "./smsdir";
      	     mkdir(dir, 0700);
	     mkdir(dir+"/cur", 0700);
	     mkdir(dir+"/tmp", 0700);	     
	     mkdir(dir+"/new", 0700);
       }

      public void write(string from, string message) {
      	     var name = dir+"/tmp/XXXXXX";
      	     var file = mkstemp(name);
	     string basename = name[-6:name.length];

	     var s = "From: "+from+"@sms\n\n"+message;
	     Posix.write(file, s, s.length);
	     Posix.close(file);
	     
	     /* vala does not contain Posix.rename? */
	     Posix.link(name, dir+"/new/"+basename);
	     Posix.unlink(name);
      }
}
