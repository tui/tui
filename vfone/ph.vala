class MonitorFactory : Factory {
	public override void my_init() {
	}

	public override void incoming_message(string from, string text, string stime, string lstime) {
	}
	
	public override void new_call(string from, string state) {
	       print("new_call, state %s\n", state);	       
	}

	public override void signal_updated(int signal, string network) {
 	       stdout.printf("override %d%% %s\n", signal, network);
	}
}

public void monitor()
{
       var f = new MonitorFactory();
       f.init();

       var loop = new MainLoop();
       loop.run();
}

public void help()
{
	print("Usage: ph command\n");
	print("\n");
	print("monitor|mon	monitor modem status\n");
	print("call|c x         start call with given number\n");
	print("sms|s x y        send message to given number\n");
	print("hangup|h         hangup all calls\n");
}

public int main(string[] args)
{
	if (args[1] == "help") {
	       help();
	       return 0;
	}

       if ((args[1] == "monitor") || (args[1] == "mon")) {
	      monitor();
	      return 0;
       }

       if ((args[1] == "call") || (args[1] == "c")) {
       	      var f = new Factory();
	      f.init();
	      f.manager.modem.voice_manager.proxy.Dial(args[2], "");
       	      print("Should call to %s\n", args[2]);
	      return 0;
       }

      if ((args[1] == "hangup") || (args[1] == "h")) {
       	      var f = new Factory();
	      f.init();
	      f.manager.modem.voice_manager.proxy.HangupAll();
	      return 0;
       }
       if ((args[1] == "sms") || (args[1] == "s")) {
       	      var f = new Factory();
	      f.init();
	      f.manager.modem.message_manager.proxy.SendMessage(args[2], args[3]);
       	      print("Sent  sms to %s\n", args[2]);
	      return 0;
       }

       help();
       return 1;
}