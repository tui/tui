
public class AlertWindow : HalfLockWindow {
       public Software sw;
       bool do_sound; 

       public void start_alerts() {
       	        do_sound = false;
		if (do_sound) {
		        sw.start_vibrations();		
			sw.play_mp3("../ofone/audio/ringtone.mp3");
		}
		sw.screen_on();
       }

       public virtual void stop_alerts() {
       	      if (do_sound) {
 	              sw.stop_vibrations();
		      sw.stop_mp3();
 	      }
	      do_sound = false;
       }

       public override void do_close() {
       	      stop_alerts();
	      base.do_close();
       }
}

public class AlarmWindow : AlertWindow {
       public override Gtk.Widget aux_interior_2() {
	      var l = new TLabel.big("It is time. Wake up!");

	      sw = new Software();
	      return l;
       }
}

public class AlarmApplication : Gtk.Application {
        public AlarmWindow window;
	protected override void activate () {
		window = new AlarmWindow();
		window.basic_main_window(this);
		window.start_alerts();
		//window.window.fullscreen();
 		//window.window.unfullscreen();
	}
}

public class CallWindow : AlertWindow {
       public MyVoiceCall voicecall;

       public override void main_interior_2(Gtk.Grid t) {
       	      var b = new TButton.big("Answ");
	      b.clicked.connect((t) => voicecall.proxy.Answer());
	      t.attach(b, 2,0,1,1);

       	      b = new TButton.big("Hang");
	      b.clicked.connect((t) => voicecall.proxy.Hangup());
	      t.attach(b, 3,0,1,1);
       
       	      b = new TButton.big("Quiet");
	      b.clicked.connect((t) => stop_alerts());
	      t.attach(b, 4,0,1,1);
       }
}

public class MyVoiceCall : VoiceCall {
       public CallWindow window;
       
       public override void new_call(string from, string state) {
	        window = new CallWindow();
		window.message = "Call from "+from;
		window.voicecall = this;
		window.basic_window();
		window.start_alerts();
       }

       public override void done_managee() {
       	        window.do_close();
		window = null;
       }
}

public class MyFactory : Factory {
	public AlertWindow window;

	public override void my_init() {
	       stdout.printf("Initializing: my init.........................\n");
	}

	public override VoiceCall new_VoiceCall() { return new MyVoiceCall(); }

	public override void incoming_message(string from, string text, string stime, string lstime) {
	        window = new AlertWindow();
		window.message = "From "+from+"\n"+text;
		window.basic_window();
		window.start_alerts();
	}
}

public class MonitorApplication : Gtk.Application {
        public MyFactory factory;
	public AlarmWindow window;

	protected override void activate () {
		  factory = new MyFactory();
		  factory.init();
		window = new AlarmWindow();
		window.basic_main_window(this);
		window.window.fullscreen();
	}
}


public int main(string[] args) {
       for (int i = 0; i < args.length; i++) {
       	     if (args[i] == "alarm")
	            return new AlarmApplication().run(null);
	     if (args[i] == "monitor") {
	     	    return new MonitorApplication().run(null);
		}
       }
        return 1;
}
