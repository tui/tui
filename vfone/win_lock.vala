
public class LockWindow : HalfLockWindow {
       public override Gtk.Widget aux_interior_2() {
	      var b = new TLabel.big("Screen locked\nslide the bar to unlock.");
	      return b;
       }
}

public class StartupWindow : HalfLockWindow {
       Gtk.ProgressBar progress;
       Gtk.Label details;
       public Watchdog wd;
       
       public bool task() {
       	      stdout.printf("foo\n");
	      progress.fraction += 0.05;
	      return true;
       }
       
       public bool file_mon() {
       	      stdout.printf("file_mon\n");
	      progress.fraction += 0.05;
	      return true;
       }


       public void file_changed() {
       	      stdout.printf("File changed\n");

	      var dis = wd.read();
	      var l = dis.read_line(null);
	      stdout.printf("changed: %s\n", l);
	      var i = l.to_int();
	      progress.fraction = i/100.0;
	      l = dis.read_line(null);
	      stdout.printf("changed: %s\n", l);	      
	      details.set_text(l);
       }
       
       public override Gtk.Widget aux_interior_2() {
	      var t = new TGrid.hom();
	      {
	      	      var b = new TLabel.big("System starting up");
	      	      t.attach(b, 0,0,1,1);
	      }
	      progress = new Gtk.ProgressBar();
	      t.attach(progress, 0,1,1,1);

	      details = new TLabel.big("???");
	      t.attach(details, 0,2,1,1);
	      
	      Timeout.add_seconds(100, task);
	      wd = new Watchdog("startup");
	      wd.start_monitor();
	      
 	      wd.monitor.changed.connect ((src, dest, event) => {
	      	    if (event == FileMonitorEvent.CHANGES_DONE_HINT) {
		       stdout.printf("changes done\n");
 		       file_changed();
		    }
 	            if (dest != null) {
 		       stdout.printf ("%s: %s, %s\n", event.to_string (), src.get_path (), dest.get_path ());
 		       } else {
 		       stdout.printf ("%s: %s\n", event.to_string (), src.get_path ());
 		       }
	       });
 
	      return t;
       }
}

public class TestWindow : HalfLockWindow {
       Gtk.ProgressBar progress;

       public override Gtk.Widget aux_interior_2() {
	      var t = new TGrid.hom();
	      {
	      	      var b = new TLabel.big("Trap");
	      	      t.attach(b, 0,0,1,1);
	      }
 	      {
	      	      var b = new Gtk.ProgressBar();
	      	      b.fraction = 0.3;
		      t.attach(b, 0,1,6,1);
		      progress = b;
	      }
	      for (var i = 2; i < 5; i++) {
	      	      var b = new Gtk.Label("");
		      t.attach(b, i,i,1,1);
	      }
	      {
	      	      var b = new TLabel.big("Here");
		      t.attach(b, 5,6,1,1);
	      }

	      return t;
       }
}

public class MyApplication : Gtk.Application {
        HalfLockWindow window;
	public string[] args;
	bool debug;

       public void new_window() {
       	      for (int i = 0; i < args.length; i++) {
 	      	       if (args[i] == "--debug")
		      	     debug = true;
		       if (args[i] == "startup") {
				window = new StartupWindow();
				return;
		       }
		       if (args[i] == "test") {
				window = new TestWindow();
				return;
		       }
		       stdout.printf("Have unknown argument, %s\n", args[i]);
		       
		}
		window = new LockWindow();
       }

	protected override void activate() {
		new_window();

		window.basic_main_window(this);
		if (!debug)
		        window.window.fullscreen();
	}
}

public int main (string[] args) {
	var a = new MyApplication();
	a.args = args;
	return a.run();
}
