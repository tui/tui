public abstract class HalfLockWindow : Rotatable {
       public TButton close;
       public TButton unfull;       
       public Gtk.Scale scale;
       public bool lock_harder;
       List<Gtk.Widget> sensitive;
       public string message;

       public HalfLockWindow() {
       	      stdout.printf("LockWindow constructor\n");
	      lock_harder = false;
       }

       public void setup_widgets() {
       	      close = new TButton.big("Close");
 	      close.clicked.connect((t) => button_close());
	      sensitive.append(close);

       	      unfull = new TButton.big("min");
 	      unfull.clicked.connect((t) => button_unfull());
	      sensitive.append(unfull);

 	      scale = new Gtk.Scale.with_range(Gtk.Orientation.HORIZONTAL, 0, 100, 1);
 	      scale.value_changed.connect((_) => scale_moved());
       }

       public void set_sensitive(bool on) {
       	      foreach(var w in sensitive) {
	       	  w.sensitive = on;
	      }
       }

       public virtual void button_unfull() {
       	      window.unfullscreen();
       }

       public virtual void button_lock() {
       	      set_sensitive(false);
		stdout.printf("lock button pressed\n");
 		lock_harder = true;
		scale.set_value(0);
       }

       public virtual void do_close() {
       	      window.destroy();
       }

       public void button_close() {
       	      do_close();
       }

       public virtual void do_unlock() {
       	      scale.set_value(0);
       	      set_sensitive(true);
       	      //do_close();
       }

       public void scale_moved() {
       	      double v;
	      v = scale.get_value();
       	      stdout.printf("Scale moved: %f\n", v);
	      if (v == 100)
	      	       do_unlock();
       }

       public virtual void main_interior_2(Gtk.Grid t) {
       	      var b = new TButton.big("Lock");
	      b.clicked.connect((t) => button_lock());
	      t.attach(b, 2,0,3,1);
       }

       public override Gtk.Widget main_interior() {
	      var t = new TGrid.hom();

       	      setup_widgets();
 	      t.attach(close, 0,0,1,1);
	      t.attach(unfull, 1,0,1,1);
	      main_interior_2(t);
	      {
		      var b = new TButton.big("Lock");
		      b.clicked.connect((t) => button_lock());
		      t.attach(b, 0,1,5,5);
	      }
	      t.attach(scale, 0,6,5,1);	      
	      return t;
       }

       public virtual Gtk.Widget aux_interior_2() {
	      var l = new TLabel.big(message);
	      l.set_line_wrap(true);
	      return l;
       }

       public override Gtk.Widget aux_interior() {
	      var t = aux_interior_2();
	      var r = new Gtk.Button();
	      r.add(t);
	      r.clicked.connect((t) => button_lock());
	      return r;
      }
}
