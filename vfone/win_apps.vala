
public class AppsWindow : Rotatable {
       public override Gtk.Widget aux_interior() {
	       return new Gtk.Label("aux");
       }

       public override Gtk.Widget main_interior() {
	      var t = new TGrid.hom();
	      for (var x=0; x<4; x++)
		      for (var y=0; y<4; y++) {
			      var b = new TButton.big("bu_t");
			      b.clicked.connect((_) => stdout.printf("button pressed\n"));
			      t.attach(b, x,y,1,1);
		      }
	      return t;
       }
}

public class MyApplication : Gtk.Application {
        public AppsWindow window;
	protected override void activate () {
		window = new AppsWindow();
		window.basic_main_window(this);
	}
}

public int main (string[] args) {
	return new MyApplication ().run (args);
}
