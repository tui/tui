class MyMessageManager : MessageManager {
 	public override void incoming_message (string message, HashTable<string, Variant> info) {
	       stdout.printf("overriden Incoming message: %s\n", message);
	       factory.print_prop(info);
	       (factory as MyFactory).maildir.write(info["Sender"].get_string(), message);
	}
}

class MyFactory : Factory {
        public class Maildir maildir;

	public override MessageManager new_MessageManager() { return new MyMessageManager(); }
        public override VoiceCall new_VoiceCall() { return new VoiceCall(); }

	public override void my_init() {
	       stdout.printf("Initializing: my init.........................\n");
	       maildir = new Maildir();
	       maildir.prepare();
	}

}

public int main (string[] args) {
       var f = new MyFactory();
       f.init();
       {
              var loop = new MainLoop();
	      loop.run();
       }
       return 0;
}