public class Software {
       public void screen_off() {
       	      Posix.system("xscreensaver-command -deactivate");
       }

       public void screen_on() {
       	      Posix.system("xscreensaver-command -deactivate");
       }

       public void play_mp3(string name) {
       	      Posix.system("mpg123 "+name+" &");
       }

       public void stop_mp3() {
       	      Posix.system("killall mpg123");
	}

       public void start_vibrations() {
       	      Posix.system("../cfone/vibrations &");
	}

       public void stop_vibrations() {
       	      Posix.system("killall vibrations");
	}
}

public class Hardware {
	public void vibrate(double seconds) {
	       Posix.system("(echo 5; sleep "+seconds.to_string()+"; echo -1) | sudo fftest /dev/input/event2 &");
	}
}

