#!/usr/bin/python3

# https://github.com/TheImagingSource/tiscamera/blob/master/examples/python/07-appsink.py

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib
import os
import time

def gst_convert(mega_dir, out_file, use_jpeg):
    def sa_read(name, t):
        with open(name, "rb") as file:
            rgb_data = file.read(10*1024*1024)

        caps_string = "audio/x-raw,format=U16LE,channels=2,rate=48000,layout=interleaved,channel-mask=3"
        caps = Gst.Caps.from_string(caps_string)

        buffer = Gst.Buffer.new_wrapped(rgb_data)
        if False:
            time.sleep(1/30.)
            # nanoseconds
            buffer.pts = time.time() * 1000*1000*1000
            buffer.dts = time.time() * 1000*1000*1000
        elif True:
            buffer.pts = t
            buffer.dts = t
        buffer.duration = (1000*1000*1000)/10.

        return buffer, caps

    def sa_src(appsrc):
        def on_need_data(appsrc, data, length):
            name = audio.get_path()
            if name == None or name[-22:] != ".48000-s16le-stereo.sa":
                appsrc.emit("end-of-stream")
                print("End of audio stream")
                return
            t = audio.get_time()
            #print("Audio: ", name, " need ", data, t)
            audio.pop()
            buffer, caps = sa_read(name, t)
            appsrc.set_property("caps", caps)
            appsrc.emit("push-buffer", buffer)

        appsrc.set_property("format", Gst.Format.TIME)
        appsrc.set_property("is-live", False)
        appsrc.set_property("block", True)

        name = audio.get_path()
        buffer, caps = sa_read(name, 0)

        appsrc.set_property("caps", caps)
        #appsrc.emit("push-buffer", buffer)

        s = appsrc.connect("need-data", on_need_data, "")
        print("Connect", s)

    class grwBase:
        def init(m, dir):
            m.dir = dir
            m.list = os.listdir(dir)
            m.list.sort()
            m.slen = len(m.suffix)
            m.start_time = 0
            print("Movie", len(m.list))

        def get_path(m):
            s = m.get_name()
            if s: return m.dir + s
            return s

        def get_name(m):
            #print("Get path -- ")
            while True:
                if (len(m.list)) == 0:
                    return None
                #print("Get path: ", m.list[0], m.suffix)
                if m.list[0][-m.slen:] != m.suffix:
                    m.pop()
                    continue
                return m.list[0]

        def get_time(m):
            s = m.get_name()
            s = s[:-m.slen]
            return int(s) * 1000 - m.start_time

        def pop(m):
            m.list = m.list[1:]

    class grwVideo(grwBase):
        suffix = ".grw"
        def __init__(m, dir):
            m.init(dir)

    class grwJPEG(grwBase):
        suffix = ".jpeg.sv"
        def __init__(m, dir):
            m.init(dir + "sm/")

    class grwAudio(grwVideo):
        suffix = ".48000-s16le-stereo.sa"
        def __init__(m, dir):
            m.init(dir + "sm/")

    def grw_read(name, t):
        with open(name, "rb") as file:
            rgb_data = file.read(10*1024*1024)
        i = len(rgb_data)
        i -= 1
        while rgb_data[i] != 0:
            i -= 1
        footer = rgb_data[i+1:]
        sp = str(footer, 'ascii').split('\n')
        # Create caps for the file
        caps_string = sp[0][6:]
        caps = Gst.Caps.from_string(caps_string)
        if sp[0][:6] != "Caps: ":
            print("Bad footer")
        if sp[1][:6] != "Size: ":
            print("Bad footer")
        if sp[-1] != "GRW":
            print("Missing GRW footer")

        buffer = Gst.Buffer.new_wrapped(rgb_data)
        # This does not work for interactive use.
        if False:
            time.sleep(1/30.)
            # nanoseconds
            buffer.pts = time.time() * 1000*1000*1000
            buffer.dts = time.time() * 1000*1000*1000
        elif True:
            buffer.pts = t
            buffer.dts = t
        buffer.duration = (1000*1000*1000)/30.

        return buffer, caps

    def grwsrc(appsrc):
        def on_need_data(appsrc, data, length):
            name = movie.get_path()
            if name == None or name[-4:] != ".grw":
                appsrc.emit("end-of-stream")
                print("End of video stream")
                return
            t = movie.get_time()
            #print("Video: ", name, t)
            movie.pop()
            buffer, caps = grw_read(name, t)
            appsrc.set_property("caps", caps)
            appsrc.emit("push-buffer", buffer)

        appsrc.set_property("format", Gst.Format.TIME)
        appsrc.set_property("is-live", False)
        appsrc.set_property("block", True)

        name = movie.get_path()
        buffer, caps = grw_read(name, 0)

        appsrc.set_property("caps", caps)
        #appsrc.emit("push-buffer", buffer)

        s = appsrc.connect("need-data", on_need_data, "")
        print("Connect", s)

    def jpeg_read(name, t):
        with open(name, "rb") as file:
            rgb_data = file.read(10*1024*1024)
        i = len(rgb_data)
        buffer = Gst.Buffer.new_wrapped(rgb_data)

        caps_string = "image/jpeg"
        caps = Gst.Caps.from_string(caps_string)

        # This does not work for interactive use.
        if False:
            time.sleep(1/30.)
            # nanoseconds
            buffer.pts = time.time() * 1000*1000*1000
            buffer.dts = time.time() * 1000*1000*1000
        elif True:
            buffer.pts = t
            buffer.dts = t
        buffer.duration = (1000*1000*1000)/30.

        return buffer, caps

    def jpeg_src(appsrc):
        def on_need_data(appsrc, data, length):
            name = movie.get_path()
            if name == None or name[-8:] != ".jpeg.sv":
                appsrc.emit("end-of-stream")
                print("End of video stream")
                return
            t = movie.get_time()
            #print("Video: ", name, t)
            movie.pop()
            buffer, caps = jpeg_read(name, t)
            appsrc.set_property("caps", caps)
            appsrc.emit("push-buffer", buffer)

        appsrc.set_property("format", Gst.Format.TIME)
        appsrc.set_property("is-live", False)
        appsrc.set_property("block", True)

        name = movie.get_path()
        buffer, caps = jpeg_read(name, 0)

        appsrc.set_property("caps", caps)
        #appsrc.emit("push-buffer", buffer)

        s = appsrc.connect("need-data", on_need_data, "")
        print("Connect", s)

    def v_src(appsrc):
        if not use_jpeg:
            grwsrc(appsrc)
        else:
            jpeg_src(appsrc)

    count = 0
    path = mega_dir
    if use_jpeg:
        movie = grwJPEG(path)
    else:
        movie = grwVideo(path)
    audio = grwAudio(path)
    t1 = movie.get_time()
    t2 = audio.get_time()
    tm = min(t1,t2)
    print("Time base is", tm)
    movie.start_time = tm
    audio.start_time = tm

    def pipeline_video():
        if True:
            s = "appsrc name=source"
            if use_jpeg:
                s += " ! jpegdec "
        else:
            s = "videotestsrc"
            s += " ! video/x-raw,width=(int)640,height=(int)480,format=(string)RGB "
        if False:
            s += " ! videoconvert ! jpegenc"
            s += " ! appsink name=sink"
        elif True:
            s += " ! videoconvert ! autovideosink"
        else:
            s += " ! videoconvert ! x264enc bitrate=3072 speed-preset=ultrafast ! matroskamux ! filesink location=" + out_file

        pipeline = Gst.parse_launch(s)

        p = pipeline.get_by_name("source")
        if p:
            if False:
                mysrc(p)
            else:
                v_src(p)
        p = pipeline.get_by_name("sink")
        if p:
            mysink(p)
        return pipeline

    def pipeline_audio():
        # audiotestsrc ! audioconvert ! audioresample ! autoaudiosink
        if True:
            s = "appsrc name=source"    
        else:
            s = "audiotestsrc"

        if True:
            s += " ! audiobuffersplit ! audioconvert ! audioresample ! autoaudiosink"
        else:
            s += " ! ! ! "

        pipeline = Gst.parse_launch(s)

        p = pipeline.get_by_name("source")
        if p:
            sa_src(p)
        p = pipeline.get_by_name("sink")
        if p:
            mysink(p)
        return pipeline

    def pipeline_both():
        if True:
            s = "appsrc name=asrc"
        else:
            s = "audiotestsrc"
        # Audiobuffersplit creates problems with A/V synchronization, avoid.
        #s += "! audiobuffersplit"
        s += " ! audioconvert ! vorbisenc ! mux. "

        if True:
            s += "appsrc name=vsrc"
            if use_jpeg:
                s += " ! jpegdec "
        else:
            s += "videotestsrc"
            s += " ! video/x-raw,width=(int)640,height=(int)480,format=(string)RGB "

        s += " ! videoconvert ! x264enc bitrate=3072 speed-preset=ultrafast ! matroskamux name=mux"
        if False:
            s += " ! decodebin ! playsink"
        else:
            s += " ! filesink location=/tmp/delme.mkv sync=0"


        pipeline = Gst.parse_launch(s)

        p = pipeline.get_by_name("asrc")
        if p:
            sa_src(p)
        p = pipeline.get_by_name("vsrc")
        if p:
            v_src(p)
        return pipeline

    Gst.init(None)
    Gst.debug_set_default_threshold(Gst.DebugLevel.WARNING)
    if False:
        Gst.debug_set_default_threshold(Gst.DebugLevel.INFO)

    if False:
        pipeline = pipeline_video()
    elif False:
        pipeline = pipeline_audio()
    else:
        pipeline = pipeline_both()

    # Function to handle end of stream
    def on_eos(bus, message):
        print("End of stream")
        pipeline.set_state(Gst.State.NULL)
        loop.quit()

    # Set up bus to handle messages
    bus = pipeline.get_bus()
    bus.add_signal_watch()
    bus.connect("message::eos", on_eos)

    # Set the pipeline to the playing state
    pipeline.set_state(Gst.State.PLAYING)

    # Run the main loop to handle GStreamer events
    loop = GLib.MainLoop()
    try:
        loop.run()
    except KeyboardInterrupt:
        pipeline.set_state(Gst.State.NULL)
        loop.quit()

gst_convert("/data/tmp/megapixels.ejP5ZN/", "/tmp/delme.mkv", True)
