#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*
 * gcc -march=native -O3 matmult.c -o matmult && time ./matmult
 * Wow. on x60, gcc automatically uses psubusb %xmm4,%xmm0, paddb  %xmm3,%xmm1,pcmpeqb %xmm2,%xmm0 ... it speeds up 12s->3s.
 *
 * x86 has 256-bit vectors; 512-bit is optional and eats extra power
 * Arm has Neon.
 */

const int stride = 1920;
#define HEIGHT 1080
#define WIDTH 1920
#define VS 3
#define PIX 256

typedef float pixel_t;

#define barrier() asm volatile("": : : "memory")
static pixel_t src[WIDTH*HEIGHT*VS], dst[WIDTH*HEIGHT*VS];

// https://gist.github.com/nadavrot/5b35d44e8ba3dd718e595e40184d03f0

static pixel_t corrmat[VS][VS] = { { 10, 1, 2 }, { 3, 10, 1 }, { 4, 2, 10 } };

// gcc -march=native -mfma -funroll-loops -O3 matmult.c -o matmult && time ./matmult
//  g++ -mavx2 -mfma -march=native -funroll-loops -O3 -S mmul.cpp

// Ok, so fma helps with the native code, too. 0.952 -> 0.801 sec on AMD EPYC 7351.

void exp_matmult(const pixel_t* left, const pixel_t* right, pixel_t* result)
{
	const int n = VS;
  
	int in = 0;
	for (int i = 0; i < n; ++i) {
		int kn = 0;
		for (int k = 0; k < n; ++k) {
			pixel_t aik = left[in + k];
			/* fixme: set result to zero! */
			/* -mfma -- generates VFMADD231SS instruction, not on x220 */
			for (int j = 0; j < n; ++j) {
				result[in + j] += aik * right[kn + j];
			}
			kn += n;
		}
		in += n;
	}
}

void exp_line(pixel_t *dst, const pixel_t *src)
{
        for (int x = 0; x < (int)WIDTH*VS; x+=VS)  {
		exp_matmult(src+x, corrmat, dst+x);
        }
}

void experimental(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		exp_line(dst + y*stride, src + y*stride);
	}
}

/* -------------------------------------------------------------------- */

void naive_matmult(pixel_t a[][VS], pixel_t b[][VS], pixel_t mul[][VS])
{
	int i,j,k;

	for(i=0;i<VS;i++) {
		for(j=0;j<VS;j++) {
			mul[i][j] = 0;
			for(k=0;k<VS;k++) {
				mul[i][j] = mul[i][j] + a[i][k]*b[k][j];
			}
		}
	}
}

void naive_line(pixel_t *dst, const pixel_t *src) {
        for (int x = 0; x < (int)WIDTH*VS; x+=VS)  {
		naive_matmult(src+x, corrmat, dst+x);
        }
}

void naive(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		naive_line(dst + y*stride, src + y*stride);
	}
}

/* Vectors
 * https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html
 */

#define V 8
typedef unsigned char vu8 __attribute__ ((vector_size (V)));
typedef unsigned short vu16 __attribute__ ((vector_size (2*V)));

#define shuf __builtin_shuffle
#define widen(pr)  __builtin_convertvector(pr, vu16)
#define narow(pr)  __builtin_convertvector(pr, vu8)

void line_vector(pixel_t *dst, const pixel_t *src)
{
	vu8 *cr = (vu8 *)src;
	vu8 *ds = (vu8 *)dst;

        for (int x = 0; x < (int)WIDTH; x+=V) {
		vu8 cu = *cr++;
		vu8 out = cu;

		*ds++ = out;
        }
}

void vector(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		line_vector(dst + y*stride *3, src + y*stride);
	}
}

void main(void)
{
	int i;
	for (i=0; i<WIDTH*HEIGHT*VS; i++) {
		src[i] = random();
	}
	barrier();
	int mode = 1;
	int iter = 160;
	switch (mode) {
	case 0:
		printf("Just memcpy\n");		
		for (i=0; i<iter; i++) {
			memcpy(dst, src, WIDTH*HEIGHT*VS);
			barrier();
		}
		break;
	case 1:
		printf("Naive -- simple multiply\n");
		for (i=0; i<iter; i++) {
			naive();
			barrier();
		}
		break;
	case 2:
		printf("Experimental -- ???\n");
		for (i=0; i<iter; i++) {
			experimental();
			barrier();
		}
		break;
	case 3:
		printf("Vector debayer\n");
		for (i=0; i<iter; i++) {
			vector();
			barrier();
		}
		break;
	default:
		printf("This is not good\n");
	}
	
	barrier();
	{
		unsigned long sum;
		for (i=0; i<WIDTH*HEIGHT; i++) {
			sum += dst[i];
		}
		printf("Sum: %lx\n", sum);
	}
}
