#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*
 * gcc -march=native -O3 debayer.c -S debayer.s
 *
 * x86 has 256-bit vectors; 512-bit is optional and eats extra power
 * Arm has Neon.
 */

const int stride = 1920;
#define HEIGHT 1080
#define WIDTH 1920
#define PIX 256

typedef uint8_t pixel_t;
pixel_t red_[PIX], green_[PIX], blue_[PIX];

#define barrier() asm volatile("": : : "memory")
static char src[WIDTH*HEIGHT], dst[WIDTH*HEIGHT*3];

#define B(color, val) color##_[(val)]
//#define B(color, val) (val)
#define DECLARE_SRC_POINTERS(pixel_t)					\
	const pixel_t *prev = (const pixel_t *)(src - stride); \
        const pixel_t *curr = (const pixel_t *)src; \
        const pixel_t *next = (const pixel_t *)(src + stride);

// RGR
// GBG
// RGR
#define BGGR_BGR888(p, n, div) \
        *dst++ = B(blue,curr[x] / (div));				\
        *dst++ = B(green, (prev[x] + curr[x - p] + curr[x + n] + next[x]) / (4 * (div))); \
        *dst++ = B(red, (prev[x - p] + prev[x + n] + next[x - p]  + next[x + n]) / (4 * (div))); \
        x++;

// GRG
// BGB
// GRG
#define GBRG_BGR888(p, n, div) \
        *dst++ = B(blue, (curr[x - p] + curr[x + n]) / (2 * (div)));	\
        *dst++ = B(green, curr[x] / (div));				\
        *dst++ = B(red, (prev[x] + next[x]) / (2 * (div)));		\
        x++;

void debayer8(uint8_t *dst, const uint8_t *src)
{
        DECLARE_SRC_POINTERS(uint8_t)

        for (int x = 0; x < (int)WIDTH;) {
                BGGR_BGR888(1, 1, 1)
                GBRG_BGR888(1, 1, 1)
        }
}

void debayer(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		debayer8(dst + y*stride *3, src + y*stride);
	}
}

#undef BGGR_BGR888
#undef GBRG_BGR888

// RGR
// GBG
// RGR
#define BGGR_BGR888(p, n, div) \
        *dst++ = B(blue,curr[x] / (div));				\
        *dst++ = B(green, (curr[x - p]) / (1 * (div))); \
        *dst++ = B(red, (prev[x - p]) / (1 * (div))); \
        x++;

// GRG
// BGB
// GRG
#define GBRG_BGR888(p, n, div) \
        *dst++ = B(blue, (curr[x - p]) / (1 * (div)));	\
        *dst++ = B(green, curr[x] / (div));				\
        *dst++ = B(red, (prev[x]) / (1 * (div)));		\
        x++;

void line_nearest(uint8_t *dst, const uint8_t *src)
{
        DECLARE_SRC_POINTERS(uint8_t)

        for (int x = 0; x < (int)WIDTH;) {
                BGGR_BGR888(1, 1, 1)
                GBRG_BGR888(1, 1, 1)
        }
}

void nearest(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		line_nearest(dst + y*stride *3, src + y*stride);
	}
}

/* Vectors
 * https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html
 */

// RGGB, other examples here are BGGR.
// _1:
// BGB
// GRG
// BGB

// _2:
// GBG
// RGR
// GBG

#define V 8
typedef unsigned char vu8 __attribute__ ((vector_size (V)));
typedef unsigned short vu16 __attribute__ ((vector_size (2*V)));

#define shuf __builtin_shuffle
#define widen(pr)  __builtin_convertvector(pr, vu16)
#define narow(pr)  __builtin_convertvector(pr, vu8)

void line_vector(uint8_t *dst, const uint8_t *src)
{
        DECLARE_SRC_POINTERS(uint8_t)
	vu8 *ds = (vu8 *)dst;

        for (int x = 0; x < (int)WIDTH; x+=V) {
	  	vu8 pr = *(vu8 *)prev;
		vu8 cu_l = *(vu8 *)(curr-1);
		vu8 cu_r = *(vu8 *)(curr+1);
		vu8 ne = *(vu8 *)next;
		vu16 around = widen(pr) + widen(ne);
		vu16 cu_mix = widen(cu_l) + widen(cu_r);

		vu8 red = narow(cu_mix / 2);            /* ?? R1 */
		vu8 green = narow(cu_mix + around / 4); /* G0 ?? G1 ?? ... */

		prev += sizeof(vu8);
		curr += sizeof(vu8);
		next += sizeof(vu8);

		*dst++ = cu_l[1];
		*dst++ = green[0];
		*dst++ = 0;

		*dst++ = red[0];
		*dst++ = cu_l[2];
		*dst++ = 0;

		

#if 0		
#define X -1		
		vu8 out1, out2, out3;
		/* Some R/G pixels can be used directly from the source */
		/*                      R         G     R */
		out1 =        shuf(cu_l, (vu8){1,X,X,  X,2,X,  3,X });
		out2 =        shuf(cu_l, (vu8){                    X,  X,4,X,  5,X,X,  X });
		out3 =        shuf(cu_r, (vu8){                                          4,X,   5,X,X,   X,6,X});

		out1 = shuf(out1, green, (vu8){0,8,2,  3,4,5,  6,10 });
		out2 = shuf(out1, green, (vu8){                    0,  1,2,3,  4,12,6,  7 });
		out3 = shuf(out1, green, (vu8){                                          0,1,   2,14,4,  5,6,7});

		/* FIXME: figure out red constants */
		out1 = shuf(out1, red  , (vu8){0,1,2,  8,4,5,  6,7 });
		out2 = shuf(out1, red  , (vu8){                    0,  10,2,3,  4,5,6,  12 });
		out3 = shuf(out1, red  , (vu8){                                          0,1,   2,3,4,   5,6,7});

#if 0
		out1 = shuf(out1, green, (vu8){0,1,2,  3,4,5,  6,7 });
		out2 = shuf(out1, green, (vu8){                    0,  1,2,3,  4,5,6,  7 });
		out3 = shuf(out1, green, (vu8){                                          0,1,   2,3,4,   5,6,7});
#endif		
		*ds++ = out1;
		*ds++ = out2;
		*ds++ = out3;
#endif
        }
}

void vector(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		line_vector(dst + y*stride *3, src + y*stride);
	}
}

#undef BGGR_BGR888
#undef GBRG_BGR888



void main(void)
{
	int i;
	for (i=0; i<PIX; i++) {
		red_[i] = green_[i] = blue_[i] = i>32 ? i-32 : 0;
	}
	for (i=0; i<WIDTH*HEIGHT; i++) {
		src[i] = random();
	}
	barrier();
	int mode = 3;
	switch (mode) {
	case 0:
		printf("Just memcpy\n");		
		for (i=0; i<400; i++) {
			memcpy(dst, src, WIDTH*HEIGHT);
			memcpy(dst+WIDTH*HEIGHT, src, WIDTH*HEIGHT);
			memcpy(dst+WIDTH*HEIGHT*2, src, WIDTH*HEIGHT);
			barrier();
		}
		break;
	case 1:
		printf("Nearest debayer\n");
		for (i=0; i<400; i++) {
			nearest();
			barrier();
		}
		break;
	case 2:
		printf("Average debayer\n");
		for (i=0; i<400; i++) {
			debayer();
			barrier();
		}
		break;
	case 3:
		printf("Vector debayer\n");
		for (i=0; i<400; i++) {
			vector();
			barrier();
		}
		break;
	default:
		printf("This is not good\n");
	}
	
	barrier();
	{
		unsigned long sum;
		for (i=0; i<WIDTH*HEIGHT*3; i++) {
			sum += dst[i];
		}
		printf("Sum: %lx\n", sum);
	}
}
