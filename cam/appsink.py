#!/usr/bin/python3

# https://github.com/TheImagingSource/tiscamera/blob/master/examples/python/07-appsink.py

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def mysink(appsink):
    # Define a callback function to handle received buffers
    def on_new_sample(appsink, data):
        global count
        print("new sample")

        # Get the source pad of the element
        pad = appsink.get_static_pad("sink")
        # Get the caps from the pad
        caps = pad.get_current_caps()
        # Print the caps
        print("Caps:", caps.to_string())
        
        sample = appsink.emit("pull-sample")
        buffer = sample.get_buffer()
        
        # Process the received buffer as needed
        size = buffer.get_size()
        data = buffer.extract_dup(0, size)  # Extract raw data
        # Handle the raw data (e.g., write to file, process further, etc.)
        #print(data)
        name = "/tmp/frame.grw"
        with open(name, "wb") as file:
            file.write(data)
            file.write(b'\0\n')
            file.write(bytes(caps.to_string()+"\n", "ascii"))
            file.write(bytes("%d\n"%size, "ascii"))
            file.write(b"GST\n")
            count += 1
        print("Wrote: ", name)

        return 0

    # Set the callback function on the "new-sample" signal of appsink
    s = appsink.connect("new-sample", on_new_sample, "")
    appsink.set_property("emit-signals", True)
    print("Connect", s)

count = 0   
Gst.init(None)
if False:
    Gst.debug_set_default_threshold(Gst.DebugLevel.INFO)

if True:
    s = "videotestsrc"
    s += " ! video/x-raw,width=(int)1280,height=(int)720,format=(string)YUY2 "
if True:
    s += " ! appsink name=sink"
pipeline = Gst.parse_launch(s)

p = pipeline.get_by_name("sink")
if p:
    mysink(p)

# Set the pipeline to the playing state
pipeline.set_state(Gst.State.PLAYING)

# Run the main loop to handle GStreamer events
loop = GLib.MainLoop()
try:
    loop.run()
except KeyboardInterrupt:
    pipeline.set_state(Gst.State.NULL)
    loop.quit()

