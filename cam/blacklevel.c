#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*
 * gcc -march=native -O3 blacklevel.c -S blacklevel.s
 * Wow. on x60, gcc automatically uses psubusb %xmm4,%xmm0, paddb  %xmm3,%xmm1,pcmpeqb %xmm2,%xmm0 ... it speeds up 12s->3s.
 *
 * x86 has 256-bit vectors; 512-bit is optional and eats extra power
 * Arm has Neon.
 */

const int stride = 1920;
#define HEIGHT 1080
#define WIDTH 1920
#define PIX 256

typedef uint8_t pixel_t;

#define barrier() asm volatile("": : : "memory")
static char src[WIDTH*HEIGHT], dst[WIDTH*HEIGHT];

#define BL 16

void debayer8(uint8_t *dst, const uint8_t *src)
{
        for (int x = 0; x < (int)WIDTH; x++)  {
		uint8_t v = src[x];
		if (v < 16)
			dst[x] = 0;
		else
			dst[x] = v-16;
        }
}

void debayer(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		debayer8(dst + y*stride, src + y*stride);
	}
}

/* -------------------------------------------------------------------- */

void line_nearest(uint8_t *dst, const uint8_t *src)
{
        for (int x = 0; x < (int)WIDTH; x++) {
		dst[x] = src[x];
        }
}

void nearest(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		line_nearest(dst + y*stride, src + y*stride);
	}
}

/* Vectors
 * https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html
 */

#define V 8
typedef unsigned char vu8 __attribute__ ((vector_size (V)));
typedef unsigned short vu16 __attribute__ ((vector_size (2*V)));

#define shuf __builtin_shuffle
#define widen(pr)  __builtin_convertvector(pr, vu16)
#define narow(pr)  __builtin_convertvector(pr, vu8)

void line_vector(uint8_t *dst, const uint8_t *src)
{
	vu8 *cr = (vu8 *)src;
	vu8 *ds = (vu8 *)dst;

        for (int x = 0; x < (int)WIDTH; x+=V) {
		vu8 cu = *cr++;
		vu8 out = cu;

		*ds++ = out;
        }
}

void vector(void) {
	for (int y = 1; y < HEIGHT-1; y++) {
		line_vector(dst + y*stride *3, src + y*stride);
	}
}

void main(void)
{
	int i;
	for (i=0; i<WIDTH*HEIGHT; i++) {
		src[i] = random();
	}
	barrier();
	int mode = 2;
	int iter = 2000;
	switch (mode) {
	case 0:
		printf("Just memcpy\n");		
		for (i=0; i<iter; i++) {
			memcpy(dst, src, WIDTH*HEIGHT);
			barrier();
		}
		break;
	case 1:
		printf("Nearest -- per byte copy\n");
		for (i=0; i<iter; i++) {
			nearest();
			barrier();
		}
		break;
	case 2:
		printf("Debayer -- per byte processing\n");
		for (i=0; i<iter; i++) {
			debayer();
			barrier();
		}
		break;
	case 3:
		printf("Vector debayer\n");
		for (i=0; i<iter; i++) {
			vector();
			barrier();
		}
		break;
	default:
		printf("This is not good\n");
	}
	
	barrier();
	{
		unsigned long sum;
		for (i=0; i<WIDTH*HEIGHT; i++) {
			sum += dst[i];
		}
		printf("Sum: %lx\n", sum);
	}
}
