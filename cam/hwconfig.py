#!/usr/bin/python3

import os
import time
import sys
import datetime
import re

class RecParam:
    def __init__(m, w, h, fps):
        m.height = h
        m.width = w
        m.fps = fps
        m.bayer = False

def picture_name(ext = ".mkv"):
    s = os.getenv("HOME")+"/Pictures/"
    if ext == ".jpg":
        s += "IMG"
    else:
        s += "VID"
    s += datetime.datetime.now().strftime("%y%m%d%H%M%S")
    s += ext
    return s

def run_timed(s):
    t = time.time()
    print("Running ", s)
    os.system(s)
    print("... ", time.time() - t, " sec")
        
class HWConfig:
    audio = True
    preview = True
    
    def init(m):
        pass
    
    def on_finish(m):
        pass

    def setup_media_rec(m, rp):
        pass

    def gl_source(m, rp=None, buffers=0):
        s =  "v4l2src "
        if buffers:
            s += " num-buffers=%d" % buffers
        s += m.gl_source_format()
        return s

    def gl_source_format(m):
        if m.mode == "capture" or m.mode == "record":
            return " ! video/x-raw,width=1280,height=720 "
        else:
            return " ! video/x-raw,width=640,height=480 "

    def gl_preview(m):
        l = ""
        l += " ! videoconvert ! video/x-raw,format=YUY2 "
        #l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=360,height=320 "
        l += " ! videoconvert "
        l += " ! ximagesink "
        #l += " ! waylandsink "
        #l += " ! fakesink "
        return l

    def gl_record(m, compress = False, fname = picture_name()):
        s = ""
        # avimux... resulting file is "fast"? Due to dropped frames?

        video = True

        if video:
            #s += " ! queue "
            if m.preview:
                s += " ! tee name=t "
            s += " ! queue "                
            #s += " ! avenc_mjpeg ! mux. "
            #s += " ! ffmpegcolorspace ! jpegenc ! mux. "
            #s += " ! videoconvert ! matroskamux ! filesink location=/tmp/delme.flv sync=false"
            if compress:
                s += " ! videoconvert "
                #s += " ! x264enc ! mux. "
                s += " ! jpegenc ! mux. "
            else:
                s += " ! mux. "
            if m.preview:
                s += " t. "
                #s += " ! videoconvert  "
                s += " ! queue leaky=downstream "
                s += m.gl_preview()
                #s += " ! fakesink "

        # Works okay when ran locally. Does not work well over ssh. Expected?
        if m.audio:
            s += "autoaudiosrc ! queue ! audioconvert ! mux. "

        s += "matroskamux name = mux ! filesink location=%s sync=false " % fname
        return s

    def gl_capture(m):
        l = "! videoconvert "
        l += "! jpegenc ! filesink location="+picture_name(".jpg")
        return l

    def gl_pipeline(m):
        if m.mode == "capture":
            l = m.gl_source(buffers = 1)
        else:
            l = m.gl_source()

        if m.mode == "record":
            l += m.gl_record(compress = True)
        if m.mode == "preview":
            l += m.gl_preview()
        if m.mode == "capture":
            l += m.gl_capture()

        print("Pipeline is", l)
        return l

class PCConfig(HWConfig):
    pass

class DroidConfig(HWConfig):
    # For systems like droidian, which have small Android inside
    def gl_source(m, rp=None, buffers=0):
        s = "droidcamsrc video-torch=1 mode=2 "
        #s = "videotestsrc "
        if buffers:
            s += " num-buffers=%d" % buffers
        s += m.gl_source_format()
        return s

    def gl_source_format(m):
        #return " ! video/x-raw,width=1920,height=1080,format=NV21,framerate=30/1 ! queue "
        return " ! video/x-raw,width=320,height=240,format=NV21,framerate=30/1 ! queue "

class L5dConfig(HWConfig):
    audio = False
    # I did not figure out how to record audio.
    
    def gl_source_format(m):
        # Record in 1280x720 results in dropped frames?
        if m.mode == "capture":
            return " ! video/x-raw,width=1280,height=720 "
        else:
            return " ! video/x-raw,width=640,height=480 "

class JPEGConfig(HWConfig):
    audio = True
    preview = True
    
    # For cameras that can do JPEG encoding in hardware
    def gl_preview(m):
        l = " ! jpegdec ! videoflip method=clockwise "
        l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=202,height=360 "
        l += " ! ximagesink "
        return l

    def gl_record(m, compress = False, fname = picture_name()):
        s = ""
        video = True
        if video:
            if m.preview:
                s += " ! tee name=t "
            s += " ! queue ! jpegparse"
            s += " ! mux. "
            if m.preview:
                s += " t. "
                s += " ! queue leaky=downstream "
                s += " ! videorate ! image/jpeg,framerate=3/1 "
                s += m.gl_preview()

        if m.audio:
            s += "autoaudiosrc ! queue ! audioconvert ! mux. "

        s += "matroskamux name = mux ! filesink location=%s sync=false " % fname
        return s

    def gl_capture(m):
        l = " ! jpegparse ! filesink location="+picture_name(".jpg")
        return l

class PineConfig(JPEGConfig):
    dev_capture = "/dev/v4l/by-path/platform-1cb0000.csi-video-index0"

    def setup_media(m, s):
        pass

    def gl_source(m, rp=None, buffers=0):
        if not rp:
            rp = RecParam(1280, 720, 30)
        if True:
            s = "v4l2src device=%s" % m.dev_capture
        else:
            s = "libcamerasrc camera-name=/base/i2c-csi/rear-camera@4c"
        if buffers:
            s += " num-buffers=%d" % buffers
        s += " ! image/jpeg,width=%d,height=%d,framerate=%d/1 " % ( rp.width, rp.height, rp.fps)
        return s

    def gl_pipeline(m):
        if m.mode != "preview" and m.mode != "capture" and m.mode != "record":
            print("Unknown pipeline requested")
            return None

        buffers = 0
        #rp = RecParam(1920, 1080, 30)
        #rp = RecParam(1280, 720, 30)
        #rp = RecParam(640, 480, 30)
        rp = RecParam(320, 240, 30)
        if m.mode == "capture":
            # First image is simply green
            # Second image is simply black
            buffers = 3
        if m.mode == "record":
            l = m.gl_source(rp)
            l += m.gl_record(True, picture_name())
            return l
            
        m.setup_media_rec(rp)
        l = m.gl_source(rp, buffers=buffers)

        if m.mode == "preview":
            l += m.gl_preview()
        if m.mode == "capture":
            l += "! jpegenc ! multifilesink location=/tmp/delme%d.jpg"

        return l

class PineConfig_gst(HWConfig):
    dev_capture = "/dev/v4l/by-path/platform-1cb0000.csi-video-index0"
    # UYVY stopped working with new gstreamer. UYVY would be nicer, but
    # needs a patch -- gst-plugins-good.diff
    force_bayer = False

    def setup_media_dev(m, s, dev):
        base = "media-ctl -d %s " % dev
        # Na novych kernelech je to 3-004c
        print("Setup for ", s)
        if True: # Main camera
            #cmdline = base + '--links \'"gc2145 4-003c":0->"sun6i-csi":0[0]\''
            #run_timed(cmdline)
            #cmdline = base + '--links \'"ov5640 4-004c":0->"sun6i-csi":0[1]\''
            #run_timed(cmdline)
            cmdline = base + "-V '"
            cmdline += '"ov5640 3-004c":0 [%s]' % s
            cmdline += "'"
            run_timed(cmdline)
        else: # Selfie camera
            cmdline = base + '--links "ov5640 4-004c":0->"sun6i-csi":0[0]'
            run_timed(cmdline)
            cmdline = base + '--links "gc2145 4-003c":0->"sun6i-csi":0[1]'
            run_timed(cmdline)
            cmdline = base  + "-V '"
            s = 'fmt:UYVY8_2X8/1280x720'
            cmdline += '"gc2145 4-003c":0 [%s]' % s
            cmdline += "'"
            run_timed(cmdline)
        run_timed("media-ctl -d %s -p" % dev)
#        sys.exit(0)
# Test with: gst-launch-1.0 v4l2src device=/dev/v4l/by-path/platform-1cb0000.csi-video-index0 \! queue \! autovideosink ?

    def setup_media(m, s):
        m.setup_media_dev(s, "platform:1cb0000.csi")

    def setup_media_rec(m, rp):
        if m.force_bayer:
            rp.bayer = True
        s = "UYVY8_2X8"
        if rp.bayer:
            s = "SBGGR8_1X8"

        s = "%s %dx%d" % (s, rp.width, rp.height)
        print("Media setup", s)
        m.setup_media(s)

    def gl_source(m, rp=None, buffers=0):
        if not rp:
            rp = RecParam(1280, 720, 30)
        s = "v4l2src device=%s" % m.dev_capture
        if buffers:
            s += " num-buffers=%d" % buffers
        s += " !"
        if not m.force_bayer:
            s += " video/x-raw,width=%d,height=%d,format=UYVY,framerate=%d/1 " % ( rp.width, rp.height, rp.fps)
        else:
            s += " video/x-bayer,width=%d,height=%d,format=bggr,framerate=%d/1,colorimetry=(string)1:1:0:0 ! bayer2rgb ! videoconvert ! video/x-raw,format=UYVY " % ( rp.width, rp.height, rp.fps)
        # ,colorimetry=(string)1,1,0,0
        return s

    def gl_preview_fast(m):
        l = " ! videoscale method=0 envelope=1 ! video/x-raw,width=640,height=360 "
        #l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=512,height=288 "
        l += " ! videoconvert "
        l += " ! videoflip method=clockwise "
        l += " ! ximagesink "
        return l

    def gl_preview(m):
        if m.mode != "record":
            l = " ! videoscale method=0 envelope=1 ! video/x-raw,width=640,height=360 "
            #l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=512,height=288 "
            l += " ! videorate ! video/x-raw,framerate=10/1 "
            l += " ! videoconvert "
            l += " ! videoflip method=clockwise "
            l += " ! ximagesink "
            return l
        else:
            s = " ! videorate ! video/x-raw,framerate=3/1 "
            s += " ! videoscale method=0 envelope=1 ! video/x-raw,width=320,height=180 "
            s += " ! videoflip method=clockwise "        
            s += " ! videoconvert "
            #s += " ! queue leaky=downstream ! autovideosink "
            s += " ! ximagesink "
            #s += " ! autovideosink "
            return s

    def gl_pipeline(m):
        if m.mode != "preview" and m.mode != "capture" and m.mode != "record":
            print("Unknown pipeline requested")
            return None

        buffers = 0
        if m.mode ==  "record":
            #rp = RecParam(1920, 1080, 15)
            rp = RecParam(1280, 720, 30)
        elif m.mode == "preview":
            rp = RecParam(1280, 720, 30)
        else:
            #rp = RecParam(2592, 1944, 10) # Failed to allocate memory, maybe running megapixels first would fix it.
            rp = RecParam(1920, 1080, 15)
            # First image is simply green
            # Second image is simply black
            buffers = 3
        if m.mode == "record":
            l = m.gl_source(rp)
            compress = True
            # 30/30:                              18.87 sec
            # 30/30, 320x180 save:                18.91 sec, 158% CPU
            #        640x360 raw save:            17.74 sec, 70% CPU
            #        640x360 raw save + a + p:
            if not compress:
                l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=640,height=360 "
            else:
                # no scaling -- PinePhone too slow
                # 1600x900 -- too slow
                # 1408x792 -- PinePhone 114% CPU, too slow
                # 1280x720 -- PinePhone , too slow, just 105%?
                # 1267x712 (99/100)  -- PinePhone 156% CPU, ok
                # 1216x684 -- PinePhone 150% CPU, ok?
                # 1152x654 (9/10) -- PinePhone 160% CPU, ok
                # 960x540 -- PinePhone 150% CPU, ok
                # 853x480 -- PinePhone 142% CPU, ok
                # 640x360 -- PinePhone works ok
                if not m.force_bayer:
                    l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=1267,height=712 "
                else:
                    #l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=960,height=540 "
                    l += " ! videoscale method=0 envelope=1 ! video/x-raw,width=640,height=360 "
            l += m.gl_record(compress, picture_name())
            return l
            
        m.setup_media_rec(rp)
        l = m.gl_source(rp, buffers=buffers)

        if m.mode == "preview":
            l += m.gl_preview()
        if m.mode == "capture":
            #l += "! jpegenc ! filesink location=/tmp/delme.jpg"
            l += "! jpegenc ! multifilesink location=/tmp/delme%d.jpg"

        return l

    def on_finish(m):
        os.system("mv /tmp/delme2.jpg "+picture_name(".jpg"))

def hw_probe():
    sys = "PC"
    path = "/proc/device-tree/compatible"
    if os.path.exists(path):
        sys = open(path).read().split('\0')[0]
    if sys == "pine64,pinephone-1.2":
        return PineConfig()
    if sys == "purism,librem5-devkit":
        return L5dConfig()
    if sys == "qcom,sdm845-mtp":
        return DroidConfig()
    return PCConfig()

# FIXME: This should really get .hw parameter and proceed as usual.
class QuickHacks:
    def __init__(m):
        m.hw = hw_probe()
        
    def test_output(m):
        s = "gst-launch-1.0 "
        s += "videotestsrc \! queue \! autovideosink"
        run_timed(s)

    def test_mirror(m):
        m.setup_media("UYVY8_2X8 1280x720")
        s = "gst-launch-1.0 "
        if True:
            s += "v4l2src device=%s num-buffers=150 " % m.dev_capture
            s += " \! video/x-raw,width=1280,height=720,format=UYVY,framerate=15/1 "
            s += " \! queue leaky=downstream \! videoconvert \! autovideosink "
        run_timed(s)

    def save_raw(m):
        m.setup_media("UYVY8_2X8 1280x720")
        s = "gst-launch-1.0 "
        #s += "matroskamux name = mux \! filesink location=/tmp/delme.flv sync=false "
        # matroskamux... ~same as flvmux
        s += "v4l2src device=%s num-buffers=300 " % m.dev_capture
        s += " \! video/x-raw,width=1280,height=720,format=UYVY,framerate=30/1 "
        #s += " \! videoconvert \! x264enc "
        s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=640,height=360 "
        s += " \! filesink location=/my/delme.raw sync=false "
        run_timed(s)
        #                    17 seconds. 500MB/10second, 50MB/second.
        # avenc-mjpeg:       88 seconds
        # x264enc:           81 seconds
        # scale to half:    119 seconds
        # videoscale method=0 envelope=1   17 seconds. 12MB/second...

    def record_raw(m):
        m.setup_media("UYVY8_2X8 1280x720")
        s = "gst-launch-1.0 "
        s += "v4l2src device=%s num-buffers=300 " % m.dev_capture
        s += " \! video/x-raw,width=1280,height=720,format=UYVY,framerate=30/1 "
        s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=640,height=360 "
        s += " \! matroskamux"
        s += " \! filesink location=/my/delme.raw.mkv sync=false "
        run_timed(s)
        #                    17 seconds. 500MB/10second, 50MB/second.
        
    def parse_raw(m):
        s = "gst-launch-1.0 "
        s += "filesrc location=/my/delme.raw \! videoparse format=5 framerate=30/1 width=640 height=360 "
        s += " \! videoconvert \! autovideosink"
        run_timed(s)

    def convert_raw(m):
        s = "gst-launch-1.0 "
        s += "filesrc location=/my/delme.raw \! videoparse format=5 framerate=30/1 width=640 height=360 "
        s += " \! videoconvert \! x264enc \! matroskamux \! filesink location=/my/delme.mkv sync=false"
        run_timed(s)

    # video/x-bayer,width=2592,height=1944,format=bggr,framerate=10/1
    # video/x-bayer,width=1920,height=1080,format=bggr,framerate=15/1

    def record_try(m):
        rp = RecParam(1280, 720, 30)
        #rp = RecParam(1920, 1080, 15)
        #rp = RecParam(2592, 1944, 10)   # Does not work?
        m.record_rec(rp)

    def run_gst(m, l):
        l = re.sub("!", "\!", l)
        run_timed("gst-launch-1.0 "+l)

    def record_old(m, fname = "/tmp/delme.mkv"):
        compress = True
        rp = RecParam(1280, 720, 30)
        m.hw.mode = "record"
        m.hw.setup_media_rec(rp)
        s = m.hw.gl_source(rp)
        # 30/30:                              18.87 sec
        # 30/30, 320x180 save:                18.91 sec, 158% CPU
        #        640x360 raw save:            17.74 sec, 70% CPU
        #        640x360 raw save + a + p:
        
        #if not compress:
        #    s += " ! videoscale method=0 envelope=1 ! video/x-raw,width=640,height=360 "
        #else:
        #    s += " ! videoscale method=0 envelope=1 ! video/x-raw,width=320,height=180 "        
        s += m.hw.gl_record(compress, fname)
        print(s)
        m.run_gst(s)
        os.system("ls -alh "+fname)

    def record(m):
        m.hw.mode = "record"
        s = m.hw.gl_pipeline()
        print(s)
        m.run_gst(s)
        os.system("ls -alh ~/Pictures/VID*")
        

    def photo(m):
        rp = RecParam(1280, 720, 30)
        #rp = RecParam(2592, 1944, 10)   # Does not work? ... running megapixels fixes it.
        rp.bayer = True
        m.setup_media_rec(rp)
        s = "gst-launch-1.0 "
        #s += "v4l2src device=%s num-buffers=2 " % m.dev_capture
        s += "videotestsrc num-buffers=100 "
        s += " \! video/x-bayer,width=%d,height=%d,format=bggr,framerate=10/1 " % (rp.width, rp.height)
        s += " \! bayer2rgb \! videoconvert "
        s += " \! jpegenc \! filesink location=/tmp/delme.jpg"
        #s += " \! autovideosink "
        #s += " \! fakesink "
        run_timed(s)
        # With videotestsrc, ~ 435msec per frame, wow.
        #                       85msec per frame in 1280x720 config. On one cpu.
        #                   ... 89msec per frame in 1280x720 ... when all 4 CPUs are loaded.
        #                                       -> 44.9fps.

    def jpeg_speedtest(m):
        rp = RecParam(1280, 720, 30)
        #rp = RecParam(2592, 1944, 10)   # Does not work? ... running megapixels fixes it.
        rp.bayer = True
        m.hw.setup_media_rec(rp)
        s = "gst-launch-1.0 "
        #s += "v4l2src device=%s num-buffers=2 " % m.dev_capture
        s += "videotestsrc num-buffers=100 "
        s += " \! video/x-raw,width=%d,height=%d,format=UYVY,framerate=10/1 " % (rp.width, rp.height)
        s += " \! videoconvert "
        s += " \! jpegenc \! filesink location=/tmp/delme.jpg"
        run_timed(s)
        # With videotestsrc, ~ 54msec per frame in 1280x720, on one cpu.

    def audiotest(m):
        #s = "autoaudiosrc ! queue ! audioconvert "
        s = "pulsesrc ! queue ! audioconvert "
        s += " ! matroskamux"
        s += " ! filesink location=/my/delme.raw.mkv sync=false "
        m.run_gst(s)

    def handle_cmdline(m, argv):
        if len(argv) < 2:
            m.record()
            return
        if argv[1] == "-t":
            m.jpeg_speedtest()
        if argv[1] == "-a":
            m.audiotest()
        if argv[1] == "-i":
            m.hw.setup_media("UYVY8_2X8 1280x720")
        if argv[1] == "-j":
            m.hw.mode = "capture"
            s = m.hw.gl_pipeline()
            os.system("rm /tmp/delme[012].jpg")
            print(s)
            m.run_gst(s)
            os.system("ls -alh /tmp/delme*jpg")
            # yavta -c -F /dev/v4l/by-path/platform-1cb0000.csi-video-index0


class Recorder:
    def init(m):
        m.hw = hw_probe()
        m.hw.init()

        m.pipeline("record")

    def pipeline(m, s):
        # = m.test_pipeline()
        m.hw.mode = s
        cmd = m.hw.gl_pipeline()
        print(cmd)
        #m.hw.setup_media("SBGGR8_1X8 1280x720")
        
        #run_timed("gst-launch-1.0 " + cmd)
        

if False and  __name__ == "__main__":
    r = QuickHacks()
#r.save_raw()
#r.parse_raw()
#r.convert_raw()
#r.test_output()
#r.test_mirror()
#r.record_raw_audio()
    r.handle_cmdline(sys.argv)

if True and  __name__ == "__main__":
    r = Recorder()
    r.init()
    
