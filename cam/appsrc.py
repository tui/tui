#!/usr/bin/python3

# https://github.com/TheImagingSource/tiscamera/blob/master/examples/python/07-appsink.py

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def mysrc(appsrc):
    def on_need_data(appsrc, data, length):
        print("Need data: ", length)
        
    appsrc.set_property("format", Gst.Format.TIME)
    appsrc.set_property("is-live", True)
    appsrc.set_property("block", True)

    # Create caps for the RGB format
    caps_string = "video/x-raw,format=RGB,width=10,height=10"
    caps = Gst.Caps.from_string(caps_string)
    appsrc.set_property("caps", caps)

    # Push RGB image data
    rgb_data = bytes([255, 0, 0] * 80 + [0, 255, 0] * 10 + [0, 255, 255] * 10)  # RGB 10x10 image data
    print(rgb_data)
    buffer = Gst.Buffer.new_wrapped(rgb_data)
    appsrc.emit("push-buffer", buffer)

    s = appsrc.connect("need-data", on_need_data, "")
    print("Connect", s)

def grwsrc(appsrc):
    def on_need_data(appsrc, data, length):
        print("Need data: ", length)
        
    appsrc.set_property("format", Gst.Format.TIME)
    appsrc.set_property("is-live", True)
    appsrc.set_property("block", True)

    name = "/tmp/1714721401249920.grw"
    with open(name, "rb") as file:
        rgb_data = file.read(10*1024*1024)
    i = len(rgb_data)
    print("File size:", i)
    i -= 1
    while rgb_data[i] != 0:
        i -= 1
    print("Footer at:", i)
    footer = rgb_data[i+1:]
    print(footer)
    sp = str(footer, 'ascii').split('\n')
    print(sp)
    # Create caps for the file
    caps_string = sp[0][6:]
    if sp[0][:6] != "Caps: ":
        print("Bad footer")
    if sp[1][:6] != "Size: ":
        print("Bad footer")

    caps = Gst.Caps.from_string(caps_string)
    appsrc.set_property("caps", caps)

    buffer = Gst.Buffer.new_wrapped(rgb_data)
    # nanoseconds
    print(buffer.pts, buffer.dts, buffer.duration)
    
    appsrc.emit("push-buffer", buffer)

    s = appsrc.connect("need-data", on_need_data, "")
    print("Connect", s)

count = 0
    
def mysink(appsink):
    # Define a callback function to handle received buffers
    def on_new_sample(appsink, data):
        global count
        print("new sample")

        # Get the source pad of the element
        pad = appsink.get_static_pad("sink")
        # Get the caps from the pad
        caps = pad.get_current_caps()
        # Print the caps
        print("Caps:", caps.to_string())
        
        sample = appsink.emit("pull-sample")
        buffer = sample.get_buffer()
        
        # Process the received buffer as needed
        size = buffer.get_size()
        data = buffer.extract_dup(0, size)  # Extract raw data
        # Handle the raw data (e.g., write to file, process further, etc.)
        #print(data)
        name = "/tmp/delme.image%06d.jpeg" % count
        with open(name, "wb") as file:
            file.write(data)
            file.write(b'\0\n')
            file.write(bytes(caps.to_string()+"\n", "ascii"))
            file.write(bytes("%d\n"%size, "ascii"))
            file.write(b"GST\n")
            count += 1
        print("Wrote: ", name)

        return 0

    # Set the callback function on the "new-sample" signal of appsink
    s = appsink.connect("new-sample", on_new_sample, "")
    appsink.set_property("emit-signals", True)
    print("Connect", s)

Gst.init(None)
if False:
    Gst.debug_set_default_threshold(Gst.DebugLevel.INFO)

if True:
    s = "appsrc name=source"    
else:
    s = "videotestsrc"
    s += " ! video/x-raw,width=(int)640,height=(int)480,format=(string)RGB "
if False:
    s += " ! videoconvert ! jpegenc"
    s += " ! appsink name=sink"
else:
    s += " ! videoconvert ! autovideosink"
pipeline = Gst.parse_launch(s)

p = pipeline.get_by_name("source")
if p:
    if False:
        mysrc(p)
    else:
        grwsrc(p)
p = pipeline.get_by_name("sink")
if p:
    mysink(p)

# Set the pipeline to the playing state
pipeline.set_state(Gst.State.PLAYING)

# Run the main loop to handle GStreamer events
loop = GLib.MainLoop()
try:
    loop.run()
except KeyboardInterrupt:
    pipeline.set_state(Gst.State.NULL)
    loop.quit()

