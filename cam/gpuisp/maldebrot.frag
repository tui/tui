#version 100
// 100 is suitable for X220 with X.
// 110 may be suitable for pinephone.

#ifdef GL_ES
precision mediump float;
#endif

//
// Shader experiments.
//

vec4 v_color = vec4(1.0, 1.0, 1.0, 1.0);
varying vec2 v_texcoord;

uniform sampler2D tex0;

int maxIterations = 100;

vec4 gradient(vec2 pos) {
  float x = pos.x;
  float y = pos.y;
  return vec4(x, y, 0.0, 0.0);
}

vec4 maldebrot(vec2 pos, vec2 start, float zoom) {
  float x = (pos - start).x / zoom;
  float y = (pos - start).y / zoom;

  float zx = 0.0, zy = 0.0;
  int i = 0;
    
    // Calculate whether the current point belongs to the Mandelbrot set
    while ((zx * zx + zy * zy < 4.0) && (i < maxIterations)) {
      float tmp = zx * zx - zy * zy + x;
      zy = 2.0 * zx * zy + y;
      zx = tmp;
      i++;
    }

  //return vec4(x, y, 0.0, 0.0);
  return vec4(i / maxIterations, i / maxIterations, i / maxIterations, 0.0);
}

void main(){
  vec2 pos=v_texcoord;
  vec2 start = vec2(0.0, 0.0);
  float zoom = 1.0;
  if (false) {
    vec3 comm = texture2D(tex0, start, 0.0).rgb;
    float r1 = comm.r;
    float g1 = comm.g;
    float b1 = comm.b;
    float x = r1/1.0;
    float y = g1/1.0;

    start.x = x;
    start.y = y;
    zoom = (b1 - 0.5) * 15.0;
  }

  vec4 fragColor = maldebrot(pos, start, zoom);
  gl_FragColor=v_color * vec4(fragColor.rgb, 1.0);
}

