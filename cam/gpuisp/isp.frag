#version 100
#ifdef GL_ES
precision mediump float;
#endif
varying vec2 v_texcoord;
uniform sampler2D tex;
uniform float time;
uniform float width;
uniform float height;

#define S 16
float lut_black[S], lut_tone[S], lut_exposure[S], lut_gamma[S];
#define T 4 // Works but slow with 64.
// Possible improvement is different for green1, green2. 16x16 should be enough for this.
float lut_shading_red[T*T], lut_shading_green[T*T], lut_shading_blue[T*T];
#define U 3
// Had 16 here. That led to significant CPU&memory use. 16 is what profesionals use.
vec3 map_hue_sat[U*U*U], map_color[U*U*U];

// Function to perform 1D lookup with linear interpolation on a GLSL array
float lookup_1d(float array[S], float value) {
    float index = value * (float(S) - 1.0); // Scale the value to the range [0, arraySize-1]
    int floorIndex = int(floor(index));
    float fractIndex = index - float(floorIndex);

    float lowerValue = array[floorIndex];
    float upperValue = array[min(floorIndex + 1, S - 1)];

    return mix(lowerValue, upperValue, fractIndex);
}

#define AR2(x, y) ar2[(y)*T+(x)]
// Function to perform 2D lookup with linear interpolation on a GLSL 2D array
float lookup_2d(float ar2[T*T], vec2 value) {
    vec2 index = value * vec2(float(T - 1), float(T - 1));

    ivec2 floorIndex = ivec2(floor(index));
    vec2 fractIndex = index - vec2(floorIndex);

    float lowerLeftValue = AR2(floorIndex.x, floorIndex.y);
    float lowerRightValue = AR2(min(floorIndex.x + 1, T - 1), floorIndex.y);
    float upperLeftValue = AR2(floorIndex.x, min(floorIndex.y + 1, T - 1));
    float upperRightValue = AR2(min(floorIndex.x + 1, T - 1), min(floorIndex.y + 1, T - 1));

    float topInterpolation = mix(upperLeftValue, upperRightValue, fractIndex.x);
    float bottomInterpolation = mix(lowerLeftValue, lowerRightValue, fractIndex.x);

    return mix(bottomInterpolation, topInterpolation, fractIndex.y);
}

#define AR3I(x, y, z) ((z)*U*U+(y)*U+(x))
#define AR3(x, y, z) ar3[AR3I(x,y,z)]
// Function to perform 3D lookup with linear interpolation on a GLSL 3D array
vec3 lookup_3d(vec3 ar3[U*U*U], vec3 value) {
    vec3 index = value * vec3(float(U - 1), float(U - 1), float(U - 1));

    ivec3 floorIndex = ivec3(floor(index));
    vec3 fractIndex = index - vec3(floorIndex);

    vec3 frontLowerLeftValue = AR3(floorIndex.x, floorIndex.y, floorIndex.z);
    vec3 frontLowerRightValue = AR3(min(floorIndex.x + 1, U - 1), floorIndex.y, floorIndex.z);
    vec3 frontUpperLeftValue = AR3(floorIndex.x, min(floorIndex.y + 1, U - 1), floorIndex.z);
    vec3 frontUpperRightValue = AR3(min(floorIndex.x + 1, U - 1), min(floorIndex.y + 1, U - 1), floorIndex.z);

    vec3 backLowerLeftValue = AR3(floorIndex.x, floorIndex.y, min(floorIndex.z + 1, U - 1));
    vec3 backLowerRightValue = AR3(min(floorIndex.x + 1, U - 1), floorIndex.y, min(floorIndex.z + 1, U - 1));
    vec3 backUpperLeftValue = AR3(floorIndex.x, min(floorIndex.y + 1, U - 1), min(floorIndex.z + 1, U - 1));
    vec3 backUpperRightValue = AR3(min(floorIndex.x + 1, U - 1), min(floorIndex.y + 1, U - 1), min(floorIndex.z + 1, U - 1));

    vec3 frontTopInterpolation = mix(frontUpperLeftValue, frontUpperRightValue, fractIndex.x);
    vec3 frontBottomInterpolation = mix(frontLowerLeftValue, frontLowerRightValue, fractIndex.x);

    vec3 backTopInterpolation = mix(backUpperLeftValue, backUpperRightValue, fractIndex.x);
    vec3 backBottomInterpolation = mix(backLowerLeftValue, backLowerRightValue, fractIndex.x);

    vec3 zInterpolationFront = mix(frontBottomInterpolation, frontTopInterpolation, fractIndex.y);
    vec3 zInterpolationBack = mix(backBottomInterpolation, backTopInterpolation, fractIndex.y);

    return mix(zInterpolationFront, zInterpolationBack, fractIndex.z);
}

void init_luts(void) {
     if (false) {
          lut_black[0] = 0.0;
	  lut_black[1] = 0.1;
	  lut_black[2] = 0.2;
	  lut_black[3] = 0.4;
	  lut_black[4] = 0.6;
	  lut_black[5] = 0.8;
	  lut_black[6] = 0.9;
	  lut_black[7] = 1.0;
     } else {
          for (int i = 0; i < S; i++) {
     	  	 lut_black[i] = float(i)/float(S-1);
     	  	 lut_exposure[i] = float(i)/float(S-1);
     	  	 lut_tone[i] = float(i)/float(S-1);
     	  	 lut_gamma[i] = float(i)/float(S-1);		 
          }
	  //lut_black[1] = 0.1;
     }

     for (int i = 0; i < T*T; i++) {
     	 lut_shading_red[i] = lut_shading_green[i] = lut_shading_blue[i] = 1.0;
     }
     //lut_shading_green[6] = 0.1;

     for (int r = 0; r < U; r++)
          for (int g = 0; g < U; g++)
	      for (int b = 0; b < U; b++) {
	          vec3 val = vec3(float(r)/float(U-1), float(g)/float(U-1), float(b)/float(U-1));
	      	  map_hue_sat[AR3I(r,g,b)] = val;
	      	  map_color[AR3I(r,g,b)] = val;
	      }
}

float black(float v) {
      if (false)
            return v;
      if (false)	    
            return v < 0.3 ? 0.0 : v - 0.3;
      return lookup_1d(lut_black, v);
}

float shading(vec2 p, float v) {
      if (true)
            return v;
      return (p.x < 0.1 || p.x > 0.9) || (p.y < 0.1 || p.y > 0.9) ? v : v * 0.5;
}

// FIXME: these three lookups could be done in paralel, on vectors.
// Probably should simply have lookup table of vec3.
float shading_r(vec2 p, float r) { return lookup_2d(lut_shading_red, p) * r; }
float shading_g(vec2 p, float r) { return lookup_2d(lut_shading_green, p) * r; }
float shading_b(vec2 p, float r) { return lookup_2d(lut_shading_blue, p) * r; }

vec3 noisy(vec2 p) {
     return texture2D( tex, p ).rgb;
}

vec3 denoised(vec2 p) {
     vec2 sx = vec2(1. / width, 0.);
     vec2 sy = vec2(0., 1. / height);
     vec3 res = noisy(p);
     vec3 m;

     m = noisy(p-sx);
     m = min(m, noisy(p+sx));
     m = min(m, noisy(p-sy));
     m = min(m, noisy(p+sy));
     res = max(res, m);

     m = noisy(p-sx);
     m = max(m, noisy(p+sx));
     m = max(m, noisy(p-sy));
     m = max(m, noisy(p+sy));
     res = min(res, m);

     return res;
}

/*
From http://jgt.akpeters.com/papers/McGuire08/

Efficient, High-Quality Bayer Demosaic Filtering on GPUs

Morgan McGuire

This paper appears in issue Volume 13, Number 4.
---------------------------------------------------------
Copyright (c) 2008, Morgan McGuire. All rights reserved.

Ported to OpenGL ES 2.0 and tested on Raspberry Pi
by Rasmus Raag, Tallinn University of Technology

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

float fetch(float x, float y) {
    vec2 p = vec2(x, y);
    vec3 res = denoised(p);

    // 2. Black light + linearize (1D LUT)
    // black/white level might be enough
    res = vec3(black(res.r), black(res.g), black(res.b));
    // 3. Lens shading            (2D LUT)
    res = vec3(shading_r(p, res.r), shading_g(p, res.g), shading_b(p, res.b));
    // debayer
    // noise reduction

    // 4. Debayer
    // ...

    return res.g;
}

vec3 debayer(vec4 center, vec4 xCoord, vec4 yCoord) {
    vec2 pos = center.xy;
    vec2 alt = center.zw;
    float C = fetch(pos.x, pos.y); // ( 0, 0)
    const vec4 kC = vec4( 4.0,  6.0,  5.0,  5.0) / 8.0;

    // Determine which of four types of pixels we are on.
    vec2 alternate = mod(alt, 2.0);

    vec4 Dvec = vec4(
        fetch(xCoord[1], yCoord[1]),  // (-1,-1)
        fetch(xCoord[1], yCoord[2]),  // (-1, 1)
        fetch(xCoord[2], yCoord[1]),  // ( 1,-1)
        fetch(xCoord[2], yCoord[2])); // ( 1, 1)

    vec4 PATTERN = (kC.xyz * C).xyzz;

    // Can also be a dot product with (1,1,1,1) on hardware where that is
    // specially optimized.
    // Equivalent to: D = Dvec[0] + Dvec[1] + Dvec[2] + Dvec[3];
    Dvec.xy += Dvec.zw;
    Dvec.x  += Dvec.y;

    vec4 value = vec4(
        fetch(center.x, yCoord[0]),   // ( 0,-2)
        fetch(center.x, yCoord[1]),   // ( 0,-1)
        fetch(xCoord[0], center.y),   // (-1, 0)
        fetch(xCoord[1], center.y));  // (-2, 0)

    vec4 temp = vec4(
        fetch(center.x, yCoord[3]),   // ( 0, 2)
        fetch(center.x, yCoord[2]),   // ( 0, 1)
        fetch(xCoord[3], center.y),   // ( 2, 0)
        fetch(xCoord[2], center.y));  // ( 1, 0)

    // Even the simplest compilers should be able to constant-fold these to
    // avoid the division.
    // Note that on scalar processors these constants force computation of some
    // identical products twice.
    const vec4 kA = vec4(-1.0, -1.5,  0.5, -1.0) / 8.0;
    const vec4 kB = vec4( 2.0,  0.0,  0.0,  4.0) / 8.0;
    const vec4 kD = vec4( 0.0,  2.0, -1.0, -1.0) / 8.0;

    // Conserve constant registers and take advantage of free swizzle on load
    #define kE (kA.xywz)
    #define kF (kB.xywz)

    value += temp;

    // There are five filter patterns (identity, cross, checker,
    // theta, phi).  Precompute the terms from all of them and then
    // use swizzles to assign to color channels.
    //
    // Channel   Matches
    //   x       cross   (e.g., EE G)
    //   y       checker (e.g., EE B)
    //   z       theta   (e.g., EO R)
    //   w       phi     (e.g., EO R)
    #define A (value[0])
    #define B (value[1])
    #define D (Dvec.x)
    #define E (value[2])
    #define F (value[3])

    // Avoid zero elements. On a scalar processor this saves two MADDs
    // and it has no effect on a vector processor.
    PATTERN.yzw += (kD.yz * D).xyy;

    PATTERN += (kA.xyz * A).xyzx + (kE.xyw * E).xyxz;
    PATTERN.xw  += kB.xw * B;
    PATTERN.xz  += kF.xz * F;

    vec3 res = (alternate.y < 1.0) ?
        ((alternate.x < 1.0) ?
            vec3(C, PATTERN.xy) :
            vec3(PATTERN.z, C, PATTERN.w)) :
        ((alternate.x < 1.0) ?
            vec3(PATTERN.w, C, PATTERN.z) :
            vec3(PATTERN.yx, C));
    return res;
}

float exposure(float v) {
      if (false)
      	 return v*2.0;
      // Simple linear function should be enough
      return lookup_1d(lut_exposure, v);
}

float tone(float v) {
      return lookup_1d(lut_tone, v);
}

float gamma(float v) {
      // Or maybe just do real gamma?
      return lookup_1d(lut_gamma, v);
}

const mat3 white1 = mat3( 0.99, 0.0, 0.0,
      	   	    	  0.0, 1.0, 0.0,
			  0.0, 0.0, 0.98 );
const mat3 white2 = mat3( 0.99, 0.01, 0.0,
      	   	    	  0.02, 0.98, 0.0,
 			  0.0, 0.01, 1.0 );
const mat3 final = mat3( 0.99, 0.0, 0.0,
      	   	    	  0.0, 1.0, 0.0,
			  0.0, 0.0, 0.98 );

// Processing steps are from paper:
// https://karaimer.github.io/camera-pipeline/paper/Karaimer_Brown_ECCV16.pdf
void main(void) {
     init_luts();
     
     // Denoise. FIXME: Paper suggests denoise after debayer.
     vec2 p = v_texcoord;
     vec4 f1 = vec4(0., 0., 0., 0.);
     vec4 f2 = vec4(0., 0., 0., 0.);
     vec4 f3 = vec4(0., 0., 0., 0.);
     vec3 res = debayer( f1, f2, f3 );


     // 6. White balance           (* \ mat * mat )
     // Transforms color from camera RGB into standard RGB
     res = res * white1 * white2;

     // 7. Hue/sat map             (3D LUT)
     // 6x6x3 lookup table, should not be needed most of the time. RGB->RGB
     res = lookup_3d(map_hue_sat, res);

     // 8. Exposure compensation   (1D LUT)
     // Could do 4096 LUT, also, but linear should be enough
     // 8-entry LUT should be enough.
     res = vec3(exposure(res.r), exposure(res.g), exposure(res.b));
     
     // 9. Color manipulation      (3D LUT)
     // 36x16x16 LUT is suggested
     res = lookup_3d(map_color, res);
     
     // 10. Tone curve             (1D LUT)
     // 248 entries LUT is suggested
     res = vec3(tone(res.r), tone(res.g), tone(res.b));
    
     // 11. Final color space      (* mat)
     res = res * final;

     // 12. Gamma curve             (1D LUT)
     // 4096 entries suggested
     res = vec3(gamma(res.r), gamma(res.g), gamma(res.b));

     gl_FragColor.rgb = res;
     gl_FragColor.a = 1.0;
}
