#!/usr/bin/python3

import os

class Gst:
    def __init__(m):
        m.width = 1920
        m.height = 1080
    
    def launch(m, cmd):
        os.system("time gst-launch-1.0 " + cmd)

    def testsrc(m):
        c = "videotestsrc num-buffers=150 ! video/x-raw,width=%d,height=%d " % (m.width, m.height)
        return c
    
    def prep(m):
        c = m.testsrc()
        c += " ! rgb2bayer ! video/x-bayer,format=bggr ! filesink location=bayer.raw"
        m.launch(c)
        os.system("ls -alh bayer.raw")

    def input(m):
        c = "filesrc location=bayer.raw ! rawvideoparse use-sink-caps=false width=%d height=%d format=rgba " % (m.width, m.height)

    # Something is wrong with this one; only works with small sizes, data are corrupted.
    def debayer_cpu(m):
        c = "filesrc location=bayer.raw ! video/x-bayer,format=bggr,width=%d,height=%d,framerate=30/1 ! bayer2rgb ! videoconvert ! autovideosink " % (m.width, m.height)
        m.launch(c)

    def test_cpu(m):
        c = m.testsrc()
        c += " ! rgb2bayer ! video/x-bayer,format=bggr ! bayer2rgb ! videoconvert ! autovideosink "
        m.launch(c)

    def glshader(m):
        shader = ""
        return ' ! glshader fragment="\"%s\""' % shader

    def debayer_gpu(m):
        c = 'location=bayer.raw ! rawvideoparse use-sink-caps=false width=%d height=%d format=rgba ! glupload ' % (m.width, m.height)
        c += m.glshader()
        c += ' !  glimagesink'
        #print(c)
        m.launch(c)

g = Gst()
#g.prep()
#g.debayer_cpu()
g.debayer_gpu()
#g.test_cpu()
