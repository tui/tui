//#version 100
#ifdef GL_ES
precision mediump float;
#endif
varying vec2 v_texcoord;
uniform sampler2D tex;
uniform float time;
uniform float width;
uniform float height;

void main () {
  vec2 pos = v_texcoord;
  float sx = 1.0/width;
  float sy = 1.0/height;
  
  vec4 four00 = texture2D( tex, pos );
  vec4 four10 = texture2D( tex, vec2(pos.x+sx, pos.y) );
  vec4 four01 = texture2D( tex, vec2(pos.x   , pos.y+sy) );
  vec4 four11 = texture2D( tex, vec2(pos.x+sx, pos.y+sy) );

  vec4 res = vec4(0.0, 0.0, 0.0, 0.0);

  if (pos.x / sx > 149.90 && pos.x / sx < 151.10) {
     res = vec4(1.0, 1.0, 1.0, 1.0);
  }
  if (pos.y * sy < 150.0 || pos.y * sy > 400.0) {
     //res = vec4(0.5, 0.5, 1.0, 1.0);
  }

  gl_FragColor = res;
}
