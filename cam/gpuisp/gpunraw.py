#!/usr/bin/python3

import time
import subprocess

class Tools:
    def dng_to_jpeg(m, base):
        # 1.42 sec
        subprocess.run(['dcraw',
                        '-w',      # -w Use camera white balance
                        '+M',      # +M use embedded color matrix
                        '-H', '2', # -H 2 Recover highlights by blending them
                        '-o', '1', # -o 1 Output in sRGB colorspace
                        '-q', '0', # -q 0 Debayer with fast bi-linear interpolation
                        '-f',      # -f Interpolate RGGB as four colors
                        '-T', base+".dng"])  # -T Output TIFF
        # 0.41 sec
        #subprocess.run(['convert', base+'.tiff', base+'.jpeg'])

    def dng_to_netpbm(m, base):
        # 0.64 sec
        # time dcraw -E -c delme.in/IMG20240917130822.dng > delme.pgm
        # gst-launch-1.0 filesrc location=delme.pgm ! rawvideoparse format=gray8 width=3120 height=4208 framerate=1/1 ! pngenc ! filesink location=delme.png
        
        # 0.77 sec
        #dcraw -D -4 -o 0 -c delme.in/IMG20240917130822.dng > image.raw
        # (bad) gst-launch-1.0 filesrc location=image.raw ! rawvideoparse format=gray16_le width=<WIDTH> height=<HEIGHT> ! videoconvert ! autovideosink

        # 3.14 sec
        # time convert delme.in/IMG20240917130822.dng delme.pgm
        # (bad) gst-launch-1.0 filesrc location=delme.pgm ! rawvideoparse format=gray8 width=4208 height=3120 ! videoconvert ! autovideosink
        pass

t = Tools()
t.dng_to_jpeg("delme.in/IMG20240917130822")
