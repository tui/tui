#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define WIDTH 512
#define HEIGHT 512

GLuint computeProgram;
GLuint inputBuffer, outputBuffer;

void initOpenGL() {
    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 1);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "OpenGL Compute Shader Example", NULL, NULL);
    if (!window) {
        fprintf(stderr, "Failed to create GLFW window\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    printf("glewInit\n");
    glfwMakeContextCurrent(window);
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        glfwDestroyWindow(window);
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    printf("debug out\n");

    glEnable(GL_DEBUG_OUTPUT);
#if 0    
    glDebugMessageCallback([](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
        fprintf(stderr, "OpenGL Debug Message: %s\n", message);
    }, 0);
#endif

    printf("Buffers\n");
    glGenBuffers(1, &inputBuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, inputBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, WIDTH * HEIGHT * sizeof(float), NULL, GL_DYNAMIC_COPY);

    glGenBuffers(1, &outputBuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, outputBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, WIDTH * HEIGHT * sizeof(float), NULL, GL_DYNAMIC_COPY);

    printf("Fill: bind\n");
    // Initialize data in the input buffer
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, inputBuffer);
    float* inputData = (float*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
    printf("Fill: array\n");
#if 0    
    for (int i = 0; i < WIDTH * HEIGHT; ++i) {
        inputData[i] = i;
    }
#endif
    printf("Fill: unmap\n");
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    printf("Create\n");
    // Create and compile compute shader
    const char* computeShaderSource = R"(
        #version 430

        layout(local_size_x = 16, local_size_y = 16) in;

        buffer InputBuffer {
            float inputData[];
        };

        buffer OutputBuffer {
            float outputData[];
        };

        void main() {
            uvec2 storePos = gl_GlobalInvocationID.xy;
            uint index = storePos.x + WIDTH * storePos.y;
            outputData[index] = inputData[index] * inputData[index];
        }
    )";

    GLuint computeShader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(computeShader, 1, &computeShaderSource, NULL);
    glCompileShader(computeShader);

    GLint compileStatus;
    glGetShaderiv(computeShader, GL_COMPILE_STATUS, &compileStatus);
    if (compileStatus != GL_TRUE) {
        fprintf(stderr, "Failed to compile compute shader\n");
        char log[1024];
        GLsizei length;
        glGetShaderInfoLog(computeShader, sizeof(log), &length, log);
        fprintf(stderr, "Compile log:\n%s\n", log);
        exit(EXIT_FAILURE);
    }

    // Create and link the compute program
    computeProgram = glCreateProgram();
    glAttachShader(computeProgram, computeShader);
    glLinkProgram(computeProgram);

    GLint linkStatus;
    glGetProgramiv(computeProgram, GL_LINK_STATUS, &linkStatus);
    if (linkStatus != GL_TRUE) {
        fprintf(stderr, "Failed to link compute program\n");
        char log[1024];
        GLsizei length;
        glGetProgramInfoLog(computeProgram, sizeof(log), &length, log);
        fprintf(stderr, "Link log:\n%s\n", log);
        exit(EXIT_FAILURE);
    }

    // Use the program and bind the buffers
    glUseProgram(computeProgram);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, inputBuffer);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, outputBuffer);

    // Execute the compute shader
    glDispatchCompute((GLuint)WIDTH / 16, (GLuint)HEIGHT / 16, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    // Verify results (for simplicity, only printing a few values)
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, outputBuffer);
    float* outputData = (float*)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
    for (int i = 0; i < 5; ++i) {
        printf("Result[%d]: %f\n", i, outputData[i]);
    }
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    // Cleanup
    glDeleteBuffers(1, &inputBuffer);
    glDeleteBuffers(1, &outputBuffer);
    glDeleteShader(computeShader);
    glDeleteProgram(computeProgram);

    glfwDestroyWindow(window);
    glfwTerminate();
}

int main() {
    initOpenGL();
    return 0;
}
