#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*
 * x86 has 256-bit vectors; 512-bit is optional and eats extra power
 * Arm has Neon.

 * gcc rgba.c -march=native -msse -mavx -O3 && time ./a.out
 */

#define HEIGHT 300
#define WIDTH 300

#define barrier() asm volatile("": : : "memory")
static char src[WIDTH*HEIGHT*4], dst[WIDTH*HEIGHT*3];

#define V 16
typedef unsigned char vu8 __attribute__ ((vector_size (V)));

void vector(uint8_t *dst, const uint8_t *src)
{
	vu8 *s = (vu8 *)src;
	vu8 *d = (vu8 *)dst;
        for (int x = 0; x < WIDTH*HEIGHT*4; x+=(V*4)) {
		vu8 s1 = *s++;
		vu8 s2 = *s++;
		vu8 s3 = *s++;
		vu8 s4 = *s++;
		/*         0123 4567 */
		/*         RGBa RGBa */
 		/* d1 =         */
		/*              0123 4567 */
		/* d2 =    RGBa -GBa RG-a */
		/*                   0123 4567 */
		/* d3 =                B  RGB */
		vu8 m1 = {0, 1, 2,   4, 5, 6,     8, 9, 10,  12, 13, 14,
			16, 17, 18,  20 };
		vu8 d1 = __builtin_shuffle(s1, s2, m1);
		vu8 m2 = {              5, 6,     8, 9, 10,  12, 13, 14,
			16, 17, 18,  20, 21, 22,  24,25};
		vu8 d2 = __builtin_shuffle(s2, s3, m2);
		vu8 m3 = {                              10,  12, 13, 14,
			16, 17, 18,  20, 21, 22,  24,25,26,  28, 29, 30};
		vu8 d3 = __builtin_shuffle(s2, s3, m3);
		*d++ = d1;
		*d++ = d2;
		*d++ = d3;
        }
}

void main(void)
{
	int i;
	for (i=0; i<WIDTH*HEIGHT*4; ) {
		src[i++] = 'R';
		src[i++] = 'R';
		src[i++] = 'R';
		src[i++] = 'A';
	}
	barrier();
	int mode = 2;
	int it = 20000;
	switch (mode) {
	case 0: /* 0.41sec on x220, 1.38 on pp */
		printf("Just memcpy\n");		
		for (i=0; i<it; i++) {
			memcpy(dst, src, WIDTH*HEIGHT*3);
			barrier();
		}
		break;
	case 1: /* 0.62sec on x220, 2.59 on pp */
		printf("Simple\n");
		for (i=0; i<it; i++) {
			int k=0;
			for (int j=0; j<WIDTH*HEIGHT*4; ) {
				dst[k++] = src[j++];
				dst[k++] = src[j++];
				dst[k++] = src[j++];
				j++;
			}
			barrier();
		}
		break;
	case 2: /* .45sec on x220, 2.4 on pp */
		printf("Vector\n");
		for (i=0; i<it; i++) {
			vector(dst, src);
			barrier();
		}
		break;
	default:
		printf("This is not good\n");
	}
	
	barrier();
	{
		unsigned long sum;
		for (i=0; i<WIDTH*HEIGHT*3; i++) {
			if (dst[i] != 'R') {
				printf("Bad\n");
				break;
			}
		}
	}
}
