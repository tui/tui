#!/usr/bin/env python3
# Thanks to https://brettviren.github.io/pygst-tutorial-org/pygst-tutorial.html#sec-9
# Needs to be ran as GDK_SYNCHRONIZE=1 GDK_BACKEND=x11 ./cam.py

import sys, os
import gi
from gi.repository import Gst, GLib, GObject

import hwconfig

class MainWin:
    def __init__(m):
        m.hw = hwconfig.hw_probe()
        m.hw.init()

        m.pipeline("capture")

    def pipeline(m, s):
        # = m.test_pipeline()
        m.hw.mode = s
        m.player = Gst.parse_launch(m.hw.gl_pipeline())

GObject.threads_init()
Gst.init(None)
MainWin()
#mainclass = CLI_Main()
#thread.start_new_thread(mainclass.start, ())
loop = GLib.MainLoop()
loop.run()


