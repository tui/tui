#!/usr/bin/python3

# apt install gstreamer1.0-plugins-base-apps

import time
import os

class RecParam:
    def __init__(m, w, h, format):
        m.height = h
        m.width = w
        m.format = format

class Bench:
    num_buffers = 100

    def __init__(m):
        m.results = ""
    
    def run_timed(m, s):
        t = time.time()
        print("Running ", s)
        os.system(s)
        dt = time.time() - t
        print("... ", time.time() - t, " sec")
        return dt

    def run_gst(m, p, s):
        if p.format == "rggb":
            return m.run_timed("gst-launch-1.0 videotestsrc pattern=snow num-buffers=%d ! video/x-bayer,width=%d,height=%d %s ! fakesink" % (m.num_buffers, p.width, p.height, s))
        else:
            return m.run_timed("gst-launch-1.0 videotestsrc pattern=snow num-buffers=%d ! video/x-raw,width=%d,height=%d,format=%s %s ! fakesink" % (m.num_buffers, p.width, p.height, p.format, s))

    def run_bench_param(m, name, s1, s2, p):
        dt1 = m.run_gst(p, s1)
        dt2 = m.run_gst(p, s2)
        spf = (dt2-dt1)/m.num_buffers
        spm = spf / (p.width * p.height / 1000000.)
        r = name
        r += " %d x %d " % (p.width, p.height)
        #r += " %.2f -> %.2f, " % (dt1, dt2)
        r += "%.1f msec/frame, " % (spf * 1000.)
        r += "%.1f msec/MP, " % (spm * 1000.)
        r += "%.1f fps, " % (1 / spf)
        r += "%.1f MP/s" % (1 / spm)
        print(r)
        m.results += r + "\n"
    
    def run_bench(m, name, s1, s2):
        if False:
            p = RecParam(160, 120, "YUY2")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(320, 240, "YUY2")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(640, 480, "YUY2")
            m.run_bench_param(name, s1, s2, p)
        if False:
            p = RecParam(1280, 720, "YUY2")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(1920, 1080, "YUY2")
            m.run_bench_param(name, s1, s2, p)
        if True:
            p = RecParam(1052, 780, "YUY2")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(1920, 1080, "YUY2")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(1920, 1440, "YUY2")
            m.run_bench_param(name, s1, s2, p)

        m.results += "\n"

    def run_bayer(m, name, s1, s2):
        if True:
            p = RecParam(1024, 768, "rggb")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(1052, 780, "rggb")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(1400, 1050, "rggb")
            m.run_bench_param(name, s1, s2, p)
            p = RecParam(1600, 1200, "rggb")
            m.run_bench_param(name, s1, s2, p)

        m.results += "\n"

    def summary(m):
        print("--------------------------------------------------------------------------------")
        print("Results summary:")
        print(m.results)

    def run_h264(m, sp):
        # ultrafast (1) – ultrafast
        # superfast (2) – superfast
        # veryfast (3) – veryfast
        # faster (4) – faster
        # fast (5) – fast
        # medium (6) – medium
        # slow (7) – slow
        # slower (8) – slower
        # veryslow (9) – veryslow
        m.run_bench("H.264 (x) encoding (%s)" % sp, "", "! videoconvert ! x264enc bitrate=3072 speed-preset=" + sp)
        
            
    def run_encoders(m):
        m.run_bench("JPEG encoding", "", "! jpegenc")
        # 0.4 MP/s
        #m.run_bench("MPEG2 encoding", "", "! videoconvert ! mpeg2enc" )
        # MPEG4 is multithreaded. Wow.
        m.run_bench("MPEG4 encoding", "", "! videoconvert ! avenc_mpeg4" )
        m.run_bench("H.264 (x) encoding", "", "! videoconvert ! x264enc" )
        m.run_h264("ultrafast")
        m.run_h264("superfast")
        m.run_h264("veryfast")
        m.run_h264("faster")
        #m.run_h264("medium")
        #m.run_h264("slow")
        #m.run_h264("slower")
        #m.run_h264("veryslow")
        #m.run_bench("H.265 (x) encoding (slow)", "", "! videoconvert ! video/x-raw,format=I420 ! x265enc bitrate=3072 speed-preset=slow" )
        #m.run_bench("H.265 (x) encoding (ultrafast)", "", "! videoconvert ! video/x-raw,format=I420 ! x265enc bitrate=3072 speed-preset=ultrafast" )
        # 0.2 MP/s
        #m.run_bench("VP8 encoding", "", "! videoconvert ! vp8enc" )
        # 0.0 MP/s -- 26268.4 msec/MP
        #m.run_bench("VP9 encoding", "", "! videoconvert ! vp9enc" )
        # Cedrus supports VP6, H264, VP8, H265, VP9
        
        #m.run_bench("JPEG decoding", "! jpegenc", "! jpegenc ! jpegdec" )
        m.summary()

    def run_threading(m):
        m.run_bench("JPEG encoding", "", "! jpegenc")
        m.run_bench("JPEG encoding threaded", "", "! queue ! jpegenc")

        m.run_bench("JPEG encode/decoding threaded", "", "! queue ! jpegenc ! jpegdec" )
        m.summary()

    def run_preview(m):
        # Gives nonsensical results.
        #m.run_bench("JPEG parsing", "! jpegenc", "! jpegenc ! jpegparse")
        # ! videoscale method=0 envelope=1 ! video/x-raw,width=202,height=360
        m.run_bench("Preview from JPEG", "! jpegenc", "! jpegenc ! videoflip method=clockwise")

        m.summary()

    def run_debayer(m):
        m.run_bayer("Debayer", "", "! bayer2rgb")
        m.run_bayer("Debayer + x264 ultrafast", "", "! bayer2rgb ! videoconvert ! x264enc bitrate=3072 speed-preset=ultrafast")
        m.run_bayer("Debayer + x264 superfast", "", "! bayer2rgb ! videoconvert ! x264enc bitrate=3072 speed-preset=superfast")
        m.run_bayer("Debayer + q + x264 ultrafast", "", "! bayer2rgb ! videoconvert ! queue ! x264enc bitrate=3072 speed-preset=ultrafast")
        m.run_bayer("Debayer + q + x264 superfast", "", "! bayer2rgb ! videoconvert ! queue ! x264enc bitrate=3072 speed-preset=superfast")
	# Theora is slow, cca 2MP/sec
	# m.run_bayer("Debayer + q + theoraenc", "", "! bayer2rgb ! videoconvert ! queue ! theoraenc bitrate=3072")
        m.summary()

    def run(m):
        m.run_encoders()
        #m.run_debayer()

b = Bench()
b.run()
    
