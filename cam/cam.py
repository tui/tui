#!/usr/bin/env python3
# Thanks to https://brettviren.github.io/pygst-tutorial-org/pygst-tutorial.html#sec-9
# Needs to be ran as GDK_SYNCHRONIZE=1 GDK_BACKEND=x11 ./cam.py

import sys, os
import gi
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GstVideo', '1.0')
from gi.repository import Gst, GObject, Gtk
from gi.repository import GstVideo  # for sink.set_window_handle()
from gi.repository import GdkX11    # for window.get_xid()

import hwconfig

class MainWin:
    def __init__(m):
        window = Gtk.Window(type = Gtk.WindowType.TOPLEVEL)
        window.set_title("Unicsy Camera")
        # PinePhone is 720x1440, scaled to 2x.
        window.set_default_size(360, 680)
        window.connect("destroy", Gtk.main_quit, "WM destroy")
        vbox = Gtk.VBox()
        window.add(vbox)
        m.movie_window = Gtk.DrawingArea()
        vbox.add(m.movie_window)
        hbox = Gtk.HBox()
        vbox.pack_start(hbox, False, False, 0)
        hbox.set_border_width(10)
        m.label = Gtk.Label(label = "Starting up...")
        hbox.pack_start(m.label, False, False, 0)

        m.button4 = Gtk.Button(label = "Prev")
        m.button4.connect("clicked", m.preview)
        hbox.pack_start(m.button4, False, False, 0)
        m.button5 = Gtk.Button(label = "Cap")
        m.button5.connect("clicked", m.capture)
        hbox.pack_start(m.button5, False, False, 0)
        m.button = Gtk.Button(label = "Rec")
        m.button.connect("clicked", m.record)
        hbox.pack_start(m.button, False, False, 0)
        m.button2 = Gtk.Button(label = "Quit")
        m.button2.connect("clicked", m.exit)
        hbox.pack_start(m.button2, False, False, 0)
        m.button3 = Gtk.Button(label = "Test")
        m.button3.connect("clicked", m.test)
        hbox.pack_start(m.button3, False, False, 0)
        hbox.add(Gtk.Label())
        window.show_all()
        m.xid = m.movie_window.get_window().get_xid()
        m.autopreview = False

        m.hw = hwconfig.hw_probe()
        m.hw.init()

        m.pipeline("preview")
        m.start_pipeline()

    def test_pipeline(m):
        pipeline = Gst.Pipeline()
        src = Gst.ElementFactory.make("videotestsrc", None)
        sink = Gst.ElementFactory.make("ximagesink", None)
        pipeline.add(src, sink)
        src.link(sink)
        return pipeline

    def pipeline(m, s):
        # = m.test_pipeline()
        m.hw.mode = s
        m.player = Gst.parse_launch(m.hw.gl_pipeline())

#    def test_pipeline(m):
        # Thanks to https://lubosz.wordpress.com/2014/05/27/gstreamer-overlay-opengl-sink-example-in-python-3/
 #       sink.set_window_handle(m.xid)

    def start_pipeline(m):
        bus = m.player.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message", m.on_message)
        bus.connect("sync-message::element", m.on_sync_message)

        m.player.set_state(Gst.State.PLAYING)
        m.label.set_label("ok  ")

    def stop_pipeline(m):
        m.label.set_label("Busy...")
        m.player.set_state(Gst.State.NULL)
        m.player = None

    def test(m, w):
        m.stop_pipeline()
        m.pipeline("test")
        m.start_pipeline()

    def record(m, w):
        m.stop_pipeline()
        m.pipeline("record")
        m.start_pipeline()

    def preview(m, w):
        m.stop_pipeline()
        m.pipeline("preview")
        m.start_pipeline()

    def capture(m, w):
        m.autopreview = True
        m.stop_pipeline()
        m.pipeline("capture")
        m.start_pipeline()

    def exit(m, widget, data=None):
        Gtk.main_quit()

    def on_message(m, bus, message):
        t = message.type
        if t == Gst.MessageType.EOS:
            m.player.set_state(Gst.State.NULL)
            m.label.set_label("Done/Ready")
            m.hw.on_finish()
            if m.autopreview:
                m.autopreview = False
                m.pipeline("preview")
                m.start_pipeline()
        elif t == Gst.MessageType.ERROR:
            err, debug = message.parse_error()
            msg = "Error: %s" % err
            print(msg, debug)
            m.player.set_state(Gst.State.NULL)
            m.label.set_label(msg)

    def on_sync_message(m, bus, message):
        struct = message.get_structure()
        if not struct:
            return
        message_name = struct.get_name()
        #print("On sync message", message, message_name)
        if message_name == 'prepare-window-handle':
            imagesink = message.src
            imagesink.set_property("force-aspect-ratio", True)
            imagesink.set_window_handle(m.xid)

Gst.init(None)
MainWin()
#GObject.threads_init()
Gtk.main()
