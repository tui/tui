linux, I have movie with german audio, I want to automatically generate subtitles in english

To generate English subtitles for a movie with German audio on Linux,
you can use tools like Whisper (by OpenAI), which is excellent for
automatic speech recognition (ASR) and can handle translations. Below
is a step-by-step guide:

1. Install Dependencies
First, ensure you have Python installed and the necessary tools for Whisper:

sudo apt update
sudo apt install ffmpeg python3-pip
pip3 install openai-whisper

2. Extract Audio from the Movie
Whisper works on audio files, so extract the audio track from your movie using ffmpeg:

ffmpeg -i your_movie.mp4 -vn -acodec copy movie_audio.aac
Alternatively, convert it to a supported format like WAV or MP3:

ffmpeg -i your_movie.mp4 -vn -ac 1 -ar 16000 movie_audio.wav


3. Use Whisper for Subtitles
Run Whisper to generate English subtitles:

whisper movie_audio.wav --model medium --language de --task translate --output_format srt
--model medium: Choose a model size. You can also try base, small, or large.
--language de: Specifies the source language (German).
--task translate: Translates from German to English.
--output_format srt: Generates subtitles in the .srt format.
This will create a file like movie_audio.srt.

4. Combine Subtitles with the Movie
You can merge the generated subtitles back into the movie using ffmpeg:

bash
￼Copy code
ffmpeg -i your_movie.mp4 -i movie_audio.srt -c:v copy -c:a copy -c:s mov_text output_with_subtitles.mp4
linux, I have movie with german audio, I want to automatically generate subtitles in english
ChatGPT said:
ChatGPT
To generate English subtitles for a movie with German audio on Linux, you can use tools like Whisper (by OpenAI), which is excellent for automatic speech recognition (ASR) and can handle translations. Below is a step-by-step guide:

1. Install Dependencies
First, ensure you have Python installed and the necessary tools for Whisper:

bash
￼Copy code
sudo apt update
sudo apt install ffmpeg python3-pip
pip install openai-whisper
2. Extract Audio from the Movie
Whisper works on audio files, so extract the audio track from your movie using ffmpeg ... Alternatively, convert it to a supported format like WAV or MP3:

ffmpeg -i input.mp4 -vn -ac 1 -ar 16000 movie_audio.wav

3. Use Whisper for Subtitles
Run Whisper to generate English subtitles:

bash
￼Copy code
whisper movie_audio.wav --model medium --language de --task translate --output_format srt
--model medium: Choose a model size. You can also try base, small, or large.
--language de: Specifies the source language (German).
--task translate: Translates from German to English.
--output_format srt: Generates subtitles in the .srt format.
This will create a file like movie_audio.srt.

4. Combine Subtitles with the Movie
You can merge the generated subtitles back into the movie using ffmpeg:


ffmpeg -i your_movie.mp4 -i movie_audio.srt -c:v copy -c:a copy -c:s mov_text output_with_subtitles.mp4

--

pip3 install --break-system-packages openai-whisper
arecord test.wav

ffmpeg -i input.mp4 -vn -ac 1 -ar 16000 input.wav

# op6.
# medium -- cca 5GB. small -- cca 1.5GB.
# 23 sec long test, small -- 2m18 real, 4m50 user -- medium -- 5m20 real, 15m55 user
# small -> 6x slower than real time
# medium -> 16x slower than real time
# hmm. time should be 600min
# 

ffmpeg -i input.mp4 -vn -ac 1 -ar 16000 input.wav
# idealne ve screen.
time whisper input.wav --model small --language de --task translate --output_format srt --threads 8 > input.srt

..then convert with python

..mplayer 38c3-*Och* -sub delme.srt

