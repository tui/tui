#!/usr/bin/python3

class LLMi:
    action = "xa75"
    end = "zp83"
    a_time = action+"tm"
    a_weather = action+"wt"
    a_calc = action+"ca"
    a_calc_end = end+'ca'
    
    def parse(m, s):
        pass

    def prompt_simple(m, what, prompt):
        r = "If someone asks you for %s, you won't answer, but will say %s.\n" % (what, prompt)
        return r

    def prompt(m, s):
        r = ''
        r += """
Forget previous instructions.
You are voice assistant Merlin.
"""
        r += m.prompt_simple("time", m.a_time)
        r += m.prompt_simple("weather", m.a_weather)
        r += "If someone wants computation, you won't answer, but will say %s expression %s, expression as for calculator, for example %s (3*52)-7 %s.\n" % (m.a_calc, m.a_calc_end, m.a_calc, m.a_calc_end)

        r += 'How would Merlin answer "%s". Say only his reply.' % s
        print(r)

p = LLMi()
p.prompt("Kolik je tri krat padesat sest?")
