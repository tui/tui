#!/usr/bin/python3

from llama_cpp import Llama

# Initialize the model
model_path = "llama.gguf"
llm = Llama(model_path=model_path, logits_all=True)

if True:
    # Define user input and classification options
    user_input = "What time is it"
    user_input = "Will it snow tommorow"
    user_input = "Please call Merlin"
    user_input = "Wake me up at sunrise"
    #user_input = "Jump to warp speed"
    options = {
        "a": "asking for time",
        "b": "asking for a phone call",
        "c": "asking for weather",
        "d": "asking for computation",
        "e": "asking to set an alarm", 
#        "z": "something else"
    }

if False:
    # Define user input and classification options
    #user_input = "The capital of France"
    #user_input = "Big city where people speak German"
    user_input = "City of a Hundred Towers"
    #user_input = "Jump to warp speed"
    options = {
        "a": "Berlin",
        "b": "Prague",
        "c": "Paris",
        "d": "Brno",    
        "z": "something else"
    }

# Function to get the total probability of generating each option
def get_option_probability(input_text, option_text):
    # Construct the prompt by combining the user input and the option text
    prompt = f"'{input_text}' can be summarized as '{option_text}'."
    
    # Use `logits` to obtain per-token probabilities for the completion
    response = llm(prompt=prompt, max_tokens=1, temperature=0, echo=True, logprobs=1)

#    print(response['choices'][0]['logprobs'])
    
    # Sum the log probabilities of the tokens in the option_text part
    log_probs = response['choices'][0]['logprobs']['token_logprobs']
    print(log_probs)
    
    # Calculate the cumulative log probability for the whole option completion
    # Probably incorrect -- tokens != words
    #total_log_prob = sum(log_probs[-len(option_text.split()):])
    total_log_prob = sum(log_probs[1:])
    
    return total_log_prob

# Score each option and find the best one based on cumulative log probabilities
log_probs = {label: get_option_probability(user_input, text) for label, text in options.items()}
best_option = max(log_probs, key=log_probs.get)

# Print results
print()
print("Log probabilities:", log_probs)
print("Best classification:", options[best_option])
