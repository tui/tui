import re

def fix_subtitle_format(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as infile, open(output_file, 'w', encoding='utf-8') as outfile:
        content = infile.readlines()
        seq_num = 1
        for line in content:
            # Match lines with timestamps like [00:00.000 --> 00:22.000]
            match = re.match(r'^\[(\d+):(\d+\.\d+) --> (\d+):(\d+\.\d+)\](.*)$', line)
            if match:
                # Convert timestamps to SRT format: 00:00:00,000 --> 00:00:22,000
                start_min, start_sec, end_min, end_sec, rest = match.groups()
                start_time = f"00:{int(start_min):02}:{float(start_sec):06.3f}".replace('.', ',')
                end_time = f"00:{int(end_min):02}:{float(end_sec):06.3f}".replace('.', ',')
                # Write sequence number and timestamp
                outfile.write(f"{seq_num}\n")
                outfile.write(f"{start_time} --> {end_time}\n")
                outfile.write(f"{rest}\n")
                seq_num += 1
            else:
                # Write non-timestamp lines as they are
                outfile.write(line.strip() + '\n')
        print(f"Formatted subtitles saved to: {output_file}")

# Usage
fix_subtitle_format('delme.srt', 'fixed.srt')
