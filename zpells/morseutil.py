# GPLv3
# Copyright 2011,2013 Pavel Machek

class MorseUtil:
  letters = [('A',".-"),   ('B',"-..."), ('C',"-.-."), ('D',"-.."), ('E',"."),
             ('F',"..-."), ('G',"--."),  ('H',"...."), ('I',".."),  ('J',".---"),
             ('K',"-.-"),  ('L',".-.."), ('M',"--"),   ('N',"-."),  ('O',"---"),
             ('P',".--."), ('Q',"--.-"), ('R',".-."),  ('S',"..."), ('T',"-"),
             ('U',"..-"),  ('V',"...-"), ('W',".--"),  ('X',"-..-"),('Y',"-.--"),
             ('Z',"--.."), ('CH',"----")] 

  spelling = [('A','alpha'), ('B','bravo'), ('C','charlie'), ('D','delta'), ('E',"echo"),
             ('F',"foxtrot"), ('G',"golf"),  ('H',"hotel"), ('I',"india"),  ('J',"juliet"),
             ('K',"kilo"),  ('L',"lima"), ('M',"mike"),   ('N',"november"),  ('O',"oskar"),
             ('P',"papa"), ('Q',"quebec"), ('R',"romeo"),  ('S',"sierra"), ('T',"tango"),
             ('U',"uniform"),  ('V',"victor"), ('W',"whiskey"),  ('X',"x-ray"),('Y',"yankee"),
             ('Z',"zulu"),   ('.',"dit"),   ('-',"dah"),   ('CH','chronos')]

  def __init__(s):
    s.init_morse()

  def init_morse(s):
    s.morse = {}
    s.morse_back = {}
    for letter, code in s.letters:
      s.morse[code] = letter
      s.morse_back[letter] = code

    s.spell = {}
    for letter, code in s.spelling:
      s.spell[letter] = code

  def spell_letter(s, x):
    if x in s.spell:
      return s.spell[x]
    return "unknown"

  def spell_word(s, x):
    return ' '.join(map(s.spell_letter, x))

  def spell_morse(s, x):
    res = ""
    for c in x:
      if c == ".":
        res += " di"
      else:
        res += " dah"
    if x[-1] == '.':
      res += "t"
    res += "..."
    return res
