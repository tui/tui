import base
import os
import random
import android
import sys
import subprocess
import string
import copy

global intf

class Music(base.Background):
  def __init__(m):
    m.music = m.load_list("find /data/mp3/ /data/mp3.local/ /media/mmc1/")
    m.playlist = m.music
    m.autoplay = 0
    m.playing = 0
    m.paused = 1
    m.timer = 0

  def start(s):
    intf.droid.mediaPlay("hello.wav")

  def volume(s, up):
    x = '9'
    if up: x = '0'
    intf.droid.player.stdin.write(bytearray(x+x+x, "ascii"))
    intf.droid.player.stdin.flush()

  def unpause(m):
    print("unpause")
    if m.paused:
      m.talk_now()
      if m.playing:
        intf.droid.mediaPlayStart()
      else:
        m.play_next()
    else: intf.droid.mediaPlayPause()
    m.paused = not m.paused
    return

  def load_list(s, p):
    print("loading ", p)
    list = []
    f = os.popen(p)
    while 1:
      try:
        l = f.readline()[:-1]
      except UnicodeDecodeError:
        continue
      if not l: break
      #print l
      list.append(l)

    random.shuffle( list )
    return list

  def play_next(s):
      print("play next")
      # s.talk_now()
      n = s.playlist.pop()
      intf.droid.mediaPlay(n)
      s.autoplay = 1
      # s.dec.say("Player failed.")
      s.playing = 1

  def play_note(s):
    n = data.stack[-1]
    stat = os.stat(n)
    s.say_ago(time.time() - stat.st_mtime)
    intf.droid.mediaPlay(n)

  def run(m):
    if m.autoplay and intf.droid.playback_finished():
      m.play_next()

