#!/usr/bin/python
# Copyright 2013 Pavel Machek <pavel@ucw.cz>, GPLv3
#
# You probably want to killall headset-control
#
# This only works well when mixer is not outputting too much noise.

import sys
import time
import getopt
import alsaaudio


def putc(c):
    sys.stdout.write(c)
    sys.stdout.flush()

class AlsaLoop:
    def set_format(self, inp):
        inp.setchannels(1)
        inp.setrate(8000)
        inp.setformat(alsaaudio.PCM_FORMAT_U8)

        inp.setperiodsize(160)

    def init(self):
        card = 'default'

        self.inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, card)
        self.outp = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, alsaaudio.PCM_NONBLOCK)

        self.set_format(self.inp)
        self.set_format(self.outp)

    def loop(self):
      loops = 1000000

      while loops > 0:
        loops -= 1
        # Read data from device
        l, data = self.inp.read()
      
        if not l:
            print "???"
            continue

        bytes = bytearray(data)
        energy = 0.0
        b = bytes[0]
        for a in bytes:
            energy += (a-b)*(a-b)
            b = a

        if 1:
            self.decode(energy)

        if 0:
            if energy < 6:
                putc("!")
            elif energy < 30:
                putc("X")
            elif energy < 60:
                putc("o")
            elif energy < 90:
                putc("_")
            else:
                putc(",")

        #self.outp.write(data)

class Decoder(AlsaLoop):
    last = " "
    spaces = 0
    marks = 0
    
    def decode(s, energy):
        max_dot = 16

        now = " "
        if energy < 60: now = "-"
        s.now = now

        if s.now == " ": s.spaces += 1
        else: s.marks += 1

        if s.now == s.last:
            if s.now == " " and s.spaces > 5:
                if s.marks >= max_dot and s.marks<40:
                    putc("-")
                    s.marks = 0
                elif s.marks > 0 and s.marks < max_dot:
                    putc(".")
                    s.marks = 0
            if s.now == "-" and s.marks > 4:
                spaces = 0
            if s.now == "-" and s.marks > 80:
                putc("#")
                s.marks = 40
            if s.spaces > 50:
                putc("/")
                s.spaces = 10
        else:
            if now == "-":
                if s.spaces < 3:
                    s.marks += s.spaces
                s.spaces = 0

        s.last = s.now

dec = Decoder()
dec.init()
dec.loop()

