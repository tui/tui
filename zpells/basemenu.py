#!/usr/bin/python
# Copyright 2013,2014 Pavel Machek <pavel@ucw.cz>
# GPLv3

import textui
import sys
import music
import time
import random
import string
import formulate
import astro
import position
import subprocess
import signal
import os
import util
from base import *

global intf

text = textui.TextUI()
text.adjust_termios()

class BaseAction:
    def __init__(m):
        m.param = 0
        m.key = ""
        m.say_ack = ""
        m.say_help = ""
        m.hotmorse = "N/A"

class Action(BaseAction):
    def __init__(m, path, title, action, param = "", say_help = "", hotmorse = ""):
        BaseAction.__init__(m)
        m.path = path
        m.title = title
        m.action = action
        m.param = param
        m.say_help = say_help
        m.hotmorse = hotmorse

class SubMenu(Action):
    def __init__(m, path, title, param):
        BaseAction.__init__(m)
        m.path = path
        m.title = title
        m.action = 0
        m.param = param

class NonBlocking(Action):
    def __init__(m, path, title, param, key = "", say_ack = "", say_help = "", hotmorse = ""):
        BaseAction.__init__(m)
        m.path = path
        m.title = title
        m.action = m.nonblock
        m.param = param
        m.key = key
        if say_ack != "":
            m.say_ack = say_ack
        else:
            m.say_ack = title
        m.say_help = say_help
        m.hotmorse = hotmorse

    def nonblock(m, param):
        intf.say(m.say_ack)
        print("Nonblocking action")
        param()
        if 0: yield

class AppMessage(Exception):
    pass

class BackToMainMenu(Exception):
    pass

class LongDisplay:
    def __init__(m):
        m.time = "10:30"
        m.title = "sms"
        m.text = "Your liveview wants to live"
        m.icon = None

class CelestialDisplay(LongDisplay):
    def __init__(m):
        m.time = "???"
        m.title = "Sun times"
        a = astro.AstroUtil()
        m.text = "" #a.format()
        # print a.formulate()

class Dialog:
    def __init__(m, menu):
        m.menu = menu
        m.name = "(Configure name)"
        m.init()

    def init(m):
        pass

    def input(m):
        return intf.input

    def no_input(m):
        intf.input = ""

class BaseMenu(Dialog):
    event = 0

    def display_dialog(m, dialog):
        print dialog
        dialog.base_menu = m
        for a in dialog.run():
            yield a

    def run_menu(m, full_menu, path):
        print("Entering submenu", path)
        menu = [ action for action in full_menu if path == action.path ]
        print("Have", len(menu), "menu items.")
        m.pos = 0
        while True:
            intf.display("%s %d/%d" % (path, m.pos, len(menu)), menu[m.pos].title)

            while True:
                yield
                if m.input() != "":
                    break

            m.this_item = menu[m.pos]

            if m.input() in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                morse_found = 0
                for item in full_menu:
                    if m.input() == item.hotmorse:
                        morse_found = 1
                        item.used_morse = 1
                        for a in item.action(item.param):
                            yield a
                        m.no_input()
                if morse_found:
                    continue
                else:
                    intf.say("Unknown command " + m.input())
            
            if m.input() == "w":
                m.pos -= 1
            if m.input() == "s":
                m.pos += 1
            if m.input() == "d":
                item = menu[m.pos]
                item.used_morse = 0
                if item.action:
                    for a in item.action(item.param):
                        yield a
                else:
                    for a in m.run_menu(m.main_menu, item.param):
                        yield a
                m.no_input()
                continue

            if m.input() == "a":
                return

            if m.pos < 0:
                m.pos = 0
            if m.pos >= len(menu):
                m.pos = len(menu)-1

    def run_long_display(m, ld):
        intf.prepare_long(ld)
        while True:
            print(ld.title)
            print(ld.time)
            print(ld.text)
            print("up to quit")
            yield
            if m.input() == "w":
                break
        intf.finish_long()

    def submenu(m, param):
        if 0: yield

    def debug_action(m, param):
        print("debug action: ", param)
        if 0: yield

    def unimplemented(m, param):
        print("unimplemented: ", param)
        if 0: yield

    def nonblock(m, param):
        param()
        if 0: yield

class Status(Background):
  def __init__(m):
      Background.__init__(m)
      m.color = 0
      m.interval = 600

  def run_every(m):
      print 'Setting leds -- periodic action'
      ( _, sms, _) = intf.droid.smsGetMessageCount(1)
      if sms > 0:
          print "# messages: ", sms
          intf.set_led(0, 0, 31)
          return
          
      # intf.set_led(m.color/2, m.color*2, m.color)
      if m.color == 0:
          intf.set_led(3, 20, 10)
          m.color += 1
          return
      if m.color == 1:
          intf.set_led(1, 6, 3)
          m.color = 0

class TimedDialog(Dialog):
    def run(m):
        print "Setting start time"
        m.start_time = time.time()
        intf.display(m.name, "0:00")

    def display_time(m, time, add):
        intf.display(m.name + add, formulate.StopwatchTime(time).format())

    def progress(m, add = ""):
        m.display_time(time.time() - m.start_time, add)

class VoiceNote(TimedDialog):
    def init(m):
        m.name = "Voice note"

    def run(m):
        TimedDialog.run(m)
        m.note = "note_%d.wav" % time.time()
        m.worker = subprocess.Popen([ "arecord", m.note ], shell=False)
        while True:
            yield
            if m.input() == "a":
                break
            if m.input() == "E":
                break
            m.progress()

        # Only in python 2.6+ :-(
        # m.worker.send_signal(signal.SIGTERM)
        # m.worker.terminate()
        os.system("killall arecord")

class Simon(TimedDialog):
    def run(m):
        next_time = 0
        simon = ''
        user = ''
        level = 1
        best_level = 1
        next_time = time.time()
        mutil = intf.mutil

        simon = 'foo'
        while True:
            simon = ''
            for i in range(level):
                simon += random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
            intf.say('Simon says ' + mutil.spell_word(simon))
            timer = time.time()

            user = ''
            while True:
                yield
                if m.input() in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                    user += m.input()
                print("User said so far ", user)
                delta = time.time() - timer
                if (len(user) >= len(simon)) or (delta > 20):
                    break

            if user == simon:
                intf.say('Good, %d second' % delta)
                level += 1
                if level > best_level:
                    best_level = level
                    intf.say('New best, level %d' % level)
            else:
                intf.say('Simon wanted '+mutil.spell_word(simon)+
                          ', you said '+mutil.spell_word(user)+'.')
                level -= 1
                if level < 1:
                    level = 1

            yield
            if m.input() and m.input() in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                intf.say('Thanks for playing, best level was %d.' % best_level)
                return

class Stopwatch(TimedDialog):
    def run(m):
        TimedDialog.run(m)
        total_time = 0
        started = True
        while True:
            current_time = time.time() - m.start_time
            if not started:
                current_time = 0
            m.display_time(total_time + current_time, "")

            yield

            current_time = time.time() - m.start_time
            if m.input() == "d":
                started = not started
                if started:
                    m.start_time = time.time()
                else:
                    total_time += current_time

            if m.input() == "s":
                if started:
                    # Handle splits
                    pass
                else:
                    total_time = 0

            if m.input() == "a":
                return

class SpyMe(Dialog):
    def write_html(m, pos):
        f = open("/tmp/spyme.html", "w")
        f.write("<html><head><title>My current position</title></head><body>\n")
        f.write("<p>My current position is %f %f %f m %f km/h\n" % (pos.lat, pos.lon, pos.eph, pos.speed))
        f.write('<p><a href="http://www.openstreetmap.org/#map=16/%f/%f">map</a>\n' % (pos.lat, pos.lon))
        f.write("</body></html>")
        f.close()
        os.system("scp /tmp/spyme.html pavel@atrey.karlin.mff.cuni.cz:~/WWW/zpells/")

    def run(m):
        position = m.base_menu.position
        position.start()
        count = 19
        while True:
            yield
            position.locate()
            intf.display("position", "%f %f %f m %f km/h" % (position.pos.lat, position.pos.lon, position.pos.eph, position.pos.speed))
            count += 1
            if count == 20:
                count = 0
                intf.display("uploading", "%f %f %f m %f km/h" % (position.pos.lat, position.pos.lon, position.pos.eph, position.pos.speed))
                m.write_html(position.pos)
        position.stop()
            
class Navigate(Dialog):
    def run(m):
        position = m.base_menu.position
        position.start()
        places = intf.data.places
        places_lists = [ pl for ( _, pl ) in places ]
        places = []
        for p in places_lists:
            places += p
        print places
        i = 0
        while True:
            position.locate()
            t = formulate.TravelTo(position.pos, places[i], places[i].name)
            intf.display(t.format(), "%d/%d" % (i+1, len(places)))
            yield
            inp = m.input()
            if inp == '': continue
            if inp in 'wA' and i > 0:
                i -= 1
                #t.say()
            if inp in 'sN' and i < len(places)-1:
                i += 1
                #t.say()
            if inp in 'E': t.say()
            if inp in 'aT': break
        position.stop()

class Countdown(TimedDialog):
    def run(m):
        total_time = 3*60
        while True:
            intf.display("countdown", "%f min" % (total_time / 60.))
            yield
            if m.input() == "w":
                total_time += 60
            if m.input() == "s":
                total_time -= 60
            if m.input() == "d":
                break

        limit = time.time() + total_time
        while True:
            left = limit - time.time()
            intf.display("countdown (running)", "%f min" % (left / 60.))
            if left < 0:
                intf.droid.vibrate(1000)
                intf.display("countdown done", "___")
                intf.say("Countdown done.")
                break
            yield
            if m.input() == "d":
                break

class NoteDetail(BaseMenu):
    def __init__(m, menu, fname):
        m.fname = fname
        pass

    def run(m):
        menu = []
        menu += [ NonBlocking( "", "play", lambda: intf.droid.mediaPlay(m.fname), hotmorse = "E") ]
        #menu += [ NonBlocking( "", "navigate", lambda: print "implement me", hotmorse = "I") ]
        return m.run_menu(menu, "")

class NoteList(BaseMenu):
    def inc_pos(m):
        pos += 1

    def dec_pos(m):
        pos -= 1

    def activate(m):
        item.used_morse = 1
        for a in m.this_item.action(m.this_item.param):
            yield a

    def run(m):
        l = os.popen("ls -1 *.wav").readlines()
        menu = []
        for fname in l:
            menu += [ Action( "", fname, m.display_dialog, NoteDetail(m.menu, fname.rstrip())) ]

        menu += [ NonBlocking( "", "next", lambda: m.inc_pos(), hotmorse = "E") ]
        menu += [ NonBlocking( "", "previous", lambda: m.dec_pos(), hotmorse = "I") ]
        menu += [ Action( "", "select", m.activate, hotmorse = "T") ]
        return m.run_menu(menu, "")
        
    pass

class Model(BaseMenu):
    def __init__(m):
        pass

    def about(m, param):
        intf.say("This is Zpells version 0 0.")
        raise BackToMainMenu

    def speed(m, param):
        intf.say('Speed meter')
        m.position.start()
        while True:
            yield
            m.position.locate()
            intf.display("speed", "%f km/h" % m.position.pos.speed)
        m.position.stop()

    def long_test(m, param):
        ld = LongDisplay()
        for a in m.run_long_display(ld):
            yield a

    def display_long(m, ld):
        for a in m.run_long_display(ld):
            yield a

    def run(m):
        print("Hello world")
        print("Main menu")
        main_menu = []

        main_menu+= [ SubMenu( "", "phone", param = "phone" ) ]
        main_menu+= [ NonBlocking( "phone", "find", lambda: intf.droid.vibrate(1000))]
        main_menu+= [ NonBlocking( "phone", "disconnect", lambda: intf.close_bt())]

        main_menu+= [ SubMenu( "", "music", param = "music" ) ]
        main_menu+= [ NonBlocking( "music", "play/pause", lambda: m.music.unpause(), hotmorse = "E" )]
        main_menu+= [ NonBlocking( "music", "next", lambda: m.music.play_next(), hotmorse = "N") ]
        main_menu+= [ NonBlocking( "music", "volume up", lambda: m.music.volume(1), hotmorse = "U" ) ]
        main_menu+= [ NonBlocking( "music", "volume down", lambda: m.music.volume(0), hotmorse = "D" ) ]
#        main_menu+= [ Action( "music", "podcasts/music", m.unimplemented, param = "" ) ]
        main_menu+= [ SubMenu( "", "time", param = "time" ) ]
        main_menu+= [ NonBlocking( "time", "time", lambda: formulate.AbsTime().both(), hotmorse = "T" ) ]
        main_menu+= [ Action( "time", "celestial", m.display_long, CelestialDisplay() ) ]
#        main_menu+= [ Action( "time", "alarm", m.unimplemented, param = "should set up alarm" ) ]
        main_menu+= [ Action( "time", "countdown",  m.display_dialog, Countdown(m) ) ]
        main_menu+= [ Action( "time", "stopwatch", m.display_dialog, Stopwatch(m) ) ]
#        main_menu+= [ SubMenu( "", "world", param = "world" ) ]
#        main_menu+= [ Action( "world", "weather", m.unimplemented, param = "" ) ]
        main_menu+= [ SubMenu( "", "gps", param = "gps" ) ]
        main_menu+= [ NonBlocking( "gps", "10 minutes", lambda: m.position.add_time(10*60)) ]
        main_menu+= [ Action( "gps", "speed", m.speed ) ]
        main_menu+= [ Action( "gps", "spy me", m.display_dialog, SpyMe(m) ) ]
        main_menu+= [ Action( "gps", "navigate", m.display_dialog, Navigate(m) ) ]
        main_menu+= [ SubMenu( "", "notes", param = "notes" ) ]
#        main_menu+= [ Action( "misc", "mark", m.unimplemented, param = "" ) ]
        main_menu+= [ Action( "notes", "voice note", m.display_dialog, VoiceNote(m), hotmorse = "V" ) ]
        main_menu+= [ Action( "notes", "notes", m.display_dialog, NoteList(m)) ]
        main_menu+= [ SubMenu( "", "help", param = "help" ) ]
        main_menu+= [ Action( "help", "about", m.about ) ]
        main_menu+= [ Action( "help", "Long demo", m.long_test ) ]
        main_menu+= [ NonBlocking( "help", "LED demo", lambda: intf.test_led() ) ]
        main_menu+= [ NonBlocking( "help", "Vibrator demo", lambda: intf.test_vibra() ) ]
        main_menu+= [ NonBlocking( "help", "morse code help", lambda: intf.droid.mediaPlay("morseovka.wav"), hotmorse = "CH" ) ]
        main_menu+= [ NonBlocking( "help", "alcoholical help", lambda: intf.droid.mediaPlay("morseovka.wav"), hotmorse = "X" ) ]
        main_menu+= [ Action( "help", "simon says", m.display_dialog, Simon(m), hotmorse = "Q" ) ]

        m.main_menu = main_menu
        print 
        return m.run_menu(main_menu, "")

    def start(m, intf):
        m.music = music.Music()
        m.status = Status()
        m.position = position.Position()
        intf.add_task(m.music)
        intf.add_task(m.status)
