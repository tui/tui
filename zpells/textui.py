import termios
import sys
import select

class TextUI:
  def adjust_termios(s):
    TERMIOS=termios
    fd = sys.stdin.fileno()
    try:
      s.old = termios.tcgetattr(fd)
    except:
      return
    new = termios.tcgetattr(fd)
    new[3] = new[3] & ~TERMIOS.ICANON & ~TERMIOS.ECHO
    new[6][TERMIOS.VMIN] = 1
    new[6][TERMIOS.VTIME] = 0
    termios.tcsetattr(fd, TERMIOS.TCSANOW, new)

  def reset_termios(s):
    termios.tcsetattr(fd, TERMIOS.TCSAFLUSH, s.old)

  def clear(s):
    sys.stdout.write("\x1b[2J\x1b[H")

class BaseLoop:
    def __init__(m):
        m.background = []
        m.tasks = []
        m.timer = 0
        m.watch = [ sys.stdin ]

    def display(m, line1, line2, icon = None):
        print("---------------------------------------------")
        print(line1)
        print(line2)
        print("---------------------------------------------")

    def add_task(m, background):
        m.tasks += [ background ]

    def handle_stdin(m):
        print("Got morse input")
        event = sys.stdin.read(1)
        m.handle_key(event)

    def handle_input(m, rlist):
        if sys.stdin in rlist:
            m.handle_stdin()

    def run(m):
        m.restart_model()
        while 1:
            ( rlist, wlist, xlist ) = select.select(m.watch, [], [], 1)
            m.handle_input(rlist)

            m.timer += 1
            if m.timer > 2:
                m.timer = 0
                for t in m.tasks:
                    t.run()
