#!/usr/bin/python
# Thanks to mechomaniac.com
# GPLv3
# Copyright 2011,2013 Pavel Machek
#
# Decode morse code from input device.
#
# Cooler name for the project?
# bit spells?
# Incantation?
# wearable magic?
# gealdor?
#
# echo "dah di dah dit... di dah... dah di dah..." | espeak
# 35 WPM possible with straight key.
# boy scouts do 5 WPM, amateur radio 10-40 WPM 
#
#  A .-    ABSTAK
#  B -...  BLIT AZ DOMA
#  C -.-.  CIL JE VYCEP
#  D -..   DAM JEDNO
#  E .     EX
#  F ..-.  FERNET PISE
#  G --.   GRONSKY RUM
#  H ....  HRUSKOVICE
# CH ----  CHVATAM K PIPAM
#  I ..    IRON
#  J .---  JABCAK BILY
#  K -.-   KYBL VIN
#  L .-..  LIKERECEK
#  M --    MZI Z PIP
#  N -.    NAVYK
#  O ---   O MUJ LIH
#  P .--.  PIVNI TACEK
#  Q --.-  QILIM OZRAN
#  R .-.   RUMICEK
#  S ...   SUD JE TU
#  T -     THE
#  U ..-   UCHLASTAN
#  V ...-  VODKA FINSKA
#  W .--   WHISKOU PLOUT
#  X -..-  XYRU BURCAK
#  Y -.--  Y, JSEM ZLAMAN
#  Z --..  ZLISKAM SE HNED


import struct
import time
import select
import os
import sys
import socket

global droid, device

def putc(c):
  sys.stdout.write(c)
  sys.stdout.flush()

class Decoder:
  dot = 0.15
  timeout = 0.7

  def __init__(self):
    self.last_time = 0
    self.last_value = 0
    self.last_code = ''

  def event(self, value):
    last_time = self.last_time
    last_value = self.last_value

    now = time.time()
    if debug:
      print "Event ", value, " at ", now, " delta ", now - last_time
    if value == 0:
      if now - last_time < self.dot:
        putc('.')
      else:
        putc('-')

    if value == 2:
      if last_value == 0:
        putc('/')
      else:
        putc('#')

    if value == 0 or value == 1:
      self.last_value = value
      self.last_time = now

class PC(Decoder):
  input_device = "/dev/input/by-id/usb-Chicony_USB_Keyboard-event-kbd"
  key = 126

  def init(self):
    pass

class X60(Decoder):
  input_device = "/dev/input/by-path/platform-i8042-serio-0-event-kbd"
  key = 159

  def init(self):
    pass

class N900(Decoder):
  debian_root = ""
  # You need to killall headset-control, first
#  input_device = "/dev/input/event4"
  input_device = "/dev/input/event4"
  key = 164
  timeout = 1.0

  def init(self):
    pass


me = socket.gethostname()
if me == 'duo':
    dec = X60()
elif me == 'Nokia-N900':
    dec = N900()
else:
    dec = PC()
dec.init()
debug = 0

#
# format of the event structure (int, int, short, short, int)
inputEventFormat = 'llhhi'
inputEventSize = 16 # 24 on 64-bit
file = open(dec.input_device, "rb", 0) # standard binary file input

while 1:
  ( i, o, e ) = select.select( [file], [], [], dec.timeout )
#  ( i, o, e ) = select.select( [file], [], [], 5 )
  if i == []:
    dec.event(2)
    continue
  event = file.read(inputEventSize)
  (time1, time2, type, code, value) = struct.unpack(inputEventFormat, event)
  if type == 1:
    if code == dec.key:
      if value == 1 or value == 0:
        if debug:
          print "type ", type, " code ", code, " value ", value
        dec.event(value)

file.close()
