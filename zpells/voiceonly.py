#!/usr/bin/python
# GPLv3
# Copyright 2011,2013 Pavel Machek
#
# More modern version of morse.py, expects external decoding of commands from
# alsamorse.py. (See morse.py for decoder-from-/dev/input)
#
# "zpells"? 
# Any sufficiently advanced technology is indistinguishable from magic. Arthur C. Clarke
#
# hpodder is for podcasts.

import re
import struct
import time
import select
import os
import random
import android
import sys
import subprocess
import string
import copy
import util
import traceback
import select
import Sun

global droid
global data

class City(util.FakePos):
  pass

class Data:
  voice_note_number = 0

class Decoder:
  def __init__(s):
    pass

  def run(s):
    s.keep_running = 1
    while s.keep_running:
      event = sys.stdin.read(1)
      s.event(event)

class VoiceNote(Menu):
  def __init__(s):
    pass

  def volume(s, up):
    x = '9'
    if up: x = '0'
    droid.player.stdin.write(x+x+x)
    droid.player.stdin.flush()

  def unpause(s):
    print "unpause"
    if s.paused:
      s.talk_now()
      droid.mediaPlayStart()
    else: droid.mediaPlayPause()
    s.paused = not s.paused
    return

  def load_list(s, p):
    print "loading ", p
    list = []
    f = os.popen(p)
    while 1:
      l = f.readline()[:-1]
      if not l: break
      list.append(l)

    random.shuffle( list )
    return list

  def play_next(s):
    print "play next"
    s.talk_now()
    try:
      n = s.playlist.pop()
    except:
      s.dec.say("Nothing more to play")
    try:
      droid.mediaPlay(n)
    except:
      s.dec.say("Player failed.")

  def say_ago(s, t):
    if t < 2*60:
      s.dec.say("%d seconds ago. " % t)
      return
    if t < 90:
      s.dec.say("%d minutes ago. " % (t/60))
      return
    s.dec.say("%d hours ago. " % (t/(60*60)))

  def play_note(s):
    n = data.stack[-1]
    stat = os.stat(n)
    s.say_ago(time.time() - stat.st_mtime)
    droid.mediaPlay(n)

  def run(s):
    if droid.playback_finished():
      s.play_next()

  def mark(s):
    print s.dec.gps
    mark = copy.deepcopy(s.dec.gps.pos)
    data.marks.append(mark)

class Podcasts(Music):
  def __init__(s):
    s.podcasts = s.load_list("find /data/prebrat.mp3/podcasts -maxdepth 1")
    Music.__init__(s)

    s.actions['C'] = Action(s, "podcasts", s.switch_podcasts)
    s.actions['A'] = Action(s, "author", s.switch_authors)

    s.current_author = 0

  def load_podcast(s):
    author = s.podcasts[s.current_author]
    author_name = re.sub(".*/", "", author)
    s.dec.say("Current author is "+author_name+".")
    s.playlist = s.load_list("find "+author)
    print "Switched author " + author
    print "Playlist is ", s.playlist

  def switch_podcasts(s):
    if s.playlist == s.music:
      s.load_podcast()
    else:
      s.dec.say("Music")
      s.playlist = s.music

  def switch_authors(s):
    s.current_author += 1
    if s.current_author == len(s.podcasts):
      s.current_author = 0
    s.load_podcast()

class BtWidget:
  def event(s, direction, action):
    pass

  def run(s):
    pass

class MenuEntry:
  def __init__(s, _menu, _text, _action):
    s.menu = _menu
    s.text = _text
    s.action = _action


class BtMenu(Selection):

class NumberInput(Selection):
  state = 0
  val = ""
  alphabet = "0123456789"
  name = ""

  def __init__(s):
    s.current = 0
    s.max = len(s.alphabet)

  def event_key(s, direction):
    t = s.alphabet[s.current]
    if direction ==  LiveViewMessages.NAVTYPE_DOWN:
      s.val += t
    elif direction ==  LiveViewMessages.NAVTYPE_UP:
      s.val = s.val[:-1]
    elif direction ==  LiveViewMessages.NAVTYPE_SELECT:
      s.done()
      return
    u = s.alphabet[:s.current] + '_' + s.alphabet[s.current] + '_' + s.alphabet[s.current+1:]
    s.dec.display_bt(s.name + ":[" + s.val + t + "]", u)

class Decoder(Morse):
  def __init__(s):
    Morse.__init__(s)
    print "Initializing decoder"
    s.timer = 0
    s.voice_widgets = []
    s.tasks = []
    s.bt_widgets = []

    s.music = Podcasts()
    s.voice_note = VoiceNote()
    s.simon = Simon()
    s.voice_widgets += [ s.simon, s.voice_note, s.music ]

    s.countdown = Countdown()
    s.bt_widgets += [ s.countdown ]

    menu = []
    menu += [ MenuEntry(s, "countdown", lambda: s.fg_bt_widget(s.countdown)) ]
    menu += [ MenuEntry(s, "alarm", lambda: s.say("implement alarm")) ]
    s.bt_menu_time = BtMenu(menu)
    s.bt_widgets += [ s.bt_menu_time ] 

    menu = []
    menu += [ MenuEntry(s, "play next", lambda: s.say("implement play next")) ]
    menu += [ MenuEntry(s, "info", lambda: s.say("implement info")) ]
    s.bt_menu_music = BtMenu(menu)
    s.bt_widgets += [ s.bt_menu_music ] 

    menu = []
    menu += [ MenuEntry(s, "music", lambda: s.fg_bt_widget(s.bt_menu_music)) ]
    menu += [ MenuEntry(s, "time", lambda: s.fg_bt_widget(s.bt_menu_time)) ]
    s.bt_menu = BtMenu(menu)
    s.bt_widgets += [ s.bt_menu ] 
    s.bt_menu.back = MenuEntry(s, "", lambda: s.say("nowhere to go"))
    s.bt_menu_time.back = MenuEntry(s, "", lambda: s.fg_bt_widget(s.bt_menu))
    s.bt_menu_music.back = MenuEntry(s, "", lambda: s.fg_bt_widget(s.bt_menu))

    s.gps = GPS()

    print "Preparing back pointer"

    for v in s.voice_widgets + s.tasks + s.bt_widgets:
      v.dec = s

    s.fg_v_widget(s.music)
    s.fg_bt_widget(s.bt_menu)

  def event(s, code):
      s.voice_w.run()
      s.bt_w.run()
    Morse.event(s, code)

  def process(s, code):
    s.voice_w.process(code)

  def fg_v_widget(s, v):
    s.voice_w = v

  def fg_bt_widget(s, v):
    s.bt_w = v

  def event_bt(s, direction, action):
    s.bt_w.event_bt(direction, action)

  def debug_event_bt(s, direction, action):
    if direction == LiveViewMessages.NAVTYPE_DOWN:
      t = "down"
      s.music.start()
    elif direction == LiveViewMessages.NAVTYPE_UP:
      t = "up"
    elif direction == LiveViewMessages.NAVTYPE_LEFT:
      t = "left"
    elif direction == LiveViewMessages.NAVTYPE_RIGHT:
      t = "right"
    elif direction == LiveViewMessages.NAVTYPE_SELECT:
      t = "select"
    elif direction == LiveViewMessages.NAVTYPE_MENU:
      t = "menu"
    else:
      t = "unknown"

    if action == LiveViewMessages.NAVACTION_PRESS:
      u = "press"
    elif action == LiveViewMessages.NAVACTION_LONGPRESS:
      u = "long"
    elif action == LiveViewMessages.NAVACTION_DOUBLEPRESS:
      u = "double"
    else:
      u = "unknown"

    s.display_bt("button %s / %s time %d" % (t, u, s.test_i), "(ok, %d)" % s.test_i)
    s.test_i += 1

  def say(s, t):
    s.display_bt(t, "(message)")
    say(t)

import textui

text = textui.TextUI()
try:
  text.adjust_termios()
except:
  print "input is from a pipe?"
droid = android.Android()
data = Data()
mutil = Util()

paused = 0
while 1:
  try:
    Decoder().run()
  except: # Exception as e:
    print traceback.format_exc()
#    print "%s" % e
    say("Any sufficiently advanced technology is indistinguishable from magic. But this failed. %s" % e)
  print "Main loop returned?!"
  time.sleep(10)
