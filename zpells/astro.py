#!/usr/bin/python

# More complex python ephemeris:
# apt-get install python-pip
# pip install pyephem

# Values from other source:
# http://www.timeanddate.com/astronomy/czech-republic/prague

# Moon brightness computations:
# http://home.earthlink.net/~kitathome/LunarLight/moonlight_gallery/technique/moonbright.htm
# Atmospheric Attenuation Effects
# http://home.earthlink.net/~kitathome/LunarLight/moonlight_gallery/technique/attenuation.htm

from formulate import *
import util
import time
sys.path += [ "../lib" ]
import ephemeris
import datetime

global intf

class AstroUtil(ephemeris.AstroUtil):
    def normalize(m, t):
        if t < 0:
            t += 24
        if t > 24:
            t -= 24
        return t
        
    def adjacent_times(m):
        m.format(1)
        pos = m.get_pos()
        now = ephem.Date(pos.date)
        min_val = now + 50.
        max_val = now - 50.
        
        for a in m.all_times:
            if max_val<a and a<now:
                max_val = a
                
            if min_val>a and a>now:
                min_val = a

        return now, min_val, max_val
    
    def formulate_short(m):
        now, min_val, max_val = m.adjacent_times()
        max_name = m.all_times[max_val]
        min_name = m.all_times[min_val]
                
        t = RelTime(0)
        r = ""
        t.diff = (now - max_val) * 3600. * 24.
        r += max_name + " was " + t.formulate() + ", "

        t.diff = (now - min_val) * 3600. * 24.
        r += min_name + " will be " + t.formulate() + ".\n"
        return r

class Dummy:
    pass

if __name__ == "__main__":
    intf = Dummy()
    intf.config = Dummy()
    intf.config.timezone = 1
    a = AstroUtil()
    print a.format()
    print a.formulate()
    print a.formulate_short()
#    a.pos.lat = 35
#    print a.format()
#    a.pos.lat = 15
#    print a.format()
