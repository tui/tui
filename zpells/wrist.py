
import LiveViewMessages
import sys
import time
try:
  import bluetooth
except:
  print "Bluetooth not available, aptitude install python-bluetooth ?"
  

global intf

# Copyright (c) 2011, Andrew de Quincey
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

class BtSupport:
  def __init__(m):
    #m.setup_incoming_bt()
    m.force_connection()
    #m.no_bt()

  def no_bt(m):
    m.server_socket = None
    m.have_client = 0

  def setup_incoming_bt(m):
    print("Waiting for bluetooth")
    m.server_socket = None
    m.have_client = 0
    try:
      serverSocket = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
      serverSocket.bind(("",1))
      serverSocket.listen(1)
      bluetooth.advertise_service(serverSocket, "LiveView", 
                                service_classes=[ bluetooth.SERIAL_PORT_CLASS ],
                                profiles=[ bluetooth.SERIAL_PORT_PROFILE ])

    except:
      print "No bluetooth available"
      return
    m.server_socket = serverSocket
    m.restart_bt()

  def restart_bt(m):
    m.watch = [ sys.stdin, m.server_socket ]
    m.have_client = 0
    m.long_text = None

  def force_connection(m):
    print("Connecting to bluetooth")
    m.server_socket = "Unused"
    m.long_text = None
    m.have_client = 0
    m.client_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
#sock.connect(("00:16:CE:EC:4D:C2", 1))
#sock.connect(("5C:57:C8:77:CE:5C", 1))
    m.client_socket.connect(("6C:23:B9:9B:57:7D", 1))
    m.watch += [ m.client_socket ]
    m.finish_accept()

  def accept_connection(m):
    print("Accepting bluetooth connection")
    if not m.server_socket:
      return
    m.client_socket, address = m.server_socket.accept()
    m.finish_accept()

  def finish_accept(m):
    m.watch += [ m.client_socket ]
    m.have_client = 1
    print("Accepted.")
    msg = LiveViewMessages.EncodeGetCaps()
    print(msg)
    m.client_socket.send(msg)
    print("Sent caps")

    testPngFd = open("../liveview/test36.png")
    m.test_png = testPngFd.read()
    testPngFd.close()

    testPngFd = open("../liveview/test126.png")
    m.test_128_png = testPngFd.read()
    testPngFd.close()

    m.test_i = 0

  def handle_client(m):
    try:
      data = m.client_socket.recv(4096)
    except bluetooth.btcommon.BluetoothError:
      print "Bluetooth failed."
      m.restart_bt()
      return
    for msg in LiveViewMessages.Decode(data):
      m.handle_bt_msg(msg)

  def send_bt(m, msg):
    if not m.have_client:
      return
    m.client_socket.send(msg)

  def display(m, top, bottom, png = None):
    if not m.have_client:
      return
    if not png:
      png = m.test_png
    m.send_bt(LiveViewMessages.EncodeDisplayPanel(top, bottom, png, False))

  def handle_bt_msg(m, msg):
    clientSocket = m.client_socket
    print(("Got message", msg))
    # Handle result messages
    if isinstance(msg, LiveViewMessages.Result):
      if msg.code != LiveViewMessages.RESULT_OK:
        print("---------------------------- NON-OK RESULT RECEIVED ----------------------------------")
        print(msg)
      return

    # Handling for all other messages
    m.send_bt(LiveViewMessages.EncodeAck(msg.messageId))
    if isinstance(msg, LiveViewMessages.GetMenuItems):
      if not m.long_text:
        m.send_bt(LiveViewMessages.EncodeGetMenuItemResponse(0, True, 0, "Hello", m.test_png))
        m.send_bt(LiveViewMessages.EncodeDisplayPanel("Start", "here :-)", m.test_png, False))
      else:
        m.send_bt(LiveViewMessages.EncodeGetMenuItemResponse(0, True, 0, "select view, up quit", m.test_png))

    elif isinstance(msg, LiveViewMessages.GetMenuItem):
      print("---------------------------- GETMENUITEM RECEIVED ----------------------------------")
      # FIXME: do something!
    elif isinstance(msg, LiveViewMessages.DisplayCapabilities):
      deviceCapabilities = msg
      m.send_bt(LiveViewMessages.EncodeSetMenuSize(1))
      m.send_bt(LiveViewMessages.EncodeSetMenuSettings(5, 0))

    elif isinstance(msg, LiveViewMessages.GetTime):
      m.send_bt(LiveViewMessages.EncodeGetTimeResponse(time.time() + intf.config.timezone*3600, 1))

    elif isinstance(msg, LiveViewMessages.DeviceStatus):
      m.send_bt(LiveViewMessages.EncodeDeviceStatusAck())

    elif isinstance(msg, LiveViewMessages.GetAlert):
      ld = m.long_text
      if not ld:
        m.send_bt(LiveViewMessages.EncodeGetAlertResponse(20, 4, 15, "now", "bad", "Long text display was requested, but interface does have anything prepared. Watch are now confused.", m.test_png))
      else:
        m.send_bt(LiveViewMessages.EncodeGetAlertResponse(20, 4, 15, ld.time, ld.title, ld.text, m.test_png))

    elif isinstance(msg, LiveViewMessages.Navigation):
      m.send_bt(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_EXIT))
      m.event_bt(msg.navType, msg.navAction)

  def event_bt(m, direction, action):
    if direction == LiveViewMessages.NAVTYPE_DOWN:
      k = "s"
      t = "down"
    elif direction == LiveViewMessages.NAVTYPE_UP:
      k = "w"
      t = "up"
    elif direction == LiveViewMessages.NAVTYPE_LEFT:
      k = "a"
      t = "left"
    elif direction == LiveViewMessages.NAVTYPE_RIGHT:
      k = "d"
      t = "right"
    elif direction == LiveViewMessages.NAVTYPE_SELECT:
      k = "e"
      t = "select"
    elif direction == LiveViewMessages.NAVTYPE_MENUSELECT:
      k = "q"
      t = "menu"
    else:
      t = "unknown"

    if action == LiveViewMessages.NAVACTION_PRESS:
      u = "press"
    elif action == LiveViewMessages.NAVACTION_LONGPRESS:
      u = "long"
    elif action == LiveViewMessages.NAVACTION_DOUBLEPRESS:
      u = "double"
    else:
      u = "unknown"

#    m.display_bt("button %s / %s time %d" % (t, u, m.test_i), "(ok)")
    m.test_i += 1
    m.handle_key(k)

  def test_vibra(m):
    m.send_bt(LiveViewMessages.EncodeSetVibrate(1, 300))
                
  def test_led(m):

    m.send_bt(LiveViewMessages.EncodeSetMenuSize(0))
    m.send_bt(LiveViewMessages.EncodeSetLED(0x3f, 0x3f, 0x3f, 1, 30000))
    # Nice iconds are in :~/sf/liveview/openliveview/assets
    # 60x60, 425 bytes picture IS accepted
    # 60x60, 1803 bytes picture IS accepted
    # 65x65, 503 bytes picture IS accepted
    # 77x77, XX bytes picture IS  accepted
    # 80x80, 640 bytes picture IS  accepted
    # 80x80, 948 bytes picture IS  accepted
    # 81x81,  picture NOT accepted 
    # 82x82,  picture NOT accepted 
    # 83x83, 662 bytes picture NOT  accepted
    # 90x90, 1004 bytes  picture NOT accepted
    # 126x126 picture NOT accepted
    # 128x128, 1199 bytes picture NOT accepted
    # 128x128, 416 bytes  picture NOT accepted
    m.send_bt(LiveViewMessages.EncodeDisplayBitmap(0, 0, m.test_128_png))
    time.sleep(1)
    m.send_bt(LiveViewMessages.EncodeSetLED(0x3f, 0, 0, 1, 30000))
    m.send_bt(LiveViewMessages.EncodeDisplayBitmap(0, 36, m.test_png))
    time.sleep(1)
    m.send_bt(LiveViewMessages.EncodeSetLED(0, 0x3f, 0, 1, 30000))
    m.send_bt(LiveViewMessages.EncodeDisplayBitmap(36, 0, m.test_png))
    m.send_bt(LiveViewMessages.EncodeSetMenuSize(1))
    m.send_bt(LiveViewMessages.EncodeSetLED(0, 0, 0x3f, 1, 30000))
    time.sleep(1)

    m.send_bt(LiveViewMessages.EncodeGetAlertResponse(20, 4, 15, "TIME", "HEADER", "01234567890123456789012345678901234567890123456789", m.test_png))

  def test_long(m):
    m.send_bt(LiveViewMessages.EncodeSetMenuSize(1))

  def prepare_long(m, ld):
    m.long_text = ld
    m.send_bt(LiveViewMessages.EncodeSetMenuSize(1))

  def finish_long(m):
    m.long_text = None

  def set_led(m, r, g, b):
    # 30 000 = cca 10 seconds.
    m.send_bt(LiveViewMessages.EncodeSetLED(r, g, b, 1, 30000))

  def test_led_input(m):
    r,g,b = input()

    print "Setting leds ", r, g, b
    m.set_led(r, g, b)

  def close_bt(m):
    m.client_socket.close()
    m.restart_bt()
