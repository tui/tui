#

import time

class Background:
    def __init__(m):
        m.name = "Unknown"
        m.last_run = 0
        m.interval = 999

    def run(m):
        now = time.time()
        if m.last_run + m.interval < now:
            m.last_run = now
            m.run_every()

    def run_every(m):
        pass

    def get_info(m):
        return m.name + " is running."

    def talk_now(s):
        return ""


class Timed(Background):
    timeout = 0

    def start(s):
        pass

    def stop(s):
        pass

    def add_time(s, seconds):
        if s.timeout == 0:
            s.start()
            s.started = time.time()
            s.timeout = s.started + seconds
        else:
            s.timeout += seconds

    def active_run(s):
        pass

    def active(s):
        return time.time() <= s.timeout

    def run(s):
        if not s.active():
            s.stop()
            s.timeout = 0
        else:
            s.active_run()
