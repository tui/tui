import util
import base
import time
import sys

class Position(base.Timed):
    name = "G P S"
    pointnum = 0
    outf = open("voiceonly.gpx", "a")
    last_talk = 0
    pos = util.FakePos()
    pos.eph = 100000000

    def start(s):
        intf.droid.startLocating()

    def stop(s):
        intf.droid.stopLocating()

    def locate(s):
        s.pos.lat = 50
        s.pos.lon = 14
        s.pos.eph = 100000000
        s.found = 0
        s.time = time.time()
      
        (a, p, b) = intf.droid.getLastKnownLocation()
        if "gps" in p:
            p = p["gps"]

            s.pos.lat = p["latitude"]
            s.pos.lon = p["longitude"]
            s.pos.eph = p["accuracy"]
            s.pos.speed = p["speed"]
            s.pos.altitude = 0
            if "altitude" in p:
                s.pos.altitude = p["altitude"]
            s.pos.track = 0
            if "track" in p:
                s.pos.track = p["track"]
            s.found = 1

            print("GPS: %f deg %f deg %f ? %f m %f km/h\n" % (s.pos.lat, s.pos.lon, s.pos.eph, s.pos.altitude, 0))
            
            

    def talk(s, talkative):
        if talkative == 0 and time.time() - s.last_talk < 600:
            return
        if talkative == 1 and time.time() - s.last_talk < 3*60:
            return
        if not s.active():
            return
        if s.pos.eph < 100000:
            intf.say("Have G P S .")
        else:
            intf.say("No G P S position .")
        if s.pos.speed > 5:
          s.dec.say("Moving %s at %d kilometers per hour." % (s.get_direction(s.pos.track), s.pos.speed))
        if data.marks != []:
          last_mark = data.marks[-1]
          s.dec.say(s.describe_position("Target", last_mark))
        nearest = data.get_nearest(s.pos, data.cities)
        s.dec.say(s.describe_position(nearest.name, nearest)) 
        s.last_talk = time.time()

    def get_info(s):
        s.talk(2)
        return ""

    def talk_now(s):
        s.talk(1)

    def active_run(s):
        s.locate()
        if s.pos.eph < 100000:
            s.pointnum += 1
            print >>s.outf, "<node id='%d' lat='%f' lon='%f'><tag k='note' v='auto' /></node>" % ( s.pointnum, s.pos.lat, s.pos.lon )
        s.talk(0)
