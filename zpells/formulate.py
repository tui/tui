import base
import os
import random
import android
import sys
import subprocess
import string
import copy
import time
import util
import position
sys.path += [ "../lib" ]
import basic_formulate
import nearest

global intf

class Formulate(basic_formulate.Formulate):
    def say(m):
        intf.say(m.formulate())

    def display(m):
        intf.display("???", m.format())

    def both(m):
        m.display()
        m.say()

class AbsTime(Formulate):
    def __init__(m, val = 0):
        if not val:
            val = time.time()
        m.val = val

    def fmt(m):
        if m.v > 0:
            s = "%_I %M on %A, %B %_e. "
        else:
            s = "%A %B %_e %_I:%M"
        return time.strftime("%_I %M on %A, %B %_e. ", time.localtime(m.val))

class RelTime(Formulate):
    def __init__(m, diff):
        m.diff = diff

    def ago(m):
        diff = m.diff
        if diff > 0:
            if diff < 90:
                return "%d seconds ago" % diff
            if diff < 90*60:
                return "%d minutes ago" % (diff / 60)
            if diff < 3600*48:
                return "%d hours ago" % (diff / 3600)
            return "%d days ago" % (diff / (3600*24))
        else:
            diff = -diff
            if diff < 90:
                return "in %d seconds" % diff
            if diff < 90*60:
                return "in %d minutes" % (diff / 60)
            if diff < 3600*48:
                return "in %d hours" % (diff / 3600)
            return "in %d days" % (diff / (3600*24))

    def fmt(m):
        if m.v > 0:
            return  m.ago()
        else:
            return  "(" + m.ago() + ")"

class StopwatchTime(RelTime):
    def fmt(m):
        return "%d:%02d" % ( m.diff/60, m.diff%60 )

class Distance(Formulate):
    def __init__(m, dist):
        m.dist = dist

    def fmt(m):
        d = m.dist
        if d < 5:
            s = "%d " % m.num(d*1000)
        else:
            s = "%d "  % m.num(d)
            s += m.alt("k", "kilo")
        return s + m.alt("m", "meters")

class Angle(Formulate):
    def __init__(m, angle):
        if angle < 0:
            angle += 360
        if angle >= 360:
            angle -= 360
        m.angle = angle

class Direction(Angle):
    directions = ["north", "north-east", "east", "south-east", "south", "south-west", "west", "north-west", "north"]

    def get_direction(m):
        track = m.angle
        track += 45. / 2
        track /= 45
        track = int(track)
        return m.directions[track]

    def fmt(m):
        s =  m.get_direction()
        if m.v <= 0:
            s += " (%d deg)" % m.num(m.angle)
        return s

class RelDirection(Angle):
    def get_oclock(m):
        angle = m.angle + 15
        hour = int(angle / 30)
        if hour == 0:
            hour = 12
        return hour

    def fmt(m):
        if m.v <= 0:
            angle = m.angle
            if angle > 180:
                return "left %d deg" % m.num(360-angle)
            return "right %d deg" % m.num(angle)
        return "%d o clock" % ( m.get_oclock() )

class TravelTo(Formulate):
    def __init__(m, pos, target, target_name = None):
        m.pos = pos
        m.target = target
        m.target_name = target_name

    def compute(m):
        m.distance = util.distance(m.pos, m.target)
        m.bearing = util.bearing(m.pos, m.target)

    def fmt(m):
        m.compute()
        name = m.target_name
        if not name:
            name = m.alt("", "Destination")
        else:
            name += ": "
        if m.pos.eph > 1000:
            s = name + "(no GPS)"
        else:
            s = name + " %s %s. " % (Distance(m.distance).format(m.v), Direction(m.bearing).format(m.v))
            if m.pos.track:
                s += RelDirection(m.bearing-m.pos.track).format(m.v) + ". "
        return s

class Position(Formulate):
    def __init__(m, pos):
        m.pos = pos

    def fmt(m):
        s = ""
        mind = 9999999999
        for type, list in intf.data.places:
            d = mind
            ( minp, mind ) = intf.data.get_nearest(m.pos, list, mind)
            if mind < d:
                s += type + ": " + TravelTo(m.pos, minp, minp.name).format(m.v) + "\n"

        return s

class TestPos(nearest.Place):
    track = 90
    speed = 5

class Time:
  def activate_gps(s):
    s.dec.gps.add_time(10*60)

  def mark(s):
    print(s.dec.gps)
    mark = copy.deepcopy(s.dec.gps.pos)
    data.marks.append(mark)

def selftests():
    print("Should do some self-tests.")
    prague = TestPos(50.1, 14.3, "Prague")
    kbely = TestPos(50.14, 14.34, "Kbely")
    ricany = TestPos(49.993, 14.681, "Ricany")
    zernovka = TestPos(50.00, 14.75, "Zernovka")
    near_zernovka = TestPos(50.005, 14.752)
    near_prague = TestPos(50.05, 14.45)

    print(Position(near_zernovka).selftest())
    print(Position(near_prague).selftest())

    # Cruel tests; with 90 it divides by zero.
    south_pole = TestPos(-89.99, 0)
    north_pole = TestPos(89.99, 0)
    south2_pole = TestPos(-89.99, 180)

    print("4 m")
    Distance(0.004).selftest()
    print("400 m")
    Distance(0.4).selftest()
    print("8 km")
    Distance(8).selftest()


    print("Ricany to Zernovka:")
    TravelTo(ricany, zernovka).selftest()

    print("South pole to North pole:")
    TravelTo(south_pole, north_pole).selftest()

    print("South pole to North pole:")
    TravelTo(south_pole, north_pole).selftest()

    print("South pole to same south pole:")
    TravelTo(south_pole, south2_pole).selftest()

    print("Straight ahead:")
    RelDirection(0).selftest()
    RelDirection(-1).selftest()
    RelDirection(1).selftest()
    RelDirection(14).selftest()
    RelDirection(15).selftest()
    RelDirection(16).selftest()
    RelDirection(-14).selftest()
    RelDirection(-15).selftest()
    RelDirection(-16).selftest()

    AbsTime().selftest()
    RelTime(-100).selftest()
    RelTime(100).selftest()

if __name__ == "__main__":
    intf = util.FakePos()
    intf.data = nearest.Data()
    selftests()
