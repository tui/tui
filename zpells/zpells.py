#!/usr/bin/python
# Copyright 2013 Pavel Machek <pavel@ucw.cz>
#
# More modern version of morse.py and voiceonly.py, expects external
# decoding of commands from alsamorse.py. (See morse.py for
# decoder-from-/dev/input)
#
# "zpells"? 
# Any sufficiently advanced technology is indistinguishable from magic. Arthur C. Clarke

import textui
import sys
import basemenu
import android
import music
import morseutil
import formulate
import wrist
import select
import astro
import position
sys.path += [ "../lib" ]
import nearest

class Config:
    # CET == 1, CET DST == 2
    timezone = 2

class BaseInterface(textui.BaseLoop):
    def __init__(m):
        textui.BaseLoop.__init__(m)
        m.mutil = morseutil.MorseUtil()
        m.config = Config()
        m.droid = android.Android()

    def say(m, text):
        print("Say ", text)
        m.droid.ttsSpeak(text)

    def restart_model(m):
        m.state = m.model.run()
        intf.input = ""
        m.state.next()

    def handle_key(m, key):
        intf.input = key
        try: 
            m.state.next()
        except (basemenu.BackToMainMenu, StopIteration):
            m.restart_model()

class MorseInterface(BaseInterface):
    def __init__(m):
        BaseInterface.__init__(m)
        m.last_time = 0
        m.last_value = 0
        m.last_code = ''
        m.mutil = morseutil.MorseUtil()

    def handle_stdin(m):
        print("Got morse input")
        value = sys.stdin.read(1)
        m.handle_key(value)

        if value in [ '.', '-' ]:
            m.last_code = m.last_code + value

        if value in [ '/' ]:
            c = m.last_code
            m.last_code = ''
            if not c in m.mutil.morse:
                return
            c = m.mutil.morse[c]
            m.handle_key(c)

class BtInterface(MorseInterface, wrist.BtSupport):
    def __init__(m):
        MorseInterface.__init__(m)
        wrist.BtSupport.__init__(m)

    def handle_input(m, rlist):
        was_event = 0
        if sys.stdin in rlist:
            was_event = 1
            m.handle_stdin()
        if m.server_socket in rlist:
            print("Got bt client")
            m.accept_connection()
        if m.have_client and m.client_socket in rlist:
            print("have bt data")
            was_event = 1
            m.handle_client()
        if not was_event:
            m.handle_key("")

    def display(m, l1, l2, icon = None):
        MorseInterface.display(m, l1, l2, icon)
        wrist.BtSupport.display(m, l1, l2, icon)
        

class Interface(BtInterface):
    pass
    
intf = Interface()
basemenu.intf = intf
music.intf = intf
formulate.intf = intf
astro.intf = intf
wrist.intf = intf
position.intf = intf
model = basemenu.Model()
intf.model = model
intf.data = nearest.Data()
model.start(intf)
intf.run()
