/* gcc audioclick.c -o audioclick && parec | ./audioclick
*/

void main(void)
{
#define S 8196
  short buf[S];

  while (1) {
    int i;
    long sum = 0;
    double res;
    {
      int s = sizeof(buf);
      char *b = buf;
      do {
	i = read(0, b, s);
	if (i < 0) {
	  printf("Bad read: %m\n");
	  exit(1);
	}
	b += i;
	s -= i;
      } while (i>0);
    }
    for (i=0; i<S-1; i++) {
      if (buf[i] < -300 || buf[i] > 300)
	printf("Loud!\n");
      sum += (buf[i+1]-buf[i]) * (buf[i+1]-buf[i]);
    }
    res = sum/S;
    printf("Sum: %.2f\n", (float) res);
  }
  
}
