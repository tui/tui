#include <stdio.h>

int skip;

unsigned char get_c(void)
{
  int c;

  while (1) {
    c = getc(stdin);
    if (c == EOF)
      exit(0);
    if (c >= 'A' && c < ('A' + 16))
      return c - 'A';
  }
}

void main(void)
{
  while (1) {
    unsigned char c, d;
    c = get_c();
    d = get_c();
    c |= get_c() << 4;
    d = get_c();
    if (c != putc(c, stdout))
      exit(1);
  }
}
