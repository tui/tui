/*
 * Play the game of logik against devil!
 * Copyright 1998 Pavel Machek
 * GPLv3
 */

#include <stdio.h>

#define N 8
#define SPACE (N*N*N*N*N)
#define MOVES 20

int numallowed = SPACE;
int bestcnt;
char allowed[SPACE];

int
splitit( int i, int *r )
{
  r[0] = i % N; i /= N;
  r[1] = i % N; i /= N;
  r[2] = i % N; i /= N;
  r[3] = i % N; i /= N;
  r[4] = i % N; i /= N;
  if (i) abort();
}

#define MIN(a,b) ((a)<(b)?(a):(b))

/* Hidden, Tah, ... */
int
score( int h, int t, int *b, int *w )
{
  int hh[5], tt[5], ch[N+1], ct[N+1];
  int j;

  *w = 0; *b = 0;
  splitit( h, hh );
  splitit( t, tt );

  for (j=0; j<N+1; j++)
    ch[j] = ct[j] = 0;

  for (j=0; j<5; j++) {
    if (hh[j]==tt[j])
      { (*b)++; hh[j]=N; tt[j]=N; }
  }

  for (j=0; j<5; j++) {
    ch[ hh[j] ]++;
    ct[ tt[j] ]++;
  }

  for (j=0; j<N; j++)
    (*w) += MIN( ch[j], ct[j] );
}

#if 0
int
isallowed( int h )
{
  int j;
  for (j = 0; j<moves; j++) {
    int b, w;
    score( h, move[j], &b, &w );
    if (b != black[j]) return 0;
    if (w != white[j]) return 0;
  }
  return 1;
}
#endif

void
print( int h )
{
  int s[5];
  splitit( h, s );
  printf( "%d %d %d %d %d ", s[4], s[3], s[2], s[1], s[0] );
}

int 
input( void )
{
  int r = 0, i;
  scanf( "%d", &i ); r *= N; r += i;
  scanf( "%d", &i ); r *= N; r += i;
  scanf( "%d", &i ); r *= N; r += i;
  scanf( "%d", &i ); r *= N; r += i;
  scanf( "%d", &i ); r *= N; r += i;
  return r;
}

void
main( void )
{
  int i, num = 0;
  int verbose = 0;

  setvbuf( stdout, NULL, _IONBF, 0 );
  setvbuf( stderr, NULL, _IONBF, 0 );

  for (i=0; i<SPACE; i++)
    allowed[i]=1;

  while (1) {
    int i, j, m, b, w, bestb, bestw;
    int cnt[6][6];

    num++;
    printf( "Move %d, %d chances: ", num, numallowed );
    m = input();
    printf( "Asking " ); print( m ); 
    for (b=0; b<6; b++)
      for (w=0; w<6; w++)
	cnt[b][w]=0;
    for (i=0; i<SPACE; i++)
      if (allowed[i]) {
	score( i, m, &b, &w );
//	print(i); printf( " - scored %d %d\n", b, w );
	cnt[b][w]++;
      }

    bestcnt = -1;
    for (b = 0; b<6; b++) {
      if (verbose)
	printf( "\n" );
      for (w = 0; w+b<6; w++) {
	if (verbose)
	  printf( " %d:%d-%4d, ", b, w, cnt[b][w] );
	if (cnt[b][w] > bestcnt)
	  { bestb = b; bestw = w; bestcnt=cnt[b][w]; }
      }
    }

    printf( "\nDevil selects %d black, %d white... ", bestb, bestw );
    j = 0;
    for (i=0; i<SPACE; i++)
      if (allowed[i]) {
	score( i, m, &b, &w );
	if ((b != bestb) || (w != bestw))
	  { allowed[i]=0; j++; }
      }
    printf( "%d disappeared, %d should remain, %d remains\n", j, bestcnt, numallowed-j );
    numallowed = bestcnt;
  }
}
