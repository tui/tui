/*
 * Multiuser xonix - action game for text console
 *
 * Copyright 1998 Pavel Machek, distribute under terms of
 * GNU General Public License (version 2 or later).
 *
 * Specifications said use sysV IPC and use curses, so this 
 * code is both broken and cursed.
 *

How to play
~~~~~~~~~~~

This is similar to traditional xonix game: You are trying to steal
space from other player without him catching you. You get points for
every point you steal. Game ends if score (indicated by Sc:) reaches
win or -win value (indicated by Win:), or when someone's lives reach
zero. Lives are decreased with every catch.

Controls are     l
                 ^
            h <- | -> k
                 v  
                 j

To steal space, you have to go to enemy's space. While on enemy's
space, you draw a line consisting with '+' characters. Enemy can catch
you by either touching you, or line you draw behind. In any case, your
lives decrease and you go to your home corner.

So now, find someone to play with you, and game can begin!

                          Share and enjoy,
			                                        Pavel
 */

#include <time.h>
#include <stdio.h>
#include <curses.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a>b?a:b)

#define MSG_EXCEPT 0

/*
 * Some things that may change
 */
#define VERSION "0.0.3"
#define MAX_PL 20
#define MAX_X 250
#define MAX_Y 250
#define F_MAX 10
#define SPEED 75

#define COLOR 4
#define MARK 8
#define MARKNOT 16

/*
 * Our data structures
 */
int sizex, sizey, startgame;
int field[ MAX_X * MAX_Y ];
#define FIELD( x, y ) (field[ (y) * MAX_X + (x) ])
#define F_WALL 0
#define F_EMPTY 1
#define F_ME 2
#define F_TEMPORARY 3
#define numpl 6
char chars[ F_MAX ] = { 'X', ' ', '*', '+', '?', '!', 'a', 'b', 'c', 'd' };

struct player {
  int color; /* Am I black or white? */
  int type;  /* One of below */
#define T_NONE 0
#define T_PLAYER 1
#define T_ROBOT 2
  int lives; /* # of lives remaining */
  char *name;
  int x, y, dx, dy;
  int prev;
};
struct player pl[MAX_PL], *mypl, *otherpl;
int other; /* PID of other process */

int twoplayer = 0;

/*
 * Messages we pass around
 */
struct my_msgbuf {
  long sender;	/* PID of sender */
  long type;
  int data[10];
} message;

#define M_HELLO 1
#define M_PARAMS 2
#define M_GAME 3
int msgque;
int balance = 0, goal, win = -1; /* >0 - player 0 is better, < 0 - player 1 is better */

#define C_LINE 1

/*
 * Get time from beggining 
 */
int
ticks( void )
{
struct timeb tb;
int sec, msec;
ftime( &tb );
sec = tb.time - startgame;
msec = tb.millitm;
return sec * 1000 + msec;
}

/*
 * Set color of field [x,y]
 */
void
set( int x, int y, int c ) 
{ 
int d = FIELD( x, y );
FIELD( x, y ) = c; 
if (c!=d) { 
  (c & COLOR) && attron( A_STANDOUT ); 
  mvaddch( y+1, x, chars[ c & (~COLOR) & (~MARK) & (~MARKNOT) ] );
  (c & COLOR) && attroff( A_STANDOUT );
  }
}

/*
 * Display message on statusline, update other values
 */
void
statusline( char *message )
{
char buf[ MAX_X ];
(mypl->type & COLOR) && attron( A_STANDOUT );
sprintf( buf, "M/Xonix %30s Lives: %1d %1d Sc: %5d Win: %5d    ", message, pl[1].lives, pl[0].lives, balance, goal );
mvaddstr( 0, 0, buf );
(mypl->type & COLOR) && attroff( A_STANDOUT );
refresh();
}

/*
 * Paint frame around the field
 */
void
fieldframe( void )
{
int i,j;
for( i=0; i<MAX_X * MAX_Y; i++)
  field[i]=-1;
for( i=0; i<sizex; i++ )
  for( j=0; j<sizey-1; j++ )
    set( i, j, F_EMPTY | (COLOR * (i < (sizex>>1))) );
for( i=0; i<sizex; i++ ) {
  set( i, 0, F_WALL );
  set( i, sizey-2, F_WALL );
  }
for( i=0; i<sizey-1; i++ ) {
  set( 0, i, F_WALL );
  set( sizex-1, i, F_WALL );
  }
set( 1, 1, F_WALL );
set( sizex-2, 1, F_WALL );
}

/*
 * Initialize structure of player
 */
void
initpl( struct player *pl, int c )
{
  pl->type = T_PLAYER | c;
  pl->y = 1;
  pl->x = !(c & COLOR) ? sizex-2 : 1;
  pl->dx = 0;
  pl->dy = 1;
  pl->prev = F_WALL;
}

/*
 * Player dies, clean things up after him
 */
void
playerdies( int which )
{
int i,j;
struct player *player = &pl[ which ];
set( player->x, player->y, player->prev );
player->lives--;
for( i=0; i<sizex; i++ )
  for( j=0; j<sizey; j++ )
    if (FIELD(i,j)==(F_TEMPORARY | which*COLOR))
      set(i,j,(F_EMPTY | (which*COLOR)) ^ COLOR);
initpl( player, which*COLOR );
if (!player->lives) win = !which;
}

/*
 * Player got back to his home area, make line behind him permanent
 */
void
gothome( int which )
{
int i,j;
for( i=0; i<sizex; i++ )
  for( j=0; j<sizey; j++ )
    if (FIELD(i,j)==(F_TEMPORARY | which*COLOR)) {
      set(i,j,F_EMPTY | which*COLOR);
      if (which) balance--;
            else balance++;
    }
}

/* 
 * Floodfill specified area
 *	doit = 0 only MARK area filled
 *      doit = 2 turn MARK into MARKNOT 
 * return value says, if we met some 'enemy' in this area
 * (in such case we can not fill it)
 */
int 
floodfill( int x, int y, int which, int doit )
{
int ret;
if (doit)
  {
  if (! (FIELD(x,y) & MARK) ) return 0;
  if (FIELD(x,y) & MARKNOT) return 0; 
  FIELD(x,y) |= MARKNOT; 
  }
else
  {
  if (FIELD(x,y) != ((F_EMPTY | which) ^ COLOR))
    return ( (FIELD(x,y) & ~COLOR) == F_ME );
  FIELD(x,y) |= MARK;
  }
ret = floodfill( x+1, y, which, doit );
ret |= floodfill( x-1, y, which, doit );
ret |= floodfill( x, y+1, which, doit );
ret |= floodfill( x, y-1, which, doit );
return ret;
}

void
maybefloodfill( int x, int y, int which )
{
if (floodfill( x, y, which, 0 )) floodfill( x, y, which, 2 );
}

/*
 * Is it possible to move into specified field? (given by its contents)
 */
int
moveok( struct player *p, int c )
{
if ((c & ~COLOR) == F_TEMPORARY)
  playerdies( ((c & COLOR) > 0) );
if ((p->type & ~COLOR)==T_ROBOT)
  return (((p->type & COLOR) ^ c) == F_EMPTY);
if (((p->prev & ~COLOR) == F_TEMPORARY) && 
    ((c & COLOR) == (p->type & COLOR)) && 
    ((c & ~COLOR) == F_EMPTY))
  {
  int x, y;
  gothome( (c & COLOR) > 0 );

  for (x=0; x<sizex; x++)
    for (y=0; y<sizey; y++)
      maybefloodfill( x, y, p->type & COLOR );

  for (x=0; x<sizex; x++)
    for (y=0; y<sizey; y++)
      if (FIELD( x,y ) & MARK)
	{
	if (FIELD( x,y ) & MARKNOT) set( x, y, FIELD(x,y) & (~MARKNOT) & (~MARK) );
	                       else { set( x, y, F_EMPTY | (p->type & COLOR)); if (p->type & COLOR) balance--; else balance++; }
	}
  return 1;
  }
return ((c & ~COLOR) == F_EMPTY);
} 

/*
 * Move player according to p->dx, p->dy
 */
void
domove( struct player *p )
{
int new;
if (p->type == T_NONE) return;
set( p->x, p->y, p->prev );
while (1)
  {
  new = FIELD( p->x + p->dx, p->y + p->dy );
  if (moveok( p, new )) break;
  p->dx = -p->dx; 
  new = FIELD( p->x + p->dx, p->y + p->dy );
  if (moveok( p, new )) break;
  p->dx = -p->dx; p->dy = -p->dy;
  new = FIELD( p->x + p->dx, p->y + p->dy );
  if (moveok( p, new )) break;
  p->dx = -p->dx;
  new = FIELD( p->x + p->dx, p->y + p->dy );
  if (moveok( p, new )) break;
  if ((p->type & ~COLOR) == T_PLAYER) { playerdies( (p->type & COLOR)>0 ); return; }
  p->type = T_NONE;
  return;
  }
p->prev = new;
p->x += p->dx;
p->y += p->dy;
set( p->x, p->y, F_ME );
if ((p->type & ~COLOR) == T_PLAYER)
  if ((p->prev & COLOR) != (p->type & COLOR))
    p->prev = F_TEMPORARY | (p->type & COLOR);
}

/*
 * Something wrong happened. Give it up.
 */
void
die( char *s )
{
statusline( s );
endwin();
msgctl( msgque, IPC_RMID, NULL );
exit(0);
}

/*
 * This is main event loop.
 * Note, that both copies do their own simulation of what happened, and
 * only messages how users decided are passed. This means for example no 
 * random numbers. (And no bugs in code :-)
 */
void
mainloop( int nexttick )
{
int first = 1;
while(1) {
  char c;
  int i, res;
  int now = ticks();
  if (nexttick > now)
    usleep( (nexttick - now) * 1000 );
  if (twoplayer && !first) {
    alarm(1);
    res = msgrcv( msgque, &message, sizeof(message), getpid(), MSG_EXCEPT );
    alarm(0);
    if (res<0) die( "Communication failed." );
    if (message.sender != other) die( "Someone else around here?" );
    if (message.type != M_GAME) die( "Expecting M_GAME." );
    otherpl->dx = message.data[0];
    otherpl->dy = message.data[1];
  } else first=0;
  for (i=0; i<numpl; i++)
    domove(& pl[i]);
  while (read( 0, &c, 1)==1)
    {
    switch( c ) {
    case 'a': case 'h': mypl->dx=-1; mypl->dy=0; break;
    case 's': case 'j': mypl->dx=0; mypl->dy=1; break;
    case 'd': case 'k':	mypl->dx=1; mypl->dy=0; break;
    case 'w': case 'u':	mypl->dx=0; mypl->dy=-1; break;
      }
    }
  if (twoplayer) {
    message.sender = getpid();
    message.type = M_GAME;
    message.data[0] = mypl->dx;
    message.data[1] = mypl->dy;
    res = msgsnd( msgque, &message, sizeof(message), 0 ); 
    if (res<0) die( "Message send returned %m" );
  }
  statusline( nexttick < now ? "Machine is too slow?!" : "Game continues!" );
  nexttick += SPEED;
  if (balance > goal) win = 0;
  if (balance < -goal) win = 1;
  if (win != -1) break;
  }
}

void
catchme( int i )
{
signal( SIGALRM, catchme );
}

void
main( int argc, char *argv[] )
{
int savex, savey, plnum;
fprintf( stderr, "M/Xonix V" VERSION ": " );

signal( SIGALRM, catchme );

initscr();
cbreak(); noecho();
sizex=COLS;
sizey=LINES;
endwin();
if (twoplayer) {
  fprintf( stderr, "screen %dx%d, ", sizex, sizey );
  msgque = msgget( 123, IPC_CREAT | 0666 );
  fprintf( stderr, "have msque %d (%m), I'm %d.", msgque, getpid() );
  savex = sizex; savey = sizey;
}
while (twoplayer)
{
#define WRONG( x ) { fprintf( stderr, x ); fprintf( stderr, "\n" ); msgctl( msgque, IPC_RMID, NULL ); exit(1); }
#define SEND { message.sender = getpid(); \
               res = msgsnd( msgque, &message, sizeof(message), 0 ); }
#define RECEIVE { alarm(15); \
                  res = msgrcv( msgque, &message, sizeof(message), getpid(), MSG_EXCEPT ); \
		  alarm(0); \
                  if (res<0) WRONG( " error %m" ); \
                  if (other && (message.sender != other)) WRONG( " someone else here?" ); \
		}

  int res, startgame;
  other = 0;
  fprintf( stderr, "\nNegotiating: " );
  message.type = M_HELLO;
  SEND;
  RECEIVE;
  if (message.type != M_HELLO) WRONG( "expecting M_HELLO!" ); 
  fprintf( stderr, "other is %ld, ", message.sender );
  other = message.sender;
  message.type = M_PARAMS;
  message.data[0] = savex;
  message.data[1] = savey;
  message.data[2] = startgame = time(NULL)+1;
  SEND;
  RECEIVE;
  if (message.type != M_PARAMS) WRONG( "expecting M_PARAMS!" ); 
  sizex = MIN( savex, message.data[0] );
  sizey = MIN( savey, message.data[1] );
  if (abs( startgame - message.data[2] )>2 ) WRONG( "more than 2 seconds difference!" ); 
  startgame = MAX( startgame, message.data[2] );
  fprintf( stderr, "size %dx%d, start in %ld ", sizex, sizey, startgame - time(NULL) );
  break;
}
fprintf( stderr, " done!\n" );
initscr();
cbreak(); noecho();
fcntl( 0, F_SETFL, O_NONBLOCK );
fieldframe();
startgame = time( NULL );
initpl( pl, 0 );
if (twoplayer) {
  initpl( pl+1, COLOR );
  plnum = (other < getpid());
  mypl =  pl+plnum;
  otherpl = pl+!plnum;
} else {
  mypl = pl;
}
goal = ((sizex-2)*(sizey-3))/6;
pl[0].lives = pl[1].lives = 25;
pl[2].x = 10; pl[2].y = 10; pl[2].dx = 1; pl[2].dy = 1; pl[2].type = T_ROBOT | COLOR; pl[2].prev = F_EMPTY | COLOR;
pl[3].x = 11; pl[3].y = 15; pl[3].dx = 1; pl[3].dy = -1; pl[3].type = T_ROBOT | COLOR; pl[3].prev = F_EMPTY | COLOR;
pl[4].x = sizex -10; pl[4].y = 10; pl[4].dx = -1; pl[4].dy = 1; pl[4].type = T_ROBOT; pl[4].prev = F_EMPTY;
pl[5].x = sizex -11; pl[5].y = 15; pl[5].dx = -1; pl[5].dy = -1; pl[5].type = T_ROBOT; pl[5].prev = F_EMPTY;
statusline( "Game starts!" );
mainloop( 0 );
  {
  char c;
  char *s = (win == plnum)?"Congratulations: YOU WON!!!":"Game ends and you lost :-(";
  statusline( s );
  sleep( 2 );
  while (read( 0, &c, 1)==1);
  die( s );
  }
}
