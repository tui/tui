#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "termint.h"
#include "igi.h"
#include "tcolors.h"

/* For debugging only	*/
/*#define info( a )    	*/
 
/* Character pseudocolors       */

#define chNeedsDraw     0x8000
#define chColorMask     0x7FFF

/* Indexs to string descriptors           */

#define stMove          0
#define stGotoXY        8
#define stHome          9
#define stLowerLeft	   10
#define stCariageReturn   11
#define stNewLine	   12
#define stOn           13
#define stOff          stOn + 6
#define stOffAll       stOn + 6 + 6
#define stColors	   stOn + 6 + 7
#define stCursor	   stColors + 2
#define stReset		   stCursor + 3
#define stScroll	   stReset  + 6

#define stMoveLeft      0 + stMove
#define stMoveRight     1 + stMove
#define stMoveLeftX     2 + stMove
#define stMoveRightX    3 + stMove
#define stMoveUp        4 + stMove
#define stMoveDown      5 + stMove
#define stMoveUpX       6 + stMove
#define stMoveDownX     7 + stMove

#define stBlink         0
#define stBold          1
#define stDim           2
#define stItalics       3
#define stReverse       4
#define stUnderline		5

#define stForegroundColor	0 + stColors
#define stBackgroundColor	1 + stColors

#define stNoCursor		0 + stCursor
#define stLineCursor	1 + stCursor
#define stBigCursor		2 + stCursor

#define stSoftReset1	0 + stReset
#define stSoftReset2	1 + stReset
#define stSoftReset3	2 + stReset

#define stHardReset1	3 + stReset
#define stHardReset2	4 + stReset
#define stHardReset3	5 + stReset

#define stScrollScreenUp	0 + stScroll	/* Order is critical: -1, +1, -n, +n	*/
#define stScrollScreenDown	1 + stScroll
#define stScrollScreenUpX	2 + stScroll
#define stScrollScreenDownX	3 + stScroll

#define stDeleteLine		4 + stScroll
#define stInsertLine		5 + stScroll
#define stDeleteLineX		6 + stScroll
#define stInsertLineX		7 + stScroll
#define stDefineWindow	    8 + stScroll
#define stDefineLines		9 + stScroll

#define stDeleteChar	   10 + stScroll
#define stInsertChar	   11 + stScroll
#define stDeleteCharX	   12 + stScroll
#define stInsertCharX	   13 + stScroll

#define stNum          51

typedef char *string;
word CommandInit[ stNum ] =
  { CP('l','e'), CP('n','d'), CP('L','E'), CP('R','I'),       	/* Moves left/right */
    CP('u','p'), CP('d','o'), CP('U','P'), CP('D','O'),			/* Moves up/down	*/
    CP('c','m'), CP('h','o'), CP('l','l'), CP('c','r'),	CP('n','w'),
	  /* GotoXY, Home, Lower Left, CR, newline */
    CP('m','b'), CP('m','d'), CP('m','h'), CP('Z','H'), CP('m','r'), CP('u','s'),  /* On         */
    CP('m','b'), CP('m','d'), CP('m','h'), CP('Z','R'), CP('m','r'), CP('u','e'), /* Off        */
    CP('m','e'),                          /* Off all    */
//    CP('A','F'), CP('A','B'),					   /* Colors	 */ 
    CP('S','f'), CP('S','b'),					   /* Colors	 */ 
    CP('v','i'), CP('v','e'), CP('v','s'),			   /* Cursors	 */
    CP('i','1'), CP('i','s'), CP('i','3'), CP('r','1'), CP('r','s'), CP('r','3'), /* Resets    */
    CP('s','f'), CP('s','r'), CP('S','F'), CP('S','R'),		   /* Scrolling	screen	*/
    CP('d','l'), CP('a','l'), CP('D','L'), CP('A','L'), 	   /* Insert/Delete line	*/
	CP('w','i'), CP('c','s'),		   							/* Defining regions	*/
	CP('d','c'), CP('i','c'), CP('D','C'), CP('I','C')		   /* Insert/Delete char	*/
  };

/* Usefull defines              */

#define TRY0( x ) Emit( Term, 0, x )
#define TRY1( x, a ) Emit( Term, 1, x, a )
#define TRY2( x, a, b ) Emit( Term, 2, x, a, b )
#define TRY4( x, a, b, c, d ) Emit( Term, 2, x, a, b, c, d )

#define TRYRET0( x ) if (TRY0( x )) return
#define TRYRET1( x, a ) if (TRY1( x, a )) return
#define TRYRET2( x, a, b ) if (TRY2( x, a, b )) return

#define CAN( x ) (DATA->Commands[ x ] != NULL)

#define DiffLess( x, y, z ) ( (abs((x)-(y)))<=(z))

#define DATA ((AnsiData *) Term->Data)
#define CURX DATA->CurX
#define CURY DATA->CurY
#define SCRX Term->ScrX
#define SCRY Term->ScrY
#define SCRN (((AnsiData *) Term->Data)->Screen)

#define DIRTYX1 DATA->DirtyX1  /* These specify "Dirty" range which needs to be redrawn        */
#define DIRTYX2 DATA->DirtyX2
#define DIRTYY1 DATA->DirtyY1
#define DIRTYY2 DATA->DirtyY2
#define MAKEDIRTY( a, b, c, d ) if ( MakeDirty( Term, a, b, c+a, d+b ) ) return;

#define READY 1         /* Is terminal at least somehow ready to receive next char?     */

/* These defines are needed for scrolling	*/
#define SCROLLED( a ) return a
#define CANSCRLY( x ) CAN( x + (dy>0))
#define SCLEFT 0, y, x, sy
#define SCRIGHT x + sx, y, SCRX - x - sx, sy
#define SCDOWN 0, y+sy, SCRX, SCRY - y - sy
#define SCUP 0, 0, SCRX, y
#define SCIN x, y, sx, sy

/* For ScreenScroll function	*/
#define ssMove  0
#define ssMoveMark  1
#define ssMark  2
#define ssCount 3

typedef struct tag_AnsiData {
  /* Selected color             */
  byte CurFore, CurBack;

  /* Current intensity			*/
  byte CurIntensity;
       #define ciDim stDim
       #define ciBold stBold
       #define ciReverse stReverse
       #define ciNormal 10	/* These two must not be equal to any else ciXX, nothing else req'd */
       #define ciColor 11	

  /* Actual (not simulated) cursor position     */
  int CurX, CurY;
#define UNKNOWN -1
  /* When actual position is unknown	*/

  /* Current update positions   */
  int CurUpX, CurUpY, CurUpPhase;	/* Note: CurUpX MAY be <0!	*/
#define PHASELAST 3

  /* Current dirty positions	*/
  int DirtyX1, DirtyY1, DirtyX2, DirtyY2;

  /* Screen image               */
  charinfo *Screen;

  /* Current scrolling area (lines)	*/
  int Line1, Line2;

  /* Current window */
  int WinX, WinY, WinSX, WinSY;

  /* Description strings        */
  string Commands[stNum];

  /* Fast moves by one character? */
  byte Fast1Moves, FastNMoves;

  /* Should colors be used?		*/
  byte DoColors;

  /* Do scrolls fill unused space with spaces of current background color? */
  byte ScrollFills;

  } AnsiData;
