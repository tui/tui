/* Note: igitermcap must include "nc"	*/

/*
  This should handle some terminals whitch are described in termcap database.
  Terminal must be capable of:
    1) Moving cursor anywhere
    2) Overwrite characters

  So anything that prints on paper is disallowed.

  Pavel 96-5-27 One of first versions, no scrolling, no colors, nothing
  Pavel 96-5-30 Modified to use termcap library
  Pavel 96-6-2  Terminal now can do anything (except colors/attributes), but 
                how. Scrolling will be done by reprinting test, fill chars 
				also... Optimizing needed. Dirty area needed. 
  Pavel 96-6-5  Colors/attributes work, dirty area added.
  Pavel 96-6-6  Scrolling improved. Trouble is, that no terminal known to me
                can support scrolling of window. Another big trouble is that
				no terminal known to me can do "repeat character"!
  Pavel 96-7-9	Adding heuristics 'is it worth scroll?'
  Pavel 96-7-10	Adding horizontal scrolling.
                [lots of debugging]
  Pavel 96-8-4 	Enhancing (or debugging? :-) cursor motion
  Pavel 96-9-16 Adding "priority of area"
  Pavel 96-11-11 Priority of area hopefully works

  Everything needed by this file was moved to termansi.h...	

  It is needed to pass options (command line) to this source - for
  example I need:
    disable relative moves 
	no cursor optimalization (always move absolutely)
	cursor paranoia option (move even if you don't need to)
	no colors
	no dim
	no reverse video
	no bold
	no highlights
	no scroll optimalization
	no scrolls
	force ANSI colors
	paranoia option
*/
#include "termansi.h"
#define info( x )

/* Internal functions   */

/* These functions generaly translate requests into strings     */

static int
TermPuts( PTerm Term, char *s )
{
printf( "%s", s ); 
// fflush( stdout ); usleep( 1 );
}

static int
Emit( PTerm Term, int Args, int Num, ... )
{
va_list ap;
int buf[512];
int i;
int ar[4];
char *s, *t;

va_start( ap, Num );
s = DATA->Commands[ Num ];
if (!s) { va_end( ap );  return 0; }

if (*s==0) die( "Computers should not use telepathy" );

for (i=0; i<Args; i++) ar[i] = va_arg( ap, int );
snprintf( buf, 512, s, ar[0], ar[1], ar[2], ar[3] );

TermPuts( Term, (char *) buf );
va_end( ap );
return 1;
}

static void
EmitChar( PTerm Term, int ch )
{
char s[2];
s[0] = ch & 0xFF;
s[1] = 0;

TermPuts( Term, s );
CURX++;
if (CURX >= SCRX) CURX = UNKNOWN;
}

static void
FlushEmit( PTerm Term )
{
fflush( stdout );
}

/* This code will probably assume, that if there's termcap entry for 
   function N times, there's also code for function one time		*/
static void
Repeat( PTerm Term, int Func1, int num )
/* Relies on '-1, +1, -n, +n' sequence in database!		*/
{
int i;
switch (num)
  {
  case 0:
	return;
  case 1:
	TRYRET0( Func1+1 );
	break;
  case -1:
	TRYRET0( Func1 );
	break;
  }
if (num<0) TRYRET1( Func1+2, -num );
      else TRYRET1( Func1+3, num );
for (i=0;i<num;i++) TRY0( (num<0)?Func1:Func1+1 );
}

                                                                    /* Cursor movement	*/
/* This must not fail!  */
static void
MoveToX( PTerm Term, int x )
{
int LastX, i;
/* Nothing to do?       */
if ( x==CURX ) return;

/* What about CR when moving to begining of line?	*/
if ( !x ) TRYRET0( stCariageReturn );

Repeat( Term, stMoveLeft, x - CURX );
CURX = x;
return;
}

static void
MoveToY( PTerm Term, int y )
{
int LastY, i;
/* Nothing to do?       */
if ( y==CURY ) return;

LastY = CURY;
CURY  = y;      /* This must not fail, so we assume it will work        */

/* Ok, normal move...   */
Repeat( Term, stMoveUp, y - LastY );
return;
}

/* Also capabilities like ch (move to column) could be used, but since
   terminals that can use them can mostly do move relative too, ignored.
   Saving and recalling positions is probably not very usefull for us.	*/

static inline void
ttGotoXY( PTerm Term, int x, int y )
{
if ((x >= SCRX) || (x < 0) || (y >= SCRY) || (y < 0))
  die( "Invalid coordinates passed to ttGotoXY()!\n" );
if ((CURX == UNKNOWN) || (CURY == UNKNOWN))
  {
  TRYRET2( stGotoXY, y, x );
  }
else
  {
  if ((CURX==x) && (CURY==y)) return;
  /* What about Home?			*/
  if ( !x && !y ) TRYRET0( stHome );
  /* What about lower left?		*/
  if ( !x && (y == SCRY-1)) TRYRET0( stLowerLeft );
  /* What about newline?		*/
  if ( !x && (y == CURY+1)) TRYRET0( stNewLine );
  /* Ok, we'll move normally	*/

  if (DATA->Fast1Moves && DiffLess( x, CURX, 1 ) && DiffLess( y, CURY, 1 ) )
    {
    MoveToX( Term, x );
    MoveToY( Term, y );
    return;
    }
  /* If it is near at least in one axis, we'll TRY to move there by MoveTo[XY]    */
  if (DATA->FastNMoves && ( DiffLess( x, CURX, 1 ) || DiffLess( y, CURY, 1 ) ))
    {
    MoveToX( Term, x );
    MoveToY( Term, y );
    return;
    }
  }
TRYRET2( stGotoXY, y, x );
MoveToX( Term, x );
MoveToY( Term, y );
return;
}

static void
tGotoXY( PTerm Term, int x, int y )
{
ttGotoXY( Term, x, y );
#if 0
CURX = x;
CURY = y;
#else
 CURX = UNKNOWN;
 CURY = UNKNOWN;
#endif
}

/* This means goto specified y, we do not   */
/*   care if you'll change X. This will 	*/
/* be used for optimalizations.				*/
static void
GotoAnyXY( PTerm Term, int y )
{
tGotoXY( Term, CURX, y );
}

/* Term ansi will use always only one of following attributes:
     Dim, Bold, Reverse
*/
static void
OffAll( PTerm Term )
{
if ( DATA->CurIntensity == ciNormal ) return;
if (TRY0( stOffAll )) { DATA->CurIntensity = ciNormal; return; }
TRY0( stOff + DATA->CurIntensity );
DATA->CurIntensity = ciNormal;
}

static void
SetIntensity( PTerm Term, int Int )
{
if (Int == DATA->CurIntensity) return;

OffAll( Term );
if (Int != ciNormal)
  if (CAN( stOn + Int ))
    {
    TRY0( stOn + Int );
    DATA->CurIntensity = Int; 
	}
}

static void
SetColor( PTerm Term, charinfo *c )
{
tcolor grey = TGREY( c->color );
int fore, back, fore1, back1;
int fgrey;

if ((TFORE( c->color )) == (TBACK( c->color ))) grey = 0;
                                           else if (!grey) grey = 1;
fgrey = TFORE( grey );
fore = TFORE( c->color );
back = TBACK( c->color );
fore1 = TONEBIT( fore );
back1 = TONEBIT( back );

if ((fore1 == back1) && (grey))
  if (CAN(stOn + stDim)) { SetIntensity( Term, ciDim ); return; }
                    else { if (back1>0) back1--; else fore1++;  }

if (DATA->DoColors)
  {
  if ((DATA->CurIntensity != ciColor) && (DATA->CurIntensity != ciNormal))
    SetIntensity( Term, ciNormal );
  if ( (DATA->CurFore != fore1) || (DATA->CurIntensity != ciColor) ) 
    TRY1( stForegroundColor, fore1 );
  if ( (DATA->CurBack != back1) || (DATA->CurIntensity != ciColor) ) 
    TRY1( stBackgroundColor, back1 );
  DATA->CurFore = fore1;
  DATA->CurBack = back1;
  DATA->CurIntensity = ciColor;
  return;
  }

if (fgrey < TBACK(grey))
  { SetIntensity( Term, ciReverse ); return; }
if (fgrey <= 3) 
  { SetIntensity( Term, ciDim ); return; }
if (fgrey <= 6)
  { SetIntensity( Term, ciNormal ); return; }
SetIntensity( Term, ciBold );
return;
}

static void
tReset( PTerm Term, int i )		/* i == 0.. soft reset, i == 1.. hard reset	*/
{
TRY0( stSoftReset1 + (3*i) );
TRY0( stSoftReset2 + (3*i) );
TRY0( stSoftReset3 + (3*i) );
}

/* This line is so big - guess why - to make itself look like LINE	*/
/* Following functions deal with memory image of screen */

static int
MakeDirty( PTerm Term, int x1, int y1, int x2, int y2 )
{
if (( x2 > SCRX ) || ( y2 > SCRY)) 
  { 
  info("MAKEDIRTY: Attempt to access out of range." ); 
  return 1; 
  }
DATA->CurUpX = MIN( DATA->CurUpX, x1-1 );
DATA->CurUpY = MIN( DATA->CurUpY, y1 );
DATA->CurUpPhase = 0;
DATA->DirtyX1 = MIN( DATA->DirtyX1, x1 );
DATA->DirtyY1 = MIN( DATA->DirtyY1, y1 );
DATA->DirtyX2 = MAX( DATA->DirtyX2, x2 );
DATA->DirtyY2 = MAX( DATA->DirtyY2, y2 );
return 0;
}

static inline charinfo *
ScreenPos( PTerm Term, int x, int y )
{ return (SCRN + SCRX*y + x ); }

static inline void
SetFlags( charinfo *t, int flags )
{ t->color = (t->color & chColorMask) | flags; }

inline static int
GetFlags( charinfo *t, int flags )
{ return (t->color & flags); }

inline static int
SameColor( PTerm Term, int c1, int c2 )
{ return ((c1 & chColorMask) == (c2 & chColorMask)); }

inline static int
SameChar( PTerm Term, charinfo *s, charinfo *t )
{ return ((t->ch == s->ch) && (SameColor( Term, t->color, s->color))); }

static inline int  /* Does specified character look like space?	*/
IsSpace( PTerm Term, int ch ) 
{ return (ch==' '); }

static void
ScreenFill( PTerm Term, int x, int y, int sx, int sy, charinfo *c )
{
charinfo *t = ScreenPos( Term, x, y );
charinfo *u;
int i,j;
/* 'Dirty' range should be updated      */
for (j=0; j<sy; j++)
  {
  u = t;
  for (i=0; i<sx; i++)
    {
    if (!SameChar(Term,c,t))
      { *t = *c; SetFlags( t, chNeedsDraw ); }
    t++;
    }
  t = u + SCRX;
  }
}

/* Marks screen for updating, but does not touch "dirty" area		*/
static void
ScreenTouch( PTerm Term, int x, int y, int sx, int sy )
{
charinfo *t = ScreenPos( Term, x, y );
charinfo *u;
int i,j;

for (j=0; j<sy; j++)
  {
  u = t;
  for (i=0; i<sx; i++)
    {
    SetFlags( t, chNeedsDraw );
    t++;
    }
  t = u + SCRX;
  }
}

                                                             /* Scrolling functions	*/
static void
DefineLines( PTerm Term, int l1, int l2 )
{
if ((DATA->Line1 == l1) && (DATA->Line2 == l2)) return;
TRY2( stDefineLines, l1, l2 );
}

static void
DefineWindow( PTerm Term, int x, int y, int sx, int sy )
{
if ((DATA->WinX == x) && (DATA->WinY == y) &&
    (DATA->WinSX == sx) && (DATA->WinSY == sy)) return;
TRY4( stDefineWindow, x, y, sx + x, sy + y );
}

static int
ScreenScroll( PTerm Term, int x, int y, int sx, int sy, int dx, int dy, int mark )
{
int i, line, xcorr, ycorr;
charinfo *src, *dst;
int CharsTouched = 0;

//info("ScreenScroll: %d\n", mark);

if ((abs(dx)>sx) || (abs(dy)>sy)) return 0;
if ((sx < 1) || (sy < 1)) return 0;
sx -= abs(dx);
sy -= abs(dy);
src = ScreenPos( Term, x, y );
dst = src;
if (dx>0) dst += dx;
     else src -= dx;
if (dy>0) dst += SCRX * dy;
     else src -= SCRX * dy;

if (dy>0) { i = SCRX * (sy-1); line = -SCRX; }
     else { i = 0;             line = +SCRX; }
src += i;
dst += i;
if (mark==ssMove)
  {
  while (sy>0)
    {
    memmove( dst, src, 4*sx );
    src += line;
    dst += line;
    sy--;
    }
  }
else
  {
  while (sy>0)
    {
    for( i = 0; i < sx; i++ )
      {
	  int j;
	  
	  if (dx>0)	j = sx-i-1;
           else j = i;
	  if( !SameChar( Term, src+j, dst+j ) ) 
        {
		switch (mark) 
		  {
		  case ssMoveMark:
            *(dst+j) = *(src+j);
		  case ssMark:
	        SetFlags( dst+j, chNeedsDraw );
			break;
		  case ssCount:
            if (GetFlags( dst+j, chNeedsDraw )==0) CharsTouched++;
		  }
        }
      }
    src += line;
    dst += line;
    sy--;
    }
  }
return CharsTouched;
}

void
ForceRedraw( PTerm Term )
{
MAKEDIRTY( 0, 0, SCRX, SCRY );
ScreenTouch( Term, 0, 0, SCRX, SCRY );
}

static int
CountValidNoSpace( PTerm Term, int x, int y, int sx, int sy, int color )
/* Color -1 is special color, which can not be on screen				*/
{
int num	= 0;
charinfo *t = ScreenPos( Term, x, y );
charinfo *u;
int i,j;

for (j=0; j<sy; j++)
  {
  u = t;
  for (i=0; i<sx; i++)
    {
    if ( !GetFlags( t, chNeedsDraw )) num++;
/*	if (color == -1) num++;
	            else
	  if (!IsSpace( Term, t->ch ) || !SameColor( Term, TBACK(color), TBACK(t->color))) num++; */
    t++;
    }
  t = u + SCRX;
  }

return num;
}

/* Touches area after vertical scroll	*/
static void
Scrolled( PTerm Term, int x, int y, int sx, int sy, int dy, int color )
{
if (dy>0) ScreenTouch( Term, x, y, sx, dy );
     else ScreenTouch( Term, x, y+sy+dy, sx, -dy );
}

static void
ScrollIt( PTerm Term, int dy )
{
Repeat( Term, stScrollScreenUp, dy );
}

/*	Only what should be scrolled was scrolled 		-> SCROLLED(0)
	Area left & right of wanted area was scrolled	-> SCROLLED(1)
	Area down of wanted area was scrolled			-> SCROLLED(2)
	Area top of wanted area was scrolled			-> SCROLLED(3)
	Nothing was scrolled							-> SCROLLED(4)
	Do not do anything, I updated it				-> SCROLLED(5)  */
static int
AreaScroll( PTerm Term, int x, int y, int sx, int sy, int dx, int dy, int color )
{
int Good;
int Bad = 0;
int i;
if (!DATA->ScrollFills) color = -1;

if (dx && dy) return 4;
  /* We will not try to scroll in both axis, it would probably not do any good.	*/

if (dx) 												/* Horizontal scroll	*/
  {
  if (!CAN( stDeleteChar + (dx>0))) return 4;
  for(i=y;i<y+sy;i++)
	{
	Good = ScreenScroll( Term, x, i, sx, 1, dx, 0, ssCount );
	Bad = ScreenScroll( Term, x + sx, i, SCRX - x - sx, 1, dx, 0, ssCount );
	if (Good < Bad + 1 )
	  ScreenScroll( Term, x, i, sx, 1, dx, 0, ssMoveMark );
	else
	  {
	  tGotoXY( Term, x, i );
	  Repeat( Term, stDeleteChar, dx );
	  CURX = CURY = UNKNOWN;
	  ScreenScroll( Term, x, i, sx, 1, dx, 0, ssMove );
	  if (dx<0) ScreenTouch( Term, x + sx + dx, i, SCRX - x - sx - dx, 1 );
           else
	    {
		ScreenTouch( Term, x, i, dx, 1 );
		ScreenTouch( Term, x + sx, i, SCRX - x - sx, 1 );
		}
	  }
	}
  return 5;
  }
else
  {
  Good = ScreenScroll( Term, x, y, sx, sy, dx, dy, ssCount );
  if (Good < Bad + 15) return 4;
  if ( (x || (sx != SCRX)) && CAN( stDefineWindow ) && CANSCRLY( stScrollScreenUp ))
    {
    DefineWindow( Term, x, y, sx, sy );
    ScrollIt( Term, dy );
	ScreenScroll( Term, x, y, sx, sy, 0, dy, ssMove );
    SCROLLED( 0 );
    }
  Bad += ScreenScroll( Term, SCLEFT, 0, dy, ssCount );		/* Area left of scrolled one	*/
  Bad += ScreenScroll( Term, SCRIGHT , 0, dy, ssCount );		/* Area right 	*/
  if (Good < Bad + 15) return 4;  
  if ( (y + sy) == SCRY )
    {
    if (CANSCRLY(stDeleteLine)) 
      {
	  GotoAnyXY( Term, y );
	  Repeat( Term, stDeleteLine, dy);
	  SCROLLED(1);
	  }
    if (!y && !CAN( stDefineLines ) && CANSCRLY( stScrollScreenUp ))
      {
      ScrollIt( Term, dy );
	  SCROLLED(1);
	  }
    }
  if (CAN( stDefineLines ))
    {
    DefineLines( Term, y, y+sy );
    ScrollIt( Term, dy );
    SCROLLED(1);
    }
  Bad += ScreenScroll( Term, SCDOWN, 0, dy, ssCount );	/* Area down	*/
  if (Good < Bad + 15) return 4;  
  if (CANSCRLY(stDeleteLine)) 
    {
    GotoAnyXY( Term, y );
	Repeat( Term, stDeleteLine, abs(dy));
	SCROLLED(2);
	}
  Bad += ScreenScroll( Term, SCUP, 0, dy, ssCount );	/* Area up of scrolled one	*/
  if (Good < Bad + 15) return 4;
  if (CANSCRLY(stScrollScreenUp))
	{
	ScrollIt( Term, dy );
	SCROLLED(3);
    }
  }
return 4;
}

void
tMoveBlock( PTerm Term, int x, int y, int sx, int sy, int nx, int ny, int color )
{
int dx = nx - x, dy = ny - y;

MAKEDIRTY( 0, 0, SCRX, SCRY );
/* Scrolling is native for TermCap terminals: they usualy can not do any kind of
   move block.																		*/
sx += abs( nx - x );
sy += abs( ny - y );
x = MIN( x, nx );
y = MIN( y, ny );
switch (AreaScroll( Term, x, y, sx, sy, dx, dy, color ))
  {
  case 3:
	Scrolled( Term, SCUP, dy, color );
	ScreenScroll( Term, SCUP, dx, dy, ssMark );
  case 2:
	Scrolled( Term, SCDOWN, dy, color);
	ScreenScroll( Term, SCDOWN, dx, dy, ssMark );
  case 1:
	Scrolled( Term, SCLEFT, dy, color );
	ScreenScroll( Term, SCLEFT, dx, dy, ssMark );
	Scrolled( Term, SCRIGHT, dy, color );
	ScreenScroll( Term, SCRIGHT, dx, dy, ssMark );
  case 0:
	Scrolled( Term, SCIN, dy, color );
	ScreenScroll( Term, SCIN, dx, dy, ssMove );
	break;	
  case 4: 
	ScreenScroll( Term, SCIN, dx, dy, ssMoveMark );
	break;
  }
}

static void
ScreenPut( PTerm Term, int x, int y, charinfo *c )
{
charinfo *t = ScreenPos( Term, x, y );

MAKEDIRTY( x, y, 1, 1 );
if (!SameChar(Term, c, t))
  { *t = *c; SetFlags( t, chNeedsDraw ); }
}

#define IN( a, b, c ) ( (a<=b) && (b<=c) )
/* Moves to next char to update.
   Returns 1 if update done       */
static inline int
UpToLine( PTerm Term )
{
if (!IN( DIRTYY1, DATA->CurUpY, DIRTYY2 )) return 2;
if (++DATA->CurUpX >= DIRTYX2) 
  if (++DATA->CurUpY >= DIRTYY2) return 2;
	                       else { DATA->CurUpX = DIRTYX1-1; return 1; }
return 0;
}

static inline int 
SingleLine( PTerm Term, int i )
{
DATA->CurUpY = Term->CursorY + i;
if (UpToLine( Term )) { DATA->CurUpPhase++; return 1; }
                 else return 0;
}

static /*inline*/ int
UpdateNext( PTerm Term )
{
//printf( "Update next %i %i %i", DATA->CurUpX, DATA->CurUpY, DATA->CurUpPhase );

switch( DATA->CurUpPhase )
  {
  case 0: if (!SingleLine( Term, 0 )) return 0;
  case 1: //info( "phase should == 1" );
          if (!SingleLine( Term, -2 )) return 0;
  case 2: if (!SingleLine( Term, -1 )) return 0;
  case 3: if (!SingleLine( Term, +1 )) return 0;
  case 4: if (!SingleLine( Term, +2 )) return 0;
          DATA->CurUpY = DIRTYY1;
          DATA->CurUpX = DIRTYX1-1;
  case 5: //info( "phase=5" );
          if (UpToLine( Term )<2) return 0;
  }
//info( "update should be done.\n" );
return 1; /* Done */
}

/* Functions to interface this module with other parts of system */
/* If you wonder, why this is so long comment, then it is to make parts of      */
/*   source visually separated.                                                 */

/* This one should be called            */
/* frequently to update screen...       */
static int	/* Return 0 = done, return 1 = I need to be called again	*/
tPoll( PTerm Term )
{
charinfo *c;

//info( "In Poll" );
while (READY)
  {
  while (!GetFlags( ScreenPos( Term, DATA->CurUpX, DATA->CurUpY ), chNeedsDraw ))
    if (UpdateNext( Term ))
      {
      if ((CURX != Term->CursorX) || (CURY != Term->CursorY))
        tGotoXY( Term, Term->CursorX, Term->CursorY );
      FlushEmit( Term );
//	  info( "End Poll" );
      return 0;
      }
  SetFlags( ScreenPos( Term, DATA->CurUpX, DATA->CurUpY ), 0 );
  tGotoXY( Term, DATA->CurUpX, DATA->CurUpY );
  SetColor( Term, ScreenPos( Term, DATA->CurUpX, DATA->CurUpY ) );
  EmitChar( Term, ScreenPos( Term, DATA->CurUpX, DATA->CurUpY )->ch );
  }
//info( "End Poll" );
return 1;
}

static void
tCursorXY( PTerm Term, int x, int y )
{
Term->CursorX = x;
Term->CursorY = y;
return;
}

static void
tPutChar( PTerm Term, int x, int y, int ch, int color )
{
charinfo c;
c.ch = ch;
c.color = color;
if ((x>=SCRX) || (y>=SCRY))
  {
  info( "tPutChar: invalid coordinates passed!" );
  return;
  }
ScreenPut( Term, x, y, &c );
}

static void
tFillChar( PTerm Term, int x, int y, int sx, int sy, int ch, int color )
{
charinfo c;
MAKEDIRTY( x, y, sx, sy );
c.ch = ch;
c.color = color;
ScreenFill( Term, x, y, sx, sy, &c );
}

static void
tCursorType( PTerm Term, int t )
{
if ((t != ctDefault) && (t != ctBig)) 
  if (t != ctUnderLine) t = ctBig;
                   else t = ctDefault;
if (t == Term->CursorNow) return;
switch (t)
  {
  case ctInvisible:
    if (TRY0( stNoCursor )) Term->CursorNow = t;
    break;
  case ctDefault:
    if (TRY0( stLineCursor )) Term->CursorNow = t;
    break;
  case ctBig:
    if (TRY0( stBigCursor )) Term->CursorNow = t;
    else if (TRY0( stLineCursor )) Term->CursorNow = t;
    break;
  }
return;
}

/* Functions callable from outside      */

void
PrintControl( PTerm Term )
{
int i;
printf( "Control sequences are :\n" );
for (i=0; i<stNum; i++)
  {
  printf( "%d:", i );
  if (DATA->Commands[i]) puts( DATA->Commands[i]+1 );
                    else printf( "[none]\n" );
  }
}

void
tDone( PTerm Term )
{
int i;
info( "Termcap terminal going down..." );
SetIntensity( Term, ciNormal );
tReset( Term, 0 );
tCursorType( Term, ctDefault );

DATA->Commands[ stForegroundColor ] = NULL;
DATA->Commands[ stBackgroundColor ] = NULL;
#if 0
/* We assigned static strings, can't just free them */
for (i=0; i<stNum; i++)
  if (DATA->Commands[i]) xfree( DATA->Commands[i], strlen( DATA->Commands[i] ) );
#endif
info( "done\n" );
}

//#define info printf
int
InitTermAnsi( PTerm Term, char *s )
{
int i;
char buf[1024] = "Termcap terminal - \0";

/**** Should send ti sequence on entry and te on exit!!! */

strcat( buf, s );
Term->Desc    = stralloc( buf );
info( "Initing " ); info( buf ); info( "..." );

#if 0
if (tgetent( buf, s ) != 1)
  {
  warn( "Can not init terminal '%s'.", s );
  return 0;
  }
#endif

Term->ScrX      = 80; /* tgetnum( "co" ); */
Term->ScrY      = 24;  /* tgetnum( "li" ); */

Term->Type    			= ttTermCap;
Term->Capabilities      = 0;

Term->Data    = xcalloc( sizeof( AnsiData ));

Term->CursorXY  = tCursorXY;
Term->PutChar   = tPutChar;
Term->FillChar  = tFillChar;
Term->MoveBlock = tMoveBlock;
Term->Close     = tDone;
Term->Poll      = tPoll;
Term->CursorType= tCursorType;

Term->CursorNow = ctUnderLine;

info( "initing strings..." );
for (i=0; i<stNum; i++)
  {
#if 1
    DATA->Commands[i] = NULL;
    continue;
#else
  char s[5];
  char buff[1024], *xbuff, *ybuff;

  s[0] = (CommandInit[i] >> 8) & 0xFF;
  s[1] = CommandInit[i] & 0xFF;
  s[2] = 0;
  xbuff = buff;
  ybuff = tgetstr( s, &xbuff );
  xbuff = ybuff;

  if (xbuff)
    DATA->Commands[i] = stralloc( xbuff );
  else
    DATA->Commands[i] = NULL;
#endif
  }
DATA->Commands[ stForegroundColor ] = "\033[3%dm";
DATA->Commands[ stBackgroundColor ] = "\033[4%dm";
DATA->Commands[ stGotoXY ] = "\033[%d;%df";
DATA->Commands[ stSoftReset1 ] = "\033c";
DATA->Screen = xcalloc( SCRX * SCRY * 4 );

tReset( Term, 1 );
info( "[" );
if ( CAN( stGotoXY ) ) info( "gotoXY," );
DATA -> Fast1Moves = 0;
DATA -> FastNMoves = 0;
if ( CAN( stMoveLeft ) && CAN( stMoveRight ) && CAN( stMoveUp ) && CAN( stMoveDown ))
  {
  DATA -> Fast1Moves = 1;
  info( "moves1," );
  }
if ( CAN( stMoveLeftX ) && CAN( stMoveRightX ) && CAN( stMoveUpX ) && CAN( stMoveDownX ))
  {
  DATA -> FastNMoves = 1;
  info( "movesN," );
  }
//DATA -> Fast1Moves = 0;
//DATA -> FastNMoves = 0;
if ( CAN( stForegroundColor ) && CAN( stBackgroundColor ) ) 
  {
  TRY1( stForegroundColor, 1 ); info( "c" );
  TRY1( stForegroundColor, 2 ); info( "o" );
  TRY1( stForegroundColor, 3 ); info( "l" );
  TRY1( stForegroundColor, 4 ); info( "o" );
  TRY1( stForegroundColor, 5 ); info( "r" );
  TRY1( stForegroundColor, 6 ); info( "s" );
  TRY1( stForegroundColor, 7 ); info( "," );
  DATA->DoColors = 1;
  }
if ( CAN( stOn + stDim ) ) { SetIntensity( Term, ciDim ); info( "dim," ); }
if ( CAN( stOn + stBold )) { SetIntensity( Term, ciBold ); info( "bold," ); }
if ( CAN( stOn + stReverse )) { SetIntensity( Term, ciReverse ); info( "reverse," ); }
SetIntensity( Term, ciNormal );
if ( CAN( stScrollScreenDown ) && CAN( stScrollScreenUp ) ) info( "scroll +-1," );
if ( CAN( stScrollScreenDownX ) && CAN( stScrollScreenUpX ) ) info( "scroll +-N," );
if ( CAN( stInsertLine ) && CAN( stDeleteLine ) ) info( "in/del line," );
if ( CAN( stInsertLineX ) && CAN( stDeleteLineX ) ) info( "in/del N lines," );
if ( CAN( stInsertChar ) && CAN( stDeleteChar ) ) info( "in/del char," );
if ( CAN( stInsertCharX ) && CAN( stDeleteCharX ) ) info( "in/del N chars," );

if ( CAN( stDefineLines )) info( "set lines," );
if ( CAN( stDefineWindow )) info( "set window," );
info( "done]..." );
CURX = CURY = UNKNOWN;

info( "done\n" );
return 1;
}
