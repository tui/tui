#ifndef TERMINT_H
#define TERMINT_H

#include "igi.h"

/* Cursor types                 */
/* Note that no termcap terminal can support other cursors than invisible/underline/solid	*/
/* and even linux console is not clever enough to support solid cursor?!?					*/

#define ctInvisible     0
#define ctDefault		1
#define ctBig			2
#define ctUnderLine     3
#define ctLowerHalf     4
#define ctUpperLine     5
#define ctUpperHalf     6
#define ctLowerThird	7
#define ctMiddleThird	8
#define ctUpperThird	9
#define ctSolid        10

/* Terminal types               */

#define ttDirectVideo   0		/* Text terminal, for example PC				*/
#define ttTermCap		1
#define ttLayers		2		/* Pseudo terminal, each layer has one			*/

/* Capabilities bit flags		*/

/* Corresponding bit set means, that it MAY BE possible to do that. It is not	*/
/* guaranteed that it will work.												*/

#define cpCanHideCursor		0x1
#define cpCanBigCursor		0x2
#define cpCanOtherCursors	0x4
#define cpHasIntensity		0x8

#define cpMonochrome    0x10    /* Color changes not supported */
#define cpReverse       0x20    /* No color changes, only reverse video supported */
#define cpHighlight     0x40    /* Reverse video & highlight */
#define cpGrey          0x80    /* Real gray-scale */
#define cpColor         0x100   /* Full colors */ 

#define cpHasFramesChars	0x0200
#define cpIBMExtendedChars	0x0400

#define cpUnderline		0x1000
#define cpBold			0x2000
#define cpItalics		0x4000
#define cpShadow		0x8000

/* Font description				*/

typedef struct Font {
  char *Desc;		    /* Description of this font						*/
  /* Every font belongs to some VideoMode -> read character sizes from	*/
  /* that point															*/
  char Name[8];			/* Name of font, max 8 characters, should 		*/
                        /* identify font for outside world				*/
  int FirstChar, NumChars;	
                        /* First character in font, number of 
						   characters in font							*/
  word *Faces;			/* Array of faces for specified font, word 	
						   should be enough to store description of 
						   single character								*/
  } Font;

/* Video mode description       */

struct VideoMode;
typedef struct VideoMode *PVideoMode;

typedef struct VideoMode {
  PVideoMode *Next;
  int ScrX, ScrY;               /* Screen size          */
  int CharX, CharY;             /* Char size            */
  int CellX, CellY;             /* Character cell size  */
  int Flags;                    /* Some flags, for example      */
                                /* how significant this mode is */
  int HFreq, VFreq;		/* in kHz, 0 means unknown						*/
  char *Desc;           /* Description	first part until space should 	
						   be enough to identify it					    */
  char *Set;            /* How to set it                                */
  } VideoMode;

/* Terminal structure           */

struct Terminal;
typedef struct Terminal *PTerm;

typedef struct Terminal {
  /* These are R/O for sure. I'm even not sure if you have permission	
	 to read them...												 */
  int CursorX, CursorY;   /* Cursor position (0,0 = up left corner)  */
  int CursorNow;          /* Cursor type  - one of ctXXXX constants	 */
  int BlankNow;           /* Is screen blanked? 0..no, 1..7 how much */
  int ScrX, ScrY;         /* Screen size          */
  int Type;               /* What type it is? Direct video,
                                        curses like, ...?            */
  ulg Capabilities;       /* Bitfield capabilities                   */
  void *Data;             /* Internal data, Type - specific          */
  char *Desc;             /* Name / brief description                */
                          /* It's part before first space should
							 be enough to identify terminal			 */
  

  PVideoMode Modes;
  PVideoMode Current;

  /* Note: If some terminal does not support some of this functions,    */
  /*       correspondent pointers are set to nofunc, not NULL !! !!!    */

  void (*Close) ( PTerm Term );
                                /* Destroys terminal            */

  void (*CursorXY) ( PTerm Term, int x, int y );
                                /* Moves cursor                 */

  void (*CursorType) ( PTerm Term, int t );
                                /* Sets cursor type (ctXXX)     */

  void (*PutChar) ( PTerm Term, int x, int y, int ch, int color );
                                /* Puts single character on screen      */

  void (*PutString) ( PTerm Term, int x, int y, int num, word *ch, word *color );
                                /* Put colorfull string on screen		*/

  void (*FillChar) ( PTerm Term, int x, int y, int sx, int sy, int ch, int color );
                                /* Fills area on screen                 */

  void (*MoveBlock) ( PTerm Term, int x, int y, int sx, int sy, int nx, int ny, int color );
                                /* Moves block given by x,y,sx,sy       
                                     to area starting at nx, ny.        
                                   May have to clear some area, in such	
                                     case it will fill it with specified
									 color								*/

  int (*Poll) ( PTerm Term );
                                /* Some terminals may need you to call
								   this often enough                    */

  /* Other pointers to procedures should not be needed. If they are     
     needed, remember to modify NewTerm()                               */
  int (*Control) ( PTerm Term, int func, ... );
                                /* All functions that are called rarely
                                   go here. Status is returned          
                                     0 .. OK							
                                    -1 .. not supported                 */

  /* And now some functions that should be supported by Control()		*/
#define ctReset	0
  /* Has one parameter "how much reset"						   
      1..flush internal buffers only (=>redraw screen)		   
      5..soft reset of terminal								   
      7..get terminal from completely unknown state			   */
#define ctBlankScreen 1
  /* Has one parameter "how much blank"
      0..no blank											   
      1..blank which can be unblanked instantly                
           bigger values mean be more agresive about blanking. 
      7..turn monitor off completely			               */
#define ctSelectFont 2
#define ctSelectMode 3
  } Terminal;

PTerm NewTerm( void );  /* Allocates space on heap for Terminal         */
                        /*  structure. Call InitTermXX( Term ) on       */
                        /*  return value!                               */

#undef PTerm
#endif












