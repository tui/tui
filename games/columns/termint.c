/*
        Universal terminal interface
*/

#include <stdlib.h>
#include <stdio.h>
#include "termint.h"

void ScrollArea( Term, x, y, sx, sy, dx, dy, color ) 
PTerm Term; 
int x, y, sx, sy, dx, dy, color;
{
Term->MoveBlock( Term, (dx>0)?x:(x-(dx)), (dy>0)?y:(y-(dy)), 
                       sx-abs(dx), sy-abs(dy), 
                       (dx>0)?(x+(dx)):x, (dy>0)?(y+(dy)):y, color );
}

/* Mostly for debugging: prints info about terminal/video mode  */

void
PrintModeInfo( PVideoMode Mode )
{
if (!Mode) printf("    [Unknown]\n");
      else printf("    %s: %dx%d chars, %dx%d each cell, %dx%d each char, %s\n",
                  Mode->Desc, Mode->ScrX, Mode->ScrY, Mode->CellX, Mode->CellY,
                              Mode->CharX, Mode->CharY, Mode->Set );
}

void
PrintTermInfo( PTerm Term )
{
PVideoMode Mode;

printf( "%s: %dx%d chars, cursor at [%d,%d], type %d.\n",
  Term->Desc, Term->ScrX, Term->ScrY, Term->CursorX, Term->CursorY, Term->CursorNow );
printf( "  Active video mode:\n" );
PrintModeInfo( Term->Current );
if (Term->Modes)
  {
  printf( "  Available video modes:\n" );
  TRAVERSE( Mode, Term->Modes ) PrintModeInfo( Mode );
  }
}

/* Frees memory for video modes in specified terminal   */

void
DoneVideoModes( PTerm Term )
{
struct VideoMode *Mode, *Last = NULL;
TRAVERSE( Mode, Term->Modes )
  {
  if( Last ) xfree( Last, sizeof( struct VideoMode ) );
  Last = Mode;
  }
if( Last ) xfree( Last, sizeof( struct VideoMode ) );
Term->Modes = NULL;
}

static void 
allPutString( PTerm Term, int x, int y, int num, word *ch, word *color )
{
int i;
for (i=0; i<num; i++) Term->PutChar( Term, x + i, y, ch[i], color[i] );
}

static void
allFillChar ( PTerm Term, int x, int y, int sx, int sy, int ch, int color )
{
int i, j;
for ( j=y; j<y+sy; j++ )
  for ( i=x; i<x+sx; i++ )
	Term->PutChar( Term, i, j, ch, color );
}

#define NO( x ) t->x = (void *) nofunc
PTerm
NewTerm( void )
{
PTerm t = xcalloc( sizeof( Terminal ));
NO( Close );
NO( CursorXY );
NO( CursorType );
NO( PutChar );
NO( FillChar );
NO( MoveBlock );
NO( Poll );
NO( Control );
t->FillChar = (void *) allFillChar;
t->PutString = (void *) allPutString;
return t;
}

