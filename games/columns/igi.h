#ifndef IGI_H
#define IGI_H

#include <stdlib.h>
//#include <time.h>
#include <sys/select.h>
#include <string.h>

/* According to earlier standards */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

/*
96-5-24 Pavel : I modified it a little bit from the one posted to mailing list
                (because I did not have that one posted here when writing it),
                and I added macros to work with linked lists. Any comments
                are welcome.
96-5-26 Pavel : I wonder if it is good idea to include also basics for
                operations with rectangles. Is compiler/linker inteligent
                enough not to include functions that are not referecned?
96-5-29 Pavel : Trouble: linux includes have symbol "ulong" already defined!
96-9-27 Pavel : Adding macros ever and loop - they look helpfull
96-11-11 Pavel : Adding lists/logging functions from mj's IMPOS
*/

/* Following definitions are compiler - specific.       */

/* Turbo C      */

#ifdef __TURBOC__
typedef signed char sbyte;
typedef unsigned char byte;
typedef signed int sword;
typedef unsigned int word;
typedef signed long slg;
typedef unsigned long ulg;

#define inline          /* For functions that should be expanded inline */
                        /*   must be static !                           */
#define NONRET          /* For functions that will never return         */
                        /*   used in declaration                        */
#define NOSIDE          /* For functions with no side effects           */
#endif

/* GNU C		*/

#ifdef __GNUC__
typedef signed char sbyte;
typedef unsigned char byte;
typedef signed short sword;
typedef unsigned short word;
typedef signed int slg;
typedef unsigned long ulg;

#define inline inline   /* For functions that should be expanded inline */
                        /*   must be static !                           */
#define NONRET          /* For functions that will never return         */
                        /*   used in declaration                        */
#define NOSIDE          /* For functions with no side effects           */
#endif

typedef word uword;
typedef byte ubyte;
typedef byte uchar;
typedef word unic;

#ifndef NULL
#define NULL (void*)0
#warn NULL was not defined!
#endif

/* And now some rather independent definitions          */

void *xalloc( const size_t size );
void *xrealloc( void *ptr, const size_t size );	/* Needed.				*/
void *xcalloc( const size_t size );     /* Zero's specified area        */
void xfree( void *ptr, const size_t size );

void *nalloc( const size_t size );   /* Following two should not be used */
void *zcalloc( const size_t size );  /*                                  */
void nfree( void *ptr );             /*   unless neccessary              */

void err( const char *s, ... ) NONRET;  /* Terminate with error            */
void perr( const char *s, ... ) NONRET; /* Prints error after library call */
void die( const char *s, ... ) NONRET;  /* Used for internal errors        */
void warn( const char *s, ... );        /* Used for warnings               */
void info( const char *s, ... );		/* Used for putting text to stderr */

void nofunc( void ) NOSIDE;          /* Function which does nothing     */
                                     /*   sometimes better than NULL    */
                                     /*   pointer since it requires     */
                                     /*   no special handling...        */


/* Taken from impos.h:
 *		The Incompatible Multi-PrOcessing System -- General Declarations
 *		(c) 1996 Martin Mares, <mj@ericsson.cz>
 */

#define OFFSETOF(s,i) ((ulg)&((struct s *)0)->i)

/* Nodes & Lists */

struct node {
	struct node *next, *prev;
	};
typedef struct node node;

struct list {
	struct node head, tail;
	};
typedef struct list list;

#define NODE (node *)
#define DECL_NODE(name,type,var) struct type##_node *name = \
	(struct type##_node *) var
#define HEAD(list) ((void *)((list).head.next))
#define DO_FOR_ALL(n,list) for((n)=HEAD(list);(NODE (n))->next; \
	n=(void *)((NODE (n))->next))
#define EMPTY_LIST(list) (!(list).head.next->next)

void add_tail(list *, node *);
void add_head(list *, node *);
void rem_node(node *);
void add_tail_list(list *, list *);
void init_list(list *);
void insert_node(node *, node *);
int check_list(list *);

/* Logging classes */

#define CLASS_DEBUG 0
#define CLASS_INFO 1
#define CLASS_LOG 2
#define CLASS_AUTH 3
#define CLASS_WARN 4
#define CLASS_AUTHFAIL 5
#define CLASS_ERROR 6
#define CLASS_SYS 7
#define CLASS_FATAL 8
#define CLASS_PANIC 9
#define CLASS_MAX 10

#define C_DEBUG "<0>"
#define C_INFO "<1>"
#define C_LOG "<2>"
#define C_AUTH "<3>"
#define C_WARN "<4>"
#define C_AUTHFAIL "<5>"
#define C_ERROR "<6>"
#define C_FATAL "<7>"
#define C_SYS "<8>"
#define C_PANIC "<9>"

/* Logging */

struct log_file {
  node n;
  char *base_name;
  int open_mode;
  ulg timestamp;
  void *fh;	/* This is really a FILE * */
  byte log_flags[CLASS_MAX];
};

struct log_file *start_log(char *, int);
void stop_log(struct log_file *);
void put_log(char *, ...);

#define LOM_FILE 0
#define LOM_STDOUT 1
#define LOM_STDERR 2

/* Something for debugging                              */

/*      If debugging is ON      */

#ifdef DEBUG
#define NUM_DEBUG_SWITCHES 10
                                /* There are few debug sections. Bigger */
                                /*  number in DebugLevel array means    */
                                /*  you should print more messages      */
#define DEBUG_SECTIONS 10
extern char DebugLevel[ DEBUG_SECTIONS ];

#define DBG( section, level, text ) \
  if (DebugLevel[ (section) ]>=(level)) printf( text );
#define IFDBG( section, level, what ) \
  if (DebugLevel[ (section) ]>=(level)) { what }

#else

/*      If debugging is OFF     */

#define DBG( a, b, c )
#define IFDBG( a, b, c )

#endif

/* Link lists   */

/* Will replace this with mj's version, if possible						*/

/* I think linked lists will be used many times here. It is quite good  */
/* idea to keep "next" pointer as first in record. Please keep is as    */
/* rule. Then it will be possible to use following functions & macros.  */

#define TRAVERSE( Loop, First ) for ( Loop = First; Loop; Loop = (void **) Loop->Next )

void LinkInsert( void ***Where, void **What );
void LinkDelete( void ***Win2, void **Win );

#define LINKINSERT( x, y ) LinkInsert( (void ***) (&( x )), (void **) (y) )
#define LINKDELETE( x, y ) LinkDelete( (void ***) (&( x )), (void **) (y) )

/* Example:                                                             */
/*   LINKINSERT( Term->Modes, NewMode )                                 */
/*   TRAVERSE( Mode, Term->Modes ) PrintMode( Mode )                    */

/* String functions     */

char *stralloc( const char *s );        /* Allocates new string as copy */
                                        /*   of given string            */

/* Character pairs used because of some compilers dislike constants like 'AB'	*/
#define CP(x,y) (((x)<<8) | (y))

/* MIN and MAX macros defined because gcc does not seem to understand it		*/
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/* This macros should be usefull												*/
#define ever while(1)
#define loop( i, a ) for( i=0; i<a; i++ )

#define FULL (void *) -1

#endif


