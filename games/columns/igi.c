/*
        This source includes functions described in Igi.h header file.

   (sorry for putting each function body on one line, but for so short
   functions it is probably better)
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "igi.h"

/* Memory allocation    */

void *
xalloc( const size_t size )
{ return malloc( size ); }

void *
xrealloc( void *ptr, const size_t size )
{ return realloc( ptr, size ); }

void *
xcalloc( const size_t size )
{ void *p = xalloc( size ); memset( p, 0, size ); return p; }

void
xfree( void *ptr, const size_t size )
{ free( ptr ); }

void *
nalloc ( const size_t size )
{ return malloc( size ); }

void *
ncalloc ( const size_t size )
{ void *p = nalloc( size ); memset( p, 0, size ); return p; }

void
nfree( void *ptr )
{ free( ptr ); }

/* Linked lists         */

/*
void
LinkInsert( void ***Where, void **What )
{
*What = *Where;
*Where = What;
}

void
LinkDelete( void ***Where, void **What )
{
while (*Where)
  {
  if (*Where==What)
    {
    (*Where)=(void **) (**Where);
    break;
    }
  Where=(void ***) *Where;
  }
IFDBG( 0, -1, if (!(*Where)) die( "LinkDelete failed.\n" ));
}
*/

/* String functions     */

char *
stralloc( const char *s )
{ char *t; strcpy( t = nalloc( strlen( s ) ), s ); return t; }

/* Misc			*/

void
nofunc( void )
{ }

/* Rectangles   */

/*
void RectUnion( int *r1x, int *r1y, int *r1sx, int *r1sy,
                int r2x, int r2y, int r2sx, int r2sy )
  {
  r1->sx += r1->x;
  r2->sx += r2->x;
  r1->sy += r1->y;
  r2->sy += r2->y;

  r1->x  = min( r1->x, r2->x );
  r1->sx = max( r1->sx, r2->sx );
  r1->y  = min( r1->y, r2->y );
  r1->sy = max( r1->sy, r2->sy );

  r1->sx -= r1->x;
  r2->sx -= r2->x;
  r1->sy -= r1->y;
  r2->sy -= r2->y;
  }

void Intersect( TRect *r1, TRect *r2 )
  {
  r1->sx += r1->x;
  r2->sx += r2->x;
  r1->sy += r1->y;
  r2->sy += r2->y;

  r1->x  = max( r1->x, r2->x );
  r1->sx = min( r1->sx, r2->sx );
  r1->y  = max( r1->y, r2->y );
  r1->sy = min( r1->sy, r2->sy );

  r1->sx -= r1->x;
  r2->sx -= r2->x;
  r1->sy -= r1->y;
  r2->sy -= r2->y;
  }
*/

/* This should NOT be here and it NEEDS rewriting (probably) */
#include <termios.h>

void raw(void)
{
struct termios it;

tcgetattr(fileno(stdin),&it);
it.c_lflag &= ~(ICANON);
it.c_lflag &= ~(ECHO);
it.c_iflag &= ~(INPCK|ISTRIP|IXON);
it.c_oflag &= ~(OPOST);
it.c_cc[VMIN] = 1;
it.c_cc[VTIME] = 0;

tcsetattr(fileno(stdin),TCSANOW,&it);
}

void noraw(void)
{
struct termios it;

tcgetattr(fileno(stdin),&it);
it.c_lflag |= ICANON;
it.c_lflag |= ECHO;
it.c_iflag |= IXON;
it.c_oflag |= OPOST;

tcsetattr(fileno(stdin),TCSANOW,&it);
}


void
die(const char *msg, ...)
{
  va_list args;
  
  va_start(args, msg);
  if (msg)
    printf(msg);
  va_end(args);
  exit( 3 );
}
