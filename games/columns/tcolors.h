#ifndef TCOLORS_H
#define TCOLORS_H

#include "igi.h"

/*
  This should define colors for text modes. We agreed than in text 
modes, color information about each character (foreground & 
background) should fit into 2 bytes. Question is, how to stuff it
there. I decided to use 2-2-2 scheme - 2 bits for each color 
(thus preserving 4 bits for anything)
*/

typedef word tcolor;

#define TRED( x ) (0x3 & (x)>>4)
#define TGREEN( x ) (0x3 & (x)>>2)
#define TBLUE( x ) (0x3 & (x))

#define TGREY( x ) ( (0x0303 & (x)) +  (0x0303 & ((x)>>2)) + (0x0303 & ((x)>>4)))

/* 

  Atributes:

Blink	   (many users would kill us for this)	
Underline  (Ok) Bit 6 on foreground
Bold       (Ok) Bit 7 on foreground
Italics    (Ok) Bit 6 on background
Shadow     (Ok) Bit 7 on background

*/

typedef struct tag_CharInfo {
  word ch;			/* Character code	*/
  tcolor color;		/* Character color & attributes	*/
  } charinfo;

/* 
  IS_xxxx have two forms: first is used with charinfo structures, second
has two parametrs - character and color 
*/

#define IS_UNDERLINE2( x, y ) (y) & 0x0040
#define IS_UNDERLINE1( x ) IS_UNDERLINE2( x.ch, x.color )

#define IS_BOLD2( x, y ) (y) & 0x0080
#define IS_BOLD1( x ) IS_BLINK2( x.ch, x.color )


#define IS_ITALICS2( x, y ) (y) & 0x4000
#define IS_ITALICS1( x ) IS_ATTRA2( x.ch, x.color )

#define IS_SHADOW2( x, y ) (y) & 0x8000
#define IS_SHADOW1( x ) IS_ATTRB2( x.ch, x.color )

#define TFORE( x ) (x) & 0x3F
#define TBACK( x ) ((x) >> 8) & 0x3F

#define TONEBIT( x ) (((x) & 0x02) >> 1) + (((x) & 0x08) >> 2) + (((x) & 0x20) >> 3)

#define RGB( x, y, z ) ( (x << 4) + (y << 2) + (z) )
#define PAIR( r, g, b, R, G, B ) ( ( ((word) RGB( r, g, b ))<<8) + RGB( R, G, B ))
#endif

