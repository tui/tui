#include "termint.h"
#include <time.h>
#include "tcolors.h"
#include <stdio.h>
#include <stdlib.h>

#define XPOS 2
#define YPOS 1
#define XSIZE 7
#define YSIZE 15
#define STONES 7
#define RANDOM( a ) (random() / (RAND_MAX / (a)))+1

#define FRCOLOR PAIR( 0, 0, 0, 2, 2, 2 )
#define ABS( x ) (x<0?-x:x)

#define STATUS( x, y ) (status[ (x) + (y) * XSIZE ])
char status[ XSIZE*YSIZE ];
PTerm Term;
int pl[3];
int plx, ply;
int pltime = 500000;
int score;
int maxscore = 0;

void segfault( void ) { char *c = NULL; exit( *c ); }
void
paintframe( void )
{
Term->FillChar( Term, XPOS-1, YPOS, 1, YSIZE, 'I', FRCOLOR );
Term->PutChar( Term, XPOS-1, YPOS+YSIZE, '`', FRCOLOR );
Term->FillChar( Term, XPOS, YPOS+YSIZE, XSIZE*2, 1, '=', FRCOLOR );
Term->PutChar( Term, XPOS+XSIZE*2, YPOS+YSIZE, '\'', FRCOLOR );
Term->FillChar( Term, XPOS+XSIZE*2, YPOS, 1, YSIZE, 'I', FRCOLOR );
}

void
clearit( void )
{
paintframe();
Term->FillChar( Term, XPOS, YPOS, XSIZE*2, YSIZE, ' ', FRCOLOR );
}

void
colorstring( int *x, int *y, char *s, int color )
{
while( *s )
  {
  Term->PutChar( Term, (*x)++, *y, *s++, color );
  }
}

void
paintlogo( void )
{
int x, y;
x = XSIZE*2+18; y = 3;
Term->PutChar( Term, x++, y, 'C', PAIR( 3, 0, 0, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 3, 0, 0, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'o', PAIR( 0, 3, 0, 3, 0, 0 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 0, 3, 0, 3, 0, 0 ) ); 
Term->PutChar( Term, x++, y, 'l', PAIR( 0, 0, 3, 0, 3, 0 ) );  
Term->PutChar( Term, x++, y, ' ', PAIR( 0, 0, 3, 0, 3, 0 ) );  
Term->PutChar( Term, x++, y, 'u', PAIR( 3, 3, 0, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 3, 3, 0, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, 'm', PAIR( 0, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 0, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, 'n', PAIR( 3, 0, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 3, 0, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, 's', PAIR( 3, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 3, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '!', PAIR( 0, 0, 0, 3, 3, 3 ) );
Term->PutChar( Term, x++, y, ' ', PAIR( 0, 0, 0, 3, 3, 3 ) );

x = XSIZE*2+18; y = 4;
Term->PutChar( Term, x++, y, '=', PAIR( 0, 0, 0, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 3, 3, 0, 3, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 0, 3, 0, 3, 0 ) );  
Term->PutChar( Term, x++, y, '=', PAIR( 0, 3, 0, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 3, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 0, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 0, 0, 3, 3, 3 ) );
Term->PutChar( Term, x++, y, '=', PAIR( 3, 0, 0, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 3, 0, 3, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 0, 3, 0, 3, 0 ) );  
Term->PutChar( Term, x++, y, '=', PAIR( 0, 3, 0, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 3, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 3, 0, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 0, 3, 3, 0, 0, 0 ) ); 
Term->PutChar( Term, x++, y, '=', PAIR( 3, 0, 0, 3, 3, 3 ) );

y = 6; x = XSIZE*2 + 5;
colorstring( &x, &y, "Copyright 1997 ", PAIR( 0, 0, 0, 1, 1, 1 ) ); 
colorstring( &x, &y, "Pavel Machek ", PAIR( 0, 0, 0, 3, 3, 3 ) ); 
colorstring( &x, &y, "<pavel@elf.mj.gts.cz> ", PAIR( 0, 0, 0, 3, 3, 0 ) ); 
colorstring( &x, &y, "(GPL)", PAIR( 0, 0, 0, 1, 3, 3 ) ); 

y = 8; x = XSIZE*2 + 7;
colorstring( &x, &y, "Score: 0000", PAIR( 0, 0, 0, 3, 3, 3 ) );

y = 10; x = XSIZE*2 + 7;
colorstring( &x, &y, "Controls: J) <-, K) rotate, L) ->, space) v, Q) exit", PAIR( 0, 0, 0, 0, 3, 0 ) ); 
}

void
paintscore( int score )
{
int x,y;
y = 8; x = XSIZE *2 + 7 + 10;
while (score)
  {
  Term->PutChar( Term, x--, y, '0'+score%10, PAIR( 0, 0, 0, 3, 0, 3 ));
  score /= 10;
  }
}

void
gameover( void )
{
int x,y;
y = 5;
x = 4;

Term->PutChar( Term, x++, y, 'G', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'a', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'm', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'e', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, ' ', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'o', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'v', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'e', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, 'r', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->PutChar( Term, x++, y, '!', PAIR( 0, 0, 3, 3, 3, 3 ) ); 
Term->Poll( Term );
read( 0, 0, 1 );
}

void
stone( int x, int y, int which )
{
static char stoneleft[] = " %X#@[<O";
static char stoneright[] = " %X#@]>O";
static int stonecolor[] = {  PAIR(  0, 0, 0 , 2, 2, 2 ),
							 PAIR(  3, 0, 0 , 2, 2, 2 ), 
							 PAIR(  0, 3, 0 , 2, 2, 2 ), 
							 PAIR(  0, 0, 3 , 2, 2, 2 ), 
							 PAIR(  3, 3, 0 , 0, 0, 0 ), 
							 PAIR(  3, 0, 3 , 0, 0, 0 ), 
							 PAIR(  0, 3, 3 , 0, 0, 0 ), 
							 PAIR(  3, 3, 3 , 0, 0, 0 ) };
if (which > STONES+2) { printf( "Stoned.\n" ); segfault(); }
Term->PutChar( Term, XPOS + 2*x, YPOS + y, stoneleft[ which ], stonecolor[ which ] );
Term->PutChar( Term, XPOS + 2*x + 1, YPOS + y, stoneright[ which ], stonecolor[ which ] );
}

int
shakeit( void )
{
int y, x, res = 0;
for (x=0; x<XSIZE; x++)
  {
  for (y=YSIZE-1; y>0; y--)
	{
	while (!STATUS( x,y ) && STATUS( x, y-1 ) )
	  {
	  int y1;
	  res++;
	  for( y1=y-1; 0<=y1; y1-- )
		STATUS( x,y1+1 ) = STATUS( x, y1 );
	  STATUS( x, 0 ) = 0;
	  }
    }
  }
return res;
}

int
isfree( int x, int y )
{
return (!STATUS( x, y ) && !STATUS( x, y+1 ) && !STATUS( x, y+2 ));
}

void
clearpl( int x, int y )
{
stone( x, y, 0 );
stone( x, y+1, 0 );
stone( x, y+2, 0 );
}

void
paintpl( int x, int y )
{
stone( x, y, pl[0] );
stone( x, y+1, pl[1] );
stone( x, y+2, pl[2] );
}

void
paintit( int paintneg )	/* paintneg = 0 / -1 */
{
int x,y;
paintframe();
for( y=0; y<YSIZE; y++ )
  for( x=0; x<XSIZE; x++ )
	{
	int s;
	s = STATUS( x, y );
	if (s<0) s *= paintneg;
	stone( x, y, s );
	}
Term->Poll( Term );
}

int
ontimer( int flag ) /* Returns 1 whereever stone reached ground. Flag -> do not settle */
{
if ((ply == YSIZE-3) || STATUS( plx, ply+3 ))
  {
  if (flag) return 1;
  STATUS( plx, ply ) = pl[0];
  STATUS( plx, ply+1 ) = pl[1];
  STATUS( plx, ply+2 ) = pl[2];
  return 1;
  }
else
  {
  clearpl( plx, ply );
  paintpl( plx, ++ply );
  return 0;
  }
}

void
onkey( int i )
{
int j;
switch( i ) {
  case 'j':
  case 'J': if (plx <= 0) return;
	        if (!isfree( plx-1, ply  )) return;
	        clearpl( plx, ply );
	        paintpl( --plx, ply );
	        break;
  case 'l':
  case 'L': if (plx >= XSIZE-1) return;
	        if (!isfree( plx+1, ply  )) return;
	        clearpl( plx, ply );
	        paintpl( ++plx, ply );
	        break;
  case 'k':
  case 'K': j = pl[2];
	        pl[2] = pl[1];
	        pl[1] = pl[0];
	        pl[0] = j;
	        paintpl( plx, ply );
	        break;
  case ' ':
	        while (!ontimer( 1 )) Term->Poll( Term );
	        break;
  case 'q':
	        Term->CursorXY( Term, 0, 23 );
	        Term->Poll( Term );
	        Term->Close( Term );
	        noraw();
	        printf( "Thanx for playing columns ;-). Max score was: " ); fflush( stdout );
	        fprintf( stderr, "%d\n", maxscore );
	        printf( "\n" );
	        exit( 0 );
  }
}

#define INV( x ) { if ((x)>=0) (x) = -(x); if (x) ret=1; }
#define SCORE { if (STATUS( x,y )) score ++; }
int
blowit( void )
{
int x, y, ret;
for( y = 0; y< YSIZE; y++ )				/* --- */
  for( x = 0; x < XSIZE-2; x++ )
	{
	if ( (ABS( (STATUS( x, y )) ) == ABS( (STATUS( x+1, y )) )) &&
		 (ABS( (STATUS( x+1, y )) ) == ABS( (STATUS( x+2, y )) )))
	  { INV( (STATUS( x, y )) );
		INV( (STATUS( x+1, y )) );
		INV( (STATUS( x+2, y )) ); 
	    SCORE; }
 	}
for( y = 0; y< YSIZE-2; y++ )			/* | */
  for( x = 0; x < XSIZE; x++ )
	{
	if ( (ABS( (STATUS( x, y )) ) == ABS( (STATUS( x, y+1 )) )) &&
		 (ABS( (STATUS( x, y+1 )) ) == ABS( (STATUS( x, y+2 )) )))
	  { INV( (STATUS( x, y )) );
		INV( (STATUS( x, y+1 )) );
		INV( (STATUS( x, y+2 )) ); 
	    SCORE; }
 	}
for( y = 3; y< YSIZE; y++ )				/* / */
  for( x = 0; x < XSIZE-2; x++ )
	{
	if ( (ABS( (STATUS( x, y )) ) == ABS( (STATUS( x+1, y-1 )) )) &&
		 (ABS( (STATUS( x+1, y-1 )) ) == ABS( (STATUS( x+2, y-2 )))))
	  { INV( (STATUS( x, y )) ); 
		INV( (STATUS( x+1, y-1 )) );
		INV( (STATUS( x+2, y-2 )) ); 
	    SCORE; }
    }
for( y = 0; y< YSIZE-2; y++ )			/* \ */
  for( x = 0; x < XSIZE-2; x++ )
	{
	if ( (ABS( (STATUS( x, y )) ) == ABS( (STATUS( x+1, y+1 )) )) &&
		 (ABS( (STATUS( x+1, y+1 )) ) == ABS( (STATUS( x+2, y+2 )) )))
	  { INV( (STATUS( x, y )) );
		INV( (STATUS( x+1, y+1 )) );
		INV( (STATUS( x+2, y+2 )) );
	    SCORE; }
 	}
return ret;
}

int
nextpiece()
{
if (!isfree(XSIZE/2, 0)) return 0;
plx = XSIZE / 2;
ply = 0;
pl[0]  = RANDOM( STONES );
pl[1]  = RANDOM( STONES );
pl[2]  = RANDOM( STONES );
return 1;
}

int
ontimer2( void )
{
if (ontimer(0))
  {
  do {
	 while( shakeit() ) paintit( -1 );
	 if (blowit()) 
	   {
	   int x, y;
	   paintit( 0 );
	   usleep( 150000 );
	   paintit( -1 );
	   usleep( 150000 );
	   paintit( 0 );
	   for( y = 0; y< YSIZE; y++ )			
		 for( x = 0; x < XSIZE; x++ )
		   if (STATUS( x, y )<0) STATUS( x, y ) = 0;
	   }
     }
  while (shakeit());
  if (!nextpiece()) return 1;
  paintit( -1 );
  paintpl( plx, ply );
  }
return 0;
}

void
game( void )
{
struct timeval timeout;
bzero( status, XSIZE * YSIZE );
if (!nextpiece()) die( "Something went wrong." );
paintpl( plx, ply );
score = 0;
paintlogo();
Term->Poll( Term );
timeout.tv_sec = 0;
timeout.tv_usec = pltime;
while( 1 )
  {
  fd_set readfd;
  char c;
  FD_ZERO( &readfd );
  FD_SET( 0, &readfd );
  paintscore( score );
  Term->Poll( Term );

  select( 256, &readfd, NULL, NULL, &timeout );
  if (FD_ISSET( 0, &readfd))
	{
	read( 0, &c, 1 );
	onkey( c );
	}
  else
	{
	timeout.tv_usec = pltime;
	if (ontimer2()) break;
    }
  }
}

void 
main( void )
{
raw();
Term = NewTerm();
srandom( time( NULL ));
InitTermAnsi( Term, getenv( "TERM" ));
Term->FillChar( Term, 0, 0, Term->ScrX, Term->ScrY, ' ', 42 );
paintlogo();
while( 1 )
  {
  clearit();
  game();
  gameover();
  if (score>maxscore) maxscore = score;
  }
}
