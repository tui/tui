#!/usr/bin/python
# Copyright 2012 Pavel Machek, Lenka Vranova, GPL
# Set players below
# needs big screen, "u" to quit.
# smoke changes into fire if it touches fire

import random
import time
import sys

max_zachranenych = 7
max_mrtvol = 3
max_zborenych = 15

zachranenych = 0
mrtvol = 0
zborenych = 0

zed_ne = 0
zed_ano = 1
zed_ponicena_1 = 2
zed_dvere_zavrene = 3
zed_dvere_otevrene = 4

def vycisti_prostor(x, y):
    global sx, sy, ohen, zed
    sx = x
    sy = y

    ohen = {}
    zed = {}

    for x in range(sx):
        for y in range(sy):
            ohen[x,y] = 0

    for x in range(sx-1):
        for y in range(sy):
            zed[x+0.5,y] = zed_ne

    for x in range(sx):
        for y in range(sy-1):
            zed[x,y+0.5] = zed_ne


def kresli_zed_horizontalne(y):
    global zed
    for x in range(sx-2):
        zed[x+1, y] = zed_ano

def kresli_zed_vertikalne(x):
    global zed
    for y in range(sy-2):
        zed[x, y+1] = zed_ano

def prostor_1():
    global zed

    vycisti_prostor(10,8)

    kresli_zed_vertikalne(0.5)
    kresli_zed_vertikalne(8.5)
    kresli_zed_horizontalne(0.5)
    kresli_zed_horizontalne(2.5)
    zed[1, 2.5] = zed_ne
    zed[2, 2.5] = zed_ne
    kresli_zed_horizontalne(4.5)
    kresli_zed_horizontalne(6.5)
    zed[3.5, 1] = zed_ano
    zed[3.5, 2] = zed_ano
    zed[5.5, 1] = zed_ano
    zed[5.5, 2] = zed_ano
    zed[2.5, 3] = zed_ano
    zed[2.5, 4] = zed_ano
    zed[6.5, 3] = zed_ano
    zed[6.5, 4] = zed_ano
    zed[5.5, 5] = zed_ano
    zed[5.5, 6] = zed_ano
    zed[7.5, 5] = zed_ano
    zed[7.5, 6] = zed_ano
    zed[0.5, 3] = zed_dvere_otevrene
    zed[6, 0.5] = zed_dvere_otevrene
    zed[8.5, 4] = zed_dvere_otevrene
    zed[3, 6.5] = zed_dvere_otevrene

    zed[3.5, 1] = zed_dvere_zavrene
    zed[5.5, 2] = zed_dvere_zavrene
    zed[2.5, 3] = zed_dvere_zavrene
    zed[6.5, 4] = zed_dvere_zavrene
    zed[5.5, 6] = zed_dvere_zavrene
    zed[7.5, 6] = zed_dvere_zavrene
    zed[8, 2.5] = zed_dvere_zavrene
    zed[4, 4.5] = zed_dvere_zavrene

def prostor_2():
    global zed

    vycisti_prostor(11,8)

    kresli_zed_vertikalne(0.5)
    kresli_zed_vertikalne(9.5)
    zed[9.5, 1] = zed_ne
    kresli_zed_vertikalne(7.5)
    zed[7.5, 1] = zed_ne
    zed[7.5, 2] = zed_ne
    kresli_zed_vertikalne(6.5)
    kresli_zed_horizontalne(0.5)
    zed[7, 0.5] = zed_ne
    zed[8, 0.5] = zed_ne
    zed[9, 0.5] = zed_ne
    kresli_zed_horizontalne(2.5)
    kresli_zed_horizontalne(6.5)
    zed[2, 0.5] = zed_dvere_otevrene
    zed[1.5, 1] = zed_dvere_zavrene
#    zed[2.5, 1] = zed_dvere_zavrene
    zed[1.5, 3] = zed_dvere_zavrene
    zed[2, 3.5] = zed_dvere_zavrene
    zed[3.5, 3] = zed_dvere_zavrene
    zed[5, 2.5] = zed_ne
    zed[5.5, 2] = zed_dvere_zavrene
    zed[6.5, 4] = zed_dvere_zavrene
    zed[7, 2.5] = zed_dvere_otevrene
    zed[8, 2.5] = zed_dvere_otevrene
    zed[8.5, 2] = zed_dvere_otevrene
    zed[1, 1.5] = zed_ano
    zed[1, 2.5] = zed_ano
    zed[2, 2.5] = zed_ne
    zed[1, 3.5] = zed_ano
    zed[3, 3.5] = zed_ano
    zed[2.5, 2] = zed_ano
    zed[5.5, 1] = zed_ano
    zed[9, 1.5] = zed_ano

prostor_2()

def getch():
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

def nahodne_mezi(od, do):
    return int(random.random()*(do-od)) + od

class Clovek:
    def __init__(s):
        s.x = 0
        s.y = 3
        s.jmeno = ' hrac  '

    def tisk(s):
        return s.jmeno

    def popal(s):
        global mrtvol
        mrtvol = mrtvol + 1
        s.__init__()

    def smer(h, s):
        if s == 's':
            return (0, 1)
        if s == 'w':
            return (0, -1)
        if s == 'a':
            return (-1, 0)
        if s == 'd':
            return (1, 0)
        if s == '.':
            return (0, 0)
        return 0

    def smim_na(s, xp, yp):
        if s.x+xp < 0 or s.x+xp >= sx:
            return 0
        if s.y+yp < 0 or s.y+yp >= sy:
            return 0
        return not je_bariera(s.x, s.y, xp, yp)
    
    def jdi_na(s, kam):
        sm = s.smer(kam)
        if not sm:
            return s.chyba("Nerozumim smeru")
        (xp, yp) = sm
        if not s.smim_na(xp, yp):
            return s.chyba("Tam nemuzu jit, narazil jsem do zdi")

        s.x = s.x+xp
        s.y = s.y+yp
        return 1

    def chyba(h, s):
        print(s)
        return 0

class Obet(Clovek):
    def __init__(s):
        s.x = nahodne_mezi(1, sx-1)
        s.y = nahodne_mezi(1, sy-1)
        while ohen[s.x, s.y] == 2:
            s.x = nahodne_mezi(1, sx-1)
            s.y = nahodne_mezi(1, sy-1)

    def tisk(s):
        return '  obet '

    def jdi_na(s, kam):
        r = Clovek.jdi_na(s, kam)
        if s.x == 0 or s.x == sx-1 or s.y == 0 or s.y == sy-1:
            global zachranenych
            zachranenych = zachranenych + 1
            s.__init__()
        return r

class Hrac(Clovek):
    def has_na(s, kam):
        sm = s.smer(kam)
        if not sm:
            return s.chyba("Nerozumim smeru")
        (xp, yp) = sm
        if not s.smim_na(xp, yp):
            return s.chyba("Tam nemuzu hasit, nemuzu tam jit")
        x = s.x+xp
        y = s.y+yp
        if ohen[x, y] > 0:
            ohen[x,y] = ohen[x,y] - 1
            return 1
        return s.chyba("Tady nemuzu hasit, neni co")

    def posli_na(s, kam):
        for h in obet:
            if h.x == s.x and h.y == s.y:
                return h.jdi_na(kam)

        return h.chyba("Nemuzu poslat, neni koho")

    def otevri_na(s, kam):
        global zborenych
        sm = s.smer(kam)
        if not sm:
            return s.chyba("Nerozumim smeru")
        (xp, yp) = sm

        x = s.x+xp/2.0
        y = s.y+yp/2.0
        if zed[x,y] == zed_dvere_zavrene:
            zed[x,y] = zed_dvere_otevrene
            return 1

        if zed[x,y] == zed_dvere_otevrene:
            zed[x,y] = zed_dvere_zavrene
            return 1

        if zed[x,y] == zed_ano:
            zed[x,y] = zed_ponicena_1
            zborenych += 1
            return 2

        if zed[x,y] == zed_ponicena_1:
            zed[x,y] = zed_ne
            zborenych += 1
            return 2

        return s.chyba("Neni co otevrit")

    def proved_podtah(h):
        c = getch()
        if h.smer(c):
            return h.jdi_na(c)
        l = c.lower()
        if h.smer(l):
            return h.has_na(l)
        if c == 'h':
            return h.has_na(getch())
        if c == 'p':
            return h.posli_na(getch())
        if c == 'o':
            return h.otevri_na(getch())
        if c == 'u':
            print "Diky za hru."
            exit(1)
        return h.chyba("Nerozumim tvemu prikazu")

    def proved_tah(h):
        akcni_body = 3
        while akcni_body > 0:
            tiskni()
            s = h.jmeno + ": Zadej tah (%d akci):" % akcni_body
            print(s)
            akcni_body = akcni_body - h.proved_podtah()

hracu = 1
hrac = []
for i in range(hracu):
    hrac = hrac + [ Hrac() ]
hrac[0].jmeno = ' Pavel '
#hrac[1].jmeno = ' Lucka '

obeti = 3
obet = []

def tiskni_ohen(x, y):
    if ohen[x, y] > 2:
        return '#######'
    if ohen[x, y] == 2:
        return ' OHEN  '
    if ohen[x, y] == 1:
        return 'kour   '
    return '       '

def tiskni_hrace(x, y):
    for h in hrac:
        if x == h.x and y == h.y:
            return h.tisk()

    return '       '

def tiskni_obet(x, y):
    for h in obet:
        if x == h.x and y == h.y:
            return h.tisk()

    return '       '

def tiskni_zed(z, vertikalne):
    if z == zed_ne: return ' '
    if z == zed_ano and vertikalne: return '|'
    if z == zed_ano: return '-'
    if z == zed_ponicena_1: return 'X'
    if z == zed_dvere_zavrene: return 'z'
    if z == zed_dvere_otevrene: return 'O'
    return '#'

def tiskni():
    for y in range(sy):
        s1 = ''
        s2 = ''
        s3 = ''
        for x in range(sx):
            s1 = s1 + tiskni_ohen(x, y)
            s2 = s2 + tiskni_hrace(x, y)
            s3 = s3 + tiskni_obet(x, y)
            if x < sx - 1:
                if zed[x+0.5, y]:
                    s1 = s1 + '|'
                    s2 = s2 + tiskni_zed(zed[x+0.5, y], 1)
                    s3 = s3 + '|'
                else:
                    s1 = s1 + '`'
                    s2 = s2 + ' '
                    s3 = s3 + ' '
        print(s1)
        print(s2)
        print(s3)
        if y < sy - 1:
            s = ''
            for x in range(sx):
                if zed[x, y+0.5]:
                    s = s + '---'+ tiskni_zed(zed[x, y+0.5], 0) + '----'
                else:
                    s = s + '        '
            print(s)
    print("%d/%d zachranenych, %d/%d mrtvol, %d/%d zborenych zdi" % (zachranenych, max_zachranenych, mrtvol, max_mrtvol, zborenych, max_zborenych))
    print("asdw pohyb, Otevri, Hasit, Posli obet, Ukoncit")

def tiskni_animuj():
    tiskni()
    time.sleep(0.3)

def bariera(z):
    if z==zed_ano:
        return 1
    if z==zed_ponicena_1:
        return 1
    if z==zed_dvere_zavrene:
        return 1

def je_bariera(x, y, xp, yp):
    if xp == 0 and yp == 0:
        return 0
    return bariera(zed[x+xp/2.0, y+yp/2.0])

def exploze(x, y, xp, yp):
    global zborenych
    if je_bariera(x, y, xp, yp):
        xz = x+xp/2.0
        yz = y+yp/2.0
        z = zed[xz, yz]
        if z == zed_dvere_zavrene:
            zed[xz, yz] = zed_ne
            return
        if z == zed_ano:
            zborenych+=1
            zed[xz, yz] = zed_ponicena_1
            return

        zborenych+=1
        zed[xz, yz] = zed_ne
        return

    if ohen[x+xp,y+yp] < 2:
        ohen[x+xp,y+yp] = 2
        tiskni_animuj()
        return

    exploze(x+xp, y+yp, xp, yp)

def pridej_ohen(x, y):
    ohen[x,y] = ohen[x,y]+1
    tiskni_animuj()
    if ohen[x,y] == 3:
        ohen[x,y] = 2
        exploze(x, y, 1, 0)
        exploze(x, y, -1, 0)
        exploze(x, y, 0, -1)
        exploze(x, y, 0, 1)

def ohnuj(kolik):
    x = nahodne_mezi(1, (sx-1))
    y = nahodne_mezi(1, (sy-1))
    for i in range(kolik):
        pridej_ohen(x, y)

def vzplan_kour_na(x, y, xp, yp):
    if je_bariera(x, y, xp, yp):
        return 0

    if ohen[x+xp,y+yp] != 2:
        return 0

    ohen[x,y] = 2
    return 1


def vzplan_kour():
    vzp = 1
    while vzp:
        vzp = 0
        for x in range(sx-1):
            for y in range(sy-1):
                if ohen[x+1,y+1] == 1:
                    vzp = vzp + vzplan_kour_na(x+1, y+1, 1, 0)
                    vzp = vzp + vzplan_kour_na(x+1, y+1, -1, 0)
                    vzp = vzp + vzplan_kour_na(x+1, y+1, 0, 1)
                    vzp = vzp + vzplan_kour_na(x+1, y+1, 0, -1)

def popaleniny():
    for h in hrac + obet:
        if ohen[h.x, h.y] == 2:
            h.popal()

def odohnuj():
    for x in range(sx):
        ohen[x, 0] = 0
        ohen[x, sy-1] = 0

    for y in range(sy):
        ohen[0, y] = 0
        ohen[sx - 1, y] = 0

for i in range(3):
    ohnuj(3)
    odohnuj()

for i in range(obeti):
    obet = obet + [ Obet() ]

while 1:
    for h in hrac:
        h.proved_tah()
        ohnuj(1)
        popaleniny()
        odohnuj()
        vzplan_kour()
