#!/usr/bin/python3

import re
import csv

class Decoder:
    def __init__(m, dir_name):
        m.dir_name = dir_name

    def open(m, name):
        return open(m.dir_name + "/" + name + ".txt", encoding="cp1250")

    def csv_split(m, l):
        return list(csv.reader([l[:-2]]))[0]

    def open_csv(m, name):
        return map(m.csv_split, m.open(name).readlines())

    def read_stops(m):
        m.stop_name = {}
        m.stop_codes = {}
        for l in m.open_csv("Zastavky"):
            m.stop_name[l[0]] = l[1] + "," + l[2] + "," + l[3]
            m.stop_codes[l[0]] = l[6:13]

    def read_codes(m):
        m.codes = {}
        for l in m.open_csv("Pevnykod"):
            m.codes[l[0]] = l[1]

    def no_time(m, t):
        if t == "":
            return True
        # FIXME: How to handle these?
        # Does not stop
        if t == "|":
            return True
        # Goes different way
        if t == "<":
            return True
        return False

    def print_time(m, t):
        if m.no_time(t):
            return ""
        try:
            return "%c%c:%c%c" % (t[0], t[1], t[2], t[3])
        except:
            return "***" + t

    def read_version(m):
        for ver in m.open_csv("VerzeJDF"):
            m.ver = ver[0]
            # For version 1.11
            if m.ver == "1.11":
                m.arr = 10
            elif m.ver == "1.10":
                m.arr = 9
            elif m.ver == "1.9" or m.ver == "1.8":
                m.arr = 8
            else:
                print("## Implement version ", m.ver)
                raise "unknown version"

    def start_conn(m, name):
        print("# ", name, " ( ", m.dir_name, m.ver, ")")
        m.output = ""

    def finish_conn(m, name):
        print(m.output, end='')
        print("Remarks: ", m.print_codes(m.conn_codes[name]))

    def print_code(m, l):
        if l == "":
            return ""
        if l in m.codes:
            return m.codes[l]
        return "**** " + l

    def print_codes(m, l):
        res = ""
        for c in l:
            res += m.print_code(c)
        return res

    def format_conn(m, l0, l1):
        return l0 + "_" + l1

    def read_conns(m):
        m.conn_codes = {}
        for l in m.open_csv("Spoje"):
            c = m.format_conn(l[0], l[1])
            m.conn_codes[c] = l[2:12]

    def run(m):
        m.read_version()
        m.read_stops()
        m.read_codes()
        m.read_conns()

        last = None
        arr = m.arr
        for l in m.open_csv("Zasspoje"):
            name = m.format_conn(l[0], l[1])
            if last != name:
                if last:
                    m.finish_conn(last)
                last = name
                m.start_conn(name)
                reverse = (int(l[1]) % 2) == 0

            codes = m.print_codes(l[6:arr-1]) + m.print_codes(m.stop_codes[l[3]])
            output = "%s\t%s\t%s\t%s\t%s\n" % (m.print_time(l[arr]), m.print_time(l[arr+1]), codes, l[arr-1], m.stop_name[l[3]])
            if m.no_time(l[arr]) and m.no_time(l[arr+1]):
                continue
            if not reverse:
                m.output += output
            else:
                m.output = output + m.output

        m.finish_conn(last)
        

def decode_dir(name):
    dec = Decoder(name)
    dec.run()

def decode_all():
    for i in range(1, 10493):
        decode_dir("tmp/%d" % i)

#decode_dir("tmp/%d" % 10338)
decode_all()
