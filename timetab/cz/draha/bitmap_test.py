#!/usr/bin/python

# A really simple example of using transitfeed to build a Google Transit
# Feed Specification file.

import transitfeed
from optparse import OptionParser

schedule = transitfeed.Schedule()

service_period = schedule.GetDefaultServicePeriod()
service_period.SetWeekdayService(True)
service_period.SetDateHasService('20070704')

print service_period.IsActiveOn('20070704')

service_period.SetDateHasService('20070704', False)

print service_period.IsActiveOn('20070704')

schedule.Validate()

