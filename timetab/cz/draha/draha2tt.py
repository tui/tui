#!/usr/bin/python

import re

class Decoder:
    def __init__(m, dir_name):
        m.dir_name = dir_name

    def open(m, name):
        return open(m.dir_name + "/JizdniRad2016_06-12." + name) # encoding="cp1250"

    def csv_split(m, l):
        return l.split('|')

    def open_csv(m, name):
        return map(m.csv_split, m.open(name).readlines())

    def read_stops(m):
        m.stop_name = {}
        for l in m.open_csv("db"):
            c = l[0]+'|'+l[1]+'|'+l[2]
            m.stop_name[c] = l[3][:-2]

    def print_time(m, t1, t2):
        if t1 == "":
            return ""
        return "%d:%02d" % (int(t1), int(t2))

    def start_conn(m, name):
        m.output = "# " + m.type[name] + name + "\n"

    def finish_conn(m, name):
        m.output += '; ' + m.days_text[m.days[name]]
        print(m.output)
        #print("Remarks: ", m.print_codes(m.conn_codes[name]))

    def read_types(m):
        m.type = {}
        m.days = {}
        for l in m.open_csv("dvl"):
            c = l[0]
            m.type[c] = l[9]
            m.days[c] = l[8]
            #print "vlak ", c, " kalendar ", l[8]

    def read_days(m):
        m.days_bitmap = {}
        m.days_text = {}
        m.days_text['1'] = "jede vzdy (?)"

        d = None
        for l in m.open_csv("kvl"):
            if not d:
                d = l
                continue
            m.days_bitmap[l[0]] = l[1]
            m.days_text[l[0]] = l[2] + l[3]
            d = None
            

    def run(m):
        m.read_stops()
        m.read_types()
        m.read_days()
        #print(m.days_text)

        last = None
        for l in m.open_csv("trv"):
            name = l[0]
            if last != name:
                if last:
                    m.finish_conn(last)
                last = name
                m.start_conn(name)

            c = l[1]+'|'+l[2]+'|'+l[3]
            # FIXME: dny prijezdu, pulminuty prijezdu
            output = "%s\t%s\t%s\t0\t%s\n" % \
            	( m.print_time(l[8], l[9]),
                  m.print_time(l[14], l[15]), "",
                  m.stop_name[c] )

            if l[36] != "1" and l[8] != "" and l[14] != "":
                m.output += output

        m.finish_conn(last)
        

def decode_dir(name):
    dec = Decoder(name)
    dec.run()

decode_dir("tmp/")

