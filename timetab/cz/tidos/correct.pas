{ Program na korekciu vstupu ASCII databazy vo formate .tt}
{ Jana Sefcikova}

program Time_Table(input,output);

const
      {pole tags uchovava tagove znaky ktore sa vyskytuju v strukture .tt databazy}
      MaxTags = 11;
      Tags : array [1..MaxTags] of Char = ('!','#',';','R','G','B','X','@','L','D',#13);
      Tab  : char = #9;
      CR   : char = #13;
      LF   : char = #10;



type kratky_string = string[5];
     dlhy_string   = string[13];

var pathin,pathout : String; {vstupny,vystupny subor}


procedure nacitaj_riadok(var prcas,odcas,rem : kratky_string; var vzd : dlhy_string; var mesto : string; var znak:char);

var i :integer;
begin

  prcas := '';

  {kym nie je znak Tabulator, ziskavame hodnotu prichodu}
  while not(znak=Tab) do
   begin
    prcas := prcas + znak;
    read(znak);
   end;

  read(znak);

  odcas := '';

  {kym nie je znak Tabulator, ziskavame hodnotu odchodu}
  while not(znak=Tab) do
   begin
    odcas := odcas + znak;
    read(znak);
   end;

  read(znak);

  rem:='';

  {kym nie je znak Tabulator, ziskavame hodnotu poznamky}
  while not(znak=Tab) do
   begin
    rem := rem + znak;
    read(znak);
   end;

   read(znak);

  vzd:='';

  {kym nie je znak Tabulator, ziskavame hodnotu vzdialenosti}
  while not(znak=Tab) do
   begin
    vzd := vzd + znak;
    read(znak);
   end;

  readln(mesto);

end;

procedure zapis_riadok(prcas,odcas,rem : kratky_string; vzd : dlhy_string; mesto : string);
begin
 if prcas<>'' then write(prcas);
    write(Tab);
    write(odcas);
    write(Tab);
    if rem <> '' then write(rem:5);
    write(Tab);
    write(vzd:13);
    write(Tab);
    write(mesto);

    {ENTER}
    write(CR);
    write(LF);
end;

{zapise do vystupu tagovy riadok po znakoch}
procedure zapis_tag(var first:boolean; var znak:char);
var ignoruj : boolean;
begin
    {to je to ignorovanie ";" }
     if znak = Tags[3] then ignoruj := true
                       else ignoruj := false;
     first := true;
     while not(znak = LF) do
       begin
        if not(ignoruj) then write(znak);
        read(znak);
       end;
     if not(ignoruj) then write(LF);
end;



{halvna procedura ktora nacita vstupnu databazu, opravi ju a
 zapise do vystupneho suboru}
procedure Ta_Urob;
var

    istag             : boolean;
    i                 : integer;
    prcas1, prcas2,
    odcas1, odcas2    : kratky_string; {cas prichodu a odchodu}
    rem1, rem2        : kratky_string; {poznamky}
    vzd1, vzd2        : dlhy_string;{vzdialenost}
    mesto1, mesto2    : string;

    first,nacitaj     : boolean;
    znak,znak2        : char; {znak pouzivany pre parsovanie}

begin

 first:=true;

 znak := #0;
 nacitaj := true;

  {cyklus ktory postupne prechadza riadky vstupneho suboru a
   zapisuje ich korektne do vystupu}

  While not (EOF) do begin


   {overenie ci ide o Tagovy riadok}
   isTag:=false;
   if nacitaj then read(znak)
   else nacitaj := true;

   {tagova znacka je na prvom znaku v riadku}
   for i:=1 to MaxTags do if znak = tags[i] then isTag := true;

   if isTag then zapis_tag(first,znak) else
    begin
      {nacita #1 riadok v poradi}
      nacitaj_riadok(prcas1,odcas1,rem1,vzd1,mesto1,znak);

      {prvy riadok sa vynechava, lebo vlak zo stanice iba odchadza
       predpoklada sa korektnost zadaneho vstupu}

      if first then begin
       first:=false;
       zapis_riadok(prcas1,odcas1,rem1,vzd1,mesto1);
      end

      else
        {pokial cas prichodu je definovany, vypise sa cely riadok}
        if prcas1<>'' then zapis_riadok(prcas1,odcas1,rem1,vzd1,mesto1) else
         begin

           {nacita sa nasledujuci riadok a zisti sa, ci nazvy miest a
           vzdialenosti suhlasia, pokial ano spoja se do jedneho zaznamu,
           pokial nie tak sa nechaju}

           {nacitanie prveho znaku v riadku (moze byt tagovy riadok)}
           read(znak);

           {overenie ci ide o Tagovy riadok}
           isTag:=false;

           {tagova znacka je na prvom mieste v riadku}
           for i:=1 to MaxTags do if znak = tags[i] then isTag := true;

           if isTag then
           begin

            if (znak = Tags[2]) or (znak = Tags[3]) then  zapis_riadok('',odcas1,rem1,vzd1,mesto1)
                                                    else  zapis_riadok(odcas1,'',rem1,vzd1,mesto1);
             zapis_tag(first,znak);

           end else
           begin

             {ked je prvy nacitany riadok (premennej odcas1, atd.) posledny
             v subore}
             if EOF then zapis_riadok(odcas1,'',rem1,vzd1,mesto1)
             else begin

               {nacita #2 riadok v poradi}
               nacitaj_riadok(prcas2,odcas2,rem2,vzd2,mesto2,znak);

               Read(znak);

               nacitaj:=false;

               isTag:=false;

               {tagova znacka je na prvom mieste v riadku}
               for i:=1 to MaxTags do if znak = tags[i] then isTag := true;

                {pokial sa rovnaju nazvy miest a vzdialenosti,
               zapiseme odchod vlaku z druheho riadku na miesto
               prichodu prveho vlaku}
               if (mesto1 = mesto2) AND (vzd1 = vzd2) then begin

               prcas1 := odcas2;
               zapis_riadok(prcas1,odcas1,rem1,vzd1,mesto1);

               end else
               begin
                 {zapis #1 riadku}

                 zapis_riadok(prcas1,odcas1,rem1,vzd1,mesto1);

                 {ak za #2 riadkom je tag, tak zapise riadok #2 ako posledny}
                 if isTag then begin

                 if (znak = Tags[2]) or (znak = Tags[3]) then zapis_riadok('',odcas2,rem2,vzd2,mesto2)
                                                         else zapis_riadok(odcas2,'',rem2,vzd2,mesto2);

                 end else begin
                   {pokial sme na konci suboru tak vypiseme #2 riadok ako posledny}
                   If EOF then zapis_riadok(odcas2,'',rem2,vzd2,mesto2) else
                   zapis_riadok(prcas2,odcas2,rem2,vzd2,mesto2);

                 end;

             end;

             end;


           end;

         end; {if prcas =''}

    end; {if not(tag)}

  end; {While EOF}

end;

begin

 Ta_Urob;

end.