/*
 *	IDOS Data Extractor
 *
 *	(c) 2000--2001 Martin Mares <mj@ucw.cz>
 *
 *	This software can be freely distributed and used according
 *	to the terms of the GNU General Public License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>

int chunkid = 0;
char *dir;
FILE *in;

unsigned int gl(void)
{
  unsigned int x;
  if (fread(&x, sizeof(x), 1, in) != 1)
    {
      puts("EOF");
      exit(0);
    }
  return x;
}

void sk(int s)
{
  char buf[4096];
  FILE *g;
  sprintf(buf, "%s/%04d", dir, chunkid++);
  if (!(g = fopen(buf, "w"))) { puts("Unable to write file"); exit(1); }
  while (s--)
    fputc(fgetc(in), g);
  fclose(g);
}

void skip(int s)
{
  printf("<skipping %x>\n", s);
  sk(s);
}

void chunks(int c)
{
  int x,y,p;
  while (c--)
    {
      p = ftell(in);
      x = gl();
      if (!x) {
	printf("%d\t%07x\tNULL\n", chunkid, p);
        sk(0);
	continue;
      }
      y = gl();
      if (!y || x % y)
	printf("%d\t%07x\t%07x\t%07x\t<???>\n", chunkid, p, x, y);
      else
	printf("%d\t%07x\t%07x\t%07x\t%d\n", chunkid, p, x, y, x/y);
      sk(x);
     }
}

void string(char *name)
{
  int c;
  printf("<%s: ", name);
  while (c = fgetc(in)) putchar(c);
  puts(">");
}

int main(int argc, char **argv)
{
  int x,w;
  int lang_cnt;
  int major, minor;
  unsigned char hdr[0x64];

  if (argc != 3) { fprintf(stderr, "Usage: extract <idos-file> <directory>"); return 1; }
  in = fopen(argv[1], "r");
  if (!in) { perror("open(infile)"); return 1; }
  dir = argv[2];
  if (mkdir(dir, 0777) && errno != EEXIST) { perror("mkdir(destdir)"); return 1; }

  if (fread(hdr, 1, sizeof(hdr), in) != sizeof(hdr)) { perror("fread(header)"); return 1; }
  major = hdr[0x4a];
  minor = hdr[0x4b];
  printf("Decoding format version %d.%d\n", major, minor);
  if (major < 1 || major > 3)
    printf("### This version is not supported. Continuing, but keep your fingers crossed.\n");

  rewind(in);
  skip(0x64);
  x = lang_cnt = gl();
  printf("Languages: ");
  while (x--)
    putchar(fgetc(in));
  putchar('\n');
  chunks(6);
  string("Filename");
  skip(8);
  printf("0x%x stations\n", gl());
  chunks(31);
  x = gl();
  printf("<expecting %d chunks of names>\n", x);
  w = 1000;
  while (x--)
    {
      puts("<name block>");
      chunkid = w;
      skip(9);
      chunks(8);
      puts("<lang>");
      chunks(2*lang_cnt);
      puts("</lang>");
      puts("</name block>");
      w += 100;
    }
  chunkid = 2000;
  chunks(2*lang_cnt);
  chunkid = 3000;
  skip(8);
  chunks(2);
  skip(32);
  chunks(3);
  skip(32);
  chunks(4);
  chunkid = 4000;
  chunks(4*lang_cnt);
  chunkid = 5000;
  chunks(3+2*lang_cnt);
  chunkid = 5100;
  chunks(3+2*lang_cnt);
  chunkid = 5200;
  chunks(2+2*lang_cnt);
  chunkid = 5300;
  chunks(3);
  chunkid = 6000;
  skip(4);
  chunks(4);
  chunkid = 6100;
  chunks(1+2*lang_cnt);
  chunkid = 6200;
  chunks(1+2*lang_cnt);
  chunkid = 7000;
  puts("<table>");
  x = gl();
  while (x--) chunks(1);
  puts("</table>");
  chunkid = 8000;
  skip(0x390);
  chunks(2);
  skip(4);
  puts("<table>");
  x = gl();
  while (x--) chunks(1);
  puts("</table>");
  chunkid = 9000;
  gl(); gl();
  if (major > 1) {
    gl(); gl(); gl(); gl(); gl();
  }
  gl();
  string("Comment");
  gl();
  puts("<version-dependent part>");
  switch (major) {
	case 1:
	case 2:
		chunks(19);
		break;
	case 3:
		chunks(9);
		skip(0x20);
		chunks(1);
		skip(0x10);
		chunks(3);
		break;
  }
  skip(0x30);
  x = ftell(in);
  fseek(in, 0, SEEK_END);
  printf("<%x bytes remain at %x>\n", (int)ftell(in) - x, x);
  return 0;
}
