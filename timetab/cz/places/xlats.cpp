
#include "xlats.h"




char OdHackujTab[256] = {
  0,   1,   2,   3,   4,   5,   6,   7, 
  8,   9,  10,  11,  12,  13,  14,  15, 
 16,  17,  18,  19,  20,  21,  22,  23, 
 24,  25,  26,  27,  28,  29,  30,  31, 
 32, '!', '"', '#', '$', '%', '&',  39, 
'(', ')', '*', '+', ',', '-', '.', '/', 
'0', '1', '2', '3', '4', '5', '6', '7', 
'8', '9', ':', ';', '<', '=', '>', '?', 
'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 
'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 
'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 
'X', 'Y', 'Z', '[','\'', ']', '^', '_', 
'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 
'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 
'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 
'x', 'y', 'z', '{', '|', '}', '~', '', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', 'S', '�', 'S', 'T', 'Z', 'Z', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', 's', '�', 's', 't', 'z', 'z', 
'�', '�', '�', 'L', '�', 'A', '�', '�', 
'�', '�', 'S', '�', '�', '�', '�', 'Z', 
'�', '�', '�', 'l', '�', '�', '�', '�', 
'�', 'a', 's', '�', 'L', '�', 'l', 'z', 
'R', 'A', 'A', 'A', 'A', 'L', 'C', 'C', 
'C', 'E', 'E', 'E', 'E', 'I', 'I', 'D', 
'D', 'N', 'N', 'O', 'O', 'O', 'O', '�', 
'R', 'U', 'U', 'U', 'U', 'Y', 'T', '�', 
'r', 'a', 'a', 'a', 'a', 'l', 'c', 'c', 
'c', 'e', 'e', 'e', 'e', 'i', 'i', 'd', 
'd', 'n', 'n', 'o', 'o', 'o', 'o', '�', 
'r', 'u', 'u', 'u', 'u', 'y', 't', '�', 
};





char LowerCaseCZTab[256] = {
  0,   1,   2,   3,   4,   5,   6,   7, 
  8,   9,  10,  11,  12,  13,  14,  15, 
 16,  17,  18,  19,  20,  21,  22,  23, 
 24,  25,  26,  27,  28,  29,  30,  31, 
 32, '!', '"', '#', '$', '%', '&',  39, 
'(', ')', '*', '+', ',', '-', '.', '/', 
'0', '1', '2', '3', '4', '5', '6', '7', 
'8', '9', ':', ';', '<', '=', '>', '?', 
'@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 
'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 
'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 
'x', 'y', 'z', '[','\'', ']', '^', '_', 
'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 
'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 
'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 
'x', 'y', 'z', '{', '|', '}', '~', '', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
'�', '�', '�', '�', '�', '�', '�', '�', 
};






void OdHackuj(const char *txt, char *txtOd)	// odstrani hacky a carky; xlattab
{
	for (unsigned i = 0; i <= strlen(txt); i++) txtOd[i] = OdHackujTab[txt[i]];
};







void LowerCaseCZ(const char *txt, char * txtL)	// prevede na lowercase vcetne cestiny
{
	for (unsigned i = 0; i <= strlen(txt); i++) txtL[i] = LowerCaseCZTab[txt[i]];
};



