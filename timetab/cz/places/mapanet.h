// Header M_MapaNet.h
// 
// obsahuje deklarace fci a struct pro M_MapaNet.cpp


#define EXPORT_API



struct S_MapaNet_MestoPos
{
public:
	char * Jmeno;
	int X, Y;
	S_MapaNet_MestoPos *Dalsi, *Predchozi;
};





EXPORT_API S_MapaNet_MestoPos * M_MapaNet_Find(char *Mesto);
EXPORT_API S_MapaNet_MestoPos * M_MapaNet_Cache_Find(char *Mesto);
EXPORT_API S_MapaNet_MestoPos * M_MapaNet_Find_Inet(char *Mesto);

EXPORT_API void M_MapaNet_CleanUp(void);
EXPORT_API int M_MapaNet_StartUp(void);

EXPORT_API void * M_MapaNet_Alloc(unsigned long Pocet);
EXPORT_API void * M_MapaNet_ReAlloc(char *p, unsigned long Pocet);
EXPORT_API void M_MapaNet_Free(void *p);

EXPORT_API void M_MapaNet_Cache_Add(char *Mesto, void *Prvni);
EXPORT_API void M_MapaNet_Cache_Clear(void);


