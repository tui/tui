// M_MapaNet.cpp : Defines the entry point for the DLL application.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/errno.h>
#include "mapanet.h"
#include "xlats.h"



#define Alloc(x) M_MapaNet_Alloc(x)
#define Free(p) M_MapaNet_Free(p)
#define TERMINATE_ON_CR(x) for (unsigned tocr = 0; tocr < strlen(x); tocr++) if ((x[tocr] == 10) || (x[tocr] == 13)) x[tocr] = 0;

#define PAGE_KB 256	// predp. stranka nepresahne 256 KB, pokud ano, vrati se pouze tech 256KB.
#define E_NO_INET 128

#define SOCKET int
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1


//#define MDEBUG_MSGBOX



/*
struct S_MapaNet_List
{
	void *Ptr;
	S_MapaNet_List *Dalsi;
};
*/





//S_MapaNet_List * CleanUpList;
sockaddr StdAddress;
char INet_Present = 0;








void AddToCleanUpList(void *);
void Chyba(char *t);
int IsComplete(char * txt);








inline char HexDigit(unsigned char a)
{
	return (((a) < 10)?('0' + (a)):('A' + (a) - 10));
};







unsigned char * EscapeTxt(unsigned char *txt, unsigned char *buf)
{
	int i = 0, u = 0;
	while (txt[i] != 0)
	{
		if (((txt[i] > 64) && (txt[i] < 128)) || ((txt[i] >= '0') && (txt[i] <= '9')))
		{
			buf[u] = txt[i];
			u++;
		}
		else
		{
			buf[u] = '%';
			buf[u + 1] = HexDigit(txt[i] / 16);
			buf[u + 2] = HexDigit(txt[i] % 16);
			u += 3;
		};

		i++;
	};
	buf[u] = 0;
	return buf;
};










EXPORT_API void M_MapaNet_Cache_Add(char *mesto, S_MapaNet_MestoPos * poz)
{
	FILE *f, *f2;
	char m[] = "a.mmc", m2[] = "cachetmpa.$$$";
	char txt[20];
	char jm[1024];			// predp. ze jmeno neni delsi nez 1023 zn.
	char tmp[2048];			// no garbage please
	char mestoL[1024];	// predp. ze jmeno neni delsi nez 1023 zn.
	S_MapaNet_MestoPos * p = poz;
	int Pocet = 0, UzUlozene = 0, i;

	LowerCaseCZ(mesto, mestoL);

	m[0] = OdHackujTab[mestoL[0]];
	m2[8] = OdHackujTab[mestoL[0]];
	f = fopen(m, "r");
	if (f != NULL)
	{
		fgets(jm , 1024, f);
		TERMINATE_ON_CR(jm);
		while ((!feof(f)) && (strcasecmp(jm, mestoL)))
		{
			fgets(tmp, 2048, f);		// pocet mest
			sscanf(tmp, "%i", &Pocet);	// rescan pocet
			for (int i = 0; i < Pocet; i++)
			{
				fgets(tmp, 2048, f);		// kodovana pozice + presne jmeno
			};
			fgets(jm, 1024, f);
			TERMINATE_ON_CR(jm);
		};
		if (!feof(f))
		{
			// uz je v cache -> prekopirovat celou cache a pozmenit zaznam:

			fseek(f, 0, SEEK_SET);

			f2 = fopen(m2, "w");
			if (f2 == NULL)	return;

			// vsechny zaznamy pred:
			fgets(jm , 1024, f);
			TERMINATE_ON_CR(jm);
			while ((!feof(f)) && (strcasecmp(jm, mestoL)))
			{
				fprintf(f2, "%s\n", jm);
				fgets(tmp, 2048, f);		// pocet mest
				fprintf(f2, "%s", tmp);
				sscanf(tmp, "%i", &Pocet);	// rescan pocet
				for (i = 0; i < Pocet; i++)
				{
					fgets(tmp, 2048, f);		// kodovana pozice + presne jmeno
					fprintf(f2, "%s", tmp);
				};
				fgets(jm , 1024, f);
				TERMINATE_ON_CR(jm);
			};

			// ted pozmenit:
			fprintf(f2, "%s\n", jm);
			fgets(tmp, 2048, f);		// pocet mest
			sscanf(tmp, "%i", &UzUlozene);	// rescan pocet
			Pocet = 0;
			while (p != NULL) 
			{
				p = p->Dalsi;
				Pocet++;
			};
			fprintf(f2, "%i\n", Pocet + UzUlozene);
			p = poz;
			for (i = 0; i < UzUlozene; i++)
			{
				fgets(tmp, 2048, f);
				fprintf(f2, "%s", tmp);
			};
			txt[10] = 0;
			for (int i = 0; i < Pocet; i++)
			{
				txt[0] = ((p->X >> 28) & 127) + 48;
				txt[1] = ((p->X >> 21) & 127) + 48;
				txt[2] = ((p->X >> 14) & 127) + 48;
				txt[3] = ((p->X >> 7) & 127) + 48;
				txt[4] = (p->X & 127) + 48;
				txt[5] = ((p->Y >> 28) & 127) + 48;
				txt[6] = ((p->Y >> 21) & 127) + 48;
				txt[7] = ((p->Y >> 14) & 127) + 48;
				txt[8] = ((p->Y >> 7) & 127) + 48;
				txt[9] = (p->Y & 127) + 48;
				txt[10] = 0;
				if (p->Jmeno == NULL)
				{
					fprintf(f2, "%s %s\n", txt, mesto);
				}
				else
				{
					fprintf(f2, "%s %s\n", txt, p->Jmeno);	// kodovana pozice + presne jmeno
				};
				p = p->Dalsi;
			};
		
			// a nakonec zbytek zaznamu:
			fgets(tmp, 2048, f);
			while (!feof(f))
			{
				fprintf(f2, "%s", tmp);
				fgets(tmp, 2048, f);
			};
			fclose(f2);
			fclose(f);

			// a prejmenovat:
			remove(m);
			rename(m2, m);

			// pokud jsme az tady, tak bylo uspesne pridano.
			return;
		};
		fclose(f);
	};

	f = fopen(m, "a");
	if (f == NULL) return;
	fprintf(f, "%s\n", mestoL);
	Pocet = 0;
	while (p != NULL) 
	{
		p = p->Dalsi;
		Pocet++;
	};
	fprintf(f, "%i\n", Pocet);
	p = poz;
	txt[10] = 0;
	for (i = 0; i < Pocet; i++)
	{
		txt[0] = ((p->X >> 28) & 127) + 48;
		txt[1] = ((p->X >> 21) & 127) + 48;
		txt[2] = ((p->X >> 14) & 127) + 48;
		txt[3] = ((p->X >> 7) & 127) + 48;
		txt[4] = (p->X & 127) + 48;
		txt[5] = ((p->Y >> 28) & 127) + 48;
		txt[6] = ((p->Y >> 21) & 127) + 48;
		txt[7] = ((p->Y >> 14) & 127) + 48;
		txt[8] = ((p->Y >> 7) & 127) + 48;
		txt[9] = (p->Y & 127) + 48;
		txt[10] = 0;
		if (p->Jmeno == NULL)
		{
			fprintf(f, "%s %s\n", txt, mesto);		// kodovana pozice + (presne jmeno == NULL takze se dosadi skupinove)
		}
		else
		{
			fprintf(f, "%s %s\n", txt, p->Jmeno);	// kodovana pozice + presne jmeno
		};
		p = p->Dalsi;
	};
	fclose(f);
};











EXPORT_API void M_MapaNet_Cache_Clear(void)
{
	char m[6];
	m[1] = '.'; m[2] = 'm'; m[3] = 'm'; m[4] = 'c'; // .mmc = M_Mapa_Cache
	m[5] = 0;
	for(m[0]='a'; m[0] <= 'z'; (m[0])++) remove(m);	// odstranit soubory [a-z].mmc
};










EXPORT_API S_MapaNet_MestoPos * M_MapaNet_Cache_Find(char *mesto)
{
	FILE *f;
	char m[] = "a.mmc.";
	char jm[1024];			// predp. ze jmeno neni delsi nez 1023 zn.
	unsigned char tmp[2048];			// no garbage please
	char mestoL[1024];	// predp. ze jmeno neni delsi nez 1023 zn.
	S_MapaNet_MestoPos *p, *l = NULL, *prv = NULL;
	int Pocet = 0;

	LowerCaseCZ(mesto, mestoL);

	m[0] = OdHackujTab[mestoL[0]];
	f = fopen(m, "r");
	if (f == NULL) return NULL;
	fgets(jm , 1024, f);
	TERMINATE_ON_CR(jm);
	while ((!feof(f)) && (strcasecmp(jm, mestoL)))
	{
		fgets((char *)tmp, 2048, f);		// pocet mest
		sscanf((char *)tmp, "%i\n", &Pocet);	// rescan pocet
		for (int i = 0; i < Pocet; i++)
		{
			fgets((char *)tmp, 2048, f);		// kodovana pozice + presne jmeno
		};
		fgets(jm, 1024, f);
		TERMINATE_ON_CR(jm);
	};
	if (feof(f))
	{
		fclose(f);
		return NULL;
	};
	fgets((char *)tmp, 2048, f);		// pocet mest
	sscanf((char *)tmp, "%i\n", &Pocet);	// rescan pocet
	l = NULL;
	prv = NULL;
	for (int i = 0; i < Pocet; i++)
	{
		fgets((char *)tmp, 2048, f);		// kodovana pozice + presne jmeno
		p = (S_MapaNet_MestoPos *)Alloc(sizeof(S_MapaNet_MestoPos));
		p->Dalsi = NULL;
		p->Predchozi = l;
		if ((tmp[10] == 0) || (tmp[11] == 0))
		{
			p->Jmeno = (char *)Alloc(strlen(mesto) + 10);
			memcpy(p->Jmeno, mesto, strlen(mesto) + 10);
		}
		else
		{
			p->Jmeno = (char *)Alloc(strlen((char *)tmp) + 10);
			memcpy(p->Jmeno, &(tmp[11]), strlen((char *)tmp) + 10);
		};
		if (l != NULL) l->Dalsi = p;
		if (prv == NULL) prv = p;
		l = p;
		p->X = ((unsigned long)tmp[0] - 48UL) * 268435456UL + ((unsigned long)tmp[1] - 48UL) * 2097152UL + ((unsigned long)tmp[2] - 48UL) * 16384UL + ((unsigned long)tmp[3] - 48UL) * 128UL + ((unsigned long)tmp[4] - 48UL);
		p->Y = ((unsigned long)tmp[5] - 48UL) * 268435456UL + ((unsigned long)tmp[6] - 48UL) * 2097152UL + ((unsigned long)tmp[7] - 48UL) * 16384UL + ((unsigned long)tmp[8] - 48UL) * 128UL + ((unsigned long)tmp[9] - 48UL);
	};
	fclose(f);
	return prv;
};






EXPORT_API S_MapaNet_MestoPos * M_MapaNet_Find(char *mesto)
{
	S_MapaNet_MestoPos *f;
	f = M_MapaNet_Cache_Find(mesto);
	if (f == NULL)
	{
		f = M_MapaNet_Find_Inet(mesto);
		if (f != NULL) M_MapaNet_Cache_Add(mesto, f);
	};
	return f;
};






EXPORT_API S_MapaNet_MestoPos * M_MapaNet_Find_Inet(char *mesto)
{
	if (!INet_Present) return NULL;
	SOCKET s;
	char txt[4000];
	char * buf;
	char * pb;
	char * oldpb;
	char * cxp, *cyp;
	char cxpb[20], cypb[20];
	int a, poradi = 1, max = 1, len = 0, clen = 0;
	S_MapaNet_MestoPos *p, *l = NULL, *f = NULL;


	buf = (char *)Alloc((PAGE_KB * 1024));		
	if (buf == NULL) return NULL;
	pb = buf;
	s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == INVALID_SOCKET) 
	{
		Chyba("Invalid socket!");
		Free(buf);
		return NULL;
	};



	a = connect(s, &StdAddress, sizeof(sockaddr_in));
	if (a != 0)
	{
		if (errno != -EISCONN)
		{
			Chyba("Bad connect call!");
			close(s);
			Free(buf);
			return NULL;
		};
	};
	// projit ffsechny stranky:
	while (poradi <= max)
	{
		EscapeTxt((unsigned char *)mesto, (unsigned char *)buf);
		sprintf(txt, "GET /aquariusnet/frm_searchEx.asp?SHORTNAME=dop&S_PTRSC=&S_PTRY=&S_IS_IE=False&S_USERID=&S_LANG=CZ&S_BGC=%%232165C6&S_CY=&S_PTRDS=&S_ID=&S_SX=&S_CT=&S_ROUTETOCOORD=&S_ROUTETO=&S_DS=AtlasCR&S_SC=4000000&S_SY=&S_ROUTEFROMCOORD=&S_ROUTEFROM=&SEARCHFOR=%s&S_CX=&S_NAME=&S_LAY=&S_PTRX=&S_PTR=&asc=&trid=&poradi=%i HTTP/1.1\nHost: mapy.atlas.cz\nConnection: Keep-Alive\n\n", buf, poradi);
		a = send(s, txt, strlen(txt), 0);
		if (a == SOCKET_ERROR)
		{
			Chyba("Bad send call!");
			close(s);
			Free(buf);
			return NULL;
		};

		a = 1;
		len = 0;
		buf[0] = 0;
		while (a > 0)
		{
			a = recv(s, &(buf[len]), 128 * 1024 - len, 0);
			if (a == SOCKET_ERROR)
			{
				Chyba("Bad recv call!");
				close(s);
				Free(buf);
				return NULL;
			};
			len += a;
			pb = strstr(buf, "Content-Length:");
			if (pb != NULL)
			{
				oldpb = strstr(pb, "\x0a");	// najit konec radku, at uz CR nebo LF
				if (oldpb == NULL) oldpb = strstr(pb, "\0x0d"); 
				if (oldpb != NULL)
				{
					a = 0;
					while ((!isdigit(pb[a])) && (pb[a] > 31)) a++;
					pb = &(pb[a]);
					a = 0;
					while ((isdigit(pb[a])) && (pb[a] > 31)) a++;
					memcpy(cxpb, pb, a);
					cxpb[a] = 0;
					clen = atol(cxpb);
					if (clen <= len) a = 0; else a = 1;
				};
			};
		};

		buf[len + 1] = 0;

		// process:

//		printf( "%s: processing; length = %i.", mesto, len);

		a = 0;

//    printf("pb pred strstr = %p\n", pb);
		
//		printf("pb: >>%s<<\n", pb);

		pb = strstr(pb, "frm_map.asp?");
		
//		printf("pb po strstr = %p\n", pb);
		
		while (pb != NULL)
		{
			p = (S_MapaNet_MestoPos *)Alloc(sizeof(S_MapaNet_MestoPos));
			p->Dalsi = NULL;
			p->Predchozi = l;
			if (l != NULL) l->Dalsi = p;
			if (f == NULL) f = p;
			l = p;

			cxp = strstr(pb, "&CX=");
			cxp = &(cxp[4]);
			cyp = strstr(pb, "&CY=");
			cyp = &(cyp[4]);
			a = 0;
			while (isdigit(cxp[a])) a++;
			memcpy(cxpb, cxp, a - 1);
			cxpb[a] = 0;
			a = 0;
			while (isdigit(cyp[a])) a++;
			memcpy(cypb, cyp, a - 1);
			cypb[a] = 0;
			p->X = atol(cxpb);
			p->Y = atol(cypb);

			do
			{
				pb = &(strstr(pb, ">")[1]);
			} while (pb[0] == '<');
			cxp = strstr(pb, "<");

			p->Jmeno = (char *)Alloc((cxp - pb) + 1);
			memcpy(p->Jmeno, pb, (cxp - pb));
			p->Jmeno[cxp - pb] = 0;

			oldpb = &pb[cxp - pb];
			pb = strstr(pb, "frm_map.asp?");

		};
	
		while (oldpb != NULL)
		{
			pb = strstr(oldpb, ">Str");
			if (pb != NULL)
				oldpb = strstr(pb, "nka");
			else
				oldpb = NULL;
			if ((oldpb - pb) == 5)
			{
				oldpb = &(oldpb[3]);
				a = 0;
				while (!isdigit(oldpb[a])) a++;
				while (isdigit(oldpb[a])) a++;
				while (!isdigit(oldpb[a])) a++;
				cxp = &(oldpb[a]);
				a = 0;
				while (isdigit(cxp[a])) a++;
				memcpy(cxpb, cxp, a);
				cxpb[a] = 0;
				max = atol(cxpb);
			};
		};

		poradi++;
	};

	close(s);
	Free(buf);

//	printf("%s: Finished (pages: %i)", mesto, max);

	return f;
};











EXPORT_API void M_MapaNet_CleanUp(void)
{
  /*
	S_MapaNet_List * l, *m;
	l = CleanUpList;
	while (l != NULL)
	{
		m = l->Dalsi;
		Free(l->Ptr);
		Free(l);
		l = m;
	};
  */
};









EXPORT_API int M_MapaNet_StartUp(void)
{
//	char txt[4000];
	hostent * h;
	h = gethostbyname("mapy.atlas.cz");
	if (h == NULL) 
	{
		INet_Present = 0;
		return E_NO_INET;
	};
	INet_Present = !0;
	memcpy(&(StdAddress.sa_data[2]), h->h_addr_list[0], 4);
	StdAddress.sa_family = AF_INET;
	StdAddress.sa_data[1] = 80;
	StdAddress.sa_data[0] = 0;
	return 0;
};





EXPORT_API void * M_MapaNet_Alloc(unsigned long Pocet)
{
	return malloc(Pocet);
}



EXPORT_API void M_MapaNet_Free(void *p)
{
	free(p);
}



void AddToCleanUpList(void *p)
{
  /*
	S_MapaNet_List *l;
	l = (S_MapaNet_List *)Alloc(sizeof(S_MapaNet_List));
	l->Dalsi = CleanUpList;		// mozna kolize??
	CleanUpList = l;
	l->Ptr = p;
  */
};







void Chyba(char *t)
{
  printf("%s %m\n", t);
};







int IsComplete(char * txt)
{
	return 0;
}








int main(int argc, char *arg[])
{
	char buf[10240];
	S_MapaNet_MestoPos * m, *p;

	M_MapaNet_StartUp();
	while(fgets(buf, 10240, stdin)) 
	{
    TERMINATE_ON_CR(buf);
//	  printf("Searching for %s\n", buf);
	  m = (S_MapaNet_MestoPos *) M_MapaNet_Find(buf);
	  while (m != NULL)
	  {
	    printf("%d %d %s\n", m->X, m->Y, m->Jmeno);
	    p = m;
	    m = m->Dalsi;
	    M_MapaNet_Free(p);
	  }
	  printf("\n");
	}
}

