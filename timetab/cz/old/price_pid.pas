{distributed under GPL 2
author > Vojtech kulvait
vojta@cbox.cz
verze : 1.0
}

{ promena opravneni uchovava
pozici v case pro kazde pasmo
hlavni funkce vola s prvni
pozici v case, kde uz
neplati opravneni }
type pole1_7=array[1..7] of integer;
var pole:array[1..200]of array[1..7] of integer;
    pole_stringu: array[1..200]of string;
    xpasmo : array[1..7] of boolean;
    listky,opravneni, opravneni0, cas_listku_spicka, cas_listku_mimo:pole1_7;
    cena_listku:pole1_7;

    i: integer;
    citac_metra, citac_off:integer;
    cena: integer;
    psp_listku,nejlepsi_posloupnost,radek:string;
    prac_den, dospely: boolean;
    cas: integer;
    hodiny, minuty,m_hod, m_min: byte;




{----------------------------------------------------------------------------------}

function cas_listku(listek:integer;spicka:boolean):integer;
begin
 if(spicka=true) then cas_listku:=cas_listku_spicka[listek] else cas_listku:=cas_listku_mimo[listek];
end;

{----------------------------------------------------------------------------------}

function pozice_v_poli(pozice_v_case:integer):integer;
var i:integer;
begin
i:=1;
  while(((pole[i][1]>pozice_v_case) or (pole[i][7]<=pozice_v_case)))do
                                         {non (pole[i][1]<=pozice_v_case and pole[i][7]>pozice_v_case)}
  begin
  i:=i+1
  end;
pozice_v_poli:=i
end;
{----------------------------------------------------------------------------------}

function jespicka(pozice_v_case:integer):boolean;{zda j�zdenka plat� jako ve/ mimo spi�ku rozhoduje moment oznacen�*/}
var i:integer;
begin
i:=pozice_v_poli(pozice_v_case);
  if pole[i][3]=1 then jespicka:= true else jespicka:=false;
end;
{----------------------------------------------------------------------------------}

function najdi_pasmo(pozice_v_case:integer):integer;
var i:integer;
begin
i:=pozice_v_poli(pozice_v_case);
najdi_pasmo:=pole[i][2];
end;
{----------------------------------------------------------------------------------}


function spocitej(listky:pole1_7):integer;
begin
spocitej:=(cena_listku[1]*listky[1]+cena_listku[2]*listky[2]+cena_listku[3]*listky[3]
+cena_listku[4]*listky[4]+cena_listku[5]*listky[5]+cena_listku[6]*listky[6]+cena_listku[7]*listky[7]);
end;{v poli jsou konstaty pro detsky/dospely listek}
{----------------------------------------------------------------------------------}

function bus_je_500(pozice_v_case:integer):boolean;{rika, zda na dane lince plati neprestupni kratkodoba jizdenka l0}
var i:integer;
begin
i:=pozice_v_poli(pozice_v_case);
  if pole[i][4]=1 then
    bus_je_500:=true
  else bus_je_500:=false
end;
{----------------------------------------------------------------------------------}


procedure upgrade_opravneni(pozice_v_case :integer;listek :integer;var opravneni:pole1_7);
var pasmo, list,i :integer;
    spicka:boolean;
begin
pasmo:=najdi_pasmo(pozice_v_case);
spicka:=jespicka(pozice_v_case);
if(listek=1) then
 begin
  list:=1;
  citac_metra:=0;
 end
 else list:=listek-1;    {listek 1 plati 2 pasma ostatni plati jako jejich cislo}
  for i:=pasmo-list to pasmo+list do
   begin
    if((i>=1) and (i<=7)) then
       begin
         if(pozice_v_case+cas_listku(listek,spicka)>opravneni[i]) then
         opravneni[i]:=pozice_v_case+cas_listku(listek,spicka);

       end{pokud si stippnu l�stek v x-t�m p�smu,potom plati v tomto pasmu}
   end    {a protoze  nelze objektivne poznat v jakych okolnich pasmech jsem ho uzil}
end;      { plati v pasmech (pasmova_platnost-1)==list na obe strany}
{----------------------------------------------------------------------------------}

function kamdojedu(pozice_v_case:integer;opravneni:pole1_7;var opravneni0:pole1_7):integer;
{opravneni 0 volano odkazem, aby v dalsi funkci
 bylo jen opravdu platne - zavisi i na prestupech}
var i, j, navratovy_cas:integer;
    zmeneno:boolean;
    opr:pole1_7;
begin
citac_metra:=0;
zmeneno:=false;
i:=pozice_v_poli(pozice_v_case);
for j:=1 to 7 do
  begin
   if(opravneni[j]>opravneni0[j]) then
     opr[j]:=opravneni[j]
   else
     opr[j]:=opravneni0[j]
  end;
if(opr[pole[i][2]]>=pole[i][6]) then
  begin
  navratovy_cas:=pole[i+1][1];
  if pole[i][5]>0 then
     citac_metra:=citac_metra+1 else citac_metra:=0;
  i:=i+1;
  zmeneno:=false;
    while(pole[i][2]<>0) and (opr[pole[i][2]]>=pole[i][6])do
      begin
       if pole[i][5]>0 then citac_metra:=citac_metra+1 else citac_metra:=0;
       if((zmeneno=false) and (pole[i][5]<>pole[i-1][5])) then
           begin
             if (pole[i-1][5]<0) or (pole[i][5]<0) then
                 begin
                    for j:=1 to 7 do
                        begin
                        opr[j]:=opravneni[j];
                        opravneni0[j]:=0;
                        end;{of for}
                    zmeneno:=true;
                 end
             else
                 begin
                 if citac_metra=5 then
                     begin
                     for j:=1 to 7 do
                          begin
                          opr[j]:=opravneni[j];
                          opravneni0[j]:=0;
                          end;{of for}
                     citac_metra:=0;
                     zmeneno:=true;
                     end
                 else
                     begin
                     navratovy_cas:=pole[i+1][1];
                     i:=i+1;
                     end
                 end {of else}
           end{ of if zmeneno...}
       else{of if}
          begin
             if citac_metra=5 then citac_metra:=0;
          navratovy_cas:=pole[i+1][1];
          i:=i+1;
          end{of if-else}
      end{while}
  end{of if} else
    navratovy_cas:=opr[pole[i][2]];



      if (navratovy_cas<>-1) then
      begin
        if  (navratovy_cas < opr[pole[i][2]]) then
         kamdojedu:=opr[pole[i][2]]
         else kamdojedu:=navratovy_cas;

      end
      else
         kamdojedu:=navratovy_cas;


end;{of funkcion}
{----------------------------------------------------------------------------------}

procedure najdi(pozice_v_case:integer;listek:integer;opravneni,opravneni0,listky:pole1_7;psp_listku:string);
var dojezd,i: integer;
begin
   if(spocitej(listky)>=cena) then {uz m�m leps� reseni}
     else
      begin
       if((bus_je_500(pozice_v_case)=true) and (listek=0)) then {existuje lepsi reseni}
          else
           begin
              case listek of                         {psp obsahuje za sebou koupene listky}
                  1:   psp_listku:=psp_listku+'1';
                  2:   psp_listku:=psp_listku+'2';
                  3:   psp_listku:=psp_listku+'3';
                  4:   psp_listku:=psp_listku+'4';
                  5:   psp_listku:=psp_listku+'5';
                  6:   psp_listku:=psp_listku+'6';
                  7:   psp_listku:=psp_listku+'7'
               end;
           listky[listek]:=listky[listek]+1;
             if(listek=1) then
             upgrade_opravneni(pozice_v_case,listek,{}opravneni0)
             else
             upgrade_opravneni(pozice_v_case,listek,{}opravneni);
          dojezd:= kamdojedu(pozice_v_case,opravneni,{}opravneni0);
             if (dojezd=-1) then  {vrati prvni casovy bod, kde mi neplati jizdenka nebo -1 na konci}
             begin
                 if(spocitej(listky)<cena) then
                    begin
                     cena:=spocitej(listky);
                     nejlepsi_posloupnost:=psp_listku;
                    end
             end
             else
             begin
                for i:=7 downto 1 do {myslim, ze je rychlejsi}
                begin
                najdi(dojezd,i,opravneni,opravneni0,listky,psp_listku );
                end
             end
           end
         end
end;

{---------tahle fce spolu s upravenym nacitanim staic mi taky zajisti vystup konkretnich stanic, pro nakup listku------}
procedure najdi_off(pozice_v_case:integer;listek:integer;opravneni,opravneni0:pole1_7);
var dojezd,i, lst: integer;
begin

 lst:=ord(nejlepsi_posloupnost[citac_off])-ord('0');
 citac_off:=citac_off+1;
 Write('Na zastavce '+pole_stringu[pozice_v_poli(pozice_v_case)]+' si kupte');
  case lst of
  1: if dospely then  Writeln(' osmikorunovy') else  Writeln(' ctyrkorunovy') ;
  2: if dospely then  Writeln(' dvanactikorunovy') else  Writeln(' sestikorunovy');
  3: if dospely then  Writeln(' osmnactikorunovy') else  Writeln(' devitikorunovy');
  4: if dospely then  Writeln(' dvacetictyrkorunovy') else  Writeln(' dvanactikorunovy');
  5: if dospely then  Writeln(' tricetikorunovy') else  Writeln(' patnactikorunovy');
  6: if dospely then  Writeln(' tricetisestikorunovy') else  Writeln(' osmnactikorunovy');
  7: if dospely then  Writeln(' ctyricetidvoukorunovy') else  Writeln(' dvacetijednakorunovy');
 end;
 if(listek=1) then
     upgrade_opravneni(pozice_v_case,listek,{}opravneni0)
 else
     upgrade_opravneni(pozice_v_case,listek,{}opravneni);
 dojezd:= kamdojedu(pozice_v_case,opravneni,{}opravneni0);
 if (dojezd=-1) then  {vrati prvni casovy bod, kde mi neplati jizdenka nebo -1 na konci}
     writeln('Stastnou cestu')
 else
     najdi_off(dojezd,lst,opravneni,opravneni0);
end;



                     {1= cas  citac_off
                      2= pasmo
                      3= spicka
                      4= nocni
                      5=kolikaty
                      6=cas konce jizdy
                      7=cas o prvek dal}

{---------tahle fce spolu s upravenym nacitanim staic mi taky zajisti vystup konkretnich stanic, pro nakup listku------}

procedure nacti_ignorovana_pasma(pasmo:integer);
var ma:boolean;
    i:integer;
begin
ma:=false;
case pasmo of
1 : for i:=1 to ParamCount do if ParamStr(i)='P' then ma:=true;
2 : for i:=1 to ParamCount do if ParamStr(i)='0' then ma:=true;
3 : for i:=1 to ParamCount do if ParamStr(i)='1' then ma:=true;
4 : for i:=1 to ParamCount do if ParamStr(i)='2' then ma:=true;
5 : for i:=1 to ParamCount do if ParamStr(i)='3' then ma:=true;
6 : for i:=1 to ParamCount do if ParamStr(i)='4' then ma:=true;
7 : for i:=1 to ParamCount do if ParamStr(i)='5' then ma:=true;
end;

if ma=true then
begin
  case pasmo of
  1 : begin xpasmo[1]:=true; xpasmo[2]:=true; writeln('Prazska tramvajenka'); end;
  2 : begin xpasmo[2]:=true; writeln('Opravneni na 0 pasmo.'); end;
  3 : begin xpasmo[3]:=true; writeln('Opravneni na 1 pasmo.'); end;
  4 : begin xpasmo[4]:=true; writeln('Opravneni na 2 pasmo.'); end;
  5 : begin xpasmo[5]:=true; writeln('Opravneni na 3 pasmo.'); end;
  6 : begin xpasmo[6]:=true; writeln('Opravneni na 4 pasmo.'); end;
  7 : begin xpasmo[7]:=true; writeln('Opravneni na 5 pasmo.'); end;
end
end
end;
{----------------------------------------------------------------------------------}

procedure nacti_parametry_uzivatele;
var ano_ne:boolean;
begin
ano_ne:=true;
for i:=1 to ParamCount do
 if ParamStr(i)='d' then ano_ne:=false;

   if ano_ne=true then
      begin
      dospely:=true;
      cena_listku[1]:=8;
      cena_listku[2]:=12;
      cena_listku[3]:=18;
      cena_listku[4]:=24;
      cena_listku[5]:=30;
      cena_listku[6]:=36;
      cena_listku[7]:=42;
      end else
      begin
      dospely:=false;
      writeln('Snizeny tarif');
      cena_listku[1]:=4;
      cena_listku[2]:=6;
      cena_listku[3]:=9;
      cena_listku[4]:=12;
      cena_listku[5]:=15;
      cena_listku[6]:=18;
      cena_listku[7]:=21;
      end;
      for i:=1 to 7 do
      nacti_ignorovana_pasma(i);

      prac_den:=true;
      for i:=1 to ParamCount do
      if ParamStr(i)='s' then begin prac_den:=false;  writeln('Den bez spicky'); end

end;
{----------------------------------------------------------------------------------}
procedure cti_cas;
var a:integer;
begin
a:= pos(':',radek);
m_min:=ord(radek[a+2])-ord('0')+10*(ord(radek[a+1])-ord('0'));
m_hod:=10*(ord(radek[a-2])-ord('0'))+ord(radek[a-1])-ord('0');
end;
{----------------------------------------------------------------------------------}
function hodnota_casu(kolik:integer):string;
var cas, h, m:integer;
    hod,min:string[2];
begin
cas:=m_hod*60+m_min+kolik;
h:=cas div 60;
m:=cas mod 60;
Str(h,hod);
Str(m,min);
hodnota_casu[1]:=hod[1];
hodnota_casu[2]:=hod[2];
hodnota_casu[3]:=':';
hodnota_casu[4]:=min[1];
hodnota_casu[5]:=min[2];

end;
{----------------------------------------------------------------------------------}



procedure nacti_radek;
var c:char;
    no_radek, cas : string;
    citac, a, i, kolik :integer;
    cetl:boolean;
begin
cetl:=false;
radek:='';
no_radek:='';

readln(radek);
   
if radek[1]=';' then nacti_radek;

   
for citac:=1 to length(radek) do
  no_radek:=no_radek + ' ';
if (pos('#',radek) > pos(':',radek)) and (pos(':',radek)<>0) then
   begin
   cti_cas;
   a:= pos(':',radek);
   for citac:=1 to 5 do
      begin
      no_radek[citac]:=radek[a-3+citac];
      radek[a-3+citac]:=' ';
      end;
   if (pos('#',radek) > pos(':',radek)) and (pos(':',radek)<>0) then
      begin
      cti_cas;
      a:= pos(':',radek);
      for citac:=1 to 5 do
         begin
         no_radek[citac+6]:=radek[a-3+citac];
         radek[a-3+citac]:=' ';
         end
      end;

      if (pos('#',radek) > pos('+',radek)) and (pos('+',radek)<>0) then
        begin
        a:= pos('+',radek);
        citac:=a+1;
        kolik:=0;
        while ((ord(radek[citac])-ord('0'))>-1) and ((ord(radek[citac])-ord('0'))<11) do
           begin
           kolik:=10*kolik+(ord(radek[citac])-ord('0'));
           citac:=citac+1;
           end;
        cas:= hodnota_casu(kolik);
        for citac:=1 to 5 do
          no_radek[citac+6]:=cas[citac];
        end;

    citac:=a;
    while cetl=false do
       begin
       if radek[citac]='#'then cetl:=true;
       if ((ord(radek[citac])-ord('0'))>-1) and ((ord(radek[citac])-ord('0'))<6) then
         begin
         no_radek[13]:=radek[citac];
         cetl:=true;
         end;
       citac:=citac+1;
      end;
   for citac:=pos('#',radek) to length(radek) do
      begin
      no_radek[15+citac-pos('#',radek)]:=radek[citac];

      end;
   radek:=no_radek;

   end else
   if (pos('#',radek) > pos('+',radek)) and (pos('+',radek)<>0) then
     begin
        a:= pos('+',radek);
        citac:=a+1;
        radek[a]:=' ';
        kolik:=0;
        while ((ord(radek[citac])-ord('0'))>-1) and ((ord(radek[citac])-ord('0'))<11) do
           begin
           kolik:=10*kolik+(ord(radek[citac])-ord('0'));
           radek[citac]:=' ';
           citac:=citac+1;
           end;
        cas:= hodnota_casu(kolik);
        for citac:=1 to 5 do
          no_radek[citac]:=cas[citac];
        if (pos('#',radek) > pos('+',radek)) and (pos('+',radek)<>0) then
          begin
          a:= pos('+',radek);
          radek[a]:=' ';
          citac:=a+1;
          kolik:=0;
          while ((ord(radek[citac])-ord('0'))>-1) and ((ord(radek[citac])-ord('0'))<11) do
            begin
            kolik:=10*kolik+(ord(radek[citac])-ord('0'));
            radek[citac]:=' ';
            citac:=citac+1;
            end;
          cas:= hodnota_casu(kolik);
          for citac:=1 to 5 do
            no_radek[citac+6]:=cas[citac];
          end;

    citac:=a;
    while cetl=false do
       begin
       if radek[citac]='#'then cetl:=true;
       if ((ord(radek[citac])-ord('0'))>-1) and ((ord(radek[citac])-ord('0'))<6) then
         begin
         no_radek[13]:=radek[citac];
         cetl:=true;
         end;
       citac:=citac+1;
      end;
   for citac:=pos('#',radek) to length(radek) do
      begin
      no_radek[15+citac-pos('#',radek)]:=radek[citac];

      end;
   radek:=no_radek;

   end;


end;
{----------------------------------------------------------------------------------}


          
procedure nacti_cas(pozice:integer; kam:integer);
var cas:integer;
begin
if kam=1 then
 begin
 cas:=(ord(radek[5])-ord('0'))+10*(ord(radek[4])-ord('0'))+60*(ord(radek[2])-ord('0'))+600*(ord(radek[1])-ord('0'));
 if (cas -(60*hodiny)-minuty)>=0 then
  begin
  pole[pozice][1]:=cas+1 -(60*hodiny)-minuty;
    if pozice>1 then pole[pozice-1][7]:=cas+1 -(60*hodiny)-minuty;
  end
 else
  begin
  pole[pozice][1]:=cas-(60*hodiny)-minuty+1440+1;
    if pozice>1 then pole[pozice-1][7]:=cas -(60*hodiny)-minuty+1440+1;
  end
 end;


if kam=6 then
begin
 if (radek[9]=':') then
 cas:=(ord(radek[11])-ord('0'))+10*(ord(radek[10])-ord('0'))+60*(ord(radek[8])-ord('0'))+600*(ord(radek[7])-ord('0'))
 else
 cas:=(ord(radek[5])-ord('0'))+10*(ord(radek[4])-ord('0'))+60*(ord(radek[2])-ord('0'))+600*(ord(radek[1])-ord('0'));
 if (cas -(60*hodiny)-minuty)>=0 then
  pole[pozice][6]:=cas+1 -(60*hodiny)-minuty
 else
  pole[pozice][6]:=cas -(60*hodiny)-minuty+1440+1;

end

end;
{----------------------------------------------------------------------------------}


procedure nacti_pasmo(pozice:integer);
begin
if (radek[9]=':') then
 begin
 if radek[13]='P' then
  pole[pozice][2]:=1;

 if ((ord(radek[13])-ord('0'))>-1) and ((ord(radek[13])-ord('0'))<6) then
    begin
    pole[pozice][2]:=ord(radek[13])-ord('0')+2;
    end;

 if (radek[13]<>'P') and ( ((ord(radek[13])-ord('0'))<0) or ((ord(radek[13])-ord('0'))>5) ) then
    pole[pozice][2]:=1;

 end else
 begin
   if radek[7]='P' then
      pole[pozice][2]:=1;

   if((ord(radek[7])-ord('0'))>-1) and ((ord(radek[7])-ord('0'))<6) then
      pole[pozice][2]:=ord(radek[7])-ord('0')+2;


   if (radek[7]<>'P') and ( ((ord(radek[7])-ord('0'))<0) or ((ord(radek[7])-ord('0'))>5) ) then
      pole[pozice][2]:=1;
 end
end;
{----------------------------------------------------------------------------------}

procedure nacti_spicku(pozice:integer);
var hodina:integer;
begin
hodina:=(ord(radek[2])-ord('0'))+10*(ord(radek[1])-ord('0'));

if prac_den=true then {spicka je 5.00 - 19.59}
   begin
   if (hodina>4) and (hodina<20) then
     pole[pozice][3]:=1
   else
     pole[pozice][3]:=0;
   end
  else
     pole[pozice][3]:=0;
end;
{----------------------------------------------------------------------------------}



function nema_opravneni:boolean;
var pasmo:integer;
begin
if (radek[9]=':') then
 begin
 if radek[13]='P' then
  pasmo:=1;

 if ((ord(radek[13])-ord('0'))>-1) and ((ord(radek[13])-ord('0'))<6) then
    begin
    pasmo:=ord(radek[13])-ord('0')+2;
    end;

 if (radek[13]<>'P') and ( ((ord(radek[13])-ord('0'))<0) or ((ord(radek[13])-ord('0'))>5) ) then
    pasmo:=1;

 end else
 begin
   if radek[7]='P' then
      pasmo:=1;

   if((ord(radek[7])-ord('0'))>-1) and ((ord(radek[7])-ord('0'))<6) then
      pasmo:=ord(radek[7])-ord('0')+2;


   if (radek[7]<>'P') and ( ((ord(radek[7])-ord('0'))<0) or ((ord(radek[7])-ord('0'))>5) ) then
      pasmo:=1;
 end;
if xpasmo[pasmo]=true then
  nema_opravneni:=false
else
  nema_opravneni:=true;

end;
{----------------------------------------------------------------------------------}

function sekni(radek:string):string;
var sek:string;
    i, k : integer;
begin
i:=1;
sek:='';
while i < length(radek) do
begin
k:=ord(radek[i])-ord('0');
   if (k>-1) and (k<10) then
     sek:=''
   else
    sek:=sek+radek[i];
 i:=i+1;
end;

sekni:=sek;
end;




{----------------------------------------------------------------------------------}
procedure nacti_parametry_tt;
var spoj, metro, menitko:integer;
var i,k: integer;
var cetl, novy :boolean;
begin
novy:=true;
cetl:=false;
cas:=1;
metro:=1;
writeln('Prosim vlozte vstupni data ....>>');
nacti_radek;
while(pos('!connection',radek))=0 do nacti_radek;
i:=0;
while(pos('#',radek)=0) or (spoj=4) do
begin
nacti_radek;
if (pos('Bus_',radek)<>0)or(pos('Tram_',radek)<>0)   then begin  spoj:=1; novy:=true end;
if (pos('Metro_',radek)<>0) then begin novy:=true; spoj:=2; end;
if (pos('Sp_',radek)<>0)or (pos('Tram_5',radek)<>0)or(pos('Bus_5',radek)<>0)then begin spoj:=3; novy:=true end;
if (pos('Walk_',radek)<>0)  then begin novy:=true;  spoj:=4; end;
end;
while ((cetl=false) and (pos('!endconnection',radek)=0))  do
   begin
   nacti_radek;
   if (pos('Bus_',radek)<>0)or(pos('Tram_',radek)<>0)   then begin spoj:=1; novy:=true end;
   if (pos('Metro_',radek)<>0) then begin spoj:=2; novy:=true end;
   if (pos('Sp_',radek)<>0)or (pos('Tram_5',radek)<>0)or(pos('Bus_5',radek)<>0)then  begin novy:=true; spoj:=3; end;
   if (pos('Walk_',radek)<>0)  then begin novy:=true; spoj:=4; end;
   if nema_opravneni= false then novy:=true;
   if (pos(':',radek)=3) and (pos('#',radek)<>0) and nema_opravneni= true (*kontrola ignorovanzch pasem*)then
     begin
     cetl:=true;
     hodiny:=(ord(radek[2])-ord('0'))+10*(ord(radek[1])-ord('0'));
     minuty:=(ord(radek[5])-ord('0'))+10*(ord(radek[4])-ord('0'));
     pole[1][1]:=1;
     pole_stringu[1]:=sekni(radek);
     nacti_pasmo(1);
     nacti_spicku(1);
   if spoj=3 then     (*bus je 500*)
     pole[1][4]:=1
   else pole[1][4]:=0;
   if spoj=2 then begin pole[1][5]:=1; metro:=metro+1 end
      else pole[1][5]:=-1;
   menitko:=-1;
   nacti_cas(1,6);
   novy:=false;
   end
 end;
 i:=1;
 while (pos('!endconnection',radek))=0 do
 begin
   nacti_radek;
   if (pos('#',radek))<>0 then
   begin
   if (pos('Bus_',radek)<>0)or(pos('Tram_',radek)<>0)   then
     begin
     spoj:=1;
     novy:=true;
     if menitko=-1 then begin menitko:=-2; metro:=1 end;
     if menitko=-2 then begin menitko:=-1; metro:=1 end;
     end;
   if (pos('Metro_',radek)<>0) then begin spoj:=2; novy:=true; end ;
   if (pos('Sp_',radek)<>0)or (pos('Tram_5',radek)<>0)or(pos('Bus_5',radek)<>0)then
     begin
     spoj:=3;
     novy:=true;
     if menitko=-1 then begin menitko:=-2; metro:=1 end;
     if menitko=-2 then begin menitko:=-1; metro:=1 end;
     end;
   if (pos('Walk_',radek)<>0)  then begin novy:=true; spoj:=4; end ;
   if nema_opravneni= false then novy:=true;

   if (pos(':',radek)=3) and (spoj<>4)and nema_opravneni= true (*kontrola ignorovanzch pasem*) then
        begin
         i:=i+1;

         nacti_cas(i,1);{na pozici -1 prvek 7 da tento cas uz funkce}
         pole_stringu[i]:=sekni(radek);
         nacti_pasmo(i);
         nacti_spicku(i);
         if spoj=3 then pole[i][4]:=0 else pole[i][4]:=1;
         if spoj=2 then begin pole[i][5]:=metro; metro:=metro+1 end
             else
                pole[i][5]:=menitko;
         nacti_cas(i,6);
         if novy=false  then
           pole[i-1][6]:=pole[i-1][7];
         novy:=false
        end
   end
 end;
 pole[i+1][1]:=-1;
 pole[i][7]:=pole[i][1]+1;
 if cetl=false then  pole[i][1]:=-1;
  { 1= cas  od 1(prvni minuta) do x    }
  { 2= pasmo 1=P 2=0 3=1 4=2 5=3 6=4 7=5}
  { 3= spicka true=1 false=0 }
  { 4= nocni/lanovka/vlak -neplati 8kc listek true=1 false=0  }
  { 5=kolikaty u autobusu a tramvaji budu menit -1/-2 a u metra budu pocitat 1..n stanice}
  { 6=kam az dojedu s temito parametry}
  { 7=novy cas 1 cas dalsiho prvku pole}



end;



{---------------zacatek programu-------------------------------------------------------------------}




begin
cena:=1770;{pak je lepe mesicni casovou jizdenku a doplnkove kupony }
citac_metra:=0;
citac_off:=1;
cas_listku_spicka[1]:=15;
cas_listku_spicka[2]:=60;
cas_listku_spicka[3]:=90;
cas_listku_spicka[4]:=120;
cas_listku_spicka[5]:=150;
cas_listku_spicka[6]:=180;
cas_listku_spicka[7]:=210;
cas_listku_mimo[1]:=15;
cas_listku_mimo[2]:=90;
cas_listku_mimo[3]:=90;
cas_listku_mimo[4]:=120;
cas_listku_mimo[5]:=150;
cas_listku_mimo[6]:=180;
cas_listku_mimo[7]:=210;

  for i:=1 to 7 do
   begin
     opravneni[i]:=0;
     listky[i]:=0;
     opravneni0[i]:=0;
     xpasmo[i]:=false
   end;

nacti_parametry_uzivatele;
nacti_parametry_tt;

if pole[1][1]=-1 then writeln('Svezou Vas zadarmo') else
begin
  for i:=7 downto 1 do
  begin
       najdi(1,i,opravneni,opravneni0,listky,psp_listku);
 end;
writeln('Minimalni cena je ', cena,' Kc.' );
writeln('Kupte si tyto listky :');

    for i:=1 to 7 do
   begin
     opravneni[i]:=0;
     opravneni0[i]:=0
   end;
  najdi_off(1,(ord(nejlepsi_posloupnost[citac_off])-ord('0')),opravneni,opravneni0);
end {of if/ else}
end.{of file}
