{
Does it go?? - from the notice by the train and from the date, the program
decides if the train goes at this day.

   Copyright (C) 2001  Martin Duchacek <duchacek.martin@centrum.cz>

   This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option) any
later version.

   This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation,Inc.,
675 Mass Ave, Cambridge, MA 02139, USA.
}
unit exists2;

interface

function jede(poznamka: string) :char;

implementation

const kolik: array[1..12] of integer = (31,28,31,30,31,30,31,31,30,31,30,31);
      zacden=1;
      zacmes=8;
      zacrok=2001;     {tady je platnost j.r.}
      konden=1;
      konmes=7;
      konrok=2002;

var den,mesic,rok: integer;
    vrchol:integer;
    vrcholdny:integer;
    dny: array [1..370] of record den:integer;
                                  mesic:integer;
                           end;
    pomocnepole: array [1..370] of record den:integer;
                                          mesic:integer;
                                   end;



function prestupny(rok:integer):boolean;
{funkce urci, zda rok zadany na vstupu je prestupny nebo ne}
begin
prestupny:=false;
if rok mod 4=0 then prestupny:=true;
if rok mod 100=0 then prestupny:=false;
if rok mod 400=0 then prestupny:=true;
end;


function kolikdni(cislo:integer) :integer;
{vrati pocet dni v danem mesici}
begin
if cislo=2 then
 begin
 if prestupny(rok) then kolikdni:=29
                   else kolikdni:=28;
 end
           else
 kolikdni:=kolik[cislo]
end;

function urciprvniden(rok:integer): integer;
{funkce urci prvni den zadaneho roku, vraci pondeli-nedele 1-7}
var
s,i:integer;
begin
s:=0;
for i:=1990 to rok-1 do begin
                        if prestupny(i)=true then s:=s+366
                                             else s:=s+365;
                        end;
s:=s mod 7;
urciprvniden:=s+1;
end;


procedure rozdel1(var pozn:string;chyba:integer);
{procedura rozdeli poznamku ze vstupu na datum, ktery prevede na integer do
promennych den, mesic, rok a na poznamku}
var pomoc,cislo:string;
procedure precticislo(p:string);
begin
cislo:='';
while (p[1]<>'$') and (p[1]<>'.') do
  begin
  cislo:=cislo+p[1];
  delete(p,1,1);
  end;
delete(p,1,1);
pomoc:=p;
end;

begin
pomoc:='';
pozn:=pozn+'$';
while (pozn[1]<>#9) do
  begin
  pomoc:=pomoc+pozn[1];
  delete(pozn,1,1);
  end;
delete(pozn,1,1); { Eat tab character }
pomoc:=pomoc+'$';
precticislo(pomoc);
val(cislo,den,chyba);
precticislo(pomoc);
val(cislo,mesic,chyba);
precticislo(pomoc);
delete(cislo,5,255);
val(cislo,rok,chyba);
end;


procedure rozdel2(var pozn,ret:string);
{procedura oddeli z poznamky prvni cast na lusteni}
var i,j:integer;
begin
ret:='';
pozn:=pozn+'$';
j:=0;
if pos('jede',pozn)>0 then
  begin
  if pos('jede',pozn)>=3 then
    begin
    if (pozn[pos('jede',pozn)-2]='n') and (pozn[pos('jede',pozn)-1]='e')
      then begin
          ret:=ret+'nejede ';
          delete(pozn,1,pos('nejede',pozn)+6);
             if pos('jede',pozn)>0 then begin
                 for i:=1 to (pos('jede',pozn)-1) do ret:=ret+pozn[i];
                 delete(pozn,1,pos('jede',pozn)-1);
                 end
          else begin
              for i:=1 to (pos('$',pozn)-1) do ret:=ret+pozn[i];
              delete(pozn,1,pos('$',pozn)-1);
              end
              end
      else begin
            delete(pozn,1,pos('jede',pozn)+4);
            if pos('nejede',pozn)>0 then begin
                for i:=1 to (pos('nejede',pozn)-1) do ret:=ret+pozn[i];
                delete(pozn,1,pos('nejede',pozn)-1);
                end
          else begin
                for i:=1 to (pos('$',pozn)-1) do ret:=ret+pozn[i];
                delete(pozn,1,pos('$',pozn)-1);
               end
      end;
    end
    else
    begin
    ret:=ret+'jede ';
    delete(pozn,1,pos('jede',pozn)+4);
            if pos('nejede',pozn)>0 then begin
                for i:=1 to (pos('nejede',pozn)-1) do ret:=ret+pozn[i];
                delete(pozn,1,pos('nejede',pozn)-1);
                end
          else begin
                for i:=1 to (pos('$',pozn)-1) do ret:=ret+pozn[i];
                delete(pozn,1,pos('$',pozn));
               end
          end;
    end;

end;


function preved(a:string): integer;
{funkce vraci prevedene hodnoty mesicu}
begin
if a='I' then preved:=1;
if a='II' then preved:=2;
if a='III' then preved:=3;
if a='IV' then preved:=4;
if a='V' then preved:=5;
if a='VI' then preved:=6;
if a='VII' then preved:=7;
if a='VIII' then preved:=8;
if a='IX' then preved:=9;
if a='X' then preved:=10;
if a='XI' then preved:=11;
if a='XII' then preved:=12;
end;


procedure rozepispomlcky(var pomoc:string);
{procedura rozepise pomlcky na jednotlive dny, pripadne cisla, ke dnum
dopisuje i cisla mesicu}
var pocatek,konec:integer;
    chyba,i:integer;
    cislo,pomoc2:string;
begin
if pos('-',pomoc)>0 then
  begin
  repeat
    begin
    if pomoc[pos('-',pomoc)-1] in ['1'..'7'] then
      begin
      val(pomoc[pos('-',pomoc)-1],pocatek,chyba);
      val(pomoc[pos('-',pomoc)+1],konec,chyba);
      insert(',',pomoc,pos('-',pomoc));
      for i:=pocatek+1 to konec-1 do
        begin
        cislo:='';
        str(i,cislo);
        cislo:=cislo+',';
        insert(cislo,pomoc,pos('-',pomoc));
        end;
      delete(pomoc,pos('-',pomoc),1);
      end
    else if pomoc[pos('-',pomoc)-1]='.' then
      begin
      i:=pos('-',pomoc)-1;
      cislo:='';
      repeat
        begin
        dec(i);
        end;
      until pomoc[i-1]=',';
      repeat
        begin
        if pomoc[i] in ['0'..'9'] then cislo:=cislo+pomoc[i];
        inc(i);
        end;
      until pomoc[i+1]='-';
      val(cislo,pocatek,chyba);
      cislo:='';
      i:=pos('-',pomoc);
      repeat
        begin
        inc(i);
        if pomoc[i] in ['0'..'9'] then cislo:=cislo+pomoc[i];
        end;
      until pomoc[i+1]='.';
      val(cislo,konec,chyba);
      cislo:=',';
      insert(cislo,pomoc,pos('-',pomoc));
      for i:=pocatek+1 to konec-1 do
        begin
        cislo:='';
        Str(i,cislo);
        cislo:=cislo+'.,';
        insert(cislo,pomoc,pos('-',pomoc));
        end;
      delete(pomoc,pos('-',pomoc),1);
      end
    else
      delete (pomoc,1,1);
    end;
    until pos('-',pomoc)=0;
  end;
pomoc2:='';
i:=0;
pomoc:=pomoc+'$';
if pomoc[pos('$',pomoc)-1]<>',' then insert(',',pomoc,pos('$',pomoc));

if pos('.',pomoc)>0 then
begin
while pomoc[1]<>'$' do
  begin
  if pomoc[pos('.',pomoc)-1] in ['0'..'9'] then
    begin
    for i:=1 to pos('.',pomoc) do pomoc2:=pomoc2+pomoc[i];
    delete(pomoc,1,pos('.',pomoc));
    if pomoc[1]=',' then
      begin
      delete(pomoc,1,1);
      i:=0;
      repeat inc(i);
      until (pomoc[i]='I') or (pomoc[i]='V') or (pomoc[i]='X') or (pomoc[i]='$');
      if pomoc[1]<>'$' then
      repeat
        begin
        pomoc2:=pomoc2+pomoc[i];
        inc(i);
        end;
      until (pomoc[i+1]=',') or (pomoc[i+1]='$') or (pomoc[i+1]='v');
      pomoc2:=pomoc2+'.';
      end
                 else
      begin
      i:=1;
      repeat
        begin
        pomoc2:=pomoc2+pomoc[i];
        inc(i);
        end;
      until (pomoc[i]=',') or (pomoc[i+1]='$') or (pomoc[i+1]='v') or (pomoc[i]='v');
      delete(pomoc,1,i);
      end;
    end;
  while (pomoc[1]=' ') or (pomoc[1]=',') do delete(pomoc,1,1);
  pomoc2:=pomoc2+',';
  if (pos('v',pomoc)>0) and (pos('.',pomoc)=0) then
    begin
    delete(pomoc,pos('v',pomoc2),2);
    insert(pomoc,pomoc2,1);
    pomoc:='$';
    end;
  if (pos('.',pomoc)=0) and (pomoc[1] in ['0'..'9']) then
    begin
    for i:=1 to pos('$',pomoc) do pomoc2:=pomoc2+pomoc[i];
    pomoc:='$';
    end;
  end;
pomoc:=pomoc2;
end;
end;


procedure lustiv(ret:string);
{procedura lusti podreteyec v poznamce, ktery zacina pismenem v, pokud toto v
neni po od nebo do}
var pomoc: string;
    chyba,cislo,i,porden,pormes: integer;
procedure umistidopole;
  var je:boolean;
      j:integer;
  begin
  je:=false;
  for j:=1 to vrchol+1 do
      begin
      if (porden=pomocnepole[j].den) and (pormes=pomocnepole[j].mesic)
             then je:=true;
      end;
  if je=false then
    begin
    pomocnepole[vrchol].den:=porden;
    pomocnepole[vrchol].mesic:=pormes;
    inc(vrchol);
    end;
  end;
begin
rozepispomlcky(ret);
while ret[1] in [' ','v',','] do delete(ret,1,1);
ret:=ret+'$';
while ret[1]<>'$' do
  begin
  if (ret[2]=',') or (ret[2]=' ') then
    begin
    pomoc:='';
    pomoc:=ret[1];
    val(pomoc,cislo,chyba);
    porden:=1;
    pormes:=1;
    if prestupny(rok)=true then
      begin
      for i:=urciprvniden(rok) to urciprvniden(rok)+366 do
        begin
        if porden>kolikdni(pormes) then
          begin
          porden:=1;
          inc(pormes)
          end;
        if cislo=7 then cislo:=0;
        if (i mod 7)=cislo then
          begin
          umistidopole;
          end;
        inc(porden);
        delete(pomoc,1,2);
        end;
      end
                           else
      begin
      for i:=urciprvniden(rok) to urciprvniden(rok)+365 do
        begin
        if porden>kolikdni(pormes) then
          begin
          porden:=1;
          inc(pormes)
          end;
        if cislo=7 then cislo:=0;
        if (i mod 7)=cislo then
          begin
          umistidopole;
          end;
        inc(porden);
        delete(pomoc,1,2);
        end;
      end;
    delete(ret,1,2);
    while ret[1] in [',',' '] do delete(ret,1,1);
    end
  else if (pos('.',ret)=2) or (pos('.',ret)=3) then
    begin
    pomoc:='';
    i:=1;
    while ret[i]<>'.' do
      begin
      pomoc:=pomoc+ret[i];
      inc(i);
      end;
    val(pomoc,cislo,chyba);
    delete(ret,1,i);
    pomocnepole[vrchol].den:=cislo;
    while ret[1] in [',',' '] do delete(ret,1,1);
    pomoc:='';
    i:=1;
    if pos('.',ret)>0 then
      begin
      while (ret[i]<>'.') do
        begin
        pomoc:=pomoc+ret[i];
        inc(i);
        end;
      delete(ret,1,i+1);
      while ret[1] in [',',' '] do delete(ret,1,1);
      pomocnepole[vrchol].mesic:=preved(pomoc);
      inc(vrchol);
      end
                      else ret:='$';
    end
  else delete (ret,1,1);
  {!!! ADDED}
  end;
end;


procedure lustioddo(ret:string);
{procedura lusti cast poznamky od - do, dny uklada do pomocneho pole}
var pomoc: string;
    chyba,i,j,porden,pormes,cislo: integer;
    spodniden,spodnimes,horniden,hornimes:integer;

procedure umistidopole1;
var je:boolean;
    j:integer;
begin
je:=false;
for j:=1 to vrchol+1 do
  begin
  if (spodniden=pomocnepole[j].den) and (spodnimes=pomocnepole[j].mesic)
             then je:=true;
  end;
if je=false then
  begin
  pomocnepole[vrchol].den:=spodniden;
  pomocnepole[vrchol].mesic:=spodnimes;
  inc(vrchol);
  end;
end;


procedure umistidopole2;
var je:boolean;
    j:integer;
begin
je:=false;
for j:=1 to vrchol+1 do
    begin
    if (porden=pomocnepole[j].den) and (pormes=pomocnepole[j].mesic)
          then je:=true;
    end;
if je=false then
  begin
  pomocnepole[vrchol].den:=porden;
  pomocnepole[vrchol].mesic:=pormes;
  inc(vrchol);
  end;
end;

begin
pomoc:='';
i:=1;
repeat
  begin
  inc(i);
  if ret[i] in ['0'..'9'] then pomoc:=pomoc+ret[i]
  end;
until ret[i]='.';
val(pomoc,spodniden,chyba);
delete(ret,1,i);
i:=0;
repeat inc(i)
until ret[i] in ['I','V','X'];
pomoc:='';
repeat
  begin
  pomoc:=pomoc+ret[i];
  inc(i);
  end;
until ret[i]='.';
spodnimes:=preved(pomoc);
delete(ret,1,pos('do',ret)-1);
pomoc:='';
i:=1;
repeat
  begin
  inc(i);
  if ret[i] in ['0'..'9'] then pomoc:=pomoc+ret[i]
  end;
until ret[i]='.';
val(pomoc,horniden,chyba);
delete(ret,1,i);
i:=0;
repeat inc(i)
until ret[i] in ['I','V','X'];
pomoc:='';
repeat
  begin
  pomoc:=pomoc+ret[i];
  inc(i);
  end;
until ret[i]='.';
hornimes:=preved(pomoc);
delete(ret,1,i);
if horniden=kolikdni(hornimes) then
  if hornimes=12 then
    begin
    horniden:=0;
    hornimes:=1;
    end
                 else
    begin
    horniden:=0;
    inc(hornimes);
    end;
ret:=ret+'$';
if pos('.',ret)>0 then
  begin
  pomoc:='';
  pomoc:='v ';
  i:=pos('.',ret);
  repeat dec(i);
  until (ret[i-1]=' ') or (ret[i-1]=',');
  j:=i;
  repeat begin
         pomoc:=pomoc+ret[i];
         inc(i);
         end;
  until ret[i]='$';
  delete(ret,j,i);
  lustiv(pomoc);
  end;
if pos('v',ret)=0 then
  begin
  repeat
    begin
    umistidopole1;
    inc(spodniden);
    if (spodniden=32) and (spodnimes=12) then
      begin
      spodniden:=1;
      spodnimes:=1;
      end;
    if spodniden>kolikdni(spodnimes) then
      begin
      spodniden:=1;
      inc(spodnimes);
      end;
    end;
  until (spodniden=horniden+1) and (spodnimes=hornimes);
  end
                  else
  begin
  if pos('sud',ret)>0 then
    begin
    i:=pos('dny',ret)+2;
    delete(ret,1,i);
    repeat
      begin
      if spodniden mod 2 =0 then umistidopole1;
      inc(spodniden);
      if (spodniden=32) and (spodnimes=12) then
        begin
        spodniden:=1;
        spodnimes:=1;
        end;
      if spodniden>kolikdni(spodnimes) then
        begin
        spodniden:=1;
        inc(spodnimes);
        end;
      end;
    until (spodniden=horniden+1) and (spodnimes=hornimes);
    end;
  if pos('lich',ret)>0 then
    begin
    i:=pos('dny',ret)+2;
    delete(ret,1,i);
    repeat
      begin
      if spodniden mod 2 = 1 then umistidopole1;
      inc(spodniden);
      if (spodniden=32) and (spodnimes=12) then
        begin
        spodniden:=1;
        spodnimes:=1;
        end;
      if spodniden>kolikdni(spodnimes) then
        begin
        spodniden:=1;
        inc(spodnimes);
        end;
      end;
    until (spodniden=horniden+1) and (spodnimes=hornimes);
    end

                       else
  rozepispomlcky(ret);

  repeat delete(ret,1,1)
  until ret[1] in ['1'..'7','$'];
  ret:=ret+',$';
  while ret[1]<>'$' do
  begin
  pomoc:=ret[1];
  delete(ret,1,2);
  val(pomoc,cislo,chyba);
  porden:=0;
  pormes:=1;
  if prestupny(rok)=true then
  for i:=urciprvniden(rok) to urciprvniden(rok)+366 do
    begin
    inc(porden);
    if porden>kolikdni(pormes) then
      begin
      porden:=1;
      inc(pormes)
      end;
    if (porden=32) and (pormes=12) then
      begin
      pormes:=1;
      porden:=1;
      end;
    if cislo=7 then cislo:=0;
    if (i mod 7)=cislo then
      begin
      if spodnimes<=hornimes then
        begin
        if (pormes>spodnimes) and (pormes<hornimes) then
          begin
          umistidopole2;
          end;
        if (pormes=spodnimes) and (pormes=hornimes) then
          begin
          if (porden>=spodniden) and (porden<=horniden)then
            begin
            umistidopole2
            end
          end;
        if (pormes=spodnimes) and (pormes<hornimes) then
          begin
          if (porden>=spodniden) then umistidopole2
          end;
        if (pormes>spodnimes) and (pormes=hornimes) then
          begin
          if (porden<=horniden) then umistidopole2
          end
        end
                             else
        begin
        if (pormes>spodnimes) then
          begin
          umistidopole2;
          end;
        if (pormes=spodnimes) then
        if (porden>=spodniden) then
          begin
          umistidopole2
          end;
        if (pormes=hornimes) then
          begin
          if (porden<=horniden)then
            begin
            umistidopole2
            end
          end;
        if (pormes<hornimes) then
          begin
          umistidopole2
          end
        end
      end
    end
  else
  begin
  for i:=urciprvniden(rok) to urciprvniden(rok)+365 do
    begin
    inc(porden);
    if porden>kolikdni(pormes) then
      begin
      porden:=1;
      inc(pormes)
      end;
    if (porden=32) and (pormes=12) then
      begin
      pormes:=1;
      porden:=1;
      end;
    if cislo=7 then cislo:=0;
    if (i mod 7)=cislo then
      begin
      if spodnimes<=hornimes then
        begin
        if (pormes>spodnimes) and (pormes<hornimes) then
          begin
          umistidopole2;
          end;
        if (pormes=spodnimes) and (pormes=hornimes) then
          begin
          if (porden>=spodniden) and (porden<=horniden)then
            begin
            umistidopole2
            end
          end;
        if (pormes=spodnimes) and (pormes<hornimes) then
          begin
          if (porden>=spodniden) then umistidopole2
          end;
        if (pormes>spodnimes) and (pormes=hornimes) then
          begin
          if (porden<=horniden) then umistidopole2
          end
        end
                             else
        begin
        if (pormes>spodnimes) then
          begin
          umistidopole2;
          end;
        if (pormes=spodnimes) then
        if (porden>=spodniden) then
          begin
          umistidopole2
          end;
        if (pormes=hornimes) then
          begin
          if (porden<=horniden)then
            begin
            umistidopole2
            end
          end;
        if (pormes<hornimes) then
          begin
          umistidopole2
          end
        end
      end
    end;
  end;
  end;
end;
end;


procedure lustiod(ret:string);
{procedura lusti casti poznamky zacinajici 'od' vyuzitim procedury lustioddo
a to pridanim do 31.XII. za od...}
var i,pomden,pommes,chyba:integer;
    cislo:string;
procedure precticisla(p:string);
begin
cislo:='';
i:=0;
repeat inc(i);
until p[i] in ['0'..'9'];
while (p[i]<>'.') and (p[i]<>'$') do
  begin
  cislo:=cislo+p[i];
  inc(i);
  end;
val(cislo,pomden,chyba);
cislo:='';
repeat inc(i);
until p[i] in ['I','V','X'];
while (p[i]<>'.') and (p[i]<>'$') do
  begin
  cislo:=cislo+p[i];
  inc(i);
  end;
pommes:=preved(cislo);
end;

begin
precticisla(ret);
if ((pommes>zacmes)) or ((pommes=zacmes) and (pomden>=zacden)) then
  if rok=zacrok then
    begin
    i:=1;
    repeat inc(i);
    until (ret[i] in ['I','X','V']);
    repeat inc(i);
    until ret[i]='.';
    insert(' do 31.XII. ',ret,i+1);
    lustioddo(ret);
    end
               else
    begin
    i:=1;
    repeat inc(i);
    until (ret[i] in ['I','X','V']);
    repeat inc(i);
    until ret[i]='.';
    delete(ret,1,i);
    insert('od 1.I. do 9.VI. ',ret,1);
    lustioddo(ret);
    end
            else
  if ((pommes<konmes)) or ((pommes=konmes) and (pomden<=konmes)) then
  begin
  i:=1;
  repeat inc(i);
  until (ret[i] in ['I','X','V']);
  repeat inc(i);
  until ret[i]='.';
  insert(' do 9.VI.',ret,i+1);
  lustioddo(ret);
  end;
end;


procedure lustido(ret:string);
{procedura lusti casti poznamky zacinajici 'do' vyuzitim procedury lustioddo
a to pridanim od 1.I. na prvni misto retezce}
var i,pomden,pommes,chyba:integer;
    cislo:string;
procedure precticisla(p:string);
begin
cislo:='';
i:=0;
repeat inc(i);
until p[i] in ['0'..'9'];
while (p[i]<>'.') and (p[i]<>'$') do
  begin
  cislo:=cislo+p[i];
  inc(i);
  end;
val(cislo,pomden,chyba);
cislo:='';
repeat inc(i);
until p[i] in ['I','V','X'];
while (p[i]<>'.') and (p[i]<>'$') do
  begin
  cislo:=cislo+p[i];
  inc(i);
  end;
pommes:=preved(cislo);
end;

begin
precticisla(ret);
if ((pommes>zacmes)) or ((pommes=zacmes) and (pomden>=zacden)) then
  begin
  insert('od 28.V. ',ret,1);
  lustioddo(ret);
  end
            else
  if ((pommes<konmes)) or ((pommes=konmes) and (pomden<=konmes)) then
    if rok=konrok then
      begin
      insert('od 1.I. ',ret,1);
      lustioddo(ret);
      end
                  else
      begin
      i:=1;
      repeat inc(i);
      until (ret[i] in ['I','X','V']);
      repeat inc(i);
      until ret[i]='.';
      delete(ret,1,i);
      insert('od 28.V. do 31.XII. ',ret,1);
      lustioddo(ret);
      end


end;


procedure rozepisretezec(retez:string);
{rozepise retezec na jednotlive dny, ktere uklada do pole, odkud je pak cerpa
procedura na prevedeni, podle druhu retezce}
var prvni,druhy,pomoc,colustit:string;
    a,b,c,i:integer;
procedure urciporadi(ret:string);
{procedura urci poradi klicovych retezcu v podretezci}
begin
prvni:='';
druhy:='';
a:=pos('od',ret);
if a=0 then a:=300;
b:=pos('do',ret);
if b=0 then b:=300;
c:=pos('v',ret);
if c=0 then c:=300;
if a<b then
  if a<c then
    begin
    prvni:='od';
    if b<c then druhy:='do'
           else if c<b then druhy:='v';
    end
         else
    begin
    prvni:='v';
    if a<b then druhy:='od'
           else if b<a then druhy:='do';
    end
       else
  if b<c then
    begin
    prvni:='do';
    if a<c then druhy:='od'
           else if c<a then druhy:='v'
    end
         else
    begin
    prvni:='v';
    if a<b then druhy:='od'
    else if b<a then druhy:='do';
    end;
end;
begin
if retez[1] in ['1'..'9'] then insert('v ',retez,1);
while pos('a',retez)>0 do begin
                       insert(',',retez,pos('a',retez)-1);
                       delete(retez,pos('a',retez)-1,3);
                       end;

while retez<>'$' do begin
urciporadi(retez);
pomoc:='';
if (prvni='v') then
  begin
  colustit:='v';
  if druhy=''then
    begin
    pomoc:=retez;
    retez:='$';
    end
              else
    begin
    for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
    delete(retez,1,pos(druhy,retez)-1);
    end;
  end

else if (prvni='od') then
 if druhy='do' then
   begin
   colustit:='oddo';
   for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
   delete(retez,1,pos(druhy,retez)-1);
   urciporadi(retez);
   if druhy='v' then
     begin
     for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
     delete(retez,1,pos(druhy,retez)-1);
     urciporadi(retez);
     if druhy=''then
       begin
       pomoc:=pomoc+retez;
       retez:='$';
       end
                 else
       begin
       for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
       delete(retez,1,pos(druhy,retez)-1);
       end;
     end
                else
     begin
     for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
     delete(retez,1,pos(druhy,retez)-1);


     {urciporadi(retez);}

     if druhy=''then
       begin
       pomoc:=pomoc+retez;
       retez:='$';
       end
     end;
   end
               else
   begin
   colustit:='od';
   if druhy='v' then
     begin
     for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
     delete(retez,1,pos(druhy,retez)-1);
     urciporadi(retez);
     if druhy=''then
       begin
       pomoc:=pomoc+retez;
       retez:='$';
       end
                 else
       begin
       for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
       delete(retez,1,pos(druhy,retez)-1);
       end;
     end
                else
     begin
     for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
     delete(retez,1,pos(druhy,retez)-1);
     urciporadi(retez);
     if druhy=''then
       begin
       pomoc:=pomoc+retez;
       retez:='$';
       end
                 else
       begin
       for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
       delete(retez,1,pos(druhy,retez)-1);
       end;
     end;
   end
else if prvni='do' then
  begin
  colustit:='do';
  if druhy='v' then
     begin
     for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
     delete(retez,1,pos(druhy,retez)-1);
     urciporadi(retez);
     if druhy=''then
       begin
       pomoc:=pomoc+retez;
       retez:='$';
       end
                else
       begin
       for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
       delete(retez,1,pos(druhy,retez)-1);
       end;
     end
                else
     begin
     if druhy=''then
       begin
       pomoc:=pomoc+retez;
       retez:='$';
       end
                 else
       begin
       for i:=1 to pos(druhy,retez)-1 do pomoc:=pomoc+retez[i];
       delete(retez,1,pos(druhy,retez)-1);
       end;
     end;
   end;
if colustit='v' then lustiv(pomoc);
if colustit='od' then lustiod(pomoc);
if colustit='do' then lustido(pomoc);
if colustit='oddo' then lustioddo(pomoc);
end;
end;


procedure prevedanodopole;
{procedura postupne prepisuje dny z pomocneho pole do pole, kotrola, zda
datum uz v poli neni}
var i,j:integer;
    je: boolean;
begin
for i:=1 to vrchol do
  begin
  je:=false;
  if (pomocnepole[i].den>0) and (pomocnepole[i].mesic>0) then
    begin
    for j:=1 to vrcholdny do
      begin
      if (dny[j].den=pomocnepole[i].den) and (dny[j].mesic=pomocnepole[i].mesic)
             then je:=true;
      end;
    if je=false then
      begin
      inc(vrcholdny);
      dny[vrcholdny].den:=pomocnepole[i].den;
      dny[vrcholdny].mesic:=pomocnepole[i].mesic;
      end;
    end;
  end;
end;


procedure prevednedopole;
{procedura bere prvky z pomocneho pole a pokud nejde stejny prvek v hlavnim
poli, tak jej y z nej odstrani a na jeho misto da posledni prvek}
var i,j,misto:integer;
    je: boolean;
begin
for i:=1 to vrchol do
  begin
  je:=false;
  if (pomocnepole[i].den>0) and (pomocnepole[i].mesic>0) then
    begin
    for j:=1 to vrcholdny do
      begin
      if (dny[j].den=pomocnepole[i].den) and (dny[j].mesic=pomocnepole[i].mesic)
         then
         begin
         je:=true;
         misto:=j;
         end;
      end;
    end;
  if je=true then
    begin
    dny[misto].den:=dny[vrcholdny].den;
    dny[misto].mesic:=dny[vrcholdny].mesic;
    dec(vrcholdny);
    end;
  end;
end;


function urci:boolean;
{procedura urci z datumu a pole, zda vlak jede nebo ne}
var i: integer;
begin
urci:=false;
for i:=1 to vrcholdny do
  begin
  if (dny[i].den=den) and (dny[i].mesic=mesic) then urci:=true;
  end;
end;


function porovnej(ret:string):integer;
{urci, ktery retezec je prvni,1-jede,2-nejede}
begin
if pos('jede',ret)>2 then
  begin
  if (ret[pos('jede',ret)-2]='n') and (ret[pos('jede',ret)-1]='e') then porovnej:=2
                                                                 else porovnej:=1;
  end
                     else porovnej:=1;
end;


procedure ocistipoznamku(var poznamka:string);
{procedura odstrani pripadny text pred vlastni poznamkou}
begin
if pos('jede',poznamka)>4 then
  begin
  if porovnej(poznamka)=1 then
    begin
    delete(poznamka,1,pos('jede',poznamka)-1);
    insert('R',poznamka,1);
    end
                else
    begin
    delete(poznamka,1,pos('nejede',poznamka)-1);
    insert('R',poznamka,1);
    end;
  end;
end;


function testvstupu(ret:string; chyba:integer; poznamka: string):char;
{funkce otestuje format datumu a format poznamky}
begin
testvstupu:='t';
if chyba>0 then testvstupu:='f';
if rok<zacrok then testvstupu:='m';
if (rok=zacrok) and (mesic<zacmes) then testvstupu:='m';
if (rok=zacrok) and (mesic=zacmes) and (den<zacden) then testvstupu:='m';
if rok>konrok then testvstupu:='m';
if (rok=konrok) and (mesic>konmes) then testvstupu:='m';
if (rok=konrok) and (mesic=konmes) and (den>zacden) then testvstupu:='m';
if (den=0) or (mesic=0) or (rok=0) then testvstupu:='f'
else if den>kolikdni(mesic) then testvstupu:='f';
if pos('pouze',ret)>0 then testvstupu:='u';
if pos('jen',ret)>0 then testvstupu:='u';
if pos('po dobu',ret)>0 then testvstupu:='u';
if mesic>12 then testvstupu:='f';
if pos('ode',poznamka)>0 then testvstupu:='u';
if pos('kdyz',poznamka)>0 then testvstupu:='u';
if pos('pokud',poznamka)>0 then testvstupu:='u';
if pos('t.c.',poznamka)>0 then testvstupu:='n';
if (pos('t.',poznamka)>0) and (pos('nejede',poznamka)>0) then testvstupu:='n';
if (pos('jede',poznamka)=0) and (pos('nejede',poznamka)=0) then testvstupu:='f';
if (pos('sud',poznamka)>0) or (pos('lich',poznamka)>0) then testvstupu:='e';
end;

function jede(poznamka: string) :char;
{funkce urci. zda vlak jede a vrati jako prvni pismeno odpovedi
odpovedi: Y - ano, N - ne, U - nedokazu urcit}
var colustit:string;
    i:integer;
    porden,pormes:integer;
    chyba:integer;
    test:char;
begin
chyba:=0;
vrcholdny:=0;
rozdel1(poznamka,chyba);
test:=testvstupu(poznamka,chyba,poznamka);
case test of
't':
  begin
  if porovnej(poznamka)=1 then
    begin
    for i:=1 to 370 do
      begin
      dny[i].den:=0;
      dny[i].mesic:=0;
      end;
    end
                        else
    begin
    porden:=0;
    pormes:=1;
    for i:=1 to 366 do
      begin
      inc(porden);
      if porden>kolikdni(pormes) then
        begin
        porden:=1;
        inc(pormes);
        end;
      inc(vrcholdny);
      dny[vrcholdny].den:=porden;
      dny[vrcholdny].mesic:=pormes;
      end;
    end;
  ocistipoznamku(poznamka);
  while poznamka[1]<>'$' do
    begin
    for i:=1 to 370 do
      begin
      pomocnepole[i].den:=0;
      pomocnepole[i].mesic:=0;
      end;
    vrchol:=1;
    rozdel2(poznamka,colustit);
    while colustit[1] in [',',' '] do delete(colustit,1,1);
    while poznamka[1] in [',',' '] do delete(poznamka,1,1);
    if colustit[1]='j'then
      begin
      delete(colustit,1,5);
      rozepisretezec(colustit);
      prevedanodopole;
      end
                      else
    if colustit[1]='n' then
      begin
      delete(colustit,1,7);
      rozepisretezec(colustit);
      prevednedopole;
      end;
    end;
  if urci=true then jede:='Y'
               else jede:='N';
  end;
'n': jede:='N';
'u': jede:='U';
'm': jede:='E';
else jede:='E';
end;
end;

end.
