unit exists3;

INTERFACE

uses datum;

const nekonecno=12345678;

type Tremid=integer;
     Puzeldatum=^Tuzeldatum;
     Puzelinterval=^Tuzelinterval;
     Ppolepoznamek=^Tpolepoznamek;
     Ppoznamky=^Tpoznamky;
     Tpoletyden=array [1..7] of boolean;
     Tuzeldatum=record
       d:Tkratkedatum;
       dalsi:Puzeldatum;
     end;
     Tuzelinterval=record
       dolnimez,hornimez:Tkratkedatum;
       tyden:Tpoletyden;
       sudylichytyden,sudylichyden:shortint;
       dalsi:Puzelinterval;
     end;
     Tpoznamka=record
       tyden:Tpoletyden;
       pdatum:array [boolean] of Puzeldatum;
       pinterval:array [boolean] of Puzelinterval;
     end;
     Tpolepoznamek=array [1..nekonecno] of Tpoznamka;
     Tpoznamky=object
       private
         celkem,nacteno:Tremid;
         pp:Ppolepoznamek;
       public
         procedure init(pocet:Tremid);
         procedure pridej(s:string);
         procedure vypis(index:Tremid);
         function test(d,vychozi:Tdatum;index:Tremid):boolean;
         procedure done;
     end;

function nactipoznamku(s:string;var vystup:Tpoznamka):boolean; {vraci false, pokud se nepodari poznamku dekodovat}
procedure zruspoznamku(var poznamka:Tpoznamka); {uvolni pamet, ktera byla pri nacteni poznamky alokovana a nastavi poznamku tak,
                                             aby pri pripadnem testovani byla vzdy splnena}
procedure vypispoznamku(poznamka:Tpoznamka); {k odladeni: vypise obsah promenne}
function testujpoznamku(d,vychozi:Tdatum;poznamka:Tpoznamka):boolean; {vychozi musi obsahovat datum, kde je spravna polozka
                                                                       vychozi.w (den v tydnu:1-7), rozdil mezi daty d a vychozi
                                                                       by mel byt co nejmensi, unit datum obsahuje konstantu stddatum}
function jede(poznamka:string):char; {navrat. hodnota: 'Y'-jede,'N'-nejede,'U'-nelze urcit}




IMPLEMENTATION

type Tznaky=set of char;

var ctidatum_mesic:integer; {staticka promenna pouze pro ucely funkce nactipoznamku.ctidatum
                             funkce dostane poznamku (retezec) a do promenne ctidatum_mesic
                             ulozi mesic(1-12), ktery je prvni rimske cislo zleva (napr.:
                             "24.31.XII." ulozi do ctidatum_mesic 12), pri dalsim volani uz se
                             mesic v retezci nehleda a cte se z teto promenne dokud se v retezci
                             nevycerpaji vsechna cisla oznacujici dny v mesici (v tomto pripad 24,31)}

function zacatek(var s:string;szac:string):boolean;
begin
  zacatek:=false;
  if copy(s,1,length(szac))=szac then begin
    delete(s,1,length(szac));
    zacatek:=true;
  end;
end;

procedure smaz(var s:string;zn:Tznaky);
begin
  while s[1] in zn do
    delete(s,1,1);
end;

function nactipoznamku(s:string;var vystup:Tpoznamka):boolean;
  const zarazka='***';

  function denvtydnu(var s:string;var pole:Tpoletyden;b:boolean):boolean;
    var i,j,k,chyba:integer;
  begin
    denvtydnu:=false;
    while s[1]<>zarazka[1] do begin
      val(s[1],i,chyba);
      if (i<1)or(i>7)or(chyba<>0) then
        exit;
      delete(s,1,1);
      pole[i]:=b;
      if s[1]='-' then begin
        val(s[2],j,chyba);
        if (j<1)or(j>7)or(chyba<>0) then
          exit;
        delete(s,1,2);
        for k:=i+1 to j do
          pole[k]:=b;
      end;
      zacatek(s,',');
      if (not(s[1] in ['1'..'7']))or(s[2] in ['.','0'..'9']) then
        break;
    end;
    denvtydnu:=true;
  end;

  function ctidatum(var s:string;var vystup:Tkratkedatum):boolean;
    var i,pocet,chyba:integer;
        mesicstr:Trimskecislo;
        pomstr:string[10];
  begin
    ctidatum:=false;
    if ctidatum_mesic=0 then begin
      i:=1;
      while (not(s[i] in rimskecislice))and(i<length(s)-1) do
        inc(i);
      pocet:=1;
      while s[i+pocet] in rimskecislice do
        inc(pocet);
      mesicstr:=copy(s,i,pocet);
      if s[i+pocet]<>'.' then
        exit;
      for i:=1 to 12 do
        if rimske[i]=mesicstr then
          ctidatum_mesic:=i;
      if ctidatum_mesic=0 then
        exit;
    end;
    pocet:=1;
    while (s[pocet+1]<>'.')and(pocet+1<length(s)) do
      inc(pocet);
    pomstr:=copy(s,1,pocet);
    delete(s,1,pocet);
    val(pomstr,vystup.d,chyba);
    if chyba<>0 then
      exit;
    vystup.m:=ctidatum_mesic;
    if not platnekratkedatum(vystup) then
      exit;
    smaz(s,['.',',',' ']);
    if s[1] in rimskecislice then begin
      ctidatum_mesic:=0;
      while s[1] in rimskecislice do
        delete(s,1,1);
      if not zacatek(s,'.') then
        exit;
      smaz(s,[',',' ']);
    end;
    ctidatum:=true;
  end;

  var b:boolean;
      i:integer;
      pd:Puzeldatum;
      pi:Puzelinterval;
      zmena:boolean;
begin
  ctidatum_mesic:=0;
  nactipoznamku:=false;
  with vystup do begin
    for b:=false to true do begin
      pdatum[b]:=nil;
      pinterval[b]:=nil;
    end;
    for i:=1 to 7 do
      tyden[i]:=true;
    zmena:=false;
    b:=true;
    s:=s+zarazka;
    while s[1]<>zarazka[1] do begin
      if (zacatek(s,' '))or(zacatek(s,','))or(zacatek(s,'a '))or(zacatek(s,'v ')) then
        continue;
      if zacatek(s,'jede ') then begin
        b:=true;
        continue;
      end;
      if zacatek(s,'nejede ') then begin
        b:=false;
        continue;
      end;
      if (s[1] in ['1'..'7'])and(not(s[2] in ['.','0'..'9'])) then begin
        if zmena=false then
          for i:=1 to 7 do
            tyden[i]:=not(b);
        zmena:=true;
        if not denvtydnu(s,tyden,b) then
          exit;
        continue;
      end;
      if s[1] in rimskecislice then begin
        i:=prectimesic(s,true);
        if i=0 then
          exit;
        new(pi);
        pi^.dolnimez.d:=1;
        pi^.dolnimez.m:=i;
        pi^.hornimez.d:=pocdnu(i,0);
        pi^.hornimez.m:=i;
        pi^.sudylichytyden:=0;
        pi^.sudylichyden:=0;
        for i:=1 to 7 do
          pi^.tyden[i]:=true;
        pi^.dalsi:=pinterval[b];
        pinterval[b]:=pi;
        continue;
      end;
      if (copy(s,1,length('od '))='od ')or(copy(s,1,length('do '))='do ') then begin
        new(pi);
        if zacatek(s,'od ') then begin
          if not ctidatum(s,pi^.dolnimez) then begin
            dispose(pi);
            exit;
          end;
        end else
          pi^.dolnimez:=novyrok;
        if zacatek(s,'do ') then begin
          if not ctidatum(s,pi^.hornimez) then begin
            dispose(pi);
            exit;
          end;
        end else
          pi^.hornimez:=silvestr;
        pi^.sudylichytyden:=0;
        pi^.sudylichyden:=0;
        if zacatek(s,'v sud� t�dny') or zacatek(s,'v sud�ch t�dnech') then
          pi^.sudylichytyden:=2;
        if zacatek(s,'v lich� t�dny') or zacatek(s,'v lich�ch t�dnech') then
          pi^.sudylichytyden:=1;
        if zacatek(s,'v sud� dny') or zacatek(s,'v sud�  dny') then
          pi^.sudylichyden:=2;
        if zacatek(s,'v lich� dny') or zacatek(s,'v lich�  dny') then
          pi^.sudylichyden:=1;
        smaz(s,[' ',',']);
        for i:=1 to 7 do
          pi^.tyden[i]:=true;
        if zacatek(s,'v ') then
          if s[1] in ['1'..'7'] then begin
            for i:=1 to 7 do
              pi^.tyden[i]:=false;
            if not denvtydnu(s,pi^.tyden,true) then begin
              dispose(pi);
              exit;
            end;
          end;
        pi^.dalsi:=pinterval[b];
        pinterval[b]:=pi;
        if porovnejdatum(pinterval[b]^.dolnimez,pinterval[b]^.hornimez)=2 then begin
          new(pi);
          pi^:=pinterval[b]^;
          pinterval[b]^.hornimez:=silvestr;
          pi^.dolnimez:=novyrok;
          pi^.dalsi:=pinterval[b];
          pinterval[b]:=pi;
        end;
        continue;
      end;
      new(pd);
      if not ctidatum(s,pd^.d) then begin
        dispose(pd);
        exit;
      end;
      pd^.dalsi:=pdatum[b];
      pdatum[b]:=pd;
    end; {end while}
  end; {end with}
  nactipoznamku:=true;
end; {end procedure}

procedure zruspoznamku(var poznamka:Tpoznamka);
  var pd:Puzeldatum;
      pi:Puzelinterval;
      b:boolean;
      i:integer;
begin
  with poznamka do begin
    for b:=false to true do begin
      while pdatum[b]<>nil do begin
        pd:=pdatum[b];
        pdatum[b]:=pdatum[b]^.dalsi;
        dispose(pd);
      end;
      while pinterval[b]<>nil do begin
        pi:=pinterval[b];
        pinterval[b]:=pinterval[b]^.dalsi;
        dispose(pi);
      end; {end while}
    end; {end for}
    for i:=1 to 7 do
      tyden[i]:=true;
  end; {end with}
end; {end procedure}

procedure vypispoznamku(poznamka:Tpoznamka);
  var i:integer;
      pd:Puzeldatum;
      pi:Puzelinterval;
      b:boolean;
begin
  with poznamka do begin
    for i:=1 to 7 do
      if tyden[i] then
        write(i)
      else
        write('-');
    writeln;
    for b:=true downto false do begin
      if b then
        writeln('jede:')
      else
        writeln('nejede:');
      pd:=pdatum[b];
      while pd<>nil do begin
        write(pd^.d.d,'.',pd^.d.m,'. ');
        pd:=pd^.dalsi;
      end;
      writeln;
      pi:=pinterval[b];
      while pi<>nil do begin
        write(pi^.dolnimez.d,'.',pi^.dolnimez.m,'.-',pi^.hornimez.d,'.',pi^.hornimez.m,'. ');
        for i:=1 to 7 do
          if pi^.tyden[i] then
            write(i)
          else
            write('-');
        write(' tydny: ');
        case pi^.sudylichytyden of
          0:write('oba');
          1:write('liche');
          2:write('sude');
        end;
        write(' dny: ');
        case pi^.sudylichyden of
          0:write('oba');
          1:write('liche');
          2:write('sude');
        end;
        writeln;
        pi:=pi^.dalsi;
      end;
    end;
  end;
end;

function testujpoznamku(d,vychozi:Tdatum;poznamka:Tpoznamka):boolean;
  var t,den:integer;
      pd:Puzeldatum;
      pi:Puzelinterval;
      b:boolean;
      kratke:Tkratkedatum;
begin
  kratke.d:=d.d;
  kratke.m:=d.m;
  with poznamka do begin
    {nejvyssi prioritu ma konkretni datum}
    for b:=false to true do begin
      pd:=pdatum[b];
      while pd<>nil do begin
        if porovnejdatum(pd^.d,kratke)=0 then begin
          testujpoznamku:=b;
          exit;
        end;
        pd:=pd^.dalsi;
      end;
    end;
    {zkontroluje se den tydne}
    denvtydnu(d,vychozi);
    if tyden[d.w]=false then begin
      testujpoznamku:=false;
      exit;
    end;
    testujpoznamku:=true;
    for b:=false to true do
      if (pdatum[b]=nil)and(pdatum[not b]<>nil) then
        testujpoznamku:=b;
    if (pinterval[false]=nil)and(pinterval[true]=nil) then
      exit;
    t:=cislotydne(d) mod 2;
    if t=0 then
      t:=2;
    den:=kratke.d mod 2;
    if den=0 then
      den:=2;
    {pokud existuji intervaly napr. jen pozitivni, bude vysledek testu negativni, pokud v zadnem intervalu nedojde ke shode}
    for b:=false to true do
      if pinterval[b]=nil then
        testujpoznamku:=b;
    {kontrola intervalu}
    for b:=false to true do begin
      pi:=pinterval[b];
      while pi<>nil do begin
        if (pi^.tyden[d.w])and(porovnejdatum(pi^.dolnimez,kratke)<>2)and(porovnejdatum(pi^.hornimez,kratke)<>1)
        and((pi^.sudylichytyden=0)or(pi^.sudylichytyden=t))and((pi^.sudylichyden=0)or(pi^.sudylichyden=den)) then begin
          testujpoznamku:=b;
          exit;
        end;
        pi:=pi^.dalsi;
      end; {end while}
    end; {end for}
  end; {end with}
end; {end function}

function jede(poznamka:string):char;
  var pozn:Tpoznamka;
      d:Tdatum;
begin
  datumstr_datum(prvni(#9,poznamka),d);
  if not nactipoznamku(poznamka,pozn) then
    jede:='U'
  else
    if testujpoznamku(d,stddatum,pozn) then
      jede:='Y'
    else
      jede:='N';
  zruspoznamku(pozn);
end;

{-----------------------------Tpoznamky------------------------------}
procedure Tpoznamky.init(pocet:Tremid);
begin
  celkem:=pocet;
  nacteno:=0;
  pp:=nil;
  if celkem>0 then
    getmem(pp,sizeof(Tpoznamka)*celkem);
end;

procedure Tpoznamky.pridej(s:string);
begin
  if nacteno<celkem then begin
    inc(nacteno);
    if not nactipoznamku(s,pp^[nacteno]) then begin
      zruspoznamku(pp^[nacteno]);
      writeln('pozn. #',nacteno,' se nepodarilo dekodovat');
      writeln(s);
    end;
  end;
end;

procedure Tpoznamky.vypis(index:Tremid);
begin
  if (index<1)or(index>nacteno) then begin
    writeln('pozn. #',index,' neni nactena');
    exit;
  end;
  writeln('pozn. #',index);
  vypispoznamku(pp^[index]);
end;

function Tpoznamky.test(d,vychozi:Tdatum;index:Tremid):boolean;
begin
  test:=testujpoznamku(d,vychozi,pp^[index]);
end;

procedure Tpoznamky.done;
  var i:Tremid;
begin
  if pp<>nil then begin
    for i:=1 to nacteno do
      zruspoznamku(pp^[i]);
    freemem(pp,sizeof(Tpoznamka)*celkem);
    pp:=nil;
  end;
  nacteno:=0;
end;





END.
