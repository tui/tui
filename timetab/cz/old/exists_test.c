#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include "exists.c"

struct tm date;

/*
 * To run the test, lauch something like
cat ../exp/vlak.tt | grep '^R' | sed 's/^R/1.1.2001<tab>/' | ./exists_test
(Append | grep -B 1 E to get all errors)
 */

void
main(void)
{
	char buf[10240];
	while (gets(buf)) {
		char *s;
		sscanf(buf,"%d.%d.%d",&date.tm_mday,&date.tm_mon,&date.tm_year);
		s = strchr(buf,'\t')+1;
		printf("%d.%d.%d\t%s\n",date.tm_mday, date.tm_mon, date.tm_year, s);
		date.tm_sec=0;date.tm_min=0;date.tm_hour=0;
		date.tm_mon--;date.tm_year-=1900;
		mktime(&date);
		printf("%c\n", exists(s, &date));
	}
}
