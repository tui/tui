{
Test of exists unit

Copyright 2001 Pavel Machek <pavel@ucw.cz>

GPLv2
}

program exists3_test;

uses exists2;

var poznamka:string;
begin
   while not eof do
   begin
      readln(poznamka);
      writeln(poznamka);
      writeln(jede(poznamka));
   end;
end.