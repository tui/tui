program cena_vlaky;

{ ****************************************** }
{ **                                      ** }
{ **        Copyright 2001 A.Kutka        ** }
{ **      alexander.kutka@matfyz.cz       ** }
{ **                                      ** }
{ **       Distribute under GPL v.2       ** }
{ **                                      ** }
{ ****************************************** }

{ Warning: PRICES ARE OUTDATED! }

const {aktualne ceny}

{obycajne cestovne 1. trieda}
cenaO1:array[1..33] of integer =
(15,24,33,42,51,60,69,78,96,114,132,147,165,180,195,210,243,276,306,336,375,
411,441,477,555,636,717,798,879,960,1047,1128,1260);

{obycajne cestovne 2. trieda}
cenaO2:array[1..33] of integer =
(10,16,22,28,34,40,46,52,64,76,88,98,110,120,130,140,162,184,204,224,250,274,
294,318,370,424,478,532,586,640,698,752,840);

{detske/dochodcovske cestovne 1. trieda}
cenaD1:array[1..33] of integer =
(7,12,16,21,25,30,34,39,48,57,66,73,82,90,97,105,121,138,153,168,187,205,220,
238,277,318,358,399,439,480,523,564,630);

{detske/dochodcovske cestovne 2. trieda}
cenaD2:array[1..33] of integer =
(5,8,11,16,17,20,23,26,32,38,44,49,55,60,65,70,81,92,102,112,125,137,147,159,
185,212,239,266,293,320,349,376,420);

{Studentske JuniorPas cestovne (len 2.trieda) }
cenaJ:array[1..33] of integer =
(4,6,9,11,14,17,19,21,25,29,34,38,42,46,50,55,63,71,80,87,97,106,115,123,144,
165,186,207,228,249,270,291,324);

{ZTP, ZTP/P cestovne (len 2.trieda) }
cenaZ:array[1..33] of integer =
(2,4,5,7,8,10,11,13,16,19,22,24,27,30,32,35,40,46,51,56,62,68,73,79,92,106,
119,133,146,160,174,188,210);

{priplatok za psa}
cenaPes:array[1..33] of integer =
(3,5,7,9,11,13,15,17,20,23,26,29,32,35,38,41,17,54,60,66,74,81,87,94,109,
125,141,157,173,189,206,222,248);

var f:text;
    s,meno:string;
    kmc,kms,kmOut, riadok:longint;
    i,ts:byte;
    Vspoj,ic,ec,medz:boolean;
    {filename,}p1,p2,p3,p4,p5,p6,p7:string; {pomocne s, ..., parametre}
    zc,pc,cc,int,j:integer; {zakladna cena, priplatk.cena, celkova cena, interval}
    k,p1k,p2k,p3k,p4k,p5k,p6k,p7k:boolean; {spravnost par.1,2,3,4 ; vyskyt EC, IC}
    c4:array[0..5] of string; {pole slov v parametri 4}
    prip:boolean; {pomocna pre zav.priplatky}
    pes,koc,kocd,kolo,zav:boolean; {priplatky za psa, kocarky,...}
    Ntown,Ncon,slovo,start,ciel,datum:string;
    typ,medzera:word;
    code,nep:integer;
    errory:boolean;
    pp:array [1..4] of string;

Procedure chyba(i,j:byte);
begin
errory:=true;
case i of
0: begin
    writeln('Vstupny subor nenajdeny!');
    halt;
   end;
1: begin
    writeln('Chyba vo vstupnom subore, riadok ',j,' - udaje neoddelovane tabulatorom');
    halt;
   end;
2: begin
    writeln('Chyba vo vstupnom subore, riadok ',j,' - udaj na mieste kilometrov neije cislo');
    halt;
   end;
3:  writeln('Chyba vo vstupnom subore, riadok ',j,' - udaj za !towns nieje cislo');
4:  writeln('Chyba vo vstupnom subore, riadok ',j,' - udaj za !conns nieje cislo');
5:  writeln('Chyba vo vstupnom subore, riadok ',j,' ');
6:  writeln('Chyba vo vstupnom subore, riadok ',j,' - chyba udaj za !from');
7:  writeln('Chyba vo vstupnom subore, riadok ',j,' - chyba udaj za !to');
8:  writeln('Chyba vo vstupnom subore, riadok ',j,' - chyba udaj za !date');
9:  writeln('Chyba vo vstupnom subore, riadok ',j,' - chyba udaj za !towns');
10: writeln('Chyba vo vstupnom subore, riadok ',j,' - chyba udaj za !conns');
end;
end;

procedure getpar(var p1,p2,p3,p4:string);
var p:array [1..4] of string; {pre parametre}
   pi:array [1..4] of 0..4; {indexy paramtrov - who is who}
    j,i,Bi:byte;
   p1p,p2p,p3p,p4p:boolean; {pouzitie parametra}
    c:string;

procedure parachyba(i,ch:byte);
begin
case i of
0:writeln('Neznamy parameter - ',p[ch]);
1:writeln('Chyba v parametri - opakovane zadany druh cestovneho');
2:writeln('Chyba v parametri - opakovane zadana cestovna trieda');
4:writeln('Chyba v parametri - opakovane zadavane priplatky');
end;
halt;
end;

begin
p1p:=false;
p2p:=false;
p3p:=false;
p4p:=false;
for i:=1 to 4 do p[i]:='';
for i:=1 to 4 do pi[i]:=0;


if paramcount>4 then begin writeln('Privela parametrov!');halt;end;

for i:=1 to paramcount do p[i]:=paramstr(i);


 for i:=1 to paramcount do
  begin

   c:=copy(p[i],1,1);

   if (c='z')or(c='Z')or(c='j')or(c='J')or(c='o')or(c='O')or(c='d')or(c='D')
    then begin
          if p1p then parachyba(1,0);
          pi[i]:=1;
          p1p:=true;
         end;

   if (c='1')or(c='2')then
         begin
          if p2p then parachyba(2,0);
          pi[i]:=2;
          p2p:=true;
         end;

   if c='?' then
         begin
         pi[i]:=3;
         end;

   if length(p[i])>1 then
    begin
     if p4p then parachyba(4,0);
     pi[i]:=4;
     p4p:=true;
    end;
  end;

for i:=paramcount downto 1 do
for j:=1 to i do
begin

 if pi[j]=0 then
  begin
   parachyba(0,j);
  end;
 if pi[j]=1 then
  begin
   pp[1]:=p[j];
   continue;
  end;
 if pi[j]=2 then
  begin
   pp[2]:=p[j];
   continue;
  end;
 if pi[j]=3 then
  begin
   pp[3]:=p[j];
   continue;
  end;
 if pi[j]=4 then
  begin
   pp[4]:=p[j];
   continue;
  end;

end;


end;

Procedure zisti(l:string;var km:longint); {zisti km v riadku l}
var tab:byte;
    medzitab:string;
    j:byte;
    kmstr:string;
    novykm:longint;
    code:integer;
begin

if copy(l,1,2)='##'then exit;

if l[1]='#'then begin vspoj:=false; inc(kmc,kms);exit; end;

for j:=1 to 3 do {az ku kilometrom}
 begin
  tab:=pos(#9,l);
  if tab=0 then chyba(1,riadok);
  medzitab:=copy(l,1,tab);
  {mohli by sme event. zistit,ci medzitab je cas}
  Delete(l,1,tab);
 end;
tab:=pos(#9,l);
if tab=0 then chyba(1,riadok);
kmstr:=copy(l,1,tab-1);

if kmstr[1]='+' then begin
                      val(kmstr,novykm,code);
                      if code<>0 then chyba(2,riadok);
                      inc(km,novykm);
                     end else

                     begin
                      val(kmstr,novykm,code);
                      if code<>0 then chyba(2,riadok);
                      km:=novykm;
                     end;
end;

Procedure global;
begin
  delete(s,1,1);
  medzera:=pos(#32,s);
  if medzera=0 then medzera:=length(s)+1;
  slovo:=copy(s,+1,medzera-1);


  typ:=8;
  if slovo = 'towns'         then typ:=1;
  if slovo = 'conns'         then typ:=2;
  if slovo = 'connection'    then typ:=3;
  if slovo = 'from'          then typ:=4;
  if slovo = 'to'            then typ:=5;
  if slovo = 'date'          then typ:=6;
  if slovo = 'endconnection' then typ:=7;
  slovo:='';
  medzera:=pos(#32,s);
  if medzera=0 then medzera:=length(s)+1;
  slovo:=copy(s,medzera+1,length(s));

  if typ in [1..7] then
   case typ of
   1: begin
       val(slovo,nep,code);
       if code<>0 then Chyba(3,riadok)
        else Ntown:=slovo;
      end;
   2: begin
       val(slovo,nep,code);
       if code<>0 then Chyba(4,riadok)
        else Ncon:=slovo;
      end;
   4: start:=slovo;
   5: ciel:=slovo;
   6: datum:=slovo;
   end else chyba(5,riadok);

  if (typ=4)and(start = '') then begin start:='(neuvedene)'; chyba(6,riadok); end;
  if (typ=5)and(ciel  = '') then begin  ciel:='(neuvedene)'; chyba(7,riadok); end;
  if (typ=6)and(datum = '') then begin datum:='(neuvedene)'; chyba(8,riadok); end;
  if (typ=1)and(Ntown = '') then begin Ntown:='(neuvedene)'; chyba(9,riadok); end;
  if (typ=2)and( Ncon = '') then begin  Ncon:='(neuvedene)'; chyba(10,riadok); end;

end;                      


Procedure getdata;
begin
{$I-}
assign(f,'');
Reset(F);
Close(F);
{$I+}
if not((IOResult = 0){ and (FileName <> '')}) then chyba(0,j);
reset(f);
Vspoj:=false;
kmc:=0;
ic:=false;
ec:=false;
riadok := 0;
repeat
readln(f,s);
inc(riadok);
if (Length(s)> 0) and (s[1] = ';') then continue;
if (Length(s)> 0) and (s[1] = '!') then global;

medz:=true;
for i:=1 to length(s) do if s[i] <> ' '  then medz:=false;
if s='' then medz := true;

if (not Vspoj) and (Length(s) > 0) and (UpCase(s[1]) in ['A'..'Z']) then continue;
if      Vspoj  and (Length(s) > 0) and ((UpCase(s[1]) in ['A'..'Z']) or medz) then
                                                begin
                                                 Vspoj:=false;
                                                 inc(kmc, kms);
                                                 continue;
                                                end;
if (not Vspoj) and medz then continue; {whitespace riaok, to ignore}

if (s[1] = '#') and (not Vspoj) then
 begin
  Vspoj:=true;
  kms:=0;
  kmOut := 0;
  meno:=copy(s,3,2);
  ts:=5;
  if meno='Os'   then ts:=1;
  if meno[1]='R' then ts:=2;
  if meno='IC'   then begin ts:=3; ic:=true; end;
  if meno='EC'   then begin ts:=4; ec:=true; end;
  continue {pokracuj v dalsom riadku}
 end;

if Vspoj and (ts in [1..4]) then zisti(s,kms);{relevantny spoj}

if Vspoj and (ts = 5) then zisti(s, kmOut);

until eof(f);
close(f);
end;

Procedure najdiint(j:longint;var i:integer);
const inter:array[1..33] of integer =
(0,6,10,15,20,25,31,35,40,50,60,70,80,90,100,110,120,140,160,180,200,
225,250,275,300,350,400,450,500,550,600,650,700);
var k:integer;
begin {hladanie pasma v zavislosti od km}
for k:=33 downto 1 do if j>=inter[k] then
 begin
  i:=k;
  break;
 end;

end;


procedure about4p(s:string; var c4:array of string);
var sl,i,d,j:integer; {d = dlzka zavazadla}
    c:char;
    p:boolean; {pomocna pre dlhy vyraz}
begin
if s='' then for i:=1 to 5 do c4[i]:='' else
 begin
  sl:=length(s); {max 21}
  if s[sl]<>';' then begin
   writeln('Chyba v 4. parametri - aj posledny poziadavok v parametri 4 musi byt ukonceney znakom '';''');
                      halt;
                     end;

  for i:=1 to 5 do c4[i]:=''; {"vynulovanie" pola}

  if sl<=22 then
   begin
    d:=0;
    j:=1;
    for i:=1 to sl do
     begin;
      c:=s[i];
      if c<>';'then begin
                     c4[j]:=c4[j]+c;
                     inc(d);

                     if d>4 then begin
                                  writeln('Chyba v parametri - chybny ',j,'. typ zavazadla (prilis dlhy).');
                                  halt;
                                 end;
                    end;

      if c=';' then
       begin
        inc(j);
        d:=0;
        p:=(c4[j-1]<>'kolo')and(c4[j-1]<>'pes')and(c4[j-1]<>'zav')and(c4[j-1]<>'kocd')and(c4[j-1]<>'koc');
        if p then begin writeln ('Chyba v parametri - neznamy ',j-1,'. typ zavazadla'); halt; end;
       end;
     end;

   end else begin writeln('Chyba v priplatkovom parametri - prilis dlhy');halt;end;
 end;
end;{of procedure}

procedure cenazav(var pc:integer);
begin
pc:=0;
if (cena_vlaky.zav)
 then pc:=pc+40;
if (cena_vlaky.koc)  and (kmc<51)
then pc:=pc+20;
if (cena_vlaky.kocd) and (kmc<51)
 then pc:=pc+6;
if (cena_vlaky.koc)  and (kmc>50)
 then pc:=pc+40;
if (cena_vlaky.kocd) and (kmc>50)
  then pc:=pc+40;
if (cena_vlaky.kolo) and (kmc<51)
 then pc:=pc+40;
if (cena_vlaky.kolo) and (kmc>50)and(kmc<101)
 then pc:=pc+50;
if (cena_vlaky.kolo) and (kmc>100)
then pc:=pc+60;
end;


(*************** BEGIN OF PROGRAM *******************)

begin
cc:=0; {celkova cena:= 0}
errory:=false;
start := '';
ciel  := '';
datum := '';
Ntown := '';
Ncon  := '';

{*****  Tento program je beta-verzia.
 *****  Vypise cenu cestovneho k zadanemu spoju.
 *****  Ma 4 parametre:

  1. druh cestovneho(O,D,J,Z);
  2. trieda (1,2);
  3. detaily(?) - nepovinny
  4. batozina (kolo,pes,koc,
     kocd,zav kazdy ukonceny znakom ";" ) - nepovinny}


getpar(p1,p2,p3,p4);

p1:=pp[1];
p2:=pp[2];
p3:=pp[3];
p4:=pp[4];

if p1='' then p1:='o';
if p2='' then p2:='2';

{spracovavanie parametrov}

about4p(p4,c4);

for j:=1 to 5 do
begin

 if c4[j]='kolo'then kolo:=true;
 if c4[j]='pes' then  pes:=true;
 if c4[j]='koc' then  koc:=true;
 if c4[j]='kocd'then kocd:=true;
 if c4[j]='zav' then  zav:=true;

end;
prip:=kolo or pes or koc or kocd or zav;
{koniec spracovania}

GETDATA;

 najdiint(kmc,int); {km --> int}



  if (p1='j') or (p1='J')then zc:=cenaJ[int];

  if (p1='o') or (p1='O') then
   if p2='1' then zc:=cenaO1[int];
   if p2='2' then zc:=cenaO2[int];

  if (p1='d') or (p1='D') then
   if p2='1' then zc:=cenaD1[int];
   if p2='2' then zc:=cenaD2[int];

  if (p1='z') or (p1='Z') then zc:=cenaZ[int];


  cenazav(pc);
  if pes then pc:=pc+cenaPes[int];

  cc:=zc+pc;

  if ic then cc:=cc+30; {+ priplatok za IC}
  if ec then cc:=cc+60; {+ priplatok za EC}

    {***************** Zaver, oznamenie vysledku *******************}

  if (start = '') then start:='(neuvedene)';
  if (ciel  = '') then ciel:= '(neuvedene)';
  if (datum = '') then datum:='(neuvedene)';
  if (Ntown = '') then Ntown:='(neuvedene)';
  if ( Ncon = '') then Ncon:= '(neuvedene)';


 if p3='?'then {vypis detailov}

  begin

  if errory then begin
  writeln('Koniec chybovych hlaseni.');
  writeln('');
  end;
  writeln('Spoj ide');
  writeln('zo stanice: ',start);
  writeln('do stanice: ',ciel );
  writeln('datum ',datum);
  writeln('Cesta obsahuje ',Ntown,' miest a ',Ncon,' spojeni.');


   writeln('Cesta je dlha ',kmc,' km co spada do pasma cislo ',int,'.');
   writeln('Cena zakladneho cestovneho je ', zc,' Kc.');

    if ec then writeln('Priplatok za EC cini 60 Kc');
    if ic then writeln('Priplatok za IC cini 30 Kc');
    if prip then writeln(pc,' Kc priplatkov za: ');
     if kolo then writeln('- kolo ');
     if  pes then writeln('- psa ');
     if  koc then writeln('- kocarek ');
     if kocd then writeln('- kocarek s ditetem ');
     if  zav then writeln('- osobni zavazadlo ');

   writeln('Celkova cena cestovneho je ', cc,' Kc.');

   if ((p1='j')or(p1='J')or(p1='z')or(p1='Z'))and(p2k)
    then writeln('Poznamka: Pre drzitelov JuniorPas-u a ZTP resp. ZTP/p je prodajna len II.trida');

  end else writeln(cc);  {vypis iba cenu (jedno cislo)}


end.