/*
 * Goes: given remark in czech and date, decide, whether given connection
 * does exist or not
 */

static int sure = 0;

static unsigned char toascii_hi[] =
                           {'|', 'A', '~', 'L', '$', 'L', 'S', '$', '\"', 'S',  /*160-9*/
	                    'S', 'T', 'Z', '-', 'Z', 'Z', '~', 'a', ',', 'l',   /*170-9*/
	                    '\'', 'l', 's', '~', ',', 's', 's', 't', 'z', ' ',  /*180-9*/
	                    'z', 'z', 'R', 'A', 'A', 'A', 'A', 'L', 'C', 'C',   /*190-9*/
	                    'C', 'E', 'E', 'E', 'E', 'I', 'I', 'D', 'D', 'N',   /*200-9*/
	                    'N', 'O', 'O', 'O', 'O', 'x', 'R', 'U', 'U', 'U',   /*210-9*/
	                    'U', 'Y', 'T', 's', 'r', 'a', 'a', 'a', 'a', 'l',   /*220-9*/
	                    'c', 'c', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'd',   /*230-9*/
	                    'd', 'n', 'n', 'o', 'o', 'o', 'o', ':', 'r', 'u',   /*240-9*/
	                    'u', 'u', 'u', 'y', 't'};                           /*250-4*/
                                                          /* NEW */

unsigned char iso88592_to_ascii(unsigned char x)
{
  return ((x >= 160) && (x < 255)) ? (toascii_hi[x-160]) : (x);
}

const char mes[12][5]={"I", "II", "III", "IV", "V", "VI",
		       "VII", "VIII", "IX", "X", "XI", "XII"};
/* platnost jizdniho radu */
int defod = 528, defdo = 10609;        /* rok(0/1)*10000 + mesic*100 + den */
                                       
int _exists(char *buf, struct tm *tcas)
{
	int kladno = 0, podm, i = 0, d1, d2, d3;
	char s1[1024];
	int odd = defod, dod = defdo, tmpod = 0;
	int tmpores = -1,  ores = -1,  res = 1, savres = -1, res_found = 0;

	if (buf == NULL) return 1;
	if (!memcmp(buf, "t.�. nejede", 11)) return 0;

	for(;;) {
		podm = -1;

		if (buf[i] == 0) {       /*   koncime */
	  
			if (tmpores != -1)   /* naslo se "od" nebo "do" */   	    
				if (tmpores) {
					ores = 1;          /* zapada do intervalu "od" "do" */
					if (res_found) savres = res;
				}   
              
			if (ores == 1) return savres;  /* zapada do "jede do" */    
			if (!tmpores) return 0;        /* ani jedna pulka intervalu */
			return res;                    /* nebylo "od" "do" */
		}      

		if (!memcmp(buf + i, "jede", 4)) {  /* zacina pozitivni cast */ 
			kladno = 1;
			res = 0;
			i += 4;
			continue;
		}

		if (!memcmp(buf + i, "nejede", 6)) {  /* zacina negativni cast */
			kladno = 0;
			i += 6;
			continue;
		}
      
		if (!memcmp(buf + i, "v", 1)) {
			res_found = 1;            
			podm = 0;
			i += 2;
			d3 = 1;
	  
			while (d3) {    /* zkousi cist intervaly dnu v tydnu */
				d3 = sscanf(buf + i, "%d-%d", &d1, &d2);
				if ((d3 == -1) || (!d3) ) break;

				if (d3 == 1) {     /* neprecetl interval, ale jen jeden znak */
					if (buf[i + 1] != ' ' && buf[i + 1] != ',' && buf[i + 1] != 0) {
						i--;     /* neni to den v tydnu, ale kus datumu */
						break;
					}		  
					d2 = d1;        /* je to prave jeden den v tydnu */
					i++;		 
				} else i += 3;	  /* spadame do intervalu */		

				if ((d1 <= tcas->tm_wday) && (tcas->tm_wday <= d2)) podm = 1;
             
				while (buf[i] == ' ') i++;              
				if (buf[i] == ',') i++;                  /* za carku */
			}
			/* jsme v intervalu jede nebo nejede */
			res = ((kladno) ? (res | podm) : (res & !podm));
			continue;
		}

		if (isdigit(buf[i])) {                 /* nacitame datumy */
			res_found = 1;
			d3 = 1;
			podm = 0;
	 
			while (d3) {
				d3 = sscanf(buf + i,"%d.", &d1);
				if ((!d3) || (d3 == -1)) break;
				if (d1 == tcas->tm_mday) podm = 1;  /* den v mesici dobry */
				i += 2;
				if (d1 > 9) i++;                     /* za tecku */
				while (buf[i] == ' ') i++;

				if (buf[i] == ',') { 		     /* vice dnu stejneho mesice */
					i++;
					continue;
				}

				d2 = 0;
				while (buf[i] == ' ') i++;

				while (buf[i] != '.') {     /* chroustame rimsky mesic */
					s1[d2++] = buf[i++]; 
					if (buf[i] == 0)  return 2; /* nenasli jsme tecku - divne */
				}

				s1[d2] = 0;
				i++;	   			/* za tecku */
				for (d1 = 1; d1 <= 12; d1++)
					if (!strcmp(mes[d1 - 1], s1) && d1 != tcas->tm_mon) podm = 0;
				break;                     /* matchuje mesic, ale ne nas */
			}

			res = ((kladno) ? (res | podm) : (res & !podm));
			continue;
		}

		if (!memcmp(buf + i, "od", 2)) {
			if (tmpores == -1) {    /* nebylo do */ 
				if (!res_found) res = kladno;  /* nebylo v */
				savres = res;
			} else  if (tmpores) {        /* bylo od a zabralo */
				ores = 1; 
				if (res_found) savres = res;      /* bylo i v */
			}

			res = !kladno;
			res_found = 0;
			i += 3;
			podm = 0;
			sscanf(buf + i, "%d.", &d1);
			i += 2;
			if (d1 > 9) i++;                  /* za tecku, den */
			d2 = 0;
			while (buf[i] != '.') s1[d2++] = buf[i++];
			s1[d2] = 0;
			i++;	                            /* za tecku, mesic */

			for (d2 = 1; d2 <= 12; d2++)
				if (!strcmp(mes[d2 - 1], s1)) break;

			odd = d2 * 100 + d1;                        
			if (odd <= defdo % 10000) odd += 10000; /* druhy rok platnosti */

			tmpod = (tcas->tm_mon * 100) + tcas->tm_mday;
			if (tmpod <= (defdo % 10000)) tmpod += 10000;

			if (tmpod >= odd) podm = 1;         /* jsme v intervalu */
			tmpores = ((kladno) ? podm : !podm);

			continue;
		}

		if (!memcmp(buf + i, "do", 2)) {

			if (tmpores == -1) {         /* nebylo od */
				if (!res_found) res = kladno;  /* nebylo ani datum */
				savres = res;
			} else 	if (tmpores) {        /* bylo od a je OK */
				ores = 1;  
				if (res_found) savres = res;   /* bylo datum */
			}  
     
			res = !kladno;
			res_found = 0;
			i += 3;
			podm = 0;
			sscanf(buf + i, "%d.", &d1);
			i += 2;
			if (d1 > 9) i++;                       /* za tecku dne */

			d2 = 0;
			while (buf[i] != '.') s1[d2++] = buf[i++];
			s1[d2] = 0;
			i++;	                                  /* za tecku mesice */

			for (d2 = 1; d2 <= 12; d2++)
				if (!strcmp(mes[d2 - 1], s1)) break;

			dod = d2 * 100 + d1;                      /* podle roku */
			if (dod <= (defdo % 10000)) dod += 10000;
           
			if (!tmpod) tmpod = (tcas->tm_mon * 100) + tcas->tm_mday;
			if (tmpod <= (defdo % 10000)) tmpod += 10000;

			if ((tmpod <= dod) && (odd <= tmpod)) podm = 1;
			/* jsme v intervalu, bud od "od" */
			tmpores = ((kladno) ? podm : !podm); /* nebo od zacatku platnosti */
			continue;
		}

		if (!memcmp(buf + i, "a", 1)) return 2;
           
		if ((buf[i]==' ') || (buf[i]==',')) {
			i++;
			continue;
		}       

		return 2;             /* neznamy znak */
	}
}   

char exists(char *buf, struct tm *tcas)
{ 
	switch (_exists(buf, tcas)) {
	case 1: return 'Y'; 
	case 0: return 'N';
	default:
	case 2: if (sure) return 'N';
	        return 'E';
	}
}
