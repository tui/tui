/* -*- linux-c -*-
 * Translator of connection tables into more dense format, copyright
 * 2000, 2001 Pavel Machek, GPLv2 or later
 *
 * Converts time like "1:20 1:25" to "1:20 +5"
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#define XCHAR ';'

int pstate = 0, curtime, curkm, lasttime, lastkm;

typedef unsigned char uchar;

void
decodeline(uchar *buf)
{
	int i=0, j = 0;
	uchar townname[10240], remark[10240];
	int from1 = -1, from2 = -1, to1 = -1, to2 = -1, km = -1;

	for (i=0; i<strlen(buf); i++) {
		townname[i] = buf[i];
		if ((buf[i]==' ') && ((buf[i+1]==' '))) {
			townname[i]=0;
			break;
		}
		if (!buf[i]) {
			fprintf( stderr, "Could not find townname on %s\n", buf );
			exit(1);
		}
	}


	while (buf[i]==' ')
		i++;

	for (; i<strlen(buf); i++) {
		if (buf[i]==XCHAR)
			break;
		if (buf[i]!=' ') {
			if (sscanf( buf+i, "%d:%d", &from1, &from2 ) != 2) {
				fprintf( stderr, "Could not find time #1 on %s\n", buf );
				exit(1);
			}
			i+=5;
			break;
		}
	}
	i++;

	for (; i<strlen(buf); i++) {
		if (buf[i]==XCHAR)
			break;
		if (buf[i]!=' ') {
			if (sscanf( buf+i, "%d:%d", &to1, &to2 ) != 2) {
				to1 = to2 = -1;
				i -= 1;
				pstate = 0;		/* This must be last */
				break;
			}
			i+=5;
			break;
		}
	}
	i++;

	while (buf[i]==' ')
		i++;

	for (; i<strlen(buf); i++, j++) {
		remark[j] = buf[i];
		if ((buf[i]==' ') && ((buf[i+1]==' '))) {
			remark[j]=0;
			break;
		}
		if (!buf[i]) {
			fprintf( stderr, "Could not find remark on %s\n", buf );
			exit(1);
		}
	}
	if (remark[0]==XCHAR)
		remark[0]=0;

	while (buf[i]==' ') i++;
	if (sscanf(buf+i, "%d", &km ) != 1) {
		int km2;
		if (sscanf(buf+i, "%d.%d", &km, &km2 ) != 1) {
			fprintf( stderr, "Kilometers not right on %s *%s\n", buf, buf+i );
			lastkm = 0;
		}
	}

	if (1) {
		if (from1 != -1) {
			if ((lasttime != -1) && ((from1*60+from2 - lasttime)>0))
				printf( "+%d", from1*60+from2 - lasttime );
			else
				printf( "%d:%02d", from1, from2 );
			lasttime = from1*60+from2;
		}
		printf( "\t" );
		if (to1 != -1) {
			if ((lasttime != -1) && ((to1*60+to2 - lasttime)>0))
				printf( "+%d", to1*60+to2 - lasttime );
			else
				printf( "%d:%02d", to1, to2 );
			lasttime = to1*60+to2;
		}
		printf( "\t%s\t+%d\t%s\n", remark, km-lastkm, townname );
		lastkm = km;
	}
}

void
readit(void)
{
	uchar buf2[10240], *buf;
	uchar rbuf[20480];
	FILE *f = stdin;
	int lineno = 0;
        int rstate = 0;

	while (1) {
		lineno++;
		if (!(lineno % 10000))
			fprintf( stderr, "Reading: %dK lines\r", lineno/1000 );
		if (fgets(buf2, 10230, f)==NULL) break;
		buf = buf2;
		if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
		while (*buf == ' ')
			buf++;
                while (buf[strlen(buf)-1]==' ') buf[strlen(buf)-1]=0;
                if (!strcmp(buf,"")) continue;
		if (*buf == '#') {
                	if (rstate) printf("\n");
			printf("%s\n", buf);
			lasttime = -1;
			lastkm = 0;
			pstate = 1;
                        rstate = 0;
			continue;
		}
		if (*buf == '[') {
			char l = buf[4];
                	if (rstate) printf("\n");
                        buf+=8;
	                while (buf[0]==' ') buf++;
			printf("%c%s", l, buf);
			pstate = 0;
                        rstate = 1;
			continue;
		}
		if (pstate == 1)
			decodeline(buf);
		else {
                	if (rstate) printf(" %s",buf);
                        	else {
                                	fprintf( stderr, "Line in unexpected state: %s\n", buf );
 	                        }
		}
	}
}

int
main(int argc, char *argv[])
{
	readit();
}
