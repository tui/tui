program Convert2;
{     Convert2 - converts html documents from e.g. www.vlak-bus.cz to .tt format
      Copyright (C) 2001  Petr Cermak

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
}

const
        TURN_FILENAMES_ON=True;    {Just change it to false to disable adding filename to the name of train}

        NAME_TAG: string='<TITLE>';{Tags to check validity of the file}
        STNAMES_TAG: string='<NOSCRIPT>';
        STNOTES_TAG: string='<TD'+#09+'NOWRAP>';
        DATA_TAG: string='<TD>';
        NOTEINFO_TAG: string='IMAGES/REM';

        NONOTE_STR: string='&#';

        MAX_CHARS=500;           {Array sizes. Change as needed}
        MAX_STATIONS=100;
        MAX_NOTES=25;
        MAX_CONV=3;
                                 {Array to convert special strings and check string validity}
                                 {Change as needed (Only the last pair is important)}
        CONV: array[1..MAX_CONV, boolean] of string=(('&gt;','>'),('&lt;','<'),('&#160;',''));

        NOTHING=-20000;          {No time stored}

        CCR=#13;  {CR}           {Character values needed in this program}
        CLF=#10;  {LF}
        CSL=#95;  {_}
        CSM=#60;  {<}
        CBG=#62;  {>}
        CSP=#32;  { }
        CTB=#09;  {TAB}
        CFR=#47;  {/}
        CDT=#46;  {.}
        CCM=#34;  {"}
        CCL=#58;  {:}

        INSTRUCTIONS: string='Convert2 - converts html documents from e.g. www.vlak-bus.cz to .tt format'+CCR+CLF+
                             'Usage: convert2 list_of_files' + CCR + CLF +
                             'Output: stdout';

{***Type declarations***}
type
    TArr=array[1..MAX_CHARS] of char;
    TStr=record
               Arr :TArr;
               Len :integer;
         end;
    TStation=record
               Name: TStr;
               Note: TStr;
               Arrival: Integer;
               Departure: Integer;
               Distance: Integer;
             end;
    TStations=array[1..MAX_STATIONS] of TStation;
    TNotes=array[1..MAX_NOTES] of TStr;
    TTrain=record
               Name: TStr;
               Count: Integer;
               CountN: Integer;
               Stations: TStations;
               Notes: TNotes;
	           Float: Boolean;
           end;
    TFChr=file of char;

{***Procedures and functions***}

{***Writes error message and exits program***}
procedure IOError(var f: TFChr);
begin
     close(f);
     writeln('Error reading file: ioresult not zero');
     halt(1);
end;

{***Converts special strings as defined in CONV array***}
{***Output: what***}
procedure ConvertStr(var what: TStr);
var
   i,j: integer;
   b: boolean;
begin
   for i:=1 to MAX_CONV do
   begin
        if what.Len<>length(CONV[i,false]) then
           b:=false
        else
           b:=true;
        for j:=1 to what.Len do
             if what.Arr[j]<>CONV[i,false][j] then b:=false;
        if b=true then
        begin
             for j:=1 to length(CONV[i,true]) do
                 what.Arr[j]:=CONV[i,true][j];
             what.Len:=length(CONV[i,true]);
        end;
   end;
end;

{***Checks if string given is valid***}
{***Output: Function***}
function CheckStr(var what: TStr): boolean;
var
   i: integer;
begin
     CheckStr:=true;
     for i:=1 to what.Len do
          if (what.Arr[i]=CCR) or (what.Arr[i]=CLF) then CheckStr:=false;
end;

{***Skips count occurences of what character in f file***}
{***Note: if you want to skip strings betweem commas, set skipcm to true***}
procedure SkipUntil(var f: TFChr; what: char; count: integer; skipcm: boolean);
var
   i: integer;
   c: char;
   insidecm: boolean;
begin
     insidecm:=false;
     i:=0;
     {$I-}
     repeat
          read(f,c);
          if c=CCM then insidecm:= not insidecm;
          if ioresult<>0 then IOError(f);      {Exits program}
          if (c=what) and (((not insidecm) and (skipcm)) or (not skipcm)) then inc(i);
     until i=count;
     {$I+}
end;

{***Reads from f file until char***}
{***Output: where***}
procedure ReadUntil(var f: TFChr; what: char; var where: TStr);
var
   i: integer;
   c: char;
begin
     c:=#0;
     i:=0;
     {$I-}
     while c<>what do
     begin
          read(f,c);
          if ioresult<>0 then IOError(f);      {Exits program}
          if c<>CTB then
          begin
             inc(i);
             where.Arr[i]:=c;
          end;
     end;
     {$I+}
     where.Len:=i-1;
     if where.Len<0 then where.Len:=0;
end;

{***Reads from file until given string occures***}
{***Output: Function***}
{***Note: If parameter stop is true, function can exit program if tag not found***}
function FindTag(var f: TFChr; Tag: string; stop: boolean): boolean;
var
   c: char;
   i,len: integer;
   done: boolean;
begin
     {$I-}
     i:=1;
     len:=length(Tag);
     done:=False;
     while (not eof(f)) and (not done) do
     begin
          read(f,c);
          if ioresult<>0 then IOError(f);      {Exits program}
          if Tag[i]=UpCase(c) then
          begin
               inc(i);
               if i=(len+1) then done:=True;
          end
          else
              i:=1;
     end;
     {$I+}
     if (not done) and stop then
     begin
          writeln('Unexpected file format. Tag not found: ',Tag);
          halt(1);
     end;
     FindTag:=done;
end;

{***Finds datum between html tags***}
{***Output: where***}
procedure FindDatum(var f: TFChr; var where: TStr);   {Datum != Date}
var
    done: boolean;
begin
     done:=false;
     repeat
        ReadUntil(f, CSM, where);
        SkipUntil(f, CBG, 1, true);
        if where.Len>0 then
        begin
           done:=true;
           if not CheckStr(where) then
                where.Len:=0;                   {Invalid string}
        end;
     until done;
end;

{***Converts: ' ' -> '_'; '/' -> 'X'***}
{***Output: what***}
procedure Spc2Sls(var what: TStr);
var
   i: integer;
begin
     for i:=1 to what.Len do
     begin
         if what.Arr[i]=CSP then what.Arr[i]:=CSL;
         if what.Arr[i]=CFR then what.Arr[i]:='X'
     end
end;

{***Prints what:TStr***}
{***Output: stdout***}
procedure PrintStr(var what: TStr);
var
   i: integer;
begin
     for i:=1 to what.Len do
         write(what.Arr[i]);
end;

{***Prints the whole train ***}
{***Output: stdout***}
procedure PrintTrain(var Tr: TTrain; s: string);
var
   i,j,l,a: integer;
   stmp: string;
begin
     write('# ');
     PrintStr(Tr.Name);
     if (TURN_FILENAMES_ON) then write('_',s);
     writeln;
     for i:=1 to Tr.Count do
     begin
         if (Tr.Stations[i].Arrival<>NOTHING) and (i>1) then
            if Tr.Stations[i].Arrival<=0 then
            begin
                 Tr.Stations[i].Arrival:=-Tr.Stations[i].Arrival;
                 write(Tr.Stations[i].Arrival div 60, ':');
                 a:=Tr.Stations[i].Arrival mod 60;
                 if a<10 then
                    write('0',a)
                 else
                    write(a);
            end
            else
                write('+',Tr.Stations[i].Arrival);
         write(CTB);

         if (Tr.Stations[i].Departure<>NOTHING) and (i<Tr.Count) then
            if Tr.Stations[i].Departure<=0 then
            begin
                 Tr.Stations[i].Departure:=-Tr.Stations[i].Departure;
                 write(Tr.Stations[i].Departure div 60, ':');
                 a:=Tr.Stations[i].Departure mod 60;
                 if a<10 then
                    write('0',a)
                 else
                    write(a);
            end
            else
                write('+',Tr.Stations[i].Departure);
         write(CTB);
         if Tr.Stations[i].Note.Len<>0 then PrintStr(Tr.Stations[i].Note);
         write(CTB);
         if not Tr.float then
            write(Tr.Stations[i].Distance)
         else
         begin
            str(Tr.Stations[i].Distance,stmp);
            l:=length(stmp);
            for j:=l to 3 do
            begin
                 stmp:='0'+stmp;
                 inc(l);
            end;
            for j:=1 to l-3 do write(stmp[j]);
            write(CDT);
            for j:=3 downto 1 do write(stmp[l-j+1]);
         end;
         write(CTB);
         printstr(tr.stations[i].Name);writeln;
     end;
     for i:=1 to Tr.CountN do
     begin
         PrintStr(Tr.Notes[i]);writeln
     end
end;

{***Concatenates this two strings: where=where+what***}
{***Output: where***}
procedure ConcatStr(var where: TStr; var what: TStr);
var
   i: integer;
begin
     for i:=where.Len + 1 to where.Len + what.Len + 1 do
         where.Arr[i]:=what.Arr[i-where.Len];
     where.len:=where.Len+what.Len;
end;

{***Inits Train structure***}
{***Output: Tr***}
procedure InitTrain(var Tr: TTrain);
begin
     Tr.Count:=0;
     Tr.CountN:=0;
     Tr.Float:=false;
end;

{***Converts Str to # of seconds***}
{***Output: Function***}
function Str2Time(var where: TStr): integer;
begin
     if where.len = 4 then        {Time format:  5:25}
          Str2Time:=(ord(where.Arr[1])-48)*60 + (ord(where.Arr[3])-48)*10 +
                     ord(where.Arr[4])-48
     else                         {Time format: 15:38}
          Str2Time:=(ord(where.Arr[1])-48)*600 + (ord(where.Arr[2])-48)*60 +
                    (ord(where.Arr[4])-48)*10  +  ord(where.Arr[5])-48;
end;

{***Converts Str to integer value***}
{***Output: Function***}
function Str2Val(var where: TStr; var flt: boolean): integer;
var
   i, temp, digit: integer;
begin
    flt:=false;
    temp:=0;
    digit:=1;
    for i:=1 to where.Len do
    begin
         if where.Arr[where.Len-i+1]<>CDT then
         begin
              temp:=temp+(ord(where.Arr[where.Len-i+1])-48)*digit;
              digit:=digit*10;
         end
         else
             flt:=true;             {if the number is float}
    end;
    Str2Val:=temp;
end;


{***Main procedures***}

{***Reads train name***}
{***Output: Train***}
procedure ReadTrainName(var FileIn: TFChr; var Train: TTrain);
var
   str: TStr;
begin
    str.Len:=0;
    FindTag(FileIn, NAME_TAG, true);
    FindDatum(FileIn, str);
    Spc2Sls(str);
    Train.Name:=str;
end;

{***Reads all station names***}
{***Output: Train***}
procedure ReadStationNames(var FileIn: TFChr; var Train: TTrain);
var
   str: TStr;
   i: integer;
begin
    str.Len:=0;
    i:=0;
    FindTag(FileIn, STNAMES_TAG, true);
    SkipUntil(FileIn, CLF, 1, false);
    repeat
         FindDatum(FileIn, str);
         inc(i);
         Train.Stations[i].Name:=str;
    until str.len=0;
    Train.Count:=i-1;
end;

{***Reads station notes***}
{***Output: Train***}
procedure ReadStationNotes(var FileIn: TFChr; var Train: TTrain);
var
   str: TStr;
   i: integer;
begin
    str.Len:=0;
    i:=0;
    FindTag(FileIn, STNOTES_TAG, true);
    for i:=1 to Train.Count do
    begin
         FindDatum(FileIn, str);
         ConvertStr(str);
         Train.Stations[i].Note:=str;
    end
end;

{***Reads all arrivals***}
{***Output: Train***}
procedure ReadArrivals(var FileIn: TFChr; var Train: TTrain);
var
   str: TStr;
   i: integer;
begin
    str.Len:=0;
    i:=0;
    FindTag(FileIn, DATA_TAG, true);
    for i:=1 to Train.Count do
    begin
         FindDatum(FileIn, str);
         ConvertStr(str);
         if str.Len=0 then
            Train.Stations[i].Arrival:=NOTHING
         else
            Train.Stations[i].Arrival:=Str2Time(str)
    end
end;

{***Reads all departures***}
{***Output: Train***}
procedure ReadDepartures(var FileIn: TFChr; var Train: TTrain);
var
   str: TStr;
   Last,i,a: integer;
begin
    str.Len:=0;
    i:=0;
    Last:=0;
    a:=0;
    FindTag(FileIn, DATA_TAG, true);
    FindDatum(FileIn, str);             {First departure}
    Train.Stations[1].Departure:=Str2Time(str);
    Last:=Train.Stations[1].Departure;
    for i:=2 to Train.Count-1 do        {Reads times of departures}
    begin
         FindDatum(FileIn, str);
         a:=Str2Time(str);
         if (Train.Stations[i].Arrival=a) or (Train.Stations[i].Arrival=NOTHING) then
         begin
              Train.Stations[i].Arrival:=NOTHING;
              if a>Last then
                 Train.Stations[i].Departure:=a-Last
              else
                 Train.Stations[i].Departure:=-a;       {Change of day or timezone}
         end
         else
         begin
              if a>Train.Stations[i].Arrival then
                 Train.Stations[i].Departure:=a-Train.Stations[i].Arrival
              else
                 Train.Stations[i].Departure:=-a;       {Change of day}
              if Train.Stations[i].Arrival>Last then
                 Train.Stations[i].Arrival:=Train.Stations[i].Arrival-Last
              else
                 Train.Stations[i].Arrival:=-Train.Stations[i].Arrival;
         end;                                           {Change of day or timezone}
         Last:=a;
    end;
    i:=Train.Count;                                     {finish the last}
    if Train.Stations[i].Arrival>Last then
       Train.Stations[i].Arrival:=Train.Stations[i].Arrival-Last
    else
       Train.Stations[i].Arrival:=-Train.Stations[i].Arrival;
    Train.Stations[1].Departure:=-Train.Stations[1].Departure; {and the first station}
end;

{***Reads all Distances***}
{***Output: Train***}
procedure ReadDistances(var FileIn: TFChr; var Train: TTrain);
var
   str: TStr;
   last, temp, i: integer;
   float: boolean;
begin
    str.Len:=0;
    i:=0;
    Last:=0;
    temp:=0;
    float:=false;
    FindTag(FileIn, DATA_TAG, true);
    FindDatum(FileIn, Str);             {Skip the first distance - always 0}
    Train.Stations[1].Distance:=Last;
    for i:=2 to Train.Count do          {Reads other distances}
    begin
         FindDatum(FileIn, str);
         temp:=Str2Val(str, float);
         train.Stations[i].Distance:=temp-Last;
         Last:=temp;
    end;
    if float then Train.float:=float;
end;

{***Reads all train notes***}
{***Output: Train***}
procedure ReadNotes(var FileIn: TFChr; var Train: TTrain);
var
   str2, str1: TStr;
   i: integer;
begin
    i:=0;
    while FindTag(FileIn, NOTEINFO_TAG, false) do  {while there are notes}
    begin
         inc(i);
         ReadUntil(FileIn, CDT, str1);
         FindTag(FileIn, DATA_TAG, true);
         repeat
               FindDatum(FileIn, str2);
               ConcatStr(str1, str2);
         until str2.len=0;
         Train.Notes[i]:=str1;
    end;
    Train.CountN:=i;
end;

{***Variables used in the main program***}
var
    FileIn: TFChr;
    Train: TTrain;	{Structure containing informations about the train being read}
    i: integer;
{***Main program***}
begin
    for i:=1 to ParamCount do
    begin
        InitTrain (Train);
        {$I+}
        assign(FileIn, ParamStr(i));
        reset(FileIn);
        {$I-}
        if ioresult<>0 then IOError(FileIn);
        ReadTrainName(FileIn, Train);
        ReadStationNames(FileIn, Train);
        ReadStationNotes(FileIn, Train);
        ReadArrivals(FileIn, Train);
        ReadDepartures(FileIn, Train);
        ReadDistances(FileIn, Train);
        ReadNotes(FileIn, Train);
        PrintTrain(Train, ParamStr(i));
        close(FileIn);
    end;
    If ParamCount=0 then writeln(INSTRUCTIONS);
end.
