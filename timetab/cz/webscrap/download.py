#!/usr/bin/env python
# Copyright 2009 Petr Machek, GPL

import re
import urllib
import base64
import os
from time import time


timetable = open(os.path.curdir + "/timetable_cz.tt", "w" )

# dulezite globalni promenne, odkazy

idos = "http://jizdnirady.idnes.cz/vlakyautobusy/draha/?p="
parameter1 = "%5457256%5433295%0%35470%%%"
irk = "&irk="
parameter2 = ""
errors = ""

def generate_url(conn_number, param1, param2, conn_type = 13):
  global idos
  global irk
  
  param1 = str(conn_type) + "%" + str(conn_number) + param1
  param1 = base64.standard_b64encode(param1)
#  param2 = base64.standard_b64encode(param2)

  if param1.endswith("=="):
    param1 = param1[:-2] + "--"
  elif param1.endswith("="):
    param1 = param1[:-1] + "-"
  if param2.endswith("=="):
    param2 = param2[:-2] + "--"
  elif param2.endswith("="):
    param2 = param2[:-1] + "-"

  return idos + param1 + irk + param2


def time2minutes(string):
  hours, minutes = string.split(":")
  return 60*int(hours) + int(minutes)


def minutes2time(minutes):
  if minutes/60 >= 24:
    minutes = minutes % 24*60
  time = "%i:%02i" % (int(minutes)/60, int(minutes)%60)
  return time

# definice filtru na regularni vyrazy
namefinder = re.compile(r'<h3 title="[^>]*>([^<>]*)<.*/h3>')
arrivalfinder = re.compile(r'<td class="right  arr"[^<>]*>([^<>]*)<')
departurefinder = re.compile(r'<td class="right  arr"[^<>]*>[^<>]*</td><td class="right">([^<>]*)</td>')
imgnotefinder = re.compile(r'<img[^>]*title="([^"]*)"')
distancefinder = re.compile(r'<td class="right">([^<>]*)</td><td>[^<>]*</td>')
pointfinder = re.compile(r'<td>(<.*>)*([^<>]*)(<.*>)*\s?</td><td class="right  arr"')
footnotefinder = re.compile(r'<li class="ico-info-[^>"]+">(.*)')
jedenejede = re.compile(r'(ne)?jede [^<]*')

prev_distance = 0
prev_time = "not set"


def re_clear(page):
  pagesource = page.read()
  print page.geturl()
  splitsource = pagesource.split("\n")
  length = len(splitsource) - 1
  index = 0

  while((index <= length) and ('div class="section"' not in splitsource[index])):
    index += 1
  important_lines = []
  while((index <= length) and ('/tbody' not in splitsource[index])):
    important_lines.append(splitsource[index])
    index += 1

  global namefinder
  global pointfinder
  global arrivalfinder
  global departurefinder
  global imgnotefinder
  global distancefinder
  
  global prev_distance
  global prev_time
  
  name_match = namefinder.search(important_lines[0])

  if name_match == None:
    # nastava, pokud se nepodarilo na strance najit jmeno spoje
    errorfinder = re.compile(r'\s<td id="ErrText">([^<>]*(<br />)*[^<>]*)</td>')
    guidfinder = re.compile(r'ClientGUID (.*), ContextGUID')
    
    err_description = errorfinder.search(pagesource)
    
    if err_description != None:
      idostext = err_description.group(1)
      guid_match = guidfinder.search(idostext)
      if guid_match != None:
        global parameter2
        parameter2 = guid_match.group(1)
        print "Nacteno nove GUID %s" % parameter2
        return ""
      else:
        print "Vypis o chybe ze zpracovavane stranky idosu:\n"
        print idostext
        print "adresa stranky:", page.geturl()
        print "Zpracovavani ukonceno chybou, pravdepodobne prilis vysoke cislo spoje."
        return ""
    else:
      print "Chybny vypis spoje!"
      print "Problem s odkazem", page.geturl()
      print "Nepodarilo se ze stranky precist chybu."
      return ""
  name = name_match.group(1)
  notes = imgnotefinder.findall(important_lines[0])



  name = name.strip(" ")
  print ""
  print name
  
  output = "\n# " + name
  
  for i in range(1,len(important_lines)):
    line = important_lines[i]
    if ('class="routetr' in line) or ('</tr><tr ' in line):
      line = important_lines[i+1]
      
      arrival_match = arrivalfinder.search(line)
      departure_match = departurefinder.search(line)
      distance_match = distancefinder.search(line)
      point_match = pointfinder.search(line)
      img_note_match = imgnotefinder.findall(line)
      
      if arrival_match != None:
        arrival = arrival_match.group(1)
        if arrival == "&nbsp;":
          arrival = ""
        print arrival + "\t",
        
      if departure_match != None:
        departure = departure_match.group(1)
        if departure == "&nbsp;":
          departure = ""
        print departure + "\t",
        
      if distance_match != None:
        distance = distance_match.group(1)
        print distance + "\t",

      if point_match != None:
        point = point_match.group(2).strip(" ")
        print point

      if img_note_match != None:
        for note in img_note_match:
          notes.append("%s %s" % (point, note))

      if prev_time == "not set":
        prev_time = time2minutes(departure)
      else:
        if arrival != "":
          prev = time2minutes(arrival)
          arrival = time2minutes(arrival) - prev_time
          prev_time = prev
          if arrival < 0:
            arrival = minutes2time(prev)
          else:
            arrival = "+" + str(arrival)
        if departure != "":
          prev = time2minutes(departure)
          departure = time2minutes(departure) - prev_time
          prev_time = prev
          if departure < 0:
            departure = minutes2time(prev)
          else:
            departure = "+" + str(departure)
      try:
        distance = int(distance)
      except ValueError:
        print "Chyba idosu... vynechana vzdalenost"
        distance = prev_distance
      formated_line = "\n%s\t%s\t%s\t%i\t%s" % (arrival, departure, "note", distance - prev_distance, point)
      prev_distance = distance
      output += formated_line
    elif "td colspan" in line:
      notelist = line.split("</li>")
      for a in range(len(notelist)):
        note_match = footnotefinder.search(notelist[a])
        if note_match != None:
          note = note_match.group(1)
          if not note.startswith("<a href"):
            omezeni = jedenejede.search(note)
            if omezeni != None:
              note = omezeni.group(0)
              notes.append(note)
            else:
              if not note in notes:
                notes.append(note)
  for note in notes:
    output += "\nB" + note
  prev_distance = 0
  prev_time = "not set"
  return output+"\n"



def download(first, last = None):
  start = time()
  if last == None:
    last = first
    first = 0
  
  opener = urllib.FancyURLopener({})
  
  for conn_number in range(first-1, last):
    try:
      page = opener.open(generate_url(conn_number, parameter1, parameter2))
      timetable.write(re_clear(page))
    except KeyboardInterrupt:
      timetable.close()
      print "\n\n\nInterrupted. Last connection number:", conn_number
      return
  timetable.close()
  total_time = int(time() - start)
  print "\nZpracovano. %sh %smin %ss" % (total_time/3600, total_time/60, total_time%60)
