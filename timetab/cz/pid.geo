# Name of station <tab> DD mm.mmm (north) <tab> DD mm.mmm (east) <tab> stop size (meters) <tab> precission (default 100 meters) <tab> remark
# Example:
# �jezd	50 4.980	14 24.310		100	
B�l� Ber�nek
Pu��lka
Mototechna
Motol
Chudenick�
Fry�ovick�	50 8.332	14 30.276		100
Ln��sk�
Marj�nka
Vla�imsk�
Na Chvalce
Na Cikorce
Za Chalupami
Pavl�kova
Pelc Tyrolka	50 6.890	14 26.561		100	
Modr� �kola
Krhanice, Prose�nice
Pra�n�
Pra�n� most
Smr�ovsk�
U Ladronky
V Ladech
Kudrnova
Malvazinky
Na Vrch�ch	50 2.364	14 35.255		100	
Na Vini�k�ch
Na Vyhl�dce
Na Vr�k�ch
Na Vart�
Na Va�hov�
Na Vesel�
Hydrologick� �stav
Rad�jovice,Ole�ky
Rad�jovice
Z�b�hlick� �kola
Z�b�hlice
St�.Sk.,Kost.St�.,Skalka
St�.Sk., Kost.St��melice
St�.Sk.,Hrad. St��melice
St�.Skalice, U Konvalink�
Pr�vnick� fakulta
Ve V�rech
Ve Vilk�ch
Povltavsk�	50 6.960	14 26.032		100	
Lipence
Py�ely, Nov� Ves
Py�ely,Zaje�ice
Py�ely,hotel Doly
Py�ely,sokolovna
Py�ely,n�m�st�
Vy�ehrad
U Spoj�
�jezd nad Lesy
�jezd LD
�jezd	50 4.980	14 24.310		100	
K.P��voz,Kamenn� �jezdec
�n�tice
�n�tice,Obecn� ��ad
�n�tice,Na Parcel�ch
U Milionu
Newtonova
And�l	50 4.296	14 24.163		10	
K Tepl�rn�
Amforov�
Z�vist
Kv�tnice
Kv�tnice, rozc.
Kv�tnov�
Sv�tice
M� Satalice
M� Suchdol
Bud�jovick�
Pla�ansk�
Sud�jovick�
T�nec n.S�z.,Zbo�.Kostelec
T�nec n.S�z.,aut.st.
Hanspaulka
Vi��ov�
�ensk� domovy
U �koly
Zat��ka
Kinsk�ho zahrada
�d�nice
P�edn� Kopanina
P�ed�koln�
Bet��
Lod�sk�
PAL Kbely
Chaplinovo n�m�st�
Labe
Sdru�en�
Perunova
Kl�novice
Kl�nova
Sl�nsk�
Weberova
�eberov
�eber�k
Na N�vsi	50 3.385	14 33.721		100	
Na N�vr��
Zderazsk�
Za Nadjezdem
Hlohov�
Libe�,Libe�,O�za
Libe�,Libe�,U Hru�k�
Libe�,Libe�
Libe�,Libe�,STS
Libe�,Na ov��n�
Libe�
Libe�sk�
Libereck�	50 6.546	14 29.324		100	
Libe�sk� most
Sibeliova
Rokycanova
Albertov
Psohlavc�
Zv�novice
Dobe�ka
Struha�ov, Obecn� ��ad
Struha�ov
Struha�ov, U h�i�t�
Struha�ov, Habr
Arbesovo n�m�st�
Doln� B�e�any,Na Sp�lence
Doln� B�e�any,Pod kope�kem
Doln� B�e�any,Na kope�ku
Doln� B�e�any,�kola
Doln� B�e�any,O�
Doln� B�e�any,Lhota
Dolnokr�sk�
Doln� Chabry
Doln� ��rka
Dolnok�eslick�
Doln� M�cholupy
Dolnom�cholupsk�
Doln� Po�ernice
�perlova
U Mat�je
Dube�
Dube�ek
Avia sever
Avia Let�any	50 8.400	14 30.980		100	
Hyb�manka
M� K�eslice
Petr��lova
�pit�lka
�pit�lsk�	50 6.574	14 30.339		100	
Jankovcova	50 6.599	14 26.688		100	
Pankr�c
Pr��sk�
Vojick�
Benkova
Ruzy�sk� velkoobchod
Ruzy�sk� �kola
Ruzy�sk�
Potraviny
Podb�lohorsk�
Podbaba
Ostr�ilovo n�m�st�
C�lkova
Zbuzany,Obecn� ��ad
Zbuzany,n�dra��
V Korytech
Za Farou
Bohdaleck�
Bohdalec
Bavorsk�
Drahel�ice
Formansk�
Svahov�
Divok� ��rka
Murmansk�
M� �epy
�pejchar
�kvorec,T�ebohostice
�kvorec,n�m�st�
�kvorec,M�
Krejc�rek
��msk�
Stejskalova
Hovor�ovice
Kovo�rot
Lovosick�
N�dra�� Kl�novice,sever
N�dra�� Kl�novice
N�dra�� Uh��n�ves
N�dra�� H.Po�ernice
N�dra�� Satalice
N�dra�� Radot�n
N�dra�� Zbraslav
N�dra�� Zli��n
N�dra�� H.M�cholupy
N�dra�� Bubene�
N�dra�� Kr�
N�dra�� �akovice
Novovyso�ansk�
N�dra�� B�chovice
Novodvorsk�
N�dra�� Hostiva�	50 3.236	14 32.224		10	
N�dra�� Veleslav�n
N�dra�� Vyso�any	50 6.810	14 29.935		100	
N�dra�� Hole�ovice	50 6.517	14 26.476		100	
N�dra�� Stra�nice
N�dra�� Vr�ovice
N�dra�� Bran�k
N�dra�� Mod�any
�afr�neck�
Prosek	50 6.046	14 29.877		100	
Proseck�	50 7.130	14 30.074		100	
Hut�
Prvom�jov�
Holyn�
Polygrafick�
U Mlejna
Bojanovice, Sene�nice
Bojanovice, Mal� Le�ice
Bojasova
V�to�
Hejtmansk�	50 6.326	14 33.412		10	
Lomnick�
Karenova
V�penka
Laurov�
Nov� Vokovice
Nov� Butovice	50 3.226	14 21.193		100	
Nov� Podol�
Nov� dvory
Nov� Petrovice
Nov� Stra�nice
Jahodnice II
Jahodnice
Lahovsk�
Lahovick� most
Lahovice
Lahovi�ky
Zaho�any,Zvl�tn� �kola
Z�mky
Schodov�
Lehovec
Tehov, Lada
Tehov
Tehovec
Tehovec, Mototechna
Tehovec, Vojkov	49 59.117	14 42.551		100
Korek
Konv��ka
Lihovar
M�d�neck�
Vlastina
Kloko�n�
Unho��,U N�mc�
Unho��,n�m�st�
Poho��,Chotou�
Poho��,Markvart
Poho��,Skalsko
Poho��
Poho�elec
Roho�nick�
B�ez�
B�ezov�-Ole�ko,Ole�ko
B�ezov�-Ole�ko,U topol�
B�ezov�-Ole�ko,B�ezov�
B�ez�,U k���ku
B�ez�, Podskal�
B�ezin�ves
P�ezletice
�eh.,�akovice, U rybn�ka
Mezi H�bitovy
Tyr�ova �tvr�
U Lom�
Kvasinsk�
U Skl�rny
N.Ves p.Ple��,roz. k san.
Vosm�kov�ch
M� Nebu�ice
Nademlejnsk�	50 6.302	14 31.352		100	
Kov��ova
Kov�rna	50 7.050	14 25.000		100	
Nov� Ves pod Ple��
Nov� Ves pod Ple��,sanatorium
Nov� Ves pod Ple��,rozc. k sanatoriu
Nov� Ves
Nov� kolonie
Nov� ��rka
Tov�rny Hostiva�
Ko���sk� n�m�st�
Lomy
Ka�erov
Zho�eleck�
K Z�v�tin�m
Votuzsk�
V Z�ti��
V Z�pol�
L�beznice I
L�beznice II
L�beznice,�kola
L�beznick�
Vlkan�ice,Komorce
Vlkan.,Pysko�ely,Na B�b�
Vlkan�ice, Star� H�ra
Vlkan�ice
Vlkan�ice, Pysko�ely
Laktos
Oko�
Oko�sk�
D�lnick�
Ma�k�v ml�n
Ta�kent
Rektorsk�
�ep�nsk�
Spo�ick�
Serpentina
Spo�ilov
Chocerady,Samechov
Chocerady,R��en�n,s�dl.
Chocerady,Vlkovec,�st.
Chocerady
Kali�t�, Poddub�
Kali�t�
Kali�t�, Lensedly
Kalin�v ml�n
Tou�imsk�
Lukaveck�
Luka
Muka�ov, Srb�n
Muka�ov, �ernovka
Muka�ov, �kola
Muka�ov, odb. Tehovec
Muka�ov	49 59.240	14 44.294		100
V�hledsk� n�m�st�
V�hledy
Observato� Libu�
Bazar
Dru�n�
Kazanka	50 7.042	14 25.651		100	
Lazarsk� (Sp�len� ulice)
Lazarsk�
Mi�kovice
Po�eradsk�
Krakov
Jesenice,Horn� Jir�any
Jesenice,Horn� Jir�any,vod�rna
Jesenice,Zdim��ice
Jesenice,Osnice
Jesenice,Osnice,h�bitov
Jesenice,Kocanda
Jesenice
Jesenice,bytovky
Jesenick�
�eliveck�
�elivsk�ho
�elivsk�ho (Strojimport)
Otakarova
Kroc�nka	50 6.891	14 30.169		100	
P�nkova
Pr�jezdn�
Slovinsk�
Doliny
Poliklinika H�je
Poliklinika Mazursk�
Poliklinika �ern� Most
Poliklinika Vyso�any
Poliklinika Mod�any
Solidarita
V�trn�k
Bachova
Krematorium Stra�nice
Krematorium Motol
Macharovo n�m�st�
Rose�sk�
Tachlovice,oto�ka
Tachlovice,Na vr�ku
Tachovsk� n�m�st�
Obchodn� d�m Pet�iny
K Pitkovi�k�m
Leh�rova
Hulick�
Juliska
Kuli�ka
Sulice
Sulice,rozc. Nechanice
Sulice,Biopharm - V�
Sulice,rozc. Rad�jovice
Sulice,Hlubo�inka
Sulice,rozc. 1.0
Sulice,�elivec
Sulick�
U Svobod�rny	50 6.559	14 29.302		100	
�echtick�
�echova �kola	50 7.050	14 25.458		100	
�ech�v most
�echovo n�m�st�
�echova �tvr�
Nuselsk� radnice
Nuselsk� schody
Lichoceves
Michelsk� les
Michelsk�
Hrn���e
Hrn���sk� h�bitov
Kyselova
Orionka
V Mok�in�ch
Roztoky,Lev� Hradec
Roztoky,rozc. �alov
Roztoky,U rybn��ku
Roztoky,Tyr�ovo n�m�st�
Roztoky,n�dra��
Roztylsk� n�m�st�
Roztyly
Roztock�
Lochkov
�e�ulka
P�vovsk� n�m�st�	50 5.571	14 32.365		100	
Z�vod STE
Z�vodi�t� Chuchle
Tuh��sk�
Bucharova
Kuchy�ka
Suchdol
Tuchom��ice,Pastvi��ata
Tuchom��ice,�t�rb�v ml�n
Tuchom��ice,�pejchar
Tuchom��ice,Z�mek
Tuchom��ice,Kulturn� d�m
Tuchom��ice,Kn��vka
Tuchom��ice,U Dvora
Sychrov
Vychovatelna
Hlubo�epy
Hlubo�epsk�
V�t�zn� n�m�st�
Hutmanka
Doubrav�ice
Doubek
Balabenka	50 6.326	14 28.722		100	
Palack�ho n�m�st�
Palack�ho n�m�st� (n�b�e��)
Zahr�dky
Zahradnictv�
Zahradn� M�sto
�alansk�ho
Sob�n
Urxova
Hlavn�
Hlavn� n�dra��
Slav�nsk�ho
Slavia
Ole�ka,Krymlov
Ole�ka,rozc.Krymlov
Ole�ka
Valtick�
�honice,�kola
�honice
Pr�b�n�
V Rybn��k�ch
Oplany, h�jovna
Oplany
Nespeky,Py�ely,rozc.1.4
Nespeky
U Palaty
U Pam�tn�ku
Rezlerova
Stavebn� z�vody
Stavoservis	50 4.503	14 32.280		100	
U T�� svat�ch
Baterie
Kate�inky
Leteck� opravny
Leteck� muzeum
Letensk� n�m�st�
Z�horsk�ho
�imice
�t�rboholy
Hotel Golf
Radho��sk�
V Nov�ch domc�ch
H�ebenka
Podhajsk� pole
Podho��
T�ebotov,Solopysky
T�ebotov,Solopysky,ZD
T�ebotov,Kala
T�ebotov,pomn�k
T�ebotov,sanatorium
T�ebotov
T�ebe�ovsk�
T�ebonice
T�eboradice
T�ebe��n
T�ebenick�
St�ele�sk�
St�edokluky,n�dra��
St�elni�n�
Sib�ina
Sib�ina,Stupice
Boj., Mal� Le�ice, U Hes�
ZPA �akovice
ZPA Jinonice
U Kundratky
U Kunratick�ho lesa
Dob���, Kodetka
Dob���, n�m�st�
Dob���, �el. st.
Dob�ichovice, po�ta
Dob�ichovice, n�dra��
Dob�ejovice,�estlick�
Dob�ejovice,Na n�vsi
Dob�ichovice,�KZUZ
Dob�ichovice,l�k�rna
Dob�ichovice,rozc.Karl�k
Dob���
Na Luk�ch
Na Lysin�ch
Pob�e�n� cesta
Op�lov�
Hellichova	50 5.041	14 24.407		100	
Fran��kova
Franty Kocourka
Kr�lovice
Kr�lovick�
Kr�lova
Kr�lovsk� letohr�dek
Let�any
Vran� nad Vltavou,�kola
Bezdrevsk�
Bezd�kovsk�
Morseova
Lama�ova
Pr�mstav	50 2.815	14 34.521		100	
Pr�myslov�	50 4.336	14 32.121		100	
Zemanka
Tursko
Kn�eves
Kn�eves,sklad. Are�l
Kn�eves,Star� hospoda
Kn�eves,U n�dra��
Kn�sk� luka
Vackov
Vackova
Vajgarsk�	50 6.124	14 33.277		100	
Lipnick�
D�kanka
K Cik�nce
Spiritka
K Jezeru
Maniny	50 6.207	14 27.157		100	
�umavsk�
Sv�pravice
S�zava, aut. st.
S�zava, �ern� Budy
S�zava, osada Kopaniny
Benice
Ryman�,�el. zast.
P��kova
Vinice
Jakobiho	50 1.986	14 33.556		100	
Fark��
�akovi�ky
�akovick�
Vlnit�
L�sek
P�se�n�
�ep�ick�	50 2.920	14 32.868		100	
Hlu�i�kova
Slu�tice,�kola
Hl�dkov
Ml�d�
Na Drah�ch
Kunice,rozc. Vidovice
Kunice, Doln� Lomnice
Kunice,Dol. Lomnice,Chlum
Kunice
Kunice,Vidovice
Kunice, Pragosoja
Koko��nsk�
Lokomotivka
Rokoska
U M�churky	50 3.894	14 22.722		10	
Horky
B�chovice
M�chenick�
Bazovsk�ho
Svrkyn�
Pet�rkova
Lessnerova
Kr�sk� h�bitov
Kr�sk�
Mezou�
N�kladov� n�dra�� �i�kov
N�kladov� n�dra�� �i�kov (Ol�an.)
Jana Masaryka
Kozojedy
Kozojedy, rozcest�
Kozolupy
Vozovna Vokovice
Vozovna Pankr�c
Vozovna Stra�nice
Vozovna Motol
Vozovna Kobylisy
Vozovna St�e�ovice
Lou�ovice
Lou�ovice, Na hr�zi I.
Lou�ovice, Na hr�zi II.
Santo�ka
Hercovka
Argentinsk�
U Kest��nk�
Ov�� h�jek
Dunajeck�
Pavelkova
Xaverovsk� h�j
Xaverov
Podkov��sk�	50 6.253	14 30.719		100	
Barvy a laky
O�e�sk�
P�e�tick�
P�e�t�nsk�
Severoz�padn�
U �ist�rny
�erven� �jezd,rozc.
�erven� �jezd,U N�prstk�
�erven� �jezd
�erven� dv�r
�erve�ansk�ho
�erven� ml�n
�erven� vrch
H�jov�
Jalodvorsk�
Malotice,U J�n�
Malostransk� n�m�st�	50 5.220	14 24.392		100	
Malostransk�	50 5.455	14 24.592		100	
Malovanka
Palou�ek
Obloukov�
Na Okraji
Naskov�
Sovenick�
�eskolipsk�
�eskobrodsk�
�eskomoravsk�	50 6.381	14 29.514		100	
�im�nkova
Biskupcova
Sklon�n�
Cik�nka
Kolovraty
Kolod�jsk� obora
Kolod�je
Kolonie
N�b�e�� Kapit�na Jaro�e
U Po�ernick�ho rybn�ka
U Po�ty
V Podbab�
Bulovka
Kobylisk� n�m�st�
Rusk�
��rsk�	50 6.278	14 33.115		100	
Tylova �tvr�
Ond�ejov, Turkovice
Ond�ejov,T�emblat,r.My�l�n
Ond�ejov, T�emblat
Ond�ejov,T�emblat,r.Zv�novice
Ond�ejov,T�emblat,r.Mnichovice
Ond�ejov, U pily
Ond�ejov, n�m.
Ond�ejov, h�bitov
Mod�ansk� �kola
Mod�ansk� rokle
T�nov
L��nice,Sp�len� ml�n
L��nice
L��nice,hlavn� silnice
Jarn�kova
Nedv�dovo n�m�st�
Nedv�z�
Sluhy
Sluhy,rozc. Br�zdim
�ehenice, �akovice
�ehenice, Mo�idla
�ehenice, Da�bo�e
�ehenice, �ist� potoky
�ehenice, Babice
�ehenice,K�iv��ek
I.P.Pavlova (do�asn�)
I.P.Pavlova
�ern� Vod�rady
�ern� Vod�rady,O�
�ern� Vod�rady, Chaloupky
�ernolice,Nov� Dv�r
�ernolice
�erno�ice,II,�kola
�erno�ice,�el. zast.
�erno�ice
�ern� Most
�ern�nova
�ernokosteleck� (smy�ka)
�ernokosteleck�
�ern� k��
Mikrobiologick� �stav
Jirny,Statek
Jirny
Jirny,Nov� Jirny, I.
Jirny,Nov� Jirny, II.
Na Gro�i
Lohenick�
Tesco Let�any
Mokr�
Horn� cesta
Hornocholupick�
K Leti�ti
U Les�ka
U Samoobsluhy
Chote�
Chotkovy sady
Lhoteck� les
Lhotka
Kapit�na Str�nsk�ho
Cukrovar Mod�any
Cukrovar �akovice
Pr�honice, Kom�rov
Pr�honice,Rozko�
Pr�honice,h�jovna
Pr�honice,Tov�rn�
Pr�honice
Pr�honice,Hole
Pr�honice,Kom�rov
Pr�honsk� h�j
Zdim��ick�
Strnady
Br�zdim,Star� Br�zdim
Br�zdim,Nov� Br�zdim
Br�zdim,rozc. Velik� Br�zdim
Uh��n�vesk� h�bitov
Uh��n�ves
Ve �l�bku
Ji��ho z Pod�brad
U Stud�nky
U Statku
��kovsk�
M� Doln� Chabry
Nemocnice Motol
Nemocnice Na Homolce
Nemocnice Bubene�
Nemocnice Kr�
Mal� Hra�tice
Mal� Ohrada
Mal� Chuchle
V��erky
Kyt�n, rozcest�
Kyt�n
Kyt�n,h�bitov
P��rodn�
Jir�ansk�
Bel�rie
Pitkovice
St��brn� Skalice, �st.
St��brn� Skalice, Blatiny
St��brn� Skalice, n�m�st�
St��brn� Skalice, Hru�kov
St��brn� Skalice, Propast
St��brn� Skalice, Hradec
�itka
�itka,hlavn� silnice
Hor�i�kova
Komo�any
Skl�dka Chabry
Skl��sk�	50 3.435	14 32.173		100	
Skl�dka ��blice
Vypich
Zadn� Jiviny
Zadn� Kopanina
Jind�i�sk�
U V�ely
Harfa
V�estary, V�vrov
V�estary
V�estary, rozc. Men�ice
B.n.L.-St.B., VDO
B.n.L.-St.B., Rychta
B.n.L.-St.B.,D�m pe�. sl.
B.n.L.-St.B., Vr�b�
B.n.L.-St.B., �el.st.
B.n.L.-St.B., Kas�rna
B.n.L.-St.B., s�dli�t�
B.n.L.-St.B., aut.st.
B.n.L.-St.B., U br�ny
B.n.L.-St.B., most
B.n.L.-St.B., U soudu
B.n.L.-St.B., pek�rny
B.n.L.-St.B., nemocnice
B.n.L.-St.B., s�dl. BSS
B.n.L.-St.B., V ol�ink�ch
B.n.L.-St.B.,s�dl. u n�dr
B.n.L.-St.B., n�dra��
B.n.L.-St.B., Pra�sk�
B.n.L.-St.B., n�m.
B.n.L.-St.B., zdrav. st�.
B.n.L.-St.B., Popovice
Saf�rov�
Le�ansk�
H�rka
Podnikatelsk�
Ol�ansk�
Ol�ansk� h�bitovy
Ol�ansk� n�m�st�
Geologick�
Babice, Babi�ky
Babice
Cholupick� vrch
Cholupice
Nov� Slivenec
Nov� Prosek	50 7.284	14 30.220		100
Nov� St���kov
Nov� Hloub�t�n	50 6.533	14 31.730	300	100	
Rous�novsk�
Blatnick�
Blatov
Blatiny
Ciolkovsk�ho
Kl�r�v �stav
Plat�nova
Rudn�,Zdravotn� st�edisko
Rudn�,Delvita
Rudn�,U n�dra��
Rudn�,Du�n�ky
Rudn�,U kina
Rudn�,�kola
Rudn�,Ho�elice
Slatinov�
Zlatn�ky a Hodkovice,u prodejny
Zlatn�ky a Hodkovice,B�e�ansk�
Zlatn�ky a Hodkovice,Slune�n�
Zlatn�ky a Hodkovice,n�ves
Zlatn�ky a Hodkovice,u hasi��rny
Zlat�
�holi�ky
�holi�ky,V kopci
Brusnice
Bruselsk�
Hrusice, Vl�� hal��
Hrusice
Hrusice, Na �mejkalce
Na Radosti
Na Rol�ch
Prusice
�kola Kyje
�kola Dube�
�kola �epy
�kola Doln� Po�ernice
�kola Nebu�ice
Opatov
To�n�
Brechtova
Libi�,Spolana 4
Libi�,Spolana 3
Sibi�sk� n�m�st�
K�i��kova
Mrat�n,cukrovar
Mrat�n
Pet��kov, Radimovice
Pet��kov
Pet��kov,rozc.
Pet��n
Pet�iny
Zoologick� zahrada	50 7.011	14 24.705		100	
Kesnerka
Ps�ry,cihelna Jir�any
Ps�ry,Doln� Jir�any
Ps�ry,�t�d��k
Ps�ry
Ps�ry,�stav
Spolsk�
Kafkova
Lipany
Lipansk�
Statenice
Statenice,U Kov�rny
Statenice,�. V�l,hospoda
Statenice,�ern� V�l
Korytn�
Po�tovsk�	50 6.558	14 30.573		100	
Po�tovka
Sv�rov,�kola
Sv�rov
Sv�rov,Rym��
Svatoplukova
U Slunce
Eli�ky P�emyslovny
Fr�bortova
K Vystrkovu
Zvole,�ern�ky
Zvole, Nov� Zvole
Zvole,�kola
Zvole
Zvole, J�lovsk�
K Fialce
Do Rybn��k�
K���kov� �jezdec
K���kov� �jezdec,�en�tice
K���ov�
U Pr�honu	50 6.350	14 27.014		100	
H�gerova
Nupaky
�t�pa�sk�
�t�p�nsk�
�t�pni�n�
�st�edn�
�st�edn� d�lny DP
Mn��ek pod Brdy,�isovick�
Mn��ek pod Brdy,s�dli�t�
Mn��ek pod Brdy,Na Kv�kalce
Mn��ek pod Brdy,St��brn� Lhota,U k���ku
Mn��ek pod Brdy,St��brn� Lhota
Mn��ek pod Brdy,�VR
Mn��ek pod Brdy,Kaple
Mn��ek pod Brdy,z�vod (hl.sil.)
Mn��ek pod Brdy,z�vod
Mn��ek p.B., n�m�st�
Mn��ek pod Brdy,Nad �pejcharem
Mn��ek pod Brdy,U �ibence
Janovsk�
Senohraby,�el.st.
Senohraby,U Zvoni�ky
Senohraby,odb.
Obalovna IPS
Jino�any,Hlavn�
Jino�any,n�m�st�
Jinonick�
Jinonice	50 3.267	14 22.222		10	
Vino�
Vinohradsk� tr�nice
Vinohradsk� h�bitovy
Vinohradsk� vod�rna
B�locerkevsk�
Chaltick�
J�lov� u Prahy,�s.arm�dy
J�lov� u Prahy, Borek
J�lov� u Prahy,Pra�sk�
J�lov� u Prahy,Radl�k,rozc.
J�lov� u Prahy,Radl�k
J�lov� u Prahy,n�m�st�
J�lov� u Prahy,u�ili�t�
J�lovi�t�
J�lovi�t�,Cukr�k
J�lovi�t�,hlavn� silnice
J�lovsk�
Klukovice
Baba II
Baba I
P�skovna
Donovalsk�
Konojedy
Konojedy,pila
Konojedy,Kl��e
Konojedy, rozcest�
Vonoklasy,samoobsluha
Vonoklasy
Vonoklasy,chaty
Chodov
Chodovec
Chodovsk�
�muk��ka
Khodlova
Skalka
K Obecn�m h�jovn�m
Mikulova
Trnov�
Trnov�,�kola
Trnov�,rozc.
Trnov�,rozc. I.
U Rozcest�
Na Jelen�ch
Kom�rovsk�
Spalovna Male�ice
Kodymova
U Elektry	50 6.287	14 31.012		100	
Vr�, U transform�toru
Vr�,samoobsluha
Vr�sk�
Italsk�
Brodeck�
Brodsk�ho
Levsk�ho
Klenovsk�
Stod�lky
�valy,Elektromechanika
�valy,�el.st.
�valy,U mate�sk� �koly
Krymsk�
L�skova
Urbanova
H��bkov�
Tepl�rna Male�ice
Tepl�rna T�eboradice
Teplick�
Tepl�rna Michle
Nu�lova
Kajet�nka
L�dv�
My�l�nsk�
�SAD Sm�chov
C�l
Kojetice
Vojensk� nemocnice
Lu�ce
M�ice
M�ice,Jednota
M�ice,Agropodnik
M� Zli��n
Hutn� z�kladna	50 4.465	14 30.991		100	
Kutnohorsk�
Kyje	50 5.360	14 32.942		10	
Herink
U Dobe�ky
Mlad�jovsk�
Na Boleslavce
Na Blanici
Na Ber�nku
Zem�d�lsk� univerzita
Kavkazsk�
Hrad�ansk�
Ke B�ezin�
Osadn�
Stadion Strahov
Jablo�ov�
Kablo	50 3.673	14 32.174		100	
K Vidouli
Keblovsk�
Pr�chova
Kolbenova	50 6.573	14 30.830		100	
Na �afr�nce
Na �abatce
Na �tamberku
P�imsk� n�m�st�
Pelun�k
Dyrinka
K �i�kovu	50 5.971	14 29.860		100	
�tefkova
B�l� Hora
B�l� labu�
Robl�n
Robl�n,Kucha��k
Picassova
Holubice,Kozinec
Holubice
M.p.B., Ryman�, �el.zast.
Dreyerova
Kublov
Ji�n� M�sto
Radiov�	50 4.161	14 32.213		100	
U K���ku
U K���e	50 6.707	14 28.787		100	
Zbraslavsk� n�m�st�
�t�hlice
�t�hlice, Doubrav�ick�
Vav�enova
Odra
Zdravotn� st�edisko
Neratovice,�el. st.
Neratovice,n�m. Republiky
Neratovice,Kojetick�
Terasy
V�enory, Sportovn� are�l
V�enory, U K�covsk�ch
V�enory, U k���ku
��snovka
U L�py
V L�sk�ch
Ohrada
Loty�sk�
Ukrajinsk�
V Uli�ce
O�ech,U Sokolovny
O�ech
O�ech,U �koly
O�echovka
Vodi�kova
Rty�sk�
Korandova
Moravansk�
P�snice
Bertramka
�ertousy
Palmovka (ulice Na �ertv�ch)
Palmovka	50 4.308	14 28.295		100	
Stran�ice, Svoj�ovice
Stran.,Otice,U viaduktu
Stran�ice, Otice
Stran�ice, rozc. Sklenka
Stran�ice,P�edbo�
Stran�ice,Ka�ovice
Stran�ice,rozc. Ka�ovice
Stran�ice,rozc. V�echromy
Stran�ice,n�dra��
Stra�nick�
Basilejsk� n�m�st�
Na Ml�nici
Na M���nk�ch
Na Ml�nku
Prun��ovsk�
�ivcov�
Kunratice
B�evnovsk�
B�evnovsk� kl�ter
D�ev�ice
D�ev�ice, cihelna
Jezerka
Ban�
Filmov� ateli�ry
Nu�ice
Nu�ice,bytovky
Nu�ice,�el. zast.
Nu�ice,Sokolsk�
Nu�ice,Na Krahulov�
Nu�ice,n�dra��
Nu�ice,Pod vinic�
Nu�ice,Prokopsk� n�ves
U D�lnice
Rembrandtova
Vr�ovick� n�m�st�
Plze�ka
Depo Zli��n
Depo Ka�erov
�eporyjsk� n�m�st�
�KD
Lipovick�
Husineck�
U Zvoni�ky
U Zvonu
Muzeum
Popovi�ky, rozc.
Popovi�ky, Chomutovice
Popovi�ky, Neb�enice
Trn�n� �jezd,rozc. I.
Topolov�
Limuzsk�	50 4.759	14 30.522		200	
U Tov�ren
Tupolevova	50 8.075	14 30.340		100
Nad P��vozem
Nad Park�nem
Nad Dvorem
Nad B�chovicemi
Nad Havlem
Nad Rybn�ky
Nad Mark�tou
Nad Z�vist�
Nad Habrovkou
Nad Klamovkou
Nad Jetelkou
Nad Malou Ohradou
Nad D�b�nem
Nad Primaskou
Nad Trojou
Koda�sk�
Na �lehli
Karlov
Karl�k
Karl�tejnsk�
Karlovy l�zn�
Karl�nsk� n�m�st�
Karlovo n�m�st�
U Hang�ru
Perlit
U Drahan�
H�jek
H�je	50 1.806	14 31.728		100	
Male�ick� n�m�st�
Male�ick�
Male�ick� tov�rna
Masarykovo n�dra��
Pod Hradem
Pod ��h�kem
Pod Mez�
Pod Kyjovem
Pod Rapidem
Pod Kesnerkou
Pod Oborou
Pod Kr�lovkou
Pod Vyhl�dkou
Pod Ry��nkou
Pod �afr�nkou
Pod H�jem
Pod T�ebe��nem
Pod Kroc�nkou
Pod Lipkami
Pod Vinic�
Pod �muk��kou	50 3.670	14 22.538		10	
Pod Zat��kou
Pod Peka�kou
Pod Hrachovkou
Pod T�borem	50 5.522	14 30.633		200	
Pod H�bitovem
Pod Konv��kou
Pod D�v�nem
Pod Jezerkou
Pod Karlovem
Zborov
Zborovsk�
Kelerka
Televizn� v�
Vele�
Vele�,M�rovice
Veletr�n�
Zelen� li�ka
Zelene�
Zelene�,Bezru�ova
Zelene�,U H�i�t�
Zelen�
Zelen� pruh
Zelen� louka
Zelen� domky
K�enice
Jilemnick�
�abov�esky
Neboz�zek
Flora
Florenc
Boleveck�
Dole�alova
Hole�kova
Koleje V�trn�k
Koleje Strahov
Koleje Ji�n� M�sto
Polesn�
Polerady, rozc.
Breitcetlova
Libock�
Odstavn� plocha Suchdol
Hrazansk�
Mraz�rny
Ke Xaverovu
Vestec,Safina
Vestec,��talka
Vestec,Obecn� ��ad
Vestec,U Klime��
Vestec,U Vod�rny
U Pek�ren
�estajovice,U �koly
�estajovice,Balk�n
�estlice,Rebo
�estlice,Rehau
�estlice
�estlice,Hypernova
�estlice,Global
U Waltrovky
Klikovka
�t�chovick�
Tusarova
M� Benice
V� B�chovice
Dvorce
Hn�vkovsk�ho
Vrbova
Leti�t� Ruzyn�
Dostihov�
Hostivice,B�ve
Hostivice,Palouky
Hostivice,Nouzov
Hostivice,Star� Litovice
Hostivice,n�dra��
Hostou�,U h�i�t�
Hostou�
Hostivice,Jene�ek
Hostivice,Sportovc�
Hostivice,Litovice
Hostivice,Stadion
Hostivice
Hostivice,Na P�sk�ch
Host�nsk�
Hostiva�sk� n�m�st�
Hostavice
Hostiva�sk�
Kostelec n.�.l.,Jevansk�
Kost.n.�.l.,Dobrovsk�ho
Kostelec n.�.l,Svatb�nsk�
Kostelec n.�.l,sanatorium
Kostelec n.�.l.,Na Skalce
Kostelec n.�.l.,Trativody
Kostelec n.�.l.,U k���ku
Kostelec n.�.l.,n�m.
Kostelec nad Labem,n�m�st�
Kostelec nad Labem,Na r��ku
Kostelec nad Labem,�el. st.
Kostelec u K���k�
Kostelec u K���k�,�kola
Kostrounek
Kosteleck�
Most Z�vodu m�ru
Most�rna
N�m�st� U Lva
N�m�st� J.Berana
N�m�st� W.Churchilla
N�m�st� Brat�� Synk�
N�m�st� Republiky
N�m�st� M�ru
Bubovice
Kav�� hory
Rajsk� zahrada	50 6.357	14 33.593	200
M� �eberov
Madlina
Radlick�
Je�ick�
��blick� h�bitov
Jedli�k�v �stav
Sedleck� p��voz
Ry��nka
Bo�islavka
Mo�ina,Trn�n� �jezd,rozc.
Mo�ina,odbo�ka lom
Mo�inka
Mo�ina,U p�chot�
Mo�ina
Modletice, rozc.
Modletice
Modletice, V h�rce
Modletice, Tvin
Modletice,Doubravice,rozc
Darwinova
B�lu�sk�
Jord�nsk�
Zbyslavsk�
Smaragdov�
Smi�ick�
Na Pl�cku
Na Pastvin�ch
Na Proutc�ch
Na Parcel�ch
Na Palouku
Na Pades�tn�ku
Na Pahorku
Na Pazderce
Na Pades�t�m
Za Pavilonem
Sparta
U Bel�rie
Spojovac�	50 5.553	14 29.857		100	
Star� Zli��n
Starodejvick�
Starochuchelsk�
Star� obec
Star� Prosek
Star� Spo�ilov
Star� n�m�st�
Star� Bohnice
Starom�stsk�
Star� Hloub�t�n
Kamenice,V�edobrovice
Kamenice,�ti��n
Kamenice, z�vod
Kamenice, Valnovka
Kamenice, L�dv�
Kamenice, Kukl�k
Kamenice,Skuhe�
Kamenice,Nov� Hospoda
Kamenice,Ole�ovice
Kamenice,Kulturn� d�m
Kamenice,U dvora
Kamenice,T�pt�n,U kozl�ho kamene
Kamenice,T�pt�n
Kamenn� P��voz, Draha
Kamenn� P��voz
Kamenn� P��voz, U k���ku
Kamenolom Zbraslav
Kamenick�
Trojsk�	50 7.045	14 25.900		100	
Roz�nova
Satalice
Ametystov�
Mnichovice, My�l�n
Mnichovice, hotel My�l�n
Mnichovice, po��rn� d�m
Mnichovice, U h�bitova
Mnichovice, n�m.
Cement�rna Radot�n
Svojetice, Na skalce
Svojetice
Svojetice, Na vyhl�dce
Tryskovick�
Koh-i-noor
Chmelnice
Ptice
Ptice,k�i�ovatka
D�dina
D�dinova
Ct�nice
Zl�chov
Slivenec
Sliveneck�
Sm�chovsk� n�dra��
Vltavsk� (Bubensk� ulice)
Vltavsk�
B�rtlova
Antala Sta�ka
Botanick� zahrada
Na vrstevnici
L��iva
N�sirovo n�m�st�
�stav mate�stv�
L�ze�ka
Lu�iny
Bo�anovice,rozc.
Mal� B�evnov
Kub�nsk� n�m�st�
Aero
K Dube�ku
V�clavka
V�clavsk� n�m�st�
Kbelsk� h�bitov
Kbely
Kbelsk�	50 6.240	14 31.880		10	
Bla�imsk�
Ohrobec,K�rov,K Jarovu
Ohrobec,K�rov
Ohrobec
Ohrobec,U �isteck�ch
Ohrobec,u rybn�ka
Ocel��sk�	50 6.066	14 29.121		100	
Fo�tova
Miro�ovice
Mirovick�
Na Havr�nce
Na Hranici
Na H�ebenech
Na Homoli
Na Hroud�
Za Horou	50 5.531	14 32.026		100	
Globus �ern� Most
Klobou�nick�
Okrouhlo
Okrouhlo,Zaho�any
Okrouhlick�
Pra�sk� �tvr�
Pra�sk� hrad
Pra�sk�ho povst�n�
Pra�sk� tr�nice
Horou�any,U rybn�ka
Horou�any
Horou�any,Horou��nky
Horom��ice
Horom��ice,V Lipk�ch
Habrov�
Horom��ick�
Drobn�
Sva�it�
U Vodojemu
Bryksova
Stroj�rensk�
Strossmayerovo n�m�st�
Dlouh� m�le
Dlouh� t��da
Hloub�t�nsk�
Hloub�t�n	50 6.303	14 32.206		100	
M� �jezd
Volha
V Sudech
�ampionov�
Dobrov�z,Obecn� ��ad
Dobrov�z
Dobro�ovick�
Dobronick�
Dobratick�	50 7.832	14 30.432		100
Kobrova
Tempo
U Jankovky
Netluky
P��stav Radot�n
P��stavi�t�
Drinopol
Z�les�
Kl�nec
Kl�nec,u h�i�t�
Kl�nec,�kola
���any,Olivovna,s�dli�t�
���any, Stra��n, rest.
���any, �tef�nikova
���any, 5. Kv�tna
���any, Pod n�dra��m
���any, Podhrad�
���any, Olivovna
���any, Pod lihovarem
���any, h�bitov
���any, cihelna
���any, Ku��
���any, U nemocnice
���any, pr�m. are�l
���any, K �el. st.
���any, Rychta	49 59.441	14 40.955		100
���any, Stra��n
���any, Industri�ln� z�na
���any, Ja�lovice
���any, Vod�r�dky
���any, U l�py
���any, n�dra��
���any, park A. �vehly
���any, U podjezdu
���any, Wolkerova
���any, n�m.
���any, U Mil�na
���any, Spojovac�, Z�
���any, z�vod DISK
���any,Pacov
���anova
Gener�la Janou�ka
Jene�,n�dra��
Jene�,Karlovarsk�
Jene�
Jener�lka
Dejvick�
Jen�tejn
Hadovit�
Hadovka
Kotl��ka (smy�ka)
Kotl��ka
Radonice, Ligasova
Radonice,Plus-Discount
Radonice
Radot�nsk�
Rado�ovick�
Rado�ovick� (smy�ka)
Ot�nsk�
Kaz�nsk�
B�leneck� n�m�st�
Rone�ova	50 6.433	14 33.955	30	10
Lidov� d�m
Vidoule
�idovsk� pece
Zli��n
V Rem�zku
Z�ti�sk�
Podolanka
Podolsk� vod�rna
M�stn� ��ad Mod�any
K Holyni
U Hostiva�sk�ho n�dra��
V Hork�ch
Noutonice
Budovatelsk�
Budovec
Kl��ov	50 6.906	14 30.872		100	
Kl��ovsk�
Kam�ck�
Jiviny
Fruta	50 2.585	14 34.950		100	
Hasova
Na Sk�le
Na S�dce
Na Smetance
Na Santince
Na Sklonku
Na Star� cest�
Na Str�i
Na Srpe�ku
Trutnovsk�	50 8.775	14 30.332		100
Za Sl�nskou silnic�
P�tihosty
Selsk�
Krausova	50 8.452	14 30.532		100	
Ke Smr�in�
Ke Schod�m
Ke Stadionu
Ke St�rce
Ve Studen�m
Zmrzl�k
Gar�e Kl��ov
Gar�e Dejvice
D�stojnick� domy
M�stek	50 4.964	14 25.353		100	
�isovice,rozc. k �el. st.
�isovice
�isovice,u �koly
�isovice,Bojov,Vi��ovka
�isovice,Bojov
�isovice,Bojov,�el. st.
�isovick�
D�v�� hrady
V�stavi�t�
Chrṻany,Scania-Label
Chrṻany,Protherm
Chrṻany
Chrṻany,Mezcest�
Tolst�ho
Jir�skova �tvr�	50 5.634	14 32.758		100	
Jir�skovo n�m�st�
Koso�,samota
Koso�
Koso�,rozc.
S�dli�t� Radot�n
S�dli�t� Zbraslav
S�dli�t� Skalka
S�dli�t� Homolka
S�dli�t� Zli��n
S�dli�t� Kobylisy
S�dli�t� �imice
S�dli�t� Pankr�c
S�dli�t� Lhotka
S�dli�t� Male�ice
S�dli�t� Jinonice
S�dli�t� Stod�lky
S�dli�t� Prosek
S�dli�t� Barrandov
S�dli�t� Spo�ilov
S�dli�t� Libu�
S�dli�t� P�snice
S�dli�t� Kr�
S�dli�t� Petrovice	50 2.081	14 33.762		100	
S�dli�t� H.M�cholupy
S�dli�t� Lehovec	50 6.124	14 32.902		100	
S�dli�t� Roho�n�k
S�dli�t� Na D�din�
S�dli�t� Novodvorsk�
S�dli�t� Bohnice
S�dli�t� Zahradn� M�sto
S�dli�t� �erven� vrch
S�dli�t� �epy
S�dli�t� ��blice
S�dli�t� Hloub�t�n	50 6.317	14 32.448		100	
S�dli�t� Mod�any
Mor��
Spr�va leti��
Spr�va soci�ln�ho zabezpe�en�
M� P�edn� Kopanina
U V�clava
U V�hy
Lysolaje
Str�n�
Vysok� �jezd,sokolovna
Vysok� �jezd
Vysok� �jezd,Kucha� I.
Vysokovsk�
# CHECKME!
Vyso�ansk�	50 6.608	15 3.145		100	
K Tuchom��ic�m
Mok�ansk�
U Kapli�ky
U Ka�tanu
Kaval�rka
Vy�lovka, hotel Praha
Vy�lovka
Barrandovsk�
Th�kurova
Jevany,�ernokosteleck�
Jevany, Bohumil
Jevany, Lesn� podnik
Jevany, Smr�iny
Jevany, Pen�ice
Jevany, Spojovac�
Jevany
Nevanova
Klamovka	50 4.176	14 22.745		10	
Chvaly
Balk�n	50 5.728	14 29.732		100	
Divadlo Na Fidlova�ce
Divadlo pod Palmovkou
Na Kov�rn�
Na Kl��ov�
Na Ko��ku
Na K�e�ku
Na Krejc�rku
Na Kn��ec�	50 4.123	14 24.247		10	
Plo�n�
Velk� Popovice,�kola
Velk� Popovice,Lojovice
Velk� Popovice,rozc. Kr�msk�
Velk� Popovice,Brtnice
Velk� Popovice,Todice
Velk� Popovice
Velk� Popovice,rozc. Brtnice
Velk� Le�ice
Velk� P��lepy
Velk� Ohrada
Velk� Chuchle
B�etislavka
Invalidovna
Ke Kate�ink�m
Ke Kr�i
Ke Kam�nce
Plyn�rna Satalice
Plyn�rna M�cholupy
Plyn�rna Michle
U Libu�sk� sokolovny
U Libu�sk�ho potoka
Nebu�ice
Ch�n�
Ch�n�,Pivovarsk� dv�r
Ch�nice,Pod Radnic�
Ch�nice
Ch�novsk�
Cibulka
Libu�
N�ro�n�
N�rodn� divadlo (n�b�e��)
N�rodn� divadlo
N�rodn� t��da
K H�j�m
U H�je
�atovka
K Chat�m
V Chaloupk�ch
V Chotejn�
Beton�rka
Letov
Metod�jova
P��voz Strnady
P��vorsk�
Litovick� potok
Litochlebsk� n�m�st�
# Ouch? Does not exist in old pid data I have...
Sazka Arena	50 6.273	14 29.593		100	
Nadrazi Liben	50 6.196	14 30.046		100	
Nove Namesti	50 1.903	14 35.927		100	
Ortenovo nam.	50 6.496	14 26.841		100
O.C.Cakovice	50 8.930	14 30.489		100
Vystaviste Letnany	50 7.710	14 30.755		100
Letnanska	50 7.422	14 30.501		100
Gercenova	50 3.026	14 32.799		100
