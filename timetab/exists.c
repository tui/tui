/*
 * Copyright 2002 Pavel Machek <pavel@ucw.cz>
 * Distribute under GPLv2
 */

/*
 * Goes: given remark in new format, decide, whether given connection
 * does exist or not
 */

#define DAYS (60*60*24)

/*
   Returns
   Y .. yes, exists
   N .. no, does not exist
   E .. internal error
   R .. out of range, can not tell
*/

static char exists_fast(char *buf, time_t today)
{
	struct tm date;
	int num, test;

	if (!buf)
		return 'Y';

	if (sscanf(buf, "%d-%d-%d %d", &date.tm_year, &date.tm_mon, &date.tm_mday, &num) != 4)
		return 'E';

	date.tm_mon--; date.tm_year-=1900;
	date.tm_hour = 0; date.tm_min = 0; date.tm_sec = 0; date.tm_isdst = 0;

	test  = (today - (mktime(&date))) / DAYS;

	if ((test < 0) || (test > num)) {
		printf("Day %d out of range \n", test );
		return 'R';
	}

	while (*buf != ' ')
		buf++;
	buf++;
	while (*buf != ' ')
		buf++;
	buf++;

	while (1) {
		int num, mask;
		sscanf(buf, "%d:%x", &num, &mask);
		if (test < num*7)
			return (1<<(test%7)) & mask ? 'Y' : 'N';
		test -= num*7;
		while (*buf != ' ')
			buf++;
		buf++;
	}

	return 'E';
}   

static char exists(char *buf, struct tm *tcas)
{
	return exists_fast(buf, mktime(tcas));
}   



