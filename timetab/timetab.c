/* -*- linux-c -*-
 * Connection finder, distribute under GPL version 2 or later
 * Copyright 2000, 2001, 2019 Pavel Machek <pavel@ucw.cz>
 * Copyright 2000 Martin Hejna <m.hejna@worldonline.cz>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#ifndef COMPATIBLE
#include <unistd.h>
#include <sys/mman.h>
#endif
#include <time.h>
#include <ctype.h>
#include <errno.h>
#include "timetab.h"

#define MEMORY_SIZE 	(1024*1024*1024)
#define MEMORY_START	(0x60000000)
static int memory_handle = -1;
#define GRP_CHAR	'+'
#define SIG "timetab-mem-image"
#undef LITE

#define MAX_REMARKS	255
#define REMARK_SIZE	14
#define MAX_IDCHANGE	100
#define MAX_RETRY	10

int pargc;
char **pargv;
char *table_file = NULL, *memory_file = "stdin.mem";
int starttimes[MAX_RETRY], cur_starttime, stt_index=0, timelimit = 0;

#include "exists.c"

/* 
 * Exists can return Yes, No, Error if we can't parse, Unknown if
 * nobody can decide (for situation like connections which exist when
 * there's enough snow).
 */

int connection_forbidden(char *vlak)
{
	int cykl;
        for (cykl=5;cykl<pargc;cykl++) {
		if (!strlen(pargv[cykl]))
			continue;
        	if (!strncmp(pargv[cykl], vlak, strlen(pargv[cykl])))
			return 1;
        }
        return 0;
}

typedef unsigned char uchar;

struct id {
	struct id *next;
	char *name;
        char *cond;	/* Condition which days the train goes */
	/* Hmm, if train starts at 23:00 and arrives at 1:00, and goes on fridays only, when does it go? */
        char *note;     /* Train note */
	char *company;	/* Company which owns the node */
	int _id;
        uchar remark;
	struct town *first_town;
} __attribute__((__packed__));

static int town_id;

struct node {
	struct node *next;
	struct town *to;
	struct id *id;
	union {
		struct { 				/* Data used by -p */
			int linenum1, linenum2;
		} prep;
		struct {				/* Data used by normal search */
			struct town *from;
			unsigned short length;
		} search;
	} x;
	short time1, time2;
	uchar remark;
} __attribute__((__packed__));

#define TIME1 time1
#define TIME2 time2

struct town {
	char *name;
	struct town *next;
	struct node *last_node;
	struct node *nodes;
	unsigned short _id;	/* it is possible to hit this limit, but we have test for that */
	struct node *by;	/* changed at runtime */
	int heapindex;		/* changed at runtime */
	unsigned short time;	/* changed at runtime */
	char final;		/* changed at runtime */
} __attribute__((__packed__));

#define HASHSIZE 60000
#define INF 65000
#define ANY (-99*60)

struct state {
	char version[20];
	struct town *townhash[HASHSIZE];
	struct id *ids;
	unsigned short numtowns;
	int numnodes;
	int id_id;
        int n_remarks;
        char remarkvec[MAX_REMARKS][REMARK_SIZE];
	char *signature;
} __attribute__((__packed__));

struct state *state;

int finalized = -1;

struct town **heap;

#define TIME(heapindex) heap[heapindex]->time
#define BACKLINK(heapindex) heap[heapindex]->heapindex = heapindex
#include "heap.c"

char *memory, *memory_start, *memory_end;

char *opts = "";
char delim = ' ';
int mode = 0;
#define LIST 1
#define PREPARE 2
#define IDLIST 3

int slow = 0, retry = 0, retry_num = 0, verbose = 0, dump = 0;
struct tm date; /* FIXME: this is broken. We can travel more than one day, etc. */
int compatible = 0, opt_memory_size = MEMORY_SIZE;

#define DTL_STATIONS	1
#define DTL_NOTE	2
#define DTL_COND	4
#define DTL_DEST        8
#define DTL_HEAD        16
#define DTL_TAIL        32
#define DTL_SUM        	64
#define DTL_COMPANY	128
int details = 0;


inline void *my_malloc(int size)
{
	void *res = memory;
	memory += size;
	if (memory>memory_end)
		die( "Allocated memory exhausted\n" );
	return res;
}

inline int in_bounds(void *p)
{
	if (p<(void *)memory_start || p>(void *)memory_end )
		die( "Memory file corrupted (interrupt or out of disk space during write?)\n");
        return 1;
}

inline char *my_strdup(char *s)
{
#ifndef LITE
	char *res = my_malloc(strlen(s)+1);
	strcpy(res, s);
	return res;
#else
	return NULL;
#endif
}

inline int hashfn(char *n)
{
	uchar *name = (void *) n;
	if (strlen(n)>3)
		return (name[0] + 64*name[1] + (64*64)*name[2] + (64*64*64)*name[3])%HASHSIZE;	
	else
		return 0;
}

struct town *get_town(char *name, int create)
{
	int hash = hashfn(name);
	struct town *res = state->townhash[hashfn(name)];

	while (res != NULL) {
		if (!strcmp(res->name, name))
			return res;
		res = res->next;
	}
	if (!create)
		return NULL;	

	res = my_malloc(sizeof(struct town));
	memset(res, 0, sizeof(struct town));
	res->name = my_malloc(strlen(name)+1);
	strcpy(res->name, name);
	res->next = state->townhash[hash];
	res->time = INF;
	res->_id = town_id++;
	if (town_id > 65535) {
		printf("way too many towns\n");
		exit(1);
	}
	state->townhash[hash] = res;
	state->numtowns++;
	return res;
}


int pstate = 0, curtime, curkm, lasttime, lastkm;
char oldremark[1024];

char store_remark(void)
{
	uchar i;

        for(i=0;i<state->n_remarks;i++)
		if (!strcmp(oldremark,state->remarkvec[i]))
			return i;
	if (state->n_remarks==MAX_REMARKS)
		die( "Remarks Limit Exceeded\n");
	if (strlen(oldremark)>REMARK_SIZE)
		die( "Remarks Size Exceeded\n");
	strncpy(state->remarkvec[state->n_remarks],oldremark,REMARK_SIZE);
	return state->n_remarks++;
}

int inline
my_atoi(char *s)
{
	int t1 = 0;
	char *t = s;
	while ((*s>='0') && (*s<='9'))
		t1 = t1*10 + (*s++)-'0';
	if (t == s)
		return -1;
	return t1;
}

int inline
decodetime(char *s)
{
	int t1 = -1, t2 = -1;

	if (*s=='+')
		return lasttime = lasttime+my_atoi(s+1);
	if (sscanf( s, "%d:%d", &t1, &t2 ) == 2)
		return lasttime = t1*60+t2;
	fprintf( stderr, "Could not find time on %s", s );
	exit(1);
}

int inline
decodekm(char *s)
{
	int t1 = -1;

	if (*s=='+')
		return lastkm = lastkm+my_atoi(s+1);
	if (sscanf( s, "%d", &t1 ) == 1)
		return lastkm = t1;
	fprintf( stderr, "Could not find km on %s", s );
	exit(1);
}

struct town *
decodeline(struct id *id, char *buf, struct town *prev)
{
	int i = 0;
	char *townname;
	int remindx = 0;
	char remark[10240];
	int from = -1, to = -1, km = -1;

	if (buf[i]!='\t')
		from = decodetime(buf+i);
	while (buf[i]!='\t') i++;
	i++;

	if (buf[i]!='\t')
		to = decodetime(buf+i);
	else { to = -1; pstate = 0; }
	while (buf[i]!='\t') i++;
	i++;
        while (buf[i]!='\t') {
		remark[remindx++]=buf[i++];
	}
	remark[remindx]=0;
	i++;

	if (from==-1) from=to;
	if (to==-1) to=from;
	if (from==-1) {
		fprintf( stderr, "\nNo time on line %s\n", buf );
		return NULL;
	}

	if (buf[i] != '-')
		km = decodekm(buf+i);
	while (buf[i]!='\t') i++;
	i++;
	townname = buf+i;

//	fprintf( stderr, "Got %s: %s %d:%d %d:%d\n", id->name, townname, from/60, from%60, to/60, to%60 );
	
	{
		struct town *this;

		this = get_town(townname, 1);
		if (prev) {
			struct node *node = my_malloc(sizeof(struct node));
			memset(node, 0, sizeof(struct node));

			node->next = prev->nodes;
			node->x.search.from = prev;
			node->to = this;
			node->TIME1 = curtime;
			node->TIME2 = from;
			node->x.search.length = km - curkm;
			node->id = id;
                        node->remark = store_remark();
                        prev->nodes = node;
			state->numnodes++;
		} else	id->first_town = this;
		curkm = km;
		curtime = to;
                strcpy(oldremark,remark);
		return this;
	}
}

void do_end_id(struct id *id, char **note, char **company)
{
        if (!id) return;
        if (*note) {
        	id->note=my_strdup(*note);
        	free(*note);
                *note=NULL;
        }
        if (*company) {
        	id->company=my_strdup(*company);
        	free(*company);
                *company=NULL;
	}
	id->remark = store_remark();
        state->id_id++;
}

void
readit(void)
{
	char buf[10240];
	char *note = NULL;
	char *company = NULL;
	struct id *id = NULL;
	FILE *f = table_file ?  fopen( table_file, "r" ) : stdin;
	int lineno = 0;
	struct town *prev = NULL;

	if (!f) {
		fprintf(stderr, "Error reading %s: %m\n", table_file);
		exit(1);
	}
	state = my_malloc(sizeof(struct state));
	state->numtowns = 0;
	state->numnodes = 0;
	strcpy(state->version, __DATE__);
	state->id_id = 0;
        state->n_remarks = 1;
        state->remarkvec[0][0]=0;
	while (1) {
		lineno++;
		if (!(lineno % 10000))
			fprintf( stderr, "Reading: %dK lines, %d towns, %d nodes, %.1fMB mem\r", lineno/1000, state->numtowns, state->numnodes, ((double) ((char *) memory - (char *)memory_start))/(1024*1024));
		if (fgets(buf, 10230, f)==NULL) break;
		if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
                if (buf[0]==0) continue;
		switch (*buf) {
		case '#':
                	do_end_id(id,&note,&company);
			lasttime = -9999;
			id = my_malloc(sizeof(struct id));
			id->name = my_malloc(strlen(buf+2)+1);
			id->next = state->ids;
			id->first_town = NULL;
			id->_id = state->id_id;
			state->ids = id;
			strcpy(id->name, buf+2);
			pstate = 1;
			curkm = 0;
			prev = NULL;
			continue;
		case 'D':
			id->cond = my_strdup(buf+1);
			pstate = 0;
			break;
		case 'G':
			if (!company) {
				company = malloc(strlen(buf+1)+1);
				strcpy(company,"");
			} else {
				company=realloc(company,strlen(company)+1+strlen(buf+1)+1);
				strcat(company,"\n");
			}
			strcat(company, buf+1);
			pstate = 0;
			break;
		case 'A': case 'B': case 'C': case 'R': case 'E': case 'F':
		case 'H': case 'I': case 'J': case 'K': case 'L': case 'M':
		case 'N': case 'O': case 'P': case 'Q': case 'S': case 'T':
		case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
		case '[':
			if (note==NULL) {
				note = malloc(strlen(buf)+1);
				strcpy(note,"");
			} else {
				note=realloc(note,strlen(note)+1+strlen(buf)+1);
				strcat(note,"\n");
			}
			strcat(note, buf);
			pstate = 0;
			break;
		case ';':	/* Remark */
			break;
		case '!':	/* Followed by keyword */
			{
				int y1, m1, d1, y2, m2, d2, i, j;
				if (sscanf(buf+1, "valid y%d m%d d%d .. y%d m%d d%d\n",
					   &y1, &m1, &d1, &y2, &m2, &d2) == 6) {
					continue;
				}

				if (sscanf(buf+1, "version %d.%d\n", &i, &j) == 2) {
					continue;
				}

			}
			break;
		case 0:
			break;
		default:
			if (pstate == 1)
				prev = decodeline(id, buf, prev);
			else
				fprintf( stderr, "Line in unexpected state: %s\n", buf );
		}
	}
	do_end_id(id,&note,&company);

	state->signature = my_strdup(SIG);
	if (!compatible) {
#ifndef COMPATIBLE
		fprintf( stderr, "Msync  :\r");	/* Msync is slow, but definitely needed on NFS. MS_SYNC works over NFS */
		msync((void *) MEMORY_START, MEMORY_SIZE, MS_SYNC);
#endif
	}
	fprintf( stderr, "Ready  : %d lines, %d towns, %d nodes, %.2fMB mem\n", lineno, state->numtowns, state->numnodes, ((double) ((char *) memory-(char *)memory_start))/(1024*1024));
	if (f!=stdin)
	        fclose(f);
}

/*
 * Heap utility functions
 */

void
printheap(void)
{
#if 0
	int i;
	printf("Heap currently:\n");
	for (i=1; i<15; i++) {
		if (!heap[i]) {
			printf("# ");
			continue;
		}
		printf( "%d:%02d %s ", heap[i]->time/60, heap[i]->time%60, heap[i]->name );
	}
	printf( "\n" );
#endif
}

static void
swapheap(int a, int b)
{
	struct town *x = heap[a];
	heap[a] = heap[b];
	heap[b] = x;
	
	heap[a]->heapindex = a;
	heap[b]->heapindex = b;
}


void
setupheap(void)
{
	int size;
	heapalloc = 2*state->numtowns + 5;
	size = sizeof(struct town *) * heapalloc;
/*	fprintf(stderr, "Creating heap...\n" );*/
	if (heap)
		free(heap);
	heap = mallocz(size);
	heapcount = 0;
}

void
insertheap(struct town *town)
{
	if (heapcount > heapalloc/2)
		die( "Sorry, I was lazy\n" );
	heap[++heapcount] = town;
	fixdown(heapcount);
}

/*
 * Search functions
 */

void
finalize(struct town *fin)
{
	struct node *node = fin->nodes;

	//printf("Finalizing... %s %d:%02d\n", fin->name, fin->time/60, fin->time%60);
	fin->final = 1;
	while (node) {
		//printf("  id %s %d %d\n", node->id->name, node->TIME1, node->TIME2);
		if (node->TIME1 == ANY) {
			if (!connection_forbidden(node->id->name))
				if (fin->time + (node->TIME2 - node->TIME1) < node->to->time) {
					node->to->by = node;
					node->to->time = fin->time + (node->TIME2 - node->TIME1);
					if (!slow) {
						if (node->to->heapindex)
							deleteheap(node->to->heapindex);
						insertheap(node->to);
						printheap();
					}
				}
			node = node->next;
			continue;
		}
		if (((!fin->by || node->id == fin->by->id) && node->TIME1 >= fin->time) /* I'm staying in same train */
		    || (node->TIME1 - 3 >= fin->time)) /* I'm soon enough on railway station */
			if (node->TIME2 < node->to->time)	/* And this one is better */
			if ((exists(node->id->cond, &date) != 'N') && !connection_forbidden(node->id->name))
				if (node->TIME1 < node->TIME2) {	/* No midnights for now */
//				printf( "We know how to get to %s\n", node->to->name );
					node->to->by = node;
					node->to->time = node->TIME2;
					if (!slow) {
						if (node->to->heapindex)
							deleteheap(node->to->heapindex);
						insertheap(node->to);
						printheap();
					}
				}
		node = node->next;
	}
}

struct town *
findmin(void)
{
	int min = INF;
	int i;
	struct town *res = NULL, *town;

	if (slow) {
		for (i=0; i<HASHSIZE; i++) {
			town = state->townhash[i];
			while (town) {
				if ((town->time < min) && (!town->final)) {
					min = town->time;
					res = town;
				}
				town = town->next;
			}
		}
		return res;
	} else {
		res = heap[1];
		deleteheap(1);
		printheap();
		return res;
	}
}

int lastrem(struct node *nd)
{
        struct id *myid=nd->id;
        struct node *pnode;

        pnode=nd->to->nodes;
	if (!pnode) {
		/* ../../timetab 'Říčany,Rychta' 27.1.2019 11:06 'Letiště' */
		printf("FIXME: something is confused\n");
		return 0;
	}
        while (pnode->next!=NULL && pnode->id!=myid) pnode=pnode->next;
	if (pnode->id!=myid)
		return myid->remark;
	return pnode->remark;
}

void idto(struct node *nd)
{
        struct id *myid=nd->id;
        struct node *pnode;
        int mytime=nd->TIME2;
        int loop=0;

        pnode=nd->to->nodes;
        
        while(1) {
		if (!pnode)
			return;
	        while (pnode->next!=NULL && pnode->id!=myid) pnode=pnode->next;
#ifndef LITE
		loop++;
                if (loop>500) {printf("# to: round trip\n");break;}
                if (pnode->id!=myid) {
                	printf("; to: %s (%d:%02d)\n",pnode->x.search.from->name,
                		mytime/60,mytime%60);break;
		}
#endif
                mytime=pnode->TIME2;
        	pnode=pnode->to->nodes;
        }
}

void prefix(char *pref,char *s)
{
#if 1
	puts(pref); puts(s);
#else
	int i1=0;
        char *p;
        char tmp[1024];

	/* This should prefix all lines of s with prefix pref.
	   IT was buggy and caused problems -> commented out.
	*/
        for(;;) {
	        strcpy(tmp,pref);
        	if ((p=strchr(s+i1,'\n'))==NULL) break;
	        strncat(tmp,s+i1,p-s-i1);
        	puts(tmp);
                i1=p-s+1;
	}
        strcat(tmp,s+i1);
        puts(tmp);
#endif
}

char *from_name, *to_name;
int from_len, to_len;

int found(struct town *from)
{
	if (to_len==-1) {
	       	if (!strcmp(from->name, to_name))
			return 1;
	        return 0;
        }
       	if (!strncmp(from->name, to_name, to_len))
		return 1;
        return 0;
}

int
exist_id(struct id* id, struct town *town, int time)
{
	struct node *node=town->nodes;

        while (node) {
        	if (node->id==id && node->TIME1>time) return 1;
                node=node->next;
	}
	return 0;
}

void new_route(struct town *from, struct town *to, struct id *id)
{
        struct node *from_node;
        struct node *to_node;
        
// We check whether the train goes from from_town to to_town
// and not vice versa.
        from_node=from->nodes;
        while (from_node->id!=id) from_node=from_node->next;
        to_node=to->nodes;
        while (to_node->id!=id) to_node=to_node->next;
        if (from_node->TIME1>to_node->TIME1) return;
        
        while (from!=to) {
        	from_node=from->nodes;
                while (from_node->id!=id) from_node=from_node->next;
                from->by=from_node;
                from=from->by->to;
	}
}

/*
 * simple_audit() checks whether is possible on some id change to change
 * right to the id, by which we have to go later. This decrements number
 * of id changes and we are likely to spend more time in stations and
 * less time in trains.
 */
 
void
simple_audit(struct town *from, struct town *to)
{
        struct town *idchange[MAX_IDCHANGE];
        int times[MAX_IDCHANGE];
        int numchange=0;
        int i,ii;

        times[0]=0;
        idchange[numchange++]=from;
        while (from->by) {
/* When the id changes */
        	if (from->by->to->by!=NULL && from->by->id!=from->by->to->by->id) {
/* we write down time and town of change. */
                        if (numchange==MAX_IDCHANGE) die("To many id changes!\nIncrease MAX_IDCHANGE or don`t use -a.\n");
                	times[numchange]=from->by->TIME2;
                        idchange[numchange++]=from->by->to;
                }
		from=from->by->to;
        }
	for(i=0;i<numchange;i++) {
        	for (ii=0;ii<i;ii++) {
/* If new id (i) goes also through previous town (ii)
   in time when we are on previous (ii) town */
			if (exist_id(idchange[i]->by->id,idchange[ii],times[ii]))
				new_route(idchange[ii],idchange[i],idchange[i]->by->id);
/* we will go by new id (i). */
                }
	}
}

void
printres(struct town *from, struct town *to)
{
        struct node *by2=NULL,*by3;
        int km=0,time;

	while (from->by) {
		//printf( "%s; ", from->name);
		km+=from->by->x.search.length;
		by3=by2;
		by2=from->by;       //vymeni by2 s from->by (za pomoci by3)
		from->by=by3;
		from=by2->x.search.from;
        }
        from->by=by2;

	if (strchr(opts,'a')) simple_audit(from,to);

	cur_starttime = from->by->TIME1;
        if (retry && !verbose) return;

        if ((details & DTL_HEAD)!=DTL_HEAD)
        	printf("======================================================================\n"
	               "Connection from:  %s\n             to:  %s\n"
 	               "======================================================================\n",
        	        from->name,to->name);
        if (from==to) return;
        if ((details & DTL_SUM)!=DTL_SUM)
	        printf("%d Kilometres         Travel time %d:%02d\n\n",km,
        		(to->time - from->by->TIME1)/60,(to->time - from->by->TIME1)%60);
        printf("      %c ",delim!=' ' ? delim: '-');
	while (from->by) {
        	if (details & DTL_STATIONS) {
	                printf("%2d:%02d %c%-3s %c%-30s",from->by->TIME1/60,
                        	from->by->TIME1%60,delim,
                                state->remarkvec[from->by->remark],delim,from->name);
                        km=0;
                        time=from->by->TIME1;
                        while (from->by->to->by!=NULL && from->by->id==from->by->to->by->id)
                        	{km+=from->by->x.search.length;from = from->by->to;}
                        km+=from->by->x.search.length;
		        printf(" %c(%dkm,%dkm/h)\n",delim,km,(km*60)/(from->by->TIME2 - time));
                } else {
	                printf("%2d:%02d %c%-3s %c%-30s %c(%dkm,%dkm/h)\n",
				      from->by->TIME1/60,from->by->TIME1%60,
	                        delim,state->remarkvec[from->by->remark],
                                delim,from->name,delim,from->by->x.search.length,
	                        (from->by->x.search.length*60)/(from->by->TIME2 - from->by->TIME1));
                }
                printf("%2d:%02d %c ",from->by->TIME2/60,from->by->TIME2%60, delim!=' ' ? delim : '-');

                if (from->by->to->by==NULL || from->by->id!=from->by->to->by->id) {
                        printf("      %c%-3s %c%-30s %c\n",
                        	delim,state->remarkvec[lastrem(from->by)],
                           delim,from->by->to->name,delim);

                        if (!(details & DTL_COND)) {
	                        printf("##%s %s", exists(from->by->id->cond, &date)!='Y' ? "!!!" : "",
        	                	from->by->id->name);
                	        if (from->by->id->cond)
 	                	       printf("  ---   %s",from->by->id->cond);
	                        printf("\n");
                        }
                        if (!(details & DTL_DEST)) idto(from->by);

                        if (!(details & DTL_COMPANY)) {
	                        if (from->by->id->company)
                                	prefix("[]  ",from->by->id->company);
			}
                        if (!(details & DTL_NOTE)) {
	                        if (from->by->id->note)
		                        printf("%s\n",from->by->id->note+1);
			}
        		if (from->by->to->by) printf("\n      %c ",delim!=' ' ? delim:'-');
                }

		from = from->by->to;
	}
        if ((details & DTL_TAIL)!=DTL_TAIL)
        	printf("----------------------------------------------------------------------\n");
}

void
ttres(struct town *from, struct town *to)
{
        struct node *by2=NULL,*by3;
        int km=0;

	while (from->by) {
		km+=from->by->x.search.length;
		by3=by2;
		by2=from->by;       //vymeni by2 s from->by (za pomoci by3)
		from->by=by3;
		from=by2->x.search.from;
        }
        from->by=by2;

	printf("\n");

	if (strchr(opts,'a')) simple_audit(from,to);

	cur_starttime = from->by->TIME1;
        if (retry && !verbose) return;

	printf("; ======================================================================\n");
	printf("!connection\n");
	printf("!from %s\n", from->name);
	printf("!to %s\n", to->name);
	printf("!date %04d-%02d-%02d %d:%02d\n", date.tm_year+1900, date.tm_mon+1, date.tm_mday, starttimes[0]/60, starttimes[0]%60);
	printf("; %d Kilometres         Travel time %d:%02d\n\n",km,
	       (to->time - from->by->TIME1)/60,(to->time - from->by->TIME1)%60);

        if (from==to) return;
	printf("# %s\n", from->by->id->name);
	printf("\t");
	km = 0;
	while (from->by) {
		printf("%2d:%02d\t%-3s#\t+%d\t%-30s\n",
		       from->by->TIME1/60,from->by->TIME1%60,
		       state->remarkvec[from->by->remark],
		       km,
		       from->name );
		km = from->by->x.search.length;

                printf("%2d:%02d\t",from->by->TIME2/60,from->by->TIME2%60);

                if (from->by->to->by==NULL || from->by->id!=from->by->to->by->id) {
                        printf("\t%-3s#\t+%d\t%-30s\n",
			       state->remarkvec[lastrem(from->by)], km,
			       from->by->to->name);
			km = 0;

			if (exists(from->by->id->cond, &date)!='Y')
				printf("Sunsure\n");
			idto(from->by);
			if (from->by->id->company)
				printf("G%s\n",from->by->id->company);
			if (from->by->id->cond)
				printf("R%s\n",from->by->id->cond);
			if (from->by->id->note)
				printf("%s\n",from->by->id->note);

			if (from->by->to->by) {
				printf("# %s\n", from->by->to->by->id->name);	/* There must be hash here */
				printf("\t");
			}
                }

		from = from->by->to;
	}
        if ((details & DTL_TAIL)!=DTL_TAIL)
        	printf("; ----------------------------------------------------------------------\n");
	printf("!endconnection\n");
}

void
ttres_id(struct id *id)
{
        struct id *myid=id;
        struct node *pnode = id->first_town->nodes, *lastnode = NULL;
        int loop=0;

	printf("# %s\n", id->name);
        while(1) {
	        while (pnode->next!=NULL && pnode->id!=myid) pnode=pnode->next;
#ifndef LITE
		loop++;
                if (loop>500) {printf("# to: round trip\n");break;}
		if (lastnode)
			printf("%2d:%02d", lastnode->time2/60, lastnode->time2%60);
                if (pnode->id==myid) {
			printf("\t%2d:%02d\t%s%s\t%s\n",
			       pnode->time1/60, pnode->time1%60,
			       pnode->x.search.from->heapindex == -1 ? "#" : "",
			       "",
			       pnode->x.search.from->name);
		} else break;
#endif
		lastnode=pnode;
        	pnode=pnode->to->nodes;
        }
	printf("\t\t%s%s\t%s\n", pnode->to->heapindex == -1 ? "#" : "", "", lastnode->to->name);
	
}

void
ttres2(struct town *from, struct town *to)
{
        struct node *by2=NULL,*by3;
        int km=0;

	while (from->by) {
		from->heapindex = -1;
		from->by->x.search.from->heapindex = -1;
		km+=from->by->x.search.length;
		by3=by2;
		by2=from->by;       //vymeni by2 s from->by (za pomoci by3)
		from->by=by3;
		from=by2->x.search.from;
        }
        from->by=by2;

	printf("\n");

	if (strchr(opts,'a')) simple_audit(from,to);

	cur_starttime = from->by->TIME1;
        if (retry && !verbose) return;

	printf("; ======================================================================\n");
	printf("!connection\n");
	printf("!from %s\n", from->name);
	printf("!to %s\n", to->name);
	printf("!date %04d-%02d-%02d %d:%02d\n", date.tm_year+1900, date.tm_mon+1, date.tm_mday, starttimes[0]/60, starttimes[0]%60);
	printf("; %d Kilometres         Travel time %d:%02d\n\n",km,
	       (to->time - from->by->TIME1)/60,(to->time - from->by->TIME1)%60);

        if (from==to) return;
	km = 0;
	ttres_id(from->by->id);
	while (from->by) {
		if (!from->by->to->by)
			break;
                if (from->by->id!=from->by->to->by->id)
			ttres_id(from->by->to->by->id);
		from = from->by->to;
	}
        if ((details & DTL_TAIL)!=DTL_TAIL)
        	printf("; ----------------------------------------------------------------------\n");
	printf("!endconnection\n");
}


void
dumpres(struct town *from, struct town *to)
{
        struct node *by2=NULL,*by3;
        int km=0;

	while (from->by) {
		printf("%02d:%02d\t%s\t%-23s\t%02d:%02d\t%s\t%-23s\t%3d\t%2d\t%s%s\n",
		       from->by->time1/60, from->by->time1%60, "", from->by->x.search.from->name, 
		       from->by->time2/60, from->by->time2%60, "", from->name, 
		       from->by->x.search.length, 0, exists(from->by->id->cond, &date)!='Y' ? "!" : "", from->by->id->name);
        	km+=from->by->x.search.length;
        	by3=by2;
        	by2=from->by;       //vymeni by2 s from->by (za pomoci by3)
                from->by=by3;
                from=by2->x.search.from;
        }
	return;
}

int
search(void)
{
	int res;
        struct town *from, *to;

	finalized = -1;
	while ((from = findmin())) {
		finalized++;
		if ((timelimit) && (from->time) > timelimit) {
//			fprintf( stderr, "\nImpossible within limit of %d:%02d.\n", timelimit/60, timelimit%60 );
			retry=0;
                        return 0;
		}
		if (!(finalized % 100))
			fprintf( stderr, "Searching %2d:%02d %35s... (%d, %d%% finalized)\r", from->time/60, from->time%60, from->name, finalized, (100*finalized)/state->numtowns );
		if (found(from))
                	break;
		finalize(from);
	}
        to=from;
	if (!from) {
		fprintf( stderr, "Done with %2d:%02d %35s... (%d, %d%% finalized)\r", 0, 0, "", finalized, (100*finalized)/state->numtowns );
		printf( "No connection found\n" );
		exit(0);
	}
	res = to->time;
	fprintf( stderr, "Done with %2d:%02d %35s... (%d, %d%% finalized)\n", from->time/60, from->time%60, from->name, finalized, (100*finalized)/state->numtowns );
	if (!timelimit)
		timelimit = res;

	switch (dump) {
	case 3: ttres2(from, to); break;
	case 2: ttres(from, to); break;
	case 1: dumpres(from, to); break;
	case 0: printres(from, to); break;
	}
        return res;
}

void 
usage(void)
{
	printf( "Usage: town_from day.month.year hour:minute town_to [trains to exclude] \n" );
	exit(4);
}

void
townlist(void)
{
	int i;
	struct town *town;
	if (strchr(opts, 'v')) printf("%d\n", state->numtowns);
	for (i=0; i<HASHSIZE; i++) {
		town = state->townhash[i];
		while (town) {
			if (!strchr(opts, 'v'))
				printf( "%s\n", town->name );
			else
				if (strchr(opts, 'V'))
					printf( "%s\t%d\n", town->name, town->_id );
				else
					printf( "%d\t%s\n", town->_id, town->name );
			town = town->next;
		}
	}
}

/* Currently not used; may be used instead of map_private(): reduces memory
   requirements but slows things down.
 */

static void
map_private(void)
{
#ifndef COMPATIBLE
	memory = memory_start = (char *) MEMORY_START;
	memory_end = memory + MEMORY_SIZE;
	if (mmap(memory, MEMORY_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, memory_handle, 0)== (void *) ~0)
		die( "Could not allocate memory: %m\n" );
	state = (struct state *) memory;
#endif
}

static void
cleartowns(void)
{
	int i;
	struct town *town;
	if (compatible) {
		for (i=0; i<HASHSIZE; i++) {
			fprintf(stderr, "Clearing towns... (%d/%d)\r", i, HASHSIZE);
			town = state->townhash[i];
			while (town) {
				town->by = NULL;
				town->heapindex = 0;
				town->time = 0;
				town->final = 0;
				town = town->next;
			}
		}
	} else {
#ifndef COMPATIBLE
		fprintf(stderr, "Unmap  :\r");
		munmap(memory, MEMORY_SIZE);
		map_private();
#endif
	}
	fprintf(stderr, "\n");
}

void
townmark_old(int time)
{
	int i;
	struct town *town;

	fprintf(stderr, "Too short name specified, using very slow method.\n" );
	for (i=0; i<HASHSIZE; i++) {
		town = state->townhash[i];
		while (town) {
			if (!strncmp(from_name, town->name, from_len)) {
				town->time = time;
				insertheap(town);
			}
			town = town->next;
		}
	}
}

void
townmark(int time)
{
	struct town *town;

	if (from_len < 3)
		return townmark_old(time);

	town = state->townhash[hashfn(from_name)];
	while (town) {
		if (!strncmp(from_name, town->name, from_len)) {
			town->time = time;
			insertheap(town);
		}
		town = town->next;
	}
}

static int linenum = 0;

void
prepare_town(int now, struct town *town, int mode)
{
	struct node *node = town->nodes;
	while(node) {
		if (mode && node->TIME1 == now) {
			node->x.prep.linenum1 = linenum++;
#undef DEBUG
#ifdef DEBUG
			printf( "%d:%d event %d: *-> to %s at %d:%d by %s\n", now/60, now%60, node->x.prep.linenum1, 
				node->to->name, node->TIME2/60, node->TIME2%60, node->id->name );
#else
			printf( ">%d\t%d\t%d\t%d\n", now, town->_id, node->id->_id, town->last_node ? town->last_node->x.prep.linenum2 : -1);
#endif

		}
		if (!mode && node->TIME2 == now) {
			node->x.prep.linenum2 = linenum++;
#ifdef DEBUG
			printf( "%d:%d event %d: ->* into %s from event %d prev %d\n", now/60, now%60, node->x.prep.linenum2, node->to->name, node->x.prep.linenum1, node->to->last_node ? node->to->last_node->x.prep.linenum2 : -1 );
#else
			printf( "<%d\t%d\t%d\t%d\n",
				now, node->to->_id, node->x.prep.linenum1, node->to->last_node ? node->to->last_node->x.prep.linenum2 : -1 );
#endif
			node->to->last_node = node;
		}
		node = node->next;
	}
}

void
prepare_clear(void)
{
	int i;
	struct town *town;
	fprintf(stderr, "Clearing linenumbers\n");
	for (i=0; i<HASHSIZE; i++) {
		town = state->townhash[i];
		while (town) {
			struct node *node = town->nodes;
			while(node) {
				node->x.prep.linenum1 = 0;
				node->x.prep.linenum2 = 0;
				node = node->next;
			}
			town = town->next;
		}
	}
}

void
prepare(void)
{
	int now;
	fprintf(stderr, "Size of towns is %dKB, size of nodes is %dKB, size of ids is %dKB\n",
		sizeof(struct town) * state->numtowns / 1024,
		sizeof(struct node) * state->numnodes / 1024,
		sizeof(struct id) * state->id_id / 1024);
	prepare_clear();
	for (now=0; now<(60*24); now++) {
		fprintf(stderr, "%2d:%02d, %d lines generated\r", now/60, now%60, linenum);
		{
			int i, mode;
			struct town *town;
			for (mode=0; mode<2; mode++)
			for (i=0; i<HASHSIZE; i++) {
				town = state->townhash[i];
				while (town) {
					prepare_town(now, town, mode);
					town=town->next;
				}
			}
		}
	}
}

void
idlist(void)
{
	struct id *id = state->ids;
	printf("%d\n", state->id_id);
	while (id) {
		char *cond, *note;
		cond = id->cond ? id->cond : "";
		if (strchr(cond, '\n'))
			cond = "multiline";
		note = id->note ? id->note : "";
		if (strchr(note, '\n'))
			note = "multiline";

		if (!strchr(opts,'D'))
			printf("%d\t%s\t%s\t%s\n", id->_id, id->name, cond, note);
		else	printf("%s\n", id->name);
		id = id->next;
	}
}

void
setup_memory_file(void)
{
#ifndef COMPATIBLE
		memory_handle = open(memory_file, O_RDWR);
		if (memory_handle!=-1) {
			fprintf( stderr, "Memory image already present.\n" );
			map_private();
			if (!in_bounds(state->version) || strcmp(state->version, __DATE__))
				fprintf( stderr, "Warning, .mem file created by another version!\n" );
		} else {
			fprintf( stderr, "Creating empty file for memory image.\n" );
			memory_handle = open(memory_file, O_RDWR | O_CREAT, 0644);
#if 1
	                ftruncate(memory_handle, MEMORY_SIZE);	/* Provokes kernel bug on linux 2.4.0-test8 */
#else
			{
			char buf[1024];
			sprintf(buf, "head -c %d < /dev/zero > %s", MEMORY_SIZE, memory_file );
			system(buf);
			}
#endif
			memory = memory_start =  (char *) MEMORY_START;
			memory_end = memory + MEMORY_SIZE;
			if (mmap(memory, MEMORY_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, memory_handle, 0)== (void *) ~0)
				die( "Could not allocate memory: %m\n" );
			readit();
	                ftruncate(memory_handle,memory - (char *)MEMORY_START);
			if ((lseek(memory_handle,0,SEEK_END) == -1) || (5 != write(memory_handle, "MAGIC", 5)))
				die( "Could not write magic label: %m (delete .mem file!)\n" );
				
			fprintf( stderr, "Memory image now created.\n" );
			map_private();
		}
#endif
}

void
setup_memory(void)
{
	if (compatible) {
		memory = memory_start = malloc(opt_memory_size);
		if (!memory)
			die( "Not enough memory in system?" );
		
		memory_end = memory + MEMORY_SIZE;
		readit();
	} else	setup_memory_file();
}

int
main(int argc, char *argv[])
{
	if (argc < 2)
		usage();
	if (*argv[1] == '-') {
		opts = argv[1]+1;
		argv++;
                argc--;
	}
	if (strchr(opts,'f')) delim='\t';
	if (strchr(opts,'s')) slow = 1;
	if (strchr(opts,'l')) mode = LIST;
	if (strchr(opts,'v')) verbose=1;
	if (strchr(opts,'u')) dump = 1;
	if (strchr(opts,'U')) dump = 2;
	if (strchr(opts,'T')) dump = 3;
	if (strchr(opts,'i')) mode = IDLIST;
	if (strchr(opts,'p')) mode = PREPARE;
	if (strchr(opts,'b')) { table_file = "bus.tt"; memory_file = "bus.mem"; }
	if (strchr(opts,'t')) { table_file = "vlak.tt"; memory_file = "vlak.mem"; }
	if (strchr(opts,'x')) { table_file = "xyzzy.tt"; memory_file = "xyzzy.mem"; }
	if (strchr(opts,'.')) { table_file = NULL; memory_file = "stdin.mem"; }
	if (strchr(opts,'c')) compatible = 1;
	if (strchr(opts,'C')) { compatible = 1; opt_memory_size = atoi(argv[1])*1024; fprintf(stderr, "Selected mem size %d\n", opt_memory_size); argv++; argc--; }
#ifdef COMPATIBLE
	compatible = 1;
#endif


	if (strchr(opts,'d')) sscanf(strchr(opts,'d')+1,"%d",&details);
	if (strchr(opts,'r')) {
        	sscanf(strchr(opts,'r')+1,"%d",&retry_num);
                if (retry_num==0) retry_num=1;
                if (retry_num>=MAX_RETRY)
                	die("Sorry, at maximum %d level is allowed. Increase MAX_RETRY.\n",MAX_RETRY-1);
                retry=1;
	}

	setup_memory();
	fprintf(stderr, "Checking signature: ");
	if (!in_bounds(state->signature) || strcmp(state->signature, SIG))
		die( "Memory file corrupted (interrupt or out of disk space during write?)\n");
	fprintf(stderr, "okay\n");
	switch(mode) {
	case PREPARE:
		prepare();
		exit(0);
	case LIST:
		townlist();
		exit(0);
	case IDLIST:
		idlist();
		exit(0);
	}
	if (argc < 5)
		usage();
	{
		int time1, time2;
		struct town *from, *to;

		from_name = argv[1];
		to_name = argv[4];

		fprintf(stderr, "Getting towns...\n" );
		from = get_town(from_name, 0);
		to = get_town(to_name, 0);
                
                if (strchr(from_name, GRP_CHAR)) from_len=strchr(from_name,GRP_CHAR)-from_name;
                	else from_len=-1;
                if (strchr(to_name, GRP_CHAR)) to_len=strchr(to_name,GRP_CHAR)-to_name;
                	else to_len=-1;

		if (!from && from_len==-1) {
			fprintf(stderr, "Sorry, town '%s' does not exist.\n", from_name);
                        return 2;
		}
		if (!to && to_len==-1) {
			fprintf(stderr, "Sorry, town '%s' does not exist.\n", to_name);
                        return 2;
		}

        	sscanf(argv[2], "%d.%d.%d", &date.tm_mday, &date.tm_mon, &date.tm_year);
        	date.tm_mon--; date.tm_year-=1900;
		date.tm_hour = 12; date.tm_min = 0; date.tm_sec = 0; date.tm_isdst = 0;
        	mktime(&date);
        	if (date.tm_wday==0) date.tm_wday=7;
                if (sscanf(argv[3], "%d:%d", &time1, &time2)!=2) {
			printf( "Sorry, invalid time '%s' specified.\n", argv[3]);
			return 3;
		}
                pargc=argc; pargv=argv;
		starttimes[0] = time1*60+time2;
		if (strchr(opts, 'I')) starttimes[0]++;
		while (1) {
			setupheap();
			if (from_len!=-1) townmark(starttimes[stt_index]);
                	else { from->time=starttimes[stt_index]; insertheap(from); }

                        cur_starttime=starttimes[stt_index];
			printheap();
			//printf("Starting search...\n");
                        search();
			if (retry==0) {
                                if (retry_num==0) break;
                       		if (++stt_index==retry_num+1) stt_index=0;
                                while (starttimes[stt_index]==0)
                        		if (++stt_index==retry_num+1) stt_index=0;
                                retry_num=0;
				cleartowns();
                                continue;
                        }

			cleartowns();

			if (starttimes[stt_index]>cur_starttime) {
				/* simple_audit can lower cur_starttime
				 * so we repeat last search and exit
				 */
                                retry_num=0;retry=0;
                                continue;
                        }

                        if (++stt_index==retry_num+1) stt_index=0;
                        starttimes[stt_index]=cur_starttime+1;
			fprintf(stderr, "Trying time %2d:%02d\n", cur_starttime/60, cur_starttime%60);
		}
	}
	return 0;
}

