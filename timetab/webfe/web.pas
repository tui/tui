uses linux;

{
	Copyright 2001 Marian Cerny <cerny@spnv.sk>
	Copyright 2002 Pavel Machek <pavel@ucw.cz>
	Licensed under the GNU General Public License version 2 or higher
	(see http://www.gnu.org/licenses/gpl.html)

	see README on http://www.spnv.sk/~cerny/study/mffuk/webfe/
}

const
	CGI_NAME = 'search.cgi';
	TIMETAB = './timetab_html'; {where the timetab program is located}
	DATABASE = 'train.tt'; {database filename}
	TAB = #9; {delimiter in database}
	NUM_OF_DECIDE_CHARS = 4; {depth of the tree in Pdecide_char structure for representing town names in memory}
	TOWN_MAX_LENGTH = 37; {train.tt has 37}
	NUM_OF_TRAINS = 6;
	TRAIN: array[1..NUM_OF_TRAINS] of Pchar = ('Os', 'Sp', 'R', 'Ex', 'IC', 'EC'); {trains to exclude}

	MESSAGE_DOCUMENT_TITLE = 'Train connection search';
	MESSAGE_FROM = 'From:';
	MESSAGE_TO = 'To:';
	MESSAGE_DATE = 'Date:';
	MESSAGE_TIME = 'Time:';
	MESSAGE_TOMORROW = 'tomorrow';
	MESSAGE_I_DONT_WANT_TO_GO_WITH = 'I do not want to use:';
	MESSAGE_SEARCH = 'Search';
	MESSAGE_SWAP_FROM_TO = 'from<->to';
	MESSAGE_FIND_NEXT = 'Next';
	MESSAGE_OBJECT_NOT_FOUND = 'object not found';
	MESSAGE_TOWN_NOT_FOUND = 'timetab: Town not found';
	MESSAGE_TIME_ERROR = 'timetab: Time in wrong format';
	MESSAGE_TIMETAB_ERROR = 'timetab: error executing';
	MESSAGE_UNKNOWN_ERROR = 'timetab: unknown error';
	MESSAGE_NO_CONNECTION_FOUND = 'No connection found';

{--- error output procedure --------------------------------------------------}

procedure write_error(s: string);
begin
	write('<FONT COLOR=red>', s, '</FONT>');
end;

{--- items -------------------------------------------------------------------}

type
	Pitem = ^Titem;
	Titem = record {fields from from are stored here}
		name: string;
		value: string;
		next: Pitem;
	end;

const
	first_item: Pitem = nil;

procedure new_item(name, value: string);
var
	P: Pitem;
begin
	new(P);
	P^.next:= first_item;
	P^.name:= name;
	P^.value:= value;
	first_item:= P;
end;

function get_item(name: string): string;
{searches in items for field name and returns value of the field or '' if field doesn't exist}
var
	P: Pitem;
begin
	get_item:= '';
	P:= first_item;
	while P <> nil do
		if P^.name = name then begin
			get_item:= P^.value;
			break;
		end else
			P:= P^.next;
end;

procedure change_item(name, newvalue: string);
var
	P: Pitem;
begin
	P:= first_item;
	while P <> nil do
		if P^.name = name then begin
			P^.value:= newvalue;
			exit; {job done}
		end else
			P:= P^.next;
	new_item(name, newvalue); {not found - create}
end;

procedure free_items;
{releases items from memory}
var
	P: Pitem;
begin
	while first_item <> nil do begin
		P:= first_item^.next;
		dispose(first_item);
		first_item:= P;
	end;
end;

procedure process_input;
{reads standard input (form fields) and sets the item variables}
var
	ch: char;

	function readch: char;
	begin
		read(ch);
		readch:= ch;
	end;

	function decode(ch: char): char;
	{changes '+' to space and '%XX' to char}

		function hextoint(ch: char): integer;
		begin
			if ch in ['A'..'F'] then
				hextoint:= ord(ch) - ord('A') + 10
			else if ch in ['a'..'f'] then
				hextoint:= ord(ch) - ord('a') + 10
			else if ch in ['0'..'9'] then
				hextoint:= ord(ch) - ord('0')
			else begin {error - this should never happen}
				write_error('error decoding %XX to char');
				halt;
			end;
		end;

	var
		i: integer;
	begin
		if ch = '+' then
			decode:= ' '
		else if ch='%' then begin
			i:= hextoint(readch) * 16;
			decode:= char(i + hextoint(readch));
		end else
			decode:= ch;
	end;

var
	name: string;
	value: string;
begin
	while not eof do begin
		name:= '';
		while readch <> '=' do {reads name}
			name:= name + decode(ch);
		value:= '';
		while (not eof) and (readch <> '&') do {reads value}
			value:= value + decode(ch);
		new_item(name, value);
	end;
end;

procedure set_date_and_time_items;
{sets date and time items to actual time and date; tomorrow to tomorrow's date}

	function inttostr(cislo: integer): string;
	var
		s: string;
	begin
		str(cislo, s);
		inttostr:= s;
	end;

const
	daysinmonth: array[1..12] of integer =
		(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var
	year, month, day: word;
	hour, minute, second: word;
begin
	getdate(year, month, day);
	new_item('date', inttostr(day) + '.' + inttostr(month) + '.'
	                 + inttostr(year));
	inc(day); {what date will it be tomorrow?}
	if day > daysinmonth[month] then begin
		if (year mod 4 = 0) and ((year mod 100 <> 0)
		   or ((year mod 100 = 0) and (year mod 400 = 0))) then
			daysinmonth[2]:= 29; {leap year}
		day:= 1;
		inc(month);
		if month > 12 then begin
			month:= 1;
			inc(year);
		end;
	end;
	new_item('tomorrow', inttostr(day) + '.' + inttostr(month) + '.'
	                     + inttostr(year));

	gettime(hour, minute, second);
	if minute < 10 then
		new_item('time', inttostr(hour) + ':0' + inttostr(minute))
	else
		new_item('time', inttostr(hour) + ':' + inttostr(minute));
end;

{--- table - conversion to non-accent ----------------------------------------}

const
	no_accent: array[char] of char = (
		{converts from ISO 8859-2 to ASCII + upercase to lowercase}
		#000, #001, #002, #003, #004, #005, #006, #007,
		#008, #009, #010, #011, #012, #013, #014, #015,
		#016, #017, #018, #019, #020, #021, #022, #023,
		#024, #025, #026, #027, #028, #029, #030, #031,
		#032, #033, #034, #035, #036, #037, #038, #039,
		#040, #041, #042, #043, #044, #045, #046, #047,
		#048, #049, #050, #051, #052, #053, #054, #055,
		#056, #057, #058, #059, #060, #061, #062, #063,
		#064,  'a',  'b',  'c',  'd',  'e',  'f',  'g',
		 'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
		 'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
		 'x',  'y',  'z', #091, #092, #093, #094, #095,
		#096, #097, #098, #099, #100, #101, #102, #103,
		#104, #105, #106, #107, #108, #109, #110, #111,
		#112, #113, #114, #115, #116, #117, #118, #119,
		#120, #121, #122, #123, #124, #125, #126, #127,
		#128, #129, #130, #131, #132, #133, #134, #135,
		#136, #137, #138, #139, #140, #141, #142, #143,
		#144, #145, #146, #147, #148, #149, #150, #151,
		#152, #153, #154, #155, #156, #157, #158, #159,
		#160,  'a', #162,  'l', #164,  'l',  's', #167,
		{     \c A          \L        \v L   \'S      }
		#168,  's',  's',  't',  'z', #173,  'z',  'z',
		{     \v S  \c S  \v T   \'Z        \v Z   \.Z}
		#176,  'a', #178,  'l', #180,  'l',  's', #183,
		{     \c a          \l        \v l   \'s      }
		#184,  's',  's',  't',  'z', #189,  'z',  'z',
		{     \v s  \c s  \v t   \'z        \v z   \.z}
		 'r',  'a',  'a',  'a',  'a',  'l',  'c',  'c',
		{\'R   \'A   \^A  \v A   \"A   \'L   \'C  \c C}
		 'c',  'e',  'e',  'e',  'e',  'i',  'i',  'd',
		{\v C  \'E  \c E   \"E  \v E   \'I   \^I  \v D}
		 'd', #209,  'n',  'o',  'o',  'o',  'o', #215,
		{ ??        \v N   \'O   '^O  \H O   \"O      }
		 'r',  'u',  'u',  'u',  'u',  'y',  't',  's',
		{\v R        \'U  \H U   \"U   \'Y  \c T  \ss }
		{   \accent23 U                               }
		 'r',  'a',  'a',  'a',  'a',  'l',  'c',  'c',
		{\'r   \'a   \^a  \v a   \"a   \'l   \'c  \c c}
		 'c',  'e',  'e',  'e',  'e',  'i',  'i',  'd',
		{\v c  \'e  \c e   \"e  \v e  \'\i  \^\i  \v d}
		 'd',  'n',  'n',  'o',  'o',  'o',  'o', #247,
		{ ??   \'n  \v n   \'o   \^o  \H o   \"o      }
		 'r',  'u',  'u',  'u',  'u',  'y',  't', #255
		{\v r        \'u  \H u   \"u   \'y  \c t      }
		{   \accent23 u                               }
	);

function lowcase(s: string): string;
{converts string to lowercase + iso8859-2 to ascii}
var
	ret: string;
	i: integer;
begin
	ret:= '';
	for i:= 1 to length(s) do
		ret:= ret + no_accent[s[i]];
	lowcase:= ret;
end;

{--- towns -------------------------------------------------------------------}

const
	char_begin = 0;
	char_end = 28;
		{ 0 shows on Pstring - if length(town) < NUM_OF_DECIDE_CHARS }
		{ 1 = ' ' }
		{ 2 = any other char }
		{ 3..28 = 'a'..'z' }
	town_list_file = 'towns.dat';

type
	Pdecide_char = ^Tdecide_char;
	Tdecide_char = array[char_begin..char_end] of pointer;
	{ Tdecide_char stores an array of pointers to next Tdecide_char.
	  The NUM_OF_DECIDE_CHARSth Tdecide_char points to Tstring, where
	  a list of towns, begining with these NUM_OF_DECIDE_CHARS signs,
	  is stored. }

	Pstring = ^Tstring;
	Tstring = record {used to store town names}
		value: string[TOWN_MAX_LENGTH];
		next: Pstring;
	end;

var
	first_char: Pdecide_char;

procedure clear_Tdecide_char(var T: Tdecide_char);
{sets all pointers of Tdecide_char to nil}
var
	i: integer;
begin
	for i:= char_begin to char_end do
		T[i]:= nil;
end;

procedure initialize_town_structure;
begin
	new(first_char);
	clear_Tdecide_char(first_char^);
end;

function my_ord(ch: char): integer;
{converts char to integer (' ' to 1, 'a'..'z' to 3..28, other to 2)}
begin
	case ch of
		' ': my_ord:= 1;
		'a'..'z': my_ord:= ord(ch) - ord('a') + 2;
		else my_ord:= 2;
	end;
end;

procedure add_town(town: string; check_if_town_there: boolean);
{adds a town to tree representing town names and if check_if_town_there is true it checkes if the town isn't already there (than it doesn't add this town)}
var
	P, Q: Pdecide_char;
	R, S: Pstring;
	i: integer;
	min: integer;
	last_decide_char: integer;
begin
	if length(town) < NUM_OF_DECIDE_CHARS then begin
		min:= length(town);
		last_decide_char:= 0;
	end else begin
		min:= NUM_OF_DECIDE_CHARS - 1;
		last_decide_char:= my_ord(no_accent[town[NUM_OF_DECIDE_CHARS]]);
	end;

	P:= first_char;
	for i:= 1 to min do
		if P^[my_ord(no_accent[town[i]])] = nil then begin
			new(Q);
			P^[my_ord(no_accent[town[i]])]:= Q;
			clear_Tdecide_char(Q^);
			P:= Q;
		end else
			P:= P^[my_ord(no_accent[town[i]])];

	if P^[last_decide_char] = nil then begin
		{if there wasn't any town in list}
		new(R);
		R^.value:= town;
		R^.next:= nil;
		P^[last_decide_char]:= R;
	end else begin
		if check_if_town_there then begin {checking}
			R:= P^[last_decide_char];
			while R <> nil do
				if R^.value = town then
					exit {town was found - exit}
				else
					R:= R^.next;
		end;
		{if town wasn't found in list or checking off}
		new(S);
		S^.value:= town;
		R:= P^[last_decide_char];
		S^.next:= R^.next; {the town goes after the first town in list}
		R^.next:= S;
	end;
end;

procedure create_town_list;
{goes through DATABASE file and adds towns to tree representing town names}
var
	f: text;
	line: string;
	town: string;
	i: integer;
begin
	assign(f, DATABASE);
	reset(f);
	while not eof(f) do begin
		readln(f, line);
		if (line <> '') and (line[1] in ['0'..'9','+',TAB]) then begin
			town:= '';
			i:= length(line);
			while line[i] <> TAB do begin
				town:= line[i] + town;
				dec(i);
			end;
			add_town(town, true); {with checking}
		end;
	end;
	close(f);
end;

procedure sort_Pstring(first: Pstring);
{sorts town names in list alphabeticaly}
var
	P: Pstring;
	s: string;
	change: boolean;
begin
	if first = nil then
		exit; {nothing to sort}
	change:= true;
	while change do begin
		P:= first;
		change:= false;
		while (P^.next <> nil) do begin
			if lowcase(P^.value) > lowcase(P^.next^.value) then begin
				s:= P^.value; {swap}
				P^.value:= P^.next^.value;
				P^.next^.value:= s;
				change:= true;
			end;
			P:= P^.next;
		end;
	end;
end;

procedure free_Pstring(P: Pstring);
{releases Pstring from memory}
var
	Q: Pstring;
begin
	while P <> nil do begin
		Q:= P^.next;
		dispose(P);
		P:= Q;
	end;
end;

procedure free_Pdecide_char(depth: integer; P: Pdecide_char);
{releases Pdecide_char from memory (and all Pdecide_chars and Pstrings it points to}
var
	i: integer;
begin
	if depth = 1 then begin
		for i:= char_begin to char_end do
			free_Pstring(P^[i]);
		exit; {i finished}
	end;

	for i:= char_begin + 1 to char_end do
		if P^[i] <> nil then
			free_Pdecide_char(depth - 1, P^[i]);
	free_Pstring(P^[0]); {towns which are shorter than NUM_OF_DECIDE_CHARS}
	dispose(P);
end;

procedure free_towns;
{releases whole tree representing town names from memory}
begin
	free_Pdecide_char(NUM_OF_DECIDE_CHARS, first_char);
end;

function items_in_Pstring(P: Pstring): integer;
{returns number of items in Pstring}
var
	i: integer;
begin
	i:= 0;
	while P <> nil do begin
		inc(i);
		P:= P^.next;
	end;
	items_in_Pstring:= i;
end;

{--- searching in town structure ---------------------------------------------}

var
	output_file: text;
	apply_on_every_town: procedure(s: string);

	from_list: Pstring;
	to_list: Pstring;

procedure recursive_town_search(depth: integer; P: Pdecide_char);
{searches in town list and applies a custom procedure apply_on_every_town on every town}
var
	i : integer;
	R : Pstring;
begin
	if depth = 0 then begin {Pdecide_char must show on Pstring}
		R:= pointer(P);
		while R <> nil do begin
			apply_on_every_town(R^.value);
			R:= R^.next;
		end;
	end else if depth = 1 then
		for i:= char_begin to char_end do begin
			R:= P^[i];
			while R <> nil do begin
				apply_on_every_town(R^.value);
				R:= R^.next;
			end;
		end
	else for i:= char_begin to char_end do
		if P^[i] <> nil then
			if i <> 0 then
				recursive_town_search(depth - 1, P^[i])
			else begin
				R:= P^[i];
				apply_on_every_town(R^.value);
			end;
end;

procedure write_town_to_file(town: string);
begin
	writeln(output_file, town);
end;

procedure save_town_list;
begin
	assign(output_file, town_list_file);
	rewrite(output_file);
	apply_on_every_town:= @write_town_to_file;
	recursive_town_search(NUM_OF_DECIDE_CHARS, first_char);
	close(output_file);
end;

procedure load_town_list;
var
	f: text;
	town: string;
begin
	initialize_town_structure;
	assign(f, town_list_file);
	{$I-}
	reset(f);
	{$I+}
	if IOResult <> 0 then begin {file doesn't exist}
		create_town_list;
		save_town_list;
	end else begin
		while not eof(f) do begin
			readln(f, town);
			add_town(town, false); {without checking}
		end;
		close(f);
	end;
end;

var
	key: string;
	list: Pstring;
	exactlist: Pstring;

procedure add_to_list_if_matches(town: string);
{adds the town to list if it matches the key}
var
	i: integer;
	P: Pstring;
begin
	if length(key) <= length(town) then begin
		i:= 1;
		while i <= length(key) do
			if no_accent[town[i]] = no_accent[key[i]] then
				inc(i)
			else
				exit; {doesn't match}
		new(P); {add it to list}
		P^.value:= town;
		P^.next:= list;
		list:= P;
		if length(key) = length(town) then begin {if exact match}
			new(P); {add it to exactlist}
			P^.value:= town;
			P^.next:= exactlist;
			exactlist:= P;
		end;
	end;
end;

procedure search_matching_towns(var R: Pstring; s: string);
{searches for the matching towns and adds them to R}
var
	P: Pdecide_char;
	min: integer;
	i: integer;
begin
	if length(s) = 0 then begin
		R:= nil;
		exit;
	end;

	if NUM_OF_DECIDE_CHARS <= length(s) then
		min:= NUM_OF_DECIDE_CHARS
	else
		min:= length(s);

	P:= first_char;
	for i:= 1 to min do
		if P^[my_ord(no_accent[s[i]])] <> nil then
			P:= P^[my_ord(no_accent[s[i]])]
		else
			exit; {no matching town exists}

	key:= s;
	list:= nil;
	exactlist:= nil;
	apply_on_every_town:= @add_to_list_if_matches;
	recursive_town_search(NUM_OF_DECIDE_CHARS - min, P);
	if (exactlist <> nil) and (exactlist^.next = nil) then begin {if one item in exactlist}
		R:= exactlist;
		free_Pstring(list);
	end else begin
		R:= list;
		free_Pstring(exactlist);
	end;
end;

{--- html output -------------------------------------------------------------}

procedure print_html_header(title: string);
begin
	{header is in every output}
	writeln('Content-type: text/html; charset=iso-8859-2');
	writeln('Pragma: no-cache');
	writeln;

	writeln('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">');
	writeln('<HTML>');
	writeln('<HEAD>');
	writeln('<TITLE>', title, '</TITLE>');
	writeln('<META HTTP-Equiv="Content-Type" CONTENT="text/html; charset=iso-8859-2">');
	writeln('</HEAD>');
	writeln('<BODY>');
end;

procedure print_html_foot;
begin
	writeln('</BODY>');
	writeln('</HTML>');
end;

procedure print_town_list(P: Pstring; desc, name: string);
begin
	if P = nil then begin {no item in list}
		write('<TR><TD ALIGN=right>', desc, '</TD>');
		write('<TD><INPUT TYPE=text NAME=', name, ' VALUE="', get_item(name), '"></TD>');
		if get_item('submit') <> '' then begin
			write('<TD>');
			write_error(MESSAGE_OBJECT_NOT_FOUND);
			writeln('</TD></TR>')
		end else
			writeln('<TD>&nbsp;</TD></TR>');
	end else if P^.next = nil then begin {one item in list}
		write('<TR><TD ALIGN=right>', desc, '</TD>');
		writeln('<TD COLSPAN="2"><INPUT TYPE=text NAME=', name, ' VALUE="', P^.value, '"></TD></TR>')
	end else begin {if more than one item in list}
		sort_Pstring(P);
		write('<TR><TD ALIGN=right>', desc, '</TD>');
		writeln('<TD COLSPAN="2"><SELECT NAME=', name, '>');
		while P <> nil do begin
			writeln('  <OPTION>', P^.value, '</OPTION>');
			P:= P^.next;
		end;
		writeln('</SELECT></TD></TR>');
	end;
end;

procedure print_form;
{prints input form with values of items from, to, date and time}
var
	i: integer;
begin
	writeln('<FORM ACTION="', CGI_NAME, '" METHOD=post>');
	writeln('<TABLE>');
	print_town_list(from_list, MESSAGE_FROM, 'from');
	print_town_list(to_list, MESSAGE_TO, 'to');
	write('<TR><TD ALIGN=right>', MESSAGE_DATE, '</TD>');
	write('<TD><INPUT TYPE=text NAME=date VALUE="', get_item('date'), '"></TD>');
	if get_item('submit') = '' then
		writeln('<TD><INPUT TYPE=checkbox NAME=datetomorrow VALUE="', get_item('tomorrow'), '">', MESSAGE_TOMORROW, ' - ', get_item('tomorrow'), '</TD></TR>')
	else
		writeln('<TD>&nbsp;</TD></TR>');
	write('<TR><TD ALIGN=right>', MESSAGE_TIME, '</TD>');
	writeln('<TD COLSPAN="2"><INPUT TYPE=text NAME=time VALUE="', get_item('time'), '"></TD></TR>');
	write('<TR><TD>', MESSAGE_I_DONT_WANT_TO_GO_WITH, '</TD><TD COLSPAN="2">');
	for i:= 1 to NUM_OF_TRAINS do begin
		write('<INPUT TYPE=checkbox NAME=not', TRAIN[i], ' ');
		write('VALUE=', TRAIN[i]);
		if get_item('not' + TRAIN[i]) <> '' then
			write(' CHECKED');
		write('>', TRAIN[i]);
	end;
	writeln('</TD></TR>');
	write('<TR><TD COLSPAN="2" ALIGN=center>');
	write('<INPUT TYPE=submit NAME=submit VALUE="', MESSAGE_SEARCH, '">');
	write(' &nbsp; <INPUT TYPE=submit NAME=submit VALUE="', MESSAGE_SWAP_FROM_TO, '">');
	writeln('</TD><TD>&nbsp;</TD></TR>');
	writeln('</TABLE>');
	writeln('</FORM>');
end;

{--- write output from timetab in html ---------------------------------------}

procedure write_timetab_output(from_file: string);
{reads timetab output and produces html output; the file is than deleted}
{some parts of this procedure comes from timetab project (tools/tt2html)
author of these parts Copyright 2001 Petr Cermak <cermak01@seznam.cz> }
var
	f: text;
	line: string;
	flag: integer;
const
	NOFLAG = 0;
	inREMARK = 1;
	inHOUR = 2;
	readed_departure_time: boolean = false;

	function readline: string;
	begin
		repeat
			readln(f, line);
		until (line <> '') or eof(f);
		readline:= line;
	end;

begin
	assign(f, from_file);
	reset(f);

	if eof(f) then begin {connection not found}
		write_error(MESSAGE_NO_CONNECTION_FOUND);
		close(f);
		erase(f);
		exit;
	end;

	flag:= NOFLAG;

	while not eof(f) do begin
		readline;
		writeln(line);
		end;

	writeln('<HR>');

	close(f);
	erase(f);
end;

{--- processing data from form -----------------------------------------------}

function plus1min(s: string): string;
{adds one minute to time in s}
var
	i: integer;
begin
	i:= length(s);
	s[i]:= chr(ord(s[i]) + 1);
	while s[i] > '9' do begin
		s[i]:= '0';
		dec(i);
		if s[i] = ':' then
			dec(i);
		s[i]:= chr(ord(s[i]) + 1);
	end;
	plus1min:= s;
end;

procedure process_form_data;
{it branches the program according to input from form}
var
	s: string;
	exit_code: integer;
	date, time: string;
	last: integer;
	i: integer;
	PP: PPchar;
	arg: array[0 .. 6 + NUM_OF_TRAINS] of Pchar;
	pid: string;
begin
	if get_item('submit') = MESSAGE_SWAP_FROM_TO then begin
		s:= get_item('from'); {swap from and to towns}
		change_item('from', get_item('to'));
		change_item('to', s);
	end;

	load_town_list;
	search_matching_towns(from_list, get_item('from'));
	search_matching_towns(to_list, get_item('to'));
	free_towns;

	if get_item('submit') = MESSAGE_SWAP_FROM_TO then begin
		print_form;
		exit; {just swap town names}
	end;

	if get_item('datetomorrow') <> '' then
		change_item('date', get_item('datetomorrow'));

	if (items_in_Pstring(from_list) <> 1)
	or (items_in_Pstring(to_list) <> 1) then begin
		print_form;
		free_Pstring(from_list);
		free_Pstring(to_list);
		exit; {not going to search}
	end;

	str(GetPid, pid); {gets the pid of this process (for output.$$)}

	{ I fork the process. Parent process waits for child process to finish.
	  Child process changes the stdin and stdout (with some little trick).
	  This changes the stdin and stdout of timetab. Than child process is
	  replaced with timetab using execv. When timetab finishes, the parent
	  process continues after WaitPid }
	
	if fork = 0 then begin {child procces}
		{change stdout to output file}
		fdclose(getfs(output));
		fdopen('output.'+pid, open_wronly or open_creat or open_trunc);

		{change stdin to database file}
		fdClose(GetFS(input));
		fdOpen(DATABASE, Open_RdOnly);

	   	{change stderr to diag file}
	   	fdclose(getfs(stderr));
	   	fdopen('stderr', open_wronly or open_creat or open_trunc);

		date:= get_item('date');
		time:= get_item('time');

		with from_list^ do value[length(value) + 1]:= #0;
		with to_list^ do value[length(value) + 1]:= #0;
		date[length(date) + 1]:= #0;
		time[length(time) + 1]:= #0;

		arg[0]:= TIMETAB; {program name}
		arg[1]:= @from_list^.value[1];
		arg[2]:= @date[1];
		arg[3]:= @time[1];
		arg[4]:= @to_list^.value[1];
		last:= 4;

		for i:= 1 to NUM_OF_TRAINS do
			if get_item('not' + TRAIN[i]) <> '' then begin
				inc(last);
				arg[last]:= TRAIN[i];
			end;

		inc(last);
		arg[last]:= nil;

		getmem(PP, (last+1)*sizeof(Pchar));
		for i:= 0 to last do
			PP[i]:= arg[i];

		execv(TIMETAB, PP);
		{ ./timetab -U from date time to IC... <DATABASE >output.$$}
		halt(5);
	end;

	WaitPid(0, @exit_code, 0);

	case exit_code shr 8 of {exitcode from timetab}
		0: write_timetab_output('output.' + pid);
		2: write_error(MESSAGE_TOWN_NOT_FOUND);
		3: write_error(MESSAGE_TIME_ERROR);
		5: write_error(MESSAGE_TIMETAB_ERROR); {error executing timetab}
		else write_error(MESSAGE_UNKNOWN_ERROR);
	end;
	
	if get_item('departure_time') <> '' then
		change_item('time', plus1min(get_item('departure_time')));

	print_form;

	free_Pstring(from_list);
	free_Pstring(to_list);
end;

{--- main program ------------------------------------------------------------}

begin
	process_input;

	print_html_header(MESSAGE_DOCUMENT_TITLE);

	if get_item('submit') = '' then begin
		set_date_and_time_items;
		print_form;
	end else {if processing form}
		process_form_data;

	print_html_foot;
	free_items;
end.
