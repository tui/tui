
			Train base format
			~~~~~~~~~~~~~~~~~

.tt format is nice for data interchange, but it is rather hard to
search within it efficiently. Train base format (.tb) may not be that
nice to process, but is designed to be searchable without reading
whole datafile to memory.

Data types:
~~~~~~~~~~~

LONG == 32bit quantity, big endian
VARI == variable length. Big endian, last byte of number has high bit clear, others have it set

.tb is directory containing following files:

README
~~~~~~
Any information in .tt format. Should contain comments, ! tags, etc. Must contain
!towns, !conns,  information.

towns.txt
~~~~~~~~~
Names of towns, one per line. [should I zero separate them?]

towns.dat
~~~~~~~~~
Array of following structures:

LONG	starting position of name in towns.txt file

remarks.txt
~~~~~~~~~~~
All remarks. \0 marks end of block of remarks for one connection.

conns.txt
~~~~~~~~~
Names of connections, one per line.

conns.dat
~~~~~~~~~
LONG	starting position of name in conns.txt file
LONG	starting position of remarks in remarks.txt file
LONG	starting byte of node in nodes.dat
LONG	starting time, 0x0002 0000 -- "ANY"

nodes.dat
~~~~~~~~~
VARI	town index+1 (0 -- end of connection; notice: as distance/times/flags do not make sense at end of connection, they simply are not written to the file)
VARI	relative time of arrival
VARI	relative time of departure
VARI	index to flags.dat
VARI	distance

flags.txt
~~~~~~~~~
Flags, one per line

flags.dat
~~~~~~~~~
LONG	position in flags.txt

Following may or may not be pressent:

towns2.dat
~~~~~~~~~
LONG    starting position of list of connections in index.dat file

index.dat
~~~~~~~~
VARI    connection index+1 (0 -- end of list of connections), sorted
        by time of arrival

