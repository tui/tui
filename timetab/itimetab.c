/* Interactive front-end for hledac, distribute under GPL version 2 or later
 * Copyright 2000 Martin Hejna <m.hejna@worldonline.cz>
 *
 * compile: gcc -o itimetab itimetab.c -lncurses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <curses.h>
#include <time.h>
#include <ctype.h>
#include <locale.h>

int travel_file='t';
int scrmaxx,scrmaxy;
char opts[1024];

#define HLEDAC		"timetab"
#define CONF_FILE	".itimetab"
#define TOWNMAX		40000

#define LINE_WIDTH	70
#define LINE_MAX        23

#define DEF_ROK		2000
#define DEF_MES		10                 //default date
#define DEF_DEN		2
#define DEF_HOUR	5                  //default time
#define DEF_MIN		0


const char *wdny[7]={"nedele","pondeli","utery","streda",
			"ctvrtek","patek","sobota"};


struct townvec {
	int numtowns;
	char *towns[TOWNMAX];
};

struct townvec *townvec;

char *memory, *memory_end;
char *z,*cil,*datum,*cas,*excl,*param;
char pexcl[1024],argv1[1024];
FILE *f;

struct line {
	char s[LINE_WIDTH+1];
        struct line *next;
        struct line *prev;
};

struct line *line=NULL;
int details=0;


void zaver(void)
{
        curs_set(1);
        endwin();
        printf("\n");
        exit(0);
}

int mujsort(const char **s1,const char **s2)
{
	return strcmp(*s1,*s2);
}

void readit(void)
{
	char buf[1024];
	FILE *f;
        char command[1024];
        sprintf(command,HLEDAC " -l%c",travel_file);
        f=popen(command,"r");

	townvec = malloc(sizeof(struct townvec));
        townvec->numtowns=0;
        for(;;) {
		if (fgets(buf, 1023, f)==NULL) break;
		if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
                while (buf[strlen(buf)-1]==' ') buf[strlen(buf)-1]=0;
                townvec->towns[townvec->numtowns]=malloc(strlen(buf)+1);
                strcpy(townvec->towns[townvec->numtowns],buf);
                townvec->numtowns++;
                if (townvec->numtowns> TOWNMAX) {fprintf(stderr,"Town limit exceeded\n");exit(10);}
	}
        pclose(f);
        qsort(townvec->towns,townvec->numtowns,sizeof(char *),(void *)mujsort);
}

void getstring(int y,int x,char *inpt)
{
        int delka=0;
        int mode=0;
        int c;

        strcpy(inpt,"");
        move(y,delka+x);
        curs_set(1);
        while (1) {
        	c=getch();
                if (c==120 && mode==1) zaver();
                if (c==27) {mode=1;continue;}  //esc neni pismeno (continue)
                if (c==KEY_BACKSPACE) {
                	if (delka) {
                        	mvaddch(y,delka+x-1,' ');
                                inpt[--delka]=0;
                                move(y,delka+x);
                        } else {printf("%c",7);fflush(stdout);}
                        continue;
                }
                if (c==13) break;
                if (!isprint(c)) continue;
                mvprintw(y,delka+x,"%c",c);
                inpt[delka++]=c;
                inpt[delka]=0;
        }
        curs_set(0);
}

int vyhledej(char *s)
{
	int prvni=0,posledni=townvec->numtowns-1;
        int i;
        
        while (prvni+1!=posledni) {
        	if (strncmp(s,townvec->towns[(prvni+posledni)/2],strlen(s)) >0)
                	prvni=(prvni+posledni)/2;
                else posledni=(prvni+posledni)/2;
        }
       	if (strncmp(s,townvec->towns[(prvni+posledni)/2],strlen(s)) >0)
		prvni=(prvni+posledni)/2;
	else posledni=(prvni+posledni)/2;
        //je potreba to provyst jeste jednou, kdyby napr. s==towns[0]
        
        for (i=0;i<19;i++) {
        	move(5+i,0);clrtoeol();
        	if (posledni+i<townvec->numtowns &&
                	!strncmp(s,townvec->towns[posledni+i],strlen(s)) )
		        	mvaddstr(5+i,0,townvec->towns[posledni+i]);
        }
        if (!strncmp(s,townvec->towns[posledni],strlen(s)) )
       		return posledni;
                else return -1;
}

void getinput(int y,int x,char *inpt)
{
        int delka=0;
        int mode=0;
        int c;
        int vystup;

        strcpy(inpt,"");
        vystup=vyhledej(inpt);
        move(y,delka+x);
        curs_set(1);
        while (1) {
        	c=getch();
                if (c==120 && mode==1) zaver();
                if (c==27) {mode=1;continue;}  //esc neni pismeno (continue)
                if (c==KEY_UP) {
                	if (vystup>0) vystup--;
                        	else {printf("%c",7);fflush(stdout);}
                        if (vystup==-1) vystup=0;
			strcpy(inpt,townvec->towns[vystup]);
                        delka=strlen(inpt);
		        move(y,x);clrtoeol();
		        mvaddstr(y,x,inpt);
                        vyhledej(inpt);
	                move(y,delka+x);
                        continue;
                }
                if (c==KEY_DOWN) {
                	if (vystup<townvec->numtowns-1) vystup++;
                        	else {printf("%c",7);fflush(stdout);}
			strcpy(inpt,townvec->towns[vystup]);
                        delka=strlen(inpt);
		        move(y,x);clrtoeol();
		        mvaddstr(y,x,inpt);
                        vyhledej(inpt);
	                move(y,delka+x);
                        continue;
                }
                if (c==KEY_PPAGE) {
                	if (vystup>29) vystup-=30;
                        	else vystup=0;
			strcpy(inpt,townvec->towns[vystup]);
                        delka=strlen(inpt);
		        move(y,x);clrtoeol();
		        mvaddstr(y,x,inpt);
                        vyhledej(inpt);
	                move(y,delka+x);
                        continue;
                }
                if (c==KEY_NPAGE) {
                	if (vystup<townvec->numtowns-30) vystup+=30;
                        	else vystup=townvec->numtowns-1;
			strcpy(inpt,townvec->towns[vystup]);
                        delka=strlen(inpt);
		        move(y,x);clrtoeol();
		        mvaddstr(y,x,inpt);
                        vyhledej(inpt);
	                move(y,delka+x);
                        continue;
                }
                mode=0;
                if (c==KEY_BACKSPACE) {
                	if (delka) {
                        	mvaddch(y,delka+x-1,' ');
                                inpt[--delka]=0;
	                        vystup=vyhledej(inpt);
		                move(y,delka+x);
                        } else {printf("%c",7);fflush(stdout);}
                        continue;
                }
                if (c==13) break;
                if (!isprint(c)) continue;
                mvprintw(y,delka+x,"%c",c);
                inpt[delka++]=c;
                inpt[delka]=0;
                vystup=vyhledej(inpt);
                move(y,delka+x);
        }
	if (vystup!=-1) strcpy(inpt,townvec->towns[vystup]);
        mvaddstr(y,x,inpt);
        curs_set(0);
        
}

int iscond(char *s)
{
	if (s[0]=='#' && s[1]=='#') return 1;
        return 0;
}

void gettrain(char *to,char *from)
{
	if (from[3]=='!') strcpy(to,from+6);
		else strcpy(to,from+3);
        if (strchr(to,' ')) *strchr(to,' ')=0;
}

void getexcl(int y,int x,char *inpt)
{
        int delka=0;
        int mode=0;
        int c;
        struct line *gline=line;

        while (gline->prev) gline=gline->prev;
        strcpy(inpt,"");
        move(y,delka+x);
        curs_set(1);
        while (1) {
        	c=getch();
                if (c==120 && mode==1) zaver();
                if (c==27) {mode=1;continue;}  //esc neni pismeno (continue)
                if (c==KEY_UP) {
                	if (gline->prev) gline=gline->prev;
                	while (gline->prev && !iscond(gline->s)) gline=gline->prev;
                        if (!iscond(gline->s))
                                while (gline->next && !iscond(gline->s)) gline=gline->next;
                        if (!iscond(gline->s)) {printf("%c",7);fflush(stdout);mode=0;continue;}
                        gettrain(inpt,gline->s);
                        delka=strlen(inpt);
		        move(y,x);clrtoeol();
		        mvaddstr(y,x,inpt);
                }
                if (c==KEY_DOWN) {
                	if (gline->next) gline=gline->next;
                        while (gline->next && !iscond(gline->s)) gline=gline->next;
                        if (!iscond(gline->s))
	                	while (gline->prev && !iscond(gline->s)) gline=gline->prev;
                        if (!iscond(gline->s)) {printf("%c",7);fflush(stdout);mode=0;continue;}
                        gettrain(inpt,gline->s);
                        delka=strlen(inpt);
		        move(y,x);clrtoeol();
		        mvaddstr(y,x,inpt);
                }
                if (c==KEY_BACKSPACE) {
                	if (delka) {
                        	mvaddch(y,delka+x-1,' ');
                                inpt[--delka]=0;
                                move(y,delka+x);
                        } else {printf("%c",7);fflush(stdout);}
                        continue;
                }
                if (c==13) break;
                mode=0;
                if (!isprint(c)) continue;
                mvprintw(y,delka+x,"%c",c);
                inpt[delka++]=c;
                inpt[delka]=0;
        }
        curs_set(0);
}


void nacti()
{
        struct line *prev=NULL;
        int i;
        char s[1024];
        char *ss;

        s[0]=0;
	for(;;) {
        	line=malloc(sizeof(*line));
                if (prev) prev->next=line;
                line->prev=prev;
        	if (fgets(s,1024,f)==NULL) break;
                if (s[strlen(s)-1]=='\n') s[strlen(s)-1]=0;
               	ss=s;
                while (strlen(ss)>LINE_WIDTH) {
                	i=LINE_WIDTH;
                        while (i>0 && ss[i]!=' ') i--;
                        if (i==0) while (i<strlen(ss) && ss[i]!=' ') i++;
                        if (i==strlen(ss)) break;
                        strncpy(line->s,ss,i);
                        line->s[i]=0;
                        ss+=i+1;
                        prev=line;
	        	line=malloc(sizeof(*line));
	                if (prev) prev->next=line;
	                line->prev=prev;
                }
                strncpy(line->s,ss,LINE_WIDTH);
                line->s[LINE_WIDTH]=0;
                prev=line;
        }
        pclose(f);
        if (prev) {
        	free(line);
                line=prev;
        } else strcpy(line->s,"");
        line->next=NULL;
        while (line->prev) line=line->prev;
}

void obr()
{
	int i;
        struct line *pline=line;

        clear();
        for (i=0;i<25;i++) {
        	mvaddstr(i,0,line->s);
                if (!line->next) break;
                line=line->next;
	}
        line=pline;
}

int decode_details(char *s)
{
	int res=details;
	if (strchr(s,'H')) res&=~16;
	if (strchr(s,'S')) res&=~1;
	if (strchr(s,'C')) res&=~4;
	if (strchr(s,'D')) res&=~8;
	if (strchr(s,'N')) res&=~2;
	if (strchr(s,'T')) res&=~32;
	if (strchr(s,'I')) res&=~64;
	if (strchr(s,'O')) res&=~128;
	if (strchr(s,'h')) res|=16;
	if (strchr(s,'s')) res|=1;
	if (strchr(s,'c')) res|=4;
	if (strchr(s,'d')) res|=8;
	if (strchr(s,'n')) res|=2;
	if (strchr(s,'t')) res|=32;
	if (strchr(s,'i')) res|=64;
	if (strchr(s,'o')) res|=128;
	return res;
}

void decode_opts(char *s)
{
	int i;
        for(i=0;i<strlen(s);i++) {
                if (strchr(opts,s[i])) {
                        *(char*)strchr(opts,s[i])=' ';
                } else {
                	opts[strlen(opts)+1]=0;
                	opts[strlen(opts)]=s[i];
                }
        }
        while (strchr(opts,' ')) {
                *(char *)strchr(opts,' ')=*(char*)(opts+strlen(opts)-1);
                opts[strlen(opts)-1]=0;
        	while (opts[strlen(opts)-1]==' ') opts[strlen(opts)-1]=0;
        }
}

int zobraz()
{
	FILE *f;
        char filename[1024];
        int res=0;
        int mode=0;
        int c;
        int i;
	struct line *bline;
	struct line *pline;
        WINDOW *status_line;
        chtype status_line_color=COLOR_PAIR(1);

        wresize(stdscr,LINE_MAX+1,scrmaxx);
        mvwin(stdscr,1,0);
        status_line=newwin(1,scrmaxx,0,0);
        wattrset(status_line,status_line_color);
        wbkgd(status_line,status_line_color);
        wclear(status_line);
        mvwaddstr(status_line,0,0,"Export eXclude Permanent_exclude Showexcluded reenterTime Next Q-reenterdata");
        wrefresh(status_line);

        bline=line;
        for (i=0;i<LINE_MAX;i++)
        	if (bline->next) bline=bline->next;
        obr();
        while (1) {
		c=getch();
                if (c==120 && mode==1) zaver();  ///alt + x
                if (c==27) {mode=1;continue;}  //esc neni pismeno (continue)
                if (c==KEY_UP) {
                        if (line->prev) {
                                bline=bline->prev;
                        	line=line->prev;
                        	scrl(-1);
	        		mvaddstr(0,0,line->s);
                        }
                }
                if (c==KEY_DOWN) {
                        if (bline->next) {
                        	bline=bline->next;
                                line=line->next;
                        	scrl(1);
	        		mvaddstr(LINE_MAX,0,bline->s);
			}
                }
                if (c==KEY_PPAGE) {
                	for (i=0;i<LINE_MAX;i++) {
	                        if (!line->prev) break;
                               	line=line->prev;
                                bline=bline->prev;
                        }
                        if (i!=0) obr();
                }
                if (c==KEY_NPAGE || c==' ') {
                	for (i=0;i<LINE_MAX;i++) {
	                        if (!bline->next) break;
                               	line=line->next;
                               	bline=bline->next;
                        }
                        if (i!=0) obr();
                }
                if (c==KEY_HOME) {
                	mode=1;
                	while (line->prev) {
                        	mode=0;
                        	line=line->prev;
                                bline=bline->prev;
                        }
                        if (mode==0) obr();
                        mode=0;
                }
                if (c==KEY_END) {
                	mode=1;
                	while (bline->next) {
                        	mode=0;
                        	line=line->next;
                                bline=bline->next;
                        }
                        if (mode==0) obr();
                        mode=0;continue;
                }

                if (c=='e' || c=='E') {
	                mvaddstr(LINE_MAX,0,"Export to file:");
        	        clrtoeol();
	                getstring(LINE_MAX,15,filename);
	                obr();
	                if (filename[0]==0) continue;
	                f=fopen(filename,"w");
                        if (!f) {printf("%c",7);fflush(stdout);continue;}
	                pline=line;
		        while (pline->prev) pline=pline->prev;
	        	while (pline->next) {
	                	fprintf(f,"%s\n",pline->s);
	                	pline=pline->next;
	                }
	               	fprintf(f,"%s\n",pline->s);
	                fclose(f);
                }
                if (c=='d' || c=='D') {
	                mvaddstr(LINE_MAX,0,"Details [Head Infos Station Cond Dest Owner Note Tail]:");
        	        clrtoeol();
	                getstring(LINE_MAX,55,filename);
	                obr();
                        details=decode_details(filename);
                        res=0;
                        break;
                }
                if (c=='o' || c=='O') {
	                mvaddstr(LINE_MAX,0,"Options:");
        	        clrtoeol();
	                getstring(LINE_MAX,8,filename);
	                obr();
                        decode_opts(filename);
                        res=0;
                        break;
                }
                if (c=='x' || c=='X') {
	                mvaddstr(LINE_MAX,0,"Exclude Train:");
        	        clrtoeol();
	                getexcl(LINE_MAX,14,filename);
                        if (excl[0]!=0) strcat(excl," ");
                        strcat(excl,filename);
	                obr();
                        res=0;
                        break;
                }
                if (c=='p' || c=='P') {
	                mvaddstr(LINE_MAX,0,"Permanent Exclude Train:");
        	        clrtoeol();
	                getexcl(LINE_MAX,24,filename);
                        if (pexcl[0]!=0) strcat(pexcl," ");
                        strcat(pexcl,filename);
                        if (excl[0]!=0) strcat(excl," ");
                        strcat(excl,filename);
	                obr();
                        res=0;
                        break;
                }
                if (c=='s' || c=='S') {
	                mvaddstr(LINE_MAX,0,"Excluded Train:");
                        mvaddstr(LINE_MAX,15,excl);
        	        clrtoeol();
                        getch();
	                obr();
                }
                if (c=='c' || c=='C') {
	                mvaddstr(LINE_MAX,0,"Command Line:");
                        mvaddstr(LINE_MAX,13,param);
        	        clrtoeol();
                        getch();
	                obr();
                }
                if (c=='t' || c=='T') {
                	int p1,p2,p3;
	                mvaddstr(LINE_MAX,0,"TIME:");
        	        clrtoeol();
		        getstring(LINE_MAX,5,cas);
	                p3=sscanf(cas,"%d:%d",&p1,&p2);
	                if (p3==1) snprintf(cas,1023,"%d:%02d",p1,DEF_MIN);
			if (p3==0 || p3==EOF) snprintf(cas,1023,"%d:%02d",DEF_HOUR,DEF_MIN);
                        strcpy(excl,pexcl);
                        res=0;
                        break;
                }
                if (c=='n' || c=='N') {
                	int p1,p2;
                        pline=line;
		        while (pline->prev) pline=pline->prev;
	        	while (pline->next) {
                        	if (sscanf(pline->s,"      - %d:%d",&p1,&p2)==2) break;
	                	pline=pline->next;
	                }
                        if (sscanf(pline->s,"      -  %d:%d",&p1,&p2)!=2) continue;
	                snprintf(cas,1023,"%d:%02d",p1,p2+1);
                        strcpy(excl,pexcl);
                        res=0;
                        break;
                }
                if (c=='q' || c=='Q') {res=1;break;}
                mode=0;
	}
        mvwin(stdscr,0,0);
        wresize(stdscr,scrmaxy,scrmaxx);
        return res;
}

void usage()
{
	printf("Usage: see source\n");
        exit(0);
}


int main(int argc,char **argv)
{
        int p1,p2,p3,i;
	struct tm date;
        FILE *fconf;
        char conf_file[1024],tmpopts[1024];

	setlocale (LC_ALL, "");
        
        z=malloc(1024);
        cil=malloc(1024);
        datum=malloc(1024);
        cas=malloc(1024);
        excl=malloc(1024);
        param=malloc(1024);
        strcpy(pexcl,"");
        strcpy(tmpopts,"");
        strcpy(opts,"");

        sprintf(conf_file,"%s/" CONF_FILE,getenv("HOME"));
        fconf=fopen(conf_file,"r");
        if (fconf) {fgets(tmpopts,511,fconf);fclose(fconf);}
        while (tmpopts[strlen(tmpopts)-1]==' ' || tmpopts[strlen(tmpopts)-1]=='\n')
        	tmpopts[strlen(tmpopts)-1]=0;
        
        if (argc>1) {
        	if (strchr(argv[1],'h')) usage();
                if (!strcmp(argv[1],"--help")) usage();
                if (argv[1][0]!='-') usage();

                strncat(tmpopts,argv[1]+1,511);
                tmpopts[1022]=0;
        } //command confs goes after file confs, so they has precedence

        i=0;
        if (*tmpopts) while (1) {
	        	if (tmpopts[i]=='t' || tmpopts[i]=='b' || tmpopts[i]=='x')
                	travel_file=tmpopts[i];
            else if (tmpopts[i]!='n') {
		     	if (strrchr(tmpopts+i,tmpopts[i])==tmpopts ||
/* First letter can`t be preceded by n. */
                        *(char *)( strrchr(tmpopts+i,tmpopts[i])-1 )!=(char)'n') {
/* If the last occurence of letter isn`t preceded by n */
				if (!strchr(opts,tmpopts[i])) {
	                        	opts[strlen(opts)+1]=0;
        	                        opts[strlen(opts)]=tmpopts[i];
                                }
/* add this letter to opts. */
			}
                }
                i++;
                if (!tmpopts[i]) break;
        }

	readit();
        
        initscr();
        nonl();
        cbreak();
        noecho();
        scrollok(stdscr,TRUE);
        immedok(stdscr,TRUE);
        curs_set(0);
        getmaxyx(stdscr,scrmaxy,scrmaxx);
        start_color();
        init_pair(1,COLOR_BLACK,COLOR_WHITE);
        keypad(stdscr,TRUE);
        for(;;) {
		int y = 0, x = 6;
	        clear();
		mvaddstr(y,0,"Time table search");
		y+=2;
		mvaddstr(y,0,"From:");
	        getinput(y,x,z);
		y++;
	        mvaddstr(y,0,"  To:");
	        getinput(y,x,cil);

	        for (p1=0;p1<19;p1++) {move(5+p1,0);clrtoeol();}
                
		y++;
	        mvaddstr(y,0,"Date:");
	        getstring(y,x,datum);
                p3=sscanf(datum,"%d.%d.%d",&date.tm_mday,&date.tm_mon,&date.tm_year);

		if (p3==0 || p3==EOF) date.tm_mday=DEF_DEN;
                if (p3<2) date.tm_mon=DEF_MES;
                if (p3<3) date.tm_year=DEF_ROK;
                date.tm_mon--;date.tm_year-=1900;
        	date.tm_sec=0;date.tm_min=0;date.tm_hour=0;
        	mktime(&date);
                snprintf(datum,1023,"%d.%d.%d",date.tm_mday,date.tm_mon+1,date.tm_year+1900);
                mvprintw(y,x,"%s  %s",datum,wdny[date.tm_wday]);

		y++;
                mvaddstr(y,0,"Time:");
	        getstring(y,x,cas);
                p3=sscanf(cas,"%d:%d",&p1,&p2);
                if (p3==1) snprintf(cas,1023,"%d:%02d",p1,DEF_MIN);
		if (p3==0 || p3==EOF) snprintf(cas,1023,"%d:%02d",DEF_HOUR,DEF_MIN);
                mvaddstr(y,x,cas);

		y++;
	        mvaddstr(y,0,"Permanent Exclude trains:");
	        getstring(y,25,pexcl);
                strcpy(excl,pexcl);
		y++;
                mvaddstr(y,0,"Details [Head Infos Station Cond Dest Owner Note Tail]:");
                getstring(y,55,argv1);
                details=decode_details(argv1);

                for(;;) {
                	sprintf(param,HLEDAC " -d%d%c%s '%s' %s %s '%s' %s",
                        	details,travel_file,opts,z,datum,cas,cil,excl);
                        move(24,0);clrtoeol();
		        endwin();
		        f=popen(param,"r");
	                nacti();
//  getchar();
		        refresh();
                        curs_set(0);
	                if (zobraz()) break;
		}
	}
        return 0;
}

