#include <stdarg.h>
#include <stdlib.h>

static void __attribute__((noreturn))
die(char *c, ...)
{
	va_list args;
	fputc('\n', stderr);
	va_start(args, c);
	vfprintf(stderr, c, args);
	fputc('\n', stderr);
	exit(1);
}

#define CONNS_SIZE (4*4)
#define TOWNS_SIZE 4

static void *mallocz(long size)
{
	void *ret = malloc(size);
	if (!ret)
		die("Out of memory");
	memset(ret, 0, size);
	return ret;
} 
