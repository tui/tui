#!/usr/bin/python

import sys
sys.path += [ "../tools" ]
import tt2
import transitfeed

class TT2Gtfs(tt2.TTReader):
    def __init__(m):
        m.schedule = transitfeed.Schedule()
        m.schedule.AddAgency("Test", "http://timetab.sf.net", "Europe/Prague")

        m.service_all = m.schedule.GetDefaultServicePeriod()
        m.service_all.SetWeekdayService(True)
        m.service_all.SetWeekendService(True)

        m.stops = {}
        m.cur_lon = 14
        m.cur_lat = 50

    def new_stop(m, stop, t1, t2):
        if not stop in m.stops:
            m.stops[stop] = m.schedule.AddStop(lng=m.cur_lon, lat=m.cur_lat, name=stop)
            m.cur_lon += 0.1
            if m.cur_lon > 90:
                m.cur_lon = 14
                m.cur_lat += 0.1
                
        t = t1
        if t2 != "":
            t = t2
        if t == "":
            return

        try:
            m.cur_trip.AddStopTime(m.stops[stop], stop_time=t+':00')
        except transitfeed.problems.OtherProblem:
            print("OtherProblem")

    def new_connection(m, name):
        m.cur_route = m.schedule.AddRoute(short_name=name, long_name="", route_type="Bus")
        m.cur_trip = m.cur_route.AddTrip(m.schedule, "")

    def convert(m):
        m.run()

        m.schedule.Validate()
        m.schedule.WriteGoogleTransitFeed("delme.zip")
        
tt = TT2Gtfs()
tt.convert()
