#!/usr/bin/python
# Copyright 2008 Petr Machek, GPL

import sys

last_time = ''

def min2string(minutes):
	return "%i:%02i" % (minutes/60, minutes%60)

def one_time(chunk):
	global last_time
	if chunk == '':
		pass
	elif ':' in chunk:
		time = chunk.split(':')
		last_time = 60*int(time[0]) + int(time[1])
	elif chunk[0] == '+':
		last_time = last_time + int(chunk[0:])
		chunk = min2string(last_time)
	return chunk

while(1):
	line = sys.stdin.readline()
	if line == '':
		sys.exit(0)
	if line[0] in '\t+1234567890':
		chunks = line.split('\t')
		chunks[0] = one_time(chunks[0])
		chunks[1] = one_time(chunks[1])
		line = '\t'.join(chunks)
		sys.stdout.write(line)
	else:
		last_time = ''
		sys.stdout.write(line)

