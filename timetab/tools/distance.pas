(* Copyright 2001 Jiri Cervak <george.w@email.cz>
   Distribute under terms of General Public Licence

-----------------------------------------------------------
Program finds for each station:

     >name of the nearest Key station
     >length of the shortest way station-Key station

Database of trains (file *.tt) is in standart input.
Key stations are red from the command line as parameters
Correct form of each parametr includes name and apostrophs:

   'Name of Key station'

There can be only 255 Key stations.

Program writes results to standard output. Each line means:

Station             Length         Key station

if there's no way from station to Keystation:

   Length = -1
   Key station is:  nostation

So example usage (unix) is:
 
 zcat vlak.tt.gz | ./distance "'Praha hl.n.'"
 
----------------------------------------------------------*)

program Vzdalenostikuzlum;

const n = 5;                         {pocet uzlu}
      nekonecno = 10000;             {~nekonecno,stanice nejsou spojeny}
      maxsta = 40;                   {maximalni delka nazvu}

Type pmesto = ^mesto;
     phrana = ^hrana;
     mesto = record                  {Typ prvek seznamu stanic}
               jmeno:string[maxsta]; {Nazev stanice}
               spojeni:phrana;       {Ukazatel na seznam hran z teto stanice}
               dalsi:pmesto;         {dalsi prvek seznamu}
               pevna:boolean;        {Zda je ohodnoceni nejmensi mozne}
               uzel:byte;            {Ke kteremu uzlu nalezi}
               ohodnoceni:integer       {Aktualni ohodnoceni vrcholu grafu}
             end;
     hrana = record                  {Typ prvek seznamu hran jedne stanice}
               stanice:pmesto;       {Kam vede hrana}
               dalsi:phrana;         {dalsi prvek seznamu hran}
               delka:integer;        {delka hrany (=vzd. mezi mesty)}
             end;
     seznammest = array[0..499] of pmesto;
     seznamuzlu = array[1..255] of string[maxsta];
var start:pmesto;                    {ukazatel na uzel}
    pocet,i:integer;                 {pocet uzlu, promenna cyklu}
    Seznam:seznammest;               {seznam mest}
    uzly:seznamuzlu;                 {seznam uzlu}

{------------------Zacatek deklaraci procedur a funkci-----------------------}

function Kodovani(S:string):integer;             {Koduje mesta seznamu}
                {(Jmeno stanice):kod potrebny pro umisteni do seznamu}
begin
  Kodovani:=(ord(S[1])*41+ord(S[2])*19) mod 500
end;
{----------------------------------------------------------------------------}
procedure Pridejstanici(var sez:pmesto;jm:string); {Prida stanici do seznamu}
                       {uk. na seznam, nazev stanice}
var novy:pmesto;
begin
  new(novy);
  novy^.jmeno:=jm;
  novy^.spojeni:=nil;
  novy^.dalsi:=Sez;
  novy^.pevna:=false;
  novy^.ohodnoceni:=nekonecno;  {~nekonecno}
  novy^.uzel:=0;
  Sez:=novy;
end;
{----------------------------------------------------------------------------}
         {Najde ukazatel na stanici, kdyz neni v seznamu, je nil}
function Najdistanici(Nazev:string;psez:pmesto):pmesto;
         {nazev mesta, uk. na prislusny seznam}
begin
  Najdistanici:=nil;
  while psez<>nil do  {Prochazeni seznamem}
    begin
      if psez^.jmeno=Nazev then
        begin
          Najdistanici:=psez;
          psez:=nil
        end
      else
    psez:=psez^.dalsi;
    end;              {Prochazeni seznamem}
end;
{----------------------------------------------------------------------------}
{Prida hranu, pokud uz tam neni totozna(st. delka a stejne stanice}
procedure Pridejhranu (akt:pmesto;predchozi:pmesto;Vzd:integer);
                      {stanice,predchozi stanice,vzdalenost}
var ph:phrana;        {Ukazatel na hranu}
    stejna:boolean;   {zjistuje, jestli uz existuje stejna hrana}
begin
  stejna:=false;
  ph:=predchozi^.spojeni;
  while ph<>nil do
    if (ph^.stanice^.jmeno = akt^.jmeno) and (ph^.delka=Vzd) then
{jestli kam se dostanu po hrane = aktualni stanice}
{Pokud existuje stejna hrana z predch. do akt., existuje i z akt. do predch.}
      begin
        ph:=nil;
        stejna:=true;
      end
    else
      ph:=ph^.dalsi;
  if not stejna then    { Takova hrana jeste neni}
    begin
      new(ph);                       {hrana z predch. do akt.}
      ph^.dalsi:=predchozi^.spojeni;
      ph^.stanice:=akt;
      ph^.delka:=vzd;
      predchozi^.spojeni:=ph;
      new(ph);                       {hrana z akt. do predch.}
      ph^.dalsi:=akt^.spojeni;
      ph^.stanice:=predchozi;
      ph^.delka:=vzd;
      akt^.spojeni:=ph;
    end;
end;
{----------------------------------------------------------------------------}
{z databaze vlaku vytvori graf stanic reprezentovany seznamem nasledovniku}
procedure VytvorSeznam(var seznam:seznammest);
                    {seznam mest(zatim prazdny), cesta k databazi}
var delka,kod,chyba:integer;
    {vzdalenost mezi 2 st.,kod do seznamu,chyba procedury val}
    k:byte;                  {pocita tabelatory (#9)}
    a:char;                  {Znak v souboru}
    nazev:string[maxsta];    {jmeno stanice}
    km:string[10];           {vzdalenost mezi 2 stanicemi}
    predchozi,prvek:pmesto;  {predch. a aktualni stranice v databazi}
    stanice,cislo:boolean;   {znak souboru je soucast nazvu st.(resp.km) A/N}

begin
k:=0;km:='';nazev:='';prvek:=nil;predchozi:=nil;
while (not eof)do  {nacitani z databaze vlaku}
  begin
    read(a);
    if a=#9 then k:=k+1;    {Pocet tabelatoru}
    if k=3 then             {3 tabelatory=zacatek vzdalenosti}
      begin
        cislo:=true;
        km:='+';
        while cislo do         {Nacteni vzdalenosti}
          begin
            read(a);
            if (a in ['0'..'9']) then
              km:=km+a
            else
              cislo:=false
          end;                 {nacteni vzdalenosti}
          val(km,delka,chyba); {Prevedeni ze stringu na integer}
          stanice:=true;
          while stanice do     {Nacteni stanice}
            begin
              read(a);
              if a=#10 then    {Konec radku}
                begin
                  stanice:=false;
                  kod:=kodovani(nazev);
                  Prvek:=Najdistanici(nazev,seznam[kod]);
                  if (prvek=nil) then     {neni v seznamu}
                    begin
                      Pridejstanici(seznam[kod],nazev);
                      prvek:=seznam[kod];
                      prvek^.spojeni:=nil;
                    end;
                  if delka<>0 then   { vzdalenost nenulova}
                    pridejhranu(prvek,predchozi,delka);
                  predchozi:=prvek;
                  nazev:='';
                  k:=0;
                  a:=' '
                end
              else
                nazev:=nazev+a {Nacitani jmena stanice}
          end                  {nacteni stanice}
      end                      {3 tabelatory}
  end                         {nacitani z databaze vlaku}
end;
{----------------------------------------------------------------------------}
function Minimum(sez:seznammest):pmesto;
               {Najde stanici s min. nepevn. ohod}
var minohod,i:integer;
begin
  Minimum:=nil;
  minohod:=nekonecno;
  for i:=0 to 499 do
    while sez[i]<>nil do
      begin
        if not(sez[i]^.pevna)and(sez[i]^.ohodnoceni<minohod) then
          begin
            Minimum:=sez[i];
            minohod:=sez[i]^.ohodnoceni;
          end;
        sez[i]:=sez[i]^.dalsi;
      end;
end;
{----------------------------------------------------------------------------}
procedure Zmenohodnoceni(p:pmesto); {Zmeni ohodnoceni sousedu prvku}
var ph:phrana;
begin
  ph:=p^.spojeni;
  p^.pevna:=true;                 {Ohodnoceni prvku pevne}
  while (p^.spojeni<>nil) do            {Prochazi sousedy}
    begin
      if(p^.ohodnoceni+p^.spojeni^.delka)<(p^.spojeni^.stanice^.ohodnoceni)
        then
          begin
            p^.spojeni^.stanice^.ohodnoceni:=p^.ohodnoceni+p^.spojeni^.delka;
            p^.spojeni^.stanice^.uzel:=p^.uzel;
          end;
        p^.spojeni:=p^.spojeni^.dalsi
    end;
  p^.spojeni:=ph;
end;
{----------------------------------------------------------------------------}
procedure Projdigraf(seznam:seznammest);
         {vysledna vzd., uk. na start,cil a seznam}
var prvek:pmesto;
    podminka:boolean;   {zda ma cenu jeste hledat}
begin
     podminka:=true;
     while podminka do
           begin
                prvek:= minimum(seznam);
                if (prvek=nil) then podminka:=false
                else Zmenohodnoceni(prvek);
           end
end;
{----------------------------------------------------------------------------}
procedure prikazovaradka(var sezuzlu:seznamuzlu;var j:integer);

var v:integer; { Promenne cyklu }
    pokracuje:boolean; { Zdali stanice za mezerou pokracuje}

begin
pokracuje:=false;
j:=1;
if paramcount > 0 then
  while (v <= paramcount) and (j<=255) do
    begin
      if pokracuje and (copy(paramstr(v),1,1)<>'''') then
        if copy (paramstr(v),length(paramstr(v)),1) = '''' then
          begin
            if (length(sezuzlu[j])+length(paramstr(v))-1 < maxsta) and
               (length(paramstr(v))>1) then
              begin
                sezuzlu[j]:=sezuzlu[j]+' '
                            +copy(paramstr(v),1,length(paramstr(v))-1);
                inc(j);
              end;
            pokracuje:=false
          end
        else if length(sezuzlu[j])+length(paramstr(v)) < maxsta then
               sezuzlu[j]:=sezuzlu[j]+' '
                           +copy(paramstr(v),1,length(paramstr(v)))
             else
               pokracuje:=false
      else if (copy(paramstr(v),1,1)='''') then
             begin
               pokracuje:=false;
               if copy (paramstr(v),length(paramstr(v)),1) = '''' then
                 begin
                   if (length(paramstr(v))-3 < maxsta) and
                      (length(paramstr(v))>2) then
                     begin
                       sezuzlu[j]:=copy(paramstr(v),2,length(paramstr(v))-2);
                       inc(j);
                     end
                   end
               else if (length(paramstr(v))-2 < maxsta) and
                       (length(paramstr(v))>1) then
                 begin
                   sezuzlu[j]:=copy(paramstr(v),2,length(paramstr(v))-1);
                   pokracuje:=true;
                 end
             end;
      inc(v)
    end;
j:=j-1;
end;
{--------------------Konec deklaraci procedur a funkci-----------------------}
begin
  prikazovaradka(uzly,pocet);
  for i:=0 to 499 do
    begin
      new(seznam[i]);
      seznam[i]:=nil
    end;
  Vytvorseznam(seznam);
  if pocet > 0 then
  for i:=1 to pocet do
    begin
      start:=Najdistanici(uzly[i],seznam[kodovani(uzly[i])]);
        if start<>nil then
          begin
            start^.uzel:=i;
            start^.ohodnoceni:=0;
          end;
    end;
  Projdigraf(seznam);
  for i:=0 to 499 do
    while seznam[i]<>nil do
      begin
        seznam[i]^.jmeno:=seznam[i]^.jmeno+'                                        ';
        if seznam[i]^.uzel = 0 then
          writeln(seznam[i]^.jmeno,'-1',#9,'nostation')
        else
          writeln(seznam[i]^.jmeno,seznam[i]^.ohodnoceni,#9,uzly[seznam[i]^.uzel]);
          seznam[i]:=seznam[i]^.dalsi;
      end;
end.