program Distance;

{ copyright 2002, Jan KUDR jan.kudr@centrum.cz; distribute under GPL

usage:cat test.tt | distselect 'Hostivice;' 30

Warning:this is not a dijkstra, but something very borken. 
}  

const max_del = 50;
      nula = -255;

type    p_stanice = ^t_stanice;
        p_spojeni = ^t_spojeni;

        t_stanice = record
                  nazev: string[max_del]; { nazev stanice }
                  value: integer; { ohodnoceni vrcholu }
                  dalsi: p_stanice;
                  spojeni: p_spojeni;
                  end;

        t_spojeni = record
                  km: integer;
                  stanice: p_stanice;
                  dalsi: p_spojeni;
                  end;

var start,najdi: p_stanice;
    i,j,k: integer;
    spoj: p_spojeni;
    s,t:string;
    p:pointer;

{ najde stanici dle zadaneho nazvu a vrati ukazatel na ni}
function NajdiStanici(stanice: p_stanice; nazev: string):p_stanice;
var pom: p_stanice;
    s:string;
begin
     pom:=nil;
     while (stanice<>nil) do
     begin
          if (stanice^.nazev=nazev) then
          begin
               pom:=stanice;
               stanice:=nil;
          end
          else
              stanice:=stanice^.dalsi;
     end;
     NajdiStanici:=pom;
end;

{ najde zda uz je zapsano spojeni ve stanici pokud ne tak ho zapise }
procedure PridejSpojeni(stanice,nova: p_stanice; nazev: string; delka: integer);
var spojeni,nove_spojeni: p_spojeni;
    exist: boolean;
begin
     spojeni:=stanice^.spojeni;
     exist:=false;

     while (spojeni<>nil) do
     begin

          if ((spojeni^.stanice^.nazev=nazev) AND (spojeni^.km=delka)) then
          begin
               spojeni:=nil; { stanice tam uz je }
               exist:=true;
          end
          else
               spojeni:=spojeni^.dalsi;
     end;

     if (exist=false) then {stanice tam jeste neni }
     begin
          new(nove_spojeni);
          nove_spojeni^.dalsi:=stanice^.spojeni;
          nove_spojeni^.km:=delka;
          nove_spojeni^.stanice:=nova;
          stanice^.spojeni:=nove_spojeni;
     end;
end;

{ prida stanici do seznamu }
procedure PridejStanici(var stanice: p_stanice; nazev: string);
var nova: p_stanice;
begin
     new(nova);
     nova^.nazev:=nazev;
     nova^.value:=-1;
     nova^.dalsi:=stanice;
     nova^.spojeni:=nil;
     stanice:=nova;
end;

procedure Graf(var stanice: p_stanice);
var ch: char;
    km,tab,code,main_st: integer;
    nazev,str,s_km: string;
    f:text;
    pom,old:p_stanice;
begin
     i:=0;

     old:=stanice;
     tab:=0;
     main_st:=0;

     while (not eof) do
     begin
          read(ch);

          if (ch='#') then main_st:=0;

          if (ch=#9) then
          begin
               tab:=tab+1;

               if (tab=3) then
               begin
                    km:=0;
                    s_km:='';
                    tab:=0;
                    read(ch);

                    while (ch in ['0'..'9']) do
                    begin
                         s_km:=s_km+ch;
                         read(ch);
                    end;

                    Val(s_km,km,code);

                    read(ch);
                    nazev:='';

                    while (ch<>#10) do
                    begin
                         if (ch<>#13) then nazev:=nazev+ch;
                         read(ch);
                    end;

                    pom:=NajdiStanici(stanice,nazev);
                    if (pom=nil) then
                    begin
                        PridejStanici(stanice,nazev);
                        pom:=stanice;
                    end;

                    if (km=0) AND (main_st=1) then km:=nula;

                    if (km<>0) then
                    begin
                         PridejSpojeni(old,pom,nazev,km);
                    end;

                    if (km=0) AND (main_st=0) then main_st:=1;

                    old:=pom;
               end;
          end;
     end;
end;

procedure ProjdiGraf(stanice: p_stanice; km,ujel: integer);
var spoj: p_spojeni;
    act_km: integer;
    pom: p_stanice;
    k,j:string;
begin
     spoj:=stanice^.spojeni;

     if ((stanice^.value>ujel) OR (stanice^.value=-1)) then
     begin
	if (stanice^.value=-1) then writeln(ujel, '	', stanice^.nazev);
	stanice^.value:=ujel;
     end;

     while (spoj<>nil) do
     begin

          if (spoj^.km<>nula) then
             act_km:=ujel+spoj^.km
          else
              act_km:=ujel;

          if (act_km<=km) AND ((spoj^.stanice^.value>act_km) OR (spoj^.stanice^.value=-1)) then
             ProjdiGraf(spoj^.stanice,km,act_km);
          spoj:=spoj^.dalsi;
     end;
end;

begin
     start:=nil;
     Graf(start);

     i:=1;
     s:=''; t:='';

     while (paramstr(i)<>'') do
     begin
          t:=t+paramstr(i);
          j:=length(t);

          if (t[j]=';') then
          begin
               s:=Copy(t,1,j-1);
               t:='';
          end;

          i:=i+1;
     end;

     val(t,i,j);
    
          najdi:=NajdiStanici(start,s);
          if (najdi<>nil) then
          begin

               str(i,s);
               ProjdiGraf(najdi,i,0);

               { promaze stanice vzdalenosti na -1 }
               najdi:=start;
               while (najdi<>nil) do
               begin
                    najdi^.value:=-1;
                    najdi:=najdi^.dalsi;
               end;
          end
          else
  	       writeln('Stanice ',s,' neni v seznamu.');
end.
