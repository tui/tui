{
REMOVER
(c) 2001 Michal Danihelka <danihelka@mbox.dkm.cz>
produced under GNU-GPL

This program works with database .tt. User enters the list of stations
which will be removed from the database .tt. If the program removes the station
all others data will be changed. Exactly it changes the times of arrivals and
departures and the distances between all stations. When remains only one station
in one route, this route will be removed. The notes in routes are removed only when
all route is removed. In other cases they do not change. Whole bad input data are
written to the beginning of output.

Usage: remover [-n] <list of stations to remove> <input> <standard output>

Example: remover 'Milano Certosa' 'Berlin Wannsee' < train.tt > train2.tt

or
	 remover @banned.list < train.tt > train2.tt

Note: If you enter (in .tt) wrong data (for example time of arrival) program will
change this information to 0.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

For more detailed informations see gnu.txt
}

Program Remover;

const max_sta = 150; {maximum length of station's name}
      max_tra = 140; {maximum length of train's name}
      max_arr = 5; {maximum length of arrival of train}
      max_dep = 5; {maximum length of departure of train}
      max_com = 5; {maximum length of comment of station}
      max_kil = 4; {maximum length of distance between stations}
      max_num = 100000; {maximum of stations to remove}
      linux = true; {when it is true, it never writes #13}
      no_enter_write = true; {when it is true - if you do not enter any correct stations
                               to remove the program will write the same file}

var arr_sta: array [1..max_num] of string [max_sta];
    remove,inp_fil: integer; {number of stations to remove}
   negate : boolean;

procedure help (go:boolean);
var a:char;
begin
   if no_enter_write then
      while not eof do
      begin
         read (a);
         write (a)
      end
   else
      begin
         if go then
            writeln ('No station which you entered is in database')
         else
            writeln ('You did not enter any station to remove');
         writeln ('Usage: remover <list of stations to remove> <input> <standard output>');
         writeln ('Alternate usage: remover @filename_with_list <input> <standard output>');
	 writeln ('Example: remover ''Milano Certosa'' ''Berlin Wannsee'' <c:\train.tt >d:\train2.tt');
	 writeln ('Notice: you have to escape '' so that it is not eaten by the shell!\n');
      end;
   halt
end;

{this procedure assigns all correct stations to remove to array ARR_STA}
procedure add_stations;
var i: integer;
    bad:boolean; {is true when something of input data is not correct}

begin
   bad:=false;
   for i:=inp_fil to paramcount do
   if length (paramstr (i)) <= max_sta then
      begin
         inc (remove);
         arr_sta [remove]:= paramstr (i);
      end
   else
      begin
         if not bad then
            begin
               write ('Bad input data');
               bad:=true
            end;
         write (' - ',paramstr (i));
      end;
   if bad then
      begin
         write (' - End of bad input data');
         if not linux then
            write (#13);
         write (#10);
      end;
end;

procedure from_file;
var t:text;
    x:string;
begin
   assign (t,copy (paramstr (inp_fil),2,length (paramstr (inp_fil)) - 1));
   reset (t);
   while not eof (t) do
   begin
      inc (remove);
      readln (t,x);
      arr_sta [remove]:=x
   end;
   inc (inp_fil);
   close (t)
end;

{sort all stations in ARR_STA by ordinal numbers}
procedure sort_stations (lim_lef,lim_rig:integer);
var lef,rig: integer;
    mid,hel: string;
begin
   mid:= arr_sta [(lim_lef + lim_rig) div 2];
   lef:= lim_lef;
   rig:= lim_rig;
   while lef < rig do
   begin
      while arr_sta [lef] < mid do inc (lef);
      while arr_sta [rig] > mid do dec (rig);
      if lef <= rig then
         begin
            hel:= arr_sta [lef];
            arr_sta [lef]:= arr_sta [rig];
            arr_sta [rig]:= hel;
            inc (lef);
            dec (rig)
         end;
   end;
   if lim_lef < rig then sort_stations (lim_lef, rig);
   if lim_rig > lef then sort_stations (lef, lim_rig)
end;

{main procedure which change the database}
procedure change_database;
var find: boolean; {is true when the current station is to remove}
    nam: string [max_tra];
    arr: array [0..1] of string [max_arr];
    dep: array [0..1] of string [max_dep];
    com: array [0..1] of string [max_com];
    kil: array [0..1] of string [max_kil];
    sta: array [0..1] of string [max_sta];
    i:integer;
    a:char;

   {convert time value from string to its numerical value}
   function str2int (x:string):integer;
   var i,j,k,l:integer;
   begin
      if x[1] = '+' then
         begin
            val (copy (x,2,length (x) - 1),i,j);
            if j = 0 then
               str2int:=i
            else
               str2int:=0
         end
      else
         if pos (':',x) > 0 then
            begin
               val (copy (x,1,pos (':',x) - 1),i,j);
               val (copy (x,pos (':',x) + 1,length (x) - pos (':',x)),k,l);
               if (j = 0) and (l = 0) then
                  str2int:=60 * i + k
               else
                  str2int:=0
            end
         else str2int:=0
   end;

   {convert time value from numerical value to string with :}
   function int2str (i:integer):string;
   var x,y:string;
   begin
      str ((i mod 1440) div 60,x);
      str (i mod 60,y);
      if length (y) = 1 then
         y:='0' + y;
      int2str:=x + ':' + y
   end;

   {convert time value from numerical value to string with +}
   function int2plus (i:integer):string;
   var x: string;
   begin
      if i = 0 then
         int2plus:=''
      else
         begin
            str (i,x);
            int2plus:='+' + x
         end;
   end;

   {function is true when the assigned station is in ARR_STA}
   function find_station (x:string):boolean;
   var lef,rig,mid:integer;
   begin
      if x[length (x)] = #13 then
         x:=copy (x,1,length (x) - 1);
      lef:=1;
      rig:=remove;
      repeat
      mid:=(lef + rig) div 2;
      if x > arr_sta [mid] then
         lef:=mid + 1
      else
         rig:=mid - 1
      until (arr_sta [mid] = x) or (lef > rig);
      if arr_sta [mid] = x then
         find_station:=true
      else
         find_station:=false
   end;

   {add one time to another}
   function add_times (x,y:string):string;
   begin
      if y[1] = '+' then
         if x[1] = '+' then
            add_times:=int2plus (str2int (x) + str2int (y))
         else
            if pos (':',x) > 0 then
               add_times:=int2str (str2int (x) + str2int (y))
            else
               add_times:=int2plus (str2int (y))
      else
         if pos (':',y)>0 then
            add_times:=int2str (str2int (y))
         else
            if pos (':',x) > 0 then
               add_times:=int2str (str2int (x))
            else
               add_times:=int2plus (str2int (x))
   end;

   {add one number of kilometres to another}
   function add_kilometres (x,y:string):string;
   var i,j,k,l: integer;
       z:string[max_kil];
   begin
      val (x,i,k);
      val (y,j,l);
      if (k = 0) and (l = 0) then
         begin
            str (i + j,z);
            add_kilometres:=z
         end
      else
         add_kilometres:='0'
   end;

   {read one string, you assign ordinal value of character 'I' and all characters
   from now to one character before 'I' are that string}
   function read_var (i:integer): string;
   var x: string;
   begin
      x:='';
      while (ord (a) <> i) and (not eof) do
      begin
         x:=x + a;
         read (a)
      end;
      read (a);
      read_var:=x
   end;

   {read one arrival, one departure, one comment, one number of kilometres
    and one name of station (so it has name line)}
   procedure read_line (i:integer);
   begin
      arr[i]:=read_var (9);
      dep[i]:=read_var (9);
      com[i]:=read_var (9);
      kil[i]:=read_var (9);
      sta[i]:=read_var (10)
   end;

   {write one line over another (in case that the 'another' line is to remove)}
   procedure rewrite_line (j:integer);
   var hel:string;
   begin
      hel:=read_var (9);
      if length (hel) = 0 then
         begin
            dep[j]:= add_times (add_times (arr[j],dep[j]),read_var (9));
            arr[j]:='';
         end
      else
         begin
            arr[j]:= add_times (add_times (arr[j],dep[j]),hel);
            dep[j]:= read_var (9);
         end;
      com[j]:=read_var (9);
      if (kil[j] = '0') and (i = 0) and (j = 0) then
         hel:=read_var (9)
      else
         kil[j]:= add_kilometres (kil[j],read_var (9));
      sta[j]:=read_var (10)
   end;

var first :boolean;

   {write one line to standard output}
   procedure write_line (i:integer);
   begin
      if (first) then
	 write (arr[i],chr(9),dep[i],chr(9),com[i],chr(9),kil[i],chr(9),sta[i],chr(10))
      else
	 write (arr[i],chr(9),dep[i],chr(9),com[i],chr(9),'+',kil[i],chr(9),sta[i],chr(10));
      first := false;
   end;

   {read line which is not to remove}
   procedure read_correct_line (j:integer);
   begin
      read_line (j);
      find:=(find_station (sta[j])) xor negate;
      while ((ord (a) = 9) or (ord (a) = 43) or (a in ['0'..'9'])) and find do
      begin
         rewrite_line (j);
         find:=(find_station (sta[j])) xor negate;
      end;
   end;

begin
   read (a);
   while not eof do
   begin
      i:=0;
      while (a <> '#') and not eof do
	 read (a);
      if a = '#' then
         begin
            nam:=read_var (10);
	    first := true;
            if ((ord (a) = 9) or (ord (a) = 43) or (a in ['0'..'9'])) then
               begin
                  read_correct_line (0);
                  if not find and ((ord (a) = 9) or (ord (a) = 43) or (a in ['0'..'9'])) then
                     begin
                        read_correct_line (1);
                        if not find then
                           begin
                               write (nam,chr (10));
                               dep[0]:=add_times (arr[0],dep[0]);
                               arr[0]:='';
                               write_line (0);
                               while ((ord (a) = 9) or (ord (a) = 43) or (a in ['0'..'9'])) do
                               begin
                                  read_correct_line (i mod 2);
                                  inc (i);
                                  if not find then
                                     write_line ((i) mod 2)
                               end;
                               if (not find) or (i = 0) then
                                  inc (i);
                               if arr[i mod 2] = '' then
                                  arr[i mod 2]:=dep[i mod 2];
                               dep[i mod 2]:='';
                               write_line (i mod 2);
                               while (a <> '#') and  not eof do
                               begin
                                  write (a);
                                  read (a)
                               end;
                               if eof then
                                  write (a)
                           end;
                     end;
               end;
         end;
   end;
end;

{MAIN PROGRAM}
begin
remove:=0;
negate:=false;
inp_fil:=1;
if paramcount = 0 then help (false);
if paramstr(inp_fil) = '-n' then
begin
   negate:=true;
   inc(inp_fil);
end;   
if paramstr(inp_fil)[1] = '@' then
   from_file;
add_stations;
if remove = 0 then help (true);
sort_stations (1,remove);
change_database;
end.