{Copyright 2002 Alexander Kuzmin}
{Alexander.Kuzmin@seznam.cz}
{Distribute under GPL v2}

program Jizdni_rad;

type
Zeit = record
x:byte;
y:byte;
end;

const RozmerBloku=32000;

var a:array[1..RozmerBloku] of char;
    nazvy:array[1..50] of string;
    nazvy1:array[1..50] of string;
    time:array[1..3000] of Zeit;
    tramvaj,hledany:string;
    vstupni,docasny,zastavka,tram:file;
    jecas,jetam,ZastavkyStejne:boolean;
    kdevsouboru:longint;
    posledni:integer;
    indexzastavky,numread,pozice,krizek,delkazbytku,zarazkacas,zarazkazastavka,zarazkazastavka1,kdejsem:word;
    velikost:real;
    percent,kolikpricist:byte;


procedure Prectisoubor(var f:file);

begin
     Reset(f,1);

     seek(f,kdevsouboru);
     blockread(f,a[delkazbytku+1],sizeof(a)-delkazbytku,numread);
     Close(f);
     kdevsouboru:=kdevsouboru+(sizeof(a)-delkazbytku);
end;

function Najdikrizek:integer;

var i:integer;
    probehlasponjednou:boolean;
begin
     probehlasponjednou:=false;
     for i:=1 to delkazbytku do
     begin
          if (a[i]=#35) and (probehlasponjednou) then
          begin
               Najdikrizek:=i;
               exit;
          end;
          probehlasponjednou:=true;
     end;
     Najdikrizek:=1;
end;

function Najdi(hledany:string;kde:array of char):boolean; {hleda zastavku v poli}
var x,y,i,znakunalezeno:integer;
    naselznak:boolean;
begin
     naselznak:=false;
     x:=length(hledany);
     i:=1;
     znakunalezeno:=0;
     y:=0;

     while (i<>x) and (y<pozice+1) do
     begin

          if hledany[i]=kde[y] then
          begin

               y:=y+1;
               i:=i+1;
               naselznak:=true;
          end

          else
          begin
               if naselznak=true then
               begin
                    naselznak:=false;
                    i:=1;
                    znakunalezeno:=0;

               end;
               y:=y+1;
          end;

          if (naselznak=true) and (i=x) then {dosel na konec hledaneho a nasel to}
          begin
               Najdi:=true;
               exit;
          end;

     end;
     Najdi:=false;{nic nenasel}
end;

procedure Nahravej(var f:file);

begin
     blockwrite(f,a,krizek-1);
end;


procedure Posun(odkud:integer);

begin
     move(a[odkud],a[1],delkazbytku-krizek+1);
     delkazbytku:=delkazbytku-krizek+1;
end;




function Cas:boolean;

var i,i1,cisloX,cisloY:word;

begin

	i:=1;

	while (a[i]<>#9) do
	i:=i+1;

	i1:=i+1;

    if i1>delkazbytku then
    begin
    	cas:=false;
        exit;
    end;


	cisloX:=0;
    while ((ord(a[i1])) <= ord('9') ) and ((ord(a[i1])) >= ord('0')) do
    begin
    	if (cisloX<>0) then
    	begin
        	cisloX:=(cisloX*10) + (ord(a[i1])-ord('0'));
        end
        else
        begin
    		cisloX:=(ord(a[i1])-ord('0'));
        end;

        i1:=i1+1;

        if i1>delkazbytku then
    	begin
    		cas:=false;
        	exit;
    	end;
    end;


    i1:=i1+1;

    if i1>delkazbytku then
    begin
    	cas:=false;
        exit;
    end;

    cisloY:=0;

    while ((ord(a[i1])) <= ord('9') ) and ((ord(a[i1])) >= ord('0')) do
    begin
    	if (cisloY<>0) then
        begin
        	cisloY:=(cisloY*10) + (ord(a[i1])-ord('0'));
        end
        else
        begin
    		cisloY:=(ord(a[i1])-ord('0'));
        end;

        i1:=i1+1;

        if i1>delkazbytku then
    	begin
    		cas:=false;
        	exit;
    	end;

    end;
    time[zarazkacas].x:=cisloX;
    time[zarazkacas].y:=cisloY;
    zarazkacas:=zarazkacas+1;
    cas:=true;
end;


function SrovnejZastavky:boolean;

var x1,x2:word;
    i:longint;
	pomocny:string;
    tabelator:byte;
    skoncil,jezastavka:boolean;

begin
    pomocny[0]:=chr(255);
    i:=1;
    x2:=1;
    skoncil:=false;
    while (a[i]<>#10) do

    begin
    	i:=i+1;
    end;
    i:=i+1;

	while not skoncil do

    begin

        while (a[i]<>#10) do
        begin
        	if (tabelator=0) and (a[i]='+') then
        	begin
				skoncil:=true;
        	end;

        	if (a[i]='+') then

        	begin
        		tabelator:=0;
        	end;



        	if (a[i]=#9) then tabelator:=tabelator+1;
       		if (ord(a[i])>64) or (ord(a[i])=32) then

       		begin
           		pomocny[x2]:=a[i];
                x2:=x2+1;
                i:=i+1;
        	end
            else
            begin
                i:=i+1;
            end;

        end;
        i:=i+1;
        pomocny[0]:=chr(x2-1);
        nazvy1[zarazkazastavka1]:=pomocny;
        zarazkazastavka1:=zarazkazastavka1+1;
        x2:=1;
        pomocny[0]:=chr(255);
        tabelator:=0;
    end;
    for i:=1 to zarazkazastavka-1 do
    begin
    	if nazvy[i]=nazvy1[i] then
        begin
        	SrovnejZastavky:=true;
        end
        else

        begin
        	SrovnejZastavky:=false;
            exit;
        end;


    end;



end;



procedure Zastavky;

var i,x2:word;
	x1:byte;
	pomocny:array[1..50] of char;
    pomocny1:string;
    tabelator:byte;
    skoncil,jezastavka:boolean;

begin
    pomocny1[0]:=chr(255);
    fillchar(pomocny,sizeof(pomocny),'#');
    i:=1;
    x2:=1;
    kolikpricist:=0;
    tabelator:=0;
    jezastavka:=false;
    skoncil:=false;
    while (a[i]<>#10) do

    begin
    	i:=i+1;
    end;
    i:=i+1;
	while not skoncil do

    begin



        while (a[i]<>#10) do


        begin

            if (tabelator=0) and (a[i]='+') then
            begin
				skoncil:=true;
            end;

            if (a[i]='+') and (jezastavka=false) then
			begin
				kolikpricist:=kolikpricist+(ord(a[i+1]))-(ord('0'));
        	end;

            if (a[i]='+') then

            begin
            	tabelator:=0;
            end;



            if (a[i]=#9) then tabelator:=tabelator+1;

        	if (ord(a[i])>64) or (ord(a[i])=32) then

        	begin
            	pomocny[x2]:=a[i];
                x2:=x2+1;
                i:=i+1;
        	end
            else
            begin
                i:=i+1;
            end;

        end;
        pozice:=x2;
        i:=i+1;
        tabelator:=0;
        x2:=1;

        if not jezastavka then
        begin
        	jezastavka:=Najdi(hledany,pomocny);
            if jezastavka then
            begin
                indexzastavky:=zarazkazastavka;
            end;
        end;

        x1:=1;
        pomocny1[0]:=chr(255);
        while (pomocny[x1]<>('#')) do

        begin

        	pomocny1[x1]:=pomocny[x1];
            x1:=x1+1;
        end;


        pomocny1[0]:=chr(x1-1);
        nazvy[zarazkazastavka]:=pomocny1;
        zarazkazastavka:=zarazkazastavka+1;
        fillchar(pomocny,sizeof(pomocny),'#');

    end;

end;

procedure Scitej;
var i,i1,i2:word;

begin


	i:=time[zarazkacas-1].y+kolikpricist;
    i1:=time[zarazkacas-1].x;

    if i>59 then

    begin
    	i:=i-60;
        i1:=i1+1;
    end;

    if i1>23 then

    begin
    	i1:=i1-24;
    end;
    time[zarazkacas-1].x:=i1;
    time[zarazkacas-1].y:=i;



end;

procedure Setrid(var pole:array of Zeit);
var i, x, y, kolik, rozsah: byte;
    tridi:boolean;

begin

tridi:=true;
x:=1;

while (tridi) and (x<>zarazkacas-1) do
begin
	tridi:=false;
    for i:=1 to (zarazkacas-1) do
    begin
    	if ((pole[i].x)>(pole[i+1].x)) or ( ((pole[i].x)=(pole[i+1].x)) and ((pole[i].y)>(pole[i+1].y)) ) then
        begin
        	y:=pole[i].x;
            pole[i].x:=pole[i+1].x;
            pole[i+1].x:=y;

            y:=pole[i].y;
            pole[i].y:=pole[i+1].y;
            pole[i+1].y:=y;

            tridi:=true;
        end;
    end;

inc(x);

end;

end;


procedure Prevod(coprevest:string);
var i,y:word;

begin
    y:=1;
	for i:=pozice to (length(coprevest)+pozice) do
    begin
        if i>RozmerBloku then
        begin
            blockwrite(zastavka,a,RozmerBloku);
            pozice:=1;
            fillchar(a,sizeof(a),'#');
        end;
      	a[i]:=coprevest[y];
        y:=y+1;
    end;
    pozice:=i;
end;

procedure ZastavkyHtml;
var pomocny:string;
	i:word;

begin
	pomocny[0]:=chr(255);
    pomocny:='<left> <table width=100% cellspacing="0" cellpadding="0" border="0"> ';
    Prevod(pomocny);

    for i:=1 to zarazkazastavka-1 do
    begin
    	pomocny:='<tr> <td> ';
        Prevod(pomocny);

        if i=indexzastavky then
        begin
            pomocny:='<b> ';
            Prevod(pomocny);
        end;

        pomocny:=nazvy[i];
        Prevod(pomocny);

        if i=indexzastavky then
        begin
            pomocny:='</b> ';
            Prevod(pomocny);
        end;

        pomocny:='</td> </tr> ';
        Prevod(pomocny);


    end;
    pomocny:='</table>';
    Prevod(pomocny);

end;

procedure CasyHtml;

var pomocny:string;
	i,i1:word;

begin

   	i:=1;
    pomocny:='<right> <table width=100% cellspacing="0" cellpadding="1" border="1"> ';
    Prevod(pomocny);
    pomocny:='<tr> <td width=20> <b> ';
    Prevod(pomocny);
    str(time[i].x,pomocny);
    Prevod(pomocny);
    pomocny:='</b> </td> <td> ';
    Prevod(pomocny);
    if (time[i].y<10) then
    begin
       pomocny:='0';
       Prevod(pomocny);
    end;
    str(time[i].y,pomocny);
    Prevod(pomocny);
    pomocny:='</td> ';
    i1:=time[i].x;
	for i:=2 to zarazkacas-1 do
    begin
    	if time[i].x>i1 then
        begin
        	pomocny:='</tr> <tr> <td width=20> <b> ';
            Prevod(pomocny);
            str(time[i].x,pomocny);
            Prevod(pomocny);
            pomocny:='</b> </td> <td> ';
            Prevod(pomocny);
	    if (time[i].y<10) then
	    begin
	       pomocny:='0';
	       Prevod(pomocny);
	    end;
            str(time[i].y,pomocny);
            Prevod(pomocny);
            pomocny:='</td> ';
            Prevod(pomocny);
        end;

        if time[i].x=i1 then
        begin
        	pomocny:='<td> ';
            Prevod(pomocny);
      	    if (time[i].y<10) then
	    begin
	       pomocny:='0';
	       Prevod(pomocny);
	    end;
            str(time[i].y,pomocny);
            Prevod(pomocny);
            pomocny:='</td> ';
            Prevod(pomocny);
        end;
        i1:=time[i].x;

    end;
    pomocny:='</table>';
    Prevod(pomocny);

end;


procedure VypisHtml;
var pomocny:string;
	i:word;

begin
    pozice:=1;
    pomocny[0]:=chr(255);
    pomocny:='<html> <head> <title> Vypis pro zastavku ';
    Prevod(pomocny);
    pomocny:=hledany;
    Prevod(pomocny);
    pomocny:=', spoj ';
    Prevod(pomocny);
    pomocny:=tramvaj;
    Prevod(pomocny);
	pomocny:='</title> <body> <center> <table border="2" cellspacing="1" width="600"> <tr>';
    Prevod(pomocny);
    pomocny:='<td align=center colspan=2> ';
    Prevod(pomocny);
    pomocny:=tramvaj;
    Prevod(pomocny);
    pomocny:='</td> ';
    Prevod(pomocny);
    pomocny:='</tr> ';
    Prevod(pomocny);

    pomocny:='<td align=left width=50%> ';
    Prevod(pomocny);
	ZastavkyHtml;
    pomocny:='</td>';
    Prevod(pomocny);

    pomocny:='<td align=right width=50%> ';
    Prevod(pomocny);
	CasyHtml;
    pomocny:='</td>';
    Prevod(pomocny);

    pomocny:='</table> </body> </html> ';
    Prevod(pomocny);
    krizek:=Najdikrizek;
    blockwrite(zastavka,a,krizek-3);

end;


function NactiPar:string;
var i:integer;
	s:string;

begin


	s := '';
	for i:=1 to paramcount-2 do
	s := s +paramstr(i) +' ';
    dec(s[0]);
	nactipar:=s;

end;


begin
kdejsem:=0;
hledany:=NactiPar;

Assign(tram, paramstr(paramcount)+'tram.tt');
Assign(zastavka, paramstr(paramcount)+'vypis.htm');
tramvaj:=paramstr(paramcount-1);
writeln('Probiha vypis...');
Reset(tram,1);

Rewrite(zastavka,1);
numread:=1;
krizek:=1;
zarazkacas:=1;
zarazkazastavka:=1;
zarazkazastavka1:=1;
jecas:=true;
jetam:=false;
fillchar(nazvy1,sizeof(nazvy1),'#');
fillchar(time,sizeof(time),255);

Prectisoubor(tram);
delkazbytku:=RozmerBloku;
Zastavky;
jecas:=Cas;
Scitej;
krizek:=Najdikrizek;
posun(krizek);

while numread<>0 do

begin


     if krizek=1 then
     begin
          Prectisoubor(tram);
          delkazbytku:=RozmerBloku;
     end;

     krizek:=Najdikrizek;

     if (krizek<>1) then
     begin
     	ZastavkyStejne:=SrovnejZastavky;
        zarazkazastavka1:=1;
        fillchar(nazvy1,sizeof(nazvy1),'#');

        if ZastavkyStejne then
	begin
	        jecas:=Cas;
	end
	
	   else
     	begin
     		write ('Zastavky nejsou stejne, program da divny vystup');
     	end;

     end;


     if (jecas) then
     begin
     	Scitej;
     end;


     if (krizek<>1) then
     begin
	 	Posun(krizek);
     end;


end;
Setrid(time);
fillchar(a,sizeof(a),'#');
VypisHtml;
Close(zastavka);
writeln('Vypis dokoncen, byl vytvoren soubor vypis.htm');
end.