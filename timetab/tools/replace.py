#!/usr/bin/python
# Copyright 2008 Petr Machek, GPL

import sys

line = sys.stdin.readline()
dict = {}
while(line[0] != '#'):
	new, old = line.split('\t')
	dict[old] = new
	line = sys.stdin.readline()

while(line != ''):
	if line[0] in '\t+1234567890':
		chunks = line.split('\t')
		name = chunks[-1]
		if name in dict:
			chunks[-1] = str(dict[name]) + '\n'
		line = '\t'.join(chunks)
	sys.stdout.write(line)
	line = sys.stdin.readline()
