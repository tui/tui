#!/usr/bin/perl -w

# Copyright 2002, Roman Krejcik <roman.krejcik@ruk.cuni.cz>
# Copyright 2002, Pavel Machek <pavel@ucw.cz>
#
# Distribute under GPL v2 licence

use locale;

use constant TABLE => '<div align="center"><table width="95%" border=1 cellspacing=0 cellpadding=2>'."\n";
use constant TABLE_ONLY_A_D => '<div align="center"><table width="75%" border=1 cellspacing=0 cellpadding=2>'."\n";
use constant TABLE_COLS => '<col width="23%"><col width="20%"><col width="10%"><col width="10%"><col width="20%"><col width="17%">'."\n";
use constant TABLE_COLS_ONLY_A_D => '<col width="30%"><col width="25%"><col width="20%"><col width="25%">'."\n";
use constant TABLE_HEAD => "<tr align=\"center\" bgcolor=\"#0099FF\"><td><b>connection</b></td><td><b>from</b></td><td><b>arr.</b></td><td><b>dep.</b></td><td><b>to</b></td><td><b>notes</b></td></tr>\n";
use constant TABLE_HEAD_ONLY_A => "<tr align=\"center\" bgcolor=\"#0099FF\"><td><b>connection</b></td><td><b>from</b></td><td><b>arr.</b></td><td><b>notes</b></td></tr>\n";
use constant TABLE_HEAD_ONLY_D => "<tr align=\"center\" bgcolor=\"#0099FF\"><td><b>connection</b></td><td><b>arr.</b></td><td><b>to</b></td><td><b>notes</b></td></tr>\n";

$pattern_expected = 0;

foreach $arg (@ARGV) {
    if ($arg =~ /^-(.*)/) {
	if ($pattern_expected) {
	    print STDERR "Pattern expected after -l,-f, or -t.\n";
	    exit 1;
	}
	if ($1 eq "h" || $1 eq "-help") {
	    print "Usage:\n\n\tstation2html [-d|a] [-l|f|t pattern] station_name\n\nPrint the station timetable. Read data from the standard input and\nwrite in HTML to the standard output. Input data expected in .tt\nformat. Pattern can be arbitrary Perl regexp.\n\nOptions:\n\t-h print help\n\t-a only arrivals\n\t-d only departures\n\t-l print only lines which names match the pattern\n\t-f print only lines starting in station which match the pattern\n\t-t print only lines ending in station which match the pattern\n\n";
	    exit 0;
	} elsif ($1 eq "a") {
	    if (exists($option{"d"})) { 
		print STDERR "Too many parameters. Try -h for more information.\n"; 
		exit 1;
	    }
	    $option{"a"} = 1;
	} elsif ($1 eq "d") {
	    if (exists($option{"a"})) { 
		print STDERR "Too many parameters. Try -h for more information.\n"; 
		exit 1;
	    }
	    $option{"d"} = 1;
	} elsif ($1 eq "l") {
	    $pattern_expected = "l";
	} elsif ($1 eq "f") {
	    $pattern_expected = "f";
	} elsif ($1 eq "t") {
	    $pattern_expected = "t";
	} else {
	    print STDERR "Unknown option. Try -h for more information.\n";
	} 
    } else {
	if ($pattern_expected) { 
	    $option{$pattern_expected} = $arg; 
	    $pattern_expected = 0;
	} else {
	    if (!defined $station) { 
		$station = $arg; 
	    } else { 
		print STDERR "Too many parameters. Try -h for more information.\n";
		exit 1;
	    }
	}
    }
}

if (!defined($station)) {
    print STDERR "Station name missing. Try -h for more information.\n";
    exit 1;
}
print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n<html>\n<head>\n<title>Timetable for station $station.</title>\n</head>\n<body>\n";

if (exists($option{"a"})) {
    print "Arrivals to<div align=\"center\"><h2>$station</h2></div>\n";
} elsif (exists($option{"d"})) {
    print "Departures from<div align=\"center\"><h2>$station</h2></div>\n";
} else {
    print "Timetable for station<div align=\"center\"><h2>$station</h2></div>\n";
}

$free_note = 1;  #first free note nuber for use

while (<STDIN>) {
    if (/^\#\s*(.*)/) {
        &next_con;
	$time{"minute"} = 0;
	$time{"hour"} = 0;
	next;
    }
    if (/^\@\s*(\S*)\s+(\d+):(\d+)\s+(.*)/) {
	&next_con;
	$time{"minute"} = $3;
	$time{"hour"} = $2;
	@time_interval = split(/\s/, $4); 
	next;
    }
    if (/^([+\d:]*)\t([+\d:]*)\t([^\t]*)\t([-+\d]*)\t(.*)/) {
	$first = $5 if (!defined($first));
	$last = $5;
	my ($arr_time, $dep_time) = ($1, $2);
	$arr_time = &count_time($arr_time);
	$dep_time = &count_time($dep_time);
	if ($last eq $station) {
	    @line = ($arr_time, $dep_time); 
	}
	next;
    }
    if (/^[A-Z](.*)/) {
	if (defined($line[0]) && $time{"hour"} != 25) {
	    if (!exists($notes{"$1"})) {
		$notes{"$1"} = $free_note;
		$free_note++;
	    }
	    push(@con_notes, $notes{"$1"});
	}
	next;
    }
}

&next_con; #process last connection from input

while (($a, $b) = each(%notes)) { $rev_notes{"$b"} = $a; }
	   
&print_lines;
&print_notes;

print "</body>\n</html>";

sub next_con {   # add con to table and clear variables with attributes   
    if (defined($line[0])) {
	&add_line;
	$line[0] = undef;
    }
    $con_name = $1;
    @con_notes = ();
    @time_interval = ();
    $first = undef;
}

sub print_lines {
    if (exists($option{"a"}) or exists($option{"d"})) {
	print TABLE_ONLY_A_D . TABLE_COLS_ONLY_A_D;
    } else {
	print TABLE . TABLE_COLS . TABLE_HEAD;
    }
    if (exists($option{"a"})) {
	print TABLE_HEAD_ONLY_A;
    }
    if (exists($option{"d"})) {
	print TABLE_HEAD_ONLY_D;
    }
    foreach $l (sort compare_times @lines) {
	if (exists($option{"a"})) {
	    print "<tr><td><a href=\"connection.cgi?name=$$l[0]\">$$l[0]</a></td><td><a href=\"station.cgi?name=$$l[1]\">$$l[1]</a></td><td>$$l[2]</td><td>";
	} elsif (exists($option{"d"})) {
	    print "<tr><td><a href=\"connection.cgi?name=$$l[0]\">$$l[0]</a></td><td>$$l[3]</td><td><a href=\"station.cgi?name=$$l[4]\">$$l[4]</a></td><td>";
	} else {
	    print "<tr><td><a href=\"connection.cgi?name=$$l[0]\">$$l[0]</a></td><td><a href=\"station.cgi?name=$$l[1]\">$$l[1]</a></td><td>$$l[2]</td><td>$$l[3]</td><td><a href=\"station.cgi?name=$$l[4]\">$$l[4]</a></td><td>";
	}
	if (@{$$l[5]} == 0) {
	    print "&nbsp;";  #no notes in this line
	} else {
	    foreach $n ( @{$$l[5]}) {
		if (defined($n)) {		   
		    $text=$rev_notes{"$n"};
		    print "<a href=\"#n$n\" title=\"$text\">$n</a> ";
		}
	    }
	}
	print "</td></tr>\n";
    }
    print "</table></div><p>\n";
}

sub compare_times {  
    my @aa = @$a;
    my @bb = @$b;
    if ($aa[3] eq "&nbsp;") {$aa[3] = $aa[2]; }
    if ($bb[3] eq "&nbsp;") {$bb[3] = $bb[2]; }
    my (@aaa) = split(/:/,$aa[3]);
    my (@bbb) = split(/:/,$bb[3]);
    if (($aaa[0] <=> $bbb[0]) == 0) { # hours comparation
	return ($aaa[1] <=> $bbb[1]);
    } else { return ($aaa[0] <=> $bbb[0]); }
}

sub add_line {
    if ($time{"hour"} == 25 and $time{"minute"} == 0) { return; }
    my @n_list = ();
    while (@con_notes) {
	push(@n_list, shift(@con_notes));
    }
    @n_list = sort compare_notes @n_list;
    if (exists($option{"f"})) {
	if ($first !~ /$option{"f"}/) { return; }
    }
    if (exists($option{"t"})) {
	if ($last !~ /$option{"t"}/) { return; }
    }
    if ($station eq $first) {   #connection starts in station
	if (exists($option{"a"})) { return; }
	$first = "&nbsp;";
    }
    if ($station eq $last) {  #connection ends in station
	if (exists($option{"d"})) { return; }
	$last = "&nbsp;";
    }
    if (exists($option{"l"})) {
	if ($con_name !~ /$option{"l"}/) { return; }
    }
    push(@lines, [$con_name,  $first, $line[0], $line[1], $last, \@n_list]);
    my ($offset) = 0;
    foreach $interval (@time_interval) {  #process @ list, if is common # format, @time_interval is empty
	my ($count, $inter) = split(/x/, $interval);
	for ($i = 0; $i< $count; $i++) {
	    $offset += $inter;
	    push(@lines, [$con_name, $first, &make_time_string($line[0],$offset), &make_time_string($line[1],$offset), $last, \@n_list]);
	} 
    }
}

sub make_time_string {  #makes XX:XX string from hash %time
    if ($_[0] eq "\&nbsp;") { return "\&nbsp;" }
    my ($base, $offset) = ($_[0], $_[1]);
    my ($time_h, $time_m) = split(/:/, $base);
    my $ret =  $time_h+int($offset/60)+int(($time_m+($offset % 60))/60).":";
    my $min = (($time_m+($offset % 60)) % 60);
    if ($min < 10) {
	return $ret.'0'.$min;
    } else {
	return $ret.$min;
    }
}


sub print_notes {
    print "Notes:<br>\n<ul>\n";
    foreach $key (sort compare_notes (keys(%rev_notes))) {
       printf("<li><a name=\"n$key\">$key</a>  ".$rev_notes{"$key"}."\n");
    }
    print "</ul>\n";

}

sub compare_notes { #numerical comparation
    return -1 if (int($a) < int($b));
    return 1 if (int($a) > int($b));
    return 0;
}

sub count_time { #in $_[0] time string from file (xx:xx or +x), finally time save to %time 
    if ($_[0] =~ /^\s*$/) { return "&nbsp;"; }
    if ($time{"hour"} == 25 and $time{"minute"} == 0) { return "&nbsp;"};
    if ($_[0] =~ /^(\d+):(\d+)$/) {
	$time{"hour"} = $1;
	$time{"minute"} = $2;
	return $_[0];
    }
    if ($_[0] =~ /^\+(\d+)$/) {
	$time{"minute"} += $1;
	if ($time{"minute"} > 59) {
	    $time{"hour"} += int($time{"minute"} / 60);
	    $time{"minute"} = $time{"minute"} % 60;
	}
	if ($time{"minute"} < 10) {
	    return  $time{"hour"}.":0".$time{"minute"};
	} else {
	    return  $time{"hour"}.":".$time{"minute"};
	}
	
    }
    print STDERR "Unknown input file format\n";
    exit 1;
}
