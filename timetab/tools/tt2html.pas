program tt2html;
{ Distribute under GPL v. 2 or later }
{ Copyright 2001 Petr Cermak <cermak01@seznam.cz> }
{ Copyright 2002 Pavel Machek <pavel@ucw.cz> }

var
   s	      : string;
   endwritten : boolean;
   remark     : boolean;
   for_FE     : boolean;	{ -f: Write links in strange format suitable for delphi fronend }
   links      : boolean;	{ -l: Write links suitable for station.cgi }
   noheaders  : boolean;	{ -n: Do not write <html> headers }

function nbsp(s : string) : string;
begin
   if s='' then
      nbsp:='&nbsp;'
   else
      nbsp:=s;
end; { nbsp }

procedure writehtmlhead;
begin
   if noheaders then exit;
   writeln('<HTML>');
   writeln('<HEAD>');
   writeln('</HEAD>');
   writeln('<BODY>');
end; { writehtmlhead }

procedure endtable;
begin
   writeln('<TR><TD COLSPAN=5>');
end; { endtable }

procedure endul;
begin
   writeln('</UL>');
   writeln('</TR><TD></TABLE><BR>');
end; { endul }

procedure writehtmlend;
begin
   if not endwritten then
      endtable;
   if remark then
      endul;
   if noheaders then exit;
   endwritten:=true;
   writeln('</BODY>');
   writeln('</HTML>');
end; { writehtmlend }

procedure writehead(s : string);
begin
   writeln('<TABLE border="1" cellspacing="0" cellpadding="0" width="70%" bgcolor="#DDEEFF">');
   writeln('<col width=10%><col width=10%><col width=10%><col width=10%><col width=50%>');
   writeln('<TR><TD COLSPAN=5>');
   writeln('<TABLE>');
   write('<TR><TD bgcolor="#2233BB">&nbsp;</TD><TD width="90%" bgcolor="#AABBFF">');
   s := copy(s, 3, length(s)-2);
   if (links) then
      write('<b><A HREF="connection.cgi?name=' + s +'"><font color=black>'+ s + '</font></a><b>')
   else
      write('<B>&nbsp;' + s + '</B>');
   writeln('</TD></TR>');
   writeln('</TABLE>');
   writeln('</TR></TD>');
end; { writehead }

procedure writeline(s : string);
var
   r : string;
begin
   r:=s;
   write('<TR>');
   write('<TD align="center">');
   write(nbsp(copy(s, 1, pos(#9, s)-1)));
   s:=copy(s, pos(#9, s)+1, length(s));
   write('</TD><TD align="center">');
   write(nbsp(copy(s, 1, pos(#9, s)-1)));
   s:=copy(s, pos(#9, s)+1, length(s));
   write('</TD><TD align="center">');
   write(nbsp(copy(s, 1, pos(#9, s)-1)));
   s:=copy(s, pos(#9, s)+1, length(s));
   write('</TD><TD align="center">');
   write(nbsp(copy(s, 1, pos(#9, s)-1)));
   s:=copy(s, pos(#9, s)+1, length(s));
   write('</TD><TD>');
   if for_FE then
      write('<A href="' + r + '">');
      if links then
	 write('<A href="station.cgi?name=' + s + '">')
      else
         write('<B>');
   write(s);
   if for_FE or links then
      write('</A>')
   else
      write('</B>');
   write('</TD>');
   writeln('</TR>');
end; { writeline }

procedure writeremark(s : string);
begin
   if not endwritten then
   begin
      endtable;
      endwritten:=true;
      writeln('<UL>');
      remark:=true;
   end;
   write('<LI>');
   write('<B>');
   write(copy(s, 2, length(s)-1));
   writeln('</B></LI>');
end;

var i:integer;
begin
   for i:=1 to paramcount do
   begin
      if (paramstr(i)='-f') then
	 for_FE := true;
      if (paramstr(i)='-l') then
	 links := true;
      if (paramstr(i)='-n') then
	 noheaders := true;
   end;
   readln(s);
   writehtmlhead;
   writehead(s);
   while not eof(Input) do
   begin
      readln(s);
      case s[1] of
        '#'           : 
      begin
         if not endwritten then
            endtable;
         endwritten:=false;
         if remark then
            endul
         else
            writeln('<BR>');
         remark:=false;
         writehead(s)
      end;
        'R', 'B', 'C' : 
        writeremark(s);
      else
         writeline(s);
      end; { case }
   end;
   writehtmlend;
end.
      
