unit Main;

interface

uses
  SysUtils, Types, Classes, Variants, QGraphics, QControls, QForms, QDialogs,
  Global, Exec, QStdCtrls, QExtCtrls, QComCtrls, QActnList, QMask, DateUtils;

type
  TMainForm = class(TForm)
    StatusBar: TStatusBar;
    Panel1: TPanel;
    pcPages: TPageControl;
    tbByName: TTabSheet;
    tbOneStation: TTabSheet;
    tbTwoStations: TTabSheet;
    Splitter: TSplitter;
    Panel2: TPanel;
    sbPosition: TScrollBar;
    Label1: TLabel;
    edConnName: TEdit;
    btSearch: TButton;
    Label6: TLabel;
    edStNameOS: TEdit;
    btSearchOS: TButton;
    Label7: TLabel;
    edSt1NameTS: TEdit;
    Label8: TLabel;
    edSt2NameTS: TEdit;
    btSearchTS: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    pnLeft: TPanel;
    pnLeftBt: TPanel;
    lbList: TListBox;
    btSave: TButton;
    btSaveAll: TButton;
    btClear: TButton;
    Label11: TLabel;
    Label12: TLabel;
    tbOptions: TTabSheet;
    GroupBox1: TGroupBox;
    btApply: TButton;
    edTTIFileName: TEdit;
    Label15: TLabel;
    edTTZFileName: TEdit;
    Label14: TLabel;
    TextBrowser: TTextBrowser;
    meDate1OS: TMaskEdit;
    meDate2OS: TMaskEdit;
    meTime1OS: TMaskEdit;
    meTime2OS: TMaskEdit;
    meDate1TS: TMaskEdit;
    meDate2TS: TMaskEdit;
    meTime1TS: TMaskEdit;
    meTime2TS: TMaskEdit;
    btReachable: TButton;
    btAnyOS: TButton;
    btClOS: TButton;
    btClTS: TButton;
    btAnyTS: TButton;
    sdSave: TSaveDialog;
    Label13: TLabel;
    meDefInt: TMaskEdit;
    odOpen: TOpenDialog;
    btOpenTTZ: TButton;
    btOpenTTI: TButton;
    tbAbout: TTabSheet;
    mmAbout: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SplitterMoved(Sender: TObject);
    procedure StConnEditKeyPress(Sender: TObject; var Key: Char);
    procedure StConnEditChange(Sender: TObject);
    procedure pcPagesPageChanging(Sender: TObject; NewPage: TTabSheet;
      var AllowChange: Boolean);
    procedure EditEnter(Sender: TObject);
    procedure lbListClick(Sender: TObject);
    procedure tbOneStationShow(Sender: TObject);
    procedure tbTwoStationsShow(Sender: TObject);
    procedure tbByNameShow(Sender: TObject);
    procedure btSearchClick(Sender: TObject);
    procedure TextBrowserHighlightText(Sender: TObject;
      const HighlightedText: WideString);
    procedure TextBrowserMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure tbOptionsShow(Sender: TObject);
    procedure sbPositionChange(Sender: TObject);
    procedure btReachableClick(Sender: TObject);
    procedure btAnyClick(Sender: TObject);
    procedure btClClick(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure btClearClick(Sender: TObject);
    procedure meDefIntExit(Sender: TObject);
    procedure btApplyClick(Sender: TObject);
    procedure btOpenClick(Sender: TObject);


  private
    { Private declarations }
    ListPos: integer;
    MaxNum: integer;
    Changing: boolean;
    CurList: PMyStrList;
    LastFocus: TObject;
    SelStation: string;
    RunID: integer;
    act_page : TActPage;
    inc_hours : TDateTime;
    DoSaveSettings : boolean;
    procedure SetDefaults;
    procedure SaveSettings;
    procedure LoadSettings;
    procedure StationTabEnter;
    procedure ConnTabEnter;
    procedure OptionsTabEnter;
    procedure FillList(s : string; pos : integer; strings : PMyStrList);
    function CheckDateTime(St1Name, St2Name : TEdit; Date1, Date2, Time1, Time2 : TMaskEdit) : boolean;
    procedure ShowItemNr(i : integer);
    procedure UpdateDB;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.xfm}

procedure TMainForm.FillList(s : string; pos : integer; strings : PMyStrList);
var
   i : integer;
begin
   ListPos:=pos;
   if s='' then
   begin
      for i:=0 to min(strings^.Count-1, 9) do
         lbList.Items[i]:=inttostr(i) + '. ' + strings^[i];
      MaxNum:=min(strings^.Count-1, 9);
      lbList.TopIndex:=0;
      exit;
   end;
   i:=0;
   while CompStr(s, strings^[pos]) and (i<10) do
   begin
     { if (i<10) then
      begin}
         lbList.Items.Add(inttostr(i) + '. ' + strings^[pos]);
         inc(i);
{      end;}
      {else
         lbList.Items.Add(strings^[pos]);}
      inc(pos);
      if pos>(strings^.Count-1) then break;
   end;
   MaxNum:=i-1;
end;

procedure TMainForm.UpdateDB;
var
   fl : string;
begin
   prg_to_file(comptab_exe+' -l ' + ttz_file + ' | sort', 'towns', '.lst', fl);
   mesta.LoadFromFile(fl);
   DeleteFile(fl);
   prg_to_file(comptab_exe+' -L ' + ttz_file + '| sort', 'trains', '.lst', fl);
   vlaky.LoadFromFile(fl);
   DeleteFile(fl);
end;

function TMainForm.CheckDateTime(St1Name, St2Name : TEdit; Date1, Date2, Time1, Time2 : TMaskEdit) : boolean;
var
   a, b : TDateTime;
   tmp : integer;
begin
   CheckDateTime:=false;
   if not mesta.Find(St1Name.text, tmp) then
   begin
      St1Name.SetFocus;
      ShowMessage(EWrongStation);
      exit;
   end;
   if not mesta.Find(St2Name.text, tmp) then
   begin
      St2Name.SetFocus;
      ShowMessage(EWrongStation);
      exit;
   end;
   try
      if Date1.Enabled then
         a:=StrToDate(Date1.Text)
      else
         a:=0;
   except
      begin
         Date1.SetFocus;
         ShowMessage(EWrongDate);
         exit;
      end;
   end;
   try
      if Date2.Enabled then
         b:=StrToDate(Date2.Text)
      else
         b:=0;
   except
      begin
         Date2.SetFocus;
         ShowMessage(EWrongDate);
         exit;
      end;
   end;
   try
      a:=a + StrToTime(Time1.Text)
   except
      begin
         Time1.SetFocus;
         ShowMessage(EWrongTime);
         exit;
      end;
   end;
   try
      b:=b + StrToTime(Time2.Text)
   except
      begin
         Time2.SetFocus;
         ShowMessage(EWrongTime);
         exit;
      end;
   end;
   if a>b then
   begin
      Date1.SetFocus;
      ShowMessage(EWrongTime + #13#10 + EWrongDate);
      exit;
   end;
   CheckDateTime:=true;
end;

procedure TMainForm.ShowItemNr(i : integer);
var
   fl, fl1 : string;
begin
   extract_from_tt(tt_file, i, fl);
   prg_to_file('cat ' + fl + ' | ' + tt2html_exe + ' -FE', 'html.conn.search', '.html', fl1);
   TextBrowser.LoadFromFile(fl1);
   TextBrowser.Refresh;
   DeleteFile(fl);
   DeleteFile(fl1);
end;

procedure TMainForm.SetDefaults;
var
   i : integer;
begin
   ttz_file:='train_cz.ttz';
   tti_file:='train_cz.tti';
   tt2html_exe:='tt2html';
   comptab_exe:='comptab';
   inc_hours:=StrToTime('1:00');;
   Settings[1]:=comptab_exe;
   Settings[2]:=tt2html_exe;
   Settings[3]:=ttz_file;
   Settings[4]:=tti_file;
   Settings[5]:=TimeToStr(inc_hours);
end;


procedure TMainForm.SaveSettings;
var
   i : integer;
   fil : system.text;
begin
   if not DoSaveSettings then
      exit;
   try
      system.assign(fil, get_home_dir + SettingsFile);
      system.rewrite(fil);
   except
      ShowMessage(EOpenSettings);
      exit;
   end;
   Settings[1]:=comptab_exe;
   Settings[2]:=tt2html_exe;
   Settings[3]:=ttz_file;
   Settings[4]:=tti_file;
   Settings[5]:=TimeToStr(inc_hours);
   for i:=1 to NSettings do
      writeln(fil, SettingName[i] + '=' + Settings[i]);
   writeln(fil, WEOF);
   system.close(fil);
end;

procedure TMainForm.LoadSettings;
var
   filename : string;
   fl : system.text;
   i : integer;
   r, s : string;
begin
   try
      system.assign(fl, get_home_dir + SettingsFile);
      system.reset(fl);
   except
      if MessageDlg(MBConfirmation, mtConfirmation,
         [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
         DoSaveSettings:=true;
         SaveSettings;
         try
            system.reset(fl);
         except
            ShowMessage(EOpenSettings);
            exit;
         end;
      end
      else
      begin
         DoSaveSettings:=false;
         exit;
      end;
   end;
   DoSaveSettings:=true;
   readln(fl, s);
   while not eof(fl) do
   begin
      r:=copy(s, 1, pos('=', s)-1);
      for i:=1 to NSettings do
         if r=SettingName[i] then break;
      if (i=NSettings) and (SettingName[i]<>r) then
      begin
         ShowMessage(EWrongSetting + r);
         readln(fl, s);
         continue;
      end;
      Settings[i]:=copy(s, pos('=', s)+1, length(s));
      readln(fl, s);
   end;
   system.close(fl);
   if Settings[1]<>'' then
   comptab_exe:=Settings[1];
   tt2html_exe:=Settings[2];
   ttz_file:=Settings[3];
   tti_file:=Settings[4];
   inc_hours:=StrToTime(Settings[5]);
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
   fl : string;
begin
   DoSaveSettings:=false;
   mesta:=TMyStrList.Create;
   vlaky:=TMyStrList.Create;
   reach:=TMyStrList.Create;
   DateSeparator:='.';
   LongTimeFormat:='hh:nn';
   ShortDateFormat:='dd.mm.yyyy';
   SetDefaults;
   LoadSettings;
{   ttz_file:='train_cz.ttz';
   tti_file:='train_cz.tti';
   tt2html_exe:='tt2html';}
   SelStation:='';
   tt_file:='';
   ListPos:=0;
   MaxNum:=-1;
   train_count:=0;
   train_pos:=1;
   RunID:=0;
   Changing:=false;
   act_page:=OneStation;
   LastFocus:=edStNameOS;
{   comptab_exe:='comptab';}
    UpdateDB;
{   mesta.LoadFromFile('mesta');}
{   vlaky.LoadFromFile('vlaky');}
   StationTabEnter;
   fl:=datetostr(date);
   meDate1OS.Text:=fl;
   meDate2OS.Text:=fl;
   meDate1TS.Text:=fl;
   meDate2TS.Text:=fl;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
   mesta.free;
   vlaky.free;
   SaveSettings;
   if tt_file<>'' then
      DeleteFIle(tt_file);
end;

procedure TMainForm.SplitterMoved(Sender: TObject);
begin
   btSave.width:=pnLeftBt.width;
   btSaveAll.width:=pnLeftBt.width;
   btClear.width:=pnLeftBt.width;
end;

procedure TMainForm.StConnEditKeyPress(Sender: TObject; var Key: Char);
var
   num: integer;
begin
   if Key=#13 then
   begin
      Key:=#0;
      btSearchClick(self);
      exit;
   end;
   if CurList=@vlaky then
      exit;
   if Key in Numbers then
   begin
      num:=ord(Key)-48;
      if num<=MaxNum then
         (Sender as TEdit).text:=CurList^[ListPos + ord(Key)-48];
      Key:=#0;
   end;
end;

procedure TMainForm.StConnEditChange(Sender: TObject);
var
   s : string;
   pos : integer;
   res : boolean;
begin
   MaxNum:=-1;
   res:=false;
   s:=(Sender as TEdit).Text;
{   if length(s)=0 then
   begin
      lbList.Items:=mesta;
      lbList.TopIndex:=0;
      MaxNum:=-1;
      exit;
   end;}
   lbList.Items.Clear;
   res:=CurList^.FindFirst(s, pos);   
   if not res then
      exit;
   FillList(s, pos, CurList);
end;

procedure TMainForm.StationTabEnter;
begin
   CurList:=@mesta;
end;

procedure TMainForm.ConnTabEnter;
begin
   CurList:=@vlaky;
end;

procedure TMainForm.OptionsTabEnter;
begin
   edTTZFileName.text:=ttz_file;
   edTTIFileName.text:=tti_file;
   meDefInt.text:=TimeToStr(inc_hours);
end;

procedure TMainForm.pcPagesPageChanging(Sender: TObject;
  NewPage: TTabSheet; var AllowChange: Boolean);
begin
   if not MainForm.visible then exit;
   if (NewPage=tbOneStation) or (NewPage=tbTwoStations) then
      StationTabEnter
   else
      if NewPage=tbByName then
         ConnTabEnter
      else
         if NewPage=tbOptions then
            OptionsTabEnter;
end;

procedure TMainForm.EditEnter(Sender: TObject);
begin
   LastFocus:=Sender;
   if Sender=edSt1NameTS then
      CurList:=@mesta;
   StConnEditChange(Sender);
end;

procedure TMainForm.lbListClick(Sender: TObject);
begin
   if (lbList.ItemIndex>MaxNum) or (lbList.ItemIndex=(-1)) then
      exit;
   (LastFocus as TEdit).text:=CurList^[ListPos + lbList.ItemIndex];
   (LastFocus as TEdit).SetFocus;
end;

procedure TMainForm.tbOneStationShow(Sender: TObject);
begin
   edStNameOs.SetFocus;
   act_page:=OneStation;
end;

procedure TMainForm.tbTwoStationsShow(Sender: TObject);
begin
   edSt1NameTs.SetFocus;
   act_page:=TwoStations;
end;

procedure TMainForm.tbByNameShow(Sender: TObject);
begin
   edConnName.SetFocus;
   act_page:=ByName
end;
                     {
procedure TMainForm.btSearchOSClick(Sender: TObject);
var
   fl, outf : string;
begin
   prg_to_file(comptab_exe+' -a ' + ttz_file + ' ' + tti_file + ' '
               + edStNameOs + ' ' + edDate1OS + ' ' + edTime1OS + ' '
               + edDate2Os + ' ' + edTime2OS, 'search.result', fl);

   DeleteFile(fl);
end;                  }

procedure TMainForm.btSearchClick(Sender: TObject);
var
   fl  : string;
   tmp : integer;
begin
   inc(RunID);
   if RunID=MaxInt then
      RunID:=0;
   case act_page of
      OneStation:
      begin
         if not CheckDateTime(edStNameOs, edStNameOs, meDate1OS, meDate2OS, meTime1OS, meTime2OS) then
            exit;
         StatusBar.SimpleText:='Searching...';
         StatusBar.Refresh;
         Refresh;
         if btAnyOS.down then
            prg_to_file(comptab_exe + ' -a "' + ttz_file + '" "' + tti_file + '" "' +
                        edStNameOs.text + '" any "' + TimeToStr(StrToTime(meTime1OS.text)) + '" any "' +
                        TimeToStr(StrToTime(meTime2Os.text)) + '"' , 'conn.search.OS.' + IntToStr(RunID), '.tt', fl)
         else
            prg_to_file(comptab_exe + ' -a "' + ttz_file + '" "' + tti_file + '" "' +
                        edStNameOs.text + '" "' + DateToStr(StrToDate(meDate1OS.text)) + '" "' +
                        TimeToStr(StrToTime(meTime1OS.text)) + '" "' + DateToStr(StrToDate(meDate2OS.text)) +
                        '" "' + TimeToStr(StrToTime(meTime2Os.text)) + '"' , 'conn.search.OS.' + IntToStr(RunID), '.tt', fl);
      end;
      TwoStations:
      begin
         if not CheckDateTime(edSt1NameTS, edSt2NameTS, meDate1TS, meDate2TS, meTime1TS, meTime2TS) then
            exit;
         StatusBar.SimpleText:='Searching...';
         StatusBar.Refresh;
         Refresh;
         if btAnyTS.down then
            prg_to_file(comptab_exe + ' -b "' + ttz_file + '" "' + tti_file + '" "' +
                        edSt1NameTS.text + '" any "' + TimeToStr(StrToTime(meTime1TS.text)) +
                        '" any "' + TimeToStr(StrToTime(meTime2TS.text)) +
                        '" "' + edSt2NameTs.text +'"', 'conn.search.TS.' + IntToStr(RunID), '.tt', fl)
         else
            prg_to_file(comptab_exe + ' -b "' + ttz_file + '" "' + tti_file + '" "' +
                        edSt1NameTS.text + '" "' + DateToStr(StrToDate(meDate1TS.text)) + '" "' +
                        TimeToStr(StrToTime(meTime1TS.text)) + '" "' + DateToStr(StrToDate(meDate2TS.text)) +
                        '" "' + TimeToStr(StrToTime(meTime2TS.text)) + '" "' + edSt2NameTs.text +
                        '"', 'conn.search.TS.' + IntToStr(RunID), '.tt', fl);
      end;
      ByName:
      begin
         if not vlaky.Find(edConnName.text, tmp) then
         begin
            ShowMessage(EWrongConnection);
            exit;
         end;
         StatusBar.SimpleText:='Searching...';
         StatusBar.Refresh;
         Refresh;
         prg_to_file(comptab_exe + ' -c ' + ttz_file + ' ' + edConnName.text,
                     'conn.search.BN.' + IntToStr(RunID), '.tt', fl);
      end;
   end; {case}

   if tt_file<>'' then
      DeleteFile(tt_file);
   tt_file:=fl;
   train_count:=get_train_count(tt_file);
   train_pos:=1;
   if train_count=0 then
   begin
      {$I-}
      DeleteFile(tt_file);
      {$I+}
      tt_file:='';
      TextBrowser.FileName:='';
      TextBrowser.Refresh;
  {    showmessage(ENothingFound);}
      Changing:=true;
      sbPosition.Position:=1;
      sbPosition.Max:=1;
      sbPosition.Refresh;
      Changing:=false;
      StatusBar.SimpleText:='Nothing found';
      Refresh;
      case act_page of
         OneStation:   edStNameOS.SetFocus;
         TwoStations:  edSt1NameTS.SetFocus;
         ByName:       edConnName.SetFocus;
      end; {case}
      exit;
   end;

   Changing:=true;
   sbPosition.Max:=train_count;
   sbPosition.Position:=train_pos;
   Changing:=false;
   StatusBar.SimpleText:='Item: ' + IntToStr(train_pos) + '/' + IntToStr(train_count);
   ShowItemNr(train_pos);
end;

procedure TMainForm.TextBrowserHighlightText(Sender: TObject;
  const HighlightedText: WideString);
begin
   SelStation:=HighlightedText;
end;

procedure TMainForm.TextBrowserMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   s, StName, Time : string;
   t : TDateTime;
begin
   if SelStation='' then exit;
   {dirty trick - I just don't want to let QTextBrowser respond on OnMouseDown event}
   Mouse.Capture:=Panel1;
   Time:=copy(SelStation, 1, pos(#9, SelStation)-1);
   s:=copy(SelStation, pos(#9, SelStation)+1, length(SelStation));
   if Time='' then
      Time:=copy(s, 1, pos(#9, s)-1);
   s:=copy(s, pos(#9, s)+1, length(s));
   s:=copy(s, pos(#9, s)+1, length(s));
   StName:=copy(s, pos(#9, s)+1, length(s));
   case act_page of
      OneStation:
      begin
        { if StrToTime(Time)<StrToTime(meTime1OS.Text) then
            t:=StrToTime(Time) + incDay(StrToDate(meDate1OS.Text), 1)
         else}
         t:=StrToTIme(Time) + StrToDate(meDate1OS.Text);
         meTime1OS.Text:=TimeToStr(t);
         meDate1OS.Text:=DateToStr(t);
         t:=t + inc_hours;
         meTime2OS.Text:=TimeToStr(t);
         meDate2OS.Text:=DateToStr(t);
         edStNameOs.Text:=StName;
         edStNameOS.SetFocus;
      end;
      TwoStations:
      begin
         {if StrToTime(Time)<StrToTime(meTime1TS.Text) then
            t:=StrToTime(Time) + incDay(StrToDate(meDate1TS.Text), 1)
         else}
         CurList:=@mesta;
         t:=StrToTime(Time) + StrToDate(meDate1TS.Text);
         meTime1TS.Text:=TimeToStr(t);
         meDate1TS.Text:=DateToStr(t);
         t:=t + inc_hours;
         meTime2TS.Text:=TimeToStr(t);
         meDate2TS.Text:=DateToStr(t);
         edSt1NameTS.Text:=StName;
         edSt2NameTS.SetFocus;
      end;
      ByName:       edConnName.SetFocus;
   end; {case}

end;

procedure TMainForm.tbOptionsShow(Sender: TObject);
begin
   act_page:=Options;
end;

procedure TMainForm.sbPositionChange(Sender: TObject);
begin
   if (tt_file='') or Changing then
      exit;
   ShowItemNr(sbPosition.Position);
   train_pos:=sbPosition.Position;
   StatusBar.SimpleText:='Item: ' + IntToStr(train_pos) + '/' + IntToStr(train_count);
end;

procedure TMainForm.btReachableClick(Sender: TObject);
var
   s, fl : string;
   tmp : integer;
begin
   if not mesta.Find(edSt1NameTS.text, tmp) then
   begin
      ShowMessage(EWrongStation);
      edSt1NameTS.SetFocus;
      exit;
   end;
   s:=StatusBar.SimpleText;
   StatusBar.SimpleText:='Searching...';
   StatusBar.Refresh;
   Refresh;
   prg_to_file(comptab_exe+' -s ' + ttz_file + ' ' + tti_file + ' "' + edSt1NameTS.text +
               '" | sort -u', 'reachable', '.lst', fl);
   reach.LoadFromFile(fl);
   CurList:=@reach;
   DeleteFile(fl);
   edSt2NameTS.SetFocus;
   StatusBar.SimpleText:=s;
   StatusBar.Refresh;
end;

procedure TMainForm.btAnyClick(Sender: TObject);
begin
   case act_page of
      OneStation:
      begin
         meDate1OS.enabled:=(Sender as TButton).down;
         meDate2OS.enabled:=(Sender as TButton).down;
         edStNameOS.SetFocus;
      end;
      TwoStations:
      begin
         meDate1TS.enabled:=(Sender as TButton).down;
         meDate2TS.enabled:=(Sender as TButton).down;
         (LastFocus as TEdit).SetFocus;
      end;
   end; {case}

end;

procedure TMainForm.btClClick(Sender: TObject);
begin
   case act_page of
      OneStation:
      begin
         meDate1OS.text:=DateToStr(date);
         meDate2OS.text:=DateToStr(date);
         meTime1OS.text:='0:00';
         meTime2OS.text:='23:59';
         edStNameOS.SetFocus;
      end;
      TwoStations:
      begin
         meDate1TS.text:=DateToStr(date);
         meDate2TS.text:=DateToStr(date);
         meTime1TS.text:='0:00';
         meTime2TS.text:='23:59';
         (LastFocus as TEdit).SetFocus;
      end;
   end; {case}
end;


procedure TMainForm.btSaveClick(Sender: TObject);
var
   fl : string;
begin
   if tt_file = '' then
   begin
      ShowMessage(ENothingToSave);
      exit;
   end;
   if not sdSave.Execute then
      exit;
   if Sender=btSave then
      extract_from_tt(tt_file, sbPosition.Position, fl)
   else
      fl:=tt_file;
   case sdSave.FilterIndex of
      1:
      begin
         if not exec_command('cp ' + fl + ' ' + sdSave.FileName) then
         begin
            ShowMessage(EWriteError);
            exit;
         end;
         if fl<>tt_file then
            DeleteFile(fl);
      end;
      2:
      begin
         if not exec_command('cat ' + fl + ' | ' + tt2html_exe + ' >' + sdSave.FileName) then
         begin
            ShowMessage(EWriteError);
            exit;
         end;
         if fl<>tt_file then
            DeleteFile(fl);
      end;
   end; {case}
end;

procedure TMainForm.btClearClick(Sender: TObject);
begin
   tt_file:='';
   TextBrowser.FileName:='';
   TextBrowser.Refresh;
   Changing:=true;
   sbPosition.Position:=1;
   sbPosition.Max:=1;
   Changing:=false;
   StatusBar.SimpleText:='Ready';
   case act_page of
      OneStation:   edStNameOS.SetFocus;
      TwoStations:  edSt1NameTS.SetFocus;
      ByName:       edConnName.SetFocus;
   end; {case}
end;

procedure TMainForm.meDefIntExit(Sender: TObject);
begin
   try
      inc_hours:=StrToTime(meDefInt.Text);
   except
      ShowMessage(EWrongTime);
      meDefInt.SetFocus;
      exit;
   end;
end;

procedure TMainForm.btApplyClick(Sender: TObject);
var
   fl1, fl2 : string;
   fil : system.text;
begin
   fl1:=edTTZFileName.Text;
   fl2:=edTTIFileName.Text;
   try
      system.assign(fil, fl1);
      system.reset(fil);
   except
      ShowMessage(EWrongFile);
      edTTZFileName.SetFocus;
      exit;
   end;
   system.close(fil);
   try
      system.assign(fil, fl2);
      system.reset(fil);
   except
      ShowMessage(EWrongFile);
      edTTIFileName.SetFocus;
   end;
   system.close(fil);
   ttz_file:=edTTZFileName.Text;
   tti_file:=edTTIFileName.Text;
   fl1:=StatusBar.SimpleText;
   StatusBar.SimpleText:='UpdatingDB...';
   StatusBar.Refresh;
   Refresh;
   UpdateDB;
   StatusBar.SimpleText:=fl1;
end;

procedure TMainForm.btOpenClick(Sender: TObject);
begin
   if Sender=btOpenTTZ then
      odOpen.Filter:=TTZFilter
   else
      odOpen.Filter:=TTIFilter;
   if not odOpen.Execute then
      exit;
   if Sender=btOpenTTZ then
   begin
      edTTZFileName.Text:=odOpen.FileName;
      edTTZFileName.SetFocus;
   end
   else
   begin
      edTTIFileName.Text:=odOpen.FileName;
      edTTIFileName.SetFocus;
   end;
end;

end.
