unit Exec;

interface

function get_prg_out(s : string) : string;
procedure prg_to_file(s, prefix, ext : string; var fl : string);
procedure extract_from_tt(ttf : string; nr : integer; var fl : string);
function get_train_count(fil : string) : integer;
function exec_command(cmd : string) : boolean;
function get_home_dir : string;

implementation
uses sysutils, libc;

function get_home_dir : string;
var
   s : string;
begin
   s:=getenv('HOME');
   if s[length(s)]<>'/' then s:=s+'/';
   get_home_dir:=s;
end;

function get_uniq_filename(prefix, ext : string) : string;
var
   pid : integer;
begin
   pid:=getpid;
   get_uniq_filename:='/tmp/'+ prefix + inttostr(pid) + getenv('HOSTNAME') + ext;
end;

function get_prg_out(s : string) : string;
var
   cmd, outf : string;
   fil : text;
begin
   get_prg_out:='';
   outf:=get_uniq_filename('comptab', '.tmp');
   cmd:=s + ' >' + outf + ' 2>/dev/null';
   libc.system(PChar(cmd));
   {$I-}
   assign(fil, outf);
   reset(fil);
   {$I+}
   cmd:='';
   if ioresult<>0 then
      exit;
   while not eof(fil) do
   begin
      readln(fil, s);
      cmd:=cmd + s + #10;
   end;
   close(fil);
   DeleteFile(outf);
   get_prg_out:=cmd;
end;

procedure prg_to_file(s, prefix, ext : string; var fl : string);
var
   cmd : string;
begin
   fl:=get_uniq_filename(prefix, ext);
   cmd:=s + ' >' + fl {+ ' 2>/dev/null'};
   libc.system(PChar(cmd));
end;

function exec_command(cmd : string) : boolean;
var
   s, errfl : string;
   fl : text;
begin
   exec_command:=true;
   errfl:=get_uniq_filename('stderr','.txt');
   cmd:=cmd + ' 2>' + errfl;
   libc.system(PChar(cmd));
   {$I-}
   assign(fl, errfl);
   reset(fl);
   {$I+}
   if ioresult<>0 then
      exit;
   readln(fl, s);
   close(fl);
   if s<>'' then
      exec_command:=false;
   DeleteFile(errfl);
end;

procedure extract_from_tt(ttf : string; nr : integer; var fl : string);
var
   inf, outf : text;
   i : integer;
   s : string;
begin
   fl:='';
   {$I-}
   assign(inf, ttf);
   reset(inf);
   {$I+}
   if ioresult<>0 then
      exit;
   fl:=get_uniq_filename('extracted', '.tt');
   {$I-}
   assign(outf, fl);
   rewrite(outf);
   {$I+}
   if ioresult<>0 then
   begin
      fl:='';
      exit;
   end;
   i:=0;
   while i<nr do
   begin
      readln(inf, s);
      if s[1]='#' then inc(i);
   end;
   writeln(outf, s);
   readln(inf, s);
   while (s[1]<>'#') and not eof(inf) do
   begin
      writeln(outf, s);
      readln(inf, s);
   end;
   if s[1]<>'#' then
      writeln(outf, s);
   close(inf);
   close(outf);
end;

function get_train_count(fil : string) : integer;
var
   inf : text;
   s : string;
   i : integer;
begin
   i:=0;
   get_train_count:=i;
   {$I-}
   assign(inf, fil);
   reset(inf);
   {$I+}
   if ioresult<>0 then
      exit;
   while not eof(inf) do
   begin
      readln(inf, s);
      if s[1]='#' then inc(i);
   end;
   get_train_count:=i;
   close(inf);
end;

end.

