unit Global;

interface
uses SysUtils, Classes, libc;

const
   NonAlCh=['-', #32];
   Numbers=['0'..'9'];
   EWrongStation='Given station doesn''t exist';   {DODELAT:  Mozna casem normalni vyjimky}
   EWrongConnection='Given connection desn''t exist';
   EWrongDate='Wrong date specified';
   EWrongTime='Wrong time specified';
   ENothingFound='Nothing found!';
   ENothingToSave='Nothnig to save!';
   EWriteError='Error writing file';
   EOpenSettings='Error reading/creating/writing settings file';
   EWrongSetting='Wrong setting name in settings file: ';
   EWrongFile='Given file doesn''t exist';
   MBConfirmation='Settings file doesn''t exist. Create?';
   WEOF='End of file';
   TTZFilter='packed ttdos database file (*.ttz)';
   TTIFilter='index of ttdos database file (*.tti)';
   SettingsFile='.CompTabFE';
   NSettings=5;
   SettingName : array[1..NSettings] of string =
      ('comptab executable', 'tt2html executable', 'ttz file', 'tti file', 'default interval');
type
   TActPage = ( OneStation, TwoStations, ByName, Extra, Options );
   TSettings = array[1..NSettings] of string;
   PMyStrList = ^TMyStrList;
   TMyStrList = class(TStringList)
   public
      function FindFirst(const S: string; var Index: Integer): Boolean; {override;}
end;


function CompStr(const R, S: string): Boolean;
function min(a, b : integer): integer;

var
   mesta, vlaky, reach : TMyStrList;
   ttz_file, tti_file, tt_file : string;
   comptab_exe, tt2html_exe : string;
   train_count, train_pos : integer;
   Settings : TSettings;

implementation

function min(a, b : integer): integer;
begin
   if a<b then
      min:=a
   else
      min:=b;
end;

function CompStr(const R, S: string): Boolean;
var
   I, L : Integer;
begin
   Result:=False;
   L:=length(R);
   if L>length(S) then
      exit;
   Result:=True;
   if R='' then
      exit;
   I:=1;
   while I<=L do
      if (UpCase(R[I])=UpCase(S[I])) or
         (R[I] in NonAlCh) and (S[I] in NonAlCh) then
         inc(i)
      else
      begin
         Result:=False;
         exit;
      end;
end;

function TMyStrList.FindFirst(const S: string; var Index: Integer): Boolean;
var
   I : Integer;

begin
   Result:=False;
   for I:=0 to GetCount - 1 do
      if CompStr(S, Get(I)) then
      begin
         Result:=True;
         break;
      end;
   Index:=I;
end;



end.
