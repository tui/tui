#!/usr/bin/python
# Copyright 2014,2019 Pavel Machek <pavel@ucw.cz>
# Distribute under GPLv2+.

# Format description
# https://developers.google.com/transit/gtfs/reference
# Github gtfs sources:
# https://github.com/bmander/gtfs/blob/master/gtfs/entity/map_entities.py
# pip install gtfs
#
# Prague PID:
# http://www.infoprovsechny.cz/request/aktuln_verze_gtfs
#
# This should be newer/faster to use:
# https://code.google.com/p/gtfsdb/source/browse/README

# FIXME: does not take service period exceptions into account.

from gtfs.loader import load
from gtfs.schedule import Schedule
#from gtfs.types import Boolean
from gtfs.entity import StopTime

"""
from gtfs.entity import *
s = Schedule("pid.db")
q = s.session.query(StopTime)

# .first()?
# .offset()?
# .slice(od, do)

>>> q.first()
<StopTime 1 <Time 06:37:40>>
>>> q.offset(1).first()
<StopTime 1 <Time 06:40:25>>
>>> q.offset(2).first()
<StopTime 1 <Time 06:42:35>>
>>> q.offset(0).first()
<StopTime 1 <Time 06:37:40>>
>>> q.slice(0,100).all()
[<StopTime 1 <Time 06:37:40>>, <StopTime 1 <Time 06:40:25>>, <StopTime 1 <Time 06:42:35>>, <StopTime 1 <Time 06:44:20>>, <StopTime 1 <Time 06:45:55>>, <StopTime 1 <Time 06:47:30>>, <StopTime 1 <Time 06:49:15>>, <StopTime 1 <Time 06:50:50>>, <StopTime 1 <Time 06:52:20>>,
"""


import getopt, sys

profile = 0

class Convert:
  def __init__(m):
    m.fname = "sample-feed.zip"
    m.outname = None
    m.limit = 1000000000

  def set_up(m):
    if m.fname and m.outname:
      m.schedule = load(m.fname, m.outname)
      return
    if m.fname:
      m.schedule = load(m.fname)
      return
    m.schedule = Schedule(m.outname)

  def handle_opts(m, opts):
    for opt, arg in opts:
      if opt == '-h':
        m.help()
      elif opt in ("-p", "--pid"):
        m.fname = "PID_GTFS.zip"
        m.outname = "pid.db"
      elif opt in ("-F", "--first"):
        m.fname = "/data/gis/osm/pid_ascii.zip"
        m.outname = "pid.db"
      elif opt in ("-f", "--full"):
        m.fname = None
        m.outname = "pid.db"
      elif opt in ("-P", "--profile"):
        global profile
        print "Will profile"
        profile = 1
      elif opt in ("-l", "--limit"):
        m.limit = 100

  def print_time(m, t):
    if t is not None:
      v = t.val / 60
      return "%d:%02d" % (v / 60, v % 60)
    return ""

  def encode_period(m, p):
    i = 0
    if int(p.monday) == 1: i |= 1
    if int(p.tuesday) == 1: i |= 2
    if int(p.wednesday) == 1: i |= 4
    if int(p.thursday) == 1: i |= 8
    if int(p.friday) == 1: i |= 0x10
    if int(p.saturday) == 1: i |= 0x20
    if int(p.sunday) == 1: i |= 0x40
    return i

  def print_header(m, trip):
    print "# %s" % (trip.route.route_short_name.encode('utf-8'))
    
  def print_footer(m, trip):
    print "D2014-05-19 3650 1000:%x" % m.encode_period(trip.service_period)

  def print_stop(m, stop):
    dist = stop.shape_dist_traveled
    if dist is float:
      dist = "%.3f" % dist
    else:
      dist = "0"
    print "%s	%s	.	%s	%s" % \
          (m.print_time(stop.arrival_time), m.print_time(stop.departure_time), \
           dist, stop.stop.stop_name.encode('utf-8'))

  def run_slow(m):
    for route in m.schedule.routes:
      for trip in route.trips:
        m.print_header(trip)
        # This takes ~1 second :-(
        times = trip.stop_times
        for stop in times:
          m.print_stop(stop)
        m.print_footer(trip)
        if not m.limit:
          print "!Limit reached"
          return
        m.limit -= 1

  def run(m):
    q = m.schedule.session.query(StopTime)
    trip = None
    block = 10000
    for i in range(q.count()/block):
     for stop in q.slice(i*block, (i+1)*block):
      if trip != stop.trip:
        if trip:
          m.print_footer(trip)
        trip = stop.trip
        if not m.limit:
          print "!Limit reached"
          return
        m.limit -= 1
        m.print_header(trip)
      m.print_stop(stop)

    m.print_footer(trip)


cvt = Convert()
try:
    opts, args = getopt.getopt(sys.argv[1:], "fFpPl", [ "full", "first", "profile", "limit", "pid" ])
except getopt.GetoptError:
    print "Getopt problem"
    cvt.help()

print "args", args
cvt.handle_opts(opts)

if not profile:
  cvt.set_up()
  cvt.run()
else:

  import hotshot, hotshot.stats
  prof = hotshot.Profile("stones.prof")
  prof.runcall(cvt.set_up)
  prof.runcall(cvt.run_slow)
  prof.close()

  stats = hotshot.stats.load("stones.prof")
  stats.strip_dirs()
  stats.sort_stats('time', 'calls')
  stats.print_stats(20)
