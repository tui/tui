{Copyright 2002 Tomas Kuchar <tomas.kuchar@centrum.cz>,  Distribute under GPL v2} {Verze 1.1}


const
   max1=50;  {odhad maximalniho poctu nasobku opakovani v approx (napr 6x12 1x8 => 2)}
   max2=80;  {odhad maximalniho poctu zastavek v jedne lince}


type
   Tdoceho=(a,b,h);


var
   hod, min, phod, pmin, {promenne pro cas}
   km, pkm, pred,        {promenne pro kilometry}
   x: integer;
   s: char;
   l, text: string;
   prevadi: boolean;
   doceho: Tdoceho;
   walk : boolean;



procedure na_co;      {zjisti z parametru, do ktereho formatu ma prevadet}
var i:integer;
begin
   doceho:=h;
   for i:=1 to ParamCount do
   begin
      l:=ParamStr(i);
      if l[1]='-' then
	 if l[2]='a' then doceho:=a
	 else if l[2]='b' then doceho:=b
   end;
end;


procedure help;       {vypise help}
begin
   writeln('TT2TT Verze 1.1 Copyright (c) 2002 Tomas Kuchar');
   writeln('Distribute under GPL v2');
   writeln;
   writeln('Program prevadi formaty jizdnich radu.');
   writeln('Program pracuje se standardnim vstupem a standardnim vystupem.');
   writeln('Parametry:');
   writeln('       -a      prevede do formatu s absolutnimi casy/kilometry');
   writeln('       -b      prevede do formatu s relativnimi casy/kilomtery');
   writeln('       -h      vypise help');
end;


procedure horn1;      {hornerovo schema 1}
begin
   read(s);
   x:=0;
   while (s in ['0'..'9']) and not eof do
   begin
      x:=10*x + ord(s)-ord('0');
      read(s);
   end;
end;


procedure horn2;      {hornerovo schema 2}
begin
   x:=0;
   while (s in ['0'..'9']) and not eof do
   begin
      x:=10*x + ord(s)-ord('0');
      read(s);
   end;
end;


procedure start;      {nacte cas ve kterem spoj vyjizdi}
begin
   horn1;
   hod:=x;
   horn1;
   min:=x;
   write(hod); write(':');
   if min<10 then write('0',min) else write(min);
   write(s);
end;


procedure prepis;
begin
   while (not eof and not (s in ['@','#'])) do
   begin
      write(s); read(s);
   end;
end;


procedure prepis1;
begin
   while (not eof and not (ord(s)=10)) do
   begin
      read(s); write(s);
   end;
end;


procedure prepis2;
begin
   while (not eof and not (s='	')) do
   begin
      write(s); read(s);
   end;
end;


procedure prepis3;
begin
   text:='';
   while (not eof and not (ord(s)=10) and not (s in ['@','#'])) do
   begin
      read(s); text:= text + s;
   end;
end;


procedure zpozdeni_approx(k:integer);
var narust: integer;
begin
   narust:=0;
   narust:=(pmin + k);
   if walk then phod:=(phod + (narust div 60))
   else phod:=((phod + (narust div 60)) mod 24);
   pmin:=(narust mod 60);
   write(phod); write(':');
   if pmin<10 then write('0',pmin) else write(pmin);
end;


procedure kilometry_approx;
begin
   if s='+' then
   begin
      horn1;
      km:= x;
   end
   else if (s in ['0'..'9']) then
   begin
      horn2;
      pkm:= x;
      km:= pkm - pred;
      pred:= pkm;
   end;
end;


procedure cas_approx;
begin
   if s='	' then
   begin
      read(s);
      x:=0;
   end
   else if s='+' then
   begin
      horn1;
      read(s);
   end
end;


procedure approx;     {prevede z formy s '@' (approx) do formy a nebo b}
var
   hlavicka: string;
   i, j, k, m, n, pso, sumkm, radek, atrib: integer;
   p_op: array [1..max1,1..2] of integer;           {opakovani spoje (napr 1x12)}
   p_cas: array [1..max2,1..2] of integer;          {cas prijezdu, cas odjezdu}
   p_km: array [1..max2] of integer;                {kilometry}
   p_pozn,p_zast,p_atrb: array [1..max2] of string; {poznamky; zastavky; atributy} begin
      {***************************** nulovani hodnot *****************************} hlavicka:=''; for i:=1 to max1 do
      begin
	 p_op[i,1]:=0;
	 p_op[i,2]:=0;
      end;
      for i:=1 to max2 do
      begin
	 p_cas[i,1]:=0;
	 p_cas[i,2]:=0;
	 p_km[i]:=0;
	 p_pozn[i]:='';
	 p_zast[i]:='';
	 p_atrb[i]:='';
      end;
      pso:=0;                            {pocet 'soucinu' opakovani}
      pred:=0; pkm:=0; km:=0; sumkm:=0;  {kilometry}
      radek:=0;                          {radek}
      atrib:=0;                          {radek, na kterem zacinaji atributy}
      {***************************** nacitani hodnot *****************************} read(s); read(s);
      while not eof and not (s in [' ']) do             {hlavicka}
      begin
	 hlavicka:=hlavicka+s; read(s)
      end;
      horn1; hod:=x;                                    {startovni cas}
      walk := (hod = 25);
      horn1; min:=x;
      while not eof and (s=' ') and not (pso=max1-1) do {opakovani}
      begin
	 inc(pso);
	 horn1; p_op[pso,1]:=x;
	 horn1; p_op[pso,2]:=x;
      end;
      while (not eof) and (not (s in ['#','@'])) do
      begin
	 read(s);
	 inc(radek);
	 if (s in ['	','+']) then
	 begin
	    inc(atrib);
	    cas_approx; p_cas[radek,1]:=x;            {casy}
	    cas_approx; p_cas[radek,2]:=x;
	    text:='';
	    while (not (s='	')) do                    {poznamky}
	    begin
	       text:= text + s; read(s);
	    end;
	    read(s);
	    p_pozn[radek]:=text;
	    kilometry_approx;                         {kilometry}
	    if doceho=b then p_km[radek]:=km
	    else
	    begin
	       sumkm:=sumkm + km;
	       p_km[radek]:=sumkm;
	    end;
	    prepis3;                                  {zastavky}
	    p_zast[radek]:=text;
	 end
	 else                                          {atributy}
	 begin
	    prepis3;
	    p_atrb[radek]:=text;
	 end;
      end;
      {****************************** prvni zaznam *******************************}
      writeln('# '+hlavicka);                           {vypise prvni radek prvniho zaznamu}
      write('	'); write(hod);write(':');
      if min<10 then write('0',min) else write(min);
      write('	'+p_pozn[1]+'	');
      if doceho=b then write('+');
      write(p_km[1]);
      write('	'+p_zast[1]);
      phod:=hod; pmin:=min;
      for i:=2 to atrib do                              {vypise dalsi radky prvniho zaznamu}
      begin
	 if p_cas[i,1]<>0 then
	 begin
	    if doceho=a then zpozdeni_approx(p_cas[i,1])
	    else
	    begin
	       write('+'); write(p_cas[i,1]);
	    end;
	 end;
	 write('	');
	 if i<>atrib then
	 begin
	    if doceho=a then zpozdeni_approx(p_cas[i,2])
	    else
	    begin
	       write('+'); write(p_cas[i,2]);
	    end;
	 end;
	 write('	'+p_pozn[i]+'	');
	 if doceho=b then write('+');
	 write(p_km[i]);
	 write('	'+p_zast[i]);
      end;
      for i:=(atrib+1) to radek do
      begin
	 write(p_atrb[i]);
      end;
      {*************************** vypise dalsi zaznamy ***************************}
      for i:=1 to pso do
      begin
	 m:=p_op[i,1];
	 n:=p_op[i,2];
	 for j:=1 to m do
	 begin
	    writeln('# '+hlavicka);
	    if walk then hod:=hod+((min+n) div 60)
	    else hod:=(hod+((min+n) div 60) mod 24);
	    min:=(min+n) mod 60;
	    write('	'); write(hod); write(':');
	    if min<10 then write('0',min) else write(min);
	    if doceho=b then
	       write('	'+p_pozn[1]+'	+')
	    else
	    begin
	       phod:=hod; pmin:=min;
	       write('	'+p_pozn[1]+'	');
	    end;
	    write(p_km[1]);
	    write('	'+p_zast[1]);
	    for k:=2 to atrib do
	    begin
	       if (p_cas[k,1]<>0) or (k=atrib) then
	       begin
		  if doceho=b then
		  begin
		     write('+');
		     write(p_cas[k,1])
		  end
		  else zpozdeni_approx(p_cas[k,1])
	       end;
	       write('	');
	       if k<>atrib then
	       begin
		  if doceho=b then
		  begin
		     write('+');
		     write(p_cas[k,2])
		  end
		  else zpozdeni_approx(p_cas[k,2])
	       end;
	       if doceho=b then write('	'+p_pozn[k]+'  	+')
	       else write('	'+p_pozn[k]+'  	');
	       write(p_km[k]);
	       write('	'+p_zast[k]);
	    end;
	    for k:=(atrib+1) to radek do
	    begin
	       write(p_atrb[k]);
	    end;
	 end;
      end;
   end;


procedure zpozdeni_a; {nacte zpozdeni (napr +2) a vytiskne upraveny cas (napr 4:25 a 4:27)}
var narust:integer;
begin
   horn1;
   narust:=0;
   narust:=(min + x);
   if walk then hod:=(hod + (narust div 60))
   else hod:=((hod + (narust div 60)) mod 24);
   min:=(narust mod 60);
   write(hod);
   write(':');
   if min<10 then write('0',min)
   else write(min);
   write(s);
   read(s);
end;


procedure kilometry_a;
begin
   if s='+' then
   begin
      horn1;
      km:= km + x;
      write(km);
      write(s);
      prevadi:=true;
   end
   else write(s);
end;


procedure cas_a;
begin
   if s='	' then
   begin
      write(s); read(s)
   end
   else if s='+' then
   begin
      zpozdeni_a;
      prevadi:=true;
   end
   else
   begin
      prepis2;
      write(s); read(s);
   end;
end;


procedure preved_a;   {prevede do formatu a}
begin
   read(s);
   prepis;
   while not eof do
   begin
      if s='@' then approx
      else
      begin
	 prevadi:=false; km:=0;
	 write(s);
	 prepis1;
	 read(s); write(s);
	 start;
	 walk := (hod = 25);
	 read(s); write(s); read(s);
	 kilometry_a;
	 prepis1;
	 while (not eof) and (not (s in ['#','@'])) do
	 begin
	    read(s);
	    if not (s in ['0'..'9','	','+']) then
	    begin
	       prepis;
	       continue;
	    end;
	    cas_a; cas_a;
	    prepis2; write(s); read(s);
	    kilometry_a;
	    prepis1;
	    if not prevadi then
	    begin
	       read(s);
	       prepis;
	       if eof then write(s);
	    end;
	 end;
      end;
   end;
end;


procedure zpozdeni_b; {nacte casi (napr 4:25 a 4:27) a vytiskne zpozdeni (napr +2)}
var rozdil:integer;
begin
   horn2;
   phod:=x;
   horn1;
   pmin:=x;
   rozdil:=0;
   rozdil:=(pmin + (60 * phod))-(min + (60 * hod));
   if rozdil < 0 then rozdil:=(pmin + (60 * (phod+24)))-(min + (60 * hod));
   write('+');
   write(rozdil);
   hod:=phod;
   min:=pmin;
   write(s);
end;


procedure kilometry_b;
var rozdil:integer;
begin
   if (s in ['0'..'9']) then
   begin
      horn2;
      pkm:= x;
      rozdil:= pkm - km;
      km:= pkm;
      write('+'); write(rozdil);
      write(s);
      prevadi:= true;
   end
   else write(s);
end;


procedure cas_b;
begin
   if s='	' then
   begin
      write(s); read(s)
   end
   else if (s in ['0'..'9']) then
   begin
      zpozdeni_b;
      prevadi:= true;
      read(s);
   end
   else
   begin
      prepis2;
      write(s); read(s);
   end;
end;


procedure preved_b;   {prevede do formatu b}
begin
   read(s);
   while (not eof) and (not (s in ['#','@'])) do
   begin
      write(s);
      read(s);
   end;
   while not eof do
   begin
      if s='@' then approx
      else
      begin
	 prevadi:=false; km:=0; pkm:=0;
	 write(s);
	 prepis1;
	 read(s); write(s);
	 start;
	 read(s); write(s); read(s);
	 kilometry_b;
	 prepis1;
	 while (not eof) and (not (s in ['#','@'])) do
	 begin
	    read(s);
	    if not (s in ['0'..'9','	','+']) then
	    begin
	       prepis;
	       if eof then write(s);
	       continue;
	    end;
	    cas_b; cas_b;
	    prepis2; write(s); read(s);
	    kilometry_b;
	    prepis1;
	    if not prevadi then
	    begin
	       read(s);
	       prepis;
	       if eof then write(s);
	    end;
	 end;
      end;
   end;
end;


begin
   hod:=0; min:=0;
   na_co;
   if doceho=a then preved_a
   else if doceho=b then preved_b
   else help
end.