#!/usr/bin/python3

import re

def load_stops(name):
    return map(lambda x: x[:-1], open(name).readlines())

def canonical(s):
    s = re.sub("[,. ]", "_", s)
    s = re.sub("_+", "_", s)
    return s

name = "praha"

osm = {}
for n in load_stops(name + ".osm"):
    osm[canonical(n)] = n

cis = {}
for n in load_stops(name + ".cis"):
    cis[canonical(n)] = n


good = 0
missing_in_osm = 0
bad_in_osm = 0
for n in cis:
    if n in osm:
        good += 1
        #print("good: ", cis[n], "/", osm[n])
        continue
    missing_in_osm += 1
    print(cis[n], " -- missing in osm")

for n in osm:
    if n in cis:
        continue
    bad_in_osm += 1
    print(osm[n], " -- typo in osm?")

print()
print("@", len(osm), "names known in osm.")
print("@", len(cis), "names known in cis.")
print("@", good, "names common in osm and cis.")
print("@", missing_in_osm, "names missing from osm.")
print("@", bad_in_osm, "names not in cis, probably typo in osm.")

    
    
