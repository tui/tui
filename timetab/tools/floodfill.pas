{ Copyright 2001 Marian Ertl <marian.ertl@post.sk>, distribute under GNU GPL }

{
 This utility works with the .tt data format mentioned in ../timetab.html.
 
 After entering the 'starting town' and the list of the 'banned towns' -
 this towns cannot be passed - this utility will write the names of all
 towns that are reachable from the starting towns without passing any of
 'banned towns'.
 This is the equivalent to the Floodfill procedure in computer graphics -
 just think of the 'banned towns' list as the boundary line and the starting
 town as the seed point and this will 'fill all the area in which the seed
 point lies' = write all town names reachable ftom the starting town.
 Therefore it can be used to mark all the towns in the specific area, e.g.
 The Czech Republic (see ../cz/cz_boundary.list).

Usage: floodfill cz_train_boundary.list 'Praha hl.n.' < train.tt > list_of_noncz

 The program requires the train database in the .tt format. Database is given
 on stdin.
 Furthermore, it requires the list of the 'banned towns' (the format: text
 file with each town in separate line). The file name can be listed as the
 second parameter.
 At last it requires the starting town name - can be listed as the third
 parameter.
}

Program (***)Floodfill(***);

const Rozsah = 10000;

type
  Nazov = string[40];

  UkMesto = ^Mesto;
  UkZakaz = ^Zakaz;

  Mesto = record
    meno   : nazov;
    pred   : UkMesto;
    dalsi  : UkMesto;
  end;

  Zakaz = record
    meno  : nazov;
    dalsi : UkZakaz;
  end;

  Hashpole = array[0..rozsah-1] of UkMesto;


var
  p                  : pointer;
  strtkoren,strtuk   : UkMesto;
  h                  : Hashpole;
  f,zf               : text;
  x                  : boolean;
  strtmeno           : Nazov;
  zaczakaz           : UkZakaz;
  i,strtind,pocpar   : integer;
  datasub,zakazsub   : string;


Function (***)Najdi(***) (kluc:Nazov; var ukaz:UkMesto; var vysind:integer)
                         :boolean;
var koniec  : boolean;
    index   : integer;
    akt     : UkMesto;
    i       : integer;
    pomkluc : Nazov;

  function Konvert(s:nazov):integer;
  var kon:longint;
  begin
    kon:=(17*ord(s[1]) mod rozsah
        +108*ord(s[3]) mod rozsah
        + 83*ord(s[4]) mod rozsah) mod rozsah;
    konvert:=kon;
  end;

begin
  pomkluc:=kluc;
  index:=konvert(kluc+'    ');

  if (index<0)or(index>rozsah-1)
  then begin
    writeln('Najdi:index out of range');
    halt(1);
  end;
  kluc:=pomkluc;

  vysind:=index;
  ukaz:=nil;
  akt:=h[index];
  koniec:=false;
  Najdi:=false;

  while not koniec
  do begin
    if akt=nil
    then begin
      koniec:=true;
      Najdi:=false;
      ukaz:=nil;
    end
    else if akt^.meno=kluc
    then begin
      koniec:=true;
      Najdi:=true;
      ukaz:=akt;
    end
    else
      akt:=akt^.dalsi;
  end;
end;(*****Najdi*****)


Function (***)Nacitaj(***) (var vstup:Nazov) :boolean;
var riadok : string;
    zn     : char;

begin
  Nacitaj:=false;
  repeat
    riadok:='';
    readln(f,riadok);
    if length(riadok)>0
      then zn:=riadok[1] else zn:=#0;
    if zn='#'
      then Nacitaj:=true;
    if not (zn in ['+',#9,'0'..'9'])
      then riadok:='';
  until (riadok<>'') or eof(f);

  while pos(#9,riadok)<>0
    do delete(riadok,1,pos(#9,riadok));
  if length(riadok)<=40
    then vstup:=riadok
  else
    vstup:=copy(riadok,1,40);
  if eof(f)
    then vstup:='ertlm0am_test';
end;(*****Nacitaj*****)


Function (***)Zakazane(***) (mesto:Nazov) :boolean;
var akt    : UkZakaz;
    koniec : boolean;

begin
  Zakazane:=false;
  akt:=zaczakaz;
  koniec:=false;
  while not koniec
  do begin
    if akt=nil
    then begin
      koniec:=true;
      Zakazane:=false;
    end
    else if akt^.meno=mesto
    then begin
      koniec:=true;
      Zakazane:=true;
    end
    else
      akt:=akt^.dalsi;
  end;
end;(*****Zakazane*****)


Function (***)Find(***) (ukaz:UkMesto) :UkMesto;
var akt : UkMesto;

begin
  Find:=ukaz;
  akt:=ukaz;
  if akt=nil
  then begin
    writeln('Find:null pointer assignment');
    halt(1);
  end;
  while akt^.pred<>nil
    do akt:=akt^.pred;
  Find:=akt;
end;(*****Find*****)


Procedure (***)Union(***) (ukaz,koren:UkMesto);
var akt,pom : UkMesto;

begin
  akt:=ukaz;
  if (akt=nil) or (koren=nil)
  then begin
    writeln('Union:null pointer assignment');
    halt(1);
  end;
  repeat
    pom:=akt^.pred;
    akt^.pred:=koren;
    akt:=pom;
  until akt=nil;
end;(*****Union*****)


Procedure (***)Zarad(***);
var novyspoj                   : boolean;
    aktmesto                   : Nazov;
    aktuk,pomuk,koren,aktkoren : UkMesto;
    aktindex                   : integer;

begin
  novyspoj:=true;
  while not eof(f)
  do begin
    novyspoj:=Nacitaj(aktmesto) or novyspoj;   {nacitaj mesto}
    if not eof(f)
    then begin
      if aktmesto='ertlm0am_test'
        then writeln('Zarad:Invalid end of file');

      if novyspoj
      then begin                               {I. Ak je zaciatkom noveho spoja}
        if not Zakazane(aktmesto)
        then begin                             {a nie je zakazane}
          if Najdi(aktmesto,aktuk,aktindex)
          then begin                           {1. a je uz v zozname}
            aktkoren:=Find(aktuk)              {tak jeho koren je korenom noveho spoja}
          end
          else begin
            new(pomuk);                        {2. ink ho zarad do zoznamu}
            pomuk^.meno:=aktmesto;
            pomuk^.pred:=nil;                  {je korenom novej komponenty}
            pomuk^.dalsi:=h[aktindex];
            h[aktindex]:=pomuk;
            aktkoren:=h[aktindex]              {a sucasne korenom noveho spoja}
          end;
          novyspoj:=false;                     {1. aj 2. - uz sme vnutri spoja}
        end;
      end

      else begin                               {II. Ak sme vnutri spoja}
        if not Zakazane(aktmesto)
        then begin                             {A. a nie je to zakazane mesto}
          if Najdi(aktmesto,aktuk,aktindex)
          then begin                           {1. ak je v zozname}
            koren:=Find(aktuk);                {tak najdi jeho koren}
            if koren<>aktkoren                 {ak sa lisi od korena akt. spoja}
              then Union(aktuk,aktkoren);      {tak tieto komponenty stotozni}
          end
          else begin                           {2. a ak nie je v zozname}
            new(pomuk);                        {tak ho tam zarad}
            pomuk^.meno:=aktmesto;
            pomuk^.pred:=aktkoren;             {a zarad ho pod koren akt. spoja}
            pomuk^.dalsi:=h[aktindex];
            h[aktindex]:=pomuk;
          end;
        end
        else                                   {B. a ak to je zakazane mesto}
          novyspoj:=true;                      {tak zacni akoby novy spoj}
      end;

    end;
  end;
end;(*****Zarad*****)


Procedure (***)Vypis(***) (koren:UkMesto);
var akt,pom : UkMesto;
    i       : integer;

begin
  for i:=0 to rozsah-1
  do begin
    akt:=h[i];
    while akt<>nil
    do begin
      if koren=Find(akt)
        then writeln(akt^.meno);
      akt:=akt^.dalsi;
    end;
  end;
end;(*****Vypis*****)


Procedure (***)Iniczakaz(***);
var pom : UkZakaz;

begin
  zaczakaz:=nil;
  while not eof(zf)
  do begin
    new(pom);
    readln(zf,pom^.meno);
    pom^.dalsi:=zaczakaz;
    zaczakaz:=pom
  end;
end;(*****Iniczakaz*****)


begin
  pocpar:=ParamCount;

  if pocpar>0
  then
    zakazsub:=ParamStr(1)
  else begin
    writeln('Enter ban file name:');
    readln(zakazsub);
  end;

  f := Input;

  {$I-}
  assign(zf,zakazsub);
  reset(zf);
  if IoResult<>0
  then begin
    writeln('Ban file not found.');
    halt(1)
  end;
  {$I+}

  Iniczakaz;
  for i:=0 to rozsah-1
    do h[i]:=nil;

  if pocpar>1
  then begin
    strtmeno:='';
    for i:=2 to ParamCount
      do strtmeno:=strtmeno+' '+ParamStr(i);
    delete(strtmeno,1,1);
  end
  else begin
    writeln('Enter the starting town:');
    readln(strtmeno);
  end;

  Zarad;

  x:=Najdi(strtmeno,strtuk,strtind);
  if x=true
  then begin
    strtkoren:=Find(strtuk);
    Vypis(strtkoren);
  end
  else
    writeln('Starting town not found in the data file, or starting town banned.');

  close(zf);
end.(*****Floodfill*****)
