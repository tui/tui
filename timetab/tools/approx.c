/*****************************************************************
approx - .tt file compress (timetable databse) by creating @ list  
Distribute under GPL version 2.   
Copyright (C),  Roman Krejcik, 2001,  roman.krejcik@ruk.cuni.cz   
******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> 
#include <string.h>
#include <limits.h>

#define HASH_TABLE_SIZE 32768UL
#define MULTIPY_IN_HASH_FUNCTION 28351UL

#define MAX_ENTRY 200000  /* max connections in input file */

#define MAX_ITEM_SIZE 80 /* max chars in string with station name and in items*/ 
#define MAX_NOTE_LENGTH 2000 /* max chars in connection atribute */

#define OPTION_NUMBER 4  /* number of flags */

enum {
  VERBOSE,
  ABSOLUTE_TIME,
  HELP,
  NO_COMPRESS
} OPTION; 

enum {
  SUCCESS,
  FEW_PARAM,
  MANY_PARAM,
  UNKNOWN_OPTION,
  ERR_OPEN_INPUT,
  ERR_OPEN_OUTPUT,
  MANY_CONNECTIONS,
  LONG_ITEM,
  LONG_NOTE,
  UNKNOWN_FORMAT,
  LONG_CONNECTION_NAME,
  NO_MEMORY
} ERROR_CODE;

struct ENTRY {
  long fp; // index to file  
  int hash; // index to hash table
};

struct LIST {  //list in hash table
  int ep; // index to array entry
  struct LIST *next;
};

struct HASH_TABLE {
  int count;
  struct LIST *first, *last;   /* last for  quick adding */
};

struct TIME {
  int h,m;   /* hours, minutes */
};

struct STATION {
  int km;
  struct TIME arr,dep;
  char note[MAX_ITEM_SIZE], name[MAX_ITEM_SIZE];
  struct STATION *next;
};

struct NOTE {
  char *label;
  struct NOTE *next;
};

struct COMPLETE_ENTRY {
  char name[MAX_ITEM_SIZE];
  struct STATION *station;
  struct NOTE *note;
};

struct ENTRY entry[MAX_ENTRY];   /* array for storing entry order */
int ep;   //  index to array entry 

struct HASH_TABLE hashtable[HASH_TABLE_SIZE];

FILE *in,*out;
static int last_item_length; /* in this variable returns function readitem item length */

int flag[OPTION_NUMBER];  /* flags for options, 0 inactive, 1 active */

int addentry(void); /* reads and adds entry to hash table, returns value: 0 succes,  1 fail */  
int readitem(char *item); /* reads one item and returns separator (\t,\n,eof,#) */
inline void after_seek(void); /* seek after first # */
inline unsigned long hashfunction(unsigned long suma);
int readentry(unsigned int *hash);  
unsigned long stringsuma(char *str);
int isemptystring(char *str);
int getentry(struct COMPLETE_ENTRY *e);
int getkm(char *item, int *lastkm);
struct TIME maketime(char *item, struct TIME *last);
void freeentry(struct COMPLETE_ENTRY *e);
void die(int err); /* write error and exit */
int isequal(struct COMPLETE_ENTRY *a, struct COMPLETE_ENTRY *b); /* is connections equal in stations and time diffs */
inline int timediff(struct TIME a, struct TIME b); /* counts difference between times in minutes */
inline struct TIME gettime(struct COMPLETE_ENTRY *e); /* returns first time in connection*/
void writeentry(struct COMPLETE_ENTRY *e, int tp, struct TIME *t); 
inline void writetime(struct TIME t,struct TIME *lastt);
int cmptime(const void *a, const void *b); /* compares two times */ 
void printhelp(void);

int main(int argc, char *argv[]) {
  int i,err;
  struct COMPLETE_ENTRY a,b;  /* structures for compare entries */ 
  struct LIST *hashstring;
  struct TIME *times;
  int tp;  /* index to times array */
  
  for(i = 1; i < argc; i++) {
    if (*argv[i] == '-') { /* option */
      if (*(argv[i]+1) == '-') { /* long option */
        if (strcmp(argv[i],"--verboose") == 0) {flag[VERBOSE] = 1; continue;}
        if (strcmp(argv[i],"--help") == 0) {flag[HELP] = 1; continue;}
        if (strcmp(argv[i],"--absolute-time") == 0) {flag[ABSOLUTE_TIME] = 1; continue;}
        if (strcmp(argv[i],"--no-compress") == 0) {flag[NO_COMPRESS] = 1; continue;}
      } 
      if (isalpha(*(argv[i]+1))) { /* short option */
        char *opt = argv[i]+1;
        while (*opt) {
	  switch (*opt) {
	  case 'h': flag[HELP] = 1; break;
	  case 'v': flag[VERBOSE] = 1; break;
	  case 'a': flag[ABSOLUTE_TIME] = 1; break;
	  case 'c': flag[NO_COMPRESS] = 1; break;
	  default: printf("%c %d \n",*opt, *opt); die(UNKNOWN_OPTION);  
	  }
	  opt++;
	}
        continue; 
      }
      die(UNKNOWN_OPTION);
    } else {  /* file name */
      if (!in) {
        if (!(in = fopen(argv[i],"r"))) die(ERR_OPEN_INPUT); 
        continue;
      }
      if (!out) {
        if (!(out = fopen(argv[i],"w"))) die(ERR_OPEN_OUTPUT); 
        continue;
      }
      die(MANY_PARAM);
    }
  }

  if (flag[HELP]) {
    printhelp();
    return 0;
  }
  
  if (!in) die(FEW_PARAM); 
  if (!out) { if (flag[VERBOSE]) flag[VERBOSE] = 0; out = stdout; }

  if (flag[VERBOSE]) printf("Reading data from input file to hash table.\n");
  
  after_seek();  /* seek after first # */    
  while (!feof(in)) {
    if ((err = addentry()) > 0) {
      die(err);
    }
  }  

  if (flag[VERBOSE]) {
    if (flag[NO_COMPRESS]) printf("Writing data to output file.\n");
    else  printf("Searching for equal connections and writing results to output file.\n");
  }

  for(i = 0; i< ep; i++) {
    if (entry[i].fp == -1) continue; /* already written in some @list */

    fseek(in,entry[i].fp,SEEK_SET);
    
    if ((err = getentry(&a)) > 0) die(err);     
   
    hashstring = hashtable[entry[i].hash].first; 

    tp = 0;
    times = (struct TIME *) malloc(hashtable[entry[i].hash].count*sizeof(struct TIME));
    if (!times) die(NO_MEMORY);

    while(hashstring->next && !flag[NO_COMPRESS]) {
      fseek(in,entry[hashstring->next->ep].fp,SEEK_SET);
      if ((err = getentry(&b)) > 0) die(err);
      if (isequal(&a,&b)) {
        struct LIST *temp = hashstring->next->next;;
        if ((a.station->arr.h == -1 && cmptime(&(a.station->dep),&(b.station->dep)) > 0) || cmptime(&(a.station->arr),&(b.station->dep)) > 0) {
	  /* b time is less then a time => change a,b */
	  struct COMPLETE_ENTRY temp = a;
	  a = b;
	  b = temp;
	}
	times[tp++] = gettime(&b);
        entry[hashstring->next->ep].fp = -1; /* mark as written in entry array */ 
        free(hashstring->next);
	hashstring->next = temp;
        hashtable[entry[i].hash].count--;
      } else hashstring = hashstring->next;
      freeentry(&b);
    }
    hashstring = hashtable[entry[i].hash].first->next;
    free(hashtable[entry[i].hash].first);
    hashtable[entry[i].hash].first = hashstring;
    hashtable[entry[i].hash].count--;
    writeentry(&a,tp,times);  
    free(times);
    freeentry(&a);
  }
  if (flag[VERBOSE]) printf("Done.\n");

  fclose(in);
  fclose(out);
  return SUCCESS;
}

int addentry(void) {
  int err;
  unsigned int ch;
  long position = ftell(in);
  struct LIST *newlist;
  
    
  if ((err = readentry(&ch)) > 0) return err;

  hashtable[ch].count++;

  newlist = (struct LIST *) malloc(sizeof(struct LIST));
  newlist->ep = ep;
  newlist->next = NULL;
  if (hashtable[ch].first == NULL) {
    hashtable[ch].first = hashtable[ch].last = newlist;
  } else {
    hashtable[ch].last -> next = newlist; 
    hashtable[ch].last = newlist; 
  }
  entry[ep].fp = position;
  entry[ep].hash = ch;
  ep++;
  if (ep == MAX_ENTRY) return MANY_CONNECTIONS;
  return SUCCESS;
}


int readitem(char *item) {
  int ch = getc(in), ip = 0;
  if (ch == '#' || feof(in)) {   /* next entry */
    item[0] = '#';
    item[1] = '\0';
    last_item_length = 2;
    if (feof(in)) return -1; else return '#';
  }
  while (ch != '\t' && ch != '\n') {
    item[ip++]= ch; 
    ch = getc(in); 
    if (ip == MAX_NOTE_LENGTH-2) { item[ip++] = '\0'; last_item_length = ip; return 0;}
    if (feof(in)) { ch = -1; break; }
  }
  item[ip++] = '\0';
  last_item_length = ip; /* write length to global variable */
  return ch;
}

inline void after_seek(void) {
  int ch = getc(in);
  while (ch != '#') {
    while (ch != '\n') {
      if (feof(in)) return;
      ch = getc(in);
    }
    ch = getc(in);
  }
}

inline unsigned long hashfunction(unsigned long suma) {
  return (unsigned long) (suma * MULTIPY_IN_HASH_FUNCTION) % HASH_TABLE_SIZE; 
}

int readentry(unsigned int *hash) {
  int i;
  char item[MAX_NOTE_LENGTH];
  unsigned long suma = 0;
  int separator;
  
  if (readitem(item) != '\n') return UNKNOWN_FORMAT; /*skip name */
  if (last_item_length > MAX_ITEM_SIZE - 2) return LONG_CONNECTION_NAME;
  while ((separator = readitem(item)) != '\t') {   /* skip empty lines */
    if (*item == ';') while (separator != '\n') separator = readitem(item);
    if (separator != '\n') { return UNKNOWN_FORMAT; }
  }
  for(;; separator = readitem(item)) {
    if (separator == '#' || isalpha(*item) || separator == -1) break; /* next entry or note */
    if (last_item_length > MAX_ITEM_SIZE - 2) return LONG_ITEM; 
    if (separator == '\n') continue; /* empty line, tag or remark without \t */
    if (*item == ';') { 
      while (separator != '\n') separator = readitem(item); /* ramark with \t */  
      continue;
    }
    for(i = 0; i < 4; i++) {
      if (separator != '\t') return UNKNOWN_FORMAT; 
      separator = readitem(item);
    }
    if (separator != '\n' && separator != -1) return UNKNOWN_FORMAT;    
    suma += stringsuma(item);  /* suma from station string */ 
  }
  while (*item != '#') {
    if (!isemptystring(item)) {
      if (*item == ';') {  /* remark */
        while (separator != '\n') separator = readitem(item);
      } else {
        if (!isalpha(*item)) return UNKNOWN_FORMAT;
        if (last_item_length > MAX_NOTE_LENGTH - 2) return LONG_NOTE;
        suma += stringsuma(item);
      }
    } 
    if (feof(in)) break;
    readitem(item);
  }
  *hash = (unsigned int) hashfunction(suma);
  return SUCCESS;
}

unsigned long stringsuma(char *str) {
  unsigned long suma = 0;
  while (*str) { suma += *str; str++; }
  return suma;  
}

int isemptystring(char *str) {
  while (*str++) { if (!isspace(*str)) return 0; }
  return 1;
}

int getentry(struct COMPLETE_ENTRY *e) {
  int ch = getc(in), i=0;
  struct STATION *s,*laststation = NULL;
  struct NOTE *note,*lastnote = NULL;
  struct TIME lasttime = {0, 0};
  char item[MAX_NOTE_LENGTH];
  int separator, lastkm = 0;
 
  e->station = NULL;
  e->note = NULL;

  while(ch != '\n') {
    e->name[i++]=ch;
    ch = getc(in);
  }
  e->name[i++]='\0';

  for(;;) {
    while  ((separator = readitem(item)) == '\n' && isemptystring(item)); /* skip empry lines */

    if (separator == -1 && isemptystring(item)) break; 
  
    if (*item == '#') break; /* begin of next entry */

    if (isalpha(*item)) {   /* note */
      note = (struct NOTE *) malloc(sizeof(struct NOTE));
      if (!note) return NO_MEMORY;
      note->label = (char *) malloc(last_item_length);
      if (!note->label) return NO_MEMORY;
      strcpy(note->label, item);
      note->next = NULL;
      if (lastnote == NULL) {
	e->note = note;
	lastnote = note;
      } else {
	lastnote->next = note;
	lastnote = note;
      }
    } else { /* regular row */
      if (*item == ';' || *item == '!') { /* tag or remark */
        while (separator != '\n') separator = readitem(item);
	continue;
      }
  
      s = (struct STATION *) malloc(sizeof(struct STATION));
      if (!s) return NO_MEMORY;
      s->arr = maketime(item, &lasttime);
      readitem(item);
      s->dep = maketime(item, &lasttime);
      readitem(item);
      strcpy(s->note,item);
      readitem(item);
      s->km = getkm(item, &lastkm);
      readitem(item);
      strcpy(s->name, item);
      s->next = NULL;
      if (laststation == NULL) {
        e->station = s;
        laststation = s;
      } else {
        laststation->next = s;
        laststation = s;
      }
    }
    if (separator == -1) break;
  }
  return 0;
}  

int getkm(char *item, int *lastkm) {
  int rel = 0, km = 0;
  if (*item == '+') { rel++; item++; }
  while (*item) { if (isdigit(*item)) km = (km*10) + *item - '0'; else die(UNKNOWN_FORMAT); item++; }
  if (rel) { *lastkm += km; return *lastkm; } else { *lastkm = km; return km; } 
}

struct TIME maketime(char *item, struct TIME *last) {
  struct TIME ret;
  if (*item == '\0') {  /* no time in item */
    ret.h = -1;
    ret.m = -1;
  } else {
    if (*item == '+') { /* relative time */
      item++;
      ret.m = 0;
      while (*item) { 
        if (!isdigit(*item)) die(UNKNOWN_FORMAT);
        ret.m = ret.m*10 + (*item) - '0';
	item++;
      }
      ret.m += last->m;
      ret.h = last->h + (ret.m / 60);
      ret.m %= 60;
      last->m = ret.m;
      last->h = ret.h;
    } else { /* absolute time */
      int i = 0;
      ret.h = 0;
      while (item[i]!=':') {
        if (!isdigit(item[i])) die(UNKNOWN_FORMAT);
        ret.h = ret.h * 10 + item[i] - '0';
	i++;
      }
      ret.m = 0; 
      i++;
      while (item[i]) {
        if (!isdigit(item[i])) die(UNKNOWN_FORMAT); 
        ret.m = ret.m*10 + item[i] - '0';
	i++;
      }
      last->h = ret.h;
      last->m = ret.m;
    }
  }
  return ret;
}

void freeentry(struct COMPLETE_ENTRY *e) {
  struct STATION *s1 = e->station, *s2;
  struct NOTE *n1 = e->note,*n2;
  while (s1) {
    s2 = s1;
    s1 = s1->next;
    free(s2);
  }
  while (n1) {
    n2 = n1;
    n1 = n1->next;
    free(n2->label);
    free(n2);
  } 
}

void die(int err) {
  char *message[] = { 
    "",
    "Too few parameters.\nTry -h or --help for more information.\n",
    "Too many parameters.\nTry -h or --help for more information.\n",
    "Unknown option.\nTry -h or --help for more information.\n",
    "Cannot open input file.\n",
    "Cannot open output file.\n",
    "Too many connection in input file. Reading failed.\n",
    "Too long item in input file. Reading failed.\n",
    "Too long note in input file. Reading failed.\n",
    "Reading error. Unknown format or input file corrupted.\n",
    "Too long connection name in input file. Reading failed.\n",
    "No free memory available.\n"
  };
  
  fprintf(stderr, message[err]);
  exit(err);
}

int isequal(struct COMPLETE_ENTRY *a, struct COMPLETE_ENTRY *b) {
  struct STATION *as = a->station,*bs = b->station;
  struct NOTE *an = a->note, *bn = b->note;   
  int tdiff = INT_MAX;

  while (as && bs) {
    if (strcmp(as->name,bs->name) != 0 || as->km != bs->km) return 0;
    if (as->dep.h == -1) {
      if (bs->dep.h != -1) return 0;
    } else {
      if (tdiff == INT_MAX) tdiff = timediff(as->dep,bs->dep); 
      else if (tdiff != timediff(as->dep,bs->dep)) return 0; 
    }
    if (as->arr.h == -1) {
      if (bs->arr.h != -1) return 0;
    } else {
      if (tdiff == INT_MAX) tdiff = timediff(as->arr,bs->arr); 
      else if (tdiff != timediff(as->arr,bs->arr)) return 0; 
    }
    as = as->next;
    bs = bs->next;
  }
  if (as || bs) return 0; /* one station list is shorter */ 

  while (an && bn) {
    if (strcmp(an->label,bn->label) != 0) return 0;
    an = an->next;
    bn = bn->next; 
  }  
  if (an || bn) return 0; /* one note list is shorter */

  return 1;
}

inline int timediff(struct TIME a, struct TIME b) {
  return ((b.h - a.h)*60)+b.m-a.m;
}

inline struct TIME gettime(struct COMPLETE_ENTRY *e) {
  return e->station->arr.h == -1 ? e->station->dep : e->station->arr;
}

void writeentry(struct COMPLETE_ENTRY *e, int tp, struct TIME *t) {
  struct STATION *s = e->station;
  struct NOTE *n = e->note;
  struct TIME *lasttime;
  int lastkm = -1;

  lasttime = (struct TIME *) malloc(sizeof(struct TIME));
  if (!lasttime) die(NO_MEMORY); 
  if (tp==0) { /* single connection */
    fprintf(out,"#%s\n",e->name);
    lasttime->h = -1;
    if (flag[ABSOLUTE_TIME] == 1) flag[ABSOLUTE_TIME]++;  /* flag[ABSOLUTE] == 2 means single connecet + flag -> print absolute time */
  } else {    /* @list of connections */
    int i,*diffs,dcounter;
    struct TIME less = gettime(e);

    fprintf(out,"@%s %d:%02d",e->name,less.h,less.m);
    qsort(t,tp,sizeof(t[0]),cmptime);

    diffs = (int *) malloc(tp * sizeof(int)); 
    if (!diffs) die(NO_MEMORY);   

    for(i = 0; i < tp; i++) diffs[i] = timediff(less,t[i]);  
    for(i = tp-1; i > 0; i--) diffs[i] -= diffs[i-1];

    dcounter = 1;
    for(i = 1 ; i < tp; i++) {
      if (diffs[i] != diffs[i-1]) {
        fprintf(out," %dx%d",dcounter,diffs[i-1]);
	dcounter = 1;
      } else dcounter++;
    }
    fprintf(out," %dx%d\n",dcounter,diffs[i-1]);
    
    free(diffs);
    *lasttime = less;
    if (flag[ABSOLUTE_TIME] == 2) flag[ABSOLUTE_TIME]--;
  }  
  if ((s->arr.h > 24 || s->dep.h > 24)  && flag[ABSOLUTE_TIME] == 2) flag[ABSOLUTE_TIME]--; /* 25:00 connection always print in relative time */   

  while (s) {
    writetime(s->arr,lasttime);
    putc('\t',out);
    writetime(s->dep,lasttime);
    fprintf(out,"\t%s\t",s->note);
    if (lastkm == -1)  fprintf(out,"%d\t",s->km); else  fprintf(out,"+%d\t",s->km - lastkm); 
    lastkm = s->km;
    fprintf(out,"%s\n",s->name);
    s = s->next;
  }
  while (n) {
    fprintf(out,"%s\n",n->label);
    n = n->next;
  }
  
  free(lasttime);
}

inline void writetime(struct TIME t,struct TIME *lastt) {
  if (t.h == -1) return;
  if (lastt->h == -1 || flag[ABSOLUTE_TIME] == 2) fprintf(out,"%d:%02d",t.h,t.m); 
  else {
    int diff = timediff(*lastt,t);
    if (diff<0) fprintf(out,"%d:%02d",t.h,t.m); else fprintf(out,"+%d",diff);
  }
  *lastt = t;
}

int cmptime(const void *a, const void *b) {
  struct TIME *aa = (struct TIME *) a ,*bb = (struct TIME *) b;
  if (aa->h < bb->h) return -1;
  if (aa->h > bb->h) return 1;
  if (aa->m < bb->m) return -1;
  if (aa->m > bb->m) return 1;
  return 0;
}

void printhelp(void) {
  printf("approx compress .tt file (timetable database format).\nCreate .tt with @list connections.\nWrite result to standart output or given output file\nThis program is distiributed under GPL v2.\n\nUsage: approx [options] infile [outfile]\n\nOptions:\n   -a, --absolute-time\tabsolute time in output file instead increment\n   -h, --help\t\tprint this help\n   -v, --verbose\tprint info about performed operations,\n\t\t\t  if output file isn't given this option is ignored.\n   -c, --no-compress\tdon't create @list, can be used to convert\n\t\t\t  absolute and relative time...\n\n");
}
