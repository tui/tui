/* -*- linux-c -*-
 *
 * Pass only some connections, based on their id
 *
 * Copyright 2000 Pavel Machek, GPL
 */
#include <stdio.h>

int
main(int argc, char *argv[])
{
	char buf[10240];
	int pass = 1, negate = 0;
	if (argv>1 && !strcmp(argv[1], "-n"))
		negate = 1, pass = 0;
	while (gets(buf)) {
		if (buf[0] == '#') {
			int i;
			pass = 0;
			for (i=1; i<argc; i++)
				if (!strncmp(argv[i], buf+2, strlen(argv[i])))
					pass = 1;
		}
		if (pass ^ negate)
			puts(buf);
	}
	return 0;
}

