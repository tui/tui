{***************************************************************************}
{Citydate version 1, Copyright 2001 Jaroslav Dra�an <jaroslavdrazan@seznam.cz>}
{                    Copyright 2001 Pavel Machek <pavel@ucw.cz>}
{Citydate is covered by GPL}
{There is absolutly no warranty for Citydate}
{***************************************************************************}

Program Citydate;
uses dos;

const
 delka = 150;
Type
  Pstrings = ^Tstrings;
  Tstrings = record
    slovo: string[delka];
    next: Pstrings;
   end;
 uk = ^spojak;
 spojak = record
    mesto:string[delka];
    velke:string[delka];
    velkeASCII:string[delka];
    BezYZ:string[delka];
    bezoddelovacu:string[delka];
    slova:Pstrings;
    next:uk;
   end;

var
 seznam,kol: uk;
 cas1,datum1,zadani1,zadani2,cesta,pomoc:string;
 i,j:word;
 pravda: boolean;

{odstrani ze slova oddelovace}
function bezodd (s:string):string;
var i:integer;
    b:string;
begin
 b:='';
 for i:= 1 to length(s) do
    case s[i] of
     ' ','.',',',':','(',')','\','/','|','#','"','[',']',';','-' : ;
    else
      b:=b+s[i];
    end;
 bezodd:= b;
end; {end bezodd}

function ASCII (a:string):string;
 var i:integer;
 begin
  for i:=1 to length(a) do
   case a[i] of
    #161,#193..#196     : a[i]:='A';
    #198..#200          : a[i]:='C';
    #207..#208          : a[i]:='D';
    #201..#204          : a[i]:='E';
    #205,#206,#222      : a[i]:='I';
    #163,#165,#197      : a[i]:='L';
    #209,#210           : a[i]:='N';
    #211..#214          : a[i]:='O';
    #192,#216           : a[i]:='R';
    #166,#167,#169,#170 : a[i]:='S';
    #171                : a[i]:='T';
    #217..#220          : a[i]:='U';
    #221                : a[i]:='Y';
    #172,#174,#175      : a[i]:='Z';
    #177,#225..#228     : a[i]:='a';
    #230..#232          : a[i]:='c';
    #239..#240          : a[i]:='d';
    #233..#236          : a[i]:='e';
    #237..#238          : a[i]:='i';
    #179,#181,#229      : a[i]:='l';
    #241,#242           : a[i]:='n';
    #243..#246          : a[i]:='o';
    #224,#248           : a[i]:='r';
    #182,#185,#186,#223 : a[i]:='s';
    #187,#254           : a[i]:='t';
    #249..#252          : a[i]:='u';
    #253                : a[i]:='y';
    #188,#190,#191      : a[i]:='z';
   end; {end case}
  ASCII:=a;
 end; {end acsi}

{prevede slova na velka pismena}
function nafuk (a:string):string;
 var i:integer;
 begin
  for i:=1 to length(a) do
    case a[i] of
     #97..#122,#224..#246,#248..#254          : a[i]:=chr(ord(a[i]) -32);
     #177,#179,#181,#182,#185..#188,#190,#191 : a[i]:=chr(ord(a[i]) -16);
    end; {end case}
  nafuk:=a;
 end; {end nafuk}

{zjisti, zda je dane mesto v seznamu}
function test (a:string;b:uk):boolean;
 var q:uk;
 begin
  test:= false;
  q:=b ;
  while q<>nil do
   if q^.mesto = a then
     begin
      test:=true;
      exit;
     end
                   else
            q:=q^.next;
 end;  {function test}

function YZ (a:string): string;
 var i: integer;
     b:string;
 begin
  b:='';
  for i:= length(a) downto 1 do
   case a[i] of
   'z','Z','y','Y': ;
    else
     b:= a[i]+b;
   end;
  YZ:= b;
 end; {end function YZ}

procedure add (a:string; var b:uk);
 var
   q:uk;
 begin
   new(q);
   q^.mesto:= a;
   q^.next:= b;
    b:=q;
 end;   {procedure add}

Procedure newseznam (var p:uk;c:string);
 var t:text;
     s:string;
 begin
  assign(t,c);
  reset(t);
  readln(t,s);
  new(p);
  p:=nil;
  while not eof(t) do
   begin
    add(s,p);
    readln(t,s);
   end;
 end;  {procedure newseznam}

Procedure zrusseznam (var p:uk);
 var
     q:uk;
 begin
   begin
    q:=p;
    while q<> nil do
     begin
      p:=q^.next;
      dispose(q);
      q:=p;
     end;
   end;
 end; {procedure zrusseznam}

function JeTamOdd (a:string):boolean;
 var i: integer;
 begin
  JeTamOdd:= false;
  for i:= 1 to length(a) do
   case a[i] of
    ' ','.',',',':','(',')','\','/','|','#','"','[',']',';','-'
                                                            :begin
                                                              JeTamOdd:= true;
                                                              exit;
                                                             end;
   end;
end; {end function JeTamOdd}

{vrati pocet shodnych pismen ve dvou retezcich}
function porovnej (a,b:string): integer;
 var i,j:integer;
 begin
  j:=0;
  if length(a) > length(b) then
   i:= length(b)
                           else
                            i:= length(a);
  for i:= 1 to i do
    if a[i] = b[i] then
     j:= j+1;
  porovnej:= j;
 end;{end function porovnej}

procedure pridejVelke (var seznam:uk);
 var q:uk;
 begin
  q:=seznam;
  while q<> nil do
  begin
   q^.velke:= nafuk(q^.mesto);
   q:=q^.next;
  end;
 end; {procedure pridejVelke}

procedure pridejVelkeASCII (var seznam:uk);
 var q:uk;
 begin
  q:=seznam;
  while q<> nil do
  begin
   q^.velkeASCII:= ASCII(q^.velke);
   q:=q^.next;
  end;
 end; {procedure pridejVelkejASCII}

procedure pridejBezodd (var seznam:uk);
 var q:uk;
 begin
  q:=seznam;
  while q<> nil do
  begin
   q^.bezoddelovacu:= bezodd(q^.velkeASCII);
   q:=q^.next;
  end;
 end; {procedure pridejBezodd}

procedure pridejBezyz (var seznam:uk);
 var q:uk;
 begin
  q:=seznam;
  while q<> nil do
  begin
   q^.bezyz:= yz(q^.Bezoddelovacu);
   q:=q^.next;
  end;
 end; {procedure pridejBezyz}

 {tato procedure vytvori spojak vice slovnych mest}
 procedure NewViceSlov (var seznam:uk;var vysledek:uk);
 var k,r,q:uk;
 begin
  q:=seznam;
  vysledek:= nil;
  k:= nil;
  r:=nil;
  while q<> nil do
   begin
    if JeTamOdd(q^.mesto) then
     begin
      new(r);
      r^.mesto:= q^.mesto;
      r^.velke:= q^.velke;
      r^.velkeASCII:= q^.velkeASCII;
      r^.BezYZ:= q^.BezYZ;
      r^.bezoddelovacu:= q^.bezoddelovacu;
      r^.next:= k;
      k:= r;
     end;
    q:=q^.next;
   end;
  vysledek:= k;
 end; {procedure NewViceSlov}

procedure Rozdel (a:string;var vysledek:Pstrings);
 var i:integer;
   q,r,k:Pstrings;
       b:string;
 begin
  vysledek:= nil;
  k:=nil;
  r:=nil;
  b:='';
  for i:= length (a) downto 1 do
   begin
    case a[i] of
    ' ','.',',',':','(',')','\','/','|','#','"','[',']',';','-'
                                                             :begin
                                                              if b<>''then
                                                              begin
                                                              new(r);
                                                              r^.slovo:= b;
                                                              r^.next:= k;
                                                              k:=r;
                                                              b:='';
                                                              end;
                                                             end
    else
     b:=a[i]+b;
    end;{end case}
    if (i = 1) and (b<>'') then
     begin
      new(r);
      r^.slovo:= b;
      r^.next:= k;
      k:=r;
     end;
   end;
  vysledek:= k;
 end; {procedure rozdel}

procedure PridejSlova (var seznam:uk);
 var q:uk;
     r:Pstrings;
 begin
  q:=seznam;
  while q <> nil do
   begin
    rozdel(q^.VelkeASCII,r);
    q^.slova:=r;
    q:=q^.next;
   end;
 end;{procedure PridejSlova}



procedure NewDleDelky (var seznam:uk;zadani:string;var vysledek:uk);
 var i:integer;
     q,r,k:uk;
 begin
  q:= seznam;
  i:= length(bezodd(zadani))-1;
  vysledek:= nil;
  k:=nil;
  while q <> nil do
   begin
    if length(q^.bezoddelovacu) in [i..i+2] then
     begin
      new(r);
      r^.mesto:= q^.mesto;
      r^.velke:= q^.velke;
      r^.velkeASCII:= q^.velkeASCII;
      r^.BezYZ:= q^.BezYZ;
      r^.bezoddelovacu:= q^.bezoddelovacu;
      r^.next:= k;
      k:= r;
     end;
    q:= q^.next;
   end;
  r:=nil;
 vysledek:= k;
 end; {procedure NewDleDelky}

{zjistim,zda se slovo a (to spravne) od slova b lisi jen nepatrne,
pokud plati ||a|-|b||<2}
function preklep (a,b:string):boolean;
 var pomoc:string;
     i,j,k:integer;
 begin
  preklep:= false;
  pomoc:= b;
  k:= length(b);
  j:= length(a);
    if j > k then
     {muze chybet pismeno}
     begin
      for i:= 1 to k do
       if a[i] <> b[i] then
        begin
         insert(a[i],pomoc,i);
         if a = pomoc then
          begin
           preklep:= true;
           exit;
          end;
         pomoc:= b;
        end;
      if b+a[j] = a then
        begin
         preklep:= true;
         exit;
        end;
     end;
    if j < k then
     {muze prebyvat pismeno}
     begin
      for i:= 1 to j do
       if a[i] <> b[i] then
        begin
         delete(pomoc,i,1);
         if a = pomoc then
          begin
           preklep:=true;
           exit;
          end;
         pomoc:= b;
        end;
      if b = a+b[k] then
        begin
         preklep:= true;
         exit;
        end;
     end;
    if j = k then
     begin
     {muze jit o presmycku ci preklep v jednom pismenu}
      {zkouska preklepu}
      for i:= 1 to k do
       if a[i] <> b[i] then
        begin
         delete(pomoc,i,1);
         insert(a[i],pomoc,i);
         if a = pomoc then
          begin
           preklep:=true;
           exit;
          end;
          pomoc:= b;
        end;
      {zkouska presmycky, pr. Olomouc = Olmoouc }
      j:=0;
      for i:= 1 to k do
       if (a[i] <> b[i]) then
        begin
         if j = 0 then
          j:=  i
                  else
                   begin
                    pomoc[j]:= a[j];
                    pomoc[i]:= a[i];
                    if pomoc = a then
                     begin
                      preklep:= true;
                      exit;
                     end;
                    pomoc:= b;
                   end;
        end;
      {zkouska presmycky, pr. Olomouc = Oluomoc }
      j:=0;
      pomoc:= b;
      for i:= 1 to k do
       begin
       if (a[i] <> pomoc[i]) then
        begin
         if j = 0 then
          begin
           j:=  i;
           delete(pomoc,i,1);
          end
                  else
                   begin
                    insert(a[i],pomoc,i);
                    if length(pomoc) > k then
                     delete(pomoc,k+1,1);
                    if pomoc = a then
                     begin
                      preklep:= true;
                      exit;
                     end;
                    pomoc:= b;
                   end;
        end;
       if (i=k-1) and (j <> 0) then
         insert('.',pomoc,k);
       end;
      pomoc:= b;
      {zkouska presmycky, pr. Olomouc = Oomoluc }
      j:=0;
      for i:= 1 to k do
       begin
       if (a[i] <> pomoc[i]) then
        begin
         if j = 0 then
          begin
           j:=  i;
           insert(a[i],pomoc,i);
          end
                  else
                   begin
                    delete(pomoc,i,1);
                    if pomoc = a then
                     begin
                      preklep:= true;
                      exit;
                     end;
                    pomoc:= b;
                   end;
        end;
         if (i = k) and (j<>0) then
          begin
           delete(pomoc,length(pomoc),1);
           if pomoc = a then
            begin
             preklep:= true;
             exit;
            end;
          end;
       end;
     end; {zkoumani preklepu a presmycky}
 end; {function preklep}

 {pomocna procedura prida prvek q do seznamu h}
 procedure pridej (q:uk;var h:uk);
  var r:uk;
  begin
   new(r);
   r^.mesto:= q^.mesto;
   r^.velke:= q^.velke;
   r^.velkeASCII:= q^.velkeASCII;
   r^.BezYZ:= q^.BezYZ;
   r^.bezoddelovacu:= q^.bezoddelovacu;
   r^.next:= h;
   h:= r;
  end; {procedure pridej}

{v teto fci zkoumam jednoduche preklepy, seznam v tomto pripade je vytvoren
procedurou NewDleDelky}
function drobnost (var seznam:uk;zadani:string;var vysledek:string): boolean;
 var
  i,k: integer;
  q,r,h:uk;
  bodd:string;
  byz:string;
 begin
  drobnost:= false;
  bodd:= ASCII(bezodd(nafuk(zadani)));
  byz:= yz(bodd);
  h:= nil;
  q:= seznam;
  while q <> nil do
   begin
    if preklep(q^.bezyz,byz) then
      pridej(q,h);
    q:=q^.next;
   end;
  if h <> nil then
   begin
    i:=0;
    q:=h;
    r:=h;
    while q <> nil do
     begin
      k:= porovnej(bodd,q^.bezoddelovacu);
      if k > i then
       begin
        i:=k;
        r:=q;
       end;
      q:=q^.next;
     end;
    drobnost:= true;
    vysledek:= r^.mesto;
   end;
  end; {function drobnost}

function JeTamVelke (var seznam:uk;zadani:string;var vysledek:string): boolean;
 var
  vyber,r,q: uk;
  big:string;
  i,j,k:longint;
 begin
 JeTamVelke:= false;
 q:= seznam;
 vyber:= nil;
 big:= nafuk(zadani);
 while q<>nil do
  begin
   if big = q^.velke then
     begin
      new(r);
      r^.mesto:= q^.mesto;
      r^.velke:= big;
      r^.next:= vyber;
      vyber:= r;
     end;
    q:=q^.next;
  end;
  q:=vyber;
  {pokud jsem nalezl nejaka podobna mesta, tak hledam to nejpodobnejsi}
  if vyber <> nil then
   begin
    k:=0;
    while q <> nil do
     begin
      j:= porovnej(zadani,q^.mesto);
      if (k < j) then
        begin
         k:=j;
         r:=q;
        end;
      q:=q^.next;
     end;{end while}
    JeTamVelke:= true;
    vysledek:= r^.mesto;
    zrusseznam(vyber);
   end; {if vyber...}
 end;{function JeTamVelke}


Function JeTamVelkeASCII (var seznam:uk;zadani:string;var vysledek:string): boolean;
 var
  vyber,r,q: uk;
  big,ABC:string;
  i,j,k:longint;
 begin
  JeTamVelkeASCII:= false;
  vyber:= nil;
  big:= nafuk(zadani);
  ABC:= ASCII(big);
  q:= seznam;
  while q<>nil do
   begin
    if ABC = q^.velkeASCII then
     begin
      new(r);
      r^.mesto:=q^.mesto;
      r^.velke:=q^.velke;
      r^.velkeASCII:= ABC;
      r^.next :=vyber;
      vyber:=r;
     end;
    q:=q^.next;
  end;
  q:=vyber;
  {pokud jsem nalezl podobne mesto, zacnu hledat to nejpodobnejsi}
  if vyber <> nil then
   begin
    k:=0;
    while q <> nil do
     begin
      j:= porovnej(big,q^.velke);
      if (k < j) then
        begin
         k:=j;
         r:= q;
        end;
      q:=q^.next;
     end;{end while}
    JeTamVelkeASCII:= true;
    vysledek:= r^.mesto;
    zrusseznam(vyber);
   end; {if vyber...}
 end;{function JeTamVelke}

function JeTamBezodd (var seznam:uk;zadani:string;var vysledek:string): boolean;
 var
  vyber,r,q: uk;
  big,ABC,bodd1,bodd2:string;
  i,j,k:longint;
 begin
  JeTamBezodd:= false;
  vyber:= nil;
  big:= nafuk(zadani);
  ABC:= ASCII(big);
  bodd1:= bezodd(ABC);
  bodd2:= bezodd(big);
  q:= seznam;
  while q<>nil do
   begin
    if bodd1 = q^.bezoddelovacu then
     begin
      new(r);
      r^.mesto:=q^.mesto;
      r^.velke:=q^.velke;
      r^.velkeASCII:= q^.velkeASCII;
      r^.bezoddelovacu:= bodd1;
      r^.next :=vyber;
      vyber:=r;
     end;
    q:=q^.next;
  end;
  q:=vyber;
  {pokud jsem nalezl podobne mesto, zacnu hledat to nejpodobnejsi}
  if vyber <> nil then
   begin
    k:=0;
    while q <> nil do
     begin
      j:= porovnej(bodd2,bezodd(q^.velke));
      if (k < j) then
        begin
         k:=j;
         r:= q;
        end;
      q:=q^.next;
     end;{end while}
    JeTambezodd:= true;
    vysledek:= r^.mesto;
    zrusseznam(vyber);
   end; {if vyber...}
 end;{function JeTamBezodd}

function JeTamBezyz (var seznam:uk;zadani:string;var vysledek:string): boolean;
 var
  vyber,r,q,pom: uk;
  big,ABC,bodd,byz1,byz2:string;
  i,j,k:longint;
 begin
  JeTamBezyz:= false;
  vyber:= nil;
  big:= nafuk(zadani);
  ABC:= ASCII(big);
  bodd:= bezodd(big);
  byz2:= yz(bodd);
  byz1:= ASCII(byz2);
  q:= seznam;
  while q<>nil do
   begin
    if byz1 = q^.bezyz then
     begin
      new(r);
      r^.mesto:=q^.mesto;
      r^.velke:=q^.velke;
      r^.velkeASCII:= q^.velkeASCII;
      r^.bezoddelovacu:= q^.bezoddelovacu;
      r^.bezyz:=byz1;
      r^.next :=vyber;
      vyber:=r;
     end;
    q:=q^.next;
  end;
  {pokud jsem nalezl podobne mesto, zacnu hledat to nejpodobnejsi}
  q:=vyber;
  if vyber <> nil then
   begin
    k:=0;
    r:= nil;
    while q <> nil do
     begin
      j:= porovnej(byz2,yz(bezodd(q^.velke)));
      if (k < j) then
        begin
         k:=j;
         zrusseznam(r);
         new(r);
         r^.mesto:=q^.mesto;
         r^.velke:=q^.velke;
         r^.velkeASCII:= q^.velkeASCII;
         r^.bezoddelovacu:= q^.bezoddelovacu;
         r^.bezyz:=byz1;
         r^.next :=nil;
        end
                 else
                  if (k=j) then
                   begin
                    new(pom);
                    pom^.mesto:=q^.mesto;
                    pom^.velke:=q^.velke;
                    pom^.velkeASCII:= q^.velkeASCII;
                    pom^.bezoddelovacu:= q^.bezoddelovacu;
                    pom^.bezyz:=byz1;
                    pom^.next:=r;
                    r:=pom;
                   end;
      q:=q^.next;
     end;{end while}
    if r =  nil then
     begin
      JeTambezyz:= true;
      vysledek:= vyber^.mesto;
      zrusseznam(vyber);
      exit;
     end;
    zrusseznam(vyber);
    vyber:= r;
    q:= vyber;
    k:= 0;
    while q <> nil do
     begin
      j:= porovnej(bodd,bezodd(q^.velke));
      if (k < j) and (length(bodd) = length(bezodd(q^.velke))) then
        begin
         k:=j;
         r:= q;
        end;
      q:=q^.next;
     end;{end while}
    if r = nil then
     begin
      JeTambezyz:= true;
      vysledek:= vyber^.mesto;
      zrusseznam(vyber);
      exit;
     end;
    JeTambezyz:=true;
    vysledek:= r^.mesto;
    zrusseznam(vyber);
   end; {if vyber...}
 end;{function JeTamBezyz}

function PocetSlov (seznam:Pstrings): integer;
 var q:Pstrings;
     i:integer;
 begin
  q:= seznam;
  i:= 0;
  while q <> nil do
   begin
    i:= i+1;
    q:= q^.next;
   end;
  Pocetslov:= i;
 end; {function PocetSlov}

procedure zrusseznam1 (var a: pstrings);
var
 q:Pstrings;
begin
 q:= a;
 while a<> nil do
  begin
   a:= a^.next;
   dispose(q);
   q:= a;
  end;
end; {procedure zrusseznam1}
{tato procedura smaze 1 slovo ze seznamu, vim ze neni pazdny}
procedure vyluc (var a,seznam: Pstrings);
var
 q: Pstrings;
begin
 q:= seznam;
 while (q<>nil) and (q^.next <> a) do
  q:= q^.next;
 if (q<>nil) and (a<>nil) then
 begin
  q^.next:= a^.next;
  dispose(a);
 end;
end;{procedure vyluc}

{ Tato procedura zkopiruje data ze zadaneho seznamu do jineho seznamu}
procedure okopiruj (var a,seznam: Pstrings);
var
 q,s: Pstrings;
begin
 q:= seznam;
 a:= nil;
 while q<> nil do
  begin
   new(s);
   s^.slovo:= q^.slovo;
   s^.next:= a;
   a:=s;
   q:= q^.next;
  end;
end; {procedure okopiruj}

function PocetStejnychSlov (b,a:Pstrings):real;
 var
  q,p,r:Pstrings;
  i:real;
 begin
  pocetstejnychslov:= 0;
  i:=0;
  okopiruj(r,b);
  p:= r;
  q:= a;
  while q <> nil do
   begin
    while p <> nil do
     begin
      if q^.slovo = p^.slovo then
       begin
        if length(q^.slovo) < 3 then
         i:= i - 0.2;
        i:= i+1;
        vyluc(p,r);
        break;
       end;
      p:=p^.next;
     end;{while2}
    q:=q^.next;
    p:=r;
   end;{while1}
  PocetStejnychSlov:=i;
 end;{function PocetStejnychSlov}

function PocetPodobSlov (b,a:Pstrings):real;
 var
  q,p,r:Pstrings;
  i:real;
 begin
  Pocetpodobslov:= 0;
  if (a=nil) or (b = nil) then exit;
  i:=0;
  okopiruj(r,b);
  p:=r;
  q:=a;
  while q <> nil do
   begin{1}
    while p <> nil do
     begin{2}
      if (q^.slovo <> '') and (p^.slovo <> '') and(preklep(q^.slovo,p^.slovo)) then
       begin {3}
        if (p^.slovo <>'') and (length(p^.slovo) = 1) then
         i:= i - 0.3;
        vyluc(p,r);
        i:= i+1;
        break;
       end;{3}
      p:=p^.next;
     end;{2}
    q:=q^.next;
    p:=r;
   end;{1}
  PocetPodobSlov:=i;
 end;{function PocetPodobSlov}

{Udela slovo z prvnich pismen slov mesta}
function PrvniPismena (q:Pstrings): string;
 var a:Pstrings;
     s:string;
 begin
  a:= q;
  s:= '';
  while a <> nil do
   begin
    s:= s + a^.slovo[1];
    a:= a^.next;
   end;
  PrvniPismena:= s;
 end; {function PrvniPismena}

{tato fce se pokuci doplnit spravnou koncovku,pokud zadane slovo neni moc
zdeformovane, seznam ktery sem zadavam je ten, ktery se zklada pouze
z viceslovnych slov,opravovane mesto se sklada z viceslov}
function Koncovka1 (var seznam:uk;zadani:string;var vysledek:string): boolean;
 var
  q,r,vyber1,vyber2:uk;
  Pstr,p1,p2,Pstr2,pomoc:Pstrings;
  i:integer;
  k1,k2,j:real;
  d,mn:string;
 begin
  Koncovka1:= false;
  if seznam = nil then exit;
  q:= seznam;
  rozdel(nafuk(ASCII(zadani)), Pstr);
  k1:= pocetslov(Pstr);j:=0;
  pomoc:= Pstr;
  i:=  0;
  while pomoc <> nil do
   begin
    if length(pomoc^.slovo) < 3 then
     i:= i+1;
    pomoc:= pomoc^.next;
   end;
  {test na pouze prehozena slova}
  while q<> nil do
   begin
    if pocetstejnychslov(q^.slova,Pstr) >= k1-0.2*i then
      begin
       vysledek:= q^.mesto;
       Koncovka1:= true;
       exit;
      end;
    q:= q^.next;
   end;{konec testu na prehozena slova}
  vyber1:= nil;
  k1:=0;
  q:= seznam;
  while (q<> nil) and (pocetstejnychslov(q^.slova,Pstr)  +
      pocetpodobslov(q^.slova,Pstr) = 0) do
       q:= q^.next;
  if q<> nil then
   begin
    pridej(q,vyber1);
    k1:=1.1*pocetstejnychslov(q^.slova,Pstr)  +
        pocetpodobslov(q^.slova,Pstr);
    if pocetslov(q^.slova)= pocetslov(Pstr) then
     k1:= k1 + 0.05;
    q:= q^.next;
   end;
  while q<> nil do
   begin
    j:= (1.1*pocetstejnychslov(q^.slova,Pstr)  +
      pocetpodobslov(q^.slova,Pstr));
    if pocetslov(q^.slova)= pocetslov(Pstr) then
     j:= j + 0.05;
    if (j > k1) then
      begin
       dispose(vyber1);
       vyber1:= nil;
       pridej(q,vyber1);
       k1:= j;
      end;
    q:= q^.next;
   end;
   {pocad jsem delal vyber nejpodobnejsi moznosti(preklep ve vice slovech),
    ted se vrhnu na zkomolene zkratky}
  d:= PrvniPismena(Pstr);
  q:= seznam;
  k2:= 0;
  vyber2:= nil;
  while q<> nil do
   begin{1}
    j:= 0;
    mn:= PrvniPismena(q^.slova);
    if length(d)= length(mn) then j:= j+0.05;
    p1:= Pstr;
    if (d=mn)or (preklep(d,mn)) then
     begin{2}
      okopiruj(Pstr2,q^.slova);
      while p1<> nil do
       begin{3}
        p2:= pstr2;
        while p2 <> nil do
         begin{4}
         if (length(p1^.slovo) < length(p2^.slovo))
          and (pos(p1^.slovo,p2^.slovo) = 1) then
           begin
            vyluc(p2,pstr2);
            j:= j+1;
            break;
           end;
         if (length(p1^.slovo) = length(p2^.slovo))
          and (p1^.slovo= p2^.slovo) then
           begin
            vyluc(p2,Pstr2);
            j:= j +1.1;
            break;
           end;
         if (length(p1^.slovo) > length(p2^.slovo))
          and (pos(p2^.slovo,p1^.slovo) = 1) then
           begin
            vyluc(p2,Pstr2);
            j:= j +1;
            break;
           end;
         p2:= p2^.next;
         end;{4}
        p1:= p1^.next;
       end;{3}
      if k2 < j then
       begin
        vyber2:= q;
        k2:= j;
       end;
     end;{2}
    q:= q^.next;
   end;{1}
  if (k1 = 0) and (k2 = 0) then exit;
  koncovka1:= true;
  if k1 < k2 then
   begin
    vysledek:= vyber2^.mesto;
    exit;
   end;
  vysledek:= vyber1^.mesto;
 end;{ function koncovka1}

{Tato fce se pokusi opravit mesto, pokud jako zadani dostane 1 slovo, predpokladam,
 ze uzivatel bud vynechal oddelovace slov a jeste udelal chybu, nebo se jedna o samostatne
 mesto, kde uzivatel nezna koncovku, v tom pripade doplnim libovolnou, zde zadany
 seznam se zklada pouze z viceslovnych mest.}
Function koncovka2 (var seznam:uk;zadani:string;var vysledek:string): boolean;
var
 q, vyber:uk;
 Pstr: Pstrings;
 i,k: real;
 c: integer;
 zadani2,pomoc:string;
begin
 koncovka2:= false;
 if seznam = nil then exit;
 if length(zadani) < 3 then exit;
 zadani2:= nafuk(ASCII(zadani));
 q:= seznam;
 i:= 1000;
 {nejprve testuji, zda zadani je v seznamu, pokud ano, tak vyberu jako
 vysledek to mesto, kde je co nejvice ve predu, predpoklad je, ze toto slovo
 je delsi jak 2 pismena}
 while q<> nil do
  begin{1}
   Pstr:= q^.slova;
   k:= 1;
   while Pstr<> nil do
    begin{2}
     if zadani2 = Pstr^.slovo then
      begin{3}
       if i < k then
        begin{4}
         vyber:= q;
         i:= k;
         if i = 1 then
          begin{5}
           vysledek:= vyber^.mesto;
           Koncovka2:= true;
           exit;
          end;{5}
        end;{4}
       break;
      end;{3}
     k:= k+1;
     Pstr:= Pstr^.next;
    end;{2}
   q:= q^.next;
  end;{1}
 if i < 1000 then
  begin
   vysledek:= vyber^.mesto;;
   koncovka2:= true;
   exit;
  end;
 {ted budu testovat, zda to nejsou prvni pismena slov nejakeho mesta}
 q:= seznam;
 while q<> nil do
  begin
   if zadani2 = PrvniPismena(q^.slova) then
    begin
     vysledek:= q^.mesto;
     koncovka2:= true;
     exit;
    end;
   q:= q^.next;
  end;
 {ted to muze byt zkomolena smesice nekolika ruznych zkratek majicich
  znamenat nejake mesto}
 i:= 0;
 q:= seznam;
 pomoc:= Zadani2;
 while q<> nil do
  begin
   k:= 0;
   Pstr:= q^.slova;
   Zadani2:= pomoc;
   while Pstr <> nil do
    begin
     c:= Pos(Pstr^.slovo,Zadani2);
     if c>0 then
      begin
       delete(zadani2,c,length(Pstr^.slovo));
       if length(Pstr^.slovo) > 3 then
        k:= k+ 0.4;
       k:= k+1;
      end;
     Pstr:= Pstr^.next;
    end;
   if k > i then
    begin
     vyber:= q;
     i:= k;
    end;
   q:= q^.next;
  end;{1.while}
 if i <> 0 then
  begin
   koncovka2:= true;
   vysledek:= vyber^.mesto;
   exit;
  end;
 {ted otestuji, zda to nejsou prvni pismena slov mesta, ale s preklepem}
 q:= seznam;
 while q<> nil do
  begin
   if Preklep(zadani2,PrvniPismena(q^.slova)) then
    begin
     vysledek:= q^.mesto;
     koncovka2:= true;
     exit;
    end;
   q:= q^.next;
  end;
 end;{function koncovka2}

function mesto (var i: integer; var seznam: uk; zadani: string): string;
 var
  kam:string;
  pomseznam:uk;
  x:pstrings;
 begin
  mesto:= 'error';
  if seznam = nil then exit;
  if test(zadani,seznam)then
   begin
    mesto:= zadani;
    exit;
   end;
  if i < 2 then begin pridejVelke(seznam); i:= 2; end;
  if jetamvelke(seznam,zadani,kam) then
   begin
    mesto:= kam;
    exit;
   end;
  if i < 3 then begin pridejvelkeASCII(seznam);  i:= 3; end;
  if jetamvelkeASCII(seznam,zadani,kam) then
   begin
    mesto:= kam;
    exit;
   end;
  if i < 4 then begin pridejbezodd(seznam); i:= 4; end;
  if jetambezodd(seznam,zadani,kam) then
   begin
    mesto:= kam;
    exit;
   end;
  if i < 5 then begin pridejbezyz(seznam);i:= 5; end;
  if jetambezyz(seznam,zadani,kam) then
   begin
    mesto:= kam;
    exit;
   end;
  pomseznam:= nil;
  NewDleDelky(seznam,zadani,pomseznam);
  if drobnost(pomseznam,zadani,kam) then
   begin
    mesto:= kam;
    zrusseznam(pomseznam);
    exit;
   end;
  zrusseznam(pomseznam);
  if i < 6 then
   begin
    Newviceslov(seznam,kol);
    pridejslova(kol);
    i:= 6;
   end;
  if kol = nil then exit;
  rozdel(zadani,x);
  if pocetslov(x) = 0 then exit;
  if pocetslov(x) > 1 then
   if koncovka1(kol,zadani,kam) then
    begin
     mesto:= kam;
     exit;
    end;
  if pocetslov(x) = 1 then
   if koncovka2(kol,zadani,kam) then
    begin
     mesto:= kam;
     exit;
    end;
 end;{procedure mesto}

{tato fce opravi cas}
function cas (a:string): string;
var i,j,k: integer;
    t,u:string;
function ted:string;
var
    b,c: string;
    hod,min,sec,setiny:word;
 begin
  gettime(hod,min,sec,setiny);
  str(hod,b);
  str(min,c);
  if length(c) = 1 then
    c:= '0'+c;
  b:= b+':'+c;
  ted:= b;
  exit;
 end;
begin
 if length(a) = 4 then
  a:= '0' + a;
 if length(a) = 5 then
   if (a[1] in ['0'..'2']) and (a[2] in ['0'..'9']) then
             begin
              val(a[1],i,k);
              val(a[2],j,k);
              if ((i*10 + j) < 24) and (a[3] = ':') and (a[4] in ['0'..'5']) and (a[5] in ['0'..'9']) then
               begin
                cas:= a;
                exit;
               end;
             end;
  if a='now' then
   cas:= ted
             else
              cas:= 'chyba';
end; {function cas}

{tato fce opravi datum}
function datum (a: string): string;
const pole: array [1..10] of string = ('yesterday',
 'today','tomorrow','sunday','monday','tuesday','wednesday','thursday','friday','saturday');

      l: array[1..12] of integer = (31,28,31,30,31,30,31,31,30,31,30,31);

      mesice: array[1..12] of string = ('january ','february ','march ','april ','may ','june ','july ',
 'august ','september ','october ','november ','december ');
var
   b: string;
   chyba1,chyba2,chyba3,h1,h2,h3,pos1,pos2:word;
   pocitadlo,index,jnext,i: integer;
   o,p,q:string;

function small_bezmezer (k: string): string;
 var
  l: string;
  i: integer;
 begin
  l:='';
  for i:= 1 to length(k) do
   if k[i] in ['A'..'Z'] then
    l:=l + chr(ord(k[i])+32)
                       else
                        if k <> ' ' then
                         l:=l+k[i];
  small_bezmezer:= l;
 end;

function scel (rok,mes,day: word): string;
 var d,e,h: string;
 begin
  str(rok,d);
  str(mes,e);
  str(day,h);
  h:= h+ '.'+ e+ '.'+ d;
  scel:= h;
 end;

function dnescislo: word;
 var
  rok, mes, day, x: word;
 begin
  getdate(rok,mes,day,x);
  dnescislo:= x;
 end;

function dnes: string;
 var
  rok, mes, day, x: word;
 begin
  getdate(rok,mes,day,x);
  dnes:= scel(rok,mes,day);
 end;

{j cislo dne, 0 je nedele}
function den (j: integer;korekce:integer): string;
 var
  b,c: string;
  day, mes, rok, num : word;
  rozdil: integer;
 begin
  getdate(rok,mes,day, num);
  if rok mod 4 = 0 then
   l[2]:= 29;
  if j >= num then
   rozdil:= j - num
             else
              rozdil:= 7 + j - num;
  if day + rozdil + korekce > l[mes] then
   begin
    day:= rozdil + day - l[mes] + korekce;
    if mes + 1 = 13 then
      begin
       mes:= 1;
       rok:= rok+1;
      end
                     else
                      mes:= mes+1;
   end
                           else
                            day:= day + rozdil + korekce;
  den:= scel(rok,mes,day);
 end;
begin
 l[2]:=28;
 pocitadlo:= 0;
 b:= small_bezmezer(a);
 if pos(b,pole[1]) = 1 then
   begin
    datum:= den(dnescislo - 8,0);
    exit;
   end;
 for i:= 2 to 3 do
  if pos(b,pole[i]) = 1 then
   begin
    pocitadlo:= pocitadlo + 1;
    index:= i;
   end;
 if pocitadlo = 1 then
    begin
     datum:= den(dnescislo + index - 2,0);
     exit;
    end;
 jnext:=0;
 if length(b) > 5 then
  begin
   p:= copy(b,1,5);
   if p = 'next ' then
    begin
     jnext:=7;
     delete(b,1,5);
    end;
  end;
 for i:= 4 to 10 do
  if pos(b,pole[i]) = 1 then
   begin
    pocitadlo:= pocitadlo + 1;
    index:= i;
   end;
 if pocitadlo = 1 then
    begin
     datum:= den(index-4,jnext);
     exit;
    end;
 if (pocitadlo > 1) or (jnext <> 0) then
  begin
   datum:= 'chyba';
   exit;
  end;
 if (length(b) < 3) then
  begin
   datum:= 'chyba';
   exit;
  end;
  o:='';
  for i:= 1 to 12 do
   begin
    if pos(mesice[i],b) = 1 then
     begin
      str(i,p);
      delete(b,1,length(mesice[i]));
      if pos(' ',b) = 0 then
       o:=b+'.'+p+'.'
                        else
       for index:=1 to length(b) do
       begin
        if b[index] <> ' ' then
         o:= o + b[index];
        if b[index] = ' ' then
         begin
          o:=o+'.'+p+'.';
          for pocitadlo:= index + 1 to length(b) do
           o:= o + b[pocitadlo];
          break;
         end;
       end;
      b:=o;
      break;
     end;
   end;
  pos1:=0;
  pos2:=0;
  for i:= 1 to length(b) do
  if (pos1 = 0) and ((b[i] = '.') or (b[i] = '/')) then
   pos1:= i
                                                    else
                             if (b[i] = '.') or (b[i] = '/') then
                              pos2:= i;
  o:='';
  p:='';
  q:='';
  if pos1 > 0 then
  for i:= 1 to pos1 - 1 do
   o:= o + b[i];
  if pos2 > pos1 then
   begin
    for i:= (pos1 + 1) to (pos2 - 1) do
     p:= p + b[i];
    for i:= pos2 + 1 to length(b) do
     q:= q + b[i];
   end
                 else
                  for i:= pos1+1 to length(b) do
                   p:=p+ b[i];
  val(q,h3,chyba3);
  if (chyba3 = 0) and (h3 mod 4 = 0) then
   l[2]:= 29;
  if (length(o) in [1..2]) and (length(p) in [1..2]) then
   begin
    val(p,h2,chyba2);
    val(o,h1,chyba1);
   end;
  if (chyba2 = 0) and (chyba1 = 0) then
   if h2 in [1..12] then
    if (h1 <= l[h2]) and (h1 > 0) then
     begin
      if chyba3 = 0 then
       begin
        if length(q) = 4 then
         begin
          datum:= scel(h3,h2,h1);
          exit;
         end;
        if (length(q) = 3) or (length(q) = 1)  then
         begin
          datum:='chyba';
          exit;
         end;
        if h3 < 100 then
         begin
          datum:= scel(2000 + h3,h2,h1);
          exit;
         end;
       end;
      if q=''then
       begin
        getdate(h3,chyba1,chyba2,chyba3);
        if h3 mod 4 = 0 then l[2]:= 29;
        if (h1 > l[h2]) then
         begin
          writeln('chyba');
          exit;
         end;
        datum:= scel(h3,h2,h1);
        exit;
       end;
     end;
  datum:= 'chyba';
end;

begin
 i:= 1;
 kol:= nil;
 if paramcount <> 1 then
  begin
   writeln('Please, write a path of file with name of cities as a param.');
   halt;
  end;
 cesta:= paramstr(1);
 write('zadej 1.mesto: ');
 readln(zadani1);
 write('zadej 2.mesto: ');
 readln(zadani2);
 write('zadej cas ve formatu hh:mm : ');
 readln(cas1);
 write('zadej datum ve formatu 1.5.2001: ');
 readln(datum1);
 if (zadani1 = '') or (zadani2 = '') then
  begin
   writeln('error');
   halt;
  end;
 {odstranuji naproste blbosti v zadani mest}
 if (length(zadani1) <2) or (length(zadani2) < 2) then
  begin
   writeln('error');
   halt;
  end;
 pomoc:= nafuk(ascii(zadani1));
 pravda:= false;
 for j:= 1 to length(pomoc) do
  begin
   if pomoc[j] in ['A'..'Z'] then
    pravda:= true;
  end;
  if pravda = false then
   begin
    writeln('error');
    halt;
   end;
 pomoc:= nafuk(ascii(zadani2));
 pravda:= false;
 for j:= 1 to length(pomoc) do
  begin
   if pomoc[j] in ['A'..'Z'] then
    pravda:= true;
  end;
  if pravda = false then
   begin
    writeln('error');
    halt;
   end;
 {zacina zde kontrola mest}
 newseznam(seznam,cesta);
 writeln(mesto(i,seznam,zadani1));
 writeln(mesto(i,seznam,zadani2));
 writeln(cas(cas1));
 writeln(datum(datum1));
 zrusseznam(kol);
 zrusseznam(seznam);
end.
