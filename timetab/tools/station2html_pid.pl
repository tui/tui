#!/usr/bin/perl -w

# Copyright 2002, Roman Krejcik <roman.krejcik@ruk.cuni.cz>

use locale;

use constant TABLE_HEAD => "<table width=\"95%\" border=1 cellpadding=2 cellspacing=0>\n<col width=\"33%\">\n<col width=\"17%\">\n<col width=\"17%\">\n<col width=\"33%\">\n";

use constant STATION_BEFORE_CHARACTER => "&darr;";
use constant NOT_IN_LIST_CHARACTER => ".";

use constant DAY_MASK_MON_THU => 1;
use constant DAY_MASK_FRI => 2;
use constant DAY_MASK_SAT => 4;
use constant DAY_MASK_SUN => 8;

use constant MIDDLE_TAG => 5000000;

$regexp_no_time = "t.�. nejede";
$regexp_mon_thu = "jede v 1-4";
$regexp_mon_fri = "jede v 1-5";
$regexp_sat_sun = "jede v 6,7";
$regexp_fri = "jede v 5";
$regexp_sat = "jede v 6";
$regexp_sun = "jede v 7";

#    DATA STRUCTURES  
#
#  $station  - store name of station which timetab is printed 
#
#  $station_list[$current_direction]{"$station_name"}[$current_marking_char] = $interval  
#     stores list of all stations, two lists for each direction, 
#       for each line in station (marking_char) stores relative time since station
#
#  $station_tag[$current_direction]{"$station_name"}
#      tag for every station in each direction, for sorting station list
#
#  $marking_char_list[$direction]{"$key"} = $marking_char
#      marking char for each line, $key is all line station joined together
#
#  $direction{$key} = $direction 
#     line direction, $key is all line station joined together
#
#  $time_table[$current_direction]{"$time_in_station"} = string
#    time table for each direction, string contain marking chars of lines which come out of station in time 
# 
#  $is_marking_char_used[$current_direction][$marking_char]
#
#  marking_char is integer
#

foreach $arg (@ARGV) {
    if ($arg =~ /^-(.*)/) {
	if ($1 eq "h" || $1 eq "-help") {
	    print "Usage:\n\n\ttable [-h] station_name [line_name_pattern]\n\nPrint the station timetable. Read data from the standard input and\nwrite in HTML to the standard output. Input data expected in .tt\nformat. Line pattern can be arbitrary Perl regexp. If line pattern\nis set print only lines which name match it.\n\nOptions:\n\t-h print help\n\t-n print names of cons for each mark\n\n";
	    exit 0;
	} elsif ($1 eq "n") {
	    $print_con_names_flag = 1;
	} else {
	    print STDERR "Unknown option. Try -h for more information.\n"; 
	    exit 1;
	}
    } else {
	if (!defined $station) { 
	    $station = $arg; 
	} elsif (!defined($line_regexp)) {
	    $line_regexp = $arg;
	} else { 
	    print STDERR "Too many parameters. Try -h for more information.\n";
	    exit 1;
	}
    }
}

if (!defined($station)) {
    print STDERR "Too few parameters. Try -h for more information.\n";
    exit 1;
}

$max_direction = 0;
$set_after_name_flag = 0;

while (<STDIN>) {
    if (/^\#\s*(.*)/) {   #name of single connection
	$connection_name = $1;
	$r_connection = &new_connection;
	@r_connection_before = ( );
	if (defined($line_regexp)) {
	    $ignore_connection = 1 if ($connection_name !~ /$line_regexp/); 
	}
	push (@approx, 0);
	next;
    }

    if (/^\@\s*(\S*)\s+(\d+):(\d+)\s+(.*)/) {  #name of @-connections
	my ($t_hour, $t_minute, $t_other) = ($2, $3, $4);
	$connection_name = $1;
	$r_connection = &new_connection;
	@r_connection_before = ( );
	if (defined($line_regexp)) {
	    $ignore_connection = 1 if ($connection_name !~ /$line_regexp/);
	    next;
	}
	$time = $t_hour*60 + $t_minute;
	my @approx_temp = split (/\s/, $t_other);
	my ($one, $i,);
	my $time_since_first = 0; 
	push (@approx, 0);
	foreach $one (@approx_temp) {
	    my ($multi, $t) = split (/x/, $one);
	    for($i = 0; $i < $multi; $i++) {
		$time_since_first += $t;
		push (@approx, $time_since_first);
	    }
	} 
	next;
    }

    next if (defined($ignore_connection));

    if (/^([+\d:]*)\t([+\d:]*)\t([^\t]*)\t([-+\d]*)\t(.*)/) { #one line with time and station  
	if ($2 eq "") { $dep_time = $1; } else { $dep_time = $2; }
	$current_station = $5;
	my $station_note = $3;
	if ($station_note =~ /^\s*$/) {
	    $station_notes{"$current_station"} = "!" if (exists($station_notes{"$current_station"}));
	} else {
	    if (exists($station_notes{"$current_station"})) {
		$station_notes{"$current_station"} = "!" if ($station_notes{"$current_station"} ne $station_note);
	    } else {
		$station_notes{"$current_station"} = $station_note;
	    }
	} 
	$key .= $current_station;
	if (!defined($time_in_station)) {
	    push ( @r_connection_before , $current_station, &count_time($dep_time, $current_station));
	} else {
	    $r_connection[$r_connection_index][0] = $current_station;
	    $r_connection[$r_connection_index++][1] = &count_time($dep_time, $current_station);
	}
	next;	
    }
    if (/^([A-Z])(.*)/ && defined($time_in_station)) {  #note
	my $note = $2;
	if ($1 eq "R") {
	    if ($note =~ /$regexp_no_time/) { $ignore_connection = 1; }
	    elsif ($note =~ /$regexp_mon_thu/) { $day_mask = DAY_MASK_MON_THU; $current_only_friday_con = 1;}
	    elsif ($note =~ /$regexp_mon_fri/) { $day_mask = DAY_MASK_MON_THU | DAY_MASK_FRI; }
	    elsif ($note =~ /$regexp_sat_sun/) { $day_mask = DAY_MASK_SAT | DAY_MASK_SUN; }
	    elsif ($note =~ /$regexp_fri/) { $day_mask = DAY_MASK_FRI; $current_only_friday_con = 1; }
	    elsif ($note =~ /$regexp_sat/) { $day_mask = DAY_MASK_SAT; }
	    elsif ($note =~ /$regexp_sun/) { $day_mask = DAY_MASK_SUN; }
	    else {
		$unknown_note = $note;
	    }
	}
	next;
    }
}

&new_connection;   #process last connection in input 

print  "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n<html>\n<head>\n<title>Timetable for station $station.</title>\n</head>\n<body>\n";

print "Timetable for station<br><div align=\"center\"><h2>$station</h2>";


#} else {
#    print "No connection found.";
#    print STDERR "No connection found.\n";
#}

for(my $i = 0; $i < $max_direction; $i++) {
    &print_one_direction_time_tab($i);
    &print_names($i) if (defined($print_con_names_flag));
}

print "</div>\n</body>\n</html>";

sub print_names {
    my $mark;
    my $max_index = $marking_char[$_[0]];
    print "<p>\n<div align=\"left\"><table>";
    for( my $i = 0; $i < $max_index ; $i++) {
	print "<tr><td>&nbsp;&nbsp;&nbsp;";
	    if ($most_frequently[$_[0]] == $i) {
		print "&nbsp;";
	    } else {
		print &to_chr($i);
	    }
	print "</td><td>-</td><td>".$marking_char_con_name[$_[0]][$i]."</td></tr>\n";
    }
    print "</table></div><br><br>\n";
}

sub print_one_direction_time_tab {
    my $direction_number = $_[0];
    my $direction_number_plus = $_[0]+1;
    my $dir_num;
    
    if ($direction_number % 10 == 0) {
	$dir_num = $direction_number_plus."st";
    } elsif ($direction_number % 10 == 1) {
	$dir_num = $direction_number_plus."nd";
    } elsif ($direction_number % 10 == 2) {
	$dir_num = $direction_number_plus."rd";
    } else {
	$dir_num = $direction_number_plus."th";
    }
    print TABLE_HEAD;
    print "<tr bgcolor=\"#0099FF\" align=\"center\"><td colspan=2><b>$dir_num direction</b></td>\n<td colspan=2><b>";
    if (defined($only_friday_con[$direction_number])) {
	print "Monday - Thursday";
    } else {
	print "Monday - Friday";
    }
    print "</b></td></tr>";
    print "<tr valign=\"top\"><td colspan=2>\n";
    &print_station_list($direction_number);
    print "</td><td colspan=2>\n";
    &print_time_table($direction_number, 0);
    print "</td></tr>\n";
    if (defined($only_friday_con[$direction_number])) {
	print "<tr bgcolor=\"#0099FF\" align=\"center\"><td><b>Friday</b></td><td colspan=2><b>Saturday</b></td><td><b>Sunday</b></td></tr>\n";
	print "<tr  valign=\"top\"><td>";
	&print_time_table($direction_number, 1);
	print "</td>";
	print "<td colspan=2>";
	&print_time_table($direction_number, 2);
	print "</td><td>";
	&print_time_table($direction_number, 3);
    } else {
	print "<tr bgcolor=\"#0099FF\" align=\"center\"><td colspan=2><b>Saturday</b></td><td colspan=2><b>Sunday</b></td></tr>\n";
	print "<tr valign=\"top\"><td colspan=2>";
	&print_time_table($direction_number, 2);
	print "</td><td colspan=2>";
	&print_time_table($direction_number, 3);
    }
    print "</td></tr></table><br>\n";
}

sub to_chr {
    if ($_[0] < 26) {
	return chr($_[0]+ord('a'));
    } elsif ($_[0] < 52) {
	return chr($_[0]-26+ord('A'));
    } else {
	return "*";
    }
}

sub new_connection {
    my $current_direction;                                           # not ending in station   
    my $current_marking_char;
    my ($st, $i);
    if (defined($time_in_station) && !defined($ignore_connection) && $set_after_name_flag == 0) {
	#process previous connection
	if (!exists($direction{"$key"})) {
	    for($i = 0; $i < $max_direction; $i++) {
		if ($station_after_in_direction[$i] eq $station_after_name) {
		    $current_direction = $i;
		    last;
		}
	    }
	    if ($i == $max_direction) { #not found 
		$current_direction = $i; 
		$marking_char[$current_direction] = 0;
		$max_direction++;
		$free_station_tag[$current_direction][0] = MIDDLE_TAG-1;
		$free_station_tag[$current_direction][1] = MIDDLE_TAG;
		$station_after_in_direction[$i] = $station_after_name;
	    }
	    $current_marking_char = $marking_char[$current_direction];
	    $marking_char_list{"$key"} = $marking_char[$current_direction]++;
	    if (defined($print_con_names_flag)) {
		$marking_char_con_name[$current_direction][$current_marking_char] = $connection_name;
	    }
	    $marking_char_used[$current_direction][$current_marking_char] = 0;
	    
	    while (defined($_ =  pop @r_connection_before) ) {
		my $station_name = pop @r_connection_before;		
		my $interval = $_;
		$station_list[$current_direction]{"$station_name"}[$current_marking_char] = $interval;
		if (!exists($station_tag[$current_direction]{"$station_name"})) {
		    $station_tag[$current_direction]{"$station_name"} = $free_station_tag[$current_direction][0]--;
		}
	    }

	    for(my $i = 0; $i < $r_connection_index; $i++) {
		my $station_name = $r_connection[$i][0];		
		$station_list[$current_direction]{"$station_name"}[$current_marking_char] = $r_connection[$i][1]; 
		if (!exists($station_tag[$current_direction]{"$station_name"})) {
		    $station_tag[$current_direction]{"$station_name"} = $free_station_tag[$current_direction][1]++;
		}
	    }
	    $direction{"$key"} = $current_direction;
	} else {
	    $current_direction =  $direction{"$key"};
	    $current_marking_char = $marking_char_list{"$key"};
	}
	if ($current_only_friday_con) {
	    $only_friday_con[$current_direction] = 1;
	}
	$time_in_station %= 1440;
	if (defined($unknown_note)) {
	    $unknown[$current_direction][$current_marking_char] = $unknown_note;
	}
	my ($since, $current_time);
	foreach $since (@approx) {
	    $current_time = $time_in_station + $since;
	    for (my $i = 0; $i < 4; $i++) {
		$marking_char_used[$current_direction][$current_marking_char]++;
		if ($day_mask & (1 << $i)) { 
		    $is_used_day_flag[$current_direction][$i] = 1;
		    if (exists($time_table{"$current_time"})) {
			$time_table[$current_direction][$i]{"$current_time"} .= &to_chr($current_marking_char);
		    } else {
			$time_table[$current_direction][$i]{"$current_time"} = &to_chr($current_marking_char);
		    }
		}
	    }
	}
    }
    $time = 0;
    $time_in_station = undef;
    $key = "";
    $ignore_connection = undef;
    $r_connection_index = 0;
    $current_station = "";          #in main loop sets $past_station_name
    $station_after_name = "";
    $day_mask = DAY_MASK_MON_THU | DAY_MASK_FRI |  DAY_MASK_SAT | DAY_MASK_SUN;
    $current_only_friday_con = 0;
    @approx = ( );
    return [ ]; #anonymous array
 }

sub count_time {
    if ($_[0] =~ /\+(\d+)/) {
	$time += $1;
    } elsif ($_[0] =~/(\d+):(\d+)/) {
	if ($time eq "25:00") {
	    $ignore_connection = 1;
	    return 0;
	}
	$time = $1*60 + $2;
    } else {
	print STDERR "Unknown input format.\n";
	exit 1;
    }

    if ($set_after_name_flag == 1 ) {
	$station_after_name = $_[1];
	$set_after_name_flag = 0;
    }

    if ($_[1] eq $station) { # in $_[1] is name of current station
	$time_in_station = $time;
	$set_after_name_flag = 1;
    }

    if (defined($time_in_station)) {
	return $time-$time_in_station;
    } else {
	return -1;
    }
}

sub compare_stations {
    if (!defined($station_tag[$global_var_for_compare_function]{"$a"})) {
	print STDERR "A: $global_var_for_compare_function .. $a\n";
    }
    if (!defined($station_tag[$global_var_for_compare_function]{"$b"})) {
	print STDERR "B: $global_var_for_compare_function .. $b\n";
    }
    return ( $station_tag[$global_var_for_compare_function]{"$a"} <=> $station_tag[$global_var_for_compare_function]{"$b"} );
}

sub print_station_list {
    my ($key, $ref, $i, $max);
    print "<table border=0 cellpadding=10>\n<tr><td><table border=0 cellspacing=3>\n<tr align=\"center\">\n";

    $max = 0;
    $index_max = 0;
    for ($i = 0; $i < $marking_char[$_[0]]; $i++) {
	if ($marking_char_used[$_[0]][$i] > $max) {
	    $max = $marking_char_used[$_[0]][$i];
	    $index_max = $i;
	}
    }
    $most_frequently[$_[0]] = $index_max;
    print "<tr align=\"center\"><td>&nbsp;</td>";
    for ($i = 0; $i < $marking_char[$_[0]]; $i++) {
	next if ($i == $index_max);
	$key = &to_chr($i);
	print "<td><i>$key </i></td>";
    }
    print "<td>&nbsp;</td></tr>\n";

    my $r_hash = \%{$station_list[$_[0]]};

    $global_var_for_compare_function = $_[0];

    foreach $key (sort compare_stations (keys(%$r_hash))) {
	$ref = \@{$$r_hash{"$key"}};
	print "<tr align=\"center\">";

	print "<td>";
	if (defined($ref->[$index_max])) {
	    if ($ref->[$index_max] == -1) {
		print STATION_BEFORE_CHARACTER;
	    } else {
		print "0" if ($ref->[$index_max] < 10);
		print $ref->[$index_max];
	    }
	} else {
	    print NOT_IN_LIST_CHARACTER;
	}
	print "</td>\n";

	for ($i = 0; $i < $marking_char[$_[0]]; $i++) {
	    next if ($i == $index_max);
	    print "<td>";
	    if (defined($ref->[$i])) {
		if ($ref->[$i] == -1) {
		    print STATION_BEFORE_CHARACTER;
		} else {
		    print "0" if ($ref->[$i] < 10);
		    print $ref->[$i];
		}
	    } else {
		print NOT_IN_LIST_CHARACTER;
	    }
	    print "</td>\n";
	}
	if (exists($station_notes{"$key"})) {
	    print "<td>&nbsp;".$station_notes{"$key"}."&nbsp;</td>";
	} else {
	    print "<td></td>";
	}
 
	print "<td align=\"left\">";
	if ($key eq $station) {
	    print "<u>$key</u>";
	} else { print "$key"; }
	print "</td></tr>\n";
    }
    print "</table>\n";
    for($i = 0; $i < $marking_char[$_[0]]; $i++) {
	if (defined($unknown[$_[0]][$i])) {
	    if ($index_max == $i) {
		print "<br>some unmarked connections contain unknown note:";
	    } else {
		print "<br>some connections with <i>".&to_chr($i)."</i> mark contain unknown note:";
	    } 
	    print "<br>&nbsp;&nbsp;".$unknown[$_[0]][$i];
	}
    }
    print "</td><tr></table>";
}

sub print_time_table { # @_ = ( curreny_direction, day_mask )
    my @all_times = sort { int($a) <=> int($b)} (keys(%{$time_table[$_[0]][$_[1]]}));
    my $max_index = @all_times;
    my $index = 0;
    if (!defined($is_used_day_flag[$_[0]][$_[1]])) {
	print "<br><br><div align=\"center\">Don't go.</div><br><br>";
	return;
    }

    print "<table border=0 cellpadding=10><tr><td>\n<table border=0 cellspacing=2 cellpadding=1>\n";
    for(my $i = 0; $i < 24; $i++) {
	print "<tr valign=\"top\"><td><b>";
	print "0" if ($i < 10);
	print "$i &nbsp;</b></td><td>";	
	while ($index < $max_index && $all_times[$index] < 60*($i+1)) {
	    $_ = $all_times[$index];
	    my $j;
	    foreach  $j (split(//, $time_table[$_[0]][$_[1]]{"$_"})) {
		if ($_ % 60 < 10) { print " 0"; } else { print " "; }
		print $_ % 60; 
		print "<i>$j</i>" if ($j ne &to_chr($index_max) || $j eq "*");
	    }
	    $index++;
	}
	print "</td></tr>\n";    
    }
    print "</table>\n";
    print "</td><tr></table>";
}
