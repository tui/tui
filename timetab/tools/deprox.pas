{ Copyright 2001 Peter Fila, distribute under GPLv2 }

Type ZCasy = ^Zast;
     Zast = Record s : string;
                   pocet, interval : integer;
                   dalsi : Zcasy
            end;

    I_Spoj = Record Casy : Zcasy;
                    Meno : String;
                    Pozicia, Pozicia2 : integer;
             End;

Var Pid, Pid2 : File Of Char;
    C_Spoj : I_Spoj;
    I : Integer;
    Vstup, Vystup : string;
    Time, Tim : ZCasy;

Procedure Citaj(var C : char);
begin read(pid, c);
end;

Procedure Vypis(c : char);
begin if vystup <> '-' then write(pid2, c) else write(c);
end;

Procedure Nacitajs;
Var C : Char;
    S : String;

Begin Seek(pid, C_Spoj.Pozicia2);
      S := '';
      citaj(c);
      While C <> #10 Do Begin
       S := S + C;
       Citaj(C)
      End;
      C_Spoj.Meno := S;
      S := '';
      Citaj(C);
      While (c <> #9) and (c <> #10) do begin
       S := S + C;
       Citaj(C);
      end;
      New(Tim);
      Tim^.s := s;
      C_Spoj.Casy := Tim;
      Time := Tim;
      While C <> #10 do begin
       Citaj(C);
       s := '';
       While (c <> #9) and (c <> #10) do begin
        S := S + C;
        Citaj(C);
       end;
       New(Tim);
       Tim^.S := s;
       Time^.Dalsi := Tim;
       Time := Tim;
      end;
      Time^.Dalsi := nil;
      Citaj(c);
      Citaj(c);
      Citaj(c);
      C_Spoj.pozicia := Filepos(pid);
      repeat
       Citaj(c)
      until (c = '#') or eof(pid);
      C_Spoj.Pozicia2 := Filepos(pid)-1;
End;

Procedure StrToTime(c : string; var t : integer);
var i, j, k : integer;
    s : string;
begin s := '';
      i := 1;
      while c[i] <> ':' do begin
       s := s + c[i];
       i := i + 1
      end;
      val(s, j, k);
      t := j * 60;
      i := i + 1;
      s := '';
      while i <= length(c) do begin
       s := s + c[i];
       i := i + 1
      end;
      val(s, j, k);
      t := t + j;
end;

Procedure StrToCnInt(c : string; var n, t : integer);
var i, j, k : integer;
    s : string;
begin s := '';
      i := 1;
      while c[i] <> 'x' do begin
       s := s + c[i];
       i := i + 1
      end;
      val(s, j, k);
      n:= j;
      i := i + 1;
      s := '';
      while i <= length(c) do begin
       s := s + c[i];
       i := i + 1
      end;
      val(s, j, k);
      t := j
end;

Procedure TimeToStr(t : integer; var c : string);
var i : integer;
    s : string;
begin i := t div 60;
      t := t mod 60;
      str(i, s);
      c := s;
      str(t, s);
      if length(s) = 1 then s := '0' + s;
      c := c+':'+s
end;

Procedure Zapis;
var i, j, cas, odc, poc : integer;
    c : char;
    casicek : string;
begin Tim := C_Spoj.Casy^.dalsi;
      Time := C_Spoj.Casy;
      For i := 1 to length(C_Spoj.Meno) do vypis(C_Spoj.meno[i]);
      Vypis(#10);
      Vypis(#9);
      StrToTime(Time^.s, cas);
      For i := 1 to length(Time^.s) do vypis(Time^.s[i]);
      Seek(Pid, C_Spoj.Pozicia);
      While not eof(Pid) do begin
       Citaj(c);
       if c = '#' then break;
       Vypis(c);
      end;
      Dispose(Time);
      Time := Tim;
      while Time <> nil do begin
       tim := tim^.dalsi;
       StrToCnInt(Time^.s, poc, odc);
       For j := 1 to poc do begin
        cas := cas + odc;
        TimeToStr(cas, casicek);
        For i := 1 to length(C_Spoj.Meno) do vypis(C_Spoj.meno[i]);
        Vypis(#10);
        Vypis(#9);
        For i := 1 to length(casicek) do vypis(casicek[i]);
        Seek(Pid, C_Spoj.Pozicia);
        While not eof(Pid) do begin
         Citaj(c);
         if c = '#' then break;
         Vypis(c);
        end;
       end;
       Dispose(Time);
       Time := Tim;
      end;
end;

Begin If ParamStr(1) <> '' then Vstup := ParamStr(1) else begin
       writeln('Pouzitie : Dexomp.exe <vstup (musi existovat)> <vystup (- standardny vystup)>');
       exit
      end;
      If ParamStr(2) <> '' then Vystup := ParamStr(2) else begin
       writeln('Pouzitie : Dexomp.exe <vstup (musi existovat)> <vystup (- standardny vystup)>');
       exit
      end;
      If Vystup <> '-' then begin
       assign(pid2, vystup);
       rewrite(pid2);
      end;
      Assign(Pid, vstup);
      Reset(Pid);
      C_Spoj.Pozicia2 := 0;
      While Not Eof(Pid) Do Begin
       Nacitajs;
       Zapis;
      end;
      If Vystup <> '-' then close(pid2)
End.