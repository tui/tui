{copyright 2002 Jiri Kulhanek - Kulhanekj@email.cz
 distributed under GPL v.2

zdroj: http://ttselect.kvalitne.cz
}

Program ttselect;

type znaky=set of char;
const spatnyznak: znaky=['B','R','!','G',';'];

var f,f1					      : text;
   odkud,kam,odjezd,prijezd,vstup,prostredek,prepinac : string;
   x						      : char;
odstavec					      : array[1..200] of string;
    prost1,odkud1,kam1				      : array[1..10] of string[20];
    plus					      : boolean;

function nactiradek:string;
var c:char;
    st:string;
begin
st:='';
c:=' ';
while c<>#10 do if not eof(f) then
      	begin
      	read(f,c);
      	if eof(f) then break;
      	st:=st+c;
      	end;
nactiradek:=st;
end;

function jecas(cas:string):boolean;
begin
jecas:=false;
if (length(cas)=5) then
	if (cas[1]>='0') and (cas[1]<='2') and (cas[2]>='0') and (cas[2]<='9')
	and (cas[3]=':') and (cas[4]>='0') and (cas[4]<='5') and (cas[5]>='0')
	and (cas[5]<='9') then jecas:=true;
end;

procedure najdiprostr;
var i,j:integer;
    s:string;
begin
reset(f);
j:=0;
   while not eof(f) do
 	begin
        s:=nactiradek;
        if s[1]='#' then
	   writeln(s);
	end;   
end;

function hodin(c:string):word;
begin
hodin:=(ord(c[1])-48)*10+(ord(c[2])-48);
end;

function minut(c:string):word;
begin
minut:=(ord(c[4])-48)*10+(ord(c[5])-48);
end;

function vyskyt(radek,slovo:string):boolean;
var i,j:integer;
begin
vyskyt:=false;
for i:=1 to length(radek) do
        if (upcase(radek[i])=upcase(slovo[1])) and ((radek[i-1]=#9) or (radek[1]='#')) then
	begin
        j:=1;
	while (upcase(radek[i+j])=upcase(slovo[j+1])) and (i+j<=length(radek)) do
		begin
        	inc(j);
        	if j=length(slovo) then
                	begin
                        vyskyt:=true;
                        exit;
                        end;
                end;
        end;
end;

procedure zapisodstavec;
var i:integer;
begin
i:=1;
while (odstavec[i]<>'') and (i<=200) do
	begin
	write(f1,odstavec[i]);
        inc(i);
        end;
end;

procedure vyhledatstanici(stanice:string;tam:boolean);
var i,j,k:integer;
    s:string;
    je:boolean;
begin
i:=0;
k:=0;
je:=false;
for j:=1 to 200 do odstavec[j]:='';
while not eof(f) do
	begin
        s:=nactiradek;
        inc(i);
        if (s[1] in spatnyznak) or (s[1]='#') then{(s[1]='#') or (s[1]='B') or (s[1]='R') or
	(s[1]='!') or (s[1]='G') or (s[1]=';')}
             	if k=i-1 then je:=false; {zarucuje, ze vyhledavana stanice neni posledni}
        if (s[1]='#') and (i>1) then
		begin
		i:=1;
                k:=0;
                if je then zapisodstavec;
                je:=false;
                for j:=1 to 200 do odstavec[j]:='';
                end;
        odstavec[i]:=s;
	if vyskyt(odstavec[i],stanice) and (tam) and (i<>2) and (not (odstavec[i][1] in spatnyznak))then je:=true;
        if vyskyt(odstavec[i],stanice) and (not tam) and (not (odstavec[i][1] in spatnyznak)) then
		begin
		je:=true;
                if k=0 then k:=i;
                end;
        end;
if je then zapisodstavec;
end;

procedure druhastanice(od,k:string);
var i,j:integer;
    s:string;
    je,je1:boolean;
begin
i:=0;
je:=false;
je1:=false;
for j:=1 to 200 do odstavec[j]:='';
while not eof(f) do
	begin
        s:=nactiradek;
        inc(i);
        if (s[1]='#') and (i>1) then
		begin
		i:=1;
                if je1 then zapisodstavec;
                je:=false;
                je1:=false;
                for j:=1 to 200 do odstavec[j]:='';
                end;
        odstavec[i]:=s;
	if (vyskyt(odstavec[i],od)) and (not (odstavec[i][1] in spatnyznak)) then je:=true;
        if (vyskyt(odstavec[i],k)) and (je) and (not (odstavec[i][1] in spatnyznak)) then je1:=true;
        end;
        if je1 then zapisodstavec;
end;

procedure vyhledatcas(stanice,cas:string;vetsi:boolean);
var i,j,n:integer;
    s,cas1,cas2:string;
    je:boolean;
begin
i:=0;
je:=false;
cas1:='';
for j:=1 to 200 do odstavec[j]:='';
while not eof(f) do
	begin
        s:=nactiradek;
        inc(i);
        if (s[1]='#') and (i>1) then
		begin
		i:=1;
                if je then zapisodstavec;
                je:=false;
                cas1:='';
                for j:=1 to 200 do odstavec[j]:='';
                end;
        odstavec[i]:=s;
        if vyskyt(odstavec[i],stanice) then
        	begin
                for j:=2 to length(odstavec[i])-5 do
                        begin
			if (odstavec[i,j]>='0') and (odstavec[i,j]<='9') and ((odstavec[i,j-1]<'0') or (odstavec[i,j-1]>'9')) then
                        	begin
                                cas2:='';
                        	for n:=j to j+4 do
					if ((odstavec[i,n]>='0') and (odstavec[i,n]<='9')) or (odstavec[i,n]=':') then cas2:=cas2+odstavec[i,n];
                                if length(cas2)=4 then cas2:='0'+cas2;
                                if jecas(cas2) then cas1:=cas2;
                                if not vetsi then break;
                                end;
                        if (odstavec[i,j]='+') and (odstavec[i,j+1]>='0') and (odstavec[i,j+1]<='9') then
                           	begin
                                plus:=true;
                                exit;
                                end;
                        end;
                if jecas(cas1) then
                if (vetsi) then
                	if (hodin(cas1)>hodin(cas)) or ((hodin(cas1)=hodin(cas)) and (minut(cas1)>=minut(cas))) then je:=true;
        	if not (vetsi) then
			if (hodin(cas1)<hodin(cas)) or ((hodin(cas1)=hodin(cas)) and (minut(cas1)<=minut(cas))) then je:=true;
                end;
        end;
        if je then zapisodstavec;
end;

procedure prostr;
var stanic1,y,z:byte;
begin
for z:=1 to 10 do prost1[z]:='';
stanic1:=1;
for z:=1 to length(prostredek) do
        if prostredek[z]=',' then inc(stanic1)
        else prost1[stanic1]:=prost1[stanic1]+prostredek[z];
reset(f);
assign(f1,'pomprost.tmp');
rewrite(f1);
for z:=1 to stanic1 do
	begin
	append(f1);
        reset(f);
	vyhledatstanici('# '+prost1[z],false);
        end;
close(f);
erase(f);
close(f1);
rename(f1,vstup);
end;

procedure proc_odkud;
var stanic1,y,z:byte;
begin
for z:=1 to 10 do odkud1[z]:='';
stanic1:=1;
for z:=1 to length(odkud) do
        if odkud[z]=',' then inc(stanic1)
        else odkud1[stanic1]:=odkud1[stanic1]+odkud[z];
reset(f);
assign(f1,'pomodkud.tmp');
rewrite(f1);
for z:=1 to stanic1 do
begin
	append(f1);
	reset(f);
	vyhledatstanici(odkud1[z],false);
	end;
close(f);
close(f1);
erase(f);
rename(f1,vstup);
end;

procedure proc_odkdy(jak:boolean);
var stanic1,y,z:byte;
begin
reset(f);
assign(f1,'pomodkdy.tmp');
rewrite(f1);
for z:=1 to 10 do odkud1[z]:='';
stanic1:=1;
for z:=1 to length(odkud) do
        if odkud[z]=',' then inc(stanic1)
        else odkud1[stanic1]:=odkud1[stanic1]+odkud[z];
for z:=1 to stanic1 do
	begin
	append(f1);
        reset(f);
	vyhledatcas(odkud1[z],odjezd,jak);
        if plus then break;
        end;
close(f);
close(f1);
if not plus then
	begin
	erase(f);
	rename(f1,vstup);
        end
else erase(f1);
end;

procedure proc_kam;
var stanic1,stanic2,y,z:byte;
begin
for z:=1 to 10 do odkud1[z]:='';
stanic1:=1;
for z:=1 to length(odkud) do
        if odkud[z]=',' then inc(stanic1)
        else odkud1[stanic1]:=odkud1[stanic1]+odkud[z];

for z:=1 to 10 do kam1[z]:='';
stanic2:=1;
for z:=1 to length(kam) do
	if kam[z]=',' then inc(stanic2)
	else kam1[stanic2]:=kam1[stanic2]+kam[z];
reset(f);
assign(f1,'pomkam.tmp');
rewrite(f1);
for y:=1 to stanic1 do
       	for z:=1 to stanic2 do
               	begin
                append(f1);
                reset(f);
                druhastanice(odkud1[y],kam1[y]);
                end;
close(f);
erase(f);
close(f1);
rename(f1,vstup);
end;

procedure vypis(nazev:string);
var s:string;
begin
reset(f);
if eof(f) then writeln('Neexistuje spoj vyhovujici vasim pozadavkum.')
else
	begin
        if plus then
		writeln('Cas nebyl ve spravnem tvaru (hh:mm) a pri vyhledavani na nej nebyl bran zretel.');
	while not eof(f) do write(nactiradek);
	end;
close(f);
erase(f);
end;

function prazdny:boolean;
begin
reset(f);
prazdny:=false;
if eof(f) then
      	begin
        writeln('Zadna polozka nevyhovuje vasim parametrum.');
        close(f);
        erase(f);
        prazdny:=true;
	end;
end;

begin  {of main}

Plus:=false;
prepinac:=paramstr(1);
if prepinac<>'-?' then
	begin
	assign(f,'vstup.tt');
	rewrite(f);
	while not eof(input) do begin
		read(x);
                write(f,x);
		end;
	vstup:='vstup.tt';
        reset(f);
        if eof(f) then writeln('Program nedostal ze vstupu zadna data.');
        end;

case prepinac[2] of
'?':	begin
        writeln('Napoveda:');
	writeln('-L --zobrazi vsechny pouzitelne prosredky');
	writeln('-c prostredek --najde vsechny spoje daneho prostredku');
	writeln('-a stanice [time1] [time2] [prostredek] --najde spoje jedouci');
	writeln('	stanici v case <time1,time2>');
	writeln('-b stanice1 stanice2 [time1] [time2] [prostredek] --spoje ze stanice1');
	writeln('	v case <time1,time2> do stanice2');
        writeln('-? --napoveda');
        writeln('Poznamky:');
        writeln('	"-" znamena libovolne');
        writeln('	Param. v hranatych zavorkach [] je mozno nahradit "-"');
        writeln('       Posledni parametry, ktere jsou v [] lze uplne ignorovat.');
        writeln('	Staci uvest pocatecni usek v nazvu hledane stanice.');
        writeln('	Lze uvest vic stanic ci prostredku - oddelene carkou.');
        end;
'L':	begin
	najdiprostr;
        close(f);
        erase(f);
        exit;
        end;
'c':	begin
        if (paramstr(2)<>'') and (paramstr(2)<>'-') then prostredek:=paramstr(2)
        else
		begin
		writeln('Je treba zadat nazev dopravniho prostredku.');
                exit;
                end;
        prostr;
        vypis(vstup);
        writeln('');
        exit;
        end;
'a':	begin
	if (paramstr(2)<>'') and (paramstr(2)<>'-') then odkud:=paramstr(2)
        else
		begin
		writeln('Je treba zadat nazev stanice.');
                exit;
                end;
        proc_odkud;
        if prazdny then exit;
        if (paramstr(3)<>'-') and (paramstr(3)<>'') then
	begin
	   	odjezd:=paramstr(3);
		if (not jecas(odjezd)) then
			begin
        		writeln('Spatne zadana dolni hranice casu (neni ve tveru hh:mm ani -).');
        		exit;
		        end;
                proc_odkdy(true);
                if prazdny then exit;
                end;
	if (paramstr(4)<>'-') and (paramstr(4)<>'') then
		begin
		odjezd:=paramstr(4);
		if (not jecas(odjezd)) then
			begin
        		writeln('Spatne zadana horni hranice casu (neni ve tvaru hh:mm ani -).');
        		exit;
		        end;
                proc_odkdy(false);
                if prazdny then exit;
                end;
        if (paramstr(5)<>'') and (paramstr(5)<>'-') then
		begin
		prostredek:=paramstr(5);
                prostr;
                if prazdny then exit;
                end;
        vypis(vstup);
        writeln('');
        end;
'b':	begin
	if (paramstr(2)<>'') and (paramstr(2)<>'-') then odkud:=paramstr(2)
        else
		begin
		writeln('Je treba zadat nazev stanice1.');
                exit;
                end;
        proc_odkud;
        if prazdny then exit;
	if (paramstr(3)<>'') and (paramstr(3)<>'-') then kam:=paramstr(3)
        else
		begin
		writeln('Je treba zadat nazev stanice2.');
                exit;
                end;
        proc_kam;
        if prazdny then exit;
        if (paramstr(4)<>'-') and (paramstr(4)<>'') then
		begin
		odjezd:=paramstr(4);
		if (not jecas(odjezd)) then
			begin
        		writeln('Spatne zadana dolni hranice casu (neni ve tveru hh:mm ani -).');
        		exit;
		        end;
                proc_odkdy(true);
                if prazdny then exit;
                end;
	if (paramstr(5)<>'-') and (paramstr(5)<>'') then
		begin
		odjezd:=paramstr(5);
		if (not jecas(odjezd)) then
			begin
        		writeln('Spatne zadana horni hranice casu (neni ve tvaru hh:mm ani -).');
        		exit;
		        end;
                proc_odkdy(false);
                if prazdny then exit;
                end;
        if (paramstr(6)<>'') and (paramstr(6)<>'-') then
		begin
		prostredek:=paramstr(6);
                prostr;
                if prazdny then exit;
                end;
        vypis(vstup);
        writeln('');
        end;
else if prepinac='' then writeln('Nezadal jste zadny prepinac.')
     else writeln('Zvoleny prepinac "',prepinac,'" neexistuje.');
end;
end.