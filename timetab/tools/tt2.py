#!/usr/bin/python
# Copyright 2008 Petr Machek, GPL
# Copyright 2016 Pavel Machek, GPL

import sys
import re

class TTReader:
    def __init__(m):
        pass

    def new_connection(m, new):
        pass

    def new_stop(m, name, t1, t2):
        pass

    def one_time(m, s):
        return s
    
    def run(m):
        last_time = ''
        m.cur_connection = None

        for l in sys.stdin.readlines():
            m.cur_line = l
            l = l[:-1]
            if re.match("^# ", l):
                # Spojeni
                l = re.sub("^# *", "", l)
                l = re.sub("_.*", "", l)
                m.new_connection(l)
                m.cur_connection = l
                continue
            if re.match("^[ 	0123456789+]", l):
                # Odjezd
		chunks = l.split('\t')
                stop = re.sub("^.*\t.*\t.*\t.*\t", "", l)
		t1 = m.one_time(chunks[0])
		t2 = m.one_time(chunks[1])
                m.new_stop(stop, t1, t2)

                

class TT2stations(TTReader):
    def __init__(m):
        m.stops = {}

    def new_stop(m, stop, t1, t2):
        conn = m.cur_connection
        if not stop in m.stops:
            m.stops[stop] = []
        if not conn in m.stops[stop]:
            m.stops[stop] += [ conn ]

    def dump_stops(m):
        for stop in m.stops:
            print(stop, m.stops[stop])


