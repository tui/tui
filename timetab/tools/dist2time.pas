{
	Dist2time -- when everythink else fails, you can walk

	Copyright 2001 Stefan Bigos <bigos0am@artax.karlin.mff.cuni.cz>
	Copyright 2000,2001 Marian Cerny
	Copyright 2001 Pavel Machek <pavel@ucw.cz>
	Distribute under terms of GNU GPL version 2 or later.

}
{$B-}
const
	RYCHLOST_CHODCA = 5;
	MAXIMALNA_VZDIALENOST = 2000000;	      { max distance between two towns }
	OBESMERY = true;
	TAB = #9; {delimiter v databaze}
	NUM_OF_DECIDE_CHARS = 4; { >=1 !!!! } {hlbka stromu v Pdecide_char na reprezentaciu mien miest v pamati}
	TOWN_MAX_LENGTH = 130; {train.tt has 37}
	TOLERANCIA = 0;

{--- towns -------------------------------------------------------------------}

const
	char_begin = 0;
	char_end = 29;
		{ 0 ukazuje na Pstring - ak length(town) < NUM_OF_DECIDE_CHARS }
		{ 29 pointer na zoznam Pprvkov }
		{ 1..28 su:
		  ak to je na urovni 1..NUM_OF_DECIDE_CHARS-1, tak ukazatele
		    na synovske Tdecide_char
		  ak to je na poslednej urovni, tak ukazatele na synovske 
		    Pstringy}

type
	Pdecide_char = ^Tdecide_char;
	Tdecide_char = array[char_begin..char_end] of pointer;
	{pole pointerov na dalsie Tdecide_char. NUM_OF_DECIDE_CHARS-ta 
	 Tdecide_char ukazuje na Tstring, kde je zoznam miest zacinajucich na
	 prvych NUM_OF_DECIDE_CHARS znakov.}

	Pdest = ^Tdest;
	Pstring = ^Tstring;
	Pprvok = ^Tprvok;
	Tprvok = record
		meno: string[TOWN_MAX_LENGTH];
		otec: Pprvok;
		next: Pprvok;
		moj_pstring: Pstring;
		dijkstra_nekonecno: boolean;
		dijkstra_definitivny: boolean;
		dijkstra_hodnota: real;
		dijkstra_next, dijkstra_prev: Pprvok;
	end;
	
	Tstring = record 
		meno_prvok: Pprvok;
		dest: Pdest;
		next: Pstring;
	end;
	
	Tdest = record 
		meno_prvok: Pprvok;
		km: real;
		second_zoznam_next: Pdest;
		zoznam_next: Pdest;
		otcovska_struktura: Pstring;
		next: Pdest;		
		pridana: boolean;
		obratena_hrana: Pdest;
	end;

var
	first_char: Pdecide_char;
	zac_spojaku: Pdest;
	second_zac_spojaku: Pdest;

{********** INITIALIZATION **************************************************}

procedure clear_Tdecide_char(var T: Tdecide_char);
var
	i: integer;
begin
	for i:= char_begin to char_end do
		T[i]:= nil;
end;

procedure initialize_town_structure;
begin
	new(first_char);
	clear_Tdecide_char(first_char^);
end;

{************* CREATING TOWN LIST *******************************************}

function my_ord(ch: char): integer;
begin
	my_ord:= (Ord(ch) mod 28) + 1;
end;

procedure zarad_do_spojaku(koho, od: Pdest);
{v spojaku vyhlada "koho" a zacne od "od". ked ho najde, tak ho do spojaku prida najst znamena utriedenost s rastucou hodnotou km.       mozme predpokladat, ze "od"<>nil.}
var
	pom: Pdest;
begin
	if koho^.km < od^.km then begin
		{tento pripad znamena, ze to bolo volane z hodnotou
		 od=zac_spojaku; (inak by sa to nezavolalo)}
		koho^.zoznam_next:= zac_spojaku;
		zac_spojaku:= koho;
	end else begin {pridaj niekde za od}
		pom:= od;
		while (od^.km < koho^.km) and (od^.zoznam_next <> nil) do begin
			pom:= od;
			od:= od^.zoznam_next;
		end;
		if (od^.zoznam_next=nil) and (od^.km <= koho^.km) then begin
			od^.zoznam_next:= koho;
			koho^.zoznam_next:= nil;
		end else begin
			koho^.zoznam_next:= pom^.zoznam_next;
			pom^.zoznam_next:= koho;
		end;
	end;
end;

procedure second_zarad_do_spojaku(koho, od: Pdest);
{zmaz}
var
	pom: Pdest;
begin
	if koho^.km < od^.km then begin
		{tento pripad znamena, ze to bolo volane z hodnotou
		 od=zac_spojaku; (inak by sa to nezavolalo)}
		koho^.second_zoznam_next:= second_zac_spojaku;
		second_zac_spojaku:= koho;
	end else begin {pridaj niekde za od}
		pom:= od;
		while (od^.km < koho^.km) and (od^.second_zoznam_next <> nil) do begin
			pom:= od;
			od:= od^.second_zoznam_next;
		end;
		if (od^.second_zoznam_next=nil) and (od^.km <= koho^.km) then begin
			od^.second_zoznam_next:= koho;
			koho^.second_zoznam_next:= nil;
		end else begin
			koho^.second_zoznam_next:= pom^.second_zoznam_next;
			pom^.second_zoznam_next:= koho;
		end;
	end;
end;

procedure pridaj_hranu(from_town, to_town: string; km: real);
{prida spojenie do zoznamu spojeni (miest)} 
var
	town, second_town: string;
	Ptown, Psecond_town: Pprvok;
	T: Pprvok;
	shorter: integer;
	my_decide: Pdecide_char;
	P, Q: Pdecide_char;
	second_P: Pdecide_char;
	R, S: Pstring;
	PP,QQ: Pdest;
	doteraz_najvacsie_mensie: Pdest;
	i: integer;
	min: integer;
	last_decide_char: integer;
	second_last_decide_char: integer;
	mam: boolean;
	odkaz_na_prvu: Pdest;
	label pridaj_este_druhu, konec;
begin
{naplni town lexikograficky mensim a second_town lexikograficky vetsim z from_town a to_town}
	if from_town = to_town then begin
		writeln(stderr, 'chyba: za sebou su dve zhodne mesta: ',to_town);
		Exit;
	end;
	shorter:= length(from_town);
	if length(to_town) < shorter then shorter:= length(to_town);
	town:='';
	i:=1;
	while (i<= shorter) and (from_town[i]=to_town[i]) do
		inc(i);
	if i>shorter then {ak je nazov kratsi=>lexikograficky mensi}
		if length(from_town) = shorter then begin
			town:= from_town;
			second_town:= to_town;
		end else begin {length(to_town)=shorter}
			town:=to_town;
			second_town:=from_town;
		end
	else {lexikograficky mensi ide do 'town'}
		if from_town[i] < to_town[i] then begin
			town:= from_town;
			second_town:= to_town;
		end else begin
			town:= to_town;
			second_town:= from_town;
		end;

{PRIDANIE DO STROMU SECOND_TOWN }
	{do min vlozi #urovni, ktore preskoci } 
	if length(second_town) < NUM_OF_DECIDE_CHARS then begin
		second_last_decide_char:=  my_ord(second_town[NUM_OF_DECIDE_CHARS]);
		min:= length(second_town)-1;
	end else begin
		second_last_decide_char:=  my_ord(second_town[NUM_OF_DECIDE_CHARS]);
		min:= NUM_OF_DECIDE_CHARS - 1;
	end;
	{tento cyklus sa ponori, ako si to v predch. kroku vypocital a vrati 
	ukazatel na pole na potrebny Tdecide_char, do ktoreho sa na index 29
	vlozi second_town}
	P:= first_char;
	for i:= 1 to min do 
		if P^[my_ord(second_town[i])] = nil then begin
			new(Q);
			P^[my_ord(second_town[i])]:= Q;
			clear_Tdecide_char(Q^);
			P:= Q;
		end else
			P:= P^[my_ord(second_town[i])];
	{vytvorenie Tprvku zo second_town}
	T:= P^[29];
	mam:= false;
	while (T<>nil) and (not mam) do begin
		if T^.meno = second_town then begin
			Psecond_town:= T;
			mam:=true;
		end;
		T:= T^.next;
	end;
	if not mam then begin
		new(Psecond_town);
		Psecond_town^.otec:= nil;
		Psecond_town^.meno:= second_town;
		Psecond_town^.next:=P^[29];
		P^[29]:= Psecond_town;
		Psecond_town^.dijkstra_definitivny:= false;
		Psecond_town^.dijkstra_nekonecno:= true;
		Psecond_town^.moj_pstring:= nil;
	end;
	second_P:= P;



{PRIDANIE DO STROMU TOWN }
	{do min vlozi #urovni, ktore preskoci a do last_decide_char vlozi 
	index v poli typu Tdecide_char}
	if length(town) < NUM_OF_DECIDE_CHARS then begin
		last_decide_char:= my_ord(town[NUM_OF_DECIDE_CHARS]);
		last_decide_char:= 0;
	end else begin
		min:= NUM_OF_DECIDE_CHARS - 1;
		last_decide_char:= my_ord(town[NUM_OF_DECIDE_CHARS]);
	end;
	{tento cyklus sa ponori, ako si to v predch. kroku vypocital a vrati 
	ukazatel na pole (v prem. P), do ktoreho sa vlozi na index 
	last_decide_char}
	P:= first_char;
	for i:= 1 to min do begin
		my_decide:= P;
		if P^[my_ord(town[i])] = nil then begin
			new(Q);
			P^[my_ord(town[i])]:= Q;
			clear_Tdecide_char(Q^);
			P:= Q;
		end else
			P:= P^[my_ord(town[i])];
	end;
	{vytvorenie Tprvku z town}
	if length(town) >= NUM_OF_DECIDE_CHARS then 
		my_decide:= my_decide^[my_ord(town[min])];
	T:= my_decide^[29];
	mam:= false;
	while (T<>nil) and (not mam) do begin
		if T^.meno = town then begin
			Ptown:= T;
			mam:=true;
		end;
		T:= T^.next;
	end;
	if not mam then begin
		new(Ptown);
		Ptown^.otec:= nil;
		Ptown^.meno:= town;
		Ptown^.next:=my_decide^[29];
		my_decide^[29]:= Ptown;
		Ptown^.dijkstra_definitivny:= false;
		Ptown^.dijkstra_nekonecno:= true;
		Ptown^.moj_pstring:= nil;
	end;

{VLOZIM DO STROMU HRANU (TOWN,SECOND_TOWN) }	
{writeln('Pridavam ts: ',town,' ',second_town);}
	if P^[last_decide_char] = nil then begin
		new(R);
		R^.meno_prvok:= Ptown;
		Ptown^.moj_pstring:=R;
		new(QQ);
		R^.dest:= QQ;
		R^.next:= nil;
		QQ^.meno_prvok:= Psecond_town;
		QQ^.km:= km;
		QQ^.next:= nil;
		QQ^.otcovska_struktura:= R;
		QQ^.pridana:= false;
		if zac_spojaku = nil then begin
			zac_spojaku:= QQ;
			QQ^.zoznam_next:= nil;
		end else
			zarad_do_spojaku(QQ, zac_spojaku);
		P^[last_decide_char]:= R;
	end else begin {P^[last_decide_char] <> nil }
		R:= P^[last_decide_char];
		while R <> nil do
			if R^.meno_prvok = Ptown then begin
				doteraz_najvacsie_mensie:= nil;
				PP:= R^.dest;
				while PP <> nil do 
					if PP^.meno_prvok=Psecond_town then begin				
						if km<PP^.km then PP^.km:= km;
						goto pridaj_este_druhu;
					end else begin
						if (PP^.km<=km) then begin
							if (doteraz_najvacsie_mensie = nil) then doteraz_najvacsie_mensie:= PP;
							if (PP^.km>doteraz_najvacsie_mensie^.km) then doteraz_najvacsie_mensie:= PP;
						end;
						PP:= PP^.next;
					end;
				new(QQ);
				QQ^.meno_prvok:= Psecond_town;
				QQ^.km:= km;
				QQ^.next:= R^.dest;
				R^.dest:= QQ;
				QQ^.otcovska_struktura:= R;
				QQ^.pridana:= false;
				if doteraz_najvacsie_mensie = nil then 
					zarad_do_spojaku(QQ, zac_spojaku)
				else
					zarad_do_spojaku(QQ, zac_spojaku{doteraz_najvacsie_mensie});
				goto pridaj_este_druhu;
			end else
				R:= R^.next;

		new(S);
		S^.meno_prvok:= Ptown;
		Ptown^.moj_pstring:=S;
		new(QQ);
		S^.dest:= QQ;
		QQ^.meno_prvok:= Psecond_town;
		QQ^.km:= km;
		QQ^.next:= nil;
		QQ^.otcovska_struktura:= S;
		QQ^.pridana:= false;
		zarad_do_spojaku(QQ, zac_spojaku);
		R:= P^[last_decide_char];
		S^.next:= R^.next; {mesto ide za prvy v zozname}
		R^.next:= S;
	end;

pridaj_este_druhu:
	odkaz_na_prvu:= QQ;
{VLOZIM DO STROMU HRANU (SECOND_TOWN, TOWN) }	
	if second_P^[second_last_decide_char] = nil then begin
		new(R);
		R^.meno_prvok:= Psecond_town;
		Psecond_town^.moj_pstring:=R;
		new(QQ);
		R^.dest:= QQ;
		R^.next:= nil;
		QQ^.meno_prvok:= Ptown;
		QQ^.km:= km;
		QQ^.next:= nil;
		QQ^.otcovska_struktura:= R;
		QQ^.pridana:= false;
		if second_zac_spojaku = nil then begin
			second_zac_spojaku:= QQ;
			QQ^.zoznam_next:= nil;
		end else
			second_zarad_do_spojaku(QQ, second_zac_spojaku);
		second_P^[second_last_decide_char]:= R;
	end else begin {second_P^[second_last_decide_char] <> nil }
		R:= second_P^[second_last_decide_char];
		while R <> nil do
			if R^.meno_prvok = Psecond_town then begin
				doteraz_najvacsie_mensie:= nil;
				PP:= R^.dest;
				while PP <> nil do 
					if PP^.meno_prvok=Ptown then begin				
						if km<PP^.km then PP^.km:= km;
						goto konec;
					end else begin
						if (PP^.km<=km) then begin
							if (doteraz_najvacsie_mensie = nil) then doteraz_najvacsie_mensie:= PP;
							if (PP^.km>doteraz_najvacsie_mensie^.km) then doteraz_najvacsie_mensie:= PP;
						end;
						PP:= PP^.next;
					end;
				new(QQ);
				QQ^.meno_prvok:= Ptown;
				QQ^.km:= km;
				QQ^.next:= R^.dest;
				R^.dest:= QQ;
				QQ^.otcovska_struktura:= R;
				QQ^.pridana:= false;
				if doteraz_najvacsie_mensie = nil then 
					second_zarad_do_spojaku(QQ, second_zac_spojaku)
				else
					second_zarad_do_spojaku(QQ, second_zac_spojaku{doteraz_najvacsie_mensie});
				goto konec;
			end else
				R:= R^.next;

		new(S);
		S^.meno_prvok:= Psecond_town;
		Psecond_town^.moj_pstring:=S;
		new(QQ);
		S^.dest:= QQ;
		QQ^.meno_prvok:= Ptown;
		QQ^.km:= km;
		QQ^.next:= nil;
		QQ^.otcovska_struktura:= S;
		QQ^.pridana:= false;
		zarad_do_spojaku(QQ, second_zac_spojaku);
		R:= second_P^[second_last_decide_char];
		S^.next:= R^.next; {mesto ide za prvy v zozname}
		R^.next:= S;
	end;
konec:
	QQ^.obratena_hrana:= odkaz_na_prvu;
	odkaz_na_prvu^.obratena_hrana:= QQ;
end;

procedure create_tree_and_spojak;
{prechadza databazu a vlkada spojenia do pamate}
var
	line: string;
	number_string: string;
	km: real;
	code: integer;
	from_town, to_town: string;
	i,j: integer;
	new_block: boolean;
begin
	while not eof do begin
		readln(line);
		if (line <> '') and (line[1] = '#') then new_block:= true;
		if (line <> '') and (line[1] in ['0'..'9','+',TAB]) then begin
			to_town:= '';
			i:= length(line);
			while (line[i] <> TAB) and (i>0) do begin
				to_town:= line[i] + to_town;
				dec(i);
			end;
			for j:= 1 to ( NUM_OF_DECIDE_CHARS - length(to_town)) do
				to_town:=to_town + ' ';
			dec(i);
			number_string:='';
			while (line[i] <> TAB) and (i>0) do begin
				number_string:=line[i] + number_string;
				dec(i);
			end;
			Val(number_string, km, code);
			if code <> 0 then begin
				writeln(stderr, 'chyba v procedure create_tree_and_spojak pri prevadzani kilometrov na cislo!');
				Continue;
			end;
			if new_block then begin
{!!!debug}			if km <> 0 then begin 
					writeln(stderr, 'chyba: prva polozka ma km<>0: ', to_town);
					Exit;
{!!!debug}			end;
				from_town:= to_town;
				new_block:= false;
			end else begin
				if km < MAXIMALNA_VZDIALENOST then pridaj_hranu(from_town, to_town, km);
				from_town:= to_town;
			end;
		end;
	end;
end;


{************* RELEASING *****************************************************}

var celkovy_pocet_miest: integer;
procedure Uvolni_zoznam_nazvov_miest( P: Pprvok);
var
	pom: Pprvok;
begin
	while P <> nil do begin
		pom:=P^.next;
{debug}		inc(celkovy_pocet_miest);
		Dispose(P);
		P:=pom;
	end;
end;

procedure Uvolni_Desty ( P: Pdest);
var
	pom: Pdest;
begin
	while P<>nil do begin
		pom:=P^.next;
		Dispose(P);
		P:= pom;
	end;		
end;

procedure Uvolni_zoznam_spojeni( P: Pstring );
var
	pom: Pstring;
begin
	while P <> nil do begin
		pom:=P^.next;
		Uvolni_Desty(P^.dest);
		Dispose(P);
		P:= pom;
	end;
end;

procedure Uvolni_Pdecide_char( hlbka: integer; P: Pdecide_char);
var
	i: integer;
begin
	if hlbka = 1 then begin
		if P^[0] <> nil then begin
			Uvolni_zoznam_spojeni(Pstring(P^[0])); 
			writeln(stderr, 'chyba: nasiel som neprazdnu nulu v liste!');
		end;
		for i:= char_begin+1 to char_end-1 do if P^[i] <> nil then 
			Uvolni_zoznam_spojeni(Pstring(P^[i]));
		if P^[29] <> nil then 
			Uvolni_zoznam_nazvov_miest(Pprvok(P^[29]));
		Dispose(P);
		Exit;
	end;

	if P^[0] <> nil then Uvolni_zoznam_spojeni(Pstring(P^[0])); 
	if P^[29] <> nil then Uvolni_zoznam_nazvov_miest(Pprvok(P^[29]));
	for i:= char_begin+1 to char_end-1 do if P^[i] <> nil then 
		Uvolni_Pdecide_char( hlbka-1, P^[i]);
	Dispose(P);
end;

procedure Uvolni_strom;
begin
	Uvolni_Pdecide_char(NUM_OF_DECIDE_CHARS, first_char);
end;


{********* PRINT OUTPUT ******************************************************}


function koren(x: Pprvok): Pprvok;
begin
	if x^.otec = nil then begin
		koren:= nil;
		exit;
	end;
	while x^.otec<>x do
		x:=x^.otec;
	koren:= x;
end;

function vytvor(mesto: Pprvok): Pprvok;
begin
	mesto^.otec:= mesto;
	vytvor:= mesto;
end;


var 
	counter: longint;
procedure vypis(z_mesto, do_mesto: Pprvok; km: real);
var
	hours, mins: integer;
begin
	Inc(counter);
	mins:= Trunc(60*km/RYCHLOST_CHODCA) + 5;
	hours:= 25 + mins div 60;
	mins:= mins mod 60;
	writeln('# Walk_A', counter);
	writeln(TAB, '25:00', TAB, TAB, '0', TAB, z_mesto^.meno);
	write(hours, ':');
	if mins<10 then write('0');
	writeln(mins, TAB, TAB, TAB, trunc(km), TAB, do_mesto^.meno);
	if OBESMERY then begin
		writeln('# Walk_B', counter);
		writeln(TAB, '25:00', TAB, TAB, '0', TAB, do_mesto^.meno);
		write(hours, ':');
		if mins<10 then write('0');
		writeln(mins, TAB, TAB, TAB, trunc(km), TAB, z_mesto^.meno);
	end;
end;


function minimalna_vzdialenost(zdroj, ciel: Pprvok): real;
var
	dijkstra_first: Pprvok;
	hrana: Pdest;
	vrchol, pom: Pprvok;
	d: real;
begin
	dijkstra_first:= zdroj;
	dijkstra_first^.dijkstra_next:= nil;
	dijkstra_first^.dijkstra_hodnota:= 0;
	while dijkstra_first<>ciel do begin
		{pridaj_hrany_veduce_z_dijkstra_first:}
		hrana:= dijkstra_first^.moj_pstring^.dest;
		while (hrana<>nil) do begin
			if (hrana^.pridana) and (not hrana^.meno_prvok^.dijkstra_definitivny) then begin
				d:= dijkstra_first^.dijkstra_hodnota+hrana^.km;
				vrchol:= hrana^.meno_prvok;
				if (not vrchol^.dijkstra_nekonecno) then begin
					if d>vrchol^.dijkstra_hodnota then begin
						hrana:= hrana^.next;
						Continue;
					end else begin
						{vyhodim zo spojaku vrcholov (aby som ho pridal inde)}
						vrchol^.dijkstra_prev^.dijkstra_next:= vrchol^.dijkstra_next;
						if vrchol^.dijkstra_next<>nil then
							vrchol^.dijkstra_next^.dijkstra_prev:= vrchol^.dijkstra_prev;
					end;
				end;
				pom:= dijkstra_first;
				while (pom^.dijkstra_next<>nil) and 
				      (pom^.dijkstra_next^.dijkstra_hodnota<d) do
					pom:= pom^.dijkstra_next;
				vrchol^.dijkstra_nekonecno:= false;
				vrchol^.dijkstra_hodnota:= d;
				vrchol^.dijkstra_next:= pom^.dijkstra_next;
				vrchol^.dijkstra_prev:= pom;
				pom^.dijkstra_next:= vrchol;
				if vrchol^.dijkstra_next <> nil then
					vrchol^.dijkstra_next^.dijkstra_prev:= vrchol;
			end;
			hrana:= hrana^.next;
		end;
		{zaverecne koniny:}
		dijkstra_first^.dijkstra_definitivny:= true;
		dijkstra_first:= dijkstra_first^.dijkstra_next;
	end;
	pom:= zdroj;
	while (pom <> nil ) do begin
		pom^.dijkstra_definitivny:= false;
		pom^.dijkstra_nekonecno:= true;
		pom:= pom^.dijkstra_next;
	end;
	minimalna_vzdialenost:= ciel^.dijkstra_hodnota;
end;

procedure print_output_file;
{vytlaci na vystup co ma. Vedlajsie efekty: -uvolni vsetky Pdest-y.
 ostanu: Pstringy a Pprvky}
var
	z_mesto, do_mesto: Pprvok;
	koren_z_mesto, koren_do_mesto: Pprvok;
	km: real;
	hrana: Pdest;
begin
	while zac_spojaku <> nil do begin
		z_mesto:=zac_spojaku^.otcovska_struktura^.meno_prvok;
		do_mesto:=zac_spojaku^.meno_prvok;
		km:= zac_spojaku^.km;
		hrana:= zac_spojaku;
		zac_spojaku:= zac_spojaku^.zoznam_next;
		{teraz mam:  z_mesto,  do_mesto,  km }
		
		koren_z_mesto:=koren(z_mesto);
		koren_do_mesto:=koren(do_mesto);

		{koren_z_mesto je nil ak este nebol dany do ziadneho stromu}
		if (koren_z_mesto = nil) then koren_z_mesto:=vytvor(z_mesto);
		if (koren_do_mesto = nil) then koren_do_mesto:=vytvor(do_mesto);
		
		if (koren_z_mesto=koren_do_mesto) and
		   (minimalna_vzdialenost(z_mesto,do_mesto)<=(1+TOLERANCIA)*km)
	  	then continue;

		if (koren_z_mesto<>koren_do_mesto) then
			koren_do_mesto^.otec:= koren_z_mesto;
		hrana^.pridana:= true;
		hrana^.obratena_hrana^.pridana:= true;
		Vypis(z_mesto, do_mesto, km);
	end;
end;


{********* MAIN PROGRAM *****************************************************}

BEGIN
{	celkovy_pocet_miest:= 0;}
	counter:=0;
	initialize_town_structure;

	  {teraz vytvorim strom hran a zaroven spojak hran usporiadany podla 
	  velkosti a zaroven vytvori v 29.tych polozkach zoznamy miest}
writeln(stderr, 'Nacitavam vstupny subor...');
	zac_spojaku:= nil;
	second_zac_spojaku:= nil;
	create_tree_and_spojak; 

	{berie hrany zo spojaku, mesta vlozi do stromu komponent. Ak pride 
	hrana taka, ze obe mesta su v teze komponente, tak je nevypise.}
writeln(stderr, 'Odstranujem duplicity a vypisujem...');
	print_output_file; 

	{Uvolnuje pamat.}
writeln(stderr, 'Uvolnujem strom...');
	Uvolni_strom;
{writeln('celkovy pocet miest: ',celkovy_pocet_miest);}
END.
