(*
SAVER
(c) 2001 Michal Danihelka <danihelka@mbox.dkm.cz>
produced under GNU-GPL

This program works with database .tt (!!! only with version with absolute and relative
distances) of czech trains. It finds the best combination of tickets which you have to
buy to go from one station to another with minimal price in time interval which you chose.

Usage: saver <[-pp <path of pricelist>|-sp|-pd <path of data>]> [-sw|-pw <path
             of data>] [-t <minimal time of transport between two stations>] [-o]
             <name of enter station> <first possible time of departure>
             <name of target station> [last possible time of arrival]
             <input (.tt)> <output>

Parameters:
first: -pp <path of pricelist>
           - reads pricelist from file in format
             <add> <correct prefixes> (first letters in ALL correct names
             of trains) (each in one line)
             empty line
             <maximum of kilometres> <price> (each in one line); write any
             number of km bigger then previous to last line - it means that
             any km bigger then previous cost the last price; this option
             create data for faster next running
       -sp - same as -pp, but reads pricelist from standard file in saver
       -pd <path of data> - reads all needed data from this file
       nothing - reads all needed data from standard file in saver

second: -sw                 - write created data to standard file in saver
        -pw <path of data>  - write created data to this file

third: if you do not use this parametr, default minimal time of transport between
       two stations in saver will be entered

fourth: write best connection in format: departure; station; tabulator; arrival; station;
        tabulator; distance; price; number of train;
        (wihout this option it will write best connection in format: station;
        departure; arrival; distance; * if there is crossing; x if there is buying ticket)

last possible time of arrival: if you do not use this parametr default seeking time
                                 in saver will be entered

Every times are in this format: dd.mm.yyyy hh:mm
(It is possible to write dd.mm. hh:mm too)

!!! Every times must be in validity of .tt

Example:

saver 'Praha hl.n.' 21.6.2000 0:48 'Brno hl.n.' <d:\vlak.tt

Conditions for correct working of this program:
1. a,b...distances in pricelist; (a >= b) => (price (a) >= price (b))
2. in pricelist are prices sorted by maximal kilometres
3. unit exists2 works (it was created for old database .tt)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

For more detailed informations see gnu.txt
*)

Program Saver;

uses exists2;

const max_distance = 1000;
      {maximum distance between two stations}
      max_connections = 15000;
      {maximal number of connections in .tt}
      path_read_tt_def = '';
      {default path for .tt}
      max_length = 25;
      {maximum length of any name of stations}
      max_length_name_train = 30;
      {maximal length of any name of train}
      max_tariff = 50;
      {maximum number of distance tariffs in pricelist}
      from_date = '1.1.2001 0:00';
      {begin of validity of .tt}
      to_date = '1.1.2003 0:00';
      {end of validity of .tt}
      ok_connection: set of 9..57 = [9,43,48..57];
      {ord of characters which are possible in the begginng in connection}
      path_write_def = 'd:\zapocet\data';
      {default path for data file created by saver}
      path_read_def = 'd:\zapocet\cenik';
      {default path for pricelist file created by user}
      add_number = 10;
      {maximal number of different adds}
      add_diff = 10;
      {maximal number of strings in one add}
      int_def = 5;
      {default minimal time of transport between two stations in minutes}
      tim_def = 12;
      {default time interval for seeking in hours}
      hash_arr = 1328;
      {this is the size of hash array with stations}
      max_transfer = 200;
      {maximum of transfers in one conection}
      currency = 'czech crown';      
      year = '2001';
      reading = 500;
      {when program reads this number of stations from .tt program will show it}
      check = false;
      {true - program will use exists2}
      sure = false;
      {true - program saves only connection with 'Y' from exists2
       false - program saves connection with <> 'N' from exists2}
      optimal_arrival = true;
      {true - if exists more than one connection with same best price
      program will find connection with first arrival to target station,
      next criterion is number of transfers
      false - if exists more than one connection with same best price program
      will find connection with less transfering,
      next criterion is time of arrival to target station }
      type_def = '123';
      {default type of displaying the cheapest connection - you can write more than one type
      '1' = BUYING TICKETS - display only stations where you will buy tickets
      '2' = TRANFERING - display only stations where you will transfer to another train
      '3' = ALL STATIONS - display all stations with all informations}
      max_length_notes = 200;
      {maximal length of note in .tt}
      number_days = 7;
      {number of days for save output from exists2}
      write_note_def = false;
      {true - best connection will be printed with notes from .tt}

type numbers = longint;

     ukcom = ^com;
     info_type = array [-2..max_tariff] of numbers;
     {[-2] is total number of kilometres in this conection [-1] is price,
      [0] is number of used indexes of KIL}
     com = record
       info: info_type;
       next: ukcom;
     end;

     ukhour = ^hour;
     hour = record
       dep,arr,num: numbers;
       next: ukhour;
     end;

     ukdist = ^dist;
     dist = record
       dis: numbers;
       next: ukdist;
       hour: ukhour; {all connections}
     end;

     ukadds = ^adds;
     adds = record
       add: numbers; {index in array ADD_value}
       next: ukadds;
       dist: ukdist; {all distances}
     end;

     uksta = ^sta;

     ukdirect = ^direct;
     direct = record
       name: uksta;
       next: ukdirect;
       adds: ukadds;  {all adds}
     end;

     ukprevious = ^previous;
     previous = record
       name: uksta;
       next: ukprevious;
     end;

     ukcon = ^con;
     con = record
       pri,arr,num: numbers;
       next: ukcon;
     end;

     sta = record
       name: string [max_length];
       done,pos,buy,wri: boolean;
       {for Dijkstra - min_dis is permanent and for main seek is information of visit this station in actual connection,
       possibility to go through this station in best connection
       buy is information about buying ticket to this station}
       min_dis,min_dis_2,arr,dep,num,add,dis,help: numbers;
       {min_dis is minimal distance from enter station,min_dis_2 is minimal distance to target station,
       dep is departure from previous station, arr is arrival,num is number of train, add is add of whole
       connection,dis is distance from previous station}
       arrivals: array [0..max_distance] of ukcon;
       prev,next: uksta;
       {prev is station from where is actual connection to this station}
       direct: ukdirect; {all direct connections}
       prev_all: ukprevious {all previous stations}
     end;

     ukfront = ^front;
     front = record
       sta: uksta;
       next: ukfront;
     end;

     uknotes = ^notes;
     notes = record
       note: string [max_length_notes];
       next: uknotes;
     end;

     array_type = array [0..max_transfer] of numbers;

var add_value: array [1..add_number] of numbers;
    {sorted surcharges}
    add_numbers: array [0..add_number] of numbers;
    {0 is number of adds, others are numbers of different strings for each surcharges}
    add_chars: array [1..add_number,1..add_diff] of string;
    {chars in .tt for ALL surchges}
    dat: array [0..max_distance] of ukcom;
    {better combinations than direct way}
    pri: array [0..max_distance] of numbers;
    {prices of distances}
    kil: array [0..max_tariff] of numbers;
    {maximum of kilometers in tariffs}
    min_pri: array [0..max_distance] of numbers;
    {minimum possible price for all distances}
    all_notes: array [1..max_connections] of uknotes;
    {all notes for each connection}
    last_note: array [1..max_connections] of uknotes;
    {last record in each index}
    train_num: array [1..max_connections] of string [max_length_name_train];
    {numbers of train}
    go_note,test_note: array [1..max_connections,0..number_days] of boolean;
    {information if notes cancel connection, information if go_note was tested}
    par,min_int,first_time,last_time,go_first,train_number,help: numbers;
    {parameter which is actual, minimal time of transport between two stations,
    first possible time of departure and last possible time of arrival
    (first_time,last_time - in minutes from begin of validity of .tt
    go_first - in days from begin of validity of .tt)}
    minimal_add, minimal_price,minimal_stations,minimal_distance,minimal_transfer: numbers;
    minimal_dep,minimal_arr,minimal_dis,minimal_num,minimal_go: array_type;
    minimal_name: array [0..max_transfer] of uksta;
    {all informations about best connection}
    path_write,path_read,enter_station,target_station: string;
    enter_sta,target_sta: uksta;
    write_def: boolean;

procedure display_help;
begin
  writeln ('SAVER');
  writeln;
  writeln ('This program works with database .tt (!!! only with version with absolute and');
  writeln ('relative distances) of czech trains. It finds the best combination of tickets');
  writeln ('which you have to buy to go from one station to another with minimal price in');
  writeln ('time interval which you chose.');
  writeln;
  writeln ('Usage: saver <[-pp <path of pricelist>|-sp|-pd <path of data>]> [-sw|-pw <path');
  writeln ('             of data>] [-t <minimal time of transport between two stations>]');
  writeln ('             [-o] <name of enter station> <first possible time of departure>');
  writeln ('             <name of target station> [last possible time of arrival]');
  writeln ('             <input (.tt)> <output>');
  writeln;
  writeln ('Parameters:');
  writeln ('first: -pp <path of pricelist>');
  writeln ('           - reads pricelist from file in format');
  writeln ('             <add> <correct prefixes> (first letters in ALL correct names');
  writeln ('             of trains) (each in one line)');
  writeln ('             empty line');
  writeln ('             <maximum of kilometres> <price> (each in one line); write any');
  writeln ('             number of km bigger then previous to last line - it means that');
  writeln ('             any km bigger then previous cost the last price; this option');
  writeln ('             create data for faster next running');
  writeln ('       -sp - same as -pp, but reads pricelist from standard file in saver');
  writeln ('       -pd <path of data> - reads all needed data from this file');
  writeln ('       nothing - reads all needed data from standard file in saver');
  writeln;
  writeln ('second: -sw                 - write created data to standard file in saver');
  writeln ('        -pw <path of data>  - write created data to this file');
  writeln;
  writeln ('third: if you do not use this parametr, default minimal time of transport');
  writeln ('       between two stations in saver will be entered');
  writeln;
  writeln ('fourth: write best connection in format: departure; station; tabulator;');
  writeln ('        arrival; station; tabulator; distance; price; number of train');
  writeln ('        (wihout this option it will write best connection in format: station;');
  writeln ('        arrival; departure; distance; * if there is crossing; x if there is');
  writeln ('        buying ticket)');
  writeln;
  writeln ('last possible time of arrival: if you do not use this parametr, ',tim_def,' hours');
  writeln ('                                 will be entered as seeking time');
  writeln;
  writeln ('Every times are in this format: dd.mm.yyyy hh:mm');
  writeln ('(It is possible to write dd.mm. hh:mm too)');
  writeln;
  writeln ('!!! Every times must be in validity of .tt');
  writeln;
  writeln ('Example: saver ''Praha hl.n.'' 21.6.2000 0:48 ''Brno hl.n.'' <d:\vlak.tt');
  writeln;
  writeln ('Conditions for correct working of this program:');
  writeln ('1. a,b...distances in pricelist; (a >= b) => (price (a) >= price (b))');
  writeln ('2. in pricelist are prices sorted by maximal kilometres');
  writeln ('3. unit exists2 works (it was created for old database .tt)');
end;

procedure prepare_data;
var uk_com,uk_old: ukcom;
    create: boolean;
    text_file: text;

  procedure find_pathes;
  begin
    if paramstr (par) = '-sp' then
      begin
        path_read:=path_read_def;
        inc (par);
        create:=true
      end
    else
      if paramstr (par) = '-pp' then
        begin
          path_read:=paramstr (par + 1);
          inc (par,2);
          create:=true
        end
      else
        if paramstr (par) = '-pd' then
          begin
            path_read:= paramstr (par + 1);
            inc (par,2)
          end
        else
          path_read:= path_write_def;
    if paramstr (par) = '-sw' then
      begin
        path_write:=path_write_def;
        inc (par)
      end
    else
      if paramstr (par) = '-pw' then
        begin
          path_write:=paramstr (par + 1);
          inc (par,2)
        end
      else
        path_write:=''
  end;

  procedure end_connect (var uk:ukcom);
  var i:numbers;
  begin
  if (not eoln (text_file)) and (not eof (text_file)) then
    begin
      new (uk_com);
      uk:=uk_com;
      uk_com^.info [-2]:=0;
      read (text_file,uk_com^.info[-1],uk_com^.info[0]);
      for i:=1 to uk_com^.info[0] do
      begin
        read (text_file,uk_com^.info[i]);
        inc (uk_com^.info [-2],kil [uk_com^.info[i]]);
      end;
      uk_com^.next:=nil;
      end_connect (uk_com^.next);
    end;
  end;

  procedure read_data;
  var i,j,k:numbers;
  begin
    k:=0;
    while not eof (text_file) do
    begin
      readln (text_file);
      read (text_file,j);
      if (k - 1 + j) <= max_distance then
        begin
          dat [k]:= nil;
          end_connect (dat [k]);
          for i:= (k + 1) to (k - 1 + j) do
          dat [i]:= dat [i - 1];
          k:=k + j;
        end;
    end;
    for i:= k to max_distance do
      dat [k]:= nil;
    writeln ('Needed data was readed from file ''',path_read,'''');
  end;

  procedure sort_adds;
  var i,j,k,l:numbers;
      h: array [1..add_diff] of string [2];
  begin
    for i:=1 to add_numbers [0] do
    begin
      k:=i;
      for j:= i + 1 to add_numbers [0] do
      if add_value [j] < add_value [k] then
        k:=j;
      if k <> i then
        begin
          l:= add_value [k];
          add_value [k]:= add_value [i];
          add_value [i]:= l;
          for l:=1 to add_numbers [k] do
          h [l]:= add_chars [k,l];
          for l:=1 to add_numbers [i] do
          add_chars [k,l]:= add_chars [i,l];
          for l:=1 to add_numbers [k] do
          add_chars [i,l]:= h [l];
          l:= add_numbers [k];
          add_numbers [k]:= add_numbers [i];
          add_numbers [i]:= l;
        end;
    end;
  end;

  procedure read_pri_kil_add (inp:string);
  var i,j,k:numbers;
      a:char;
      x:string;
      last: boolean;
  begin
    assign (text_file,inp);
    reset (text_file);
    for i:= 0 to add_number do
    add_numbers [i]:= 0;
    while (not eoln (text_file)) and (not eof (text_file)) do
    begin
      inc (add_numbers [0]);
      read (text_file, add_value [add_numbers [0]],a);
      readln (text_file, x);
      if ord (X[length (X)]) = 13 then
        delete (x,length (x),1);
      last:= (pos (' ',x) = 0);
      while (pos (' ',x) > 0) or last do
      begin
        inc (add_numbers [add_numbers [0]]);
        if not last then
          begin
            add_chars [add_numbers [0],add_numbers [add_numbers [0]]]:= copy (x,1,pos (' ',x) - 1);
            delete (x,1,pos (' ',x));
            last:= (pos (' ',x) = 0);
          end
        else
          begin
            add_chars [add_numbers [0],add_numbers [add_numbers [0]]]:= x;
            x:= '';
            last:= false;
          end;
      end;
    end;
    if add_numbers [0] = 0 then
      begin
        writeln ('In pricelist are not any valid adds');
        close (text_file);
        halt;
      end;
    for k:=2 to add_numbers [0] do
    if add_value [k] < add_value [k - 1] then sort_adds;
    k:=0;
    kil [0]:=0;
    readln (text_file);
    while (not eoln (text_file)) and (not eof (text_file)) do
    begin
      inc (kil [0]);
      readln (text_file,kil [kil [0]],j);
      if kil [kil [0]] > max_distance then
        begin
          write ('In pricelist is distance bigger than ',max_distance);
          close (text_file);
          halt;
        end;
      if (kil [0] > 1) then
        begin
          if kil [kil [0]] < kil [kil [0] - 1] then
            begin
              write ('Pricelist is not sorted by maximal kilometres');
              close (text_file);
              halt;
            end;
          if j < pri [ kil [kil [0] - 1] ] then
            begin
              writeln ('In pricelist exists distance ',kil [kil [0]],' for price ',j);
              write ('and distance ',kil [kil [0] - 1],' for price ',pri [kil [kil [0] - 1]]);
              close (text_file);
              halt;
            end;
        end;
      for i:=k to kil [kil [0]] do
        pri [i]:=j;
      k:=kil [kil [0]] + 1;
    end;
    kil [kil [0]]:= max_distance;
    for i:=k to max_distance do
      pri [i]:= pri [i - 1];
    writeln;
    if not create then
      read_data;
    close (text_file);
  end;

  function compare_array (arr1,arr2: info_type):boolean;
  var i: numbers;
      compare:boolean;
  begin
    if (arr1[-1] = arr2[-1]) and (arr1[0] = arr2[0]) then
      begin
        compare:=true;
        i:=1;
        while (i <= arr1 [0]) and compare do
        begin
          compare:= arr1[i] = arr2 [i];
          inc (i);
        end;
        compare_array:= compare;
      end
    else
      compare_array:=false;
  end;

  procedure connect (uk: ukcom;arr: info_type);
  var i: numbers;
  begin
    if uk^.info [-1] > arr [-1] then
      begin
        new (uk_com);
        uk_com^.next:=uk^.next;
        for i:= -1 to uk^.info [0] do
        uk_com^.info [i]:= uk^.info [i];
        for i:= -1 to arr [0] do
        uk^.info [i]:= arr [i];
        uk^.next:=uk_com;
      end
    else
      if not compare_array (arr,uk^.info) then
        connect (uk^.next,arr)
  end;

  procedure merge (arr1,arr2:info_type;var arr:info_type);
  var i,j,k: numbers;
  begin
    i:=1;
    j:=1;
    k:=1;
    arr [-1]:= arr1 [-1] + arr2 [-1];
    arr [0]:= arr1 [0] + arr2 [0];
    while (i <= arr1 [0]) and (j <= arr2 [0]) do
    if arr1 [i] <= arr2 [j] then
      begin
        arr [k]:= arr1 [i];
        inc (i);
        inc (k);
      end
    else
      begin
        arr [k]:= arr2 [j];
        inc (j);
        inc (k);
      end;
    while (i <= arr1 [0]) do
    begin
      arr [k]:= arr1 [i];
      inc (i);
      inc (k);
    end;
    while (j <= arr2 [0]) do
    begin
      arr [k]:= arr2 [j];
      inc (j);
      inc (k);
    end;
  end;

  procedure find_better (uk: ukcom;arr: info_type;i,j,k,l: numbers);
  {find out if combination uk^.info or direct way is possible in actual connection}
  var arr_next: info_type;
  begin
    if uk <> nil then
      if (uk^.info [-1] <= (l - min_pri [k])) or ((k = 0) and (uk^.info [-1] <= l)) then
        begin
          if (j <= uk^.info [-2]) then
            begin
              l:= l - uk^.info [-1];
              merge (uk^.info,arr,arr_next);
              if k = 0 then
                connect (dat [i],arr_next)
              else
                find_better (dat [k],arr_next,i,k,0,l);
            end;
          find_better (uk^.next,arr,i,j,k,l);
        end;
  end;

  procedure dispose_dat (uk: ukcom);
  begin
    if uk <> nil then
      begin
        dispose_dat (uk^.next);
        dispose (uk);
      end;
  end;

  procedure compare_combinations (i: numbers);
  {add total kilometres and if whole two combinaions are same then first will be canceled}
  var dispose_it: boolean;
      n: numbers;
  begin
    uk_com:= dat [i];
    while uk_com <> nil do
    begin
      uk_com^.info [-2]:=0;
      for n:= 1 to uk_com^.info [0] do
      inc (uk_com^.info [-2],kil [uk_com^.info [n]]);
      uk_com:= uk_com^.next;
    end;
    dispose_it:= true;
    uk_com:= dat [i];
    uk_old:= dat [i - 1];
    while (uk_com <> nil) and dispose_it do
      if uk_old <> nil then
        begin
          dispose_it:= compare_array (uk_com^.info,uk_old^.info);
          uk_com:=uk_com^.next;
          uk_old:=uk_old^.next;
        end
      else
        dispose_it:=false;
    if dispose_it then
      dispose_it:= uk_com = uk_old;
    if dispose_it then
      begin
        dispose_dat (dat [i]);
        dat [i]:= dat [i - 1];
      end;
  end;

  procedure create_dat;
  {finds for all distanceses all combinations better then direct way}
  var i,j,k,l,caunt:numbers;
      arr: info_type;
  begin
    write ('Creating needed data: ');
    caunt:= 0;
    l:=0;
    min_pri [0]:= pri [0];
    for k:=1 to kil[0] do
    begin
      for j:=l to kil [k] do
      begin
        new (uk_com);
        dat [j]:=uk_com;
        uk_com^.info [-1]:= pri [kil [k]];
        uk_com^.info [0]:= 1;
        uk_com^.info [1]:= k;
        uk_com^.next:= nil;
      end;
      l:= kil [k] + 1;
    end;
    dat [0]^.info [-2]:= kil [1];
    for i:=1 to max_distance do
    {i is total distance}
    begin
      inc (caunt);
      if caunt >= (max_distance div 50) then
        begin
          write ('&');
          caunt:= 0;
        end;
      for j:= (i - 1) downto ((i div 2) + (i mod 2)) do
      {j is distance in first part of connection}
      begin
        k:=i - j;
        {k is distance in second part of conection}
        l:= pri[i] - 1;
        {l is maximal price of seeking connection}
        arr [-1]:= 0;
        arr [0]:= 0;
        find_better (dat [j],arr,i,j,k,l);
      end;
      compare_combinations (i);
      min_pri [i]:= dat [i]^.info [-1];
    end;
    writeln;
    writeln ('Needed data was created from file ''',path_read,'''');
  end;

begin
  par:=1;
  create:= false;
  find_pathes;
  read_pri_kil_add (path_read);
  if create then
    create_dat;
end;

procedure save_data;
{save data to file in format:
first part is same as whole pricelist
empty line
number of same best combinations and then these best combinations sorted by price in one line}
var text_file: text;
    i,j,k: numbers;
    uk_com: ukcom;

begin
  assign (text_file,path_write);
  rewrite (text_file);
  for i:=1 to add_numbers [0] do
    begin
      write (text_file,add_value [i]);
      for j:=1 to add_numbers [i] do
      write (text_file,' ',add_chars [i,j]);
      writeln (text_file);
    end;
  writeln (text_file);
  for i:=1 to kil [0] do
  writeln (text_file,kil [i],' ',pri [kil [i]]);
  i:=0;
  while i <= max_distance do
  begin
    j:=1;
    while (dat [i] = dat [i + j]) and (i + j <= max_distance) do
    inc (j);
    writeln (text_file);
    write (text_file,j);
    uk_com:=dat [i];
    while uk_com <> nil do
    begin
      for k:=-1 to uk_com^.info [0] do
      write (text_file,' ',uk_com^.info [k]);
      uk_com:=uk_com^.next;
    end;
    i:=i + j;
  end;
  writeln ('Needed data was saved to file ''',path_write,'''');
  close (text_file);
end;

function convert_str_int (x:string):numbers;
{covert string with time in day to numbers;}
var i,k:numbers;
    j,l: integer;
begin
  if x[1] = '+' then
    begin
      val (copy (x,2,length (x) - 1),i,j);
      if j = 0 then
        convert_str_int:=i
      else
        convert_str_int:=0
    end
  else
    if pos (':',x) > 0 then
      begin
        val (copy (x,1,pos (':',x) - 1),i,j);
        val (copy (x,pos (':',x) + 1,length (x) - pos (':',x)),k,l);
        if (j = 0) and (l = 0) then
          convert_str_int:= 60 * i + k
        else
          convert_str_int:=0
      end
    else
      begin
        val (x,i,j);
        if j = 0 then
          convert_str_int:=i
        else
          convert_str_int:=0;
      end
end;

function add_times (x: numbers; y:string):numbers;
{add two times in .tt}
begin
  if pos (':',y) > 0 then
    add_times:=convert_str_int (y)
  else
    add_times:= x + convert_str_int (y)
end;

procedure read_var (var d,m,y:numbers;x:string);
{find out day,month,year from string}
var j,p1,p2:integer;
begin
  p1:=pos ('.',x);
  p2:=p1 + pos ('.',copy (x,p1 + 1,length (x) - p1));
  val (copy (x,1,p1 - 1),d,j);
  if j <> 0 then
    d:=0;
  val (copy (x,p1 + 1,p2 - 1 - p1),m,j);
  if j <> 0 then
    m:=0;
  val (copy (x,p2 + 1,length (x) - p2),y,j);
  if j <> 0 then
    y:=0;
end;

function convert_date_int (x:string):numbers;
{convert date dd.mm.yyyy to numbers}
const days: array [1..12] of numbers = (31,28,31,30,31,30,31,31,30,31,30,31);

var num,d1,d2,d3,m1,m2,m3,y1,y2,y3,help: numbers;

  function is_before (d1,d2,m1,m2,y1,y2:numbers):boolean;
  var fun: boolean;
  begin
    fun:= false;
    if y1 < y2 then
      fun:=true
    else
      if y1 = y2 then
        if m1 < m2 then
          fun:=true
        else
          if m1 = m2 then
            if d1 <= d2 then
               fun:=true;
    is_before:= fun;
  end;

  function count_month (m1,m2: numbers):numbers;
  var i: numbers;
  begin
    help:=0;
    if m2 < m1 then
      begin
        for i:=m2 to (m1 - 1) do
        help:=help - days [i];
      end
    else
      for i:=m1 to (m2 - 1) do
      help:=help + days [i];
    count_month:= help;
  end;

  function count_year (y1,y2: numbers):numbers;
  var i:numbers;
  begin
    i:=y1;
    help:=0;
    if ((d1 < 29) and (m1 =2)) or ((d1 < 32) and (m1 = 1)) then
      begin
        while (i mod 4 > 0) and is_before (d1,d2,m1,m2,i + 1,y2) do
        inc (i);
        while (i mod 4 = 0) and is_before (29,d2,2,m2,i,y2) do
        begin
          inc (help);
          inc (i,4)
        end
      end
    else
      begin
        while ((i + 1) mod 4 > 0) and is_before (d1,d2,m1,m2,i + 1,y2) do
        inc (i);
        while ((i + 1) mod 4 = 0) and is_before (29,d2,2,m2,i + 1,y2) do
        begin
          inc (help);
          inc (i,4)
        end;
      end;
    count_year:=help + (y2 - y1)*365;
  end;

begin
read_var (d1,m1,y1,copy (from_date,1,pos (' ',from_date) - 1));
read_var (d2,m2,y2,x);
read_var (d3,m3,y3,copy (to_date,1,pos (' ',to_date) - 1));
if is_before (d1,d2,m1,m2,y1,y2) and is_before (d2,d3,m2,m3,y2,y3) then
  begin
    num:=d2 - d1;
    num:=num + count_month (m1,m2);
    num:=num + count_year (y1,y2);
    convert_date_int:= num*1440;
  end
else
  convert_date_int:= -1;
end;

function convert_int_str (i:numbers):string;
{convert numbers to string}
var x,y:string;
begin
  str ((i div 60) mod 24,x);
  str (i mod 60,y);
  if length (y) = 1 then
    y:='0' + y;
  convert_int_str:=x + ':' + y
end;

function convert_int_date (i:numbers):string;
{convert numbers to date dd.mm.yyyy}
const days: array [1..12] of numbers = (31,28,31,30,31,30,31,31,30,31,30,31);

var d,m,y: numbers;
    x,date: string;

  procedure find_date (var d,m,y: numbers; day: numbers);
  begin
    if day > 0 then
      begin
        if ((d = 28) and (m = 2) and (y mod 4 = 0)) or (d + 1 <= days [m]) then
          begin
            inc (d);
            find_date (d,m,y,day - 1);
          end
        else
          if m + 1 <= 12 then
            begin
              d:=1;
              inc (m);
              find_date (d,m,y,day - 1);
            end
          else
            begin
              d:=1;
              m:=1;
              inc (y);
              find_date (d,m,y,day - 1);
            end
      end;
  end;

  procedure create_date;
  begin
    date:='';
    str (d,x);
    date:= date + x;
    str (m,x);
    date:= date + '.' + x;
    str (y,x);
    date:= date + '.' + x;
  end;

begin
  read_var (d,m,y,copy (from_date,1,pos (' ',from_date) - 1));
  find_date (d,m,y,i div 1440);
  create_date;
  convert_int_date:=date;
end;

function find_add (x: string):numbers;
var leng,sur,i,j: integer;
begin
  sur:=0;
  leng:=0;
  for i:=1 to add_numbers [0] do
  for j:=1 to add_numbers [i] do
  if (add_chars [i,j] = copy (x,1,length (add_chars [i,j]))) and (length (add_chars [i,j]) > leng) then
    begin
      sur:= i;
      leng:= length (add_chars [i,j]);
    end;
  find_add:= sur;
end;

procedure find_connection;
var stations: array [0..hash_arr] of uksta;
    {main array with all stations}
    act_add, act_price,act_stations,act_distance,act_transfer: numbers;
    act_dis,act_go: array_type;
    {information about actual connection}
    uk_sta: uksta;
    uk_direct: ukdirect;
    uk_adds: ukadds;
    uk_dist: ukdist;
    uk_hour: ukhour;
    uk_notes: uknotes;
    a:char;
    surcharge,total_maximal_add,total_maximal_distance,total_minimal_distance: numbers;
    {actual surcharge}
    dep,arr,dis,dis_before: numbers;
    sta_from,sta_to: string [max_length];
    tt_file: text;

  function hash (x:string): numbers;
  var a,b,c,d: numbers;
  begin
    a:= ord (x[1]);
    b:= ord (x[2]);
    c:= ord (x [length (x) - 1]);
    d:= ord (x [length (x)]);
    hash:= ((a * d) mod (1245)) + ((b * c) mod (hash_arr - 1243));
  end;

  procedure init_var;
  var i,j:numbers;
      x: string;
  begin
    if paramstr (par) = '-t' then
      begin
        min_int:= convert_str_int (paramstr (par + 1));
        inc (par,2);
      end
    else
      min_int:=int_def;
    if paramstr (par) = '-o' then
      begin
        write_def:= false;
        inc (par);
      end
    else
      write_def:= true;
    if (par > paramcount) then
      begin
        writeln ('No information about enter station');
        halt;
      end
    else
      begin
        enter_station:= paramstr (par);
        inc (par);
      end ;
    if (par > paramcount) then
      begin
        writeln ('No information about first possible DATE of departure');
        halt;
      end
    else
      begin
        x:= paramstr (par);
        inc (par);
        if x [length (x)] = '.' then
          x:= x + year;
        first_time:= convert_date_int (x);
        if first_time = -1 then
          begin
            writeln ('Bad first possible date of departure');
            halt;
          end;
      end;
    if par > paramcount then
      begin
        writeln ('No information about first possible TIME of departure');
        halt;
      end
    else
      begin
        help:= convert_str_int (paramstr (par));
        if (help < 0) or (help > 1440) then
          begin
            writeln ('Bad first possible time of departure');
            halt;
          end
        else
          inc (first_time,help);
        inc (par);
      end;
    if par > paramcount then
      begin
        writeln ('No information about target station');
        halt;
      end
    else
      begin
        target_station:= paramstr (par);
        inc (par);
      end;
    if enter_station = target_station then
      begin
        writeln ('Enter station is same as target station');
        halt;
      end;
    if (par + 1 <= paramcount) then
      begin
        x:= paramstr (par);
        if x [length (x)] = '.' then
          x:= x + year;
        last_time:= convert_date_int (x);
        if last_time = -1 then
          begin
            writeln ('Bad last possible date of arrival');
            halt;
          end;
        help:= convert_str_int (paramstr (par + 1));
        if (help < 0) or (help > 1440) then
          begin
            writeln ('Bad last possible time of arrival');
            halt;
          end
        else
          inc (last_time,help);
        if last_time < first_time then
          begin
            writeln ('Time of arrival is before time of departure');
            halt;
          end;
        inc (par,2);
      end
    else
      last_time:= first_time + tim_def * 60;
    go_first:=first_time div 1440;
    par:=0;
    for i:=1 to max_connections do
    begin
      all_notes [i]:= nil;
      last_note [i]:= nil;
    end;
    for i:=1 to max_connections do
    for j:=0 to number_days do
    test_note [i,j]:= false;
    for i:=0 to hash_arr do
    stations [i]:= nil;
    for i:=0 to max_distance do
    min_pri [i]:= dat[i]^.info [-1] + add_value [1];
    total_maximal_add:= add_numbers [0];
    total_maximal_distance:= max_distance;
    minimal_add:= 0;
    minimal_transfer:= maxint;
    train_number:= 1;
    minimal_price:= pri [max_distance] + add_value [add_numbers [0]];
    minimal_stations:= 0;
    minimal_arr [0]:= last_time + 1;
  end;

  function find_uksta (x: string): uksta;
  var uk: uksta;
      ok: boolean;
  begin
    uk:= stations [hash (x)];
    ok:=true;
    while (uk <> nil) and ok do
    if uk^.name = x then
      ok:= false
    else
      uk:= uk^.next;
    find_uksta:=uk;
  end;

  procedure combinations;
  {finds act_go and act_price = best combination for actual connection}
  var uk: ukcom;
      ok,ok2: boolean;

    function enter_it (arr: info_type;tot_dis,index:numbers;tot_fun: boolean): boolean;
    {it finds if current combination is posible for actual connection}
    var i,j,ind: numbers;
        fun: boolean;
        help: info_type;

    begin
      fun:= tot_fun;
      if arr [-2] >= tot_dis then
        begin
          i:=1;
          while (i <= arr [0]) and (not fun) do
          begin
            j:=0;
            ind:= index;
            while ((j + act_dis [ind] - act_dis [ind - 1]) <= kil [arr [i]]) and (not fun) do
            begin
              inc (j,act_dis [ind] - act_dis [ind - 1]);
              inc (ind);
              if ind > act_stations then
                begin
                  act_price:= uk^.info [-1] + add_value [act_add];
                  fun:= true;
                end;
            end;
            dec (tot_dis,j);
            inc (act_go [0]);
            act_go [act_go [0]]:= ind - 1;
            for j:= -2 to arr [0] do
            help [j]:= arr [j];
            dec (help [-2],kil [help [i]]);
            help [i]:= help [help [0]];
            dec (help [0]);
            inc (i);
            if not enter_it (help,tot_dis,ind,fun) then
              dec (act_go [0])
            else
              fun:= true;
          end;
        end;
      enter_it:= fun;
    end;

  begin
    if act_stations > 0 then
      begin
        uk:= dat [act_distance];
        ok:= true;
        ok2:=true;
        act_go [0]:= 0;
        while (uk <> nil) and ok and ok2 do
        if uk^.info [-1] + add_value [act_add] > minimal_price then
          ok2:= false
        else
          if enter_it (uk^.info,act_distance,1,false) then
            ok:= false
          else
            uk:= uk^.next;
        if ok or (not ok2) then
          begin
            act_price:= pri [act_distance] + add_value [act_add];
            act_go [0]:= 1;
            act_go [1]:= act_stations;
          end;
      end
    else
      act_price:=maxint;
  end;

  function read_before (i: numbers): string ;
  var help: string;
  begin
    help:='';
    while (ord (a) <> i) do
    if (not eof (tt_file)) then
      begin
        help:= help + a;
        read (tt_file,a);
      end
    else
      begin
        help:= help + a;
        a:= chr (i);
      end;
    read (tt_file,a);
    read_before:= help;
  end;

  procedure save_connection_5 (var var_uk: ukhour);
  var uk: ukhour;
      ok: boolean;

    procedure save_it (var uk: ukhour); {only when uk = nil}
    begin
      new (uk_hour);
      uk:= uk_hour;
      uk_hour^.num:= train_number;
      uk_hour^.dep:= dep;
      uk_hour^.arr:= arr;
      uk_hour^.next:= nil;
    end;

  begin
    if var_uk = nil then
      save_it (var_uk)
    else
      begin
        ok:= true;
        uk:= var_uk;
        while ok do
        if arr <= uk^.arr then
          begin
            new (uk_hour);
            uk_hour^:= uk^; {COPY WHOLE RECORD}
            uk^.arr:= arr;
            uk^.dep:= dep;
            uk^.num:= train_number;
            uk^.next:= uk_hour;
            ok:= false;
          end
        else
          if uk^.next = nil then
            begin
              save_it (uk^.next);
              ok:= false;
            end
          else
            uk:=uk^.next
      end;
  end;

  procedure save_connection_4 (var var_uk: ukdist);
  var uk: ukdist;
      ok: boolean;

    procedure save_it (var uk: ukdist); {only when uk = nil}
    begin
      new (uk_dist);
      uk:= uk_dist;
      uk_dist^.dis:= dis;
      uk_dist^.next:= nil;
      uk_dist^.hour:= nil;
      save_connection_5 (uk_dist^.hour);
    end;

  begin
    if var_uk = nil then
      save_it (var_uk)
    else
      begin
        ok:= true;
        uk:= var_uk;
        while ok do
        if dis < uk^.dis then
          begin
            new (uk_dist);
            uk_dist^:= uk^; {COPY WHOLE RECORD}
            uk^.dis:= dis;
            uk^.next:= uk_dist;
            uk^.hour:= nil;
            save_connection_5 (uk^.hour);
            ok:= false;
          end
        else
          if dis = uk^.dis then
            begin
              save_connection_5 (uk^.hour);
              ok:= false;
            end
          else
            if uk^.next = nil then
              begin
                save_it (uk^.next);
                ok:= false;
              end
            else
              uk:=uk^.next
      end;
  end;

  procedure save_connection_3 (var var_uk: ukadds);
  var uk: ukadds;
      ok: boolean;

    procedure save_it (var uk: ukadds); {only when uk = nil}
    begin
      new (uk_adds);
      uk:= uk_adds;
      uk_adds^.add:= surcharge;
      uk_adds^.next:= nil;
      uk_adds^.dist:= nil;
      save_connection_4 (uk_adds^.dist);
    end;

  begin
    if var_uk = nil then
      save_it (var_uk)
    else
      begin
        ok:= true;
        uk:= var_uk;
        while ok do
        if (surcharge < uk^.add) then
          begin
            new (uk_adds);
            uk_adds^:= uk^; {COPY WHOLE RECORD}
            uk^.add:= surcharge;
            uk^.next:= uk_adds;
            uk^.dist:= nil;
            save_connection_4 (uk^.dist);
            ok:= false;
          end
        else
          if (surcharge = uk^.add) then
            begin
              save_connection_4 (uk^.dist);
              ok:= false;
            end
          else
            if uk^.next = nil then
              begin
                save_it (uk^.next);
                ok:= false;
              end
            else
              uk:=uk^.next
      end;
  end;

  procedure save_connection_2 (var var_uk: ukdirect);
  var uk: ukdirect;
      ok: boolean;
      uk_help: uksta;

    procedure save_it (var uk: ukdirect); {only when uk = nil}
    var i :numbers;
        uk_previous: ukprevious;
    begin
      new (uk_direct);
      uk:= uk_direct;
      if uk_help = nil then
        begin
          i:= hash (sta_to);
          new (uk_sta);
          uk_sta^.name:= sta_to;
          uk_sta^.next:= stations [i];
          uk_sta^.direct:= nil;
          uk_sta^.prev_all:= nil;
          uk_help:=uk_sta;
          stations [i]:= uk_sta;
        end;
      uk_direct^.name:= uk_help;
      uk_direct^.next:= nil;
      uk_direct^.adds:= nil;
      new (uk_previous);
      uk_previous^.next:=uk_help^.prev_all;
      uk_previous^.name:=find_uksta (sta_from);
      uk_help^.prev_all:= uk_previous;
      save_connection_3 (uk_direct^.adds);
    end;

  begin
    uk_help:= find_uksta (sta_to);
    if var_uk = nil then
      save_it (var_uk)
    else
      begin
        ok:= true;
        uk:= var_uk;
        while ok do
        if uk^.name = uk_help then
          begin
            save_connection_3 (uk^.adds);
            ok:= false;
          end
        else
          if uk^.next = nil then
            begin
              save_it (uk^.next);
              ok:= false;
            end
          else
            uk:=uk^.next
      end;
  end;

  procedure save_connection (var var_uk: uksta);
  var uk: uksta;
      ok: boolean;

    procedure save_it (var uk: uksta); {only when uk = nil}
    begin
      new (uk_sta);
      uk:= uk_sta;
      uk_sta^.name:= sta_from;
      uk^.prev_all:= nil;
      uk_sta^.next:= nil;
      uk_sta^.direct:= nil;
      save_connection_2 (uk_sta^.direct);
    end;

  begin
    if var_uk = nil then
      save_it (var_uk)
    else
      begin
        ok:= true;
        uk:= var_uk;
        while ok do
        if uk^.name = sta_from then
          begin
            save_connection_2 (uk^.direct);
            ok:= false;
          end
        else
          if uk^.next = nil then
            begin
              save_it (uk^.next);
              ok:= false;
            end
          else
            uk:=uk^.next
      end;
  end;

  procedure read_notes;
  var x:string;
  begin
    while (a <> '#') and (not eof (tt_file)) do
    begin
      if ord (a) = 13 then
        read (tt_file,a);
      if ord (a) = 10 then
        read (tt_file,a)
      else
        begin
          x:= read_before (10);
          if ord (x [length (x)]) = 13 then
            delete (x,length (x),1);
          while x [length (x)] = ' ' do
          delete (x,length (x),1);
          new (uk_notes);
          if all_notes [train_number] <> nil then
            last_note [train_number]^.next:= uk_notes
          else
            all_notes [train_number]:= uk_notes;
          last_note [train_number]:= uk_notes;
          uk_notes^.note:= x;
          uk_notes^.next:= nil;
        end;
    end;
  end;

  procedure read_connection;
  var help: numbers;
      x: string;
  begin
    if ord (a) in ok_connection then
      begin
        x:=read_before (9);
        if x <> '' then
          begin
            arr:= add_times (dep,x);
            help:= add_times (arr,read_before (9));
          end
        else
          begin
            help:= add_times (dep,read_before (9));
            arr:=help;
          end;
        if arr < dep then
          arr:= arr + 1440;
        read_before (9);
        if a = '+' then
          dis:=convert_str_int (read_before (9))
        else
          dis:=convert_str_int (read_before (9)) - dis_before;
        dis_before:= dis;
        sta_to:= read_before (10);
        if ord (sta_to [length (sta_to)]) = 13 then
          delete (sta_to,length (sta_to),1);
        save_connection (stations [hash (sta_from)]);
        dep:= help;
        sta_from:= sta_to;
        read_connection;
      end
    else
      read_notes;
  end;

  procedure read_tt;
  var x: string;
  begin
    write ('Reading .tt: ');
    assign (tt_file,path_read_tt_def);
    reset (tt_file);
    repeat
    read (tt_file,a);
    until (a = '#') or eof (tt_file);
    while a = '#' do
    begin
      read (tt_file,a,a);
      x:= read_before (10);
      if ord (x [length (x)]) = 13 then
        delete (x,length (x),1);
      surcharge:= find_add (x);
      if (surcharge > 0) and (ord (a) in ok_connection) then
        begin
          train_num [train_number]:= x;
          dis_before:= 0;
          read_before (9);
          dep:= convert_str_int (read_before (9));
          read_before (9);
          read_before (9);
          sta_from:= read_before (10);
          if ord (sta_from [length (sta_from)]) = 13 then
            delete (sta_from,length (sta_from),1);
          read_connection;
          inc (train_number);
        end
      else
        while (a <> '#') and (not eof (tt_file)) do
        read (tt_file,a);
      if (train_number mod reading) = 0 then
        write ('*');
    end;
    close (tt_file);
    writeln;
    writeln ('Needed .tt data was readed');
    enter_sta:= find_uksta (enter_station);
    target_sta:= find_uksta (target_station);
    if enter_sta = nil then
      begin
        writeln (enter_station,' is not in .tt');        
        halt;
      end;
    if target_sta = nil then
      begin
        writeln (target_station,' is not in .tt');
        halt;
      end;
  end;

  function go_it (number,day: numbers): boolean;
  var c: char;
      ok,fun: boolean;
      num: numbers;
  begin
    fun:= true;
    num:= day - go_first;
    ok:= false;
    if num <= number_days then
      if test_note [number,num] then
        fun:= go_note [number,num]
      else
        begin
          ok:= true;
          test_note [number,num]:= true;
        end;
    if num > number_days then
      ok:= true;
    if ok then
      begin
        uk_notes:= all_notes [number];
        while (uk_notes <> nil) and fun do
        begin
          c:= 'Y';
          if check then
            c:= jede (convert_int_date (day * 1440) + #9 + uk_notes^.note);
          if c = 'N' then
            fun:=false
          else
            if sure and (c <> 'Y') then
              fun:=false;
          uk_notes:= uk_notes^.next;
        end;
        if num <= number_days then
          go_note [number,num]:= fun;
      end;
    go_it:= fun;
  end;

  procedure dispose_ukuk (uk: ukfront);
  begin
    if uk <> nil then
      begin
        dispose_ukuk (uk^.next);
        dispose (uk);
      end;
  end;

  function find_distance (uk_from,uk_to:uksta;max_add,i:numbers; var dis: numbers):boolean;
  {true when finds connection with distance < i with add <= max_add}
  var ok: boolean;
      uk: ukadds;
  begin
    uk_direct:= uk_from^.direct;
    while uk_direct^.name <> uk_to do
    uk_direct:= uk_direct^.next;
    dis:= max_distance;
    uk:= uk_direct^.adds;
    ok:=true;
    while (uk <> nil) and ok do
    if uk^.add > max_add then
      ok:= false
    else
      begin
        if dis > uk^.dist^.dis then
          dis:= uk^.dist^.dis;
        uk:= uk^.next;
      end;
    if dis < i then
      find_distance:= true
    else
      find_distance:= false;
  end;

  procedure Dijkstra_inv (max_add: numbers);
  var i: numbers;
      first_uk,new_uk: ukfront;
      ok: boolean;

    procedure init (uk: uksta);
    begin
      if uk <> nil then
        begin
          uk^.min_dis_2:= max_distance;
          uk^.done:= false;
          init (uk^.next);
        end;
    end;

    procedure take_first;
    var uknext: ukprevious;
        uk_help: ukfront;
        ok2: boolean;
        i,dis: numbers;

      procedure enter_in_front (var uk: ukfront);
      begin
        if uk = nil then
          begin
            new (new_uk);
            new_uk^.sta:= uknext^.name;
            new_uk^.next:= nil;
            uk:= new_uk;
          end
        else
          if uk^.sta^.min_dis_2 >= uknext^.name^.min_dis_2 then
            begin
              new (new_uk);
              new_uk^:= uk^;
              uk^.next:= new_uk;
              uk^.sta:= uknext^.name;
            end
          else
            enter_in_front (uk^.next);
      end;

    begin
      ok2:= true;
      while ok2 do
      if (first_uk^.sta^.done) then
        begin
          uk_help:= first_uk;
          first_uk:= first_uk^.next;
          dispose (uk_help);
          ok2 := first_uk <> nil;
        end
      else
        ok2:= false;
      if (first_uk <> nil) then
        begin
          first_uk^.sta^.done:= true;
          if (first_uk^.sta^.min_dis_2 <= total_maximal_distance) then
            uknext:= first_uk^.sta^.prev_all
          else
            begin
              ok:= false;
              uknext:= nil;
            end;
          while uknext <> nil do
          begin
            i:= uknext^.name^.min_dis_2 - first_uk^.sta^.min_dis_2;
            if (i > 0) and not (uknext^.name^.done) then
              if find_distance (uknext^.name,first_uk^.sta,max_add,i,dis) then
                begin
                  uknext^.name^.min_dis_2:= dis + first_uk^.sta^.min_dis_2;
                  enter_in_front (first_uk^.next);
                end;
            uknext:= uknext^.next;
          end;
          uk_help:= first_uk;
          first_uk:= first_uk^.next;
          dispose (uk_help);
        end;
    end;

  begin
    for i:=0 to hash_arr do
    init (stations [i]);
    inc (enter_sta^.min_dis_2);
    new (new_uk);
    first_uk:= new_uk;
    first_uk^.next:= nil;
    first_uk^.sta:= target_sta;
    first_uk^.sta^.min_dis_2:= 0;
    ok:= true;
    while (first_uk <> nil) and ok do
    take_first;
    dispose_ukuk (first_uk);
    write ('#');
  end;

  procedure Dijkstra_nor (max_add: numbers);
  var i: numbers;
      first_uk,new_uk: ukfront;
      ok: boolean;

    procedure init (uk: uksta);
    var i: numbers;
    begin
      if uk <> nil then
        begin
          uk^.min_dis:= max_distance;
          uk^.done:= false;
          uk^.buy:= false; {for main seeking}
          for i:=0 to max_distance do
          uk^.arrivals [i]:= nil; {for main seeking}
          init (uk^.next);
        end;
    end;

    procedure take_first;
    var uknext: ukdirect;
        uk_help: ukfront;
        ok2: boolean;
        i,dis: numbers;

      procedure enter_in_front (var uk: ukfront);
      begin
        if uk = nil then
          begin
            new (new_uk);
            new_uk^.sta:= uknext^.name;
            new_uk^.next:= nil;
            uk:= new_uk;
          end
        else
          if uk^.sta^.min_dis >= uknext^.name^.min_dis then
            begin
              new (new_uk);
              new_uk^:= uk^;
              uk^.next:= new_uk;
              uk^.sta:= uknext^.name;
            end
          else
            enter_in_front (uk^.next);
      end;

    begin
      ok2:= true;
      while ok2 do
      if (first_uk^.sta^.done) then
        begin
          uk_help:= first_uk;
          first_uk:= first_uk^.next;
          dispose (uk_help);
          ok2 := first_uk <> nil;
        end
      else
        ok2:= false;
      if (first_uk <> nil) then
        begin
          first_uk^.sta^.done:= true;
          if (first_uk^.sta^.min_dis <= total_maximal_distance) then
            uknext:= first_uk^.sta^.direct
          else
            begin
              ok:= false;
              uknext:= nil;
            end;
          while uknext <> nil do
          begin
            i:= uknext^.name^.min_dis - first_uk^.sta^.min_dis;
            if (i > 0) and not (uknext^.name^.done) then
              if find_distance (first_uk^.sta,uknext^.name,max_add,i,dis) then
                begin
                  uknext^.name^.min_dis:= dis + first_uk^.sta^.min_dis;
                  enter_in_front (first_uk^.next);
                end;
            uknext:= uknext^.next;
          end;
          uk_help:= first_uk;
          first_uk:= first_uk^.next;
          dispose (uk_help);
        end;
    end;

  begin
    for i:=0 to hash_arr do
    init (stations [i]);
    new (new_uk);
    first_uk:= new_uk;
    first_uk^.next:= nil;
    first_uk^.sta:= enter_sta;
    first_uk^.sta^.min_dis:= 0;
    ok:= true;
    while (first_uk <> nil) and ok do
    take_first;
    dispose_ukuk (first_uk);
    write ('#');
  end;

  procedure check_connection (comb: boolean);
  {check actual connection and if is better than best actual connection it will save it}
  var ok:boolean;
      i: numbers;
      uk_help: uksta;

    procedure read_act (uk: uksta);
    begin
      if uk <> enter_sta then
        begin
          read_act (uk^.prev);
          inc (act_stations);
          if uk^.buy then
            begin
              inc (act_go [0]);
              act_go [act_go [0]]:= act_stations;
            end;
          if uk^.num <> uk^.prev^.num then
            inc (act_transfer);
          if uk^.add > act_add then
            act_add:= uk^.add;
          act_dis [act_stations]:= uk^.dis;
        end;
    end;

  begin
    act_add:=0;
    act_stations:=0;
    act_transfer:=-1;
    act_go [0]:= 0;
    read_act (target_sta);
    act_distance:= act_dis [act_stations];
    act_dis [0]:= 0;
    if comb then
      combinations;
    ok:= false;
    if (act_price < minimal_price) then
      ok:= true
    else
      if act_price = minimal_price then
        if (optimal_arrival) then {optimize arrival}
          if target_sta^.arr < minimal_arr [minimal_stations] then
            ok:= true
          else
            begin
              if (target_sta^.arr = minimal_arr [minimal_stations]) then
                if act_transfer < minimal_transfer then
                  ok:= true
            end
        else {optimize transfering}
          if act_transfer < minimal_transfer then
            ok:= true
          else
            if (act_transfer = minimal_transfer) then
              if target_sta^.arr < minimal_arr [minimal_stations] then
                ok:= true;
    if ok then
      begin
        write (' ',act_price,' ');
        minimal_add:= act_add;
        minimal_price:= act_price;
        minimal_stations:= act_stations;
        minimal_distance:= act_distance;
        minimal_transfer:= act_transfer;
        for i:= 1 to act_stations do
        minimal_dis [i]:= act_dis [i];
        for i:= 0 to act_go [0] do
        minimal_go [i]:= act_go [i];
        uk_help:= target_sta;
        for i:=minimal_stations downto 1 do
        begin
          minimal_dep [i]:= uk_help^.dep;
          minimal_arr [i]:= uk_help^.arr;
          minimal_num [i]:= uk_help^.num;
          minimal_name [i]:= uk_help;
          uk_help:=uk_help^.prev;
        end;
        ok:=true;
        while (total_maximal_distance > 0) and ok do
        if ((min_pri [total_maximal_distance] + add_value [1]) > minimal_price) then
          dec (total_maximal_distance)
        else
          ok:= false;
        while (min_pri [total_minimal_distance] + add_value [total_maximal_add]) > minimal_price do
        dec (total_maximal_add);
      end;
  end;

  function find_arr_dep_num
  (sta: ukhour; var uk:ukhour;var max_arr,d1,d2,h1,h2,arr,dep,num: numbers;num_prev: numbers):boolean;
      {true when finds any arrival,departure,train number with departure >= (d1 * 1440 + h1) (!!!or d2 * 1440 + h2
      (with transfer)!!!) and (uk^.arr + d1 * 1440 <= arrival <= max_arr) (!!!or uk^.arr + d2 * 1440 <= arrival <= max_arr
      (with transfer)!!!),
      find arrival,departure,train number of connection with first arrival
      sta is begin of all connections between two stations and uk is actual connection}
      
  var ok,ok2: boolean;

  begin
    ok:= true;
    ok2:= true;
    while (uk <> nil) and ok and ok2 do
    begin
      if uk^.arr + d1 * 1440 > max_arr then
        ok2:= false
      else
        if num_prev <> uk^.num then {transfer}
          begin
            if (uk^.dep >= h2) and (d1 = d2) then
              if go_it (uk^.num,d2) then
                begin
                  ok:= false;
                  num:= uk^.num;
                  dep:= uk^.dep + 1440 * d2;
                  arr:= uk^.arr + 1440 * d2;
                end;
            uk:= uk^.next;
          end
        else {without transfer}
          begin
            if (uk^.dep >= h1) then
              if go_it (uk^.num,d1) then
                begin
                  ok:= false;
                  num:= uk^.num;
                  dep:= uk^.dep + 1440 * d1;
                  arr:= uk^.arr + 1440 * d1;
                end;
            uk:= uk^.next;
          end;
      if (uk = nil) then
        begin
          inc (d1);
          if d2 < d1 then
            begin
              h2:= 0;
              inc (d2);
            end;
          h1:= 0;
          uk:= sta;
        end;
    end;
    find_arr_dep_num:= not ok;
  end;

  function Dijkstra (max_add: numbers; kil: boolean): boolean;
  {kil is true - it finds only the shortest connections with minimal possible distances for each possible stations
  kil is false - it finds one of connections with bald arrival to target station (better is connection with no transfer and
  with arrival <= first possible arrival + min_int - 1}
  var i: numbers;
      first_uk,new_uk: ukfront;
      ok,fun: boolean;

    procedure init (uk: uksta);
    begin
      if uk <> nil then
        begin
          uk^.prev:= nil;
          uk^.dis:= max_distance;
          uk^.help:= maxint;
          uk^.done:= false;
          uk^.add:= 0;
          uk^.arr:= -1;
          uk^.dep:= -1;
          uk^.num:= -1;
          init (uk^.next);
        end;
    end;

    procedure take_first;
    var uknext: ukdirect;
        uk_help: ukfront;
        ok2: boolean;
        i,add,arr,dep,num,dis: numbers;

      function find_all (uknext:ukdirect;max_add,i,arr_prev:numbers;var arr,dep,num,dis,add: numbers):boolean;
      {true when finds arrival,departure,train number,distance,add
       with add <= max_add,with distance <= i,with departure >= arr_pre and with arrival <= last_time}

      var fun,ok1,ok2,ok4,ok5: boolean;
          ar,best: array [1..5] of numbers;
          uk1: ukadds;
          uk2: ukdist;
          uk_find: ukhour;
          day1,day2,hour1,hour2,depa,arri,numb,max_arr: numbers;

      begin
        best[1]:=0;
        fun:=false;
        uk1:=uknext^.adds;
        ok1:=true;
        while (uk1 <> nil) and ok1 do
        if uk1^.add > max_add then
          ok1:= false
        else
          begin
            ar [1]:=uk1^.add;
            uk2:=uk1^.dist;
            ok2:=true;
            if uk2 <> nil then
              if (i >= uk2^.dis) and kil then
                i:= uk2^.dis;
            while (uk2 <> nil) and ok2 do
            if uk2^.dis > i then
              ok2:= false
            else
              begin
                ar [2]:= uk2^.dis;
                day1:= arr_prev div 1440;
                day2:= (arr_prev + min_int) div 1440;
                hour1:= arr_prev mod 1440;
                hour2:= (arr_prev + min_int) mod 1440;
                uk_find:= uk2^.hour;
                max_arr:= last_time;
                ok4:= true;
                ok5:= true;
                while find_arr_dep_num (uk2^.hour,uk_find,max_arr,day1,day2,hour1,hour2,arri,depa,numb,first_uk^.sta^.num)
                and ok4 do
                if (numb = first_uk^.sta^.num) or ok5 then
                  begin
                    if (arri + min_int - 1 < max_arr) and (ok5) then
                      max_arr:= arri + min_int - 1;
                    if not ok5 then
                      ok4:= false;
                    ok5:= false;
                    ar [5]:= arri;
                    ar [4]:= depa;
                    ar [3]:= numb;
                  end;
                if not ok5 then
                  if best [1] = 0 then
                    best:= ar
                  else
                    if (ar [5] < best [5]) then
                      best:= ar;
                uk2:= uk2^.next;
              end;
            uk1:= uk1^.next;
          end;
        if best[1] > 0 then
          begin
            fun:= true;
            arr:= best [5];
            dep:= best [4];
            num:= best [3];
            dis:= best [2];
            add:= best [1];
          end;
        find_all:= fun;
      end;

      procedure enter_in_front (var uk: ukfront);
      begin
        if uk = nil then
          begin
            new (new_uk);
            new_uk^.sta:= uknext^.name;
            new_uk^.next:= nil;
            uk:= new_uk;
          end
        else
          if uk^.sta^.help >= uknext^.name^.help then
            begin
              new (new_uk);
              new_uk^:= uk^;
              uk^.next:= new_uk;
              uk^.sta:= uknext^.name;
            end
          else
            enter_in_front (uk^.next);
      end;

    begin
      ok2:= true;
      while ok2 do
      if (first_uk^.sta^.done) then
        begin
          uk_help:= first_uk;
          first_uk:= first_uk^.next;
          dispose (uk_help);
          ok2 := first_uk <> nil;
        end
      else
        ok2:= false;
      if (first_uk <> nil) then
        begin
          first_uk^.sta^.done:= true;
          uknext:= nil;
          if (first_uk^.sta^.arr > last_time) or (first_uk^.sta^.dis > total_maximal_distance) then
            ok:= false
          else
            uknext:= first_uk^.sta^.direct;
          while uknext <> nil do
          begin
            if kil then
              i:= uknext^.name^.dis - first_uk^.sta^.dis
            else
              i:= total_maximal_distance - first_uk^.sta^.dis;
            if (i >= 0) and not (uknext^.name^.done) then
              if find_all (uknext,max_add,i,first_uk^.sta^.arr,arr,dep,num,dis,add) then
                begin
                  if not kil then
                    uknext^.name^.help:= arr
                  else
                    uknext^.name^.help:= dis + first_uk^.sta^.dis;
                  uknext^.name^.prev:= first_uk^.sta;
                  uknext^.name^.dis:= dis + first_uk^.sta^.dis;
                  uknext^.name^.arr:= arr;
                  uknext^.name^.dep:= dep;
                  uknext^.name^.num:= num;
                  uknext^.name^.add:= add;
                  enter_in_front (first_uk^.next);
                end;
            uknext:= uknext^.next;
          end;
          uk_help:= first_uk;
          first_uk:= first_uk^.next;
          dispose (uk_help);
        end;
    end;

  begin
    fun:= false;
    for i:=0 to hash_arr do
    init (stations [i]);
    new (new_uk);
    first_uk:= new_uk;
    first_uk^.sta:= enter_sta;
    first_uk^.next:= nil;
    if not kil then
      first_uk^.sta^.help:= first_time - min_int
    else
      first_uk^.sta^.help:= 0;
    new_uk^.sta^.arr:= first_time - min_int;
    new_uk^.sta^.dis:= 0;
    while (not target_sta^.done) and (first_uk <> nil) do
    take_first;
    if (target_sta^.done) then
      begin
        fun:= true;
        check_connection (true);
      end;
    ok:= true;
    while ok and (first_uk <> nil) do
    take_first;
    dispose_ukuk (first_uk);
    write ('#');
    Dijkstra:= fun;
  end;

  procedure do_Dijkstra;
  var i,j: numbers;
  begin
    writeln ('SEEKING BEST CONNECTION: ');
    write ('1. Dijkstra: ');
    Dijkstra_inv (total_maximal_add);
    {it finds total minimal distances from stations to target station}
    total_minimal_distance:= enter_sta^.min_dis_2; {it is from last Dijkstra}
    i:=1;
    while (i <= (add_numbers [0])) do
    begin
      help:= min_pri [total_minimal_distance] + add_value [i];
      if (help <= minimal_price) then
        if Dijkstra (i,false) then
          Dijkstra (i,true);
      inc (i);
    end;
    if total_maximal_add < add_numbers [0] then
      Dijkstra_inv (total_maximal_add);
    {it finds total minimal distances from station to target station only with connections with add <= total_maximal_add}
    Dijkstra_nor (total_maximal_add);
    {it finds total minimal distances from enter station to this station only with connections with add <= total_maximal_add}
    writeln;
    i:= 0;
    for j:= 0 to hash_arr do
    begin
      uk_sta:= stations [j];
      while uk_sta <> nil do
      begin
       if (uk_sta^.min_dis + uk_sta^.min_dis_2 <= total_maximal_distance) then
          begin
            inc (i);
            uk_sta^.pos:= true;
            {initialize for main seeking}
            uk_sta^.buy:= false;
            uk_sta^.prev:= nil;
            uk_sta^.done:= false;
            uk_sta^.dis:= max_distance;
            uk_sta^.add:= 0;
            uk_sta^.arr:= -1;
            uk_sta^.dep:= -1;
            uk_sta^.num:= - maxint;
          end
        else
          uk_sta^.pos:= false;
        uk_sta:= uk_sta^.next;
      end;
    end;
    write ('Distance of best connection is in this interval: ');
    writeln (total_minimal_distance,' - ',total_maximal_distance,' kilometres');
    write ('Price of best connection is in this interval: ',min_pri [total_minimal_distance] + add_value [1]);
    writeln (' - ',pri [total_maximal_distance] + add_value [total_maximal_add],' ',currency);
    writeln ('Number of possible stations for seeking: ',i);
    {it is number of stations through which is possible to go in best connection}    
  end;

  function positive (i:integer): integer;
  begin
    if i < 0 then
      positive:= 0
    else
      positive:=min_pri [i];
  end;

  procedure main_seek;
  var i,dist:numbers;

    procedure seek (where,from: uksta; dep,arr,add,num,dis,kil_lef,act_pri: numbers);
    {where,from,departure,arrival,add,train_number,distance,kilometres left on actual ticket,actual price}
    var ok1,ok2: boolean;
        uk0: ukdirect;
        uk1: ukadds;
        uk2: ukdist;
        uk3: ukhour;
        day1,day2,hour1,hour2,max_arr,a,d,n: numbers;

        procedure seek_more;
        var i: integer;

        begin
          uk1:= uk0^.adds;
          day1:= arr div 1440;
          day2:= (arr + min_int) div 1440;
          hour1:= arr mod 1440;
          hour2:= (arr + min_int) mod 1440;
          max_arr:= last_time;
          ok1:= true;
          while (uk1 <> nil) and ok1 do
          if uk1^.add > total_maximal_add then
            ok1:= false
          else
            begin
              uk2:=uk1^.dist;
              uk3:= uk2^.hour;
              ok2:=true;
              while (uk2 <> nil) and ok2 do
              if uk2^.dis + dist > total_maximal_distance then
                ok2:= false
              else
                begin
                  while find_arr_dep_num (uk2^.hour,uk3,max_arr,day1,day2,hour1,hour2,a,d,n,where^.num) do
                  begin
                    if (a + min_int - 1) < max_arr then
                      max_arr:= a + min_int - 1;
                    if kil_lef >= uk2^.dis then {no buying new ticket}
                      seek (uk0^.name,where,d,a,uk1^.add,n,uk2^.dis,kil_lef - uk2^.dis,act_pri)
                    else
                      begin {buying new ticket}                   
                        for i:= (kil [0] - 1) downto 1 do
                        if ((kil [i] - uk2^.dis) >= 0) and ((act_pri + add_value [act_add] + pri [kil[i]]) <= minimal_price) then                                                       
                          begin
                            where^.buy:=true; 
                            seek (uk0^.name,where,d,a,uk1^.add,n,uk2^.dis,kil [i] - uk2^.dis,act_pri + pri [kil [i]]);                        
                            where^.buy:=false; 
                          end;
                      end;
                  end;
                  uk2:= uk2^.next;
                  if uk2 <> nil then
                    uk3:= uk2^.hour;
                end;
              uk1:= uk1^.next;
            end;
        end;

        function check_pos: boolean;
        var fun,ok: boolean;
            k,act_pr: integer;
            uk,uk_new,uk_pred: ukcon;

          function same (i,j: integer): integer;
          begin
            if i = j then
              same:=0
            else
              same:=min_int;
          end;

        begin
          act_pr:= act_pri + add_value [act_add];
          fun:= true;
          k:= kil_lef;
          while (k <= max_distance) and fun do
          begin
            ok:= true; {price possibility}
            uk:= where^.arrivals [k];
            while ok and fun do
            if uk = nil then
              ok:= false
            else
              if uk^.pri > act_pr then
                ok:= false
              else
                if uk^.arr + same (num,uk^.num) <= arr then
                  fun:= false
                else
                  uk:= uk^.next;
            inc (k);
          end;
          if fun then
            begin
              if where^.arrivals [kil_lef] = nil then
                begin
                  new (uk_new);
                  where^.arrivals [kil_lef]:= uk_new;
                  uk_new^.next:=nil;
                  uk_new^.pri:=act_pr;
                  uk_new^.arr:=arr;
                  uk_new^.num:=num;
                end
              else
                begin
                  uk:= where^.arrivals [kil_lef];
                  ok:= true;
                  while uk <> nil do
                  if ok then {add item}
                    begin
                      if ((uk^.pri > act_pr) or ((uk^.pri = act_pr) and (uk^.arr >= arr))) then {save}
                        begin
                          new (uk_new);
                          uk_new^:= uk^;
                          uk^.next:=uk_new;
                          uk^.pri:=act_pr;
                          uk^.arr:=arr;
                          uk^.num:=num;
                          ok:= false;
                          uk_pred:=uk;
                          uk:=uk^.next;
                        end
                      else
                        if uk^.next = nil then
                          begin
                            new (uk_new);
                            uk^.next:= uk_new;
                            uk_new^.next:=nil;
                            uk_new^.pri:=act_pr;
                            uk_new^.arr:=arr;
                            uk_new^.num:=num;
                            ok:= false;
                            uk:=nil;
                          end
                        else
                          uk:= uk^.next;
                    end
                  else {only check + dispose}
                    if ((uk^.arr) >= (same (uk^.num,num) + arr)) then
                      begin
                        uk_pred^.next:=uk^.next;
                        dispose (uk);
                        uk:= uk_pred^.next;
                      end
                    else
                      begin
                        uk_pred:=uk;
                        uk:=uk^.next;
                      end;
                end;
            end;
          check_pos:= fun;
        end;

    begin
      if (not where^.done) and (where^.pos) then
        if arr <= last_time then
          if (dist + dis - where^.min_dis <= total_maximal_distance - total_minimal_distance) then
            if (where^.min_dis_2 <= total_maximal_distance - dist - dis) then
              if act_pri + add_value [act_add] + positive (where^.min_dis_2 - kil_lef) <= minimal_price then
                if check_pos then
                  begin                    
                    dist:= dist + dis;                    
                    where^.prev:= from;
                    where^.done:= true;
                    where^.arr:= arr;
                    where^.dep:= dep;
                    where^.dis:= dist;
                    where^.add:= add;
                    if from^.add > where^.add then
                      where^.add:= from^.add;
                    where^.num:= num;
                    if where = target_sta then
                      begin                        
                        target_sta^.buy:= true;
                        act_price:= act_pri + add_value [where^.add];                     
                        check_connection (false);
                        target_sta^.buy:= false;
                      end
                    else
                      begin
                        uk0:= where^.direct;
                        while (uk0 <> nil) do
                        begin
                          seek_more;
                          uk0:= uk0^.next;
                        end;
                      end;                    
                    where^.done:= false;
                    dist:= dist - dis;
                  end;      
    end;

  begin
    dist:= 0;    
    write ('2. MAIN SEEKING: ');
    for i:= kil [0] downto 1 do
    begin
      act_add:=1;
      target_sta^.add:=1;
      seek (enter_sta,target_sta,-1,first_time - min_int,1,maxint,0,kil [i],pri [kil [i]]);      
      write ('*',i,'*');
    end;    
  end;

begin
  init_var;
  read_tt;
  do_Dijkstra;
  if total_minimal_distance <= max_distance then {connection exists}
    main_seek;
end;

procedure write_connection;
var i,l,k,num,kil,pric,add_max,add_act,add_num: numbers;
    help: string;
    ok: boolean;
    uk_notes,uk_first,uk_new,uk_new2: uknotes;

  procedure write_notes (uk_notes: uknotes);
  var x: string;
      i,l: integer;
  begin
    while uk_notes <> nil do
    begin
      x:= uk_notes^.note;
      while length (x) > 0 do
        if length (x) <= 80 then
          begin
            if length (x) < 80 then
              writeln (x)
            else
              write (x);
            x:= '';
          end
        else
          begin
            l:=80;
            for i:= 1 to 80 do
            if (x[i] = ' ') or (x [i] = ',') then
            l:= i;
            if l < 80 then
              writeln (copy (x,1,l))
            else
              write (copy (x,1,l));
            delete (x,1,l);
          end;
        uk_notes:= uk_notes^.next;
    end;
  end;

  procedure find_notes;
  var ok: boolean;
      i: numbers;
  begin
    uk_first:= nil;
    minimal_num [0]:= minimal_num [1] - 1;
    for i:= 1 to minimal_stations do
    if minimal_num [i] <> minimal_num [i -1] then
      begin
        uk_notes:= all_notes [minimal_num [i]];
        while uk_notes <> nil do
        begin
          uk_new:= uk_first;
          ok:= true;
          while (uk_new <> nil) and ok do
          if uk_new^.note = uk_notes^.note then
            ok:= false
          else
            if uk_new^.note < uk_notes^.note then
              begin
                new (uk_new2);
                uk_new2^:=uk_new^;
                uk_new^.next:= uk_new2;
                uk_new^.note:= uk_notes^.note;
                ok:= false;
              end
            else
              if uk_new^.next <> nil then
                uk_new:= uk_new^.next
              else
                begin
                  new (uk_new2);
                  uk_new2^.note:= uk_notes^.note;
                  uk_new2^.next:= nil;
                  uk_new^.next:= uk_new2;
                  ok:= false;
                end;
          if ok then
            begin
              new (uk_new2);
              uk_new2^.note:= uk_notes^.note;
              uk_new2^.next:= nil;
              uk_first:= uk_new2;
            end;
          uk_notes:= uk_notes^.next;
        end;
      end;
  end;

begin
  if minimal_stations = 0 then
    begin
      writeln;
      writeln ('Connection from ',enter_station,' to ',target_station,' does not exist');
      halt;
    end;
  minimal_num [minimal_stations + 1]:= maxint;
  minimal_num [0]:= minimal_num [1];
  minimal_dis [0]:= 0;
  writeln;
  writeln;
  writeln ('Total price: ',minimal_price,' ',currency);
  if minimal_go [0] > 1 then
    writeln ('(Total price for direct this conection: ',pri[minimal_distance] + add_value[minimal_add],' ',currency,')');
  if add_value [minimal_add] <> 0 then
    writeln ('Add: ',add_value [minimal_add],' ',currency);
  if (minimal_dep [1] div 1440) = (minimal_arr [minimal_stations] div 1440) then
    writeln ('Date of departure and arrival: ',convert_int_date (minimal_dep [1]))
  else
    begin
      writeln ('Date of departure: ',convert_int_date (minimal_dep [1]));
      writeln ('Date of arrival: ',convert_int_date (minimal_arr [minimal_stations]));
    end;
  i:= minimal_arr [minimal_stations] - minimal_dep [1];
  help:= 'minute';
  if i <> 1 then
    help:= help + 's';
  writeln ('Total time of whole connection: ',i,' ',help);
  help:= 'kilometre';
  if minimal_distance <> 1 then
    help:= help + 's';
  writeln ('Total distance of whole connection: ',minimal_distance,' ',help);
  if not write_def then
    begin
      add_max:=0;
      add_num:= minimal_stations + 1;
      ok:= false;
      for i:=1 to minimal_stations do
      begin
        add_act:= find_add (train_num [minimal_num[i]]);
        if add_act > add_max then
          begin
            add_max:= add_act;
            if (add_value [add_max] > 0) and (not ok) then
              begin
                add_num:= i;
                ok:= true;
              end;
         end;
      end;
      pric:= pri [minimal_dis [minimal_go [1]]];
      if add_num = 1 then
        pric:= pric + add_value [add_max];
      writeln;
      writeln ('Connection ''ALL STATIONS''');
      writeln ('( Departure / Station / Arrival / Station / Distance / Price / Train number )');
      writeln;
      minimal_name [0]:= enter_sta;
      for i:=1 to minimal_stations do
      begin
        kil:= minimal_dis [i];
        write (convert_int_str (minimal_dep [i]),' ');
        write (minimal_name [i - 1]^.name,#9);
        write (convert_int_str (minimal_arr [i]),' ');
        write (minimal_name [i]^.name,#9);
        write (kil,' ');
        write (pric,' ');
        for l:=1 to minimal_go [0] do
        if (i = minimal_go [l]) then
          pric:= pric + pri [minimal_dis [minimal_go [l + 1]] - minimal_dis [minimal_go[l]]];
        if add_num = i + 1 then
          pric:= pric + add_value [add_max];
        writeln (train_num [minimal_num [i]]);
        if (minimal_num [i] <> minimal_num [i + 1]) then
          begin
            write_notes (all_notes [minimal_num [i]]);
            writeln;
          end;
      end;
    end
  else
    begin
      if write_note_def then
        find_notes;
      if pos ('1',type_def) > 0 then
        begin
          writeln;
          writeln ('Connection ''BUYING TICKETS''');
          writeln ('( Station / Arrival / Departure / Distance * / Price * )');
          writeln ('( * - it means information about this and previous stations )');
          writeln;
          writeln ('Station                                                     Arr   Dep  Dis  Pri');
          write (enter_sta^.name,' ');
          for l:= 55 downto length (enter_sta^.name) do
          write ('.');
          write (' ':6);
          k:=0;
          kil:= minimal_dis [minimal_go [1]];
          for i:=1 to minimal_go [0] do
            begin
              write (convert_int_str (minimal_dep [k + 1]):6);
              if i = 1 then
                writeln ('0':5,' ':5)
              else
                writeln (kil:5,pri [kil]:5);
              write (minimal_name [minimal_go [i]]^.name,' ');
              for l:= 55 downto length (minimal_name [minimal_go [i]]^.name) do
              write ('.');
              write (convert_int_str (minimal_arr [minimal_go [i]]):6);
              if i > 1 then
                kil:= minimal_dis [minimal_go [i]] - minimal_dis [k];
              k:= minimal_go [i];
            end;
          writeln (' ':6,kil:5,pri [kil]:5);
        end;
      if pos ('2',type_def) > 0 then
        begin
          writeln;
          writeln ('Connection ''TRANSFERING''');
          writeln ('( Station / Arrival / Departure / Distance )');
          writeln;
          writeln ('Station                                                          Arr   Dep  Dis');
          write (enter_sta^.name,' ');
          for l:= 60 downto length (enter_sta^.name) do
          write ('.');
          write (' ':6);
          k:=1;
          kil:= 0;
          num:= minimal_num [1];
          i:=1;
          while (i <= minimal_stations) do
          begin
            while num = minimal_num [i + 1] do
            inc (i);
            write (convert_int_str (minimal_dep [k]):6);
            if k = 1 then
              writeln ('0':5)
            else
              writeln (kil:5);
            write (minimal_name [i]^.name,' ');
            for l:= 60 downto length (minimal_name [i]^.name) do
            write ('.');
            write (convert_int_str (minimal_arr [i]):6);
            kil:= minimal_dis [i];
            inc (i);
            num:= minimal_num [i];
            k:= i;
          end;
          writeln (' ':6,kil:5);
        end;
      if pos ('3',type_def) > 0 then
        begin
          writeln;
          writeln ('Connection ''ALL STATIONS''');
          writeln ('( Station / Arrival / Departure / Distance / Transfer / Buy ticket )');
          writeln ('( * - transfer in this station, x - buy ticket to this station )');
          writeln;
          writeln ('Station                                                  Arr   Dep  Dis Tra Buy');
          write (enter_sta^.name,' ');
          for l:= 52 downto length (enter_sta^.name) do
          write ('.');
          write (' ':6);
          kil:=0;
          help:= '';
          for i:=1 to minimal_stations do
          begin
            write (convert_int_str (minimal_dep [i]):6);
            writeln (kil:5,help);
            write (minimal_name [i]^.name,' ');
            for l:= 52 downto length (minimal_name [i]^.name) do
            write ('.');
            write (convert_int_str (minimal_arr [i]):6);
            if (minimal_num [i] = minimal_num [i + 1]) or (i = minimal_stations) then
              help:= '    '
            else
              help:= '   *';
            ok:= false;
            for l:= 1 to minimal_go [0] do
            if i = minimal_go [l] then
              ok:= true;
            if ok then
              help:= help + '   x'
            else
              help:= help + '    ';
            kil:= minimal_dis [i];
          end;
          writeln (' ':6,kil:5,help);
        end;
        write_notes (uk_first);
    end;  
end;

{MAIN PROGRAM}

begin
  if paramcount = 0 then
    display_help
  else
    begin
      prepare_data; {create or read needed data without .tt}
      if path_write <> '' then
        save_data; {save needed data without .tt}
      find_connection; {read .tt,seek best connection}
      write_connection; {write best connection}
    end;
end.