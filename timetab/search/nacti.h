#define c1 89   /* Constants for hash function */
#define c2 29
#define c3 97
#define c4 9  
#define LENGTH 10000   /* LENGTH of hash table */
#define LOL 10240

struct vlaky;

struct vlak;

struct  zaznam   /* The necessary things for a city */
{
	int prijezd,km,den;
	char *nazev;
	struct zaznam *predchozimesto;
	struct zaznam *dalsimesto;
	struct  vlaky *spojeni;
	int prestupy;
	struct zaznam *dalsi;
	struct zaznam *dalsiprestup;
	struct vlak *prijel;
};

struct spojzaznamy
{
	struct zaznam *mesto;
	struct spojzaznamy *dalsi;
};

struct cas			/* Time and distance between stations */
{
	int posun;
	int posun2;
	int km;
	struct cas *dalsi;
};

struct vlak
{
	struct vlak *dalsi;
	struct zaznam *odkud;

	char *jmeno;
	char *remark;
	char *jede;
	int odjezd;
	int pouzito;
	struct spojzaznamy *mesta;
	struct cas *posun;
};

struct vlaky
{
	struct vlak *spoj;
	struct  vlaky *dalsi;
};

struct hash
{
	struct zaznam *pole[LENGTH];
};

void nastav(struct vlak *v)
{	
	v->jmeno=NULL;
	v->odkud=NULL;
	v->odjezd=0;
	v->mesta=NULL;
	v->posun=NULL;
	v->jede=NULL;
	v->dalsi=0;
	v->remark=0;
	v->pouzito=0;
}

void pridejcas1(int pposun1,int pposun2, int km,struct vlak *v)
{
	if (v->posun==NULL) 
	{
		v->posun=malloc(sizeof(struct cas));
		v->posun->posun=pposun1;
		v->posun->posun2=pposun2;
		v->posun->km=km;
		v->posun->dalsi=NULL;
	}
	else
	{
		struct cas *r=v->posun;
		while (r->dalsi!=NULL) 
			r=r->dalsi;
		r->dalsi=malloc(sizeof(struct cas));		
		r->dalsi->posun=pposun1;
		r->dalsi->posun=pposun1;
		r->dalsi->posun2=pposun2;
		r->dalsi->km=km;
		r->dalsi->dalsi=NULL;
	}
}



int hashfun(char *nazev)
{
	int i,k=0;
	for(i=0;i<strlen(nazev);++i)
	{
		k+=nazev[i]*c1*c2;
		k+=(i*i)*nazev[i]%c3*c4;
	}
	if(k<0) 
		k=-k;
	k=k%LENGTH;
	return k;

}



int pridejmesto(char *nazev,struct hash *seznam,struct vlak *spoj)
{
	struct zaznam *t=seznam->pole[hashfun(nazev)];
	while (t!=0)
    	if (strcmp(t->nazev,nazev)==0) break;
		else t=t->dalsi;
	
	if (spoj->mesta==NULL)
	{
		spoj->mesta=malloc(sizeof(struct spojzaznamy));
		spoj->mesta->mesto=t;
		spoj->mesta->dalsi=NULL;
	}
	else
	{
		struct spojzaznamy *r=spoj->mesta;
		while (r->dalsi!=NULL) 
			r=r->dalsi;
		r->dalsi=malloc(sizeof(struct spojzaznamy));
		r->dalsi->mesto=t;
		r->dalsi->dalsi=NULL;
		
	}
	return 1;
}







struct zaznam *jev(char *mesto,struct hash *seznam)     /* Is the city in this list? */
{
	int kod=hashfun(mesto);
	if (mesto==NULL) return 0;

	if (seznam==NULL) return 0;

	if (seznam->pole[kod]==NULL) return 0;
	else
	{
		struct zaznam *r=seznam->pole[kod];
		while (r!=NULL) 
		{
			if(strcmp(r->nazev,mesto)==0) return r;
			else r=r->dalsi;
		}
	}
	return 0;
}


struct zaznam* pridejdoseznamu(char *mesto,struct hash *seznam,struct vlak *spoj)   /* Add city to hash table */
{
	struct zaznam *kam=jev(mesto,seznam);
	if (mesto==NULL) return 0;
	if (kam==NULL)
	{
		int kod=hashfun(mesto);
		if (seznam->pole[kod]==NULL) 
		{
			seznam->pole[kod]=malloc(sizeof(struct zaznam));
			seznam->pole[kod]->nazev=malloc(strlen(mesto)+1);
			strcpy(seznam->pole[kod]->nazev,mesto);
			seznam->pole[kod]->den=0;
			seznam->pole[kod]->spojeni=malloc(sizeof(struct vlaky));
			seznam->pole[kod]->dalsi=NULL;
			seznam->pole[kod]->dalsiprestup=NULL;
			seznam->pole[kod]->spojeni->spoj=spoj;
			seznam->pole[kod]->spojeni->dalsi=NULL;	
			seznam->pole[kod]->prijezd=-1;
			seznam->pole[kod]->predchozimesto=NULL;
			seznam->pole[kod]->prestupy=-1;
			seznam->pole[kod]->prijel=NULL;
			seznam->pole[kod]->dalsimesto=NULL;
			return seznam->pole[kod];
		}
		else
		{
			
			struct zaznam *r=malloc(sizeof(struct zaznam));
     		r->nazev=malloc(strlen(mesto)+1);
			strcpy(r->nazev,mesto);
			r->dalsi=seznam->pole[kod];
			r->den=0;
			r->dalsiprestup=NULL;
			r->spojeni=malloc(sizeof(struct vlaky));
			r->spojeni->spoj=spoj;
			r->spojeni->dalsi=NULL;
			r->prijezd=-1;
			r->predchozimesto=NULL;
			r->prestupy=-1;
			r->prijel=NULL;
			r->dalsimesto=NULL;
			seznam->pole[kod]=r;
			return seznam->pole[kod];
		}
	}
	else
	{ 
		struct  vlaky *r=kam->spojeni;
		if (r==NULL)
		{
			r=malloc(sizeof(struct vlaky));
			r->dalsi=NULL;
			r->spoj=spoj;
		}
		else
		{
			struct  vlaky *novy=malloc(sizeof(struct vlaky));
			novy->dalsi=r;
			novy->spoj=spoj;
			kam->spojeni=novy;
		}
		return kam;
	}
		return 0;
}





struct hash *nactivlaky()
{
	struct hash *tabulka;
	int i;
	char *radka=malloc(LOL);
	char *temp=malloc(LOL);
	int j=0;
	int hod=0,mezi=0,min=0;
	int hod2=0,min2=0,hod1,min1=0,km,posun;
	struct vlaky *q;
	struct vlak *p=malloc(sizeof(struct vlak));

	tabulka=malloc(sizeof(struct hash));

	for(i=0;i<LENGTH;i++)
	{
		tabulka->pole[i]=NULL;
	}

	nastav(p);

	q=NULL;
	while(!feof(stdin))
	{
		i=0;
		while (radka[0]!='#'&&!feof(stdin)) 
			fgets(radka,LOL,stdin);

		if(feof(stdin))
			break;

		i=2;
		for(i=2;radka[i];i++)
			temp[i-2]=radka[i];

		temp[i-3]=0;
		p->jmeno=malloc(strlen(temp)+1);
		strcpy(p->jmeno,temp);	
		i=0;
		fgets(radka,LOL,stdin);
		hod=radka[1]-'0';
		if (radka[2]==':') 
		{
			min=(radka[3]-'0')*10+radka[4]-'0';
			p->odjezd=hod*60+min;
			i=5;
		}
		else 
		{
			hod=hod*10+radka[2]-'0';
			min=(radka[4]-'0')*10+radka[5]-'0';
			p->odjezd=hod*60+min;
			i=6;
		}

		if(hod!=25)
			p->odjezd=hod*60+min;
		else p->odjezd=-1;

		posun=p->odjezd;
		i++;
		for(;radka[i]!=9;i++)
			;
		i++;
		for(;radka[i]!=9;i++)
			;
		i++;

		for(j=0;i<strlen(radka)-1;i++,j++)
		{
			temp[j]=radka[i];
		}
		temp[j]=0;

		p->odkud=pridejdoseznamu(temp,tabulka,p);
		i=0;

		fgets(radka,LOL,stdin);
		while (radka[0]!='R'&&radka[0]!='B'&&radka[0]!='#'&&strlen(radka)>3)
		{
			i=hod=min=mezi=hod1=hod2=km=0;
			switch(radka[0])
			{
			case '+':
				{
					i=1;
					while(radka[i]!=9)
					{
							hod1=hod1*10+radka[i]-'0';
							i++;
					}
					posun+=hod1;
					break;
				}

			case 9:
				{
					hod1=0;i=0;break;
				}
			default:
				{
					hod1=radka[0]-'0';
					if (radka[1]==':')
					{
						min1=(radka[2]-'0')*10+radka[3]-'0';
						i=4;
					}
					else
					{
						hod1=hod1*10+radka[1]-'0';
						min1=(radka[3]-'0')*10+radka[4]-'0';
						i=5;
					}
					if(hod1*60+min1-(posun)>=0)
					hod1=hod1*60+min1-(posun);
					else hod1=hod1*60+min1+24*60-posun;

					posun+=hod1;
					break;
				}
			}
			i++;
			switch(radka[i])
			{
			case '+':
				{
					i++;
					while(radka[i]!=9)
					{
							hod2=hod2*10+radka[i]-'0';
							i++;
					}
					posun+=hod2;
					break;
				}

			case 9:
				{
					hod2=0;break;
				}
			default:
				{
					hod2=radka[i]-'0';
					if (radka[i+1]==':')
					{
						min2=(radka[i+2]-'0')*10+radka[i+3]-'0';
						i+=4;
					}
					else
					{
						hod2=hod2*10+radka[i+1]-'0';
						min2=(radka[i+3]-'0')*10+radka[i+4]-'0';
						i+=5;
					}
					if(hod2*60+min2-(posun)>=0)
					hod2=hod2*60+min2-(posun);
					else hod2=hod1*60+min1+24*60-posun;
					posun+=hod2;
					break;
				}
			}
			i+=1;
			while(radka[i]!=9) i++;
			i++;
			for(;radka[i]!=9;i++)
				km=km*10+radka[i]-'0';
			if(hod1==0)
				pridejcas1(hod2,hod1,km,p);
			else
				pridejcas1(hod1,hod2,km,p);
			i+=1;

			j=0;
			while (i<strlen(radka)-1)
			{
				temp[j]=radka[i];
				i++;j++;
			}
			temp[j]=0;			
			pridejdoseznamu(temp,tabulka,p);
			pridejmesto(temp,tabulka,p);
			fgets(radka,LOL,stdin);
		}
		temp[0]=0;
		

		for(;radka[0]!='#'&&!feof(stdin)&&(radka[0]=='B'||radka[0]=='R'||radka[0]=='\n');fgets(radka,LOL,stdin))
		{
			if(radka[0]=='R')
			{
			  if(radka[strlen(radka)-1]==10) radka[strlen(radka)-1]=0;
				if (p->jede==NULL) 
				{
					p->jede=malloc(strlen(radka)+1);
					strcpy(p->jede,"");
				} 
				else 
				{
					p->jede=realloc(p->jede,strlen(p->jede)+1+strlen(radka));
				}
				strcat(p->jede,radka+1);
			}
			
			else
			{
				if (p->remark==NULL) 
				{
					p->remark=malloc(strlen(radka)+1);
					strcpy(p->remark,"");
				} 
				else 
				{
					p->remark=realloc(p->remark,strlen(p->remark)+1+strlen(radka));
				}
				strcat(p->remark,radka+1);
			}

	
		}
       
		p=malloc(sizeof(struct vlak));
		nastav(p);		
	}
	return tabulka;
	
}




