#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>
#include <errno.h>

#include "../exists.c"
#include "nacti.h"


#define tolerance 6

int main(int argc, char *argv[ ], char *envp[ ] )
{
	struct hash *pokus;
	struct zaznam *t;
	struct zaznam *posledni;
	struct zaznam *aktualni;
	struct zaznam *p,*ppres,*pomoc;

	int koneccyklu,tmp;
	int kod,posun,pocetprestupu,i;
	int plus=0,km=0;
	char *odkud=NULL;
	int optionu,pposun,pden;
	char *kam=NULL;
	char *pres;	

	struct vlaky *v;
	struct vlak *u;
	struct tm date,pdate;
	struct spojzaznamy *q,*pq;
	struct cas *r,*posuny;

	i=1;
	if(argv[i][0]=='-'&&argv[i][1]=='u') {optionu=1;argv+=1;}
	else optionu=0;

	odkud=argv[1];
	kam=argv[4];
	date.tm_hour=atoi(argv[i]);
	date.tm_min=atoi(argv[i]);
    if(sscanf(argv[2], "%d.%d.%d", &date.tm_mday, &date.tm_mon, &date.tm_year)!=3)
	{
		printf("Wrong date format");
		return 3;
	}
	
	if (sscanf(argv[3], "%d:%d", &date.tm_hour, &date.tm_min)!=2)
	{
		printf("Wrong time format\n");
		return 3;
	}

    date.tm_sec=0;
    date.tm_mon--; 
	date.tm_year-=1900;
    mktime(&date);
  	date.tm_mon++;
   	if (date.tm_wday==0) date.tm_wday=7;
	
	
	pokus=nactivlaky();

	kod=hashfun(odkud);
	t=pokus->pole[kod];
	while(t!=NULL)
		if(!strcmp(odkud,t->nazev)) break;
	    else t=t->dalsi;
	if(t==NULL) 
	{
		printf("Error: Unknown city %s.\n",odkud);
		exit(3);
	}
	kod=hashfun(kam);
	t=pokus->pole[kod];
	while(t!=NULL)
		if(!strcmp(kam,t->nazev)) break;
	    else t=t->dalsi;
	if(t==NULL) 
	{
		printf("Error: Unknown city %s.\n",odkud);
		exit(3);
	}	

	posun=0;
	kod=hashfun(odkud);
	t=pokus->pole[kod];
	while (t!=NULL)
	if (strcmp(t->nazev,odkud)==0) break;
	else t=t->dalsi;
	aktualni=posledni=t;
	aktualni->prestupy=0;
	t->prijezd=date.tm_hour*60+date.tm_min;
	pocetprestupu=1;

	koneccyklu=0;
	v=t->spojeni;
	pres=odkud;
	pocetprestupu=1;


while(pres!=NULL)     // I work with the station where I can change train
{
	posun=0;
	pdate=date;
	kod=hashfun(pres);
	t=pokus->pole[kod];
	while (t!=0)
    	if (strcmp(t->nazev,pres)==0) break;
		else t=t->dalsi;


	if(pocetprestupu<=t->prestupy)
	{
		if(koneccyklu) break; 
		pocetprestupu++;
	}


	v=t->spojeni;
	while(v!=NULL)
	{
		u=v->spoj;
		if(u->pouzito)
		{
			v=v->dalsi;
			continue;
		}
		posun=0;

		plus=0;

		km=0;
		pposun=0;

		q=u->mesta;
		r=u->posun;
	
		if(strcmp(u->odkud->nazev,pres)) 
		{
			while(strcmp(q->mesto->nazev,pres))          // Searching for my town
			{
				plus+=r->posun+r->posun2;
				q=q->dalsi;r=r->dalsi;
			}
			if(q==NULL) 
			{
				v=v->dalsi;
				continue;
			}
			q=q->dalsi;
			plus+=r->posun;
			pposun=r->posun2;
			r=r->dalsi;
			if(q==NULL) 
			{
				v=v->dalsi;
				continue;
			}
		}


	if((u->odjezd+plus)%1440-t->prijezd<0) pden=1; 
	else pden=0;

	pdate.tm_mday+=t->den-(u->odjezd+plus)/1440+pden;
	pdate.tm_mon--; 
    mktime(&pdate);
  	pdate.tm_mon++;
   	if (pdate.tm_wday==0) pdate.tm_wday=7;

	if(exists(u->jede,&pdate)=='N') 	//Now I try if the train goes this day
	{
		v=v->dalsi;
		continue;
	}
	if(u->odjezd==-1) u->odjezd=t->prijezd;

	if((t->prijezd+tolerance*60)<1440)   // Is this near midnight?
	{									 // No it isn't - is the departure time between arrival time and limit?
         if(((u->odjezd+plus)%1440<t->prijezd)||((u->odjezd+plus)%1440>t->prijezd+tolerance*60))
		{
			v=v->dalsi;
			continue;
		}
	}									// Yes, it is - is the departure time greater than arrival time or less than limit?
	else if(((u->odjezd+plus)%1440<t->prijezd)&&((u->odjezd+plus)%1440>(t->prijezd+tolerance*60)%1440))
	{
   		v=v->dalsi;
	   	continue;
	}


	u->pouzito=1;

	while(q!=NULL)
	{
		posun+=pposun;
		posun+=r->posun;
		km+=r->km;
	
		
			
			if(q->mesto->prestupy==-1&&!koneccyklu)               // I add this city to list, I haven't been there before
			{
				q->mesto->predchozimesto=t;
				q->mesto->prijel=u;
				q->mesto->prijezd=((u->odjezd+plus)%1440+posun)%1440;
				q->mesto->km=km;
				q->mesto->prestupy=pocetprestupu;
				q->mesto->den=((u->odjezd+plus)%1440+posun)/1440+t->den+pden;
				posledni->dalsiprestup=q->mesto;
				posledni=posledni->dalsiprestup;
				posledni->dalsiprestup=NULL;
			}
			
			else if((pocetprestupu==q->mesto->prestupy)&&((((u->odjezd+plus)%1440+posun)%1440+(t->den+pden)*1440)<(q->mesto->prijezd+q->mesto->den*1440)))
			{
				// Found better connection, so I change necessary things
				q->mesto->prijezd=((u->odjezd+plus)%1440+posun)%1440;
				q->mesto->predchozimesto=t;
				q->mesto->prijel=u;
				q->mesto->km=km;
				q->mesto->den=((u->odjezd+plus)%1440+posun)/1440+t->den+pden;
			}
			if(strcmp(q->mesto->nazev,kam)==0)  // Connection found, now finish only this wave
			{
				koneccyklu=1;
			}
		
		pposun=r->posun2;
		r=r->dalsi;
		q=q->dalsi;
	}
	v=v->dalsi;
	}
	aktualni=aktualni->dalsiprestup;
	if(aktualni!=NULL) 
	{
		pres=aktualni->nazev;
	}
	else pres=NULL;
   
}

if (!koneccyklu)
{
	printf("Connection not found!\n");
	exit(3);
}
plus=0;
kod=hashfun(kam);
p=pokus->pole[kod];
while(strcmp(p->nazev,kam))
	p=p->dalsi;

pomoc=p;
while(strcmp(pomoc->nazev,odkud))
{
	pomoc->predchozimesto->dalsimesto=pomoc;
	pomoc=pomoc->predchozimesto;
}
ppres=p->predchozimesto;
pomoc=ppres->predchozimesto;

while(strcmp(ppres->nazev,odkud))
{
	pdate=date;
	v=pomoc->spojeni;
	while(v!=NULL)
	{
		u=v->spoj;
		plus=0;
		q=u->mesta;
		r=u->posun;
		if(strcmp(u->odkud->nazev,pomoc->nazev)) 
		{
			while(strcmp(q->mesto->nazev,pomoc->nazev))          // Searching for my town
			{
				plus+=r->posun+r->posun2;
				q=q->dalsi;r=r->dalsi;
			}
			if(q==NULL) 
			{
				v=v->dalsi;
				continue;
			}
			q=q->dalsi;
			plus+=r->posun;
			pposun=r->posun2;
			r=r->dalsi;
			if(q==NULL) 
			{
				v=v->dalsi;
				continue;
			}
		}
		

		pdate.tm_mday-=(u->odjezd+posun)/1440+pomoc->den;
		pdate.tm_mon--; 
		mktime(&pdate);
		pdate.tm_mon++;
		if (pdate.tm_wday==0) pdate.tm_wday=7;
		if(!exists(u->remark,&pdate)=='N') 	//Now I try if the train goes this day
		{
			v=v->dalsi;
			continue;
		}
		
		if(u->odjezd==-1) u->odjezd=pomoc->prijezd;
					
		if((pomoc->prijezd+tolerance*60)<1440)
		{
		    if(((u->odjezd+plus)%1440<pomoc->prijezd)||((u->odjezd+plus)%1440>pomoc->prijezd+tolerance*60))
			{
				v=v->dalsi;
				continue;
			}
		}
		else if(((u->odjezd+plus)%1440<pomoc->prijezd)&&((u->odjezd+plus)%1440>(pomoc->prijezd+tolerance*60)%1440))
		{
			v=v->dalsi;
		   	continue;
		}
		posun=0;
		while(q!=NULL)          // Searching for my town
		{
			if(!strcmp(q->mesto->nazev,ppres->nazev)) break;
			posun+=r->posun+r->posun2;
			q=q->dalsi;r=r->dalsi;
		}
		if(q==NULL) 
		{
			v=v->dalsi;
			continue;
		}

			posun+=r->posun;
			tmp=r->posun2;
	
			q=p->prijel->mesta;
			r=p->prijel->posun;
			pposun=0;

			if(strcmp(p->prijel->odkud->nazev,ppres->nazev))
				while(strcmp(q->mesto->nazev,ppres->nazev))
				{
					pposun+=r->posun+r->posun2;
					q=q->dalsi;r=r->dalsi;
				}
		if((u->odjezd+posun+plus)%1440-pomoc->prijezd<0) pden=1; 
		else pden=0;

			if((((u->odjezd+plus)%1440+posun)%1440+(pden+pomoc->den)*1440>ppres->prijezd+1440*ppres->den)&&(((u->odjezd+plus)%1440+posun+tmp)%1440+(pomoc->den+pden)*1440<(p->prijel->odjezd+pposun)%1440+p->den*1440))
			{
				ppres->prijel=u;
				ppres->prijezd=((u->odjezd+plus)%1440+posun)%1440;
				ppres->den=p->den-((u->odjezd+plus)%1440+posun)/1440;
			}
			v=v->dalsi;
		}
		p=p->predchozimesto;
		ppres=ppres->predchozimesto;
		pomoc=pomoc->predchozimesto;
		if(pomoc==NULL) break;
		v=pomoc->spojeni;
	}


plus=0;
kod=hashfun(odkud);
pomoc=pokus->pole[kod];
while(strcmp(pomoc->nazev,odkud))
	pomoc=pomoc->dalsi;

ppres=pomoc;
km=0;
do
{
	plus=0;
	pomoc=ppres->dalsimesto;
	u=pomoc->prijel;
	posuny=u->posun;
	pq=u->mesta;
	if(strcmp(u->odkud->nazev,ppres->nazev))
	{
		while(strcmp(pq->mesto->nazev,ppres->nazev))
		{

			plus+=posuny->posun;
			plus+=posuny->posun2;
			pq=pq->dalsi;
			posuny=posuny->dalsi;
		}
		plus+=posuny->posun;
	}
	else 
	{
	
		plus+=posuny->posun;
		km+=posuny->km;
	    if(optionu)
			printf("%2d:%02d\t\t%-24s%2d:%02d\t\t%-24s %d	 0	%s\n",((pomoc->prijel->odjezd/60+(pomoc->prijel->odjezd%60)/60)%24),(pomoc->prijel->odjezd%60)%60,ppres->nazev,((pomoc->prijel->odjezd/60+(plus+pomoc->prijel->odjezd%60)/60)%24),(plus+pomoc->prijel->odjezd%60)%60,pq->mesto->nazev,km,pomoc->prijel->jmeno);
		else
			printf("	%d:%.2d	%s		%s\n %d:%.2d ",((pomoc->prijel->odjezd/60+(pomoc->prijel->odjezd%60)/60)%24),(pomoc->prijel->odjezd%60)%60,ppres->nazev,pomoc->prijel->jmeno,((pomoc->prijel->odjezd/60+(plus+pomoc->prijel->odjezd%60)/60)%24),(plus+pomoc->prijel->odjezd%60)%60);
						

	}
					
	while((pq->mesto!=pomoc))
	{
		ppres=pq->mesto;
		pq=pq->dalsi;
		tmp=plus;
		plus+=posuny->posun2;
		posuny=posuny->dalsi;
		km+=posuny->km;
		tmp=plus;
		plus+=posuny->posun;
		if(optionu)
	          printf("%2d:%02d\t\t%-24s%2d:%02d\t\t%-24s %d	 0	%s\n",((pomoc->prijel->odjezd/60+(tmp+pomoc->prijel->odjezd%60)/60)%24),(tmp+pomoc->prijel->odjezd%60)%60,ppres->nazev,((pomoc->prijel->odjezd/60+(plus+pomoc->prijel->odjezd%60)/60)%24),(plus+pomoc->prijel->odjezd%60)%60,pq->mesto->nazev,km,pomoc->prijel->jmeno);				
		else
			printf("	%d:%.2d	%s		%s\n %d:%.2d ",((pomoc->prijel->odjezd/60+(tmp+pomoc->prijel->odjezd%60)/60)%24),(tmp+pomoc->prijel->odjezd%60)%60,ppres->nazev,pomoc->prijel->jmeno,((pomoc->prijel->odjezd/60+(plus+pomoc->prijel->odjezd%60)/60)%24),(plus+pomoc->prijel->odjezd%60)%60);
		
											
					
	}
	if(!optionu)
		printf("		%s		%s\n%s",pomoc->nazev,pomoc->prijel->jmeno,pomoc->prijel->remark);
		
	ppres=pomoc;
}
while(strcmp(ppres->nazev,kam));
return 1;

}


