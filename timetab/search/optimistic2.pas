program Doprava;
  {Hleda nejkratsi casove spojeni mezi zadanymi stanicemi.
   Informace o spojich si nacte z externiho souboru.
   Lze take hledat nejkratsi vzdalenost mezi stanicemi pouzitim parametru -d.

   Copyright (c) 2002 Jiri Krucek
   krucji@quick.cz
   Distribute under GPL v2                                              }


type pStanice = ^stanice;
     pSoused = ^soused;
     pInf = ^inf;

     stanice = record  {informace o stanicich}
                 nazev: string[30];
                 docas: boolean;
                 hodnota: integer;
                 sousede: pSoused;
                 info: pInf;
                 dalsi: pStanice;
               end;

     soused = record  {seznam adres sousedu dane stanice}
                adresa: pStanice;
                dalsi: pSoused;
              end;

     inf = record  {informace o casech}
             pred  : pStanice;
             vzdal : integer;
             dalsi : pInf;
           end;	   

var vychozi, konecna: string;    {nazev vychozi a konecne stanice}
    vS, kS: pStanice;
      {indexy vychozi a konecne stanice v poli nazvyStanic}

    f: text;
    cesta: string;
    c: char; {pomocna pro rozhodnuti zda se ma hledat cas nebo cesta}
    zacSez: pStanice;


{* ZPRACOVANI VSTUPNIHO SOUBORU *}


function druh_souboru: boolean;
  {zjisti o jaky druh vstupniho souboru se jedna podle
   tretiho radku v souboru.
   vraci true - soubor s absolutnimi casy
         false - soubor s relativnimi casy            }

var radek: string;

begin
  readln(f, radek);
  readln(f, radek);
  readln(f, radek);
  if (radek[1] = '+') or (radek[2] = '+') then
    druh_souboru := false
  else druh_souboru := true;
  reset(f);
end;


function preved_na_cas(s: string): integer;
  {vstupni hodnota je cas ve tvaru stringu,
   funkce vraci tento cas prevedeny na minuty}

var pom1, pom2: string;
    suma1, suma2, code: integer;

begin
  if s[2] = ':' then
    begin
      pom1 := copy(s, 1, 1);
      pom2 := copy(s, 3, 2);
    end
  else
    begin
      pom1 := copy(s, 1, 2);
      pom2 := copy(s, 4, 2);
    end;
  val(pom1, suma1, code);
  val(pom2, suma2, code);
  preved_na_cas := suma1 * 60 + suma2;
end;


procedure sousede(var predAdr: pSoused; pom: pSoused; aktAdr: pStanice );
  {stanici predAdr priradi jako souseda stanici aktAdr}

var s: pSoused;

begin
  while pom <> nil do
    if pom^.adresa = aktAdr then
      exit
    else pom := pom^.dalsi;
  new(s);
  s^.adresa := aktAdr;
  s^.dalsi := predAdr;
  predAdr := s;
end;


procedure osetri(var znak: char);
  {osetreni pripadnych tabulatoru}

begin
  while znak = #9 do
    read(f, znak);
end;


function vratAdrSta(nazev: string; seznam: pStanice): pStanice;
  {vraci adresu stanice nazev}

var test: boolean;

begin
  vratAdrSta := nil;
  while (seznam <> nil) do
    if seznam^.nazev = nazev then
      begin
        vratAdrSta := seznam;
        break;
      end
    else seznam := seznam^.dalsi;
end;


procedure inicializuj(nazev: string; var zac, adr: pStanice);
  {inicializuje novou stanici a nastavuje jeji pocatecni hodnoty}

var c: pStanice;

begin
  new(c);
  c^.nazev := nazev;
  c^.docas := true;
  c^.hodnota := maxint;
  c^.sousede := nil;
  c^.info := nil;
  c^.dalsi := zac;
  zac := c;
  adr := c;
end;


function nacti(var znak: char): string;
  {pomocna funkce pro cteni udaju}

var pom: string;

begin
  pom := '';
  while znak <> #9 do
    begin
      pom := pom + znak;
      read(f, znak);
    end;
  nacti := pom;
end;


procedure zapis(var adr: pStanice; odjPre, odjAkt, vzdal: integer; predAdr: pStanice);
  {dosadi udaje pro stanici s adresou adr}

var i	 : pInf;
begin
  if upcase(c) <> 'A' then
     vzdal := odjAkt - odjPre;
  if vzdal < 0 then
      vzdal := maxint;
   
  i := adr^.info;
  while i <> nil do begin
     if i^.pred = predAdr then
     begin
	if i^.vzdal > vzdal then
	   i^.vzdal := vzdal;
	exit;
     end;
     i := i^.dalsi;
  end;
   
  new(i);
  i^.pred := predAdr;
  i^.dalsi := adr^.info;
  i^.vzdal := vzdal;
  adr^.info := i;
end;


function nacti_nazev(znak: char): string;
  {cte nazev stanice}

var nazev: string;

begin
  nazev := znak;
  while not eoln(f) do
    begin
      read(f, znak);
      nazev := nazev + znak;
    end;
  nacti_nazev := nazev;
end;


procedure zpracuj1;
  {zpracovani souboru s absolutnimi casy}

var code: integer;
    aktCas: integer;      {cas odjezdu z aktualni stanice}
    preCas: integer;      {cas odjezdu z predchozi stanice}
    aktSta: string[30];       {nazev prave zpracovavane stanice}
    vzdAkt: integer;
      {vzdalenost aktualni stanice od nastupni stanice daneho spoje}
    vzdPre: integer;
      {vzdalenost predchozi stanice od nastupni stanice daneho spoje}
    znak: char;
    pom: string;
    aktAdr: pStanice;  {adresa aktualni stanice}
    preAdr : pStanice;  {adresa predchozi stanice}

begin

  while not eof(f) do

    begin

      read(f, znak);

      osetri(znak);

      {je to radek s udaji o nejake stanici?}
      if not(znak in ['0'..'9']) then
        begin
          readln(f);
          preCas := maxint; {technicky trik}
          vzdPre := 0;
          preAdr := nil;
          continue;
        end;

      {nacteni casu odjezdu}
      aktCas := preved_na_cas( nacti(znak) );

      osetri(znak);

      {nacteni druheho udaje - o case nebo vzdalenosti}
      pom := nacti(znak);

      osetri(znak);

      {pripad, kdy je udaj o vzdalenosti treti v poradi}
      if znak in ['0'..'9'] then
        begin
          pom := nacti(znak);
          osetri(znak);
        end;

      val(pom, vzdAkt, code);

      {nacteni nazvu stanice}
      aktSta := nacti_nazev(znak);

      {zapis zpracovavane stanice do pole nazvyStanic}
      aktAdr := vratAdrSta(aktSta, zacSez);
      if aktAdr = nil then inicializuj(aktSta, zacSez, aktAdr);

      {zapis informaci o stanici}
      if preCas = maxint then
        zapis(aktAdr, 0, aktCas, vzdPre, preAdr)
      else
        begin
          sousede(preAdr^.sousede, preAdr^.sousede, aktAdr);
          if preCas > aktCas then aktCas := aktCas + 1440;
          zapis(aktAdr, preCas, aktCas, vzdAkt - vzdPre, preAdr);
        end;

      vzdPre := vzdAkt;
      preAdr := aktAdr;
      preCas := aktCas;

      readln(f);

    end;

end;


procedure zpracuj2;
  {zpracovani souboru s relativnimi casy}

var code: integer;
    aktSta: string;       {nazev prave zpracovavane stanice}
    cas1: integer;        {doba odjezdu z pocatecni stanice}
    cas2: integer;        {relativni doba odejzdu ze stanice}
    vzdalenost: integer;  {vzdalenost mezi aktualni a predchozi stanici}
    znak: char;
    pom: string;
    aktAdr: pStanice;     {adresa aktualni stanice}
    preAdr: pStanice;     {adresa predchozi stanice}

begin

  while not eof(f) do

    begin

      read(f, znak);

      osetri(znak);

      {je to radek s udaji o nejake stanici?}
      if not((znak = '+') or (znak in ['0'..'9'])) then
        begin
          readln(f);
          cas2 := maxint;
          preAdr := nil;
          vzdalenost := 0;
          continue;
        end;

      {nacteni casu odjezdu}
      pom := nacti(znak);

      {jaky radek s casem se vlastne nacital}
      if pom[1] = '+' then
        val(pom, cas2, code)
      else cas1 := preved_na_cas(pom);

      osetri(znak);

      {nacteni druheho udaje - o case nebo vzdalenosti}
      pom := nacti(znak);

      osetri(znak);

      {udaj o vzdalenosti je jako treti udaj}
      if znak in ['0'..'9'] then
        begin
          pom := nacti(znak);
          osetri(znak);
        end;

      {nacteni udaju o vzdalenosti}
      val(pom, vzdalenost, code);

      {nacteni nazvu stanice}
      aktSta := nacti_nazev(znak);

      {zapis zpracovavane stanice do pole nazvyStanic}
      aktAdr := vratAdrSta(aktSta, zacSez);
      if aktAdr = nil then inicializuj(aktSta, zacSez, aktAdr);

      {zapis informaci o stanici}
      if cas2 = maxint then
        begin
          cas2 := 0;
          zapis(aktAdr, 0, cas1, vzdalenost, preAdr);
        end
      else
        begin
          sousede(preAdr^.sousede, preAdr^.sousede, aktAdr);
          zapis(aktAdr, cas1, cas1 + cas2, vzdalenost, preAdr);
        end;

      cas1 := cas1 + cas2;
      preAdr := aktAdr;

      readln(f);

    end;

end;


procedure nacteni_dat;
  {nacteni dat ze souboru}

begin
  assign(f, cesta);
  reset(f);
  if druh_souboru then
    zpracuj1
  else zpracuj2;
  close(f);
end;


{* STANDARDNI VSTUP *}


procedure osetriSV(var znak: char);
  {osetreni pripadnych tabulatoru}

begin
  while znak = #9 do
    read(znak);
end;


function nactiSV(var znak: char): string;
  {pomocna funkce pro cteni udaju}

var pom: string;

begin
  pom := '';
  while znak <> #9 do
    begin
      pom := pom + znak;
      read(znak);
    end;
  nactiSV := pom;
end;


function nacti_nazevSV(znak: char): string;
  {cte nazev stanice}

var nazev: string;

begin
  nazev := znak;
  while not eoln do
    begin
      read(znak);
      nazev := nazev + znak;
    end;
  nacti_nazevSV := nazev;
end;


procedure zpracuj1SV;
  {zpracovani souboru s absolutnimi casy}

var code: integer;
    aktCas: integer;      {cas odjezdu z aktualni stanice}
    preCas: integer;      {cas odjezdu z predchozi stanice}
    aktSta: string[30];       {nazev prave zpracovavane stanice}
    vzdAkt: integer;
      {vzdalenost aktualni stanice od nastupni stanice daneho spoje}
    vzdPre: integer;
      {vzdalenost predchozi stanice od nastupni stanice daneho spoje}
    znak: char;
    pom: string;
    aktAdr: pStanice;  {adresa aktualni stanice}
    preAdr: pStanice;  {adresa predchozi stanice}

begin

  while not eof do

    begin

      read(znak);

      osetriSV(znak);

      {je to radek s udaji o nejake stanici?}
      if not(znak in ['0'..'9']) then
        begin
          readln;
          preCas := maxint; {technicky trik}
          vzdPre := 0;
          preAdr := nil;
          continue;
        end;

      {nacteni casu odjezdu}
      aktCas := preved_na_cas( nactiSV(znak) );

      osetriSV(znak);

      {nacteni druheho udaje - o case nebo vzdalenosti}
      pom := nactiSV(znak);

      osetriSV(znak);

      {pripad, kdy je udaj o vzdalenosti treti v poradi}
      if znak in ['0'..'9'] then
        begin
          pom := nactiSV(znak);
          osetriSV(znak);
        end;

      val(pom, vzdAkt, code);

      {nacteni nazvu stanice}
      aktSta := nacti_nazevSV(znak);

      {zapis zpracovavane stanice do pole nazvyStanic}
      aktAdr := vratAdrSta(aktSta, zacSez);
      if aktAdr = nil then inicializuj(aktSta, zacSez, aktAdr);

      {zapis informaci o stanici}
      if preCas = maxint then
        zapis(aktAdr, 0, aktCas, vzdPre, preAdr)
      else
        begin
          sousede(preAdr^.sousede, preAdr^.sousede, aktAdr);
          if preCas > aktCas then aktCas := aktCas + 1440;
          zapis(aktAdr, preCas, aktCas, vzdAkt - vzdPre, preAdr);
        end;

      vzdPre := vzdAkt;
      preAdr := aktAdr;
      preCas := aktCas;

      readln;

    end;

end;


procedure zpracuj2SV;
  {zpracovani souboru s relativnimi casy}

var code: integer;
    aktSta: string[30];       {nazev prave zpracovavane stanice}
    cas1: integer;        {doba odjezdu z pocatecni stanice}
    cas2: integer;        {relativni doba odejzdu ze stanice}
    vzdalenost: integer;  {vzdalenost mezi aktualni a predchozi stanici}
    znak: char;
    pom: string;
    aktAdr: pStanice;  {adresa aktualni stanice}
    preAdr: pStanice;  {adresa predchozi stanice}

begin

  while not eof do

    begin

      read(znak);

      osetriSV(znak);

      {je to radek s udaji o nejake stanici?}
      if not((znak = '+') or (znak in ['0'..'9'])) then
        begin
          readln;
          cas2 := maxint;
          vzdalenost := 0;
          preAdr := nil;
          continue;
        end;

      {nacteni casu odjezdu}
      pom := nactiSV(znak);

      {jaky radek s casem se vlastne nacital}
      if pom[1] = '+' then
        val(pom, cas2, code)
      else cas1 := preved_na_cas(pom);

      osetriSV(znak);

      {nacteni druheho udaje -  o case nebo vzdalenosti}
      pom := nactiSV(znak);

      osetriSV(znak);

      {vzdalenost je jako treti udaj}
      if znak in ['0'..'9'] then
        begin
          pom := nactiSV(znak);
          osetriSV(znak);
        end;

      {nacteni udaju o vzdalenosti}
      val(pom, vzdalenost, code);

      {nacteni nazvu stanice}
      aktSta := nacti_nazevSV(znak);

      {zapis zpracovavane stanice do pole nazvyStanic}
      aktAdr := vratAdrSta(aktSta, zacSez);
      if aktAdr = nil then inicializuj(aktSta, zacSez, aktAdr);

      {zapis informaci o stanici}
      if cas2 = maxint then
        begin
          cas2 := 0;
          zapis(aktAdr, 0, cas1, vzdalenost, preAdr);
        end
      else
        begin
          sousede(preAdr^.sousede, preAdr^.sousede, aktAdr);
          zapis(aktAdr, cas1, cas1 + cas2, vzdalenost, preAdr);
        end;

      cas1 := cas1 + cas2;
      preAdr := aktAdr;

      readln;

    end;

end;


{* OBECNE *}


procedure neexistuje;

begin
  writeln('Mezi temito stanicemi neexistuje spojeni.');
end;


function osetri_vstup(s: string): string;
  {pomocna procedura pro osetreni vstupu udaju z promptu}

var i: integer;

begin
  for i := 1 to length(s) do
    if s[i] = '_' then
      s[i] := ' ';
  osetri_vstup := s;
end;

{* HLEDANI VZDALENOSTI *}


function nejkratsi_vzdalenost(V, K: pStanice): integer;
  {vraci nejkratsi vzdalenost ze stanice V do K}

var pom: integer;
    info : pInf;

begin
  info := K^.info; {pracujem s infem stanice K}
  pom := maxint;
  while info <> nil do
    begin
      if (info^.pred = V) and (info^.vzdal < pom) then
        pom := info^.vzdal;
      info := info^.dalsi;
    end;
  nejkratsi_vzdalenost := pom;
end;


procedure hledej_vzdalenost(zacSez: pStanice);
  {hleda nejkratsi vzdalenost mezi stanicemi vS a kS pomoci Dijkstrova algoritmu}

var konec: boolean;
    pomSta, zac: pStanice;
    vzd: integer;
    sous: pSoused;

begin
  konec := true;
  while kS^.docas and konec do
    begin
      pomSta := kS;
      zac := zacSez;
      while zac <> nil do
        begin
          if zac^.docas and (zac^.hodnota < pomSta^.hodnota) then
            pomSta := zac;
          zac := zac^.dalsi;
        end;
      if pomSta^.hodnota = maxint then
        konec := false
      else
        begin
          pomSta^.docas := false;
          sous := pomSta^.sousede;
          while sous <> nil do
            begin
              vzd := nejkratsi_vzdalenost(pomSta, sous^.adresa);
              if (pomSta^.hodnota + vzd) < sous^.adresa^.hodnota then
                sous^.adresa^.hodnota := pomSta^.hodnota + vzd;
              sous := sous^.dalsi;
            end;
        end;
    end;
end;


{* UZIVATELSKE VSTUPY *}


procedure help;

begin
  writeln;
  writeln('Doprava - Help');
  writeln('--------------');
  writeln('Mozne prepinace:');
  writeln;
  writeln(' -d  pocita nejkratsi vzdalenost mezi stanicemi');
  writeln('     (bez tohoto parametru se pocita cas)');
  writeln;
  writeln(' -r  jako vstup je pouzit soubor s relativnimi informacemi o spojich');
  writeln('     (informace o prislusne stanici jsou brany vyhledem k informacim');
  writeln('      o predchozi stanici)');
  writeln('     (bez tohoto parametru musi byt vstupem soubor s absolutnimi informacemi) ');
  writeln;
  writeln(' -?  help');
  writeln;
  writeln('Jako argumenty jsou pouzity nazev vychozi a konecne stanice v tomto poradi.');
  writeln('U viceslovnych nazvu je treba misto mezer psat podtrzitka ''_''');
  writeln;
  writeln('pr:   doprava<spoje.tt -d Jankovcova Pelc_Tyrolka');
  halt;
end;


procedure vstup_z_obrazovky;

begin
  writeln('Doprava');
  writeln('-------');
  writeln;
  writeln('- vypise podle pozadavku nejkratsi spojeni, nebo vzdalenost mezi zadanymi');
  writeln('stanicemi.');
  writeln;
  write('Cesta k souboru se spojenimi: ');
  readln(cesta);
  write('Vychozi stanice: ');
  readln(vychozi);
  write('Konecna stanice: ');
  readln(konecna);
  write('Hledat vzdalenost? (a/n) (Jinak se hleda cas.): ');
  readln(c);
  writeln('�ekejte pros�m. Prob�h� v�po�et.');

  {zpracovani souboru s daty o spojich: }
  nacteni_dat;
end;


procedure standardni_vstup;

var soubor: boolean;
    i, j: integer;
    prepinac: string;

begin
  if ParamStr(1) = '-?' then
    help
  else
    begin
      i := 1;
      soubor := true;
      prepinac := ParamStr(i);
      while prepinac[1] = '-' do {rozebereme prepinace}
        begin
          for j := 2 to length( prepinac ) do
            case prepinac[j] of
              'd': c := 'A';  {pocitame vzdalenost}
              'r': soubor := false;  {soubor o spojich je s relativnimi informacemi}
            end;
          inc(i);
          prepinac := ParamStr(i);
        end;
      vychozi := osetri_vstup(ParamStr(i));
      konecna := osetri_vstup(ParamStr(i+1));
      if soubor then
        zpracuj1SV
      else zpracuj2SV;
    end;
end;


{* MAIN *}


begin

  new(zacSez);
  zacSez := nil;

  {cteni zakladnich parametru v poradi cesta k souboru se spojenimi,
   vychozi stanice, konecna stanice                                 }

  if ParamCount > 0 then {standardni vstup}
    standardni_vstup
  else {vstup z obrazovky}
    vstup_z_obrazovky;

  vS := vratAdrSta(vychozi, zacSez);
  kS := vratAdrSta(konecna, zacSez);

  {test zda mame takove stanice vubec v poli: }
  if (vS <> nil) and (kS <> nil) then
      begin
        vS^.hodnota := 0;
        hledej_vzdalenost(zacSez); {hledame nejkratsi vzdalenost}
        if kS^.hodnota = maxint then
          neexistuje
        else writeln(kS^.hodnota);
      end
  else neexistuje;
end.