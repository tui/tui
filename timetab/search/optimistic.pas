{ Zapoctovy program optimista
  copyright Martin Kuben <martin.kuben@seznam.cz>
  distributed under General Public Licence }


program zapocet;

	
{$IFDEF soubor}
        uses crt;
{$ENDIF}
const
        {$IFDEF soubor}
        jmsoubor = 'train_cz.tt';
        {$ENDIF}
	hodne = 10000;         { Nekonecno }
	maxstr = 50;           { Maximalni delka retezce }
	max = 1200;             { Maximalni pocet mest v seznamu }
        maxsta = 100;           { Maximalni pocet stanic v lince }
type
	pmesto = ^mesto;
	phrana = ^hrana;
	mesto = record
		jmeno : string[maxstr];        { Jmeno stanice }
		dalsi : pmesto;                { Odkaz na dalsi mesto }
                hrana : phrana;                { Odkaz na seznam mest }
                d     : integer;               { Ohodnoceni mesta }
	end;
	hrana = record
		kam : pmesto;                  { Kam smeruje hrana }
          	dalsi : phrana;                { Odkaz na dalsi hranu }
          	d     : integer;               { Delka hrany }
                jmeno : string[20];
	end;
	fronta = record                        { fronta mest }
		zac : pmesto;                  { Odkazy na zacatek a na }
           	akt : pmesto;                  {   aktualni pozici}
	end;
	tsezmest = array[0..max] of record     { Fronta urcena k hledani }
		st : string[maxstr];               { Jmeno stanice }
                hr : string[20];                   { Jmeno spoje }
        end;
var
	pom2 : phrana;         { pomocne ukazatele }
	pom1 : pmesto;
        fr : fronta;
        start, cil : string;
        code : longint;
        od, kam : pmesto;

function ukazmesto(fr : fronta;jm : string) : pmesto;
      { Funkce projde seznam fr, hleda mesto s nazvem jm a vrati ukazatel
      		na toto mesto, pokud mesto neni v seznamu vrati nil }
var
	pom : pmesto;
begin
pom := fr.zac;
ukazmesto := nil;
while (pom <> nil) and (ukazmesto = nil) do
	begin
        if pom^.jmeno = jm then ukazmesto := pom;
        pom := pom^.dalsi;
        end;
end;
(********************* PRIDA MESTO ****************************)

procedure pridejmesto(var fr : fronta; jm : string);

{  Prida nove mesto do fronty, kontroluje, zda-li uz tam je }
var
	novy : pmesto;
        pom : pmesto;
        jetam : boolean;
begin
pom := fr.zac;
jetam := false;

while (pom <> nil) and (not(jetam)) do
        begin                                     { Projde seznam, jestli uz }
        if jm = pom^.jmeno then jetam := true;    { tam je. }
        pom := pom^.dalsi;
        end;
if not(jetam) then
	begin                                      { Vytvori nove mesto v }
        new(novy);
        novy^.dalsi := fr.zac;
        novy^.jmeno := jm;
        novy^.hrana := nil;
        novy^.d := hodne;
        fr.zac := novy;
        end;
end;

(********************** PRIDA HRANU ***************************)

procedure pridejhranu(var fr : fronta;mesto : string;kam : pmesto; d : integer; jm : string);

{ Prida hranu , nekontroluje, jestli uz tam je, pripadne prepise}

var
   pom1 : pmesto;
   pom2 : phrana;
   jetam : boolean;
   hran : phrana;
   konec : boolean;
begin
pom1:=fr.zac;
konec := false;
{$UNDEF neco}
while (pom1 <> nil) and (not(konec)) do    { Dokud je v seznamu nejake mesto }
      begin
      if pom1^.jmeno = mesto then  { Jmeno vychoziho mesta stejne }
         begin
         {$IFDEF neco}
	 pom2 := pom1^.hrana;
         jetam := false;
         while pom2 <> nil do       { Kontroluje, zda-li uz tam hrana je }
               begin
               if (pom2^.kam = kam) and (pom2^.d > d) then{je tam, ale vetsi }
                  begin
                  pom2^.d := d;            { Zmeni pouze velikost a nazev }
                  pom2^.jmeno := jm;
                  end;
               if (pom2^.kam = kam) and (pom2^.d <= d) then jetam := true;
               pom2 := pom2^.dalsi;
               end;
         if not(jetam) then        { hrana tam neni }
	 {$ENDIF}
            begin
            new(hran);
            hran^.dalsi := pom1^.hrana;       { prida hranu k mestu }
            hran^.d := d;
            hran^.kam := kam;
            hran^.jmeno := jm;
            pom1^.hrana := hran;
            konec := true;
            end;
	 end;
      pom1:=pom1^.dalsi;
      end;
end;

(****************** NACTE SPOJE ZE VSTUPU *******************)

procedure nacti(var fr : fronta; code : integer);
{ Procedura nacte ze standartniho vstupu soubor typu tt a mesta a spoje
	prida do fr, code zapina ruzne prepinace }
type
        pnaz = ^naz;
        naz = record                  { typ nazev spoje }
        	jmeno : string[15];
                dalsi : pnaz;
                prvni : string[maxstr];
                posledni : string[maxstr];
                pocet : byte;
        end;
var
	opt : array[1..maxsta] of record
        	od : string[maxstr];
                kam : string[maxstr];
                d : integer;
            end;
        ii : integer;
        jm : string;		{ Jmeno aktualniho spoje }
        ch : char;
        i : integer;
        delka,prvni,druhy : integer;        { Delka spoje v minutach }
        odkud,kam : string;     { Jmena mest odkud a kam vede hrana}
        fr2 : record              { Seznamu spoju, ktere uz byly nacteny }
        	zac : pnaz;
                akt : pnaz;
        end;
        vel : longint;              {  Uchovava aktualni pozici v souboru }
        cas1, cas2 : integer;       { Pri nacitani casu }
        koneclinky : boolean;
        {$IFDEF soubor}
        f : text;
        {$ENDIF}

        function cti : char;
        begin
        {$IFNDEF soubor}
        read(cti);
        {$ENDIF}
        {$IFDEF soubor}
        read(f,cti);
        {$ENDIF}
        inc(vel);
        if cti = #13 then cti := cti;
        end;

        function nacticas(prvni : char) : integer;
        	{ Nacte cas ve tvaru 12:33 }
        var
        	ch : char;
                pom, pom2 : integer;
        begin
        ch := prvni;
        pom := 0;
        while (ch <> ':') and {$IFNDEF soubor}(not(eof)){$ENDIF}
                {$IFDEF soubor}(not(eof(f))){$ENDIF} do { Dokud neni nacteny znak ':'}
        	begin
                pom := pom*10 + ord(ch) - ord('0');
                ch := cti;
                end;
        pom := pom * 60;        { Nacte hodiny a prevede na minuty }
        pom2 := 0;
        ch := cti;
        while (ch>='0') and (ch<='9') do   { Dokud jsou nactene znaky }
        	begin                      { cislice, cti minuty }
	        pom2 := pom2*10 + ord(ch) - ord('0');
                ch := cti;
                end;
        nacticas := pom + pom2;
        end;

        procedure cislo(var delka : integer);   { Precte cislo }
        var
        	ch : char;
        begin
        ch := cti;
        delka := 0;
        while (ch>='0') and (ch<='9') do
        	begin
                delka := delka*10 + ord(ch)-ord('0');
                ch := cti;
                end;

        end;

        function jetam(jm : string;pocet : integer;od,kam : string) : boolean;
		{ Vraci, jestli je jmeno
			spoje s danym nazvem v seznamu fr2 }
        var
        	pom : pnaz;
        begin
        pom := fr2.zac^.dalsi;
        jetam := false;
        while (pom <> nil) and (not(jetam)) do
        	begin
                if (pom^.jmeno = jm) and (pom^.pocet = pocet) and
			(pom^.prvni = od) and (pom^.posledni=kam) then
				jetam := true;
                pom:= pom^.dalsi;
                end;
        end;

	procedure nazev(var jm : string; var dalsi : char);
        { Nacte nazev(string) ze standartniho vstupu }
        var
        	ch : char;
                i : integer;
	begin
        ch := cti;
        i:=1;
        while (ch<>#10) and (i<=maxstr) do
        	begin
                jm[i] := ch;
                inc(i);
                ch := cti;
		end;
        jm[0] := chr(i-1);
        dalsi := ch;
        end;

        procedure pridejnaz(jm,pr,po : string;pocet : integer);
        	{ Prida nazev spoje do seznamu fr2 }
        begin
        new(fr2.akt^.dalsi);
	fr2.akt := fr2.akt^.dalsi;
	fr2.akt^.jmeno := jm;
        fr2.akt^.prvni := pr;
        fr2.akt^.posledni := po;
        fr2.akt^.pocet := pocet;
	fr2.akt^.dalsi := nil;
        end;

begin
{$IFDEF soubor}
        assign(f,jmsoubor);
        reset(f);
{$ENDIF}
new(fr2.zac);               { Vytvori seznam nazvu spoju }
fr2.akt := fr2.zac;
fr2.zac^.dalsi := nil;
new(fr.zac); 		    { Vytvori seznam mest }
vel := 0;
ch := cti;
while {$IFNDEF soubor}(not(eof)){$ENDIF}
                {$IFDEF soubor}(not(eof(f))){$ENDIF} do { Dokud neni konec souboru }
	begin
        if (code and 2) = 2 then writeln(vel); { Tiskne pozici v souboru }
        odkud := '';    { Neni zadna vychozi stanice }
        cas1 := -1;
        ii := 1;
        koneclinky := false;
        while (ch<>'#') and {$IFNDEF soubor}(not(eof)){$ENDIF}
                {$IFDEF soubor}(not(eof(f))){$ENDIF} do  { Cte dokud nenarazi na # }
		begin
                ch := cti;
                end;
	nazev(jm,ch);			{ Nacte nazev linky }
        ch := cti;
        while (not(koneclinky)) and {$IFNDEF soubor}(not(eof)){$ENDIF}
                {$IFDEF soubor}(not(eof(f))){$ENDIF} do { Dokud na zacatku radky
						je tab }
       		begin
                if ch = '+' then
                        begin
                                cislo(prvni); { Pokud +, nacte cislo }
                                ch := cti;
                        end
                        else
                        if ch = #9 then
                                begin
                                if odkud = '' then prvni := -1
                                        else prvni := 0;
                                ch := cti;
                                end
                        else
                                begin
                                cas2 := nacticas(ch); { Jinak nacte cas }
			        if cas1 = -1 then     { Je to prvni cas }
                                        begin
                                        cas1 := cas2;
                                        prvni := -1;
                                        end
                                else
                                        begin
                                        if cas2 > cas1 then { Pres pulnoc }
                                                prvni := cas2 - cas1
                                        else
                                       	        prvni := 60 * 24 - cas1 + cas2;
                                        end;
                                ch := cti;
                                end;
      		if ch = '+' then cislo(druhy) { Pokud +, nacte cislo }
                        else
                        begin
                        if ch = #9 then
                                begin
                                koneclinky := true;
                                end
                        else
                                begin
                                cas2 := nacticas(ch); { Jinak nacte cas }
			        if cas1 = -1 then     { Je to prvni cas }
                                        begin
                                        cas1 := cas2;
                                        druhy := -1;
                                        end
                                else
                                        begin
                                        if cas2 > cas1 then { Pres pulnoc }
                                                druhy := cas2 - cas1
                                        else
                                       	        druhy := 60 * 24 - cas1 + cas2;
                                        end;
                                end;
                        end;
                for i:=1 to 2 do
		        repeat    { Ceka na tabulator }
                                ch := cti;
                        until (ch = #9) or {$IFNDEF soubor}
                                            (eof){$ENDIF}
                                            {$IFDEF soubor}(eof(f)){$ENDIF};
                nazev(kam,ch);   { Nacte jmeno cilove stanice }
        	if odkud = '' then odkud := kam; { Pokud je prvni }
        	if prvni > -1 then
        	        begin      { Prida hranu, pokud to neni prvni mesto }
                        if prvni = 0 then delka := druhy
                                else delka := prvni;
                        opt[ii].od := odkud;
                        opt[ii].kam := kam;
                        opt[ii].d := delka;
                        inc(ii);
                        end;
                odkud := kam;
                ch := cti;
        	end;  { Dale nacte posledni radek, ktery nezacina tabulatorem }

        { Ted optimalizace}
        if ((jetam(jm,ii-1,opt[1].od,opt[ii-1].kam)))
                        and ((code and 16)=16) then
              	begin           { Pokud je zapnuta optimalizace, }
                if (code and 1) = 1 then write('-'); { kdyz -v pak vypisuje }
               	end
                else
       		begin
                if (code and 1) = 1 then write('.');
              	if (code and 16) = 16 then pridejnaz(jm,opt[1].od,opt[ii-1].kam,ii-1);
                pridejmesto(fr,opt[1].od);
                for i:=1 to ii-1 do
                      	begin
                        pridejmesto(fr,opt[i].kam);
              		pridejhranu(fr,opt[i].od,
            			ukazmesto(fr,opt[i].kam),opt[i].d,jm);
                        end;
                end;
        end;
end;

(******************  NAJDE NEJKRATSI CESTU *********************)

function najdi(var fr : fronta;start,konec : pmesto; vypis : boolean) : integer;

{ Najde nejkratsi cestu mezi mesty start a konec, pokud je vypis true, pak
  vypisuje i jmena mest, pres ktere prochazi }

var
	sez : tsezmest;          { fronta obsahujici jmena mest }
	zac, pocet : integer;    { promenne ke staticke fronte }
        ted : pmesto;
        pom : phrana;
        pomm : pmesto;
        pokracuj : boolean;
        i : integer;
        maxdelka : integer;     { V teto promenne si pamatuje nejaku delku
		z start do konec }

	procedure pridej(mesto : pmesto);     { prida mesto do fronty }
        begin
        sez[(zac + pocet) mod max].st := mesto^.jmeno;
        inc(pocet);
        end;

        procedure odeber(var mesto : pmesto);  { odebere mesto z fronty }
        var
		 pom : pmesto;
                 konec : boolean;
        begin
        pom := fr.zac^.dalsi;
        konec := false;
        while (pom <> nil) and (not(konec)) do
        	begin
                if pom^.jmeno = sez[zac].st then { pokud je jmeno shodne
				s prvnim clenem fronty }
                	begin
                        mesto := pom;  { Vrati ukazatel na toto mesto}
                        konec := true;
                        if zac< max-1 then inc(zac)
                        	else zac:=0;
                        dec(pocet);
                        end;
                pom := pom^.dalsi;
                end;
        end;

begin
zac := 0;
pocet := 0;
start^.d := 0;
pridej(start);      { prida do fronty vychozi mesto }
maxdelka := hodne;

while pocet > 0 do  { dokud je co odebirat }
	begin
	odeber(ted);         { odebere mesto z fronty }
	pom := ted^.hrana;   { a prida vsechny jeho nasledovniky, pokud ...}
	while pom <> nil do
		begin
	        if ted^.d + pom^.d < pom^.kam^.d then
			begin		{ mensi vzdalenost, pridat mesto }
                	pom^.kam^.d := ted^.d + pom^.d;
	                if pom^.kam^.d < maxdelka then pridej(pom^.kam);
                                   { Prida pokud ovsem neni vzdalenost do
                                     tohoto mesta vetsi nez vzdalenost od
                                     zacatku do konce }
			end;
                if pom^.kam = konec then maxdelka := pom^.kam^.d;
	        pom := pom^.dalsi;
        	end;

        end;
najdi := konec^.d;
if konec^.d = hodne then     { Jestize je ohodnoce konce 'hodne', pak
				neexistuje spojeni }
	begin
        writeln('Neexistuje spojeni.');
        readln;
	halt(0);
        end;

if vypis then           { pokud je vypis true, pak funkce vypisuje cestu }
	begin
	pocet := 0;
	ted := konec;

        repeat
	pomm := fr.zac^.dalsi;    { prochazi vsechna mesta }
        pokracuj := true;
	while (pomm <> nil) and (pokracuj) do
        	begin
                pom := pomm^.hrana;   { prochazi vsechny hrany z mesta }
                while (pom <> nil) and (pokracuj) do
                	begin
                        if (pom^.kam = ted) and (pomm^.d+pom^.d=ted^.d) then

                            { Pokud cesta vede do konc. mesta a soucet delky
			      hrany a ohodnoceni mesta je ohodnoceni konc.
			      mesta, pak tudy vede cest }

                        	begin
                                sez[pocet].st := ted^.jmeno;
                                sez[pocet].hr := pom^.jmeno;
                                inc(pocet);
                                pokracuj := false
                                end;
                        pom := pom^.dalsi;
                        end;
                if (pokracuj) then pomm := pomm^.dalsi;
                end;
        ted := pomm;
        until pomm = start;

        sez[pocet].st := start^.jmeno;   { Vypisuje v opacnem poradi }
        writeln(sez[pocet].st);
        for i:= pocet-1 downto 0 do writeln(sez[i].st,'    {',sez[i].hr,'}');
        end;


end;

(******************* NACITANI Z PRIKAZOVE RADKY *********************)
procedure radka(var start, cil : string; var code : longint);
var
	ret : array[1..10] of string;
        help : boolean;
        i,j,k : byte;

        	function retcopy(r1, r2 : string) : string;
                { funkce spoji dva retezce v jeden }
                var
                	pom : string;
                        i : integer;
                begin
                pom[0] := chr(length(r1) + length(r2));
                for i:=1 to length(r1) do
                	pom[i] := r1[i];
                for i:=1+length(r1) to length(r1)+length(r2) do
                	pom[i] := r2[i-length(r1)];
                retcopy := pom;
                end;
begin
help := false;
code := 0;
if paramcount < 1 then help := true; { Pokud neni zadny parametr volej help}
for i:=1 to ParamCount do
	begin
        if i<=10 then ret[i] := paramstr(i); { nacte vsechny udaje }
        end;
i := 1;
while ret[i][1] = '-' do  { Dokud je prvni znak '-' nacitej parametry }
	begin
        case ret[i][2] of
                'o' : code := code or 16;
                'v' : code := code or 1;
                'w' : code := code or 2;
                's' : code := code or 4;
                'c' : code := code or 8;
        	end;
        if (ret[i][2]<>'o') and (ret[i][2]<>'v') and (ret[i][2]<>'w') and
		(ret[i][2]<>'s') and (ret[i][2]<>'c') then help := true;
        inc(i);
        end;
if paramcount > i then   { Vsechny retezce spoji v jeden }
	begin
        j := i;
        for j := i+1 to paramcount do
        	begin
                if not((ret[j] = '/') or (ret[i][length(ret[i])] = '/')) then
			ret[i] := retcopy(ret[i], ' ');
                ret[i] := retcopy(ret[i], ret[j]);
                end;
        end;
if not(help) then
	begin
        j := 1;
        while (ret[i][j] <> '/') and (not(help)) do
        	{ Nacte jmeno vychoziho mesta }
        	begin
                start[j] := ret[i][j];
                inc(j);
                if j>length(ret[i]) then help := true;
                end;
        start[0] := chr(j-1);
        for k:= j+1 to length(ret[i]) do
        { Nacte jmeno koncoveho mesta }
        	begin
                cil[k-j] := ret[i][k];
                end;
        cil[0] := chr(length(ret[i])-j);
        end;
if help then begin
	writeln('Program pro vyhledavani nejrychlejsiho spojeni.');
        writeln('Zap.exe {-[parametr]} [odkud]/[kam]');
        writeln('Parametry : ');
        writeln('       -o     Zapne optimalizaci');
        writeln('       -v     Vypis : {.}    -   Nactena linka');
	writeln('                      {-}    -   Preskocena linka');
	writeln('       -w     Vypisuje aktualni pozici v souboru');
        writeln('       -s     Seznam vsech spojeni');
        writeln('       -c     Vypise cestu z "odkud" do "kam"');
	writeln('odkud, kam : nazvy mest ze vstupu');
	writeln('Vstupem je soubor typu *.tt, pridanim na standartni vstup');
	writeln('Priklad : zap.exe -c -v Skalka / Chodov <pid.tt');
        halt;
        end;
end;


(************** Zacatek Programu ******************************)

begin
fr.zac := nil;
radka(start,cil, code);
nacti(fr,code);
if (code and 4) = 4 then
	begin
        pom1 := fr.zac;
        while pom1 <> nil do        { Vypise vsechna spojeni}
      		begin
      		pom2 := pom1^.hrana;
      		while pom2 <> nil do
            		begin
            		writeln(pom1^.jmeno,' - ',pom2^.kam^.jmeno,' : ',pom2^.d);
            		pom2 := pom2^.dalsi;
            		end;
      		pom1 := pom1^.dalsi;
      		end;
        end;
od := ukazmesto(fr, start);
kam := ukazmesto(fr, cil);
if (od = nil) or (kam = nil) then
        begin
        writeln;
        if od = nil then writeln(start,' neni v seznamu!');
        if kam = nil then writeln(cil,' neni v seznamu!');
        halt;
        end;
if (code and 8) = 8 then writeln(najdi(fr,od,kam,true),' minut.')
        else writeln(najdi(fr,od,kam,false),' minut.');
end.