{This program is licensed under GNU General Public License.
 Copyright (C) Marian Ertl 2001}
{ Parameters: town_from date time town_to probability_limit.

  Search for connection given probability limit.
}
program Hladanie_s_pravdepodobnostou;
uses exists2;
type
PSpoj=^Spoj;
PStanica=^Stanica;
PRiadok=^Riadok;
PCasspoj=^Casspoj;
PUzol=^Uzol;
nazovstan=string[150];
popisspoja=string[40];

Riadok=record
  cas,cas2 : word;
  meno : PStanica;
  opt,kon : boolean;
  naj : PSpoj;
  nasled:Priadok;
  dalsi : Priadok;
  uzodkaz:puzol;
end;
riadok2=record
  cas:word;
  cas2:word;
  meno:nazovstan;
end;
Stanica = record
  meno : nazovstan;
  lavy,pravy : Pstanica;
  odkaz : Priadok;
end;
Spoj=record
  cas : word;
  pravd : byte;
  odkaz :Priadok;
  dalsi,pred,spat:pspoj;
  spatcas:word;
  spatprav:integer;
end;
Casspoj=record
  cas:integer;
  dalsi,pred:Pcasspoj;
end;
Uzol=record
  dalsi:Puzol;
  odkaz:Priadok;
  cas:integer;
  popis:popisspoja;
end;


var
    riadky:array[0..1000] of riadok2;
    p,min,sec,limit,presprav:byte;
    ukspoj,zacspoj:pspoj;
    presspoj:pcasspoj;
    koren:pstanica;
    pom,limstr,cas,s,zn:string;
    zmeno,cmeno:nazovstan;
    zdroj,akt,ukmeno,xx:priadok;
    yy:pstanica;
    start,ciel,zmena,nasiel,koniec:boolean;
    k,drop:integer;
    startcas,prescas:word;
    pocriad:longint;
    poc:word;
    datum:string;
    zacuzol:puzol;
    i:byte;


function najdimeno(s:nazovstan;p:priadok;var nasiel:boolean;
                   var ukstan:pstanica):priadok;
var akt,uk,pred:pstanica;
begin
  akt:=koren;pred:=nil;
  while (akt<>nil) do begin
    if s<akt^.meno then begin pred:=akt;akt:=akt^.lavy end
    else if s>akt^.meno then begin pred:=akt;akt:=akt^.pravy end
    else break;
  end;
  if akt<>nil then begin
    najdimeno:=akt^.odkaz;
    nasiel:=true;
    ukstan:=akt;
  end
  else begin
    new(uk);
    uk^.meno:=s;
    uk^.lavy:=nil;uk^.pravy:=nil;
    uk^.odkaz:=p;
    if koren=nil then koren:=uk else
    if s<pred^.meno then pred^.lavy:=uk else pred^.pravy:=uk;
    najdimeno:=uk^.odkaz;
    nasiel:=false;
    ukstan:=uk;
  end;
end;

procedure vstup;
var f:text;
    r,r1,r0,r2:string;
    pop:popisspoja;
    p,n,i:byte;
    c,c0,c1,ncas,plus,plus2:word;
    jeplus,jedevlak,stop,zacspoj,novyspoj,nasiel:boolean;
    uk:puzol;
    z,ukriad,ukmeno:priadok;
    k:integer;
    ukuzol:puzol;
    ukstan:pstanica;
    jedezn:char;

begin
{  assign(f,s);reset(f);}
  pocriad:=0;stop:=false;
  novyspoj:=false;r0:='';
  jedevlak:=false;
  while not eof do begin
    if r0='' then repeat readln(r) until r<>''
      else r:=r0;
    r0:='';
    inc(pocriad);if pocriad mod 10000=0 then writeln(pocriad);
    if r[1]=#9 then delete(r,1,1);

    case r[1] of
      '#' : begin
        delete(r,1,2);
        pop:=r;
        n:=0;
        zacspoj:=true;
        ncas:=0;
      end;

      #9,'+','0'..'9' : begin
        jeplus:=false;
        p:=pos(#9,r);r1:=copy(r,1,p-1);
        if pos(':',r1)<>0 then begin
          if zacspoj then begin
            p:=pos(':',r1);val(copy(r1,1,p-1),c0,k);delete(r1,1,p);
            val(copy(r1,1,2),c1,k);ncas:=c0*60+c1;plus:=0;plus2:=0;
          end else begin
            p:=pos(':',r1);val(copy(r1,1,p-1),c0,k);delete(r1,1,p);
            val(copy(r1,1,2),c1,k);c0:=c0*60+c1;c0:=c0+1440;
            plus:=c0-ncas;plus:=plus mod 1440;
            plus2:=c0-ncas;plus2:=plus2 mod 1440;

          end;
        end else begin
          delete(r1,1,1);val(r1,c0,k);
          plus:=plus+c0;plus2:=plus2+c0;
          p:=pos(#9,r);delete(r,1,p);
          if r[1]='+' then begin
            delete(r,1,1);
            r1:=copy(r,1,pos(#9,r)-1);
            val(r1,c0,k);
            plus2:=plus2+c0;
            jeplus:=true;
          end;
        end;
        while pos(#9,r)<>0 do begin
          p:=pos(#9,r);delete(r,1,p);
        end;
        inc(n);
        riadky[n].cas:=plus;riadky[n].cas2:=plus2;
        riadky[n].meno:=r;
        zacspoj:=false;
        if jeplus then plus:=plus+c0;
      end;

      else begin
        r1:=r;
        r2:='';
        while (r1[1]<>'#')and not eof do begin
          if r1[1]='R' then r2:=r1;
          readln(r1);
        end;
        r0:=r1;
        if r2='' then jedevlak:=true
        else begin
          r2:=datum+#9+r2;
          jedezn:=jede(r2);
          if upcase(jedezn)='Y' then jedevlak:=true
            else jedevlak:=false;
        end;

        if jedevlak then begin
          uk:=nil;novyspoj:=true;
          z:=nil;
          new(ukuzol);
          ukuzol^.popis:=pop;
          for i:=n downto 1 do begin
            new(ukriad);
            ukriad^.cas:=riadky[i].cas;
            ukriad^.cas2:=riadky[i].cas2;
            ukmeno:=najdimeno(riadky[i].meno,ukriad,nasiel,ukstan);
            ukriad^.meno:=ukstan;
            if nasiel then begin
              ukriad^.dalsi:=ukmeno^.dalsi;
              ukmeno^.dalsi:=ukriad;
            end else begin
              ukriad^.dalsi:=ukriad;
            end;
            ukriad^.uzodkaz:=ukuzol;
            ukriad^.opt:=false;
            ukriad^.kon:=false;
            ukriad^.naj:=nil;
            ukriad^.nasled:=z;
            z:=ukriad;
          end;
          ukuzol^.odkaz:=ukriad;
          if zacuzol=nil then begin
            zacuzol:=ukuzol;
            ukuzol^.dalsi:=nil;
          end else begin
            ukuzol^.dalsi:=zacuzol;
            zacuzol:=ukuzol;
          end;
          ukuzol^.cas:=ncas;
        end;
        novyspoj:=false;
      end;


    end;
  end;
{  close(f);}
end;

procedure prestup(zdroj,ciel:Priadok;var prescas:word;
                  var presprav:byte);

var plus1,plus2,zcas:word;
    zprav:byte;
    akt:puzol;
    roz:integer;
    ppravd:real;
begin
  plus1:=zdroj^.cas;plus2:=ciel^.cas2;
  akt:=ciel^.uzodkaz;
  zcas:=zdroj^.naj^.cas;
  zprav:=zdroj^.naj^.pravd;
  roz:=akt^.cas+plus2-zcas;
  if roz>=0 then
    ppravd:=100-(plus1+plus2)/20+3*roz
    else if plus1+plus2=0
      then ppravd:=0
      else ppravd:=100+2400/(plus1+plus2)*roz;
  if ppravd>100 then ppravd:=100; if ppravd<0 then ppravd:=0;
  presprav:=trunc(0.01*ppravd*zprav);
  if presprav<limit then {akt:=akt^.pred;}presprav:=0;
  prescas:=akt^.cas+plus2;
{  presspoj:=akt;}
end;

function najdimin(z:pspoj):priadok;
var minc:word;
    maxp:byte;
    akt:pspoj;
begin
  if z=nil then begin najdimin:=nil; exit end;
  najdimin:=z^.odkaz;
  minc:=z^.cas;
  maxp:=z^.pravd;
  akt:=z;
  while akt<>nil do begin
    if (akt^.cas<minc) or ((akt^.cas=minc)and(akt^.pravd>maxp)) then begin
      minc:=akt^.cas;
      maxp:=akt^.pravd;
      najdimin:=akt^.odkaz;
    end;
    akt:=akt^.dalsi;
  end;
end;

procedure vypiscestu(uk:pspoj);
var akt:pspoj;
begin
  akt:=uk;
  while akt^.spat<>nil do begin
    write(akt^.spatcas div 60,':',akt^.spatcas mod 60,' ',akt^.spatprav,'% ');
    akt:=akt^.spat;
    write(akt^.odkaz^.meno^.meno,' ');
    writeln(akt^.odkaz^.uzodkaz^.popis,' ');
  end;
end;

function cyklus{(var zmena:boolean)}:boolean;
var aktcas,pomcas,scas,prescas:word;
    aktprav,presprav:byte;
    akt,uk:priadok;
    ukspoj:pspoj;
    drop:integer;
    zn:string;

begin
  cyklus:=false;
  if zacspoj=nil then writeln('zacspoj je nil');
  uk:=najdimin(zacspoj);
  if uk=nil then begin writeln('uk je nil');cyklus:=true;exit; end;

  if uk^.meno^.meno=cmeno then begin
    writeln;
    writeln('Najdena cesta:');
    writeln(cmeno,' ',uk^.naj^.cas div 60,':',uk^.naj^.cas mod 60);
    vypiscestu(uk^.naj);
    write('Dalsia moznost? (A/N)');
    readln(zn);
    if upcase(zn[1])='N' then begin
      cyklus:=true;
      ciel:=true;
      exit
    end
    else begin
      akt^.opt:=false;
      akt^.kon:=true;
      if uk^.naj^.pred<>nil
        then uk^.naj^.pred^.dalsi:=uk^.naj^.dalsi
        else zacspoj:=uk^.naj^.dalsi;
      if uk^.naj^.dalsi<>nil
        then uk^.naj^.dalsi^.pred:=uk^.naj^.pred;

      ciel:=true;
{          zmena:=true;}
      exit;
    end;
  end;

  uk^.opt:=true;
  aktcas:=uk^.naj^.cas;

  pomcas:=aktcas-uk^.cas;
  aktprav:=uk^.naj^.pravd;
  if uk^.naj^.pred<>nil
    then uk^.naj^.pred^.dalsi:=uk^.naj^.dalsi
    else zacspoj:=uk^.naj^.dalsi;
  if uk^.naj^.dalsi<>nil
    then uk^.naj^.dalsi^.pred:=uk^.naj^.pred;

  akt:=uk^.nasled;
  while akt<>nil do begin
    if akt^.opt<>true then begin
      scas:=pomcas+akt^.cas;
      if akt^.naj=nil then begin
        new(ukspoj);
        ukspoj^.cas:=scas;
        ukspoj^.pravd:=aktprav;
        ukspoj^.odkaz:=akt;
        ukspoj^.spat:=uk^.naj;
        ukspoj^.spatcas:=uk^.naj^.cas;
        ukspoj^.spatprav:=uk^.naj^.pravd;
        akt^.naj:=ukspoj;
        ukspoj^.dalsi:=zacspoj;
        ukspoj^.pred:=nil;
        zacspoj^.pred:=ukspoj;
        zacspoj:=ukspoj;
      end else
      if
         (akt^.kon and(akt^.naj^.pravd<aktprav))or
         ((akt^.naj^.cas>scas) or
         ((akt^.naj^.cas=scas) and (akt^.naj^.pravd<aktprav)))
      then begin
{         zmena:=true;}
         akt^.naj^.cas:=scas;
         akt^.naj^.pravd:=aktprav;
         akt^.naj^.spat:=uk^.naj;
         akt^.naj^.spatcas:=uk^.naj^.cas;
         akt^.naj^.spatprav:=uk^.naj^.pravd;
      end;
    end;
    akt:=akt^.nasled;
  end;

  if start then begin start:=false; end
  else begin
    akt:=uk^.dalsi;
    while akt<>uk do begin
      if akt^.opt<>true then begin
        prestup(uk,akt,prescas,presprav);
        if presprav<>0 then begin
          if akt^.naj=nil then begin
            new(ukspoj);
            ukspoj^.cas:=prescas;
            ukspoj^.pravd:=presprav;
            ukspoj^.odkaz:=akt;
            ukspoj^.spat:=uk^.naj;
            ukspoj^.spatcas:=uk^.naj^.cas;
            ukspoj^.spatprav:=uk^.naj^.pravd;
            akt^.naj:=ukspoj;
            ukspoj^.dalsi:=zacspoj;
            ukspoj^.pred:=nil;
            zacspoj^.pred:=ukspoj;
            zacspoj:=ukspoj;
          end else
            if (akt^.kon and(akt^.naj^.pravd<presprav))or
               ((akt^.naj^.cas>prescas) or
               ((akt^.naj^.cas=prescas) and(akt^.naj^.pravd<presprav)))
            then begin
{         zmena:=true;}
              akt^.naj^.cas:=prescas;
              akt^.naj^.pravd:=presprav;
              akt^.naj^.spat:=uk^.naj;
              akt^.naj^.spatcas:=uk^.naj^.cas;
              akt^.naj^.spatprav:=uk^.naj^.pravd;
            end;
        end;
      end;
      akt:=akt^.dalsi;
    end;
  end;

end;

procedure init;
var i:word;
begin
  for i:=0 to 100 do begin riadky[i].cas:=0;riadky[i].meno:=''; end;
  koren:=nil;
  zacspoj:=nil;
  ciel:=false;
  poc:=0;
  zacuzol:=nil;
end;

begin
  init;
{  write('Meno datoveho suboru:');readln(s);}
  i:=1;
  pom:=paramstr(i);
  if pos(#39,pom)<>0 then begin
    delete(pom,1,1);
    zmeno:=pom;
    repeat
      if pos(#39,zmeno)<>0 then break;
      inc(i);
      pom:=paramstr(i);
      zmeno:=zmeno+' '+pom;
    until (pos(#39,zmeno)<>0) or(i>paramcount);
    delete(zmeno,length(zmeno),1);
  end else zmeno:=pom;

  inc(i);datum:=paramstr(i);
  inc(i);cas:=paramstr(i);
  inc(i);
  pom:=paramstr(i);
  if pos(#39,pom)<>0 then begin
    delete(pom,1,1);
    cmeno:=pom;
    repeat
      if pos(#39,cmeno)<>0 then break;
      inc(i);
      pom:=paramstr(i);
      cmeno:=cmeno+' '+pom;
    until (pos(#39,cmeno)<>0) or(i>paramcount);
    delete(cmeno,length(cmeno),1);
  end else cmeno:=pom;

  inc(i);limstr:=paramstr(i);val(limstr,limit,k);
{  s:='c:\users\pid.tt';}
{   s:='h:\pid.tt';}
  if datum='' then begin
    writeln('Nekorektne parametre - datum nastaveny na 01.10.2001');
    datum:='01.10.2001'
  end;

{  vstup(s);}
    vstup;
  repeat
    ciel:=false;
    if zmeno='' then begin write('Zaciatocna stanica:');readln(zmeno);end;
    if cmeno='' then begin write('Cielova stanica:');readln(cmeno);end;
    if cas='' then begin write('Cas odchodu:(min:sec):');readln(cas);end;
    if limit=0 then begin write('Limit pravdepodobnosti:');readln(limit);end;
    p:=pos(':',cas);
    val(copy(cas,1,p-1),min,k);
    val(copy(cas,p+1,length(cas)-p),sec,k);
    startcas:=min*60+sec;

    ukmeno:=najdimeno(cmeno,xx,nasiel,yy);
    if not nasiel then begin
      writeln('Cielova stanica sa v sieti nenachadza!');
      exit;
    end;

    ukmeno:=najdimeno(zmeno,xx,nasiel,yy);
    if not nasiel then begin
      writeln('Zaciatocna stanica sa v sieti nenachadza!');
      exit;
    end;
    if zmeno=cmeno then begin
      writeln('Nie je co riesit!');
      exit;
    end;
    akt:=ukmeno;
    new(zdroj);
    new(zdroj^.naj);
    zdroj^.cas:=0;
    zdroj^.naj^.cas:=startcas;
    zdroj^.naj^.pravd:=100;
    repeat
      prestup(zdroj,akt,prescas,presprav);
      if presprav<>0 then begin
        new(ukspoj);
        ukspoj^.cas:=prescas;
        ukspoj^.pravd:=presprav;
        ukspoj^.odkaz:=akt;
        ukspoj^.spat:=nil;
        ukspoj^.spatcas:=0;ukspoj^.spatprav:=0;

        akt^.naj:=ukspoj;
        if zacspoj=nil then begin
          zacspoj:=ukspoj;
          ukspoj^.dalsi:=nil;
          ukspoj^.pred:=nil;
        end else begin
          ukspoj^.dalsi:=zacspoj;
          ukspoj^.pred:=nil;
          zacspoj^.pred:=ukspoj;
          zacspoj:=ukspoj;
        end;
      end;
      akt:=akt^.dalsi;
    until akt=ukmeno;
    start:=true;
    repeat
      koniec:=cyklus{(zmena);if not zmena then koniec:=true;}
    until koniec;
    if not ciel then writeln('Medzi zastavkami nevedie cesta!');
{    if upcase(zn[1])='N' then} exit;
  until false;
end.
