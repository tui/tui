program radyttz;

uses datum,exists3;

type Tsekce=(skip,head,towns,conns_names,brem,rrem,notes,dep,conns);
     Tznaky=set of char;
const sekcestr:array [skip..conns] of string[10]=
        ('xxx','TTZIP','TOWNS','CONN','BREM','RREM','NOTES','DEP','CONNS');
     nekonecno=12345678;
     absolutni=1500;
     maxdnu=2;
type Tmestoid=integer;
     Tspojid=longint;
     Tnoteid=integer;
     Todjezdid=longint;
     Tvzdalenost=integer;
type Tnazevmesta=string[40];
     Tnazevspoje=string[25];
     Tnotestr=string[5];
     Ptrasa=^Ttrasa;
     Podkaz=^Todkaz;
     Ppolemest=^Tpolemest;
     Ppolenazvu=^Tpolenazvu;
     Pmesta=^Tmesta;
     Ppolenotes=^Tpolenotes;
     Pnotes=^Tnotes;
     Ppoleodjezdu=^Tpoleodjezdu;
     Podjezdy=^Todjezdy;
     Ttrasa=record
       zast:Tmestoid;
       nazev:Tnazevspoje;
       spojstr:string;
       dalsi:Ptrasa;
     end;
     Todkaz=record
       p:Ptrasa;
       dalsi:Podkaz;
     end;
     Tmesto=record
       prestupu:integer;
       spoj:Tspojid;
       predchudce:Tmestoid;
       prijezd,odjezd:Tcas;
       dep0:Todjezdid;
     end;
     Tpolemest=array [1..nekonecno] of Tmesto;
     Tpolenazvu=array [1..nekonecno] of Tnazevmesta;
     Tmesta=object
       celkem,nacteno:Tmestoid;
       pm:Ppolemest;
       pn:Ppolenazvu;
       procedure init(pocet:Tmestoid);
       function vratdep(mestoid:Tmestoid;relativni:Todjezdid):Todjezdid;
       procedure zapismesto(id:Tmestoid;prest:integer;sp:Tspojid;pred:Tmestoid;prij,odj:Tcas);
       procedure pridejodjezd(index:Todjezdid;mesto:Tmestoid);
       procedure nazvyinit;
       procedure pridejnazev(s:Tnazevmesta);
       function id_nazev(id:Tmestoid):Tnazevmesta;
       function nazev_id(nazev:Tnazevmesta):Tmestoid;
       procedure nazvydone;
       procedure done;
     end;
     Tpolenotes=array [1..nekonecno] of Tnotestr;
     Tnotes=object
       celkem,nacteno:Tnoteid;
       pn:Ppolenotes;
       procedure init(pocet:Tnoteid);
       function id_notestr(id:Tnoteid):Tnotestr;
       procedure pridej(s:Tnotestr);
       procedure done;
     end;
     Todjezd=record
       kam:Tmestoid;
       wait,time:Tcas;
       km:Tvzdalenost;
       noteid:Tnoteid;
     end;
     Tpoleodjezdu=array [1..nekonecno] of Todjezd;
     Todjezdy=object
       celkem,nacteno:Todjezdid;
       poslednimesto:Tmestoid;
       po:Ppoleodjezdu;
       procedure init(pocet:Todjezdid);
       procedure pridej(s:string;pm:Pmesta);
       procedure done;
     end;
     Tjizdnirad=object
       f:text;
       pm:Pmesta;
       pp:Ppoznamky;
       pn:Pnotes;
       po:Podjezdy;
       function init(fname:string):integer;
       function nastavsoubor(sekce:Tsekce;resetuj:boolean):boolean;
       function konec(var s:string):boolean;
       function hlavicka(radka:integer):longint;
       procedure nactimesta;
       procedure nactipoznamky;
       procedure nactinotes;
       procedure nactiodjezdy;
       function relcas(cas,abscas:Tcas):Tcas;
       procedure zpracujradku(var s:string;prest:integer;spojid:Tspojid;d:Tdatum);
       procedure mergesort(var p:Podkaz);
       procedure rekonstrukce(start,cil:Tmestoid;tab:boolean);
       function hledej(odkud,kam:Tnazevmesta;d:Tdatum;vyjezd:Tcas;prestupu:integer;fname:string;tab:boolean):integer;
       procedure done;
     end;

function rline(var f:text):string;
  var c:char;
      s:string;
begin
  s:='';
  read(f,c);
  while (c<>#10) and (c<>#26) do
  begin
    s:=s+c;
    read(f,c);
  end;
  rline:=s;
end;





{-----------------------------Tmesta---------------------------------}
procedure Tmesta.init(pocet:Tmestoid);
  var i:Tmestoid;
begin
  celkem:=pocet;
  nacteno:=0;
  getmem(pm,sizeof(Tmesto)*celkem);
  for i:=1 to celkem do begin
    pm^[i].prestupu:=maxint;
    pm^[i].dep0:=0;
  end;
  pn:=nil;
end;

function Tmesta.vratdep(mestoid:Tmestoid;relativni:Todjezdid):Todjezdid;
begin
  vratdep:=pm^[mestoid].dep0+relativni;
end;

procedure Tmesta.zapismesto(id:Tmestoid;prest:integer;sp:Tspojid;pred:Tmestoid;prij,odj:Tcas);
begin
  with pm^[id] do begin
    prestupu:=prest;
    spoj:=sp;
    predchudce:=pred;
    prijezd:=prij;
    odjezd:=odj;
  end;
end;

procedure Tmesta.pridejodjezd(index:Todjezdid;mesto:Tmestoid);
begin
  pm^[mesto].dep0:=index;
end;

procedure Tmesta.nazvyinit;
begin
  getmem(pn,sizeof(Tnazevmesta)*celkem);
end;

procedure Tmesta.pridejnazev(s:Tnazevmesta);
begin
  inc(nacteno);
  pn^[nacteno]:=s;
end;

function Tmesta.id_nazev(id:Tmestoid):Tnazevmesta;
begin
  id_nazev:='';
  if pn<>nil then
    id_nazev:=pn^[id];
end;

function Tmesta.nazev_id(nazev:Tnazevmesta):Tmestoid;
  var min,max:Tmestoid;
begin
  min:=1;
  max:=celkem;
  while min<max do
    if nazev<=id_nazev((min+max)div 2) then
      max:=(min+max)div 2
    else
      min:=((min+max)div 2)+1;
  if nazev<>id_nazev(min) then
    nazev_id:=0
  else
    nazev_id:=min;
end;

procedure Tmesta.nazvydone;
begin
  nacteno:=0;
  if pn<>nil then begin
    freemem(pn,sizeof(Tnazevmesta)*celkem);
    pn:=nil;
  end;
end;

procedure Tmesta.done;
begin
  if pm<>nil then begin
    freemem(pm,sizeof(Tmesto)*celkem);
    pm:=nil;
  end;
  nazvydone;
end;

{-----------------------------Tnotes---------------------------------}
procedure Tnotes.init(pocet:Tnoteid);
begin
  celkem:=pocet;
  nacteno:=0;
  pn:=nil;
  if celkem>0 then
    getmem(pn,sizeof(Tnotestr)*celkem);
end;

function Tnotes.id_notestr(id:Tnoteid):Tnotestr;
begin
  id_notestr:='??';
  if (id<>0)and(pn<>nil) then
    id_notestr:=pn^[id];
end;

procedure Tnotes.pridej(s:Tnotestr);
begin
  inc(nacteno);
  pn^[nacteno]:=s;
end;

procedure Tnotes.done;
begin
  nacteno:=0;
  if pn<>nil then begin
    freemem(pn,sizeof(Tnotestr)*celkem);
    pn:=nil;
  end;
end;

{-----------------------------Todjezdy-------------------------------}
procedure Todjezdy.init(pocet:Todjezdid);
begin
  celkem:=pocet;
  nacteno:=0;
  poslednimesto:=0;
  getmem(po,sizeof(Todjezd)*celkem);
end;

procedure Todjezdy.pridej(s:string;pm:Pmesta);
  var odkud:Tmestoid;
      chyba:integer;
      wait,time:Tcas;
begin
  val(prvni(#9,s),odkud,chyba);
  if odkud<>poslednimesto then begin
    pm^.pridejodjezd(nacteno,odkud);
    poslednimesto:=odkud;
  end;
  inc(nacteno);
  val(prvni(#9,s),wait,chyba);
  val(prvni(#9,s),time,chyba);
  if time=0 then begin
    time:=wait;
    wait:=0;
  end;
  po^[nacteno].wait:=wait;
  po^[nacteno].time:=time;
  val(prvni(#9,s),po^[nacteno].km,chyba);
  val(prvni(#9,s),po^[nacteno].kam,chyba);
  val(prvni(#9,s),po^[nacteno].noteid,chyba);
end;

procedure Todjezdy.done;
begin
  nacteno:=0;
  if po<>nil then begin
    freemem(po,sizeof(Todjezd)*celkem);
    po:=nil;
  end;
end;

{-----------------------------Tjizdnirad-----------------------------}
function Tjizdnirad.init(fname:string):integer;
begin
  writeln('inicializace...');
  init:=0;
  assign(f,fname);
  {$I-}
  reset(f);
  {$I+}
  if ioresult<>0 then begin
    init:=1;
    exit;
  end;
  new(pm);
  pm^.init(hlavicka(2));
  new(pp);
  pp^.init(hlavicka(4));
  new(pn);
  pn^.init(hlavicka(7));
  new(po);
  po^.init(hlavicka(1));
end;

function Tjizdnirad.nastavsoubor(sekce:Tsekce;resetuj:boolean):boolean;
  var resetovano:boolean;
      s:string; i:integer;pom:string;
begin
  nastavsoubor:=true;
  if resetuj then begin
    reset(f);
    resetovano:=true;
  end else
    resetovano:=false;
  repeat
    while not eof(f) do begin
      s:=rline(f);
      if copy(s,1,2)='# ' then
        if copy(s,3,length(s)-2)=sekcestr[sekce] then
          exit;
    end;
    if resetovano then
      break
    else begin
      reset(f);
      resetovano:=true;
    end;
  until false;
  nastavsoubor:=false;
end;

function Tjizdnirad.konec(var s:string):boolean;
begin
  konec:=(copy(s,1,2)='# ')or(s='');
end;

function Tjizdnirad.hlavicka(radka:integer):longint;
  var chyba,i:integer;
      navrat:longint;
begin
  nastavsoubor(head,true);
  for i:=1 to radka-1 do
    rline(f);
  val(rline(f),navrat,chyba);
  if chyba<>0 then
    navrat:=-1;
  hlavicka:=navrat;
end;

procedure Tjizdnirad.nactimesta;
  var i:Tmestoid;
begin
  writeln('nacitam mesta...');
  pm^.nazvyinit;
  nastavsoubor(towns,true);
  for i:=1 to pm^.celkem do
    pm^.pridejnazev(rline(f));
end;

procedure Tjizdnirad.nactipoznamky;
  var i:Tremid;
      s:string;
begin
  writeln('nacitam poznamky <RREM>...');
  nastavsoubor(rrem,false);
  repeat
    s:=rline(f);
    if konec(s) then
      break;
    pp^.pridej(s);
  until false
end;

procedure Tjizdnirad.nactinotes;
  var i:Tnoteid;
begin
  writeln('nacitam poznamky <NOTES>...');
  nastavsoubor(notes,false);
  for i:=1 to pn^.celkem do
    pn^.pridej(rline(f));
end;

procedure Tjizdnirad.nactiodjezdy;
  var i:Todjezdid;
begin
  writeln('nacitam odjezdy...');
  nastavsoubor(dep,false);
  for i:=1 to po^.celkem do
    po^.pridej(rline(f),pm);
end;

function Tjizdnirad.relcas(cas,abscas:Tcas):Tcas;
  var navrat:Tcas;
begin
  navrat:=cas;
  if navrat>=absolutni then begin
    navrat:=(navrat-absolutni)-(abscas mod 1440);
    if navrat<0 then
      inc(navrat,1440);
  end;
  relcas:=navrat;
end;

procedure Tjizdnirad.zpracujradku(var s:string;prest:integer;spojid:Tspojid;d:Tdatum);
  type Puzel=^Tuzel;
       Tuzel=record
         mestoid:Tmestoid;
         odjvlak,odjspoj:Tcas;
         dalsi:Puzel;
       end;
  var zast,kam:Tmestoid;
      cas,cekani,dobajizdy:Tcas;
      chyba:integer;
      list,remstr:string;
      zac,pu:Puzel;
      odjid:Todjezdid;
      remid:Tremid;
  procedure pridejuzel; {!!! pracuje s promennymi proc zpracujradku}
    var odjezd:Tcas;
        zbyva:integer;
        pomdatum:Tdatum;
        vysledek:boolean;
  begin
    cekani:=(cas mod 1440)-(pm^.pm^[zast].prijezd mod 1440);
    if cekani<0 then
      inc(cekani,1440);
    odjezd:=pm^.pm^[zast].prijezd+cekani;
    if remid<>0 then begin
      zbyva:=maxdnu;
      repeat
        opravdatum(d,odjezd,pomdatum);
        vysledek:=pp^.test(pomdatum,d,remid);
        if not vysledek then begin
          if zbyva=0 then
            exit;
          dec(zbyva);
          inc(odjezd,1440);
        end;
      until vysledek;
    end;
    new(pu);
    pu^.mestoid:=zast;
    pu^.odjvlak:=cas;
    pu^.odjspoj:=odjezd;
    pu^.dalsi:=zac;
    zac:=pu;
  end;
begin
  val(prvni(#9,s),zast,chyba);
  val(prvni(#9,s),cas,chyba);
  dec(cas,absolutni);
  prvni(#9,s); {noteid}
  prvni(#9,s); {Brem}
  remstr:=prvni(#9,s); {Rrem}
  val(prvni(';',remstr),remid,chyba);
  prvni(#9,s); {corp}
  prvni(#9,s); {seznam zastavek oddeluji dva znaky #9}
  list:=prvni(#9,s);
  zac:=nil;
  if pm^.pm^[zast].prestupu=prest-1 then
    pridejuzel;
  while list<>'' do begin
    val(prvni(' ',list),odjid,chyba);
    odjid:=pm^.vratdep(zast,odjid);
    dobajizdy:=relcas(po^.po^[odjid].time,cas);
    inc(cas,dobajizdy);
    kam:=po^.po^[odjid].kam;
    pu:=zac;
    while pu<>nil do begin
      if (pu^.odjspoj+(cas-pu^.odjvlak)<pm^.pm^[kam].prijezd)or(pm^.pm^[kam].prestupu=maxint) then
        pm^.zapismesto(kam,prest,spojid,pu^.mestoid,pu^.odjspoj+(cas-pu^.odjvlak),pu^.odjspoj);
      pu:=pu^.dalsi;
    end;
    zast:=kam;
    cekani:=relcas(po^.po^[odjid].wait,cas);
    inc(cas,cekani);
    if (list<>'')and(pm^.pm^[zast].prestupu=prest-1) then
      pridejuzel;
  end;
  while zac<>nil do begin
    pu:=zac;
    zac:=zac^.dalsi;
    dispose(pu);
  end;
end;

procedure Tjizdnirad.mergesort(var p:Podkaz);
  var q:array [0..1] of Podkaz;
      p2,pom:Podkaz;
      i:integer;
begin
  if p=nil then
    exit;
  if p^.dalsi=nil then
    exit;
  for i:=0 to 1 do
    q[i]:=nil;
  i:=0;
  while p<>nil do begin
    pom:=p;
    p:=p^.dalsi;
    pom^.dalsi:=q[i];
    q[i]:=pom;
    inc(i);
    if i>1 then
      i:=0;
  end;
  for i:=0 to 1 do
    mergesort(q[i]);
  while (q[0]<>nil)and(q[1]<>nil) do begin
    if pm^.pm^[q[0]^.p^.zast].spoj<pm^.pm^[q[1]^.p^.zast].spoj then
      i:=0
    else
      i:=1;
    pom:=q[i];
    q[i]:=q[i]^.dalsi;
    pom^.dalsi:=p;
    p:=pom;
  end;
  for i:=0 to 1 do
    while q[i]<>nil do begin
      pom:=q[i];
      q[i]:=q[i]^.dalsi;
      pom^.dalsi:=p;
      p:=pom;
    end;
  p2:=nil;
  while p<>nil do begin
    pom:=p;
    p:=p^.dalsi;
    pom^.dalsi:=p2;
    p2:=pom;
  end;
  p:=p2;
end;

procedure Tjizdnirad.rekonstrukce(start,cil:Tmestoid;tab:boolean);
  type Tremrec=record
         id:Tremid;
         s:string;
       end;
       Prempole=^Trempole;
       Trempole=array [1..nekonecno] of Tremrec;

  procedure zprav(p:Prempole;index,poc:Tremid);
    var vymen:Tremid;
        pom:Tremrec;
  begin
    repeat
      vymen:=0;
      if index*2<=poc then
        if p^[index].id<p^[index*2].id then
          vymen:=index*2;
      if index*2+1<=poc then
        if (p^[index].id<p^[index*2+1].id)and(p^[index*2].id<p^[index*2+1].id) then
          vymen:=index*2+1;
      if vymen<>0 then begin
        pom:=p^[index];
        p^[index]:=p^[vymen];
        p^[vymen]:=pom;
        index:=vymen;
      end;
    until vymen=0;
  end;

  procedure sort(p:Prempole;poc:Tremid);
    var i:Tremid;
        pom:Tremrec;
  begin
    for i:=poc downto 1 do
      zprav(p,i,poc);
    while poc>0 do begin
      pom:=p^[1];
      p^[1]:=p^[poc];
      p^[poc]:=pom;
      dec(poc);
      zprav(p,1,poc);
    end;
  end;

  function remid_remstr(p:Prempole;id,poc:Tremid):string;
    var min,max:Tremid;
  begin
    min:=1;
    max:=poc;
    while min<max do
      if p^[(min+max)div 2].id>=id then
        max:=(min+max)div 2
      else
        min:=(min+max)div 2+1;
    remid_remstr:=p^[min].s;
  end;

  var ptzac,pt:Ptrasa;
      zast,pomzast,mesto,relativni:Tmestoid;
      nazevmesta:string;
      pocetznaku:integer;
      podkzac,podk:Podkaz;
      sp,sphled:Tspojid;
      s,list,pomlist,remstr:string;
      chyba:integer;
      cas,vlakcas,cekani,jizda:Tcas;
      depid:Todjezdid;
      delka,celkdelka:Tvzdalenost;
      noteid:Tnoteid;
      pocet:array [brem..rrem] of Tremid;
      pole:array [brem..rrem] of Prempole;
      i:integer;
      typ:Tsekce;
      remid,index:Tremid;
      strpole:array [brem..rrem] of string;
begin
  {vytvoreni spojaku mest}
  ptzac:=nil;
  mesto:=cil;
  while mesto<>start do begin
    new(pt);
    pt^.zast:=mesto;
    pt^.dalsi:=ptzac;
    ptzac:=pt;
    mesto:=pm^.pm^[mesto].predchudce;
  end;
  {dohledani conns}
  podkzac:=nil;
  pt:=ptzac;
  while pt<>nil do begin
    new(podk);
    podk^.p:=pt;
    podk^.dalsi:=podkzac;
    podkzac:=podk;
    pt:=pt^.dalsi;
  end;
  mergesort(podkzac);
  nastavsoubor(conns_names,true);
  podk:=podkzac;
  if podk<>nil then
    sphled:=pm^.pm^[podk^.p^.zast].spoj;
  sp:=0;
  while podk<>nil do begin
    inc(sp);
    s:=rline(f);
    if sp=sphled then begin
      podk^.p^.nazev:=s;
      podk:=podk^.dalsi;
      if podk<>nil then
        sphled:=pm^.pm^[podk^.p^.zast].spoj;
    end;
  end;
  nastavsoubor(conns,false);
  podk:=podkzac;
  if podk<>nil then
    sphled:=pm^.pm^[podk^.p^.zast].spoj;
  sp:=0;
  while podk<>nil do begin
    inc(sp);
    s:=rline(f);
    if sp=sphled then begin
      podk^.p^.spojstr:=s;
      podk:=podk^.dalsi;
      if podk<>nil then
        sphled:=pm^.pm^[podk^.p^.zast].spoj;
    end;
  end;
  while podkzac<>nil do begin
    podk:=podkzac;
    podkzac:=podkzac^.dalsi;
    dispose(podk);
  end;
  {dohledani rem}
  for typ:=brem to rrem do
    pocet[typ]:=0;
  pt:=ptzac;
  while pt<>nil do begin
    s:=pt^.spojstr;
    for i:=1 to 3 do
      prvni(#9,s);
    for typ:=brem to rrem do begin
      remstr:=prvni(#9,s);
      while remstr<>'' do begin
        val(prvni(';',remstr),remid,chyba);
        if remid<>0 then
          inc(pocet[typ]);
      end;
    end;
    pt:=pt^.dalsi;
  end;
  for typ:=brem to rrem do begin
    pole[typ]:=nil;
    if pocet[typ]<=0 then
      continue;
    getmem(pole[typ],sizeof(Tremrec)*pocet[typ]);
    index:=0;
    pt:=ptzac;
    while pt<>nil do begin
      s:=pt^.spojstr;
      for i:=1 to 3+(ord(typ)-ord(brem)) do
        prvni(#9,s);
      remstr:=prvni(#9,s);
      while remstr<>'' do begin
        val(prvni(';',remstr),remid,chyba);
        if remid<>0 then begin
          inc(index);
          pole[typ]^[index].id:=remid;
        end;
      end;
      pt:=pt^.dalsi;
    end;
    sort(pole[typ],pocet[typ]);
  end;
  nastavsoubor(head,true);
  for typ:=brem to rrem do begin
    nastavsoubor(typ,false);
    index:=1;
    remid:=0;
    while index<=pocet[typ] do begin
      s:=rline(f);
      inc(remid);
      while pole[typ]^[index].id=remid do begin
        pole[typ]^[index].s:=s;
        inc(index);
        if index>pocet[typ] then
          break;
      end;
    end;
  end;
  {vypis}
  mesto:=start;
  pt:=ptzac;
  while pt<>nil do begin
    writeln;
    writeln(pt^.nazev);
    writeln(' z: ',pm^.id_nazev(mesto));
    writeln('do: ',pm^.id_nazev(pt^.zast));
    val(prvni(#9,pt^.spojstr),zast,chyba); {prvni mesto}
    val(prvni(#9,pt^.spojstr),vlakcas,chyba); {cas vyjezdu}
    dec(vlakcas,absolutni);
    val(prvni(#9,pt^.spojstr),noteid,chyba); {noteid}
    strpole[brem]:=prvni(#9,pt^.spojstr); {Brem}
    strpole[rrem]:=prvni(#9,pt^.spojstr); {Rrem}
    prvni(#9,pt^.spojstr); {corp}
    prvni(#9,pt^.spojstr); {seznam zastavek oddeluji dva znaky #9}
    list:=prvni(#9,pt^.spojstr);
    while zast<>mesto do begin
      val(prvni(' ',list),relativni,chyba);
      depid:=pm^.vratdep(zast,relativni);
      jizda:=relcas(po^.po^[depid].time,vlakcas);
      inc(vlakcas,jizda);
      cekani:=relcas(po^.po^[depid].wait,vlakcas);
      inc(vlakcas,cekani);
      zast:=po^.po^[depid].kam;
    end;
    pomzast:=zast;
    pomlist:=list;
    pocetznaku:=length(pm^.id_nazev(pomzast));
    while pomzast<>pt^.zast do begin
      val(prvni(' ',pomlist),relativni,chyba);
      depid:=pm^.vratdep(pomzast,relativni);
      pomzast:=po^.po^[depid].kam;
      if pocetznaku<length(pm^.id_nazev(pomzast)) then
        pocetznaku:=length(pm^.id_nazev(pomzast));
    end;
    write(cas_prazdny,'   ');
    cas:=pm^.pm^[pt^.zast].odjezd;
    celkdelka:=0;
    while zast<>pt^.zast do begin
      nazevmesta:=pm^.id_nazev(zast);
      if tab then
        nazevmesta:=nazevmesta+#9
      else begin
        while length(nazevmesta)<pocetznaku do
          nazevmesta:=nazevmesta+' ';
        nazevmesta:=nazevmesta+' ';
      end;
      write(cas_casstr(cas),#9,nazevmesta);
      val(prvni(' ',list),relativni,chyba);
      depid:=pm^.vratdep(zast,relativni);
      jizda:=relcas(po^.po^[depid].time,vlakcas);
      inc(vlakcas,jizda);
      inc(cas,jizda);
      delka:=po^.po^[depid].km;
      inc(celkdelka,delka);
      write(delka:3,' km, ',round(delka/(jizda/60)):3,' km/h');
      if po^.po^[depid].noteid<>0 then
        write(' ',pn^.id_notestr(po^.po^[depid].noteid));
      writeln;
      write(cas_casstr(cas));
      cekani:=relcas(po^.po^[depid].wait,vlakcas);
      inc(vlakcas,cekani);
      inc(cas,cekani);
      zast:=po^.po^[depid].kam;
      if zast<>pt^.zast then
        write(' - ');
    end;
    writeln('   ',cas_prazdny,#9,pm^.id_nazev(pt^.zast));
    if noteid<>0 then
      writeln(pn^.id_notestr(noteid));
    writeln('celkem: ',celkdelka,' km, ',cas_casstr(pm^.pm^[pt^.zast].prijezd-pm^.pm^[pt^.zast].odjezd));
    for typ:=brem to rrem do
      while strpole[typ]<>'' do begin
        val(prvni(';',strpole[typ]),remid,chyba);
        if remid<>0 then
          writeln(remid_remstr(pole[typ],remid,pocet[typ]));
      end;
    mesto:=pt^.zast;
    pt:=pt^.dalsi;
  end;
  for typ:=brem to rrem do
    if pole[typ]<>nil then begin
      freemem(pole[typ],sizeof(Tremrec)*pocet[typ]);
      pole[typ]:=nil;
    end;
  while ptzac<>nil do begin
    pt:=ptzac;
    ptzac:=ptzac^.dalsi;
    dispose(pt);
  end;
end;

function Tjizdnirad.hledej(odkud,kam:Tnazevmesta;d:Tdatum;vyjezd:Tcas;prestupu:integer;fname:string;tab:boolean):integer;
  var start,cil:Tmestoid;
      i:integer;
      s:string;
      spojid:Tspojid;
begin
  hledej:=0;
  if init(fname)<>0 then begin
    hledej:=1; {nelze otevrit soubor fname}
    exit;
  end;
  nactimesta;
  start:=pm^.nazev_id(odkud);
  if start=0 then begin
    hledej:=2; {nenalezeno odkud}
    done;
    exit;
  end;
  cil:=pm^.nazev_id(kam);
  if cil=0 then begin
    hledej:=3; {nenalezeno kam}
    done;
    exit;
  end;
  if start=cil then begin
    hledej:=4; {odkud=kam}
    done;
    exit;
  end;
  nactipoznamky;
  nactinotes;
  nactiodjezdy;
  denvtydnu(d,stddatum);
  pm^.zapismesto(start,-1,0,0,vyjezd,0);
  for i:=0 to prestupu do begin
    writeln(i+1,'. cteni...');
    nastavsoubor(conns,false);
    spojid:=0;
    while not eof(f) do begin
      inc(spojid);
      s:=rline(f);
      if konec(s) then
        break;
      zpracujradku(s,i,spojid,d);
    end;
  end;
  if pm^.pm^[cil].prestupu=maxint then begin
    hledej:=5; {spojeni nenalezeno}
    done;
    exit;
  end;
  writeln('spojeni nalezeno');
  rekonstrukce(start,cil,tab);
  done;
end;

procedure Tjizdnirad.done;
begin
  close(f);
  pm^.done;
  dispose(pm);
  pp^.done;
  dispose(pp);
  pn^.done;
  dispose(pn);
  po^.done;
  dispose(po);
end;

var rad:Tjizdnirad;
    d:Tdatum;
    vysledek:integer;
    s:string;
    cas:Tcas;
    prestupy,chyba:integer;
    parametry,tab:boolean;

BEGIN
  parametry:=false;
  tab:=false;
  if paramcount=6 then
    parametry:=true;
  if (paramcount=7)and(paramstr(7)='-u') then begin
    parametry:=true;
    tab:=true;
  end;
  if not parametry then begin
    s:=paramstr(0);
    writeln(posledni('\',s),' odkud datum cas kam max.prestupu ttz_soubor [-u]');
    exit;
  end;
  if not datumstr_datum(paramstr(2),d) then begin
    writeln('zadano neplatne datum "',paramstr(2),'"');
    exit;
  end;
  cas:=casstr_cas(paramstr(3));
  if cas=-1 then begin
    writeln('zadan neplatny cas "',paramstr(3),'"');
    exit;
  end;
  val(paramstr(5),prestupy,chyba);
  if (prestupy<0)or(chyba<>0) then begin
    writeln('neplatny pocet prestupu: "',paramstr(5),'"');
    exit;
  end;
  vysledek:=rad.hledej(paramstr(1),paramstr(4),d,cas,prestupy,paramstr(6),tab);
  if vysledek<>0 then
    write('CHYBA ',vysledek,': ');
  case vysledek of
    1:writeln('soubor ',paramstr(6),' nelze otevrit');
    2:writeln('vychozi zastavka "',paramstr(1),'" nenalezena');
    3:writeln('cilova zastavka "',paramstr(4),'" nenalezena');
    4:writeln('<odkud> a <kam> se musi lisit: "',paramstr(1),'"');
    5:writeln('spojeni nenalezeno');
  end;
END.
