/*
 *  Search for Optimal Connection in Transport Network
 *
 *  Copyright (c) 2000 Martin Mares <mj@ucw.cz>
 *  Copyright (c) 2000, 2001 Pavel Machek <pavel@ucw.cz>
 *  Copyright (c) 2001 Johanka Dolezalova <johanka@ucw.cz>
 *  This program can be distributed/used under GPLv2 or higher.
 *
 * mkt script can prepare source files for this.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include "exists.c"

static unsigned char toascii_hi[] =
                           {'|', 'A', '~', 'L', '$', 'L', 'S', '$', '\"', 'S',  /*160-9*/
	                    'S', 'T', 'Z', '-', 'Z', 'Z', '~', 'a', ',', 'l',   /*170-9*/
	                    '\'', 'l', 's', '~', ',', 's', 's', 't', 'z', ' ',  /*180-9*/
	                    'z', 'z', 'R', 'A', 'A', 'A', 'A', 'L', 'C', 'C',   /*190-9*/
	                    'C', 'E', 'E', 'E', 'E', 'I', 'I', 'D', 'D', 'N',   /*200-9*/
	                    'N', 'O', 'O', 'O', 'O', 'x', 'R', 'U', 'U', 'U',   /*210-9*/
	                    'U', 'Y', 'T', 's', 'r', 'a', 'a', 'a', 'a', 'l',   /*220-9*/
	                    'c', 'c', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'd',   /*230-9*/
	                    'd', 'n', 'n', 'o', 'o', 'o', 'o', ':', 'r', 'u',   /*240-9*/
	                    'u', 'u', 'u', 'y', 't'};                           /*250-4*/
                                                          /* NEW */

unsigned char iso88592_to_ascii(unsigned char x)
{
  return ((x >= 160) && (x < 255)) ? (toascii_hi[x-160]) : (x);
}

struct town {
  char *name;
};
static int numtowns;
static struct town *towns;

struct conn {
  char *name;
  char *cond;
};
static int numconns;
static struct conn *conns;

struct event {
  unsigned short time;		/* Minutes since midnight */
  unsigned short town;		/* Town this event belongs to */
  int conn;			/* Connection, -1=incoming event */
  int prev;			/* Previous incoming event in the same town */
  int dual;			/* Dual event to this one */
};
static int numevents;
static struct event *events;
static int events_size;

struct track {			/* Optimal connection to given event from the start */
  unsigned int badness;
  int backlink;
};
static struct track *tracks;
static struct tm date;
static char *inputfile = NULL;

#define INFTY 0x70000000

static int start_time, stop_time;
static char *opts = "";
static int *start_penalty, *stop_penalty;

static long long steps, walks, skipped;
static long run_time;

#undef DEBUG

static void __attribute__((noreturn))
die(char *c, ...)
{
  va_list args;
  va_start(args, c);
  vfprintf(stderr, c, args);
  fputc('\n', stderr);
  exit(1);
}

static void *
xmalloc(unsigned size)
{
  void *x = malloc(size);
  if (!x)
    die("Out of memory (allocation of %d bytes failed)", size);
  return x;
}

static void
read_timetable(char *name)
{
  FILE *f;
  int i, id, time, lasttime = -1, townnum, connnum, fromline, lastline;
  char buf[1024], buf2[1024], *c;
  struct event *ev;

  if (name)
    f = fopen(name, "r");
  else
    f = stdin;
  if (!f)
    die("Unable to open %s: %m", name);

  fscanf(f, "%d", &numtowns);
  fprintf(stderr, "Reading %d towns (%dKB), ", numtowns, numtowns * sizeof(struct town) / 1024);
  towns = xmalloc(sizeof(struct town) * numtowns);
  for (i=0; i<numtowns; i++)
    {
      fscanf(f, "%d", &id);
      fgetc(f);
      fgets(buf, sizeof(buf), f);
      if ((c = strchr(buf, '\n')))
	*c = 0;
      towns[id].name = strdup(buf);
    }

  fscanf(f, "%d", &numconns);
  fprintf(stderr, "%d connections (%dKB), ", numconns, numconns * sizeof(struct conn) / 1024);
  conns = xmalloc(sizeof(struct conn) * numconns);
  for (i=0; i<numconns; i++)
    {
      fscanf(f, "%d", &id);
      fgetc(f);
      fgets(buf, sizeof(buf), f);
      if ((c = strchr(buf, '\t')))
        {
          strcpy(buf2, ++c);
	  *c = 0;
	  if ((c = strchr(buf2, '\t')))
            *c = 0;
        }

      conns[id].name = strdup(buf);
      conns[id].cond = strdup(buf2);
    }

  if (fscanf(f, "%d", &numevents) != 1) {
    fprintf(stderr, "can not get number of events!\n");
    fgets(buf, sizeof(buf), f);
    fprintf(stderr, "near this text: %s\n", buf);
  }
    
  fgets(buf, sizeof(buf), f);
  events_size = sizeof(struct event) * numevents;
  fprintf(stderr, "%d events (%dKB).\n", numevents, events_size / 1024);

  if (strchr(opts, 'm')) {
    int h = open("travel.swp", O_RDWR);
    ev = events = mmap(0, events_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, h, 0);
    if (ev == ~0)
      die("Could not mmap .swp file: %m (%lx)", ev);
    close(h);
    return;
  } else ev = events = xmalloc(events_size);

  if (strchr(opts, 'r')) {
    int h = open("travel.swp", O_RDONLY);
    if (read(h, events, events_size) != events_size)
      die("Could not read .swp file: %m");
    close(h);
    return;
  } 

  for(i=0; i<numevents; i++)
    {
      switch (fgetc(f))
	{
	case '>':	/* *-> */
	  fscanf(f, "%d\t%d\t%d\t%d\n", &time, &townnum, &connnum, &lastline );
	  if (time > lasttime+10)
	    {
	      fprintf( stderr, "Reading %2d:%02d (%3d%%)...\r", time/60, time%60, (i*100)/numevents );
	      lasttime = time;
	    }
	  ev->time = time;
	  ev->town = townnum;
	  ev->conn = connnum;
	  ev->dual = -1;
	  ev->prev = lastline;
	  if (lastline >= numevents)
	    die("Invalid L cross-reference: %d > %d", lastline, numevents);
	  break;
	case '<':	/* ->* */
	  fscanf(f, "%d\t%d\t%d\t%d\n", &time, &townnum, &fromline, &lastline);
	  ev->time = time;
	  ev->town = townnum;
	  ev->prev = lastline;
	  if (lastline >= numevents)
	    die("Invalid L cross-reference: %d > %d", lastline, numevents);

	  if (fromline < 0)
	    {
	      /* HACK */
	      puts("Ignoring F<0");
	      fromline = 0;
	    }
	  if (fromline < 0 || fromline >= numevents)
	    die("Invalid F cross-reference: %d > %d", fromline, numevents);
	  ev->dual = fromline;
	  ev->conn = -1;
	  events[ev->dual].dual = i;
	  break;
	default:
	  die("Syntax error");
	}
      ev++;
    }

  if (fgetc(f) != EOF) {
    fprintf(stderr, "Warning: Extra data ignored :)\n");
    fgets(buf, sizeof(buf), f);
    fprintf(stderr, "near this text: %s\n", buf);
  }
  if (strchr(opts, 'w')) {
    int h = open("travel.swp", O_RDWR | O_CREAT, 0640);
    if (write(h, events, events_size) != events_size)
      die("Could not write .swp file: %m");
    close(h);
  }
}

#define Convert(x) (iso88592_to_ascii(x))

static int
patmatch(char *p, char *s)
{
  while (*p)
    {
      if (*p == '?' && *s)
	p++, s++;
      else if (*p == '*')
	{
	  int z = p[1];

	  if (!z)
	    return 1;
	  if (z == '\\' && p[2])
	    z = p[2];
	  z = Convert(z);
	  for(;;)
	    {
	      while (*s && Convert(*s) != z)
		s++;
	      if (!*s)
		return 0;
	      if (patmatch(p+1, s))
		return 1;
	      s++;
	    }
	}
      else
	{
	  if (*p == '\\' && p[1])
	    p++;
	  if (Convert(*p++) != Convert(*s++))
	    return 0;
	}
    }
  return !*s;
}

static void
audit(void)
{
#ifdef DEBUG
  int i;
  for(i=0; i<numevents; i++)
    {
      struct event *ev = &events[i];
      /*
	printf("%d: t=%d T=%d c=%d p=%d d=%d\n", i, ev->time, ev->town, ev->conn, ev->prev, ev->dual);
	if (ev->conn >= 0 && ev->dual < 0)
	printf("Missing dual for event %d\n", i);
      */
      if (ev->prev >= 0 && events[ev->prev].town != ev->town)
	die("Inconsistent prev at %d", i);
    }
#endif
}

#if 1		/* Pokus o rozumn� spojen�, dojezd co nejd��ve, odjezd co nejpozdeji */
#define P_START(t1,t2) ((24*60)-t2)
#define P_STOP(t1,t2) (t1*5)
#define P_TRAIN_WAIT(t1,t2) (0)
#define P_CHANGE_WAIT(t1,t2) ((t2-t1 < 4) ? 5000 : 20)
#define P_TRAIN_GO(t1,t2) (0)
#define PENALTIES_NONZERO_AND_DECREASING 
#endif

#if 0		/* Optimalizace �asu dojezdu */
#define P_START(t1,t2) (0)
#define P_STOP(t1,t2) (t2-t1)
#define P_TRAIN_WAIT(t1,t2) (t2-t1)
#define P_CHANGE_WAIT(t1,t2) (t2-t1+5)
#define P_TRAIN_GO(t1,t2) (t2-t1)
#endif

#if 0		/* �as dojezdu + co nejm�n� p�estup� */
#define P_START(t1,t2) (0)
#define P_STOP(t1,t2) (t1)
#define P_TRAIN_WAIT(t1,t2) (0)
#define P_CHANGE_WAIT(t1,t2) (10000)
#define P_TRAIN_GO(t1,t2) (0)
#endif

#if 0		/* �as dojezdu + v�razn� znev�hodn�n� �ek�n� p�i p�estupu */
#define P_START(t1,t2) (0)
#define P_STOP(t1,t2) (t1)
#define P_TRAIN_WAIT(t1,t2) (0)
#define P_CHANGE_WAIT(t1,t2) (100*(t2-t1))
#define P_TRAIN_GO(t1,t2) (0)
#endif

#if 0		/* Maxim�ln� po�et p�estup� */
#define P_START(t1,t2) (1000000)
#define P_STOP(t1,t2) (0)
#define P_TRAIN_WAIT(t1,t2) (0)
#define P_CHANGE_WAIT(t1,t2) (-1)
#define P_TRAIN_GO(t1,t2) (0)
#endif

#if 0		/* Jedeme co nejd�le */
#define P_START(t1,t2) (t2-t1)
#define P_STOP(t1,t2) (t2-t1)
#define P_TRAIN_WAIT(t1,t2) (0)
#define P_CHANGE_WAIT(t1,t2) (t2-t1)
#define P_TRAIN_GO(t1,t2) (0)
#endif

#if 1		/* Pokus o rozumn� spojen�, dojezd co nejd��ve, odjezd co nejpozdeji */
#define P_START(t1,t2) ((24*60)-t2)
#define P_STOP(t1,t2) (t1*5)
#define P_TRAIN_WAIT(t1,t2) (0)
#define P_CHANGE_WAIT(t1,t2) ((t2-t1 < 4) ? 5000 : 20)
#define P_TRAIN_GO(t1,t2) (0)
#define PENALTIES_NONZERO_AND_DECREASING 
#endif

static void
start_penalties(char *spatt, char *dpatt)
{
  int i;
  int scnt = 0;
  int dcnt = 0;

  fprintf(stderr, "Initializing...\n");
  start_penalty = xmalloc(sizeof(int) * numtowns);
  stop_penalty = xmalloc(sizeof(int) * numtowns);
  tracks = xmalloc(sizeof(struct track) * numevents);
  for (i=0; i<numevents; i++)
    {
      tracks[i].badness = INFTY;
      tracks[i].backlink = -1;
    }
  for (i=0; i<numtowns; i++)
    {
      if (patmatch(spatt, towns[i].name))
	{
	  start_penalty[i] = 0;
	  scnt++;
	}
      else
	start_penalty[i] = INFTY;
      if (patmatch(dpatt, towns[i].name))
	{
	  stop_penalty[i] = 0;
	  dcnt++;
	}
      else
	stop_penalty[i] = INFTY;
    }
  fprintf(stderr, "Found %d starts and %d targets\n", scnt, dcnt);
  if (!scnt || !dcnt)
    die("Where to go?");
}

#define DBG(x, y...)
/* #define TRACE(p, x, y...) if (p < INFTY) printf(x, ##y) */
#define TRACE(p, x, y...)

static int
evaluate(void)
{
  int event, e, bestev;
  unsigned int best, p;
  unsigned int global_best = INFTY;
  int global_bestev = -1;

  for (event=0; event<numevents; event++)
    {
      struct event *ev = &events[event];
      int *ppe = NULL;
      struct event *dual;
      if (!(event % 1024))
	{ fprintf(stderr, "Processing event %7d (%2d:%02d, %3d%%)\r", event, ev->time/60, ev->time%60, event*100/numevents); fflush(stderr); }
      if (ev->time < start_time)
	continue;
      if (ev->time > stop_time)
	break;
      if (ev->conn < 0)
	{
	  TRACE(tracks[event].badness, ">>> <%d> at %d [%s] p=%u\n", event, ev->time, towns[ev->town].name, tracks[event].badness);
	  p = stop_penalty[ev->town] + tracks[event].badness;
	  if (p < INFTY)
	    p += P_STOP(ev->time, stop_time);
	  TRACE(p, "\ttotal p=%u%s\n", p, (p < global_best) ? " **" : "");
	  if (p < INFTY) fprintf(stderr, "Can be in target at %02d:%02d with badness %d.\n", ev->time/60, ev->time%60, p);
	  if ((p < INFTY) && ((stop_time-start_time) > (ev->time-start_time)*2)) {
		  stop_time = (ev->time-start_time)*2 + start_time;
		  fprintf(stderr, "Setting end time to %02d:%02d\n", stop_time/60, stop_time%60);
	  }
	  if (p < global_best)
	    {
	      global_best = p;
	      global_bestev = event;
	    }
	  continue;
	}
      if (ev->dual < 0)			/* DEBUG */
	continue;
      dual = &events[ev->dual];
      DBG("### <%d> at %d from [%s] to <%d> [%s] at %d via %s\n",
	     event, ev->time, towns[ev->town].name, ev->dual, towns[events[ev->dual].town].name, dual->time, conns[ev->conn].name);
      best = start_penalty[ev->town] + P_START(start_time, ev->time);
      DBG("\tstart -> %d\n", best);
      TRACE(best, "\t<%d> [%s] Start %d %d\n", event, towns[ev->town].name, best, event);
      bestev = -1;
      walks++;
      e = ev->prev;
      ppe = &ev->prev;
      {
	struct event * seen_sameconn = NULL;

	while (e >= 0)
	  {
	    struct event *pe = &events[e];
	    steps++;
	    if (pe->time < start_time)
	      break;
	    p = tracks[e].badness;
#if 1
	    if (p >= global_best) {
	      skipped++;
	      *ppe = pe->prev;
	      goto skip;
	    }
#ifdef PENALTIES_NONZERO_AND_DECREASING 
	    if (p >= best) {
	      skipped++;
	      *ppe = pe->prev;	/* Only works if all penalties are >= 0 */
	      goto skip;
	    }
#endif
#endif
#ifdef DEBUG
	    if (pe->conn >= 0) die("0 != 1"); /* DEBUG */
	    if (pe->town != ev->town) die("1 != 2 (ev=%d,pe=%d)", event, e);
#endif
#if 0
	    if ((events[pe->dual].conn == ev->conn) && seen_sameconn)
	       printf("Same train comes twice? [at %s, %s, %d, %d (now %d)]\n", towns[ev->town].name, conns[ev->conn].name, pe->time, seen_sameconn->time, ev->time); /* This can happen if train goes A -> B -> C -> B -> D */
#endif
	    if (events[pe->dual].conn == ev->conn)
	      p += P_TRAIN_WAIT(pe->time, ev->time), seen_sameconn = pe;
	    else
	      p += P_CHANGE_WAIT(pe->time, ev->time);
	    p += P_TRAIN_GO(ev->time, dual->time);		/* Constant in this cycle */

	    if (exists(conns[ev->conn].cond, &date) == 'N')
	      p = INFTY;

	    DBG("\t<%d> at %d -> %d\n", e, pe->time, p);
	    TRACE(p, "\t<%d> [%s] using <%d> at %d -> %d <%d> [%s]\n", event, towns[ev->town].name, e, pe->time, p, ev->dual, towns[dual->town].name);
	    if (p < best)
	      {
		best = p;
		bestev = e;
	      }
	  skip:
	    e = pe->prev;
	    ppe = &pe->prev;
	  }
      }
      TRACE(best, "<%d> [%s] -> %d <%d> [%s]\n", event, towns[ev->town].name, best, ev->dual, towns[events[ev->dual].town].name);
      if (best < tracks[ev->dual].badness)
	{
	  tracks[ev->dual].badness = best;
	  tracks[ev->dual].backlink = bestev;
	}
    }
  printf("@@@ best event %d with penalty %d\n", global_bestev, global_best);
  return global_bestev;
}

static void
follow(int event)
{
  while (event >= 0)
    {
      struct event *ev = &events[event];
      struct track *tr = &tracks[event];
      struct event *dual = &events[ev->dual];
      if (ev->conn >= 0 || dual->conn < 0) die("Bug #1");	/* DEBUG */
      if (strchr(opts, 'u'))
	printf("%02d:%02d %s\t%-23s\t%02d:%02d %s\t%-23s\t%3d %2d\t%s\n",
	       dual->time/60, dual->time%60, "", towns[dual->town].name,
	       ev->time/60, ev->time%60, "", towns[ev->town].name,
	       tr->badness, 0 /* kilometers */,
	       conns[dual->conn].name);
      else
	printf("%02d:%02d %-20s -> %02d:%02d %-20s%8d [%s]\n",
	       dual->time/60, dual->time%60, towns[dual->town].name,
	       ev->time/60, ev->time%60, towns[ev->town].name,
	       tr->badness,
	       conns[dual->conn].name);
      event = tr->backlink;
    }
}

int
main(int argc, char **argv)
{
  int x, y;

  if (argc < 5)
    die("Usage: cat file.t | travel [-r] [-w] [-m] <from> <date> <time> <to>");
  if (*argv[1] == '-')
    opts = argv[1], argv++;

  if (sscanf(argv[2], "%d.%d.%d", &date.tm_mday, &date.tm_mon, &date.tm_year)!=3)
    die("Invalid date: %s", argv[2]);
  date.tm_sec=0; 
  date.tm_min=0; 
  date.tm_hour=0;
  date.tm_mon--; 
  date.tm_year-=1900;
  mktime(&date);
  date.tm_mon++;
  if (date.tm_wday==0)
    date.tm_wday=7;

  if (sscanf(argv[3], "%d:%d", &x, &y) != 2 ||
      x < 0 || x > 23 || y < 0 || y > 59)
    die("Invalid time: %s", argv[3]);

  read_timetable(inputfile);
  fprintf(stderr, "Auditing...\n");
  audit();

  start_time = 60*x + y;
  stop_time = 24*60;

  start_penalties(argv[1], argv[4]);
  run_time = time(NULL);
  x = evaluate();
  if (x < 0)
    die("No connection found");
  else
    follow(x);
  printf("Total %Ld Ksteps (%Ld skipped) in %Ld walks, thats %.2f average walk. %ld seconds elapsed, at %.1f Ksteps/second.\n", steps/1000, skipped/1000, walks, (float)steps/(float)walks, time(NULL)-run_time, (float)(steps/1000) / (time(NULL)-run_time));

  return 0;
}
