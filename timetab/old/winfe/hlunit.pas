unit hlunit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, Grids, Calendar, Mask, ExtCtrls, Buttons, ComCtrls;

{pomocn� pole s po�ty dn� v m�s�ci}
const pole: array[1..12] of integer =(31,28,31,30,31,30,31,31,30,31,30,31);

 type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Soubor1: TMenuItem;
    Exit1: TMenuItem;
    Oprogramu1: TMenuItem;
    Vysvtlivky1: TMenuItem;
    Memo1: TMemo;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Button1: TButton;
    SpeedButton1: TSpeedButton;
    Calendar1: TCalendar;
    MaskEdit2: TEdit;
    MonthCalendar1: TMonthCalendar;
    licence1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Vysvtlivky1Click(Sender: TObject);
    procedure Licence1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure MonthCalendar1Exit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MonthCalendar1Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure MonthCalendar1KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1KeyPress(Sender: TObject; var Key: Char);
    procedure Button1KeyPress(Sender: TObject; var Key: Char);
    procedure Memo1KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure ComboBox1Enter(Sender: TObject);
    procedure ComboBox2KeyPress(Sender: TObject; var Key: Char);




  private
    { Private declarations }
  public
    { Public declarations }
  {P... - znamen� pomocn�}

  jk,h,Odkud,Kam,Den,Mesic,Hod,Min,Rok,Pstr,Prok,Pmesic,Pden,Phod,Pmin,Pcas,Pvystup: string;
  Pcontrol,lze:char;
  t,k:textfile;
  date1:Tdate;
  pl:boolean;
  end;

var
  Form1: TForm1;

implementation
uses unit3,unit2;

{$R *.DFM}
{ spust� Cmdline(Exe) jako nov� process a �ek� na ukon�en� }
procedure ExecProcess(const CmdLine: String);
var
  SI: TStartupInfo;
  PI: TProcessInformation;
begin
  FillChar(SI, SizeOf(SI), 0);
  SI.cb := SizeOf(SI);
  SI.dwFlags := STARTF_USESHOWWINDOW;
  SI.wShowWindow := sw_SHOWMINNOACTIVE;
  CreateProcess(nil,PChar(CmdLine),nil,nil,True,NORMAL_PRIORITY_CLASS,nil,nil,SI,PI);
  WaitForSingleObjectEx(PI.hProcess,INFINITE,False);
end;
procedure TForm1.FormCreate(Sender: TObject);
var a:string;i: byte;
{Po��te�n� nastaven� hodnot}
begin
 {Na�ten� nab�dek v ComboBoxech}
 shortdateformat:='d.m.yyyy';
 shorttimeformat:='h:nn';
 ComboBox1.items.loadfromfile('zdroj.txt');
 ComboBox2.items.loadfromfile('zdroj.txt');
 Maskedit2.text:= IntToStr(Calendar1.day)+'.'+IntToStr(Calendar1.month)+'.'+IntToStr(Calendar1.year);
 datetimetostring(a,'h":"nn',now);
 Maskedit1.text:=a;
 Lze:='n';
 {Pokud je rok p�estupn�, m� �nor 29 dn�}
 if Calendar1.year mod 4 = 0 then
    pole[2]:=29;
 Pcontrol:='n';
end;



procedure TForm1.MaskEdit1Exit(Sender: TObject);
{Kontrola �asu - zda existuje}
begin
  {Na�tu do pomocn�ch prom�nn�ch zadan� �as}
 Try
  Phod:= MaskEdit1.text[1]+ MaskEdit1.text[2];
  Pmin:= MaskEdit1.text[4]+ MaskEdit1.text[5];
  if (StrToInt(Phod)>23) or (StrToInt(Pmin)>59)  then
   begin
    MessageDlg('Chybn� zadan� �as!!!',mtError,[mbOK],0);
    MaskEdit1.clear;
   end;
 except
  MessageDlg('Chybn� zadan� �as!!!',mtError,[mbOK],0);
  MaskEdit1.clear;
 end;
end;

procedure TForm1.Exit1Click(Sender: TObject);
{Vypne aplikaci p�i kliknut� na "konec" v MainMenu }
begin
 Form1.close;
end;

procedure TForm1.Button1Click(Sender: TObject);
{Nejd��v zkontroluji datum}
var i,j,prvni,druhy,OK: integer;
begin
 shortdateformat:='d.m.yyyy';
 shorttimeformat:='h:nn';
 longdateformat:='d.m.yyyy';
 longtimeformat:='d.m.yyyy';
 Pcas:= 'a';
 Prok:='n';
 Pmesic:='n';
 Pden:='n';
 Pcontrol:='n';
 j:=0;
 prvni:=0;druhy:=0;
 while j<10 do
  begin
   j:=j+1;
   if (prvni =0) and((Maskedit2.text[j]='.')or(Maskedit2.text[j]='/')) then
   Prvni:=j            else
   if (prvni >0) and ((MaskEdit2.text[j]='.')or(Maskedit2.text[j]='/')) then
    begin
     Druhy:=j;
     j:=10;
    end;
  end;

 If (prvni in [2..3])and(druhy-prvni in [2..3])then
     ok:=1                                     else ok:=0;
     if ok = 1 then
        begin
         rok:='';mesic:='';den:='';
         For i:= 1 to prvni-1 do
           den:= den+MaskEdit2.text[i];
         For i:=prvni+1 to druhy-1 do
            mesic:= mesic+MaskEdit2.text[i];
         For i:= druhy+1 to druhy+4 do
            rok:= rok+MaskEdit2.text[i];
        end
               else
             mesic:='13';

 for i:= 0 to 2 do
  if rok = IntToStr(Calendar1.year-i+1)then
     Prok:='a';
 for i:= 1 to 12 do
  if mesic = IntToStr(i)then
     Pmesic:='a';
 {Pokud je rok p�estupn�, d�m do pole[2] = �nor 29, jinak 28 }
 if Prok = 'a' then
    if StrToInt(rok) mod 4 = 0 then pole[2]:= 29 else pole[2]:=28;
 {Pokud je datum spr�vn�, dosad� se do Pden 'a'}
 If (Pmesic = 'a') and (Prok = 'a') then
  For i:=1 to pole[StrToInt(mesic)] do
   If den = IntToStr(i) then
      Pden:= 'a';
 If Pden = 'n' then
    MessageDlg('Spojen� v tomto datu nemohu nal�zt, zm��te datum!',mtError,[mbOK],0);
 {Kontrola �asu}
 If MaskEdit1.text ='  :  'then Pcas:= 'n';
 If Pcas ='n' then
     MessageDlg('Zadejte �as!',mtError,[mbOK],0);
 If (Pden = 'a') and (Pcas = 'a')then
     begin
      {Testuji, zda jsou zadan� m�sta platn�, tj. zda neza��naj� mezerou}
        if (ComboBox1.text <> '') and (ComboBox2.text<>'') then
           if Pos (' ',ComboBox1.text) <> 1 then
               if Pos (' ',ComboBox2.text) <> 1 then
               Pcontrol:='a';
        if Pcontrol ='n' then
           MessageDlg('Zm�nte zadan� m�sto!',mtError,[mbOK],0);

     end;
 {Pokud jsou v�echna data v po��dku, pak spust�m timetab.exe}
 If Pcontrol = 'a' then
     begin
      screen.cursor:= crhourglass;
      assignfile(t,'x.bat');
      assignfile(k,'dds.txt');
      rewrite(t);
      rewrite(k);
      closefile(k);
      Lze:= 'a';
      Odkud:= ComboBox1.Text;
      Kam:= ComboBox2.Text;
      Phod:= MaskEdit1.Text[1]+MaskEdit1.Text[2];
      Pmin:= MaskEdit1.text[4]+MaskEdit1.Text[5];
      h:=Phod+':'+Pmin;
      jk:=den+'.'+mesic+'.'+rok;
      If MaskEdit1.Text[1] in ['1'..'2'] then
         Pvystup:='"'+ComboBox1.text+'"'+' '+MaskEdit2.text+' '+MaskEdit1.text+' '+'"'+ComboBox2.text+'"'
                                         else
                    Pvystup:='"'+ComboBox1.text+'"'+' '+MaskEdit2.text+MaskEdit1.text+' '+'"'+ComboBox2.text+'"';
      write(t,'timetab.exe '+Pvystup+'> dds.txt');
      closefile(t);
      execprocess('x.bat');
      screen.cursor:=crdefault;
      Memo1.lines.loadfromfile('dds.txt');
     end;
end;

procedure TForm1.Vysvtlivky1Click(Sender: TObject);
begin
 Form2.windowstate:= wsNormal;
 Form2.visible:=true;

end;

procedure TForm1.Licence1Click(Sender: TObject);
begin
 Form3.windowstate:= wsNormal;
 Form3.visible:=true;
end;





{Reaguje na zm�ny velikosti formul��e}
procedure TForm1.FormResize(Sender: TObject);
begin
 If (form1.Height > 100) and (form1.width >400) then
    begin
     Memo1.height:= Form1.height - 138;
     Memo1.width:= Form1.Width - 9;
     Bitbtn1.top:= Form1.height - 67;
     Bitbtn2.top:= Form1.Height - 67;
     Panel1.width:=Form1.width - 9;
    end;
end;

{Toto zaru��, �e program nezmen��te pod p�ijatelnou �rove�, pokud ho neminimalizujete}
procedure TForm1.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
If (NewHeight > 280) and (NewWidth > 507) then
   Resize:=true                           else Resize:= false;
end;
{Vyhled�v�n� p�edchoz�ho spojen�}
procedure TForm1.BitBtn1Click(Sender: TObject);
var a: boolean;
    datum:tdatetime;c:string;
begin
 a:=true;
 If Lze <> 'a' then
    MessageDlg('Nemohu hledat p�edchoz� spojen�, kdy� jste je�t� nic nehledali.',mtError,[mbOK],0)
                else
      Begin
       longdateformat:='d.m.yyyy';
       longtimeformat:='d.m.yyyy';
       shortdateformat:='d.m.yyyy';
       shorttimeformat:='h:nn';
       datum:=StrToDate(jk)+StrToTime(h);
       datum:=datum-0.007;{ode��t�m asi 11 minut}
       if datum >= strtodate('1.1.'+inttostr(calendar1.year-1)) then
          begin
           a:=true;
           datetimetostring(jk,'d"."m"."yyyy',datum);
           datetimetostring(h,'h":"nn',datum);
          end
                                                else
                     a:=false;
       if a = true then
          begin
           datetimetostring(c,'d"."m"."yyyy h":"nn',datum);
           screen.cursor:= crhourglass;
           assignfile(t,'x.bat');
           assignfile(k,'dds.txt');
           rewrite(t);
           rewrite(k);
           closefile(k);
           Pvystup:='"'+Odkud+'"'+' '+c+' '+'"'+Kam+'"';
           write(t,'timetab.exe '+Pvystup+'>dds.txt');
           closefile(t);
           execprocess('x.bat');
           Memo1.lines.loadfromfile('dds.txt');
           screen.cursor:=crdefault;
          end;
       If a = false then
          MessageDlg('Nemohu hledat p�edchoz� spojen�,bylo by mimo rozsah tohoto vyhledava�e .',mtError,[mbOK],0)
      end;
end;
{Najde n�sleduj�c� spojen�}
procedure TForm1.BitBtn2Click(Sender: TObject);
var a: boolean;
    datum:tdatetime;c:string;
begin
 a:=true;
 If Lze <> 'a' then
    MessageDlg('Nemohu hledat n�sleduj�c� spojen�, kdy� jste je�t� nic nehledali.',mtError,[mbOK],0)
               else
      Begin
       shortdateformat:='d.m.yyyy';
       shorttimeformat:='h:nn';
       longdateformat:='d.m.yyyy';
       longtimeformat:='h:nn';
       datum:=StrToDate(jk)+StrToTime(h);
       datum:=datum+0.0008;{p�i��t�m asi 1 minutu}
       if datum <= strtodate('1.1.'+inttostr(calendar1.year+2)) then
          begin
           a:=true;
           datetimetostring(jk,'d"."m"."yyyy',datum);
           datetimetostring(h,'h":"nn',datum);
          end
                                                else
                     a:=false;
        if a = true then
           begin
            datetimetostring(c,'d"."m"."yyyy h":"nn',datum);
            screen.cursor:= crhourglass;
            assignfile(t,'x.bat');
            assignfile(k,'dds.txt');
            rewrite(t);
            rewrite(k);
            closefile(k);
            Pvystup:='"'+Odkud+'"'+' '+c+' '+'"'+Kam+'"';
            write(t,'timetab.exe '+Pvystup+'>dds.txt');  { - pot�eba vlo�it na spr�vn� m�sto}
            closefile(t);
            execprocess('x.bat');
            Memo1.lines.loadfromfile('dds.txt');
            screen.cursor:=crdefault;
           end;
        If a = false then
           MessageDlg('Nemohu hledat n�sleduj�c� spojen�,bylo by mimo rozsah tohoto vyhledava�e .',mtError,[mbOK],0)
      end;
end;
procedure TForm1.Button1KeyPress(Sender: TObject; var Key: Char);
begin
case key of


 #27 : form1.Close;

end;

end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
 shortdateformat:='d.m.yyyy';
 shorttimeformat:='h:nn';
 longdateformat:='d.m.yyyy';
 longtimeformat:='h:nn';
 if MonthCalendar1.visible = false then
  begin
   Try
    MonthCalendar1.date:=StrToDate(MaskEdit2.text);
   except;
   end;
    date1:=MonthCalendar1.date;
    MonthCalendar1.visible:=true;
  end                              else
      MonthCalendar1.visible:=false;
end;

procedure TForm1.MonthCalendar1Exit(Sender: TObject);
begin
MonthCalendar1.visible:=false;
end;



procedure TForm1.MaskEdit2Exit(Sender: TObject);
begin
 Try MonthCalendar1.date:=StrToDate(MaskEdit2.text);
 except;
 end;

end;

procedure TForm1.MonthCalendar1Click(Sender: TObject);

begin
 if date1<> MonthCalendar1.date then
  begin
   Maskedit2.text:= DateToStr(MonthCalendar1.date);
   MonthCalendar1.visible:=false;
  end;
 end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin

  case key of

    #27   : form1.close;

   end;
  end;

procedure TForm1.MonthCalendar1KeyPress(Sender: TObject; var Key: Char);
begin
 if key = #27 then
   MonthCalendar1.visible:= false;
end;

procedure TForm1.BitBtn2KeyPress(Sender: TObject; var Key: Char);
begin
 case key of

 #27 : form1.Close;

 end;
end;

procedure TForm1.BitBtn1KeyPress(Sender: TObject; var Key: Char);
begin
case key of

 #27 : form1.Close;

end;
end;
procedure TForm1.Memo1KeyPress(Sender: TObject; var Key: Char);
begin
case key of

 #27 : form1.Close;

 end;
end;



procedure TForm1.MaskEdit1KeyPress(Sender: TObject; var Key: Char);
begin
 try
  if (key = #13)or(key = #9) then
      Maskedit1exit(sender);
 except
  Maskedit1exit(sender);
 end;
end;

procedure TForm1.MaskEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = vk_delete then
    maskedit1.clear;
end;

procedure TForm1.ComboBox1KeyPress(Sender: TObject; var Key: Char);
begin
 if (pl=false) and (key <> #13) and (key<>#27) then
   begin
    pl:=true;
    combobox1.droppeddown:=true;
   end;
  if (key = #13) or (key = #27) then
    pl:=false;
  if key = #13 then
    form1.activecontrol:=combobox2;
end;

procedure TForm1.ComboBox1Enter(Sender: TObject);
begin
 pl:=false;
end;

procedure TForm1.ComboBox2KeyPress(Sender: TObject; var Key: Char);
begin
  if (pl=false) and (key<>#13) and (key<>#27) then
   begin
    pl:=true;
    combobox2.droppeddown:=true;
   end;
  if (key = #13) or (key = #27) then
    pl:=false;
  if key = #13 then
    form1.activecontrol:=button1;
end;

end.
