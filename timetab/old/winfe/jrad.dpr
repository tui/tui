{This program is distributed under GPL}
{Copyrights to Jaroslav Dra�an}
program Jrad;

uses
  Forms,
  hlunit in 'hlunit.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  Unit3 in 'Unit3.pas' {Form3},
  SysUtils;
{$R *.RES}

begin
  Application.Initialize;
  dateseparator:='.';
  timeseparator:=':';
  shortdateformat:='d.m.yyyy';
  shorttimeformat:='h:nn';
  longdateformat:='d.m.yyyy';
  longtimeformat:='d.m.yyyy';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
