unit ttunit;
{
Libary of data types for reading timetabs from file *.tt
version 0.1.4

Copyright (c)2001  Dolejsek Zbynek (klon@papouch.cz)

written under GNU Library Public Licence version 2 or later

Code completed and some changes made (c)2001 Petr Cermak (cermak01@seznam.cz)
}

{It is supposed to compile this unit with strings as ansistrings, because there can be
several remarks longer than 255 characters.
Switch for freepascal follows..}
{$H+}
interface
uses typy;

procedure read_tt(var f:text);
procedure write_tt(var f:text);
{============================== Implementation ==============================}
implementation

function rline(var f:text):string;
  var c:char;
      s:string;
  begin
    s:='';
    read(f,c);
    while (c<>#10) and (c<>#26) do
    begin
      s:=s+c;
      read(f,c);
    end;
    rline:=s;
    end;{rline}

Procedure read_lines(var f:text);
var s,t             : string;
   time1,wait1,leng : my_int;
   statf,statt      : Pstation;
   notet            : Pnote;
begin
   statf:=nil;
   s:=rline(f);
   repeat
      notet:=nil;
      t:='';
      time1:=0; wait1:=0;
      if ((pos(':',s)=2)or(pos(':',s)=3)) and (s[1]<>#9) then
         time1:=1500 + 60*my_val(s)+my_val(copy(s,pos(':',s)+1,length(s)));
      if pos('+',s)=1 then
         time1:=my_val(s);
      if statf<>nil then
      begin
         s:=copy(s,pos(#9,s)+1,length(s));
         if (pos(':',s)=2)or(pos(':',s)=3) then
            wait1:=1500 + 60*my_val(s)+my_val(copy(s,pos(':',s)+1,length(s)));
         if pos('+',s)=1 then
            wait1:=my_val(s);
      end;
      s:=copy(s,pos(#9,s)+1,length(s));
      t:=copy(s,1,pos(#9,s)-1);
      
      if t<>'' then
         notet:=add_note(t)
      else
         notet:=nil;
      s:=copy(s,pos(#9,s)+1,length(s));
      if s[1]='-' then
         leng:=0  {There can be a bug in tt database}
      else
         leng:=my_val(s);
      s:=copy(s,pos(#9,s)+1,length(s));
      statt:=add_station(s,0,hash(s));
      if statf<>nil then
                     add_connection_rec(
                         add_departure(statf,statt,wait1,time1,leng,notet))
      else
         add_connection_head(time1,statt,notet);
      statf:=statt;
      s:=rline(f);
      if pos('#',s)=1 then
        begin
          add_connection_name(copy(s,3,length(s)));
          break;
        end
        else
           if s<>'' then
              if s[1] in ['R', 'B', 'G'] then
              begin
                 case s[1] of
                   'G' : add_connection_corp(add_corp(copy(s,2,length(s))));
                   'R' : add_connection_rem(add_Rrem(copy(s,2,length(s))));
                   'B' : add_connection_brem(add_Brem(copy(s,2,length(s))));
                 end; { case }
                 break;
              end;
    until s='';
  end;{read_lines}

Procedure read_connection(var f:text);
  var c:char;
  begin
    read(f,c);
    add_connection_name(rline(f));
  end;{read_connection}

Procedure read_campany(var f:text);
  begin
    add_connection_corp(
       add_corp(
          rline(f)));
    end;{read_campany}

Procedure read_remark(var f:text);
  begin
    add_connection_rem(
       add_Rrem(
          rline(f)));
    end;{read_remark}

Procedure read_bremark(var f:text);
  begin
    add_connection_brem(
       add_Brem(
          rline(f)));
    end;{read_bremark}

procedure read_tt(var f:text);
var c:char;
begin
  while not eof(f) do
  begin
   if (global.nconnections mod 1000)=0 then
    begin
       write(stderr, 'read:',global.ndepartures:8,' dep',global.nstations:6,' stat');
       write(stderr, global.nconnections:6,' conn',(mymem shr 10):8,' KB'#13);
       end;
    read(f,c);
    case c of
      '#':read_connection(f);
      'G':read_campany(f);
      'R':read_remark(f);
      'B':read_bremark(f);
      #9:read_lines(f);
      end;
    end;
    write(stderr, #13'done:',global.ndepartures:8,' dep',global.nstations:6,' stat');
    writeln(stderr, global.nconnections:6,' conn',(mymem shr 10):8,' KB');
  end;

function make_num(n : my_int):string;
var
   s:string;
begin
  if n<10 then
     make_num:='0'+char(n+48)
  else
  begin
     str(n, s);
     make_num:=s;
  end;
end;

procedure write_tt(var f:text);
var conn:Pconnection;
    list:Plist;
    num:longint;
    tmp:my_int;
    remlist: Premlist;
begin
  conn:=global.connections;
  num:=0;
  while conn <> nil do
  begin
    write(f,'# ',conn^.name,#10);
    tmp:=conn^.time-1500;
    if conn^.note<>nil then
       write(f,#9,tmp div 60,':',make_num(tmp mod 60),#9,conn^.note^.name,#9,0,#9,conn^.first^.name,#10)
    else
       write(f,#9,tmp div 60,':',make_num(tmp mod 60),#9#9,0,#9,conn^.first^.name,#10);
    list:=conn^.list;
    while list<>nil do
    begin
      if list^.departure^.time<>0 then
         if list^.departure^.time>=1500 then
         begin
              tmp:=list^.departure^.time-1500;
              write(f,tmp div 60, ':', make_num(tmp mod 60));
         end
         else
             write(f,'+',list^.departure^.time);
      write(f,#9);
      if list^.departure^.wait<>0 then
         if list^.departure^.wait>=1500 then
         begin
              tmp:=list^.departure^.wait-1500;
              write(f,tmp div 60,':', make_num(tmp mod 60));
         end
         else
             write(f,'+',list^.departure^.wait);
      if list^.departure^.note<>nil then
         write(f,#9,list^.departure^.note^.name,#9,list^.departure^.length,#9,list^.departure^.station^.name,#10)
      else
         write(f,#9#9,list^.departure^.length,#9,list^.departure^.station^.name,#10);
      list:=list^.next;
      end;
    remlist:=conn^.Brem;
    while remlist<>nil do
    begin
      write(f,'B',remlist^.remark^.name,#10);
      remlist:=remlist^.next;
    end;
    remlist:=conn^.Rrem;
    while remlist<>nil do
    begin
      write(f,'R',remlist^.remark^.name,#10);
      remlist:=remlist^.next;
    end;
    remlist:=conn^.corp;
    while remlist<>nil do
    begin
      write(f,'G',remlist^.remark^.name,#10);
      remlist:=remlist^.next;
    end;
    conn:=conn^.next;
    inc(num);
    write(stderr, 'write conns:',num,'/',global.nconnections,#13);
    end;
  writeln(stderr);
  end;
{================================= APENDIX =================================}
begin
end.
{================================ END TTUNIT ================================}
