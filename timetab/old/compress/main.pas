Program TTZIP;
{
TTZIP compresion timetabs (*.tt to *.ttz)
version 0.1.3

Copyright (c)2001  Dolejsek Zbynek (klon@papouch.cz)

writed under GNU General Public Licence version 2 or later

Code completed and some changes made (c)2001 Petr Cermak (cermak01@seznam.cz)
}
uses typy,ttunit,zipunit,mkindex;
var inputf,outputf:text;
    count:word;
    stav:string;

procedure zip;
begin
  writeln(stderr, 'zip');
  if pos('i',stav)>0 then read_tt(input)
                     else begin
                       {$I-}
                       reset(inputf);
                       {$I+}
                       if ioresult<>0 then exit;
                       read_tt(inputf);
                       close(inputf);
                       end;
  if pos('o',stav)>0 then write_zip(output)
                     else begin
                       {$I-}
                       rewrite(outputf);
                       {$I+}
                       if ioresult<>0 then exit;
                       write_zip(outputf);
                       close(outputf);
                       end;
  end;

procedure unzip;
begin
  writeln(stderr, 'unzip');
  if pos('i',stav)>0 then read_zip(input)
                     else begin
                       {$I-}
                       reset(inputf);
                       if ioresult<>0 then exit;
                       {$I+}
                       read_zip(inputf);
                       close(inputf);
                       end;
  if pos('o',stav)>0 then write_tt(output)
                     else begin
                       {$I-}
                       rewrite(outputf);
                       if ioresult<>0 then exit;
                       {$I+}
                       write_tt(outputf);
                       close(outputf);
                       end;
  end;

procedure same;
begin
  writeln(stderr, 'same');
  if pos('i',stav)>0 then read_tt(input)
                     else begin
                       {$I-}
                       reset(inputf);
                       if ioresult<>0 then exit;
                       {$I+}
                       read_tt(inputf);
                       close(inputf);
                       end;
  if pos('o',stav)>0 then write_tt(output)
                     else begin
                       {$I-}
                       rewrite(outputf);
                       if ioresult<>0 then exit;
                       {$I+}
                       write_tt(outputf);
                       close(outputf);
                       end;
  halt(0);
  end;

procedure index;
begin
  writeln('indexfile');
  if pos('i',stav)>0 then read_zip(input)
                     else begin
                       {$I-}
                       reset(inputf);
                       {$I+}
                       if ioresult<>0 then exit;
                       read_zip(inputf);
                       close(inputf);
                       end;
  if pos('o',stav)>0 then make_indexfile(output)
                     else begin
                       {$I-}
                       rewrite(outputf);
                       {$I+}
                       if ioresult<>0 then exit;
                       make_indexfile(outputf);
                       close(outputf);
                       end;
  halt(0);
  end;

{================================ Main Body ================================}
begin
  inputf := Input;	{ Warning: Input/Output does not seem the way I want it to }
  outputf := Output;
  for count:=1 to paramcount-1 do
    if pos('-',paramstr(count))=1 then stav:=paramstr(count)
                                  else assign(inputf,paramstr(count));
  writeln(stderr, paramcount,' ',stav,' ',paramstr(count),' ',paramstr(paramcount));
  if paramcount > 0 then
     if pos('-',paramstr(paramcount))<>1 then
	assign(outputf,paramstr(paramcount));
  writeln(stderr, 'Will do something');
  if pos('s',stav)>0 then same;
  if pos('n',stav)>0 then index;
  if pos('d',stav)>0 then unzip
                     else zip;
  end.
{================================ Main Body ================================}
