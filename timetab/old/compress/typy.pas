unit typy;
{
Libary of data types for work timetabs in memory
version 0.1.4

Copyright (c)2001  Dolejsek Zbynek (klon@papouch.cz)

written under GNU Library Public Licence version 2 or later

Code completed and some changes made (c)2001 Petr Cermak (cermak01@seznam.cz)
}

{It is supposed to compile this unit with strings as ansistrings, because there can be
several remarks longer than 255 characters.
Switch for freepascal follows..}
{$H+}
interface
const
     cisla:set of char=['0'..'9','+','-'];
     hash_table_length=4095;
type
     my_int=longint;
{types of pointers to structs}
     Phashtable=^ThashTable;
     Pstation=^Tstation;
     Pdeparture=^Tdeparture;
     Pconnection=^Tconnection;
     Plist=^Tlist;
     Prem=^Trem;
     Ptimetab=^Ttimetab;
     Pnote=^Tnote;
     Premlist=^Tremlist;
     Pconnlist=^Tconnlist;
{types of structs}
     Tdeparture=record     {list of departures from this station}
       next:Pdeparture;    {next departure in list}
       wait:my_int;    {wait for the connection}
       time:my_int;    {time to next station}
       length:my_int;  {length way in km}
       station:Pstation;{next station}
       note:Pnote;
       id:my_int;
       num:my_int;
       end;
     Tconnlist=record
       next:Pconnlist;
       index:my_int;
       end;
     Tstation=record    {list of stations}
       next:Pstation;   {next station in list}
       name:string[50]; {name}
       departures:Pdeparture; {list of departures}
       connlist:Pconnlist;
       id:my_int;
       end;
     Tnote=record
       next:Pnote;
       name:string[10];
       id:my_int;
       num:my_int;
       end;
     ThashTable=array[0..hash_table_length] of Pstation;
     Tremlist=record
       next:Premlist;
       remark:Prem;
       end;
     Tconnection=record
       next:Pconnection;
       name:string[40];
       time:my_int;
       note:Pnote;
       first:Pstation;
       list:Plist;
       corp,
       Rrem,
       Brem:Premlist;
       end;
     Tlist=record
       next:Plist;
       departure:Pdeparture;
       end;
     Trem=record
       next:Prem;
       name:string;
       id:my_int;
       num:my_int;
       end;
     Ttimetab=record
       version:string[20];
       stations:Phashtable;
       connections:Pconnection;
       Rrem,
       Brem,
       Corp:Prem;
       notes:Pnote;
       nstations,
       nconnections,
       nRrem,
       nBrem,
       nNotes,
       nCorp:my_int;
       ndepartures,
       nList:longint;
       end;
{=================================== VAR ===================================}
var global:Ttimetab;
    towns_pos, conn_pos, brem_pos, rrem_pos, corp_pos, notes_pos, dep_pos,
    conns_pos:my_int;
{=================================Functions=================================}
function my_val(s:string):my_int;
function add_station(name:string; id,h:my_int):Pstation;
function hash(name:string):my_int;
function add_Rrem(name:string):Prem;
function add_Brem(name:string):Prem;
function add_corp(name:string):Prem;
function add_note(name:string):Pnote;
function add_departure(statf,statt:Pstation; wait,time,length:my_int; notet:Pnote):Pdeparture;
function mymem:longint;
function add_rem(name:string; var rem:Prem; var num:my_int):Prem;
function find_rem(id:my_int; rem:Prem):Prem;
function find_note(id:my_int; notet:Pnote):Pnote;
{=================================Procedures=================================}
procedure add_connection_name(name:string);
procedure add_connection_head(time:my_int; town:Pstation; notet: Pnote);
procedure add_connection_rec(dep:Pdeparture);
procedure add_connection_rem(rem:Prem);
procedure add_connection_brem(rem:Prem);
procedure add_connection_corp(rem:Prem);
procedure sort_towns;
procedure sort_departures;

{===============================Implementation===============================}
implementation

function mymem:longint;
  begin
    with global do
         mymem:=ndepartures*sizeof(Tdeparture)
             +nstations*sizeof(Tstation)
             +nconnections*sizeof(Tconnection)
             +(nrrem+nbrem+ncorp)*sizeof(Trem)
             +nlist*sizeof(Tlist)
             +sizeof(Thashtable);
    end;

Function my_val(s:string):my_int;
  var tmp:my_int;
      i:integer;
  begin
    i:=1;
    while s[i] in cisla do inc(i);
    dec(i);
    s:=copy(s,1,i);
    val(s,tmp,i);
    if i=0 then my_val:=tmp;
    end;

function hash(name:string):my_int;
var i,tmp:my_int;
begin
  tmp:=0;
  for i:=1 to length(name) do
  begin
    tmp:=tmp+ord(name[i]) shl i+ord(name[i]) shr (8-i);
    if tmp>(MaxInt div 5) then tmp:=tmp div i;
  end;
  tmp:=abs(tmp);    {make sure, tmp isn't negative (overflow)}
  hash:=tmp mod hash_table_length;
  end;

procedure add(stat:Pstation;name:string; id:my_int);
  var tmp:Pstation;
  begin
    new(tmp);
    tmp^.name:=name;           {preda nazev}
    tmp^.next:=stat^.next; {a prida ji na zacatek seznamu}
    tmp^.departures:=nil;
    tmp^.id:=id;
    tmp^.connlist:=nil;
    stat^.next:=tmp;
    inc(global.nstations);
    end;

function add_station(name:string; id,h:my_int):Pstation;
{find or add station
 output pointer to station}
  var stat:Pstation;
  begin
    stat:=PStation(@global.stations^[h]);    {a projde seznam}
    if name='' then begin
                 while (stat^.next<>nil)and(stat^.next^.id<>id) do
                       stat:=stat^.next;
                 add_station:=stat^.next;
                 exit;
                 end
               else
                 while (stat^.next<>nil) and(stat^.next^.name>name) do
                      stat:=stat^.next;
    if (stat^.next<>nil)and(stat^.next^.name<>name)or(stat^.next=nil)
       then add(stat,name,id);
    add_station:=stat^.next;            {preda ukazatel na polozku}
    end;{add_station}

function find_rem(id:my_int; rem:Prem):Prem;
begin
  if id=0 then
  begin
       find_rem:=nil;
       exit;
  end;
  while (rem<>nil)and(rem^.id<>id) do rem:=rem^.next;
  find_rem:=rem;
  end;


function find_note(id:my_int; notet:Pnote):Pnote;
begin
  if id=0 then
  begin
       find_note:=nil;
       exit;
  end;
  while (notet<>nil)and(notet^.id<>id) do notet:=notet^.next;
  find_note:=notet;
  end;

function add_rem(name:string; var rem:Prem; var num:my_int):Prem;
var tmp:Prem;
begin 
  tmp:=rem;
  while (tmp<>nil) and (tmp^.name<>name) do
    tmp:=tmp^.next;
  if tmp=nil then begin
    new(tmp);
    tmp^.name:=name;
    tmp^.num:=1;
    tmp^.next:=rem;
    inc(num);
    tmp^.id:=num;
    rem:=tmp;
    add_rem:=rem;
    end
  else begin
    inc(tmp^.num);
    add_rem:=tmp;
    end;
  end;{add_Rrem}


function add_note(name:string):Pnote;
var tmp:Pnote;
begin
  tmp:=global.notes;
  while (tmp<>nil) and (tmp^.name<>name) do
    tmp:=tmp^.next;
  if tmp=nil then begin
    new(tmp);
    tmp^.name:=name;
    tmp^.num:=1;
    tmp^.next:=global.notes;
    inc(global.Nnotes);
    tmp^.id:=global.Nnotes;
    global.notes:=tmp;
    add_note:=global.notes;
    end
  else begin
    inc(tmp^.num);
    add_note:=tmp;
    end;
  end;{add_Rrem}

function add_Rrem(name:string):Prem;
begin
  add_Rrem:=add_rem(name,global.Rrem,global.nRrem);
  end;{add_Rrem}

function add_Brem(name:string):Prem;
begin
  add_Brem:=add_rem(name,global.Brem,global.nBrem);
  end;{add_remark}

function add_corp(name:string):Prem;
begin
  add_corp:=add_rem(name,global.corp,global.nCorp);
  end;{add_corp}

function add_departure(statf,statt:Pstation; wait,time,length:my_int; notet:Pnote):Pdeparture;
  var tmp:Pdeparture;
  begin
    tmp:=statf^.departures;
    while tmp<>nil do  {zkousi najit existujici dep}
    begin
      if tmp^.station=statt then
      if tmp^.wait=wait then
      if tmp^.time=time then
      if tmp^.length=length then
      if tmp^.note=notet then break;
      tmp:=tmp^.next;
      end;
      if tmp=nil then {pridava novou dep}
      begin
        new(tmp);
        tmp^.wait:=wait;
        tmp^.time:=time;
        tmp^.length:=length;
        tmp^.station:=statt;
        tmp^.note:=notet;
        tmp^.num:=0;
        if statf^.departures<>nil
           then tmp^.id:=statf^.departures^.id + 1
           else tmp^.id:=1;
        tmp^.next:=statf^.departures;
        statf^.departures:=tmp;
        inc(global.ndepartures);
        end;
      inc(tmp^.num);
      add_departure:=tmp;
    end;{add_deparure}

procedure add_connection_name(name:string);
  var conn:Pconnection;
  begin
    new(conn);
    conn^.name:=name;
    conn^.next:=global.connections;
    global.connections:=conn;
    inc(global.nconnections);
    end;{add_connection_name}

procedure add_connection_head(time:my_int; town:Pstation; notet:Pnote);
  var conn:Pconnection;
  begin
    conn:=global.connections;
    conn^.time:=time;
    conn^.first:=town;
    conn^.list:=nil;
    conn^.Brem:=nil;
    conn^.Rrem:=nil;
    conn^.corp:=nil;
    conn^.note:=notet;
    end;{add_connection_head}

procedure add_connection_rec(dep:Pdeparture);
  var conn:Pconnection;
      list,konec:Plist;
  begin
    inc(global.nlist);
    conn:=global.connections;
    konec:=PList(@conn^.list);
    while konec^.next<>nil do konec:=konec^.next;
    new(list);
    konec^.next:=list;
    list^.next:=nil;
    list^.departure:=dep;
  end;{add_connection_rec}

procedure add_connection_rem(rem:Prem);
var
   remlist: Premlist;
  begin
    if rem=nil then exit;
    new(remlist);
    remlist^.next:=global.connections^.Rrem;
    remlist^.remark:=rem;
    global.connections^.Rrem:=remlist;
    end;

procedure add_connection_corp(rem:Prem);
var
   remlist: Premlist;
  begin
    if rem=nil then exit;
    new(remlist);
    remlist^.next:=global.connections^.corp;
    remlist^.remark:=rem;
    global.connections^.corp:=remlist;
    end;

procedure add_connection_brem(rem:Prem);
var
   remlist: Premlist;
  begin
    if rem=nil then exit;
    new(remlist);
    remlist^.next:=global.connections^.Brem;
    remlist^.remark:=rem;
    global.connections^.Brem:=remlist;
    end;

procedure init_hash;
var i:my_int;
begin
  for i:=0 to hash_table_length do
  global.stations^[i]:=nil;
  end;

Procedure swap_stat(var l1,l2:Pstation);
var tmp:Pstation;
begin
  tmp:=l1;
  l1:=l2;
  l2:=tmp;
  end;

procedure sort_towns;
var s,t,list:Pstation;
    i,ii:my_int;
    st:string;
    nils:boolean;
begin
  write(stderr, 'sort stat');
  list:=nil;
  i:=0;
  s:=nil;
  repeat
    nils:=true;  {komtrola konce}
    ii:=60000;
    st:='';
    for i:= 0 to hash_table_length do {pro celou hash table}
    begin
      t:=global.stations^[i];
      if t<>nil then
        if (t^.name>st) then   {hleda nejvetsi string}
        begin
          nils:=false;
          s:=t;
          st:=t^.name;
          ii:=i;
          end
        else nils:=false;
      end;
    if (ii<>60000) then
    begin
      global.stations^[ii]:=s^.next;  {vyskrtne ze hash table}
      s^.next:=list;              {a prida do seznamu}
      list:=s;
      end;
  until nils or(ii=60000);
  i:=1;
  s:=list;
  while (s<>nil) do {napise id}
  begin
    s^.id:=i;
    inc(i);
    s:=s^.next;
    end;
  global.stations^[0]:=list;
  writeln(stderr, '...DONE');
  end;{sort_towns}

Procedure swap_dep(var l1,l2:Pdeparture);
var tmp:Pdeparture;
begin
  tmp:=l1;
  l1:=l2;
  l2:=tmp;
  end;

procedure sort_departures;
var dep,depsort:Pdeparture;
    stat:Pstation;
    id,i:my_int;
begin
  write(stderr, 'sort deps');
  for i:=0 to hash_table_length do
  begin
    stat:=global.stations^[i];
    while stat<>nil do
    begin
      dep:=Pdeparture(@stat^.departures);
      depsort:=dep;
      id:=0;
      while depsort^.next<>nil do
      begin
        dep:=depsort;
        while dep^.next<>nil do
        if dep^.next^.num>depsort^.next^.num then
           begin
             swap_dep(dep^.next,depsort^.next);
             swap_dep(dep^.next^.next,depsort^.next^.next);
             end
           else dep:=dep^.next;
        depsort:=depsort^.next;
        inc(id);
        depsort^.id:=id;
        end;
      stat:=stat^.next;
      end;
    end;
    writeln(stderr, '...DONE');
  end;{sort_departures}

Procedure init;
begin
  new(global.stations);
  init_hash;
  with global do
  begin
    version:='TTZIP v="0.1.3"     ';
    connections:=nil;
    Rrem:=nil;
    Brem:=nil;
    Corp:=nil;
    notes:=nil;
    nstations:=0;
    nconnections:=0;
    nRrem:=0;
    nBrem:=0;
    nCorp:=0;
    ndepartures:=0;
    nlist:=0;
    nNotes:=0;
    end;
  end;

{================================= APENDIX =================================}
begin
  init;
end.
{================================= END TYPY =================================}
