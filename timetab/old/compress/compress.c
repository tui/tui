/* -*- linux-c -*-
 * .tt file compression, distribute under GPL version 2 or later
 * Copyright 2000 Pavel Machek <pavel@ucw.cz>
 *
 * Compresses .tt files. Decompressor not yet written ;-). It seems
 * to squash .tt file into 75% of size produced by bzip2.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#define COMPATIBLE
#ifndef COMPATIBLE
#include <unistd.h>
#include <sys/mman.h>
#endif
#include <time.h>
#include <ctype.h>
#include <errno.h>

#define MEMORY_SIZE 	(128*1024*1024)
#define MEMORY_START	(0x60000000)
#define GRP_CHAR	'+'
#undef LITE

#define MAX_REMARKS	255
#define REMARK_SIZE	14

int pargc;
char **pargv;
char *table_file = "docomp.tt", *memory_file = "vlak.mem";

typedef unsigned char uchar;

struct id {
	struct id *next;
	char *name;
        char *cond;	/* Condition which days the train goes */
	/* Hmm, if train starts at 23:00 and arrives at 1:00, and goes on fridays only, when does it go? */
        char *note;     /* Train note */
	char *company;	/* Company which owns the node */
	int _id;
        uchar remark;
};

static int town_id;

struct node {
	struct node *next;
#ifndef LITE
	struct town *from;
#endif
	struct town *to;
	struct id *id;

	int _id, to_id, time1, time2, km;
	int node_id;
	uchar remark;
};

struct town {
	uchar *name;
	struct town *next;
	struct node *last_node;
	struct node *nodes;
	unsigned short _id;	/* Warning: it is possible to hit this limit */
	struct node *by;	/* changed at runtime */
	int heapindex;		/* changed at runtime */
	unsigned short time;	/* changed at runtime */
	char final;		/* changed at runtime */

	int node_id;
};

#define HASHSIZE 60000
#define INF 65000
#define ANY (25*60)

struct state {
	char version[20];
	struct town *townhash[HASHSIZE];
	struct id *ids;
	unsigned short numtowns;
	int numnodes;
	int id_id;
        int n_remarks;
        char remarkvec[MAX_REMARKS][REMARK_SIZE];
};

struct state *state;

int finalized = -1;

struct town **townheap;

uchar *memory, *memory_end;

char *opts = "";
int details = 0;
int mode = 0;
#define LIST 1
#define IDLIST 3
int slow = 0;
struct tm date; /* FIXME: this is broken. We can travel more than one day, etc. */

#define DTL_STATIONS	1
#define DTL_NOTE	2
#define DTL_COND	4
#define DTL_DEST        8
#define DTL_HEAD        16
#define DTL_TAIL        32
#define DTL_SUM        	64
#define DTL_COMPANY	128

void *my_malloc(int size)
{
	void *res = memory;
	memory += size;
	if (memory>memory_end) {
		fprintf( stderr, "Allocated memory exhausted\n" );
		exit(5);
	}
	return res;
}

uchar *my_strdup(uchar *s)
{
#ifndef LITE
	char *res = my_malloc(strlen(s)+1);
	strcpy(res, s);
	return res;
#else
	return NULL;
#endif
}

struct town *get_hash(uchar *name)
{
	int hash = (name[0] + 64*name[1] + (64*64)*name[2] + (64*64*64)*name[3])%HASHSIZE;
	return state->townhash[hash];
}

struct town *get_town(uchar *name, int create)
{
	int hash = (name[0] + 64*name[1] + (64*64)*name[2] + (64*64*64)*name[3])%HASHSIZE;
	struct town *res = get_hash(name);

	while (res != NULL) {
		if (!strcmp(res->name, name))
			return res;
		res = res->next;
	}
	if (!create)
		return NULL;	

	res = my_malloc(sizeof(struct town));
	memset(res, 0, sizeof(struct town));
	res->name = my_malloc(strlen(name)+1);
	strcpy(res->name, name);
	res->next = state->townhash[hash];
	res->time = INF;
	res->_id = town_id++;
	state->townhash[hash] = res;
	state->numtowns++;
	return res;
}

int pstate = 0, curtime, curkm, lasttime;
char oldremark[1024];

void store_remark(uchar *to)
{
	int i;

        for(i=0;i<=state->n_remarks;i++) {
		if (!strcmp(oldremark,state->remarkvec[i]))
			{*to=i;break;}
		if (i==state->n_remarks) {
			if (state->n_remarks==MAX_REMARKS) {
				fprintf( stderr, "Remarks Limit Exceeded\n");
				exit(1);
			}
			if (strlen(oldremark)>REMARK_SIZE) {
				fprintf( stderr, "Remarks Size Exceeded\n");
				exit(1);
			}
			strncpy(state->remarkvec[state->n_remarks],oldremark,REMARK_SIZE);
			*to=state->n_remarks++;
			break;
		}
	}
}

struct town *pprev;

FILE *lines, *texts, *nodes;

struct town *
decodeline(struct id *id, uchar *buf, struct town *prev)
{
	int i = 0;
	char *townname;
	int remindx = 0;
	char remark[10240];
	int t1 = -1, t2 = -1, from = -1, to = -1, km = -1;

	if (buf[i]!='\t') {
		if (sscanf( buf+i, "%d:%d", &t1, &t2 ) == 2)
			from = t1*60+t2;
		else	if (sscanf (buf+i, "+%d", &t1 ) != 1) {
			fprintf( stderr, "Could not find time 1 on %s", buf );
			exit(1);
		} else	from = lasttime + t1;
		lasttime = from;
	}
	while (buf[i]!='\t') i++;
	i++;

	if (buf[i]!='\t') {
		if (sscanf( buf+i, "%d:%d", &t1, &t2 ) == 2)
			lasttime = to = t1*60+t2;
		else	if (sscanf (buf+i, "+%d", &t1 ) != 1) {
			fprintf( stderr, "Could not find time 2 on %s", buf );
			exit(1);
		} else	to = lasttime + t1;
		lasttime = to;
	} else { to = -1; pstate = 0; }
	while (buf[i]!='\t') i++;
	i++;
        while (buf[i]!='\t') {
		remark[remindx++]=buf[i++];
	}
	remark[remindx]=0;
	i++;

	if (from==-1) from=to;
	if (to==-1) to=from;
	if (from==-1) {
		fprintf( stderr, "\nNo time on line %s\n", buf );
		return NULL;
	}

	if (sscanf(buf+i, "%d", &km ) != 1) {
		fprintf( stderr, "Kilometers not right on %s *%s\n", buf, buf+i );
		exit(1);
	}
	while (buf[i]!='\t') i++;
	i++;
	townname = buf+i;


//	fprintf( stderr, "Got %s: %s %d:%d %d:%d\n", id->name, townname, from/60, from%60, to/60, to%60 );
	
	{
		struct town *this;
		this = get_town(townname, 1);

//		printf( "%s -> %s (%s) %d %d %d\n", prev?prev->name:"", this->name, pprev?pprev->name:"", from-curtime, to-from, km );

		if (prev) {
			struct node *node = prev->nodes;
			int found = -1;
			while(node) {
				if ((node->to_id == this->_id) &&
				    (node->time1 == from-curtime) &&
				    (node->time2 == to-from) &&
				    (node->km == km))
					found = node->node_id;
				node = node->next;
			}

			if (found == -1) {
				struct node *node = my_malloc(sizeof(struct node));
				memset(node, 0, sizeof(struct node));

				node->next = prev->nodes;
				found = node->node_id = prev->node_id;
				node->to_id = this->_id;
				node->time1 = from-curtime;
				node->time2 = to-from;
				node->km = km;
				prev->nodes = node;
				prev->node_id++;

				fprintf(nodes, "%d/%d %d %d %d %d\n", prev->_id, found, this->_id, /* pprev?pprev->_id:-1, */ from-curtime, to-from, km );

			}
			fprintf(lines, "%d ", found);

			state->numnodes++;
		} else {
//			fprintf(g, "%s %d %d:", id->name, this->_id, to );
			fprintf(lines, "%d %d:", this->_id, to );
		}


		pprev = prev;
		curkm = km;
		curtime = to;
                strcpy(oldremark,remark);
		return this;
	}
}

void do_end_id(struct id *id,char **note, char **company)
{
        if (id==NULL) return;
        if (*note!=NULL) {
        	id->note=my_malloc(strlen(*note)+1);
                strcpy(id->note,*note);
        	free(*note);
                *note=NULL;
        }
        if (*company!=NULL) {
        	id->company=my_malloc(strlen(*company)+1);
                strcpy(id->company,*company);
        	free(*company);
                *company=NULL;
	}
        store_remark(&id->remark);
	fprintf(texts, "%s\t%s\t%s\t%s\n", id->name, id->note?id->note:"", id->company?id->company:"", id->cond?id->cond:"");
        state->id_id++;
}

void
readit(void)
{
	uchar buf[10240];
	char *note = NULL;
	char *company = NULL;
	struct id *id = NULL;
	FILE *f = fopen( table_file, "r" );
	int lineno = 0;
	struct town *prev = NULL;

	lines = fopen( "lines.ttz", "w" );
	nodes = fopen( "nodes.ttz", "w" );
	texts = fopen( "texts.ttz", "w" );
	

	state = my_malloc(sizeof(struct state));
	state->numtowns = 0;
	state->numnodes = 0;
	strcpy(state->version, __DATE__);
	state->id_id = 0;
        state->n_remarks = 1;
        state->remarkvec[0][0]=0;
	while (1) {
		lineno++;
		if (!(lineno % 10000))
			fprintf( stderr, "Reading: %dK lines, %d towns, %d nodes, %.1fMB mem\r", lineno/1000, state->numtowns, state->numnodes, ((double) ((char *) memory - (char *)MEMORY_START))/(1024*1024));
		if (fgets(buf, 10230, f)==NULL) break;
		if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
                if (buf[0]==0) continue;
		switch (*buf) {
		case '#':
                	do_end_id(id,&note,&company);
			lasttime = -9999;
			id = my_malloc(sizeof(struct id));
			id->name = my_malloc(strlen(buf+2)+1);
			strcpy(id->name, buf+2);
			pstate = 1;
			fprintf(lines,"\n");
			prev = NULL; 
			pprev = NULL;
			continue;
		case 'R':
			id->cond = my_strdup(buf+1);
			pstate = 0;
			break;
		case 'G':
			if (company==NULL) {
				company = malloc(strlen(buf+1)+1);
				strcpy(company,"");
			} else {
				company=realloc(company,strlen(company)+1+strlen(buf+1)+1);
				strcat(company,"\n");
			}
			strcat(company, buf+1);
			pstate = 0;
			break;
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'H': case 'I': case 'J': case 'K': case 'L': case 'M':
		case 'N': case 'O': case 'P': case 'Q': case 'S': case 'T':
		case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
		case '[':
			if (note==NULL) {
				note = malloc(strlen(buf+1)+1);
				strcpy(note,"");
			} else {
				note=realloc(note,strlen(note)+1+strlen(buf+1)+1);
				strcat(note,"\n");
			}
			strcat(note, buf+1);
			pstate = 0;
			break;
		case ';':	/* Remark */
			break;
		case '!':	/* Followed by keyword */
			{
				int y1, m1, d1, y2, m2, d2, i, j;
				if (sscanf(buf+1, "valid y%d m%d d%d .. y%d m%d d%d\n",
					   &y1, &m1, &d1, &y2, &m2, &d2) == 6) {
					continue;
				}

				if (sscanf(buf+1, "version %d.%d\n", &i, &j) == 2) {
					continue;
				}

			}
			break;
		case 0:
			break;
		default:
			if (pstate == 1)
				prev = decodeline(id, buf, prev);
			else {
				fprintf( stderr, "Line in unexpected state: %s\n", buf );
			}
		}
	}
	do_end_id(id,&note,&company);

	fprintf( stderr, "Ready  : %d lines, %d towns, %d nodes, %.2fMB mem\n", lineno, state->numtowns, state->numnodes, ((double) ((char *) memory-(char *)MEMORY_START))/(1024*1024));
        fclose(f);
}

void
townlist(void)
{
	int i;
	struct town *town;
	for (i=0; i<HASHSIZE; i++) {
		town = state->townhash[i];
		while (town) {
			printf( "%s\t%d\n", town->name, town->_id );
			town = town->next;
		}
	}
}

static int linenum = 0;

void
idlist(void)
{
	struct id *id = state->ids;
	printf("%d\n", state->id_id);
	while (id) {
		printf("%d\t%s\t%s\t%s\n", id->_id, id->name, id->cond ? id->cond : "", id->note ? id->note : "");
		id = id->next;
	}
}

void
setup_memory(void)
{
#ifndef COMPATIBLE
		int h;
		h = open(memory_file, O_RDWR);
		if (h!=-1) {
			fprintf( stderr, "Memory image already present.\n" );
			memory = (uchar *) MEMORY_START;
			memory_end = memory + MEMORY_SIZE;
			if (mmap(memory, memory_end - memory, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, h, 0)== (void *) ~0) {
				fprintf( stderr, "Could not allocate memory: %m\n" );
				exit(10);
			}
				
			state = (struct state *) memory;
			if (strcmp(state->version, __DATE__))
				fprintf( stderr, "Warning, .mem file created by another version!\n" );
		} else {
			char buf[1024];
			fprintf( stderr, "Creating empty file for memory image.\n" );
			h = open(memory_file, O_RDWR | O_CREAT, 0644);
#if 1
	                ftruncate(h, MEMORY_SIZE);	/* Provokes kernel bug on 2.4.0-test8 */
#else
			sprintf(buf, "head -c %d < /dev/zero > %s", MEMORY_SIZE, memory_file );
			system(buf);
#endif
			memory = (uchar *) MEMORY_START;
			memory_end = memory + MEMORY_SIZE;
			if (mmap(memory, memory_end - memory, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, h, 0)== (void *) ~0) {
				fprintf( stderr, "Could not allocate memory: %m\n" );
				exit(10);
			}
			readit();
	                ftruncate(h,memory - (uchar *)MEMORY_START);
			fprintf( stderr, "Memory image now created.\n" );
			memory = (void *) MEMORY_START;
                	if (mmap(memory, memory_end - memory, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, h, 0)== (void *) ~0) {
				fprintf( stderr, "Could not allocate memory: %m\n" );
				exit(10);
			}
		}
#else
		memory = malloc(MEMORY_SIZE);
		if (!memory) {
			fprintf( stderr, "Could not allocate memory: %m\n" );
			exit(10);
		}
		memory_end = memory + MEMORY_SIZE;
		readit();
#endif
}


int
main(int argc, char *argv[])
{
	if (argc>1)
		if (*argv[1] == '-') {
			opts = argv[1]+1;
			argv++;
			argc--;
		}
	if (strchr(opts,'s')) slow = 1;
	if (strchr(opts,'l')) mode = LIST;
	if (strchr(opts,'v')) mode = LIST;
	if (strchr(opts,'i')) mode = IDLIST;
	if (strchr(opts,'b')) { table_file = "bus.tt"; memory_file = "bus.mem"; }
	if (strchr(opts,'t')) { table_file = "vlak.tt"; memory_file = "vlak.mem"; }
	if (strchr(opts,'x')) { table_file = "xyzzy.tt"; memory_file = "xyzzy.mem"; }
	if (strchr(opts,'d')) { table_file = "/proc/self/fd/0"; memory_file = "stdin.mem"; }
	
	sscanf(opts,"%d",&details);
	setup_memory();
	townlist();
	return 0;
}
