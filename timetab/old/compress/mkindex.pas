unit mkindex;
{
Procedures for building index-file
version 0.1.2

(c)2001 Petr Cermak (cermak01@seznam.cz)

written under GNU Library Public Licence version 2 or later

}
interface
 uses typy;

procedure make_indexfile(var f:text);
{============================== Implementation ==============================}
implementation

procedure index_stations;
var
   conn: Pconnection;
   list: Plist;
   connlist: Pconnlist;
   i: my_int;
begin
   write('Creating index');
   conn:=global.connections;
   i:=1;
   while conn<>nil do
   begin
       new(connlist);
       connlist^.index:=i;
       connlist^.next:=conn^.first^.connlist;
       conn^.first^.connlist:=connlist;
       list:=conn^.list;
       while list<>nil do
       begin
            new(connlist);
            connlist^.index:=i;
            connlist^.next:=list^.departure^.station^.connlist;
            list^.departure^.station^.connlist:=connlist;
            list:=list^.next;
       end;
       conn:=conn^.next;
       inc(i);
   end;
   writeln(#13'done                              ');
end;

procedure write_indexfile(var f:text);
var
   stat:Pstation;
   i:my_int;
   connlist: Pconnlist;
   a,b:my_int;
begin
  write('Writing indexfile');
  for i:=0 to hash_table_length do
  begin
       stat:=global.stations^[i];
       while stat<>nil do
       begin
            connlist:=stat^.connlist;
            a:=global.nconnections-connlist^.index+1;
            while connlist<>nil do
            begin
                 if a>=0 then
                 begin
                    write(f,#9,a);
                    b:=a;
                    a:=-1;
                 end
                 else
                 begin
                    write(f,#9, global.nconnections-connlist^.index+1 - b);
                    b:=global.nconnections-connlist^.index+1;
                 end;
                 connlist:=connlist^.next;
            end;
            write(f,#10);
            stat:=stat^.next;
       end;
  end;
  write(#13'done                              ');
end;

procedure make_indexfile(var f:text);
begin
  sort_departures;
  sort_towns;
  index_stations;
  write_indexfile(f);
end;

{================================= APENDIX =================================}
begin
end.
{================================ END INDXUNIT ================================}
