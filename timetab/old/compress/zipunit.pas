unit zipunit;
{
Libary for write zip file *.ttz
version 0.1.5

Copyright (c)2001  Dolejsek Zbynek (klon@papouch.cz)

written under GNU Library Public Licence version 2 or later

Code completed and some changes made (c)2001 Petr Cermak (cermak01@seznam.cz)
}

{It is supposed to compile this unit with strings as ansistrings, because there can be
several remarks longer than 255 characters.
Switch for freepascal follows..}
{$H+}

interface
 uses typy;
{types of pointers}
{types of structures}
{================================ Functions ================================}
{================================ Procedures ================================}
procedure read_zip(var f:text);
procedure write_zip(var f:text);
{============================== Implementation ==============================}
implementation
{=================================== VAR ===================================}
var zip:Ttimetab;
{================================= READ ZIP =================================}
function my_eol(var f:text):boolean;
var c:char;
begin
  read(f,c);
  if c=#10 then my_eol:=true
           else my_eol:=false;
  end;

function rline(var f:text):string;
  var c:char;
      s:string;
  begin
    s:='';
    read(f,c);
    while (c<>#10) and (c<>#26) do
    begin
      s:=s+c;
      read(f,c);
      end;
    rline:=s;
    end;{rline}

procedure check(tag, found : string);
begin
   if tag<>found then
   begin
      writeln(stderr, 'tag expected: ', tag, ', but ', found, ' found.');
      halt(1);
   end;
end;

Procedure read_head(var f:text);
var
   s : string;
begin
  s:=rline(f);
  check('# TTZIP', s);
  read(f,zip.ndepartures);
  read(f,zip.nstations);
  read(f,zip.nconnections);
  read(f,zip.nRrem);
  read(f,zip.nBrem);
  read(f,zip.nCorp);
  read(f,zip.Nnotes);
  rline(f);
  end;

procedure read_towns(var f:text);
  var s:string;
      id:my_int;
  begin
    id:=1;
    s:=rline(f);
    check('# TOWNS', s);
    s:=rline(f);
    while s<>'# CONN' do
    begin
      add_station(s,id,id mod (hash_table_length+1));
      write(stderr, 'stations ',global.nstations:6,'/',zip.nstations:6,#13);
      s:=rline(f);
      inc(id);
      end;
    writeln(stderr);
    if global.nstations<>zip.nstations then writeln(stderr,'read ttz: ERROR in stations names')
                                       else writeln(stderr,'read ttz: stat ',global.nstations*sizeof(Tstation) shr 10,'KB');
    end;

procedure read_conn(var f:text);
var s:string;
    conn:Pconnection;
begin
  s:=rline(f);
  while s<>'# BREM' do
  begin
    add_connection_name(s);
    s:=rline(f);
    write(stderr,'conn ',global.nconnections,'/',zip.nconnections,#13);
    end;
  writeln(stderr);
  if zip.nconnections<>global.nconnections then writeln(stderr,'read ttz: ERROR in conn names')
                                           else writeln(stderr,'read ttz: conn ',global.nconnections*sizeof(Tconnection) shr 10,'KB');
  zip.connections:=nil;
  while global.connections<>nil do
  begin
    conn:=global.connections;
    global.connections:=conn^.next;
    conn^.next:=zip.connections;
    zip.connections:=conn;
    end;
  end;

procedure read_dep(var f:text);
{druhu spojeni}
var
    s                : string;
    id, noteid, code : my_int;
    dep              : Tdeparture;
    notet            : Pnote;
begin
  s:=rline(f);
  while s<>'# CONNS' do
  begin
    val(copy(s, 1, pos(#9, s)-1), id, code);                   {Uh, why can't pascal have sscanf?}
    s:=copy(s, pos(#9, s)+1, length(s));
    val(copy(s, 1, pos(#9, s)-1), dep.wait, code);
    s:=copy(s, pos(#9, s)+1, length(s));
    val(copy(s, 1, pos(#9, s)-1), dep.time, code);
    s:=copy(s, pos(#9, s)+1, length(s));
    val(copy(s, 1, pos(#9, s)-1), dep.length, code);
    s:=copy(s, pos(#9, s)+1, length(s));
    val(copy(s, 1, pos(#9, s)-1), dep.id, code);
    s:=copy(s, pos(#9, s)+1, length(s));
    val(s, noteid, code);
    notet:=find_note(noteid, global.notes);
    dep.next:=add_departure(
                            add_station('',id,id mod (hash_table_length+1)),
                            add_station('',dep.id,dep.id mod (hash_table_length+1)),
                            dep.wait,dep.time,dep.length,notet);
    write(stderr, #13'dep',global.ndepartures:6,'/',zip.ndepartures:6,mymem shr 10:6,'KB');
    s:=rline(f);
    end;
  writeln(stderr);
  if zip.ndepartures<>global.ndepartures then writeln(stderr, 'read ttz: ERROR in departures', zip.ndepartures, ':', global.ndepartures)
                                         else writeln(stderr, 'read ttz: dep ',global.ndepartures*sizeof(Tdeparture) shr 10,'KB');
  end;


procedure read_notes(var f:text);
  var s:string;
  begin
    s:=rline(f);
    while s<>'# DEP' do
    begin
      add_note(s);
      s:=rline(f);
      write('notes ',global.Nnotes,'/',zip.Nnotes,#13);
      end;
    writeln;
    if zip.Nnotes<>global.Nnotes then writeln('read ttz: ERROR in station notes')
                                 else writeln('read ttz: notes ',global.Nnotes*sizeof(TNote) shr 10,'KB');
end;

procedure read_rem(var f:text);
  var s:string;
  begin
    s:=rline(f);
    while s<>'# RREM' do
    begin
      add_rem(s,global.brem,global.nbrem);
      s:=rline(f);
      write(stderr, 'b rem ',global.nbrem,'/',zip.nbrem,#13);
      end;
    writeln(stderr);
    if zip.nbrem<>global.nbrem then writeln(stderr, 'read ttz: ERROR in B remarks')
                               else writeln(stderr, 'read ttz: bRem ',global.nbrem*sizeof(Trem) shr 10,'KB');
    s:=rline(f);
    while s<>'# CORP' do
    begin
      add_rem(s,global.rrem,global.nrrem);
      s:=rline(f);
      write(stderr, 'r rem ',global.nrrem,'/',zip.nrrem,#13);
      end;
    writeln(stderr);
    if zip.nrrem<>global.nrrem then writeln(stderr, 'read ttz: ERROR in R remarks')
                               else writeln(stderr, 'read ttz: rRem ',global.nRrem*sizeof(Trem) shr 10,'KB');
    s:=rline(f);
    while s<>'# NOTES' do
    begin
      add_rem(s,global.corp,global.ncorp);
      s:=rline(f);
      write(stderr, 'corp ',global.ncorp,'/',zip.ncorp,#13);
      end;
    writeln(stderr);
    if zip.ncorp<>global.ncorp then writeln(stderr, 'read ttz: ERROR in corporations')
                               else writeln(stderr, 'read ttz: corp ',global.ncorp*sizeof(Trem) shr 10,'KB');
    end;


function read_remark(var f:text):my_int;
var s:string;
    c:char;
begin
    s:='';
    read(f,c);
    while (c<>';') and (c<>#9) do
    begin
      s:=s+c;
      read(f,c);
    end;
    if c=#9 then
       read_remark:=-1
    else
       read_remark:=my_val(s);
end;

procedure read_conns(var f:text);
var fid,nid,time,id,br:my_int;
    conn:Pconnection;
    stat:Pstation;
    dep:Pdeparture;
    num:longint;
    c:char;
begin
  conn:=zip.connections; num:=0;
  while conn<>nil do
  begin
    inc(num);
    write(stderr, 'conns:',num,'/',zip.nconnections,#13);
    zip.connections:=conn^.next;
    conn^.next:=global.connections;
    global.connections:=conn;
    read(f,fid,time,nid);
    add_connection_head(time,add_station('',fid,fid mod (hash_table_length+1)),find_note(nid,global.notes));
    read(f,c);   {skip one #9}
    br:=read_remark(f);
    while br<>-1 do
    begin
         add_connection_brem(find_rem(br,global.brem));
         br:=read_remark(f);
    end;
    br:=read_remark(f);
    while br<>-1 do
    begin
         add_connection_rem(find_rem(br,global.rrem));
         br:=read_remark(f);
    end;
    br:=read_remark(f);
    while br<>-1 do
    begin
         add_connection_corp(find_rem(br,global.corp));
         br:=read_remark(f);
    end;
    stat:=conn^.first;
    while not my_eol(f) do begin
      read(f,id);
      dep:=stat^.departures;
      while (dep<>nil)and(id<>dep^.id) do dep:=dep^.next;
      add_connection_rec(dep);
      stat:=dep^.station;
      end;
    conn:=zip.connections;
    end;
    writeln(stderr);
  end;

procedure read_zip(var f:text);
begin
   writeln(stderr, 'read zip');
   read_head(f);
   read_towns(f);
   read_conn(f);
   read_rem(f);
   read_notes(f);
   read_dep(f);
   read_conns(f);
end;{read_zip}
{================================= READ ZIP =================================}
{================================ WRITE ZIP ================================}
procedure write_towns(var f:text);
  var stat:Pstation;
      i:my_int;
  begin
    write(f,'# TOWNS'#10);
    for i:=0 to hash_table_length do
    begin
      stat:=global.stations^[i];
      while stat<>nil do
      begin
        write(f,stat^.name,#10);
        stat:=stat^.next;
        end;
      end;
    end;

procedure write_connection_names(var f:text);
  var conn:Pconnection;
  begin
    write(f,'# CONN'#10);
    conn:=global.connections;
    while conn<>nil do
    begin
      write(f,conn^.name,#10);
      conn:=conn^.next;
      end;
    end;


procedure write_notes(var f:text);
  var notet:Pnote;
  begin
    write(f,'# NOTES'#10);
    notet:=global.notes;
    while notet<>nil do
    begin
      write(f,notet^.name,#10);
      notet:=notet^.next;
      end;
    end;

function rems(var rem:Prem; var id:my_int):string;
  begin
    rems:=rem^.name;
    rem^.id:=id;
    inc(id);
    rem:=rem^.next;
    end;

procedure write_remarks(var f:text);
  var rem:Prem;
      id:my_int;
  begin
    rem:=global.Brem; id:=1;
    write(f,'# BREM'#10);
    while rem<>nil do write(f,rems(rem,id),#10);
    rem:=global.Rrem; id:=1;
    write(f,'# RREM'#10);
    while rem<>nil do write(f,rems(rem,id),#10);
    rem:=global.corp; id:=1;
    write(f,'# CORP'#10);
    while rem<>nil do write(f,rems(rem,id),#10);
    end;

procedure write_head(var f:text);
{zapis hlavicky soubor.ttz}
begin
  write(f,'# TTZIP'#10);
  write(f,global.ndepartures,#10);
  write(f,global.nstations,#10);
  write(f,global.nconnections,#10);
  write(f,global.nRrem,#10);
  write(f,global.nBrem,#10);
  write(f,global.nCorp,#10);
  write(f,global.nNotes,#10);
  end;

procedure write_departures(var f:text);
{druhu spojeni}
var stat:Pstation;
    dep:Pdeparture;
begin
  stat:=global.stations^[0];
  write(f,'# DEP');
  while stat<>nil do
  begin
    dep:=stat^.departures;
    while dep<>nil do
    begin
      if dep^.note<>nil then
         write(f,#10,stat^.id,#9,dep^.wait,#9,dep^.time,#9,dep^.length,#9,dep^.station^.id,#9,global.nNotes-dep^.note^.id+1)
      else
         write(f,#10,stat^.id,#9,dep^.wait,#9,dep^.time,#9,dep^.length,#9,dep^.station^.id,#9,0);
      dep:=dep^.next;
      end;
    stat:=stat^.next;
    end;
  write(f,#10);
  end;

procedure write_remlist(var f:text; remlist: Premlist);
begin
  if remlist=nil then
  begin
       write(f,0,';',#9);
       exit;
  end;
  while remlist<>nil do
  begin
       write(f,remlist^.remark^.id,';');
       remlist:=remlist^.next;
  end;
  write(f, #9);
end;

procedure write_connections(var f:text);
{vlakovych spojeni}
var conn:Pconnection;
    list:Plist;
begin
  write(f,'# CONNS'#10);
  conn:=global.connections;
  while conn<>nil do
  begin
    write(f,conn^.first^.id,#9,conn^.time,#9);
    if conn^.note<>nil then
        write(f,global.nNotes-conn^.note^.id+1,#9)
    else
        write(f,0,#9);
    write_remlist(f,conn^.brem);
    write_remlist(f,conn^.rrem);
    write_remlist(f,conn^.corp);
    list:=conn^.list;
    write(f,#9);
    while list<>nil do
    begin
      write(f,list^.departure^.id);
      list:=list^.next;
      if list<>nil then write(f,' ');
      end;
    write(f,#10);
    conn:=conn^.next;
    end;
  end;

procedure write_zip(var f:text);
begin
  sort_departures;
  sort_towns;
  write_head(f);
  write(stderr, 'write ttz: ');
  write_towns(f);
  write(stderr, 'towns ');
  write_connection_names(f);
  write(stderr, 'connections ');
  write_remarks(f);
  write(stderr, 'remarks ');
  write_notes(f);
  write('notes ');
  write_departures(f);
  write(stderr, 'deps ');
  write_connections(f);
  writeln(stderr, ' conns ...DONE');
  end;
{================================ WRITE ZIP ================================}

{================================= APENDIX =================================}
begin
end.
{=============================== END ZIPUNIT ===============================}
