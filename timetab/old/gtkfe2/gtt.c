/******************************************************* 
gTimeTab - frontend for searching time tables
copyright (C) Roman Krejcik, 2002
distribute under GNU GPL v2 or later
********************************************/

//for using gtk_text in gtk 2.0
#define GTK_ENABLE_BROKEN

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gnome.h>
#include <time.h>

/* Look and feel constants */
//#define MAX_POPUP_ITEMS 20
#define FRAME_INNER_BORDER 3
#define NUM_OF_QUICK_SEL 12

#define HEADFONT "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"
#define TEXTFONT "-misc-fixed-medium-r-normal-*-*-120-*-*-c-*-*"
#define REMARKFONT "-adobe-helvetica-medium-r-normal-*-12-*-*-*-*-*-*-*"
#define BORDERFONT "-adobe-helvetica-medium-r-normal-*-*-80-*-*-p-*-*"

#define LABEL_DETAILS "Details for train:"
#define LABEL_MESSAGE "Message:"
GdkFont *headfont, *textfont, *remarkfont, *border; // fonts for details window

const gchar *CONN_LABEL = " Connection ";
const gchar *OPT_LABEL = " Options ";
const gchar *DETAIL_LABEL = " Details ";
const gint key_constants[10] = { GDK_1, GDK_2, GDK_3, GDK_4, GDK_5, GDK_6, GDK_7, GDK_8, GDK_9 };
const gint notebook_key_constants[3] = { GDK_c, GDK_o, GDK_d };

/* Buffers length */
#define MAX_CONFIG_LINE 255
#define MAX_COMMAND_LEN 255
#define MAX_DB 20
#define MAX_RESULT_TRAINS 99

const gchar EMPTY_STRING[1] = "";
const gint note_keys[] = { 0, 1, 2};

char **town_list;

gchar db[MAX_DB][MAX_CONFIG_LINE];
gint db_counter, db_loaded_flag;
gchar loaded_db[MAX_CONFIG_LINE] = "";

struct {
  int towns;
} config;

struct TRAIN_LIST {
  char arr[6],dep[6],flag[6],dist[6],station[MAX_CONFIG_LINE];
  struct TRAIN_LIST *next;
} ; 

struct REMARK_LIST {
  char r[MAX_CONFIG_LINE];
  struct REMARK_LIST *next;
} ;

struct RESULT_LIST {
  struct RESULT_LIST *next;
  char *name;
  struct TRAIN_LIST *train;
  struct REMARK_LIST *remark;
} *result[MAX_RESULT_TRAINS];

struct RESULT_TRAIN {
  char name[MAX_CONFIG_LINE];
  char *arr, *dep, *station;
  int dist;
  struct RESULT_TRAIN *next;
} *trains[MAX_RESULT_TRAINS];

GtkAccelGroup *notebook_accel;
GtkWidget *accel_button[3], *note;

/* Connection netobook page */
GtkWidget *from, *to, *quick[NUM_OF_QUICK_SEL], *quick_label[NUM_OF_QUICK_SEL], *date_edit, *overview, *detailed_result;
GtkCombo *qs_focus;
GtkAccelGroup *qs_accel;
int detailed_rows, train_selected, overview_rows;;

/* Options notebook page */
GtkWidget *win_manage_db, *searcher_select, *manage_om, *db_select, *db_menu, *db_menu_manage, *num_result_entry;

/* Detail notebook page */
GtkWidget *text_details, *detail_label;


//GList *from_items, *to_items;

void *xmalloc(unsigned int size);
void add_menu_bar(GtkWidget *mainbox); 
void make_conn_page(GtkWidget *connection);
void make_opt_page(GtkWidget *options);
void make_detail_page(GtkWidget *details);
static void show_about(GtkWidget *widget, gpointer data);
static void db_changed(GtkWidget *widget, gpointer data);
int get_match_index(gchar *value);
static void update_quick_selection(GtkWidget *widget, gpointer data);
static void use_quick_selection(GtkWidget *widget, gpointer data);
static void change_focus(GtkContainer *container, GtkWidget *widget, gpointer data);
void rebuild_db_menu(void);
static void add_db(GtkWidget *widget, gpointer data);
static void remove_db(GtkWidget *widget, gpointer data);
static void manage_db_close(GtkWidget *widget, gpointer data);
static void manage_db_open(GtkWidget *widget, gpointer data);
static void create_manage_db_window(void);
int reload_db(char *path);
int cmp_fce(const void *a, const void *b);
void read_rc_file(void);
void quit_gtt(void);
static void search(GtkWidget *widget, gpointer data);
void overview_row_selected(GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data);
void detail_row_selected(GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data);
static void note_accel_used1(GtkWidget *widget, gpointer data);
static void note_accel_used2(GtkWidget *widget, gpointer data);
static void note_accel_used3(GtkWidget *widget, gpointer data);
static void show_help(GtkWidget *widget, gpointer data);
void msg(gchar *head, gchar *body);
int get_result_num(void);
static void clear_all_used(GtkWidget *widget, gpointer data);
static void clear_all(void);

int main(int argc, char *argv[])
{
  GtkWidget *window, *mainbox, *connection, *options, *details, *lab;
  gint i;
  
  gtk_init(&argc, &argv);
    
  headfont = gdk_font_load(HEADFONT);
  textfont = gdk_font_load(TEXTFONT);
  remarkfont = gdk_font_load(REMARKFONT);
  border = gdk_font_load(BORDERFONT);

  // Top level window
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect(GTK_OBJECT(window), "destroy",
		       GTK_SIGNAL_FUNC(quit_gtt), NULL);

  gtk_window_set_title(GTK_WINDOW(window), "gTimeTab");
  gtk_widget_set_usize(window, 500, 520);

  mainbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(window), mainbox);

  notebook_accel = gtk_accel_group_new();
  gtk_window_add_accel_group(GTK_WINDOW(window), notebook_accel);
    
  add_menu_bar(mainbox);

  qs_accel = gtk_accel_group_new();
  gtk_window_add_accel_group(GTK_WINDOW(window), qs_accel);
    
  note = gtk_notebook_new();
  gtk_notebook_popup_enable(GTK_NOTEBOOK(note));
  gtk_notebook_set_tab_vborder(GTK_NOTEBOOK(note),2); 
  
  connection = gtk_vbox_new(FALSE,4);
  gtk_container_set_border_width(GTK_CONTAINER(connection),4);
  options = gtk_vbox_new(FALSE,0);
  gtk_container_set_border_width(GTK_CONTAINER(options),12);
  details = gtk_vbox_new(FALSE,0);
  gtk_container_set_border_width(GTK_CONTAINER(details),3);
  
  lab = gtk_label_new(CONN_LABEL);
  gtk_label_set_pattern(GTK_LABEL(lab)," _");
  gtk_notebook_append_page(GTK_NOTEBOOK(note), connection, lab);
  lab = gtk_label_new(OPT_LABEL);
  gtk_label_set_pattern(GTK_LABEL(lab)," _");
  gtk_notebook_append_page(GTK_NOTEBOOK(note), options, lab);
  lab = gtk_label_new(DETAIL_LABEL);
  gtk_label_set_pattern(GTK_LABEL(lab)," _");
  gtk_notebook_append_page(GTK_NOTEBOOK(note), details, lab);

  make_conn_page(connection);
  make_opt_page(options);
  make_detail_page(details);

  for(i = 0; i < 3; i++) {
    accel_button[i] = gtk_button_new();
    gtk_widget_add_accelerator(accel_button[i], "clicked", notebook_accel, notebook_key_constants[i], GDK_MOD1_MASK, GTK_ACCEL_LOCKED);
  }
  gtk_signal_connect_object(GTK_OBJECT(accel_button[0]), "clicked", GTK_SIGNAL_FUNC(note_accel_used1), NULL);
  gtk_signal_connect_object(GTK_OBJECT(accel_button[1]), "clicked", GTK_SIGNAL_FUNC(note_accel_used2), NULL);
  gtk_signal_connect_object(GTK_OBJECT(accel_button[2]), "clicked", GTK_SIGNAL_FUNC(note_accel_used3), NULL);
  

  read_rc_file();

  gtk_box_pack_start(GTK_BOX(mainbox), note, TRUE, TRUE, 0);

  create_manage_db_window();

  if (db_counter) {
    strcpy(loaded_db, db[0]);
    reload_db(loaded_db);
  }
  
  rebuild_db_menu();

  gtk_widget_show_all(window);

  if (! db_counter) msg("  Information\n","\n You have no databese in your db list.\n First use manage button in Option page to add it."); 

  gtk_main();


  return 0;
}

void *xmalloc(unsigned int size) {
  void *p = malloc(size);
  if (!p) {
    g_print("Not enough memory. Program aborted.\n");
    gtk_main_quit();
  }
  return p;
}

void add_menu_bar(GtkWidget *mainbox) {
  GtkWidget *bar, *item, *menu;
  bar = gtk_menu_bar_new();
  gtk_box_pack_start(GTK_BOX(mainbox), bar, FALSE, FALSE, 0);


  item = gtk_menu_item_new_with_label("Help");
  gtk_label_set_pattern(GTK_LABEL(GTK_BIN(item)->child), "_");
  gtk_menu_bar_append(GTK_MENU_BAR(bar), item);
  gtk_widget_add_accelerator(item, "activate_item", notebook_accel, GDK_h, GDK_CONTROL_MASK, GTK_ACCEL_LOCKED);
  menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(item), menu);
  item = gtk_menu_item_new_with_label("About");
  gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(show_about), NULL);
  gtk_menu_append(GTK_MENU(menu), item);
  item = gtk_menu_item_new_with_label("Help");
  gtk_signal_connect(GTK_OBJECT(item), "activate", GTK_SIGNAL_FUNC(show_help), NULL);
  gtk_menu_append(GTK_MENU(menu), item);
}

void make_conn_page(GtkWidget *connection) {
  gint i;
  char s[4];
  GtkWidget *frame, *tab, *w, *lab;
  gchar *over_titles[5] = {"From","To","Trains","Total time","Distance"};
  gchar *detail_titles[5] = {"Arr","Dep","Station","Train code","Distance"};

  frame = gtk_frame_new("Connection parameters");
  gtk_box_pack_start(GTK_BOX(connection), frame, FALSE, FALSE, 1);
  tab = gtk_table_new(6,3, FALSE);
  gtk_container_set_border_width(GTK_CONTAINER(tab),FRAME_INNER_BORDER);
  gtk_table_set_row_spacings (GTK_TABLE(tab),5);
  w = gtk_label_new("From:");
  gtk_table_attach_defaults(GTK_TABLE(tab), w ,0,1,0,1);
  from = gtk_combo_new();
  gtk_combo_set_case_sensitive(GTK_COMBO(from), FALSE);
  gtk_signal_connect(GTK_OBJECT(from),"set-focus-child",change_focus, NULL);
  gtk_signal_connect(GTK_OBJECT(GTK_COMBO (from)->entry),"changed",update_quick_selection, NULL);
  gtk_table_attach_defaults(GTK_TABLE(tab), from ,1,2,0,1);
  w = gtk_label_new(" To: ");
  gtk_table_attach_defaults(GTK_TABLE(tab), w,2,3,0,1);
  to = gtk_combo_new();
  gtk_combo_set_case_sensitive(GTK_COMBO(to), FALSE);
  gtk_signal_connect(GTK_OBJECT(to),"set-focus-child",change_focus, NULL);
  gtk_signal_connect(GTK_OBJECT(GTK_COMBO (to)->entry),"changed",update_quick_selection, NULL);
  gtk_table_attach_defaults(GTK_TABLE(tab), to ,3,6,0,1);
  date_edit = gnome_date_edit_new(time(NULL), TRUE, TRUE);
  gtk_table_attach_defaults(GTK_TABLE(tab), date_edit ,0,4,1,2);
  w = gtk_button_new_with_label("Clear All");
  gtk_signal_connect(GTK_OBJECT(w),"clicked",GTK_SIGNAL_FUNC(clear_all_used), NULL);
  gtk_table_attach_defaults(GTK_TABLE(tab), w ,4,5,2,3);

  w = gtk_button_new();
  lab = gtk_label_new("Search");		
  gtk_label_set_pattern(GTK_LABEL(lab),"_");
  gtk_container_add(GTK_CONTAINER(w),lab);
  gtk_signal_connect(GTK_OBJECT(w),"clicked",search, NULL);
  gtk_widget_add_accelerator(w, "clicked", notebook_accel, GDK_s, GDK_MOD1_MASK, GTK_ACCEL_LOCKED);
  gtk_table_attach_defaults(GTK_TABLE(tab), w ,5,6,2,3);
  gtk_container_add(GTK_CONTAINER(frame),tab);

  frame = gtk_frame_new("Quick selection of town");
  gtk_box_pack_start(GTK_BOX(connection), frame, FALSE, FALSE, 1);
  tab = gtk_table_new(3, 4, TRUE);
  gtk_container_set_border_width(GTK_CONTAINER(tab),FRAME_INNER_BORDER);

  for(i = 0; i < NUM_OF_QUICK_SEL; i++) {
    GtkWidget *lab;
    sprintf(s, "%2d.", i+1);
    quick[i] = gtk_button_new();
    if (i<10) gtk_widget_add_accelerator(quick[i], "clicked", qs_accel, key_constants[i], GDK_MOD1_MASK, GTK_ACCEL_LOCKED);
    gtk_signal_connect(GTK_OBJECT(quick[i]),"clicked",use_quick_selection, NULL);
    w = gtk_hbox_new(FALSE,0);
    lab = gtk_label_new(s);
    if (i < 9) {
      gtk_label_set_pattern(GTK_LABEL(lab)," _");
    }
    gtk_box_pack_start(GTK_BOX(w),lab , FALSE, FALSE, 1);
    quick_label[i] = gtk_label_new(EMPTY_STRING);
    gtk_misc_set_alignment(GTK_MISC(quick_label[i]), 0.0, 0.0);
    gtk_box_pack_start(GTK_BOX(w),quick_label[i], TRUE, TRUE, 4);
    gtk_container_add(GTK_CONTAINER(quick[i]),w);
    gtk_table_attach_defaults(GTK_TABLE(tab), quick[i], i%4, i%4 + 1, i/4, i/4 + 1);
  }
  gtk_container_add(GTK_CONTAINER(frame),tab);

  frame = gtk_frame_new("Overview");
  gtk_box_pack_start(GTK_BOX(connection), frame, TRUE, TRUE, 1);
  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_container_set_border_width(GTK_CONTAINER(w),FRAME_INNER_BORDER);
  gtk_container_add(GTK_CONTAINER(frame),w);
  overview = gtk_clist_new_with_titles(5,over_titles);
  gtk_signal_connect(GTK_OBJECT(overview), "select_row", GTK_SIGNAL_FUNC(overview_row_selected), NULL);
  gtk_clist_set_column_min_width(GTK_CLIST(overview),0,100);
  gtk_clist_set_column_min_width(GTK_CLIST(overview),1,100);
  gtk_clist_set_column_min_width(GTK_CLIST(overview),2,35);
  gtk_clist_set_column_min_width(GTK_CLIST(overview),3,40);
  gtk_clist_set_column_min_width(GTK_CLIST(overview),4,40);
  gtk_clist_set_column_max_width(GTK_CLIST(overview),2,50);
  gtk_clist_set_column_max_width(GTK_CLIST(overview),3,70);
  gtk_clist_set_column_width(GTK_CLIST(overview),0,140);
  gtk_clist_set_column_width(GTK_CLIST(overview),1,140);
  gtk_clist_set_column_width(GTK_CLIST(overview),2,40);
  gtk_clist_set_column_width(GTK_CLIST(overview),3,50);
  gtk_clist_set_column_width(GTK_CLIST(overview),4,40);
  gtk_container_add(GTK_CONTAINER(w),overview);

  frame = gtk_frame_new("Detailed results");
  gtk_box_pack_start(GTK_BOX(connection), frame, TRUE, TRUE, 1);
  w = gtk_scrolled_window_new(NULL, NULL);
  gtk_container_set_border_width(GTK_CONTAINER(w),FRAME_INNER_BORDER);
  gtk_container_add(GTK_CONTAINER(frame),w);
  detailed_result = gtk_clist_new_with_titles(5,detail_titles);
  gtk_signal_connect(GTK_OBJECT(detailed_result), "select_row", GTK_SIGNAL_FUNC(detail_row_selected), NULL);
  gtk_clist_set_column_min_width(GTK_CLIST(detailed_result),0,40);
  gtk_clist_set_column_min_width(GTK_CLIST(detailed_result),1,40);
  gtk_clist_set_column_min_width(GTK_CLIST(detailed_result),2,100);
  gtk_clist_set_column_min_width(GTK_CLIST(detailed_result),3,100);
  gtk_clist_set_column_min_width(GTK_CLIST(detailed_result),4,40);
  gtk_clist_set_column_max_width(GTK_CLIST(detailed_result),0,60);
  gtk_clist_set_column_max_width(GTK_CLIST(detailed_result),1,60);
  gtk_clist_set_column_max_width(GTK_CLIST(detailed_result),4,60);
  gtk_clist_set_column_width(GTK_CLIST(detailed_result),0,50);
  gtk_clist_set_column_width(GTK_CLIST(detailed_result),1,50);
  gtk_clist_set_column_width(GTK_CLIST(detailed_result),2,130);
  gtk_clist_set_column_width(GTK_CLIST(detailed_result),3,140);
  gtk_container_add(GTK_CONTAINER(w),detailed_result);
}

void make_opt_page(GtkWidget *options) {
  GtkWidget *tab, *w;
  tab = gtk_table_new(4,4, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE(tab),5);
  w = gtk_label_new("  Database to use:");
  gtk_misc_set_alignment (GTK_MISC(w),0.0,0.0);          
  gtk_table_attach_defaults(GTK_TABLE(tab),w,0,1,0,1);
  w = gtk_label_new("  Searcher to use:");
  gtk_misc_set_alignment (GTK_MISC(w),0.0,0.0);          
  gtk_table_attach_defaults(GTK_TABLE(tab),w,0,1,1,2);
  db_select = gtk_option_menu_new();
  gtk_table_attach_defaults(GTK_TABLE(tab),db_select,1,3,0,1);
  searcher_select = gnome_file_entry_new("search", "Search engine to use");
  gnome_file_entry_set_modal(GNOME_FILE_ENTRY(searcher_select),TRUE);
  gtk_table_attach_defaults(GTK_TABLE(tab),searcher_select,1,3,1,2);
  w = gtk_button_new_with_label("Manage");
  gtk_signal_connect(GTK_OBJECT(w),"clicked",manage_db_open, NULL);
  gtk_table_attach_defaults(GTK_TABLE(tab),w,3,4,0,1);
  gtk_table_attach_defaults(GTK_TABLE(tab),gtk_label_new(EMPTY_STRING),0,4,2,3);
  w = gtk_label_new("  Max results:");
  gtk_misc_set_alignment (GTK_MISC(w),0.0,0.0);                                            
  gtk_table_attach_defaults(GTK_TABLE(tab),w,0,1,3,4);
  num_result_entry = gtk_entry_new();
  gtk_entry_set_max_length(GTK_ENTRY(num_result_entry),2);   
  gtk_entry_set_text(GTK_ENTRY(num_result_entry),"10");
  gtk_table_attach_defaults(GTK_TABLE(tab),num_result_entry,1,2,3,4);
  gtk_box_pack_start(GTK_BOX(options), tab, FALSE, FALSE, 1);
}

void make_detail_page(GtkWidget *details) {
  GtkWidget *tab;
  tab = gtk_vbox_new(FALSE, 2);

  detail_label = gtk_label_new(LABEL_DETAILS);
  gtk_misc_set_alignment (GTK_MISC(detail_label),0.0,0.0);                                            
  gtk_box_pack_start(GTK_BOX(tab), detail_label, FALSE, FALSE, 1);
  text_details = gtk_text_new(NULL,NULL);
  gtk_text_set_editable(GTK_TEXT(text_details), FALSE);
  gtk_text_set_word_wrap(GTK_TEXT(text_details), FALSE);
  gtk_box_pack_start(GTK_BOX(tab), text_details, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(details), tab, TRUE, TRUE, 1);
}

static void show_about(GtkWidget *widget, gpointer data) {
 msg("   About\n","\n  gTimeTab - frontend for searching time tables.\n\n   Copyright (C) Roman Krejcik (roman.krejcik@ruk.cuni.cz), 2002\n   distribute under GPL v2\n   Time Table project - http://timetab.sourceforge.net" );
}

static void db_changed(GtkWidget *widget, gpointer data) {
  gchar *newdb = GTK_LABEL(GTK_BIN(db_select)->child)->label;
  if (strcmp(newdb, loaded_db) != 0) {
    strcpy(loaded_db, newdb);
    reload_db(loaded_db);
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(from)->entry),EMPTY_STRING);
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(to)->entry),EMPTY_STRING);
  }
}

int get_match_index(gchar *value) {
  int a = 0, b = config.towns - 1, half, cmp;
  for(;;) {
    half = (a+b)/2;
    cmp = strcasecmp(town_list[half], value);
    if (cmp == 0) return half;
    if (a+1 == b) return b;
    if (cmp < 0) {
      a = half;
    } else {
      b = half;
    }
  }
}

static void update_quick_selection(GtkWidget *widget, gpointer data) {
  gchar *value;
  int i, idx, len;		   

  if (db_loaded_flag) {
    value = gtk_entry_get_text(GTK_ENTRY(widget));
    if (*value == '\0') {
      for(i = 0; i < NUM_OF_QUICK_SEL; i++) {
	gtk_label_set_text(GTK_LABEL(quick_label[i]), EMPTY_STRING);
      }
      return;
    }
    idx = get_match_index(value);
    len = strlen(value);
    for( i = 0; i < NUM_OF_QUICK_SEL; i++) {
      if (idx+i == config.towns || strncasecmp(value, town_list[idx+i], len) != 0) break;
      gtk_label_set_text(GTK_LABEL(quick_label[i]), town_list[idx+i]);
    }
    while (i < NUM_OF_QUICK_SEL) {
      gtk_label_set_text(GTK_LABEL(quick_label[i]), EMPTY_STRING);
      i++;
    }
  }
  //  items = (qs_focus == GTK_COMBO(from)) ? &from_items : &to_items;
  /*  g_list_free(from_items);
      from_items = NULL; */
  //station_items = g_list_append(station_items, (gchar *) EMPTY_STRING);
  /*  for(i = 0; i < MAX_POPUP_ITEMS; i++) {
    if (idx+i == config.towns) break;
    from_items = g_list_append(from_items, town_list[idx+i]);
  }
  signal_lock = TRUE;
  gtk_combo_set_popdown_strings(qs_focus, from_items);
  gtk_entry_set_text(GTK_ENTRY(qs_focus->entry),value);
  signal_lock = FALSE;*/
}

static void use_quick_selection(GtkWidget *widget, gpointer data) {
  gchar *label;
  int i = 0;
  while (quick[i] != widget) i++;
  gtk_label_get(GTK_LABEL(quick_label[i]), &label);
  if (*label) {
    gtk_entry_set_text(GTK_ENTRY(qs_focus->entry), label);
  }
}

static void change_focus(GtkContainer *container, GtkWidget *widget, gpointer data) {
  qs_focus = GTK_COMBO(container);
  update_quick_selection(qs_focus->entry, NULL);
}

void rebuild_db_menu(void) {
  int i;
  GtkWidget *item;
  gtk_option_menu_remove_menu(GTK_OPTION_MENU(manage_om));
  db_menu_manage = gtk_menu_new();
  gtk_option_menu_remove_menu(GTK_OPTION_MENU(db_select));
  db_menu = gtk_menu_new();
  for(i = 0; i < db_counter; i++) {
    item = gtk_menu_item_new_with_label(db[i]);
    gtk_signal_connect(GTK_OBJECT(item),"activate",db_changed, NULL);
    gtk_menu_append(GTK_MENU(db_menu), item);
    gtk_menu_append(GTK_MENU(db_menu_manage), gtk_menu_item_new_with_label(db[i]));
  }
  gtk_option_menu_set_menu(GTK_OPTION_MENU(manage_om), db_menu_manage);
  gtk_widget_show_all(manage_om);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(db_select), db_menu);
  gtk_widget_show_all(db_select);
}

static void add_db(GtkWidget *widget, gpointer data) {
  gchar *en = gtk_entry_get_text(GTK_ENTRY(data));
  strncpy(db[db_counter],en,MAX_CONFIG_LINE);
  db_counter++;
  rebuild_db_menu();
}

static void remove_db(GtkWidget *widget, gpointer data) {
  gchar *label = GTK_LABEL(GTK_BIN(data)->child)->label;
  int idx = 0;
  while (strcmp(db[idx],label) != 0) idx++;
  db_counter--;
  if (idx != db_counter) {
    strcpy(db[idx],db[db_counter]);
  }
  rebuild_db_menu();
}

static void manage_db_close(GtkWidget *widget, gpointer data) {
  gtk_window_set_modal(GTK_WINDOW(win_manage_db), FALSE);
  gtk_entry_set_text(GTK_ENTRY(data),EMPTY_STRING);
  gtk_widget_hide_all(win_manage_db);
}

static void manage_db_open(GtkWidget *widget, gpointer data) {
  gtk_window_set_modal(GTK_WINDOW(win_manage_db), TRUE);
  gtk_widget_show_all(win_manage_db);
}

static void create_manage_db_window(void) {
  GtkWidget *tab, *w, *add_entry;
  win_manage_db = gtk_window_new(GTK_WINDOW_DIALOG);
  
  gtk_window_set_title(GTK_WINDOW(win_manage_db), "db list manager");
  gtk_widget_set_usize(win_manage_db, 400, 300);

  tab = gtk_table_new(2,4, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE(tab),5);
  w = gtk_label_new("Removing database:");
  gtk_table_attach_defaults(GTK_TABLE(tab),w,0,2,0,1);
  w = gtk_label_new("Adding database:");
  gtk_table_attach_defaults(GTK_TABLE(tab),w,0,2,2,3);
  manage_om = gtk_option_menu_new();
  gtk_widget_set_usize(manage_om,120,30);
  gtk_table_attach_defaults(GTK_TABLE(tab),manage_om,0,1,1,2);
  w = gtk_button_new_with_label("Remove");
  gtk_signal_connect(GTK_OBJECT(w),"clicked",remove_db, manage_om); 
  gtk_table_attach_defaults(GTK_TABLE(tab),w,1,2,1,2);
  add_entry = gtk_entry_new();
  gtk_table_attach_defaults(GTK_TABLE(tab),add_entry,0,1,3,4);
  w = gtk_button_new_with_label("Add");
  gtk_signal_connect(GTK_OBJECT(w),"clicked",add_db, add_entry);
  gtk_table_attach_defaults(GTK_TABLE(tab),w,1,2,3,4);

  w = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(win_manage_db), w);
  gtk_box_pack_start(GTK_BOX(w), tab, FALSE, FALSE, 0);
 
  tab = gtk_button_new_with_label("Close");
  gtk_signal_connect(GTK_OBJECT(tab), "clicked", manage_db_close, add_entry);
  gtk_box_pack_start(GTK_BOX(w), tab, FALSE, FALSE, 0);
}

int reload_db(char *path) {
  FILE *f;
  char buf[256], line[256], **endptr = NULL;    
  int success = 0, i;
  strncpy(buf, path, 240);
  strcat(buf, "/README");
  if ((f = fopen(buf, "r"))) {
    while (fgets(line, MAX_CONFIG_LINE, f)) {
      line[strlen(line)-1] = '\0';
      if (line[0] == '!') {
	if (!strncmp(line+1,"towns",5)) {
	  config.towns = strtol(line+7,endptr,10);
	  if (endptr) break;
	  success = 1;
	  db_loaded_flag = 1;
	}
      }
    }
    fclose(f);
  }
  if (! success) {
    db_loaded_flag = 0;
    msg("  Warning\n", "\n Database is not correct. No one loaded. Please select another.");
    return 1;
  }

  /*free old town_index ********************/
 
  strncpy(buf, path, 240);
  strcat(buf, "/towns.txt");
  if (!(f = fopen(buf, "r"))) return 1;
  town_list = (char **) xmalloc(sizeof(char *) * config.towns);
  i = 0;
  for(i = 0; i < config.towns; i++) { 
    fgets(line, MAX_CONFIG_LINE, f);
    success = strlen(line);
    line[success-1] = '\0';
    town_list[i] = (char *) xmalloc(success);
    strcpy(town_list[i], line);  
  }
  qsort(town_list, config.towns, sizeof(town_list[0]), cmp_fce);

  //  station_items = g_list_append(station_items, (gchar *) EMPTY_STRING);

  /******************
  for(i = 0; i < config.towns; i++) {
    station_items = g_list_append(station_items, town_list[i]);
  }
  ***************/
  /*gtk_combo_set_popdown_strings(GTK_COMBO(from), station_items);
    gtk_combo_set_popdown_strings(GTK_COMBO(to), station_items);*/

  return 0;
}

int cmp_fce(const void *a, const void *b) {
  char **aa = (char **) a , **bb = (char **) b;
  return strcasecmp(*aa,*bb);
}

void read_rc_file(void) {
  char line[256];
  FILE *rc;
  strncpy(line,getenv("HOME"), MAX_CONFIG_LINE);
  strcat(line, "/.gtt.rc");
  rc = fopen(line,"r");
  if (rc) {
    fgets(line, MAX_CONFIG_LINE, rc);
    line[strlen(line)-1] = '\0';
    if (strcmp(line,"(null)") == 0) line[0]= '\0';
    gtk_entry_set_text(GTK_ENTRY(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(searcher_select))),line);
    while (fgets(line, MAX_CONFIG_LINE, rc)) {
      line[strlen(line)-1] = '\0';
      strcpy(db[db_counter++], line);
    }
  }
}

void quit_gtt(void) {
  FILE *rc;
  int i;
  char line[256];
  strncpy(line,getenv("HOME"), MAX_CONFIG_LINE);
  strcat(line, "/.gtt.rc");
  rc = fopen(line,"w");
  if (rc) {
    fprintf(rc,"%s\n",gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(searcher_select), FALSE));
    for(i = 0; i < db_counter; i++) {
      fprintf(rc,"%s\n",db[i]);
    }
    fclose(rc);
  } else {
    g_print("Error: Cannot write ~/.gtt.rc\n");
  }
  gtk_main_quit();
}

static void search(GtkWidget *widget, gpointer data) {
  FILE *pipe;
  char command[MAX_COMMAND_LEN], read_buffer[MAX_CONFIG_LINE];
  time_t dt = gnome_date_edit_get_date(GNOME_DATE_EDIT(date_edit));
  struct tm *ltime;
  char *lastarr;
  int line_counter, sp, readed, i, train_counter, new_train_flag, distance;
  struct TRAIN_LIST *last;
  struct RESULT_TRAIN *ptrain;
  struct RESULT_LIST *lastres;
  struct REMARK_LIST *lastremark;

  gchar line[MAX_CONFIG_LINE];
  gchar overview_clist_line[5][MAX_CONFIG_LINE], *searcher;
  gchar *pline[] = {overview_clist_line[0],overview_clist_line[1],overview_clist_line[2],overview_clist_line[3],overview_clist_line[4]};
  ltime = localtime(&dt);

  if ((searcher = gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(searcher_select), TRUE)) == NULL) {
    msg("  Error\n","\n No searcher selected or invalid path. Update searcher in Option page.");
    if (! db_loaded_flag)   gtk_text_insert(GTK_TEXT(text_details), NULL, NULL, NULL, "\n\n No valid database selected.", -1);
    return;
  }

  if (! db_loaded_flag) {
    msg("  Error\n","\n No valid database selected. Please choose one in Option page.");
    return; 
  }

  clear_all();

  /* Free last cached search hresult */ 

  for(i = 0; i < MAX_RESULT_TRAINS; i++) {
    struct RESULT_LIST *p;
    lastres = result[i];
    while (lastres) {
      struct TRAIN_LIST *t;
      struct REMARK_LIST *r;
      last = lastres->train;
      while (last) {
        t = last->next;;
	free(last);
	last = t;
      }
      lastremark = lastres -> remark;
      while (lastremark) {
	r = lastremark->next;;
	free(lastremark);
	lastremark = r;
      }
      p = lastres;
      lastres = lastres->next;
      free(p);
    }
    result[i] = NULL;
  }
  for(i = 0; i < MAX_RESULT_TRAINS; i++) {
    struct RESULT_TRAIN *p;
    ptrain = trains[i];
    while (ptrain) {
      p = ptrain;
      ptrain = ptrain->next;
      free(p);
    }
    trains[i] = NULL;
  }
  detailed_rows = 0;
  overview_rows = 0;

  snprintf(command, MAX_COMMAND_LEN, "%s -n %d -p %s \"%s\" %d.%d.%d %d:%d \"%s\" | ./gtt-parser.pl",
	   searcher, get_result_num(), GTK_LABEL(GTK_BIN(db_select)->child)->label, 
	   gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(from)->entry)),
	   ltime->tm_mday, ltime->tm_mon+1, ltime->tm_year+1970, ltime->tm_hour, ltime->tm_min, 
	   gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(to)->entry)));
  pipe = popen(command,"r");
  line_counter = 0;
  sp = 0;    

  /* Parsing result */

  while((readed = fread(read_buffer, 1, MAX_CONFIG_LINE, pipe)) > 0) {
    for (i = 0; i < readed; i++) {
      if (read_buffer[i] == '\n') {    
	line[sp] = '\0';
	switch (line[0]) {
	case '>': // new connection
	  if (overview_rows && line_counter == 0) {
	    sprintf(overview_clist_line[2],"%d",train_counter);
	    gtk_clist_append(GTK_CLIST(overview),pline);
	  }
	  strcpy(overview_clist_line[line_counter],line+1);
	  switch (line_counter) {
	  case 0: line_counter++; break;
	  case 1: line_counter = 4; break;
	  case 4: line_counter = 3; break;
	  case 3: 
	    overview_rows++;
	    line_counter = 0;
	    train_counter = 0;
	    if (ptrain) {
	      ptrain -> next = (struct RESULT_TRAIN *) xmalloc(sizeof(struct RESULT_TRAIN));
	      ptrain = ptrain-> next;
	      ptrain -> arr  = lastarr;
	      ptrain -> dep = (char *) EMPTY_STRING;
	      strcpy(ptrain -> name,(char *) EMPTY_STRING);
	      ptrain->station = last->station;
	      distance += atoi(last->dist);
	      ptrain->dist = distance;
	      ptrain-> next = NULL;
	    }
	    last = NULL;
	    ptrain = NULL;
	    lastres = NULL;
	  }
	  break;
	case '#': // train name
	  train_counter++;
	  new_train_flag = 1;
	  if (ptrain) {
	    ptrain -> next = (struct RESULT_TRAIN *) xmalloc(sizeof(struct RESULT_TRAIN));
	    ptrain = ptrain->next;
	    ptrain -> arr  = lastarr;
	    distance += atoi(last->dist);
	  } else {
	    trains[overview_rows-1] = (struct RESULT_TRAIN *) xmalloc(sizeof(struct RESULT_TRAIN));
	    ptrain = trains[overview_rows-1];
	    ptrain -> arr = (char *) EMPTY_STRING;
	    distance = 0;
	  }
	  ptrain->dist = distance;
	  ptrain->next = NULL;
	  strncpy(ptrain->name, line+1, MAX_CONFIG_LINE-1);
	  if (lastres) {
	    lastres -> next = (struct RESULT_LIST *) xmalloc(sizeof(struct RESULT_LIST));
	    lastres = lastres->next;
	  } else {
	    result[overview_rows-1] = (struct RESULT_LIST *) xmalloc(sizeof(struct RESULT_LIST));
	    lastres = result[overview_rows-1];
	  }
	  lastres -> name = ptrain -> name;
	  lastres -> next = NULL;
	  lastres -> remark = NULL;
	  last = NULL;
	  lastremark = NULL;
	  break;
	case '+':
	  switch (line_counter) {
	  case 0: 
	    if (last) {
	      last -> next = (struct TRAIN_LIST *) xmalloc(sizeof(struct TRAIN_LIST));
	      last = last->next;
	    } else {
	      lastres -> train = (struct TRAIN_LIST *) xmalloc(sizeof(struct TRAIN_LIST));
	      last = lastres->train;
	    }
	    last->next = NULL;
	    strncpy(last->arr, line+1, 6);
	    lastarr = last->arr;
	    line_counter++;
	    break;
	  case 1: 
	    strncpy(last->dep, line+1, 6);
	    if (new_train_flag) {
	      ptrain->dep = last->dep;
	    }
	    line_counter++;
	    break;
	  case 2:
	    strncpy(last->flag, line+1, 6);
	    line_counter++;
	    break;
	  case 3:
	    strncpy(last->dist, line+1, 6);
	    line_counter++;
	    break;
	  case 4:
	    strncpy(last->station, line+1, MAX_CONFIG_LINE-1);
	    line_counter = 0;
	    if (new_train_flag) {
	      ptrain->station = last->station;
	      new_train_flag = 0;
	    }
	    break;
	  }
	  break;
	default : //remark
	  if (lastremark) {
	    lastremark -> next = (struct REMARK_LIST *) xmalloc(sizeof(struct REMARK_LIST));
	    lastremark = lastremark->next;
	  } else {
	    lastres->remark = (struct REMARK_LIST *) xmalloc(sizeof(struct REMARK_LIST));
	    lastremark = lastres->remark;
	  }
	  lastremark->r[0] = '\n';
	  strncpy(lastremark->r+1, line, MAX_CONFIG_LINE-2);
	  lastremark->next = NULL;
	  break;
	} 
	sp = 0;
      } else {
	line[sp++] = read_buffer[i];
      }
    }
  }

  if (overview_rows) {
    if (ptrain) {
      ptrain -> next = (struct RESULT_TRAIN *) xmalloc(sizeof(struct RESULT_TRAIN));
      ptrain = ptrain-> next;
      ptrain -> arr  = lastarr;
      ptrain -> dep = (char *) EMPTY_STRING;
      strcpy(ptrain -> name,(char *) EMPTY_STRING);
      ptrain->station = last->station;
      distance += atoi(last->dist);
      ptrain->dist = distance;
      ptrain-> next = NULL;
    }
    sprintf(overview_clist_line[2],"%d",train_counter);
    gtk_clist_append(GTK_CLIST(overview),pline);
  }
  pclose(pipe);
}

void overview_row_selected(GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data) {
  gchar *pline[5];
  gchar dist[5];
  struct RESULT_TRAIN *t = trains[row];
  int i;

  for(i = 0; i < detailed_rows; i++) {
    gtk_clist_remove(GTK_CLIST(detailed_result), 0);
  }
  detailed_rows = 0;

  while (t) {
    pline[0] = t->arr;
    pline[1] = t->dep;
    pline[2] = t->station; 
    pline[3] = t->name;
    sprintf(dist,"%d",t->dist);
    pline[4] = dist;
    gtk_clist_append(GTK_CLIST(detailed_result),pline);        
    detailed_rows++;
    t = t->next;
  }

  train_selected = row;
}

void detail_row_selected(GtkCList *clist, gint row, gint col, GdkEventButton *event, gpointer user_data) {
  struct RESULT_LIST *r = result[train_selected];
  struct TRAIN_LIST *l;
  struct REMARK_LIST *remark;
  char buf[MAX_CONFIG_LINE];
  int i;

  gtk_label_set_text(GTK_LABEL(detail_label),LABEL_DETAILS);
  gtk_text_backward_delete(GTK_TEXT(text_details), gtk_text_get_length(GTK_TEXT(text_details)));

  for(i = 0; i < row; i++) { r = r->next; }
  if (r) {
    gtk_text_freeze(GTK_TEXT(text_details));
    gtk_text_insert(GTK_TEXT(text_details), border, NULL, NULL, "\n", -1);
    l = r->train;
    snprintf(buf, MAX_CONFIG_LINE-1,"  %s\n",r->name);
    gtk_text_insert(GTK_TEXT(text_details), headfont, NULL, NULL, buf, -1);
    gtk_text_insert(GTK_TEXT(text_details), textfont, NULL, NULL, "\n", -1);
    
    while (l) {
      snprintf(buf, MAX_CONFIG_LINE-1,"  %5s  %5s  %4s  %4s  %s\n",l->arr, l->dep, l->flag, l->dist, l->station);
      gtk_text_insert(GTK_TEXT(text_details), textfont, NULL, NULL, buf, -1);
      l = l->next;
    }

    remark = r->remark;
    while (remark) {
      gtk_text_insert(GTK_TEXT(text_details), remarkfont, NULL, NULL, remark->r, -1);
      remark = remark -> next;
    }
    gtk_text_thaw(GTK_TEXT(text_details));
  }
}

static void note_accel_used1(GtkWidget *widget, gpointer data) {
  gtk_notebook_set_page(GTK_NOTEBOOK(note),  0);
}

static void note_accel_used2(GtkWidget *widget, gpointer data) {
  gtk_notebook_set_page(GTK_NOTEBOOK(note),  1);
}

static void note_accel_used3(GtkWidget *widget, gpointer data) {
  gtk_notebook_set_page(GTK_NOTEBOOK(note),  2);
}

static void show_help(GtkWidget *widget, gpointer data) {
  static gchar hlptxt[] = "\n  Conncetion page:\n    You can use Alt+number for quick selection of town.\n    Click on overview line shows detailed results for that connection.\n    Click on line in detailed result list shows details for that train in Detail page.\n     You can use shorcut alt+d to change page to details.\n  Option page:\n    In db list directories with dabases in tb format (each db is directory).\n    For adding and removint it use manage button\n    Searcher is tbsearch or some compatibile searcher.\n    Max result affects results in overview.\n  Details page:\n    Page for train details or program messages and warnings.\n\n  You can use shortcuss alt+c, alt+o and alt+d to switch pages.";
  msg("   gTimeTab help\n",hlptxt);
}

void msg(gchar *head, gchar *body) {
  gtk_label_set_text(GTK_LABEL(detail_label),LABEL_MESSAGE);
  gtk_notebook_set_page(GTK_NOTEBOOK(note),  2);
  gtk_text_freeze(GTK_TEXT(text_details));
  gtk_text_backward_delete(GTK_TEXT(text_details), gtk_text_get_length(GTK_TEXT(text_details)));
  gtk_text_insert(GTK_TEXT(text_details), border, NULL, NULL, "\n", -1);
  gtk_text_insert(GTK_TEXT(text_details), headfont, NULL, NULL, head, -1);
  gtk_text_insert(GTK_TEXT(text_details), NULL, NULL, NULL, body, -1);
  gtk_text_thaw(GTK_TEXT(text_details));
}

int get_result_num(void) {
  int n = atoi(gtk_entry_get_text(GTK_ENTRY(num_result_entry)));
  if (n < 1 || n > 99) {
    n = 10;
    gtk_entry_set_text(GTK_ENTRY(num_result_entry),"10");
  }
  return n;
}

static void clear_all_used(GtkWidget *widget, gpointer data) {
  clear_all();
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(from)->entry),EMPTY_STRING);
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(to)->entry),EMPTY_STRING);
}

static void clear_all(void) {
  int i;
   gtk_clist_freeze(GTK_CLIST(detailed_result));
  for(i = 0; i < detailed_rows; i++) {
    gtk_clist_remove(GTK_CLIST(detailed_result), 0);
  } 
  gtk_clist_thaw(GTK_CLIST(detailed_result));
  gtk_clist_freeze(GTK_CLIST(overview));
  for(i = 0; i < overview_rows; i++) {
    gtk_clist_remove(GTK_CLIST(overview), 0);
  }
  gtk_clist_thaw(GTK_CLIST(overview));
  gtk_text_backward_delete(GTK_TEXT(text_details), gtk_text_get_length(GTK_TEXT(text_details)));  
  gtk_label_set_text(GTK_LABEL(detail_label),LABEL_DETAILS);
}
