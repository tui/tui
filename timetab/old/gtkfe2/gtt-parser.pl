#!/usr/bin/perl -w

# parser for gTimeTab utility
# Copyright (C) Roman Krejcik, 2002 
# distribute under GPL v2 

$remark_afterward = 0;

while (<>) {
    chomp;
    /^\s*$/ and next;
    if (/Connection from: (.*)/) { print ">$1\n"; next; }
    if (/\s*to: (.*)/) { print ">$1\n"; next; }
    if (/(\d+) kilometres,\s+travel time\s+(\d+):(\d+)/) { print ">$1\n>$2:$3\n"; next; }
    if (/\# (.*)/) { print "#$1\n"; $remark_afterward = 0; next; }
    if (/([\d:]*)\t\s*([\d:]*)\t(\S*)\s+(\d+)\s+(.*)/) { print "+$1\n+$2\n+$3\n+$4\n+$5\n"; $remark_afterward = 1; next; }
    if (/^\=+/) { $remark_afterward = 0; next; }
    $remark_afterward and print "$_\n";
}
