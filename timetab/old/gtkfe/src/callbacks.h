#include <gnome.h>


void
on_new_file1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_exit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_cut1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_copy1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_paste1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_properties1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_app1_delete_event                   (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_search_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_from_entry_activate                 (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_from_entry_changed                  (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_to_entry_changed                    (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data);

void
on_table4_check_resize                 (GtkContainer    *container,
                                        gpointer         user_data);

void
on_vbox11_check_resize                 (GtkContainer    *container,
                                        gpointer         user_data);

void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data);

void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data);

void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data);

void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data);

void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data);

void
on_from_entry_move_cursor              (GtkEditable     *editable,
                                        gint             x,
                                        gint             y,
                                        gpointer         user_data);

void
on_to_entry_move_cursor                (GtkEditable     *editable,
                                        gint             x,
                                        gint             y,
                                        gpointer         user_data);

void
on_from_entry_activate                 (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_to_entry_activate                   (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_searchall_clicked                   (GtkButton       *button,
                                        gpointer         user_data);
