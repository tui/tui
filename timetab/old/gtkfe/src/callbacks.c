#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include <time.h>


#define TOWNMAX         30000
#define HLEDAC          "timetab"
#define travel_file     't'

int lastchanged=0;

struct townvec {
        int numtowns;
        char *towns[TOWNMAX];
};

struct townvec *townvec;
 
int mujsort(const char **s1,const char **s2)
{
        return strcmp(*s1,*s2);
}

void readit(void)
{
        char buf[1024];
        FILE *f;
        char command[1024];
        sprintf(command,HLEDAC " -l%c",travel_file);
        f=popen(command,"r");

        townvec = malloc(sizeof(struct townvec));
        townvec->numtowns=0;
        for(;;) {
                if (fgets(buf, 1023, f)==NULL) break;
                if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
                while (buf[strlen(buf)-1]==' ') buf[strlen(buf)-1]=0;
                townvec->towns[townvec->numtowns]=malloc(strlen(buf)+1);
                strcpy(townvec->towns[townvec->numtowns],buf);
                townvec->numtowns++;
                if (townvec->numtowns> TOWNMAX) {fprintf(stderr,"Town limit exce
eded\n");gtk_exit(10);}
        }
        pclose(f);
        qsort(townvec->towns,townvec->numtowns,sizeof(char *),(void *)mujsort);
}

void
quickselect(GtkWidget *widget, int show)
{
	GtkWidget *quickselect = lookup_widget(GTK_WIDGET (widget),"quickselect");
	if (show)
		gtk_widget_show(quickselect);
	else
		gtk_widget_hide(quickselect);
}

void
schedule(void)
{
	while(g_main_pending())
		g_main_iteration(FALSE);
}

void
message(GtkWidget *widget, char *message)
{
	GtkWidget *status = lookup_widget(GTK_WIDGET (widget),"status");
	gtk_label_set_text(GTK_LABEL(status), message);
	schedule();
}

int vyhledej(char *s, GtkWidget *widget)
{
        int prvni=0,posledni=townvec->numtowns-1;
        int i;
	char label_name[15];
	GtkWidget *opt_label;

	quickselect(widget, 1);
        while (prvni+1!=posledni) {
                if (strncmp(s,townvec->towns[(prvni+posledni)/2],strlen(s)) >0)
                        prvni=(prvni+posledni)/2;
                else posledni=(prvni+posledni)/2;
        }
        if (strncmp(s,townvec->towns[(prvni+posledni)/2],strlen(s)) >0)
                prvni=(prvni+posledni)/2;
        else posledni=(prvni+posledni)/2;
        //je potreba to provyst jeste jednou, kdyby napr. s==towns[0]

        for (i=0;i<12;i++) {
		sprintf(label_name,"optl%d",i+1);
                opt_label=lookup_widget(GTK_WIDGET(widget),label_name);
                if (posledni+i<townvec->numtowns && 
		    !strncmp(s,townvec->towns[posledni+i],strlen(s)) ) {
			gtk_label_set_text(GTK_LABEL(opt_label),
					   townvec->towns[posledni+i]);
			gtk_label_set_pattern(GTK_LABEL(opt_label),"");
		}
		else gtk_label_set_text(GTK_LABEL(opt_label),"");
        }
        if (!strncmp(s,townvec->towns[posledni],strlen(s)) )
                return posledni;
                else return -1;
}



void
on_new_file1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save_as1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_exit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	gtk_exit(0);
}


void
on_cut1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_copy1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_paste1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_properties1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	gtk_widget_show(create_aboutdialog());
}


gboolean
on_app1_delete_event                   (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
	gtk_exit(0);
	return FALSE;
}

void
fill_tree_with_output(GtkWidget *widget, char *dest_widget, char *command)
{
	GtkWidget *output_ctree;
	FILE *f;
	gchar *s[6];
	gchar ss[1024];
	int i;
	GtkCTreeNode *parent = NULL, *node = NULL;
	gboolean isbranch=TRUE, output=FALSE;
	gchar *lastnode[5];

	for (i=0;i<5;i++) lastnode[i]=g_malloc(1024); 
	strcpy(lastnode[0],"");
	output_ctree=lookup_widget(GTK_WIDGET (widget), dest_widget);
	
	gtk_clist_clear(GTK_CLIST(output_ctree));
	f = popen(command,"r");
	while(1) {
		schedule();
		if (fgets(ss,1023,f)==NULL) break;
		if (ss[strlen(ss)-1]=='\n') ss[strlen(ss)-1]=0;
		while (ss[strlen(ss)-1]==' ') ss[strlen(ss)-1]=0;
		if (strchr(ss,'\t')) {
			s[0]=ss;
			s[1]=strchr(s[0],'\t')+1;
			s[2]=strchr(s[1],'\t')+1;
			s[3]=strchr(s[2],'\t')+1;
			s[4]=strchr(s[3],'\t')+1;
		      	while (strrchr(ss,'\t')) *(char *)strrchr(ss,'\t')=0;
			for (i=0;i<5;i++) 
				while (s[i][strlen(s[i])-1]==' ') 
					s[i][strlen(s[i])-1]=0;
			if (!output) isbranch=TRUE;
			else isbranch=FALSE;
			output=TRUE;
		} else {
			s[3]=ss;
			s[0]=s[1]=s[2]=s[4]=ss+strlen(ss);
			isbranch=TRUE;
			output=FALSE;
		}
		if (output) {
			if (isbranch) {
				gtk_ctree_remove_node(GTK_CTREE(output_ctree),node);
				/* remove last node (will be repeated - train change) */
				s[0]=lastnode[0];
				parent=gtk_ctree_insert_node(GTK_CTREE(output_ctree),
				  NULL, NULL,s,0,NULL,NULL,NULL,NULL,FALSE,FALSE);
			}
			else {
				node=gtk_ctree_insert_node(GTK_CTREE(output_ctree),
				      parent, NULL,s,0,NULL,NULL,NULL,NULL,TRUE,FALSE);
				for(i=0;i<5;i++) strcpy(lastnode[i],s[i]);
			}
		}
/*		gtk_clist_append(GTK_CLIST (output_ctree),s); */
        }
        pclose(f);
	gtk_ctree_remove_node(GTK_CTREE(output_ctree),node);
	gtk_ctree_insert_node(GTK_CTREE(output_ctree),
	       NULL, NULL,lastnode,0,NULL,NULL,NULL,NULL,TRUE,FALSE);
/*	gtk_ctree_set_line_style(GTK_CTREE(output_ctree),GTK_CTREE_LINES_NONE); */
	gtk_ctree_set_indent(GTK_CTREE(output_ctree),5);
}

void
fill_list_with_output(GtkWidget *widget, char *dest_widget, char *command)
{
	GtkWidget *output_clist;
	FILE *f;
	gchar *s[6];
	gchar ss[1024];
	int i;
	gchar *lastnode[5];

	for (i=0;i<5;i++) lastnode[i]=g_malloc(1024); 
	strcpy(lastnode[0],"");
	output_clist=lookup_widget(GTK_WIDGET (widget), dest_widget);
	
	gtk_clist_clear(GTK_CLIST(output_clist));
	f = popen(command,"r");
	while(1) {
		schedule();
		if (fgets(ss,1023,f)==NULL) break;
		if (ss[strlen(ss)-1]=='\n') ss[strlen(ss)-1]=0;
		while (ss[strlen(ss)-1]==' ') ss[strlen(ss)-1]=0;
		if (strchr(ss,'\t')) {
			s[0]=ss;
			s[1]=strchr(s[0],'\t')+1;
			s[2]=strchr(s[1],'\t')+1;
			s[3]=strchr(s[2],'\t')+1;
			s[4]=strchr(s[3],'\t')+1;
		      	while (strrchr(ss,'\t')) *(char *)strrchr(ss,'\t')=0;
			for (i=0;i<5;i++) 
				while (s[i][strlen(s[i])-1]==' ') 
					s[i][strlen(s[i])-1]=0;
		} else {
			s[3]=ss;
			s[0]=s[1]=s[2]=s[4]=ss+strlen(ss);
		}
		gtk_clist_append(GTK_CLIST (output_clist),s);
        }
        pclose(f);
}

void
do_search(GtkButton *button, int mode)
{
	gchar *from_town, *to_town;
	GtkWidget *from_entry, *to_entry;
	GtkWidget *dateedit1;
	char command[1024];
	struct tm *tm;
	time_t time;

	printf("Quickselect\n");
	message(GTK_WIDGET(button), "Searching...");
	quickselect(GTK_WIDGET(button), 0);

	printf("Entries\n");
	from_entry=lookup_widget(GTK_WIDGET (button), "from_entry");
	to_entry=lookup_widget(GTK_WIDGET (button), "to_entry");    
	dateedit1=lookup_widget(GTK_WIDGET (button), "dateedit1");
	from_town=gtk_entry_get_text(GTK_ENTRY(from_entry));
	to_town=gtk_entry_get_text(GTK_ENTRY(to_entry));
	time=gnome_date_edit_get_date(GNOME_DATE_EDIT (dateedit1));
	tm = localtime(&time);

	switch (mode) {
	case 0:
		printf("Run...\n");
		sprintf(command,"./timetab -ftd48 '%s' %d.%d.%d %d:%d '%s'",
			from_town, tm->tm_mday, tm->tm_mon+1, tm->tm_year+1900,
			tm->tm_hour, tm->tm_min, to_town);
		fill_tree_with_output(GTK_WIDGET(button), "output_ctree", command);
		break;
	case 1:
		sprintf(command,"../search/overview '%s' %d.%d.%d %d:%d '%s' | tee /tmp/delme",
			from_town, tm->tm_mday, tm->tm_mon+1, tm->tm_year+1900,
			tm->tm_hour, tm->tm_min, to_town);
		fill_list_with_output(GTK_WIDGET(button), "overview_clist", command);
		break;
	}
	message(GTK_WIDGET(button), "Ok.");
}



void
on_search_button_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
	do_search(button, 0);
}


void
on_from_entry_changed                  (GtkEditable     *editable,
                                        gpointer         user_data)
{
	gchar *s;
	GtkWidget *entry;
	
	entry=lookup_widget(GTK_WIDGET(editable),"from_entry");
	s=gtk_entry_get_text(GTK_ENTRY(entry));
	vyhledej(s,entry);
	lastchanged=0;

}


void
on_to_entry_changed                    (GtkEditable     *editable,
                                        gpointer         user_data)
{
	gchar *s;
	GtkWidget *entry;
	
	entry=lookup_widget(GTK_WIDGET(editable),"to_entry");
	s=gtk_entry_get_text(GTK_ENTRY(entry));
	vyhledej(s,entry);
	lastchanged=1;

}


void
on_opt_clicked                         (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *entry;
	GtkWidget *label;
	gchar *text;

	if (lastchanged) entry=lookup_widget(GTK_WIDGET(button),"to_entry");
	else entry=lookup_widget(GTK_WIDGET(button),"from_entry");
	label=lookup_widget(GTK_WIDGET(button),user_data);
	gtk_label_get(GTK_LABEL(label),&text);
	gtk_entry_set_text(GTK_ENTRY(entry),text);

}


void
on_table4_check_resize                 (GtkContainer    *container,
                                        gpointer         user_data)
{
	g_print("table4_resize\n");
}


void
on_vbox11_check_resize                 (GtkContainer    *container,
                                        gpointer         user_data)
{
	g_print("vbox12_resize\n");
}

void
on_from_entry_move_cursor              (GtkEditable     *editable,
                                        gint             x,
                                        gint             y,
                                        gpointer         user_data)
{
	g_print("from_entry_move_cursor\n");
}


void
on_to_entry_move_cursor                (GtkEditable     *editable,
                                        gint             x,
                                        gint             y,
                                        gpointer         user_data)
{
	g_print("to_entry_move_cursor\n");
}


void
on_from_entry_activate                 (GtkEditable     *editable,
                                        gpointer         user_data)
{
	g_print("from_entry_activate\n");
}


void
on_to_entry_activate                   (GtkEditable     *editable,
                                        gpointer         user_data)
{
	g_print("to_entry_activate\n");
}

void
on_searchall_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
	do_search(button, 1);
}

