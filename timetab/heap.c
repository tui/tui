
/*
 * Heap utility functions
 */

static int heapcount = 0, heapalloc = 0;

static void swapheap(int a, int b);

void
fixup(int heapindex)
{
	int lefttime = INF, righttime = INF, better = 0, mytime = INF;

//	printf("Fixup %d, heapcount %d\n", heapindex, heapcount);
	if (heapindex > heapcount)
		die("...thats fatal\n");
	mytime = TIME(heapindex);
	BACKLINK(heapindex);

	if (heap[heapindex*2])
		lefttime = TIME(heapindex*2);
	if (heap[heapindex*2+1])
		righttime = TIME(heapindex*2+1);
	
	if ((mytime < lefttime) && (mytime < righttime))
		return;

	better = heapindex*2+(lefttime > righttime);
	swapheap(heapindex, better);
	fixup(better);
}

void
fixdown(int heapindex)
{
//	printf("Fixdown %d, heapcount %d\n", heapindex, heapcount);

	BACKLINK(heapindex);
	if (!(heapindex/2))
		return;
	if (TIME(heapindex/2) > TIME(heapindex)) {
		swapheap(heapindex/2, heapindex);
		fixdown(heapindex/2);
	}
}

void
deleteheap(int heapindex)
{
	if (!heap[heapindex])
		die("Nothing to delete?\n");
	if (!heap[heapcount])
		die("Null at heapcount!\n");
	heap[heapindex] = heap[heapcount];
	heap[heapcount--] = NULL;
	if (heapcount+1 != heapindex) {
		fixup(heapindex);
		fixdown(heapindex);
	}
}
