#!/usr/bin/python
# Copyright 2008 Petr Machek, GPL
# Copyright 2008 Pavel Machek

import sys

way_id = 1000000000
last_time = 0
line = sys.stdin.readline()
dict = {}
used = {}
edges = {}
while(line[0] != '#'):
	new, old = line.split('\t')
	if not old in dict:
		dict[old] = []	
		used[old] = 0
	dict[old].append(new)
	line = sys.stdin.readline()

def min2string(minutes):
	return "%i:%02i" % (minutes/60, minutes%60)

def one_time(chunk):
	global last_time
	if chunk == '':
		return ''
	elif ':' in chunk:
		time = chunk.split(':')
		last_time = 60*int(time[0]) + int(time[1])
	elif chunk[0] == '+':
		last_time = last_time + int(chunk[0:])
	return last_time

def print_edge(s1, s2, pl1, pl2, t1, t2, conn_name):
	date =  "(" + remark[:-1] + ") "
	new = min2string(t1)
	name = conn_name.split("_")[0] + " ("+ s1[:-1] + " -> " + s2[:-1] + ")"
	time = t2-t1
	if (time < 0):
		# FIXME: 0:00 is represented as 24:00; I'm not sure that's
		# fully correct. Plus handle_one_opening may have problems
		# over midnight.
		time += 24*60;

	if ( pl1, pl2, name, time ) in edges:
		( f1, f2, name, time, times ) = edges[ ( pl1, pl2, name, time ) ]
	else:
		times = {}

	if ( date ) in times:
		( i1, oldtext ) = times[ ( date ) ]
		new = oldtext + " " + new

	times[ ( date ) ] = ( date, new )
	edges[ ( pl1, pl2, name, time ) ] = ( pl1, pl2, name, time, times )

while(line != ''):
	if line[0] in 'D':
		remark = line[1:]
	if line[0] in '\t+1234567890':
		chunks = line.split('\t')
		name = chunks[-1]
		chunks[0] = one_time(chunks[0])
		chunks[1] = one_time(chunks[1])
		if chunks[0] == '':
			chunks[0] = chunks[1]
		if name in dict:
			chunks[-1] = dict[name]
			used[name] = 1
		# With breaks, it only creates arbitrary one link.
		# Sometimes it may be desirable to create all of them...
		if (old_chunks != None):
			for pl1 in old_chunks[-1]:
				for pl2 in chunks[-1]:
					print_edge(old_name, name, pl1, pl2, old_chunks[1], chunks[0], conn_name)
					break
				break
	
		old_name = name
		old_chunks = chunks
	else:
		if line[0] in '#':
			conn_name = line[1:-1]
			remark = ""
		old_chunks = None
	line = sys.stdin.readline()

way_id = 0
for edge in edges:
	( pl1, pl2, name, ttime, times ) = edges[edge]
	print "<way id='" + str(way_id) + "'>"
	print "  <nd ref='"+pl1+"' />"
	print "  <nd ref='"+pl2+"' />"
	print "  <tag k='highway' v='path' />"
	print "  <tag k='travel_time' v='" + str(ttime*60) + "' />"
	print "  <tag k='name' v='" + name + "' />"
	text = 0
	for date in times:
		( date, time ) = times[date]
		if text:
			text = text + "; " + date + time
		else:	text = date + time
	print "  <tag k='opening_hours' v='" + text + "' />"
	print "  <tag k='oneway' v='true' />"
	print "</way>"
	way_id = way_id + 1
		

for name in used:
	if not used[name]:
		print "NOTE: " + name[:-1] + " is not used in .tt file"
	
print "NOTE: %d ways processed" % way_id
