#!/usr/bin/env python
# Copyright 2009 Petr Machek, GPL

import sys, re

try:
  infile = open("input.osm", "r")
except:
  print "Wrong path for input file"
  sys.exit(1)
stopslist = open("./stops.list", "w")
stopshash = open("./stops.hash", "w")

type_match = re.compile(r"k='(.*)' v='(.*)'")
id_match = re.compile(r"id='(.*)' lat")

line = infile.readline()
while(line):
  if "<node" in line:
    ID = id_match.search(line).group(1)
    conn_type="unknown"
    name="unknown"
  elif "<tag" in line:
    type_info = type_match.search(line)
    if type_info != None:
      k = type_info.group(1)
      v = type_info.group(2)
      if k=="highway" and v=="bus_stop":
        conn_type = "bus"
      elif k=="railway" and v=="station":
        conn_type = "rail"
      elif k=="railway" and v=="tram_stop":
        conn_type = "tram"
      elif k=="name":
        name = v
  elif "</node>" in line:
    if conn_type != "unknown" and name != "unknown":
      output_line = "type %s id %s name %s\n" % (conn_type, ID, name)
      print output_line
      stopslist.write(name + "\n")
      stopshash.write("%s\t%s\n" % (ID, name))
  line = infile.readline()
stopslist.close()
stopshash.close()
