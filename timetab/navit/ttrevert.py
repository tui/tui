#!/usr/bin/python
# Move D remarks to the beggining of connection
#
# Copyright 2008 Petr Machek, GPL
# Copyright 2008 Pavel Machek

import sys

line = sys.stdin.readline()

while(line != ''):
	if line[0] in 'D':
		remark = line
	else:
		if line[0] in '#':
			print remark[:-1]
			remark = "Dshould not happen"
		print line[:-1]
	line = sys.stdin.readline()
