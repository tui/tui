char *tbname;

FILE *subfile(char *subname)
{
	FILE *ret;
	char buf[10240];
	sprintf(buf, "%s/%s", tbname, subname);
	ret = fopen(buf, "r");
	if (!ret)
		die("Could not open file");
	return ret;
}

unsigned long getlong(FILE *f)
{
	unsigned long i;
	int ok;
	
	i = fgetc(f) << 24 | fgetc(f) << 16 | fgetc(f) << 8 | (ok = fgetc(f));
	if (ok == EOF)
		printf("Error reading?!");
	return i;
}

unsigned long getvar(FILE *f)
{
	unsigned long i = 0; 
	unsigned char j = 0;

	while ( (j = fgetc(f)) & 0x80 ) {
		i <<= 7;
		i |= (j & 0x7f);
	}
	i <<= 7;
	i |= (j & 0x7f);
	return i;
}

FILE *readme, *towns_txt, *towns_dat, *remarks_txt, *flags_txt, *flags_dat, *conns_txt, *conns_dat, *nodes_dat;

int num_towns, num_conns, num_nodes;

void read_readme(void) {
	char buf[10240];
	while (fgets(buf, 10240, readme)) {
		int i;
		if (sscanf(buf, "!towns %d", &i))
			num_towns = i;
		if (sscanf(buf, "!conns %d", &i))
			num_conns = i;
		if (sscanf(buf, "!nodes %d", &i))
			num_nodes = i;
		printf(buf);
	}
}

char *get_line(FILE *txt, int offset)
{
	static char buf[10240];
	int i;

	sprintf(buf, "(error reading at %lx)\n", offset);
	fseek(txt, offset, SEEK_SET);
	fread(buf, 10240, 1, txt);
	for (i=0; i<10240; i++)
		if (buf[i] == '\n')
			buf[i] = 0;
	return buf;
}

char *gettext(FILE *dat, FILE *txt, int offset)
{
	static char buf[10240];
	int i;

	fseek(dat, offset, SEEK_SET);
	i = getlong(dat);
	return get_line(txt, i);
}

void read_headers(char *s)
{
	tbname = s;
	readme = subfile("README");
	towns_txt = subfile("towns.txt");
	towns_dat = subfile("towns.dat");
	conns_txt = subfile("conns.txt");
	conns_dat = subfile("conns.dat");
	remarks_txt = subfile("remarks.txt");
	nodes_dat = subfile("nodes.dat");
	flags_txt = subfile("flags.txt");
	flags_dat = subfile("flags.dat");
	read_readme();
}

char *town2name(int town)
{
	return gettext(towns_dat, towns_txt, town*TOWNS_SIZE);
}

char *conn2name(int i)
{
	return gettext(conns_dat, conns_txt, i*CONNS_SIZE);
}

/* Returns town or -1 at end of connection */
int print_node(int *starttime, char *add, int flags)
{
	int town = getvar(nodes_dat);
	if (!town)
		return -1;
	town--;
	*starttime += getvar(nodes_dat);
	printf("%2d:%02d\t", *starttime/60, *starttime%60);
	*starttime += getvar(nodes_dat);
	printf("%2d:%02d\t", *starttime/60, *starttime%60);
	if (add)
		printf("%s", add);
	printf("%s\t", get_line(flags_txt, getvar(nodes_dat)));
	printf("+%d\t%s\n", getvar(nodes_dat), town2name(town));
	return town;
}

void print_conn(int i, char *add, int flags)
{
	int node, starttime, town, remark;
	printf("# %s\n", conn2name(i));

	fseek(conns_dat, i*CONNS_SIZE, SEEK_SET);
	getlong(conns_dat);	/* Name */
	remark = getlong(conns_dat);
	node = getlong(conns_dat);
	starttime = getlong(conns_dat);
	fseek(nodes_dat, node, SEEK_SET);
	while (1) {
		if (print_node(&starttime, add, flags) == -1)
			break;
	} 
	printf("%s\n", get_line(remarks_txt, remark));
}
