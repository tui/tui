unit halda;

interface

const
     lowlongint=maxlongint-100;
type
    pcon=^tcon;
    tcon=record
 {              train:longint;    }       {akym vlakom}
               adress:longint;
{               dest:ptown;  }            {kam}
{               rempos:longint;   }       {poznamky}
{               depa:longint;    }        {odchod z mesta}
{               arri:longint;    }        {prichod do ineho mesta}
               next:pcon;
         end;

    ptown=^ttown;
    ttown=record
{                name:string;            meno mesta}
                 adress:longint;
                 odk:longint;            {odkial sme prisli}
                 smer:longint;           {kam ideme}
                 arrived:longint;        {pouzity vlak (index do conns.txt)}
                 finished:boolean;       {je to uz finalny vyhl.cas}
                 index:longint;          {index mesta}
                 time:longint;           {vyhladavaci cas}
                 date:longint;           {vyhladvaci datum}
                 pres:byte;              {pocet vykonanych prestupov}
                 dist:longint;           {vzdialenost}
                 hcon:pcon;              {hlava konexii}
                 l,r:ptown;
                 up:ptown;
          end;


var
   hh:ptown;
   prvky:longint;

procedure init;                                  {inicializuje haldu}
{procedure writex(what:ptown);}
function issmaller(fst,snd:ptown):boolean;       {porovna dve mesta, ktore je mensie}
                                                 {uz prehladane mesta (finished=true) su "vacsie"}
procedure bubble(what:ptown);                    {bubla prvok v halde korektne hore}
procedure switch(dir:boolean;what:ptown);        {vymeni dva posebe iduce prvky v halde}
procedure bubbledspec(what:ptown);               {procedure na bublanie v halde dolu, jedna korktna, druha nekorektna/pomocna}
procedure bubbled(what:ptown);
procedure addnew(ind,tim,dat:longint);           {prida prvok do haldy}
procedure delete(what:ptown);                    {znici prvok v halde}
procedure maketownsfalse(start:ptown);           {oznaci vsetky mesta ako este neprehladavane, tj finished:=false}
function town(co:longint;start:ptown):ptown;     {vrati pointer na mesto s danym indexom}
procedure donestruct;                            {uvolni pamet}

implementation

procedure init;
begin
     hh:=nil;
     prvky:=0;
end;

{procedure writex(what:ptown);
var
   x:char;
begin
     writeln;
     writeln;
     if what^.up=nil then writeln('              NIL') else
                          writeln('             ',what^.up^.index);
     writeln;
     if what^.up<>nil then
     begin
          if what^.up^.l=what then write('LEFT      : ') else
                                   write('RIGHT     :');
     end else                      write('NIL       :');
     writeln(what^.index);
     writeln(what^.l^.index,'   ',what^.r^.index);
     x:=readkey;
end;}

function issmaller(fst,snd:ptown):boolean;
begin
     issmaller:=false;
     if ((not fst^.finished) and (snd^.finished)) then issmaller:=true else
     if (fst^.date<snd^.date) then issmaller:=true else
     if ((fst^.date=snd^.date) and (fst^.time<snd^.time)) then issmaller:=true;
end;

procedure bubble(what:ptown);
var
   spec,pom:ptown;
   l1,l2:ptown;
   save:boolean;
begin
     while (what^.up<>nil) and (issmaller(what,what^.up)) do
     begin
          pom:=what^.up;
          spec:=pom^.up;
          if spec<>nil then
          begin
               if spec^.l=pom then spec^.l:=what else spec^.r:=what;
          end;
          if pom=hh then save:=true else save:=false;
          what^.up:=pom^.up;
          if pom^.l=what then
          begin
               l1:=what^.l;
               l2:=what^.r;
               what^.l:=pom;
               what^.r:=pom^.r;
               pom^.r^.up:=what;
               pom^.l:=l1;
               if l1<>nil then l1^.up:=pom;
               pom^.r:=l2;
               if l2<>nil then l2^.up:=pom;
          end else
          if pom^.r=what then
          begin
               l1:=what^.l;
               l2:=what^.r;
               what^.r:=pom;
               what^.l:=pom^.l;
               pom^.l^.up:=what;
               pom^.l:=l1;
               if l1<>nil then l1^.up:=pom;
               pom^.r:=l2;
               if l2<>nil then l2^.up:=pom;
          end;
          pom^.up:=what;
          if save then hh:=what;
     end;
end;

procedure switch(dir:boolean;what:ptown);
var
   pom,pom2,up:ptown;
   save:boolean;
begin
     if what=hh then save:=true else save:=false;
     if dir then
     begin
          pom:=what^.l;
          pom2:=what^.r;
     end else
     begin
          pom:=what^.r;
          pom2:=what^.l;
     end;
     up:=what^.up;
     if up<>nil then
     begin
          if up^.l=what then
          begin
               up^.l:=pom;
          end else up^.r:=pom;
     end;
     what^.up:=pom;
     what^.l:=pom^.l;
     what^.r:=pom^.r;
     pom^.up:=up;
     if pom^.l<>nil then pom^.l^.up:=what;
     if pom^.r<>nil then pom^.r^.up:=what;
     if dir then
     begin
          pom^.l:=what;
          pom^.r:=pom2;
     end else
     begin
          pom^.l:=pom2;
          pom^.r:=what;
     end;
     if pom2<>nil then pom2^.up:=pom;
     if save then hh:=what^.up;
end;

procedure bubbledspec(what:ptown);
var
   l1,l2:ptown;
   dir:boolean;
begin
     dir:=true;
     l1:=what^.l;
     l2:=what^.r;
     if (l1<>nil) and (l2<>nil) and (not issmaller(l1,l2)) then
     begin
          l1:=l2;
          dir:=false;
     end;
     while (l1<>nil) and (issmaller(l1,what)) do
     begin
          switch(dir,what);
          dir:=true;
          l1:=what^.l;
          l2:=what^.r;
          if (l1<>nil) and (l2<>nil) and (not issmaller(l1,l2)) then
          begin
               l1:=l2;
               dir:=false;
          end;
     end;
end;

procedure bubbled(what:ptown);
var
   z:longint;
procedure convertz;
var
   pom:longint;
begin
     pom:=1;
     while z>1 do
     begin
          if z mod 2 =0 then pom:=pom*2 else pom:=pom*2+1;
          z:=z div 2;
     end;
     z:=pom;
end;
begin
     z:=prvky;
     convertz;
     while z>1 do
     begin
          if z mod 2=0 then switch(true,what) else
                            switch(false,what);
          z:=z div 2;
     end;
end;

procedure addnew(ind,tim,dat:longint);
var
   z:longint;
   pom,pom2:ptown;
procedure convertz;
var
   pom:longint;
begin
     pom:=1;
     while z>1 do
     begin
          if z mod 2 =0 then pom:=pom*2 else pom:=pom*2+1;
          z:=z div 2;
     end;
     z:=pom;
end;
begin
     pom:=hh;
     pom2:=hh;
     prvky:=prvky+1;
     z:=prvky;
     convertz;
     while z>1 do
     begin
          if z mod 2=0 then
          begin
               pom:=pom^.l;
               if pom<>nil then pom2:=pom;
          end else
          if z mod 2=1 then
          begin
               pom:=pom^.r;
               if pom<>nil then pom2:=pom;
          end;
          z:=z div 2;
     end;
     if pom2=nil then
     begin
          new(pom);
          pom^.time:=tim;
          pom^.date:=dat;
          pom^.index:=ind;
          pom^.finished:=false;
          pom^.up:=nil;
          pom^.l:=nil;
          pom^.r:=nil;
          pom^.hcon:=nil;
          hh:=pom;
     end else
     begin
          new(pom);
          pom^.time:=tim;
          pom^.date:=dat;
          pom^.index:=ind;
          pom^.finished:=false;
          pom^.up:=pom2;
          if pom2^.l=nil then
          begin
               pom2^.l:=pom;
          end else
               pom2^.r:=pom;
          pom^.l:=nil;
          pom^.r:=nil;
          pom^.hcon:=nil;
          bubble(pom);
     end;
end;

procedure delete(what:ptown);
var
   up:ptown;
begin
     what^.time:=maxlongint;
     what^.date:=maxlongint;
     what^.finished:=true;
     bubbled(what);
     up:=what^.up;
     if up<>nil then
       if up^.l=what then up^.l:=nil else up^.r:=nil;
     if what=hh then
     begin
          dispose(what);
          hh:=nil;
     end else dispose(what);
end;

procedure maketownsfalse(start:ptown);
var
   vvt:ptown;
begin
     vvt:=start;
     if vvt^.l<>nil then maketownsfalse(vvt^.l);
     if vvt^.r<>nil then maketownsfalse(vvt^.r);
     if vvt<>nil then
     begin
          vvt^.finished:=false;
          vvt^.odk:=0;
          vvt^.time:=lowlongint;
          vvt^.date:=lowlongint;
          vvt^.arrived:=0;
          vvt^.pres:=0;
     end;
end;

function town(co:longint;start:ptown):ptown;
var
   pom,poml,pomr:ptown;
begin
     pom:=start;
     if pom^.index=co then town:=pom else
     begin
          if pom^.l<>nil then poml:=town(co,pom^.l) else poml:=nil;
          if (poml<>nil) and (poml^.index=co) then
          begin
               town:=poml;
               pom:=poml;
          end else
          begin
               if pom^.r<>nil then pomr:=town(co,pom^.r) else pomr:=nil;
               if (pomr<>nil) and (pomr^.index=co) then
               begin
                    town:=pomr;
                    pom:=pomr;
               end;
          end;
     end;
     if pom^.index<>co then town:=nil;
end;

procedure donestruct;
var
   vc:pcon;
begin
     while hh<>nil do
     begin
          with hh^ do
          begin
               vc:=hcon;
               while vc<>nil do
               begin
                    vc:=vc^.next;
                    dispose(hcon);
                    hcon:=vc;
               end;
          end;
          delete(hh);
{          writex(hh);}
          prvky:=prvky-1;
     end;
end;

end.
