const maxdataindex = 100000;
type condata = record
                conp, remp, nodep, time: Longint;
               end;
     stationdata = record
                    fpos: Longint;
                    st: string[50];
                    end;
     stationdatarr = array[0..maxdataindex] of stationdata;
     nodedata = record
                 stind: Longint;
                 arriv, depart: Longint;
                 flg: Longint;
                 dist: Longint;
                end;



var f {pid.txt}: text;
    cont {conns.txt},
    remt {remarks.txt},
    flagt {flags.txt},
    townt {towns.txt},
    townb {towns.dat},
    conb {conns.dat},
    flagb {flags.dat},
    nodeb {nodes.dat}: file;
    w: Word;
    s, leaves, arrives, kms, flags, station: string;
    cd: condata;
    sts, conlist: stationdatarr;
    stations, connections: Longint;
    existsprevious, newconstarted: Boolean;
    curnode: nodedata;
    line, zind, initime, ldtime: Longint;
    openresult: Word;


const pidname: string = 'pid2.tt';
      newline: array[0..1] of char = (#$0d, #$0a);
      lastconn: string = '';
      zero: Byte = 0;
      lzero: Longint = 0;

procedure DIE(preco: string; rijadok: Longint);
begin
WriteLn('SKAPINAM z nasledujuceho dovodu: ', preco, ' na vstupnom subore riadok #', rijadok);
Halt(1);
end;

function Str2Int(str: string): Longint;
var err: integer;
    l: Longint;
begin
val(str, l, err);
if err <> 0 then DIE('Str2Int: nespravny format cisla', line);
Str2Int := l;
end;

function FindOrAdd(what: string; var datarr: stationdatarr; var rcount: Longint): Longint;
var l, fp: longint;
    found: Boolean;
begin
found := false;
for l := 1 to rcount do
  begin
  if what=datarr[l].st then
    begin
    found := true;
    fp := datarr[l].fpos;
    end;
  end;
if found then FindOrAdd := fp else
  begin
  inc(rcount);
  if rcount > maxdataindex then DIE('FindOrAdd: Prilis vela zaznamov!', line);
  with datarr[rcount] do
    begin
    fpos := datarr[rcount-1].fpos + Length(datarr[rcount-1].st) + 2;
    st := what;
    FindOrAdd := fpos;
    end;
  end;
end;

function FindOrAddIndex(what: string; var datarr: stationdatarr; var rcount: Longint): Longint;
var l, fp: longint;
    found: Boolean;
begin
found := false;
for l := 1 to rcount do
  begin
  if what=datarr[l].st then
    begin
    found := true;
    fp := datarr[l].fpos;
    end;
  end;
if found then FindOrAddIndex := l else
  begin
  inc(rcount);
  if rcount > maxdataindex then DIE('FindOrAddIndex: Prilis vela zaznamov!', line);
  with datarr[rcount] do
    begin
    fpos := datarr[rcount-1].fpos + Length(datarr[rcount-1].st) + 2;
    st := what;
    FindOrAddIndex := rcount;
    end;
  end;
end;

function Wrlnpos(var fileid: file; what: string): Longint;
begin
Wrlnpos := FilePos(fileid);
BlockWrite(fileid, what[1], Length(what));
BlockWrite(fileid, newline, 2);
end;

function Wrpos(var fileid: file; what: string): Longint;
begin
Wrpos := FilePos(fileid);
BlockWrite(fileid, what[1], Length(what));
end;

function WrDWPos(var fileid: file; what: Longint): Longint;
begin
WrDWPos := FilePos(fileid);
BlockWrite(fileid, what, sizeof(what));
end;

function NewCon(str: string): Boolean;
var rslt: Boolean;
    conpos: Longint;
begin
rslt := false;
if (Length(s) > 0) and (s[1] = '#') then
  begin
  rslt := true;
  if existsprevious then
    begin
    BlockWrite(nodeb, lzero, sizeof(lzero));
    BlockWrite(remt, zero, 1);
    end;
  Delete(str, 1, 2);
  cd.conp := FindOrAdd(str, conlist, connections);
  cd.remp := FilePos(remt);
  cd.nodep := FilePos(nodeb);
  end;
NewCon := rslt;
end;

procedure SplitLine(what: string; var l, a, r, k, stn: string);
var b, tabp: Byte;
begin
tabp := Pos(#9, what);
l := '';
a := '';
r := '';
k := '';
stn := '';
if tabp >1 then l := Copy(what, 1, tabp-1);
Delete(what, 1, tabp);
tabp := Pos(#9, what);
if tabp >1 then a := Copy(what, 1, tabp-1);
Delete(what, 1, tabp);
tabp := Pos(#9, what);
if tabp >1 then r := Copy(what, 1, tabp-1);
Delete(what, 1, tabp);
tabp := Pos(#9, what);
if tabp >1 then k := Copy(what, 1, tabp-1);
Delete(what, 1, tabp);
stn := what;
end;

function EncodeTime(str: string): Longint;
var hr, min: string;
    hl, ml: Longint;
    err: integer;
begin
{Write('cas: ', str, ' ** ');}
hr := Copy(str, 1, Pos(':', str)-1);
min := Copy(str, Pos(':', str)+1, 255);
{WriteLn(hr, ':', min);}
val(hr, hl, err);
if err <> 0 then DIE('EncodeTime: Blby format casu', line);
val(min, ml, err);
if err <> 0 then DIE('EncodeTime: Blby format casu', line);
EncodeTime := hl*60+ml;
end;

function NoTab(where: string): Boolean;
begin
NoTab := Pos(#9, where) = 0;
end;

procedure WriteVARI(l: Longint);

  Procedure EncodeVARI(what: Longint; var dest: Longint; var bytesno: Byte);
  var highestbit, w, destbit: Longint;
  begin
  what := what and $1FFFFF;
  for w := 20 downto 0 do if (1 shl w) and what <> 0 then
    begin
    highestbit := w;
    Break;
    end;
  dest := 0;
  destbit := 0;
  bytesno := 1;
  for w := 1 to (highestbit div 7) do
    begin
    dest := dest or ((what and $7F) or $80) shl destbit;
    what := what shr 7;
    inc(destbit, 8);
    inc(bytesno);
    end;
  dest := dest or (what and $7f) shl destbit;
  end;

var vr: Longint;
    bytes: Byte;
begin
EncodeVARI(l, vr, bytes);
BlockWrite(nodeb, vr, bytes);
end;

procedure WriteNodeOnce(np: nodedata);
begin
WriteVARI(Abs(np.stind));
WriteVARI(Abs(np.arriv));
WriteVARI(Abs(np.depart));
WriteVARI(Abs(np.flg));
WriteVARI(Abs(np.dist));
end;

procedure WriteConnBin(what: condata);
begin
BlockWrite(conb, what, sizeof(what));
end;

procedure WriteStationsTxBin;
var l, l2: longint;
begin
for l := 1 to stations do
  begin
  WrLnPos(townt, sts[l].st);
  BlockWrite(townb, sts[l].fpos, sizeof(l));
  end;
end;

procedure WriteConsTx;
var l: Longint;
begin
for l := 1 to connections do Wrlnpos(cont, conlist[l].st);
end;

function ComputeTime(constime, ldeptime: Longint; timestr: string): Longint;
var l: Longint;
begin
if timestr = '' then ComputeTime := ldeptime-constime else
  begin
  if timestr[1] = '+' then
    begin
    ComputeTime := Str2Int(timestr);
    ldtime := ldtime + Str2Int(timestr);
    end else
    begin
    l := EncodeTime(timestr);
    ldeptime := l;
    ComputeTime := l-constime;
    end;
  end;
end;

BEGIN
if ParamCount < 1 then
  begin
  WriteLn('TT2TB <ttfile.tt>');
  Halt(2);
  end;
pidname := ParamStr(1);
stations := 0;
connections := 0;
sts[0].fpos := -2;
sts[0].st := '';
conlist[0].fpos := -2;
conlist[0].st := '';
existsprevious := false;
line := 0;

openresult := 0;
{$I-}
Assign(f, pidname);
Reset(f);
openresult := openresult or IOResult;
Assign(cont, 'conns.txt');
Assign(remt, 'remarks.txt');
Assign(flagt, 'flags.txt');
Assign(townt, 'towns.txt');
Assign(townb, 'towns.dat');
Assign(conb, 'conns.dat');
Assign(flagb, 'flags.dat');
Assign(nodeb, 'nodes.dat');
Rewrite(cont, 1);
openresult := openresult or IOResult;
Rewrite(remt, 1);
openresult := openresult or IOResult;
Rewrite(flagt, 1);
openresult := openresult or IOResult;
Rewrite(townt, 1);
openresult := openresult or IOResult;
Rewrite(townb, 1);
openresult := openresult or IOResult;
Rewrite(conb, 1);
openresult := openresult or IOResult;
Rewrite(flagb, 1);
openresult := openresult or IOResult;
Rewrite(nodeb, 1);
openresult := openresult or IOResult;
{I+}
if openresult <> 0 then
  begin
  WriteLn('I/O chyba pri otvarani suborov!');
  Halt(3);
  end;
while not Eof(f) do
  begin
  inc(line);
  {if line mod 1000 = 0 then WriteLn('Riadok ', line);}
  ReadLn(f, s);
  if NewCon(s) then
    begin
    newconstarted := true;
    Continue;
    end;
  SplitLine(s, arrives, leaves, flags, kms, station);
  if newconstarted then
    begin
    cd.time := EncodeTime(leaves);
    initime := cd.time;
    ldtime := cd.time;
    BlockWrite(conb, cd, sizeof(cd));
    {zistit cas odchodu, zapisat do conns.dat}
    newconstarted := false;
    zind := FindOrAddIndex(station, sts, stations);
    with curnode do
      begin
      stind := zind+1;
      arriv := 0;
      dist := 0;
      depart := 0;
      flg := WrDWPos(flagb, WrLnPos(flagt, flags));
      end;
    WriteNodeOnce(curnode);
    existsprevious := true;
    end else
    begin
    if NoTab(s) then WrLnPos(remt, s) {zapi's~ pozna'mku} else
      begin
      with curnode do
        begin
          stind := FindOrAddIndex(station, sts, stations) + 1;
          arriv := ComputeTime(initime, ldtime, arrives);
          depart := ComputeTime(initime, ldtime, leaves);
          dist := Str2Int(kms);
          flg := WrDWPos(flagb, WrLnPos(flagt, flags));
        end;
      WriteNodeOnce(curnode);
      end;
    end;
  end;
WriteConsTx;
WriteStationsTxBin;
BlockWrite(nodeb, lzero, sizeof(lzero));
BlockWrite(remt, zero, 1);
Close(f);
Close(cont);
Close(remt);
Close(flagt);
Close(townt);
Close(townb);
Close(conb);
Close(flagb);
Close(nodeb);
END.
