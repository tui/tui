/* -*- linux-c -*-
 * Connection finder, distribute under GPL version 2 or later
 * Copyright 2001 Pavel Machek <pavel@ucw.cz>
 */
#include <stdio.h>
#include "../timetab.h"

#include "utils.c"

void read_conns(void)
{
	int i;
	for (i=0; i<num_conns; i++) {
		print_conn(i, NULL, 0);
	}
}

int main(int argc, char *argv[])
{
	read_headers("output.tb");
	read_conns();
	return 0;
}
