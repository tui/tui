/* -*- linux-c -*-
 * Connection finder, distribute under GPL version 2 or later
 * Copyright 2001 Pavel Machek <pavel@ucw.cz>
 */

/* IIRC this should be simple "dijkstra" search, but over connections,
 * not towns.

  FIXME:
     ../../tbase/secon 'Tehovec' 26.1.2019 12:30 Vychovatelna

     ...produces some rather non-sensical results, like trying to go
     Palmovka->X->Palmovka->destination

     ../../tbase/secon 'Tehovec' 26.1.2019 12:30 Vychovatelna

*/

#include <stdio.h>
#include "../timetab.h"

#include "utils.c"

char *opts;

struct conn {
	int startnode;
};

/* FIXME: convert towns[].conns into towns[].nodes; make them sorted reverse by time */

struct conn *conns;

struct town {
	int nconns;
	int *conns;
	int *times;
	int flags;
#define START 1
#define END 2
};

struct town *towns;

struct node {
	double prob;
	union {
		int time;
		int via;
	} u;
	int heapindex;
};

struct node **heap;
struct node *nodes;

static int currnode = 0;

#define MAX(a, b) ((a) > (b) ? (a) : (b))

void write_conn(int i)
{
	long node, starttime, town, remark;

	fseek(conns_dat, i*CONNS_SIZE, SEEK_SET);
	getlong(conns_dat);	/* Name */
	remark = getlong(conns_dat);
	node = getlong(conns_dat);
	starttime = getlong(conns_dat);
	fseek(nodes_dat, node, SEEK_SET);
	conns[i].startnode = currnode;
	while (1) {
		town = getvar(nodes_dat);
		if (!town)
			break;
		town--;
		currnode++;
		towns[town].conns[towns[town].nconns] = i;
		starttime += getvar(nodes_dat);
		starttime += getvar(nodes_dat);
		towns[town].times[towns[town].nconns++] = starttime;
		getvar(nodes_dat);
		getvar(nodes_dat);
	}
}

void count_conn(int i)
{
	long node, starttime, town, remark;

	fseek(conns_dat, i*CONNS_SIZE, SEEK_SET);
	getlong(conns_dat);	/* Name */
	remark = getlong(conns_dat);
	node = getlong(conns_dat);
	starttime = getlong(conns_dat);
	fseek(nodes_dat, node, SEEK_SET);
	while (1) {
		town = getvar(nodes_dat);
		if (!town)
			break;
		town--;
		towns[town].nconns++;
		starttime += getvar(nodes_dat);
		starttime += getvar(nodes_dat);
		getvar(nodes_dat);
		getvar(nodes_dat);
	}
}

void count_conns(void)
{
	int i;
	for (i=0; i<num_conns; i++) {
		count_conn(i);
	}
	fprintf(stderr, "." );
	for (i=0; i<num_towns; i++) {
		towns[i].conns = mallocz(sizeof(int)*towns[i].nconns);
		towns[i].times = mallocz(sizeof(int)*towns[i].nconns);
		towns[i].nconns = 0;
	}
	fprintf(stderr, "." );
	for (i=0; i<num_conns; i++) {
		write_conn(i);
	}
}

void mark_town(char * s, int j)
{
	int i;
	for (i=0; i<num_towns; i++) {
		char *t = town2name(i);
		if (!strcmp(s, t)) {
			fprintf(stderr, "#");
			towns[i].flags = j;
		}
	}
}

double compute_change(int conn1, int time1, int conn2, int time2)
{
#if 0
	fprintf(stderr, "[%d:%2d %s ", time1/60, time1%60, conn2name(conn1));
	fprintf(stderr, "%d:%2d %s]", time2/60, time2%60, conn2name(conn2));
#endif
	if (time1 > time2)
		return 0.0;
	if ((conn1 == conn2) || (conn2 == -1))
		return 1.0;
	switch (time2-time1) {
	case 0: return 0.1;
	case 1: return 0.3;
	case 2: return 0.4;
	case 3: return 0.7;
	case 4: return 0.8;
	}
	if (time2-time1 < 7) return 0.9;
	if (time2-time1 < 15) return 0.95;
	if (time2-time1 < 30) return 0.97;
	if (time2-time1 < 60) return 0.98;
	return 0.99;
}

/* Find town, when given connection arrives to given town */

int find_time_conn_in_town(int i, int a_town, int *currnodeptr)
{
	long node, node2 = 0, starttime, starttime2, town, remark;

	fseek(conns_dat, i*CONNS_SIZE, SEEK_SET);
	getlong(conns_dat);	/* Name */
	remark = getlong(conns_dat);
	node = getlong(conns_dat);
	starttime2 = starttime = getlong(conns_dat);
	fseek(nodes_dat, node, SEEK_SET);
	currnode = conns[i].startnode;
	while (1) {
		node2 = ftell(nodes_dat);
		town = getvar(nodes_dat);
		if (!town)
			die("Bleee, town is not there?");
		town--;
		starttime += getvar(nodes_dat);
		if (town == a_town) {
			if (currnodeptr)
				*currnodeptr = currnode;
			return starttime;
		}
		starttime += getvar(nodes_dat);
		getvar(nodes_dat);
		getvar(nodes_dat);
		currnode++;
	}
}

#define SWAP(a,b) { int c=a; a=b; b=c; }
void sort_rtable(void)
{
	int i;
	for (i=0; i<num_towns; i++) {
		int changed = 0, j;
		do {
			changed = 0;
			for (j=0; j<towns[i].nconns-1; j++)
				if (towns[i].times[j] < 
				    towns[i].times[j+1]) {
					SWAP(towns[i].conns[j], towns[i].conns[j+1]);
					SWAP(towns[i].times[j], towns[i].times[j+1]);
					changed = 1;
				}
		} while (changed);
		if (!(i%100)) fprintf(stderr, ".");
	}
}



/*
 * Adds all arrivals that are before time to list of connection with nonzero probability
 */
void arrival_conn(int i, int a_town, double prob, int time, int conn_from)
{
	long node, starttime, starttime2, town, remark;

	if (find_time_conn_in_town(i, a_town, NULL) >= time)
		return;

	fseek(conns_dat, i*CONNS_SIZE, SEEK_SET);
	getlong(conns_dat);	/* Name */
	remark = getlong(conns_dat);
	node = getlong(conns_dat);
	starttime = getlong(conns_dat);
	fseek(nodes_dat, node, SEEK_SET);
	currnode = conns[i].startnode;
	while (1) {
		int flag;
		town = getvar(nodes_dat);
		if (!town)
			die("May not happen");
		town--;
		if (town == a_town)
			return;
		starttime += getvar(nodes_dat);
		flag = (nodes[currnode].u.time == -1);
		nodes[currnode].u.time = starttime;
		nodes[currnode].prob = -1; /* compute_change(i, starttime, conn_from, time); */
		if (flag) {
			insertheap(&nodes[currnode]);
		}
		starttime += getvar(nodes_dat);
		getvar(nodes_dat);
		getvar(nodes_dat);
		currnode++;
	}
}


void
arrival(int town, double prob, int time, int conn_from)
{
	int i;
	for (i=0; i<towns[town].nconns; i++) {
		int conn;
		conn = towns[town].conns[i];
		arrival_conn(conn, town, prob, time, conn_from);
	}
}

/* Probability, that we are able to leave with train "conn" going 
   at "time" to "town"
*/
double probability(int node, int town, int time, int conn, int flags)
{
	int i, time2, time3, node2, town2;
	double max = 0.0, chg, chg2;
	time3 = 99999999;
	for (i=0; i<towns[town].nconns; i++) {
		int conn2;
		double prob;
		conn2 = towns[town].conns[i];
		time2 = find_time_conn_in_town(conn2, town, &node2);
		time2 += getvar(nodes_dat);
#if 0
		/* FIXME: this triggers! */
		if (time2>time3)
			die("blee, not sorted properly (%d:%02d > %d:%02d)", time2/60, time2%60, time3/60, time3%60);
#endif
		time3 = time2;
		getvar(nodes_dat);
		getvar(nodes_dat);
		town2 = getvar(nodes_dat)-1;

		prob = nodes[node2].prob;
		chg2 = compute_change(conn, time, conn2, time2);
		chg = chg2*prob + (1-chg2)*max;	/* If we don't catch this train, we stay on the station and catch the next one. */ 
		if (flags && ((chg2 * prob) > 0.3))
			printf("; You can go using %s with probability %f\n", conn2name(conn2), chg2*prob);

		if (max < chg)
			max = chg, nodes[node].u.via = node2;
//		fprintf(stderr, "(%f, %d:%02d, %s)", chg, time2/60, time2%60, town2name(town2));
	}
	return max;
}

int node2conn(int node2)
{
	int i;
	for (i=0; i<num_conns-1; i++)
		if ((conns[i].startnode <= node2) && (conns[i+1].startnode > node2))
			break;
	if (i==num_conns)
		i = -1;
	return i;
}

/* Returns connection, as a special bonus */
int seek_to_node(int node2, int *time)
{
	int i;
	long node, starttime, stoptime, town, town2, remark;

	i = node2conn(node2);

	fseek(conns_dat, i*CONNS_SIZE, SEEK_SET);
	getlong(conns_dat);	/* Name */
	remark = getlong(conns_dat);
	node = getlong(conns_dat);
	starttime = getlong(conns_dat);
	fseek(nodes_dat, node, SEEK_SET);
	town = -1;
	currnode = conns[i].startnode;
	while (1) {
		if (node2 == currnode)
			break;
		if (node2 < currnode)
			die("Blee, I missed it somehow?");
		town = getvar(nodes_dat);
		if (!town)
			die("Bleee, departure is not there?");
		town--;
		starttime += getvar(nodes_dat);
		starttime += getvar(nodes_dat);
		getvar(nodes_dat);
		getvar(nodes_dat);
		currnode++;
	}
	if (time)
		*time = starttime;
	return i;
}

void print_details(int node2)
{
	int starttime;
	long town, stoptime, town2, i;
	i = seek_to_node(node2, &starttime);
	/* To find out probability of getting to node2,
	   we are interested in trains _leaving_ destination 
	   of node2
	*/
	town = getvar(nodes_dat)-1;
	starttime += getvar(nodes_dat);
	stoptime   = getvar(nodes_dat) + starttime;
	getvar(nodes_dat);
	getvar(nodes_dat);
	town2 = getvar(nodes_dat)-1;
	stoptime  += getvar(nodes_dat);
	if (towns[town2].flags == END) {
		printf("; This ends in target town, therefore its safe\n");
	} else {
		if (nodes[node2].prob == nodes[node2+1].prob)
			/* printf("; We are staying in some connection, thats safe\n") */;
		else
			probability(node2, town2, stoptime, i, 1);
	}
}

void print_res(int node)
{
	int starttime, town, conn = -1, i;
	while (node != -1) {
		char buf[10240];
		i = node2conn(node);
		if (i != conn) {
			if (conn != -1) {
				print_node(&starttime, NULL, 0);
				/* FIXME: print remarks etc from ending connection */
			}
			conn = i;
			printf("# %s\n", conn2name(conn));
		}
		seek_to_node(node, &starttime);
		sprintf(buf, "[%f]", nodes[node].prob);
		print_node(&starttime, buf, 0);
		{
			int i = ftell(nodes_dat);
			print_details(node);
			fseek(nodes_dat, i, SEEK_SET);
		}
		node = nodes[node].u.via;
	}
	print_node(&starttime, NULL, 0);
	/* FIXME: print remarks etc from ending connection */
}

void departure_conn(int node2)
{
	int starttime;
	long town, stoptime, town2, i;
	i = seek_to_node(node2, &starttime);
	/* To find out probability of getting to node2,
	   we are interested in trains _leaving_ destination 
	   of node2
	*/
	town = getvar(nodes_dat)-1;
	starttime += getvar(nodes_dat);
	stoptime   = getvar(nodes_dat) + starttime;
	getvar(nodes_dat);
	getvar(nodes_dat);
	town2 = getvar(nodes_dat)-1;
	stoptime  += getvar(nodes_dat);
	if (towns[town2].flags == END) {
		nodes[node2].prob = 1.0;
		nodes[node2].u.via= -1;
	} else {
		if (town2 != -1)	/* We can surely stay same connection */
			nodes[node2].prob = nodes[node2+1].prob;
		nodes[node2].prob = MAX(probability(node2, town2, stoptime, i, 0), nodes[node2].prob);
	}
#if 0
	fprintf(stderr, "%2d:%02d	%f	(%d)%s ->	", starttime/60, starttime%60, 
		nodes[node2].prob, town, town2name(town));
	fprintf(stderr, " %2d:%02d	(%d)%s\n", stoptime/60, stoptime%60, town2, town2name(town2));
#endif

	if (towns[town].flags == START) {
		static int nres = 0;
		printf("; Probability %f\n", nodes[node2].prob);
		print_res(node2);
		if (nres++ > 5)
			die("done");
	}
	if (nodes[node2].prob > 0.1)
		/* It needs to be recomputed here */
		arrival(town, nodes[node2].prob, starttime, i);
	else
		fprintf(stderr, ";");
	return;


}

/*
 * Heap stuff
 */ 

#define TIME(heapindex) -heap[heapindex]->u.time
#define BACKLINK(heapindex) heap[heapindex]->heapindex = heapindex
#define INF 999999
#include "../heap.c"

void
swapheap(int a, int b)
{
	struct node *x = heap[a];
	heap[a] = heap[b];
	heap[b] = x;
	
	heap[a]->heapindex = a;
	heap[b]->heapindex = b;
}

void
setupheap(void)
{
	int size;
	heapalloc = 2*num_nodes + 5;
	size = sizeof(struct node *) * heapalloc;
/*	fprintf(stderr, "Creating heap...\n" );*/
	if (heap)
		free(heap);
	heap = mallocz(size);
	heapcount = 0;
}

void
insertheap(struct node *node)
{
	if (heapcount > heapalloc/2)
		die( "Sorry, I was lazy\n" );
	heap[++heapcount] = node;
	fixdown(heapcount);
}

/* Returns node with maximal time or -1 */
int findmax(void)
{
#if 0
	int i, maxtime = 0, maxnode = -1;
	fprintf(stderr, ".");
	for (i=0; i<num_nodes; i++)
		if ((nodes[i].prob == -1) && (nodes[i].u.time > maxtime)) 
			maxnode = i, maxtime = nodes[i].u.time;
	return maxnode;
#endif
	int maxnode;
	maxnode = heap[1]-nodes;
	deleteheap(1);
	return maxnode;
}

int main(int argc, char *argv[])
{
	int i;

	if (*argv[1] == '-') {
		opts = argv[1]+1;
		argv++;
                argc--;
	}

	read_headers("output.tb");
	conns = mallocz(sizeof(struct conn) * num_conns);
	towns = mallocz(sizeof(struct town) * num_towns);
	num_nodes = 1300000;
	nodes = mallocz(sizeof(struct node) * num_nodes);
	fprintf(stderr, "Clearing nodes...");
	for (i=0; i<num_nodes; i++)
		nodes[i].prob = 0.0, nodes[i].u.via = -1;
	fprintf(stderr, "ok\n");

	fprintf(stderr, "Building reverse table." );
	count_conns();
	fprintf(stderr, "(%d nodes) sorting", currnode);
	sort_rtable();
	fprintf(stderr, "\n");

	fprintf(stderr, "Looking for towns..."); 
	mark_town(argv[1], START);
	mark_town(argv[4], END);
	fprintf(stderr, "...");

	fprintf(stderr, "Setting up heap...");
	setupheap();
	fprintf(stderr, "ok\n");

	{
		int h, m;
		if (sscanf(argv[3], "%d:%d", &h, &m)!=2)
			die("Bad time");
		for (i=0; i<num_towns; i++) {
			if (towns[i].flags == END)
				arrival(i, 1.0, h*60+m, -1);
		}
	}
	fprintf(stderr, "ok\n");
	fprintf(stderr, "Searching");
	while (1) {
		int maxnode = findmax();
		if (maxnode == -1) {
			fprintf(stderr, "Connection not found\n");
			exit(1);
		}
		fprintf(stderr, ":");
		/* Find out probability that we can get from maxnode to END */
		departure_conn(maxnode);
	}


	return 0;
}
