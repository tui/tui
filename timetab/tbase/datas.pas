unit datas;
interface
uses crt;

const
    month:array[1..12] of word=(31,28,31,30,31,30,31,31,30,31,30,31);
    dts:array[1..12] of string=('I','II','III','IV','V','VI','VII',
                                'VIII','IX','X','XI','XII');
type
    ptown=^ttown;
    pcon=^tcon;
    tcon=record
 {              train:longint;    }       {akym vlakom}
               adress:longint;
{               dest:ptown;  }            {kam}
{               rempos:longint;   }       {poznamky}
{               depa:longint;    }        {odchod z mesta}
{               arri:longint;    }        {prichod do ineho mesta}
               next:pcon;
         end;

    ttown=record
{                name:string;            meno mesta}
                adress:longint;
                odk:longint;            {odkial sme prisli}
                smer:longint;           {kam ideme}
                index:longint;          {jeho index}
                arrived:longint;        {pouzity vlak (index do conns.txt)}
                finished:boolean;       {je to uz finalny cas ?}
                time:longint;           {fin./ciastkovy vyhl. cas}
                date:longint;           {fin./ciastkovy vyhl.datum}
                pres:byte;              {pocet vykonanych prestupov}
                dist:longint;           {vzdialenost}
                hcon:pcon;              {hlava konexii}
                next:ptown;
          end;

    fbyte=file of byte;

    tconn=record
                spos:longint;
                srem:longint;
                snod:longint;
                stime:longint;
          end;

var
   ht,vt:ptown;
   hc,vc:pcon;
   Ntowns:longint;

procedure stop(x:byte);
procedure townnum;
function loadlong(var g:fbyte):longint;
function ishclear(co:byte):boolean;
function loadvari(var hm:fbyte;var cnter:longint):longint;
function condate(d,m,y:longint):longint;
procedure uncondate(co:longint;var d,m,y:longint);
procedure uncontime(co:longint;var h,m:longint);
function conday(num:longint):longint;
function town(co:longint):ptown;
function townstr(co:longint):string;
procedure inittowns;
procedure donestruct;
procedure maketownsfalse;
procedure loadcons;
procedure addfiles;
procedure loadadresses;

implementation

procedure stop(x:byte);
var
   z:char;
begin
     writeln('Stop ',x);
     z:=readkey;
     while z<>'y' do z:=readkey;
end;

procedure townnum;
var
   fn:boolean;
   q:text;
   xx:char;
begin
     assign(q,'README');
     reset(q);
     fn:=false;
     while not fn do
     begin
          read(q,xx);
          while xx<>'!' do read(q,xx);
          read(q,xx);
          if upcase(xx)='T' then
          begin
               read(q,xx);
               read(q,xx);
               read(q,xx);
               read(q,xx);
               read(q,xx);
               read(q,ntowns);
               fn:=true;
          end;
     end;
     close(q);
end;

function loadlong(var g:fbyte):longint;
var
   x:byte;
   z,y:longint;
begin
     y:=0;
     read(g,x);
     z:=x;
     y:=y+z*256*256*256;
     read(g,x);
     z:=x;
     y:=y+z*256*256;
     read(g,x);
     z:=x;
     y:=y+z*256;
     read(g,x);
     z:=x;
     y:=y+z;
     loadlong:=y;
end;

function ishclear(co:byte):boolean;
begin
     if co<128 then ishclear:=true else ishclear:=false;
end;

function loadvari(var hm:fbyte;var cnter:longint):longint;
var
   k,l:byte;
   qq,pom:longint;
   po:array[1..5] of byte;
begin
          read(hm,po[1]);
          cnter:=cnter+1;
          if not ishclear(po[1]) then
          begin
               read(hm,po[2]);
               cnter:=cnter+1;
               if not ishclear(po[2]) then
               begin
                    read(hm,po[3]);
                    cnter:=cnter+1;
                    if ishclear(po[3]) then
                    begin
                         read(hm,po[4]);
                         cnter:=cnter+1;
                         if ishclear(po[4]) then
                         begin
                              read(hm,po[5]);
                              cnter:=cnter+1;
                              l:=5;
                         end else l:=4;
                    end else l:=3;
               end else l:=2;
          end else l:=1;
          for k:=1 to l-1 do po[k]:=po[k]-128;
          qq:=1;
          pom:=0;
          for k:=l downto 1 do
          begin
               pom:=pom+qq*po[k];
               qq:=qq*128;
          end;
          loadvari:=pom;
end;

function condate(d,m,y:longint):longint;
var
   r,i:longint;
begin
     r:=0;
     for i:=2002 to y do
     begin
          r:=r+365;
          if (i-1) mod 4 =0 then r:=r+1;
     end;
     for i:=2 to m do
     begin
          r:=r+month[i-1];
     end;
     if (m>2) and (y mod 4=0) then r:=r+1;
     r:=r+d;
     condate:=r;
end;

procedure uncondate(co:longint;var d,m,y:longint);
begin
     y:=2001;
     while ((co>365) and (y mod 4<>0)) or ((co>366) and (y mod 4=0)) do
     begin
          co:=co-365;
          if y mod 4 = 0 then co:=co-1;
          y:=y+1;
     end;
     m:=1;
     while (co>month[m]) or ((m=2) and (co=month[m]) and (y mod 4=0)) do
     begin
          co:=co-month[m];
          if (m=2) and (y mod 4 = 0) then co:=co-1;
          m:=m+1;
     end;
     d:=co;
end;

procedure uncontime(co:longint;var h,m:longint);
begin
     h:=co div 60;
     m:=co mod 60;
end;

function conday(num:longint):longint;
var
   pom:longint;
begin
     pom:=num mod 7;
     if pom=0 then pom:=7;
     conday:=pom;
end;

function town(co:longint):ptown;
var
   vvt:ptown;
begin
     vvt:=ht;
     while (vvt<>nil) and (vvt^.index<>co) do
     begin
{          writeln(vvt^.name);}
          vvt:=vvt^.next;
     end;
     if vvt=nil then town:=nil else
     if vvt^.index=co then town:=vvt else town:=nil;
end;

function townstr(co:longint):string;
var
   f:text;
   kde:longint;
   x:char;
   r:string;
begin
     assign(f,'towns.txt');
     reset(f);
     kde:=0;
     if co>1 then
     repeat
           kde:=kde+1;
           repeat
                 read(f,x);
           until x=#10;
     until kde=co-1;
     r:='';
     read(f,x);
     while x<>#10 do
     begin
          r:=r+x;
          read(f,x);
     end;
     townstr:=r;
     close(f);
end;

procedure inittowns;
var
   u:longint;
   f:file of char;
{   r:string;}
   x:char;
begin
     ht:=nil;
     vt:=nil;
     u:=0;
     assign(f,'towns.txt');
     reset(f);
     while not eof(f) do
     begin
          u:=u+1;
{          r:='';}
          read(f,x);
          while (x<>#10) and (not eof(F)) do
          begin
{               r:=r+x;}
               read(f,x);
          end;
          new(vt);
          vt^.next:=ht;
{          vt^.name:=r;}
          vt^.odk:=0;
          vt^.index:=u;
          vt^.finished:=false;
          vt^.time:=maxlongint;
          vt^.date:=maxlongint;
          vt^.pres:=0;
          vt^.adress:=0;
          vt^.dist:=0;
          vt^.hcon:=nil;
          ht:=vt;
     end;
     close(f);
end;

procedure donestruct;
begin
     vt:=ht;
     while vt<>nil do
     begin
          with vt^ do
          begin
               vc:=hc;
               while vc<>nil do
               begin
                    vc:=vc^.next;
                    dispose(hc);
                    hc:=vc;
               end;
          end;
          vt:=vt^.next;
          dispose(ht);
          ht:=vt;
     end;
end;

procedure maketownsfalse;
var
   vvt:ptown;
begin
     vvt:=ht;
     while vvt<>nil do
     begin
          vvt^.finished:=false;
          vvt^.odk:=0;
          vvt^.time:=maxlongint;
          vvt^.date:=maxlongint;
          vvt^.arrived:=0;
          vvt^.pres:=0;
          vvt:=vvt^.next;
     end;
end;

procedure loadcons;
var
   f,h:fbyte;
   r:string;
   d:char;
   g:file of char;
   pom,p2,p3,px,st,pz,pa:longint;
   vvt,zt:ptown;
   inter:boolean;
   vcon:pcon;
   counter,cter,hmm,hzal:longint;
   lastadress:longint;
begin
     assign(f,'conns.dat');
     assign(h,'nodes.dat');
     reset(f);
     reset(h);
     cter:=0;
     lastadress:=maxlongint;
     repeat
          hzal:=filepos(f);
          pz:=loadlong(f);    {conns.txt}

          px:=loadlong(f);     {remarks.txt}

          pa:=loadlong(f);    {nodes.txt}
          seek(h,pa);
          counter:=pa;
          pom:=loadlong(f);    {starting time}

{          writeln(r,' ',pom);}

          maketownsfalse;
          inter:=false;
            p2:=loadvari(h,counter);        {town index}
{            writeln('Check  : ',hzal); stop;}
            zt:=town(p2);
            zt^.finished:=true;
            p3:=loadvari(h,counter);        {rel.arrival}
            pom:=pom+p3;
            p3:=loadvari(h,counter);        {rel.departure}
            pom:=pom+p3;
            st:=pom;
            p3:=loadvari(h,counter);        {flags, what ?}
            p3:=loadvari(h,counter);        {vzdial, najrychl => nanic}

(*               train:longint;           {akym vlakom}
               dest:ptown;              {kam}
               rempos:longint;          {poznamky}
               depa:longint;            {odchod z mesta}
               arri:longint;            {prichod do ineho mesta}
               next:pcon;   *)

          repeat
                p2:=loadvari(h,counter);       {town index}
{                writeln('Check  : ',p2); stop;}
                if (p2>0) and (counter<lastadress) and (p2<=ntowns) and
                   (px>0) and (st>=0) and (st<=1440) then
                begin
                     vvt:=town(p2);
                     with zt^ do
                     begin
                          cter:=cter+1;
                          if cter mod 10000 = 0 then
                          begin
                               write('#');
                          end;
                          new(vcon);
                          vcon^.next:=hcon;
                          hcon:=vcon;
{                          vcon^.train:=pz;
                          vcon^.dest:=vvt;
                          vcon^.rempos:=px;
                          vcon^.depa:=st;}
                          vcon^.adress:=hzal;
                          p3:=loadvari(h,counter);  {rel.arrival}
                          pom:=pom+p3;
{                          vcon^.arri:=pom;}
                          p3:=loadvari(h,counter);  {rel.depart}
                          pom:=pom+p3;
                          st:=pom;
                          p3:=loadvari(h,counter);  {flags}
                          p3:=loadvari(h,counter);  {vzdial}
                     end;
                     zt:=vvt;
                end else inter:=true;
          until (p2=0) or (inter);
          lastadress:=pa;
     until (eof(f));
     maketownsfalse;
     close(h);
     close(f);
end;

procedure addfiles;
var
   f:file of longint;
   add,max:longint;
begin
     max:=maxlongint;
     assign(f,'nodes.add');
     rewrite(f);
     add:=filepos(f);
     vt:=ht;
     while vt<>nil do
     begin
{          writeln('Check  : ',add);stop;}
          vt^.adress:=add;
          vc:=vt^.hcon;
          while vc<>nil do
          begin
{               writeln('Check2 : ',vc^.adress);stop;}
               write(f,vc^.adress);
{               add:=add+4;}
               vc:=vc^.next;
          end;
          write(f,max);
{          add:=add+4;}
          vt:=vt^.next;
          add:=filepos(f);
     end;
     close(f);
     assign(f,'tadress.dat');
     rewrite(f);
     vt:=ht;
     while vt<>nil do
     begin
          write(f,vt^.index);
          write(f,vt^.adress);
          vt:=vt^.next;
     end;
     close(f);
end;

procedure loadadresses;
var
   f:file of longint;
   pom:longint;
   twn:ptown;
begin
     assign(f,'tadress.dat');
     reset(f);
     while not eof(f) do
     begin
          read(f,pom);
          twn:=town(pom);
          read(f,pom);
          twn^.adress:=pom;
     end;
     close(f);
end;

end.