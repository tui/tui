{
copyright 2002 Jan Kubr <jan.kubr@seznam.cz>
disributed under the terms of GNU General Public License version 2
}

type uzel=record
          nazev:longint; {z conns.txt}
          remark,mesto:longint;
          pr:longint; {cas prijezdu}
          od:longint;  {cas odjezdu}
          dist,flag:longint;
          end;

var uzly:array [1..2500] of uzel;
    n:file of byte;
    {f,}c,t,ct:file;
    mesta: array [1..2500] of longint;
    pozice:integer;
    kamin,zin:array [1..2500] of longint;

function cti_long(poradi:longint;var f:file):longint;
var x:array [1..4] of longint;
    b:byte;
    i:integer;
begin
dec(poradi);
if poradi<>0 then seek(f,4*poradi);
for i:=1 to 4 do
    begin
    if eof(f) then exit
    else blockread(f,b,1);
    x[i]:=b
    end;
cti_long:=x[1]*$1000000+x[2]*$10000+x[3]*$100+x[4]
end;


function cti_vari:longint;
var i:byte;
    pom:longint;
begin
pom:=0;
read(n,i);
while (i or $7f=$ff) do
      begin
      i:=i and $7f;
      pom:=pom shl 7;
      pom:=pom or i;
      read(n,i);
      end;
pom:=pom shl 7;
pom:=pom or i;
cti_vari:=pom
end;


procedure tiskni_mesto(c:byte;index:longint;var f:file);
var b:byte;
begin
seek(f,index);
blockread(f,b,1);
repeat
    write(chr(b));
    blockread(f,b,1);
until b=c;
write(chr(10));
end;

procedure tiskni_info;
begin
writeln('Program expects tb file structure in . directory.');
writeln('usage:');
Writeln(' -l:');
writeln('Lists all stations.');
writeln;
writeln(' -L:');
writeln('Lists all connection names.');
writeln;
writeln(' -c string:');
writeln('Finds all connections which name contains given string.');
writeln;
writeln(' -s station:');
writeln('Finds all stations reachable by direct connection from given station.');
writeln;
writeln(' -a station time1 time2 string:');
writeln('Finds all connections which name contains given string going from station to anywhere at <time1,time2>');
writeln;
writeln(' -a station:');
writeln('Means -a station any any any (see the comment down there).');
writeln;
writeln(' -b station1 time1 time2 string station2:');
writeln('Finds all connections which name contains given string going from station1 to station2 at <time1,time2>.');
writeln;
writeln(' -b station1 station2:');
writeln('Means -b station1 any any any station2.');
writeln;
write('You can use the word any instead of times or string, which means time1 = 00:00, time2 = 23:59 and no other filters for connections. You will have to use apostrophes if the station is longer than one word. Program searches for exact match of station.'); 
writeln(' Use station+ to search for all stations beginning with given string.');
writeln;
writeln('Program writes connections in tt format using standard output.');
writeln;
halt
end;

procedure vstup(var z,kam,spojeni:string; var cas1,cas2:longint);
var pom:string;
    f:file of char;
    c:char;

procedure hodiny(var cas:longint;var pom:string);
{prevadi string casu na cislo}
begin
if (ord(pom[0])>5) or (ord(pom[0])<3) then
      begin
	 writeln('xx:xx nebo x:xx');
	 halt;
      end;
         if pom[2]=':' then
         begin  {pripad x:xx}
         cas:=(ord(pom[1]) and $CF)*60;
         cas:=cas+(ord(pom[3]) and $CF)*10+(ord(pom[4]) and $CF)
         end
                    else
         begin  {pripad xx:xx}
         cas:=((ord(pom[1]) and $CF)*10+(ord(pom[2]) and $CF))*60;
         cas:=cas+(ord(pom[4]) and $CF)*10+(ord(pom[5]) and $CF)
         end;
end;

begin
if (paramcount=0) or (paramcount>6) then
   tiskni_info;
if not((paramstr(1)='-a') or (paramstr(1)='-b') or (paramstr(1)='-c') or (paramstr(1)='-s') or (paramstr(1)='-l') or (paramstr(1)='-L')) then
    tiskni_info;
if (paramstr(1)='-l') or (paramstr(1)='-L') then
   begin
   if paramstr(1)='-l' then
      assign(f,'towns.txt')
                       else
      assign(f,'conns.txt');
   reset(f);
   repeat
         if eof(f) then
		halt;
         read(f,c);
         write(c);
   until false;
   close(f)
   end;


if paramstr(1)='-c' then
   begin
   if paramcount<>2 then
      tiskni_info;
   z:='any';
   kam:='any';
   cas1:=0;
   cas2:=23*60+59;
   spojeni:=paramstr(2);
   exit
   end;

z:=paramstr(2);
if ((paramstr(1)='-a') and (paramcount=2)) or ((paramstr(1)='-b') and (paramcount=3)) or (paramstr(1)='-s') then
    begin
    spojeni:='any';
    cas1:=0;
    cas2:=23*60+59;
    if (paramstr(1)='-a') then
	begin
        kam:='any';
        exit
        end;
    if paramstr(1)='-s' then
	begin
        kam:='sny';
        pozice:=1;
        exit
        end;
    kam:=paramstr(3);
    exit;
    end;

spojeni:=paramstr(5);
pom:=paramstr(4);
if pom='any' then
     cas2:=23*60+59
             else
     hodiny(cas2,pom);
pom:=paramstr(3);
if pom='any' then
    cas1:=0
             else
    hodiny(cas1,pom);
if paramstr(1)='-a' then
kam:='any'
                    else
if (paramcount=2) then
    kam:=paramstr(2)
                  else
    kam:=paramstr(6);
end;

procedure index_mesta(var mesto:string);
var t:file;
    c:byte;
    i,j:integer;
    neshoda:boolean;
    pom:longint;
begin
assign(t,'towns.txt');
reset(t,1);
neshoda:=true;
if (mesto='any') or (mesto='') then
   begin
   kamin[1]:=-2;
   exit
   end;
if mesto='sny' then
   begin
   kamin[1]:=-3;
   exit
   end;
i:=1;
j:=1;
repeat
      pom:=filepos(t);
      blockread(t,c,1);
      while ((chr(c)=mesto[j]) and (j<=ord(mesto[0]))) do
            begin
            blockread(t,c,1);
            inc(j);
            end; 
     if j>=ord(mesto[0]) then 
         if ((c=$0A) or (mesto[ord(mesto[0])]='+')) then
            begin           
            neshoda:=false;
            kamin[i]:=pom; 
            inc(i);
           end; 
       while c<>$0A do
           blockread(t,c,1);
       j:=1
until eof(t);
close(t);
end;



procedure tiskni_s;
var i:integer;
    tt:file;
begin
i:=pozice;
assign(tt,'towns.txt');
reset(tt,1);
while mesta[i]<>-5 do
      begin
      tiskni_mesto($0A,mesta[i],tt);
      inc(i);
      end;
pozice:=i;
close(tt)
end;

procedure tiskni;
var i:integer;
    a:longint;
    {ft,}r,tt:file;
begin
assign(r,'remarks.txt');
assign(tt,'towns.txt');
{assign(ft,'flags.txt');
reset(ft,1);}
reset(r,1);
reset(tt,1);
write('# ');
tiskni_mesto($0A,uzly[1].nazev,ct);
write((uzly[1].pr div 60) mod 24);
write(':');
a:=uzly[1].pr mod 60;
if a<10 then
   write('0',a)
        else
   write(a);
write(chr(9),'+');
write(uzly[1].od);
{write(chr(9));
tiskni_mesto(($0A,spoje[k][1].flag,ft);}
write(chr(9));
write('0');
write(chr(9));
tiskni_mesto($0A,uzly[1].mesto,tt);
i:=1;
repeat
      begin
      inc(i);
      write('+',uzly[i].pr);
      write(chr(9),'+');
      write(uzly[i].od);
      {write(chr(9));
      tiskni_mesto($0A,spoje[k][i].flag,ft);}
      write(chr(9),'+');
      write(uzly[i].dist);
      write(chr(9));
      tiskni_mesto($0A,uzly[i].mesto,tt);
      end;
until (uzly[i].remark=-1);
tiskni_mesto(0,uzly[1].remark,r);
close(tt);
close(r);
{close(ft)}
end;

procedure spoje(zin,kamin,cas1,cas2:longint;spojeni:string);

function porovnani(index:longint;spojeni:string):boolean;
{porovna string "spojeni" se zaznamem v conns.txt}
var i:integer;
    pom:string;
begin
if (spojeni='any') or (spojeni='') then
   begin
     porovnani:=true;
     exit
   end;
seek(ct,index);
i:=1;
porovnani:=false;
pom[0]:=chr(0);
repeat
      inc(ord(pom[0]));
      blockread(ct,pom[i],1);
      inc(i);
until pom[i-1]=chr(10);
If pos(spojeni,pom)<>0 then
   porovnani:=true;
end;

procedure prohledej(cas1,cas2,remark,index,time,node:longint);
{prohledava konkretni spojeni}
var i:integer;
    priznak:boolean;
    j,z,town:longint;
begin
seek(n,node);
town:=cti_vari;
z:=cti_long(town,t);
while (zin<>z) and (zin<>-2) do
    begin
    time:=time+cti_vari+cti_vari;
    cti_vari;
    cti_vari;
    town:=cti_vari;
    if town=0 then
        exit;
    z:=cti_long(town,t);
    end;
uzly[1].mesto:=z;
uzly[1].remark:=remark;
uzly[1].nazev:=index;
uzly[1].pr:=time+cti_vari;
uzly[1].od:=cti_vari;
time:=(uzly[1].pr+uzly[1].od) mod 1440;
if (time<>$200) then
   if (time<cas1) or (time>cas2) then
      exit;
uzly[1].flag:=cti_vari;
uzly[1].dist:=0;
cti_vari;
i:=2;
priznak:=false;
town:=cti_vari;
if town=0 then
   exit;
repeat
      z:=cti_long(town,t);
      uzly[i].mesto:=z;
      uzly[i].od:=cti_vari;
      uzly[i].pr:=cti_vari;
      uzly[i].flag:=cti_vari;
      uzly[i].dist:=cti_vari;
      uzly[i].remark:=0;
      if z=kamin then
         begin
         priznak:=true;
         break;
         end;
      town:=cti_vari;
      if town=0 then
         break;
      inc(i);
until false;
uzly[i].remark:=-1;
if priznak or (kamin=-2) then
   tiskni;
if kamin=-3 then {pro -s}
   begin
   i:=2;
   j:=0;
   repeat {pro kazdy mesto se diva, jestli ho uz netiskl a uklada, pokud ne}
         repeat
               inc(j);
         until (uzly[i].mesto=mesta[j]) or (mesta[j]=-5);
   if mesta[j]=-5 then
      mesta[j]:=uzly[i].mesto;
   j:=0;
   inc(i);
   until(uzly[i-1].remark=-1);
   tiskni_s;
   end
end;


var i,time,index,node,remark:longint;
begin {spoje}
i:=1;
repeat
      index:=cti_long(i,c); {index ukazuje do conns.txt - nazev spojeni}
      if eof(c) then
         exit;
      if porovnani(index,spojeni) then
         begin
         time:=cti_long(i+3,c);
         remark:=cti_long(i+1,c);
         node:=cti_long(i+2,c);
         prohledej(cas1,cas2,remark,index,time,node);
         end;
      i:=i+4;
until false;
end;



var z,kam,spojeni:string;
    cas1,cas2:longint;
    i,k:longint;
begin{main}
vstup(z,kam,spojeni,cas1,cas2);
for i:=1 to 2500 do
   kamin[i]:=-1;
index_mesta(z);
if kamin[1]=-1 then
   begin
   writeln('Cannot find station1.');
   exit
   end;
i:=1;
while kamin[i]<>-1 do
   begin
   zin[i]:=kamin[i];
   kamin[i]:=-1;
   inc(i);
   end;
for k:=i to 2500 do
   zin[i]:=-1;
index_mesta(kam);
if kamin[1]=-1 then
   begin
   writeln('Cannot find station2.');
   exit
   end;
if paramstr(1)='-s' then
   for k:=1 to 2500 do
             mesta[k]:=-5;
assign(c,'conns.dat');
assign(n,'nodes.dat');
assign(t,'towns.dat');
assign(ct,'conns.txt');
{assign(f,'flags.dat');
reset(f,1);}
reset(ct,1);
reset(c,1);
reset(n);
reset(t,1);
i:=1;
k:=1;
while zin[i]<>-1 do
    begin
      while kamin[k]<>-1 do
         begin
         if (zin[i]<>kamin[k]) or (zin[i]=-2) then
                begin 
		spoje(zin[i],kamin[k],cas1,cas2,spojeni);
                {reset(f,1);}
                reset(ct,1);
                reset(c,1);
                reset(n);
                reset(t,1);
                end;
         inc(k);
         end; 
    inc(i); 
    k:=1;
    end;
close(n);
close(c);
close(t);
close(ct);
{close(f)}
end.
