{
	Copyright 2001 A. Osusky <flat@email.cz>

	Distribute under GPL version 2 or later
}

uses datas, exists3, halda;

type
    Pspoj=^tspoj;
    Tspoj=record
                aky:longint;    {mesto}
                typ:boolean;    {odkial=true; kam=false}
                next:pspoj;
          end;
    Tconn=record
                spos:longint;
                srem:longint;
                snod:longint;
                stime:longint;
          end;
    Tnod=record
               index:longint;
               rarri:longint;
               rdepa:longint;
               dist:longint;
         end;

var
   spec:longint;
   kamze:pcon;
   conn:tconn;
   q:fbyte;
   ccc:char;
   hsp,vsp:pspoj;
   globalstop:boolean;


{nacita spojenie}
procedure loadcon(where:longint;var d:tconn);
var
   j:byte;
   x:longint;
begin
     seek(connsbyte,where);
     d.spos:=loadlong(connsbyte);
     d.srem:=loadlong(connsbyte);
     d.snod:=loadlong(connsbyte);
     d.stime:=loadlong(connsbyte);
end;

{nacita jednu cestu medzi dvomi stanicami; INPUT=spoj,mesto OUTPUT=ciel,cas odchodu/prichodu}
{v podstate xx je mesto startovne, yy koncove}
procedure loadnod(ind:longint;where:longint;var xx,yy:tnod);
var
   cnt,z:longint;
   pom:tnod;
begin
     cnt:=0;
     z:=0;
     seek(nodesdatbyte,where);
     pom.index:=loadvari(nodesdatbyte,cnt);
     pom.rarri:=loadvari(nodesdatbyte,cnt)+z;
     z:=pom.rarri;
     pom.rdepa:=loadvari(nodesdatbyte,cnt)+z;
     z:=pom.rdepa;
     pom.dist:=loadvari(nodesdatbyte,cnt);
     pom.dist:=loadvari(nodesdatbyte,cnt);
     while pom.index<>ind do
     begin
          pom.index:=loadvari(nodesdatbyte,cnt);
          pom.rarri:=loadvari(nodesdatbyte,cnt)+z;
          z:=pom.rarri;
          pom.rdepa:=loadvari(nodesdatbyte,cnt)+z;
          z:=pom.rdepa;
          pom.dist:=loadvari(nodesdatbyte,cnt);
          pom.dist:=loadvari(nodesdatbyte,cnt);
     end;
     xx:=pom;
     yy.index:=loadvari(nodesdatbyte,cnt);
     if yy.index<>0 then
     begin
          yy.rarri:=loadvari(nodesdatbyte,cnt)+z;
          z:=yy.rarri;
          yy.rdepa:=loadvari(nodesdatbyte,cnt)+z;
          yy.dist:=loadvari(nodesdatbyte,cnt);
          yy.dist:=loadvari(nodesdatbyte,cnt);
     end else
     {tuto by mal byt opraveny runtime error 100
      tj cital dalej aj ked je uz koniec spoju, tj index mesta=0}
     begin
          yy.rarri:=0;
          yy.rdepa:=0;
          yy.dist:=0;
          yy.dist:=0;
     end;
end;

{dekoduje poznamku, tj mozem ist v tento den vlakom ?
 pozn. existuju vlaky, ktore "nemozu" ist cely rok}
function cani(date,pos:longint):boolean;
var
   wn,wd,i:longint;
   x:char;
   r,jd,nd:string;
   d,m,y:longint;
   kto:boolean;
   pozn:tpoznamka;
begin
     seek(remchar,0);
     if pos>0 then
     begin
          seek(remchar,pos-1);
          read(remchar,x);
          while x<>#10 do
          begin
               pos:=pos-1;
               seek(remchar,pos-1);
               read(remchar,x);
          end;
     end;
     seek(remchar,pos);
     r:='';
     read(remchar,x);
     while x<>#10 do
     begin
          r:=r+x;
          read(remchar,x);
     end;
{     writeln(r);}
     if r='(null)' then
     begin
          cani:=true;
     end else
     begin
          wn:=date;
          wd:=conday(wn);
          uncondate(date,d,m,y);
          str(d,nd);
          jd:=nd+'.';
          str(m,nd);
          jd:=jd+nd+'.';
          str(y,nd);
          jd:=jd+nd+#9;
          r:=jd+r;
          x:=jede(r);
          if (x='Y') or (x='U') then cani:=true else cani:=false;
     end;
end;

{nastavi podmienky, tj dekoduje paramtere volaneho programu}
procedure setparams;
var
   a,b,c:longint;
   code:integer;
   z,z2:byte;
   sprac,r:string;
   ako,tims:boolean;
   pom:byte;
   pacnt:word;
begin
     pacnt:=paramcount;
     with opt do
     begin

     dir:='';
     help:=false;
     t1:='';
     t2:='';
     stime:=0;
     date:=0;
     typst:=0;
     pros:=false;
     force:=false;
     tims:=false;
     ako:=false;
     maxsw:=6;
     minst:=10;
     for z:=1 to pacnt do
     begin
          sprac:=paramstr(z);
          while (sprac[1]='"') and (sprac[length(sprac)]<>'"') do
          begin
               z:=z+1;
               sprac:=sprac+' '+paramstr(z);
          end;
          if sprac[2]='!' then opt.force:=true else
          if sprac[1]='"' then if not ako then
          begin
               t1:=copy(sprac,2,length(sprac)-2);
               ako:=not ako;
          end else t2:=copy(sprac,2,length(sprac)-2);
          if sprac='-i' then pros:=true else
          if sprac[1]='-' then
          begin
               if sprac[2]='s' then
               begin
                    sprac:=copy(sprac,3,length(sprac)-2);
                    val(sprac,maxsw,code);
               end else
               if sprac[2]='t' then
               begin
                    sprac:=copy(sprac,3,length(sprac)-2);
                    val(sprac,minst,code);
               end else
               if sprac[2]='?' then
               begin
                    help:=true;
               end else
               if sprac[2]='d' then dir:=copy(sprac,3,length(sprac)-2);
          end else if sprac[1]='#' then typst:=z;
          pom:=0;
          for z2:=1 to length(sprac) do if sprac[z2]='.' then pom:=1 else
                                        if sprac[z2]=':' then pom:=2;
          if (pom=1) and (not tims) then
          begin
               tims:=true;
               z2:=0;
               repeat
                     z2:=z2+1;
               until sprac[z2]='.';
               val(copy(sprac,1,z2-1),a,code);
               c:=z2;
               repeat
                     z2:=z2+1;
               until sprac[z2]='.';
               val(copy(sprac,c+1,z2-c-1),b,code);
               val(copy(sprac,z2+1,length(sprac)-z2),c,code);
               date:=condate(a,b,c);
          end else
          if pom=2 then
          begin
               z2:=0;
               repeat
                     z2:=z2+1;
               until sprac[z2]=':';
               val(copy(sprac,1,z2-1),a,code);
               val(copy(sprac,z2+1,length(sprac)-z2),b,code);
               stime:=a*60+b;
          end;
     end;

     end;
end;

{               aky:longint;    mesto
                typ:boolean;    odkial=true; kam=false
                next:pspoj;                             }

{utvori si zoznam startovnych a cielovych miest
 lebo "+" umoznuje viac volieb}
procedure expandstropt;
var
   z,z2,zz:byte;
   y,y2:byte;
   f:text;
   mesto,cast:string;
   pocet:longint;
   passed,pass2:boolean;
function readlnstr:string;
var
   pom:string;
   x:char;
begin
     pom:='';
     read(f,x);
     while x<>#10 do
     begin
          pom:=pom+x;
          read(f,x);
     end;
     readlnstr:=pom;
end;
begin
     hsp:=nil;
     pocet:=0;
     assign(f,opt.dir+'towns.txt');
     reset(f);
     while not eof(f) do
     begin
          mesto:=readlnstr;
          pocet:=pocet+1;
          z:=1;
          z2:=0;
          y:=1;
          y2:=0;
          passed:=true;
          pass2:=true;
          repeat
                if opt.t1[1]='+' then z2:=z2+1;
                repeat
                      z2:=z2+1;
                until (opt.t1[z2]='+') or (z2=length(opt.t1));
                if (opt.t1[z]<>'+') and (opt.t1[z2]<>'+') then
                begin
                     if mesto<>opt.t1 then passed:=false;
                end else
                if opt.t1[z]<>'+' then
                begin
                     if copy(mesto,1,z2-1)<>copy(opt.t1,1,z2-1) then
                        passed:=false;
                end else
                if opt.t1[z2]<>'+' then
                begin
                     if copy(mesto,length(mesto)-z2+z+1,z2-z)<>copy(opt.t1,z+1,z2-z) then
                        passed:=false;
                end else
                begin
                     if length(mesto)<z2-z+1 then passed:=false else  {znak musi byt aj pred aj za}
                     begin
                          passed:=false;
                          for zz:=2 to length(mesto)-z2+z+1 do
                             if copy(mesto,zz,z2-z-1)=copy(opt.t1,z+1,z2-z-1) then passed:=true;
                     end;
                end;
                z:=z2;
          until z2=length(opt.t1);

          repeat
                if opt.t2[1]='+' then y2:=y2+1;
                repeat
                      y2:=y2+1;
                until (opt.t2[y2]='+') or (y2=length(opt.t2));
                if (opt.t2[y]<>'+') and (opt.t2[y2]<>'+') then
                begin
                     if mesto<>opt.t2 then pass2:=false;
                end else
                if opt.t2[y]<>'+' then
                begin
                     if copy(mesto,1,y2-1)<>copy(opt.t2,1,y2-1) then
                        pass2:=false;
                end else
                if opt.t2[y2]<>'+' then
                begin
                     if copy(mesto,length(mesto)-y2+y+1,y2-y)<>copy(opt.t2,y+1,y2-y) then
                        pass2:=false;
                end else
                begin
                     if length(mesto)<y2-y+1 then pass2:=false else  {znak musi byt aj pred aj za}
                     begin
                          pass2:=false;
                          for zz:=2 to length(mesto)-y2+y+1 do
                             if copy(mesto,zz,y2-y-1)=copy(opt.t2,y+1,y2-y-1) then pass2:=true;
                     end;
                end;
                y:=y2;
          until y2=length(opt.t2);

          if passed then
          begin
               new(vsp);
               vsp^.next:=hsp;
               hsp:=vsp;
               vsp^.aky:=pocet;
               vsp^.typ:=true;
          end;
          if pass2 then
          begin
               new(vsp);
               vsp^.next:=hsp;
               hsp:=vsp;
               vsp^.aky:=pocet;
               vsp^.typ:=false;
          end;
     end;
     close(f);
end;

{zrusi strukturu spojou vytvorenych pomocou expandstropt}
procedure donespoj;
begin
     vsp:=hsp;
     while vsp<>nil do
     begin
          vsp:=vsp^.next;
          dispose(hsp);
          hsp:=vsp;
     end;
end;

{mozem ukoncit prehladavanie ?}
function evok:boolean;
var
   pom:ptown;
begin
     evok:=true;
     vsp:=hsp;
     while vsp<>nil do
     begin
          if not vsp^.typ then
          begin
               pom:=town(vsp^.aky,hh);
               if not pom^.finished then evok:=false;
          end;
          vsp:=vsp^.next;
     end;
end;

(*    tcon=record
               train:longint;           {akym vlakom}
               dest:ptown;              {kam}
               rempos:longint;          {poznamky}
               depa:longint;            {odchod z mesta}
               arri:longint;            {prichod do ineho mesta}
               next:pcon;
         end;
                odk:longint;            {odkial sme prisli}
                index:longint;          {jeho index}
                arrived:longint;        {pouzity vlak (index do conns.txt)}
                finished:boolean;       {je to uz finalny cas ?}
                time:longint;           {fin./ciastkovy vyhl. cas}
                date:longint;           {fin./ciastkovy vyhl.datum}
                pres:byte;              {pocet vykonanych prestupov}
                hcon:pcon;              {hlava konexii}
                next:ptown;
*)

{ked hladame vlaky vzdy si zapisujem odkial som prisiel ale nie kam idem
 toto tu doplni potrebne udaje aby sme vedeli aj kam ideme}
procedure invertstructure(src,desti:longint);
var
   xx,yy,zz:ptown;
begin
     xx:=town(src,hh);
     yy:=town(desti,hh);
     while xx<>yy do
     begin
          zz:=town(yy^.odk,hh);
          zz^.smer:=yy^.index;
          yy:=zz;
     end;
end;

{zisti nazov vlaku}
function puttrainstr(adr:longint):string;
var
   cn:tconn;
   x:char;
   r:string;
begin
     loadcon(adr,cn);
     seek(connschar,cn.spos);
     r:='';
     read(connschar,x);
     while x<>#10 do
     begin
          r:=r+x;
          read(connschar,x);
     end;
     puttrainstr:=r;
end;

{napise pozadovanu cestu}
procedure writespojs(source:ptown);
var
   pxp:pspoj;
   trav,fin,pom:ptown;
   ontrain:longint;
   pomcon:tconn;
   pa,pb:tnod;
   zac,kon:longint;
   h,m:longint;
begin
     pxp:=hsp;
     while pxp<>nil do
     begin
          if not pxp^.typ then
          begin
{               writeln(townstr(pxp^.aky));}
               invertstructure(source^.index,pxp^.aky);
               fin:=town(pxp^.aky,hh);
               if fin^.finished then
               begin

               trav:=source;
               ontrain:=0;
               while trav<>fin do
               begin
                    pom:=town(trav^.smer,hh);
                    loadcon(pom^.arrived,pomcon);
                    loadnod(trav^.index,pomcon.snod,pa,pb);
                    if (ontrain<>pom^.arrived) and (pb.index>0) then
                    begin

                    if ontrain<>0 then
                    begin
                      zac:=trav^.time;
                      kon:=trav^.time+(pa.rdepa-pa.rarri);
                      uncontime(zac,h,m);
                      write('     ');
                      if h<10 then write('0');
                      write(h,':');
                      if m<10 then write('0');
                      write(m);
                      uncontime(kon,h,m);
                      write('     ');
                      if h<10 then write('0');
                      write(h,':');
                      if m<10 then write('0');
                      write(m);

                      write('     ');
                      writeln(townstr(trav^.index));
                    end;


                         zac:=trav^.time;
                         kon:=trav^.time+(pa.rdepa-pa.rarri);
                         writeln;
                         writeln('# ',puttrainstr(pom^.arrived));
                         ontrain:=pom^.arrived;
                    end else
                    begin
                         zac:=trav^.time;
                         kon:=trav^.time+(pa.rdepa-pa.rarri);
                    end;

                    uncontime(zac,h,m);
                    write('     ');
                    if h<10 then write('0');
                    write(h,':');
                    if m<10 then write('0');
                    write(m);
                    uncontime(kon,h,m);
                    write('     ');
                    if h<10 then write('0');
                    write(h,':');
                    if m<10 then write('0');
                    write(m);

                    write('     ');
                    writeln(townstr(trav^.index));
                    trav:=pom;
               end;
               zac:=fin^.time;
               kon:=zac;
               uncontime(zac,h,m);
               write('     ');
               if h<10 then write('0');
               write(h,':');
               if m<10 then write('0');
               write(m);
               uncontime(kon,h,m);
               write('     ');
               if h<10 then write('0');
               write(h,':');
               if m<10 then write('0');
               write(m);

               write('     ');
               writeln(townstr(trav^.index));
               writeln;
               writeln('----------------------------------------');

               end;  {fin finished}
          end;
          pxp:=pxp^.next;
     end;
end;

(*    tcon=record
               adress:longint;
               next:pcon;
         end;

    ttown=record
                adress:longint;
                odk:longint;            {odkial sme prisli}
                index:longint;          {jeho index}
                arrived:longint;        {pouzity vlak (index do conns.txt)}
                finished:boolean;       {je to uz finalny cas ?}
                time:longint;           {fin./ciastkovy vyhl. cas}
                date:longint;           {fin./ciastkovy vyhl.datum}
                pres:byte;              {pocet vykonanych prestupov}
                hcon:pcon;              {hlava konexii}
                next:ptown;
          end;
    Tspoj=record
                aky:longint;    {mesto}
                typ:boolean;    {odkial=true; kam=false}
                next:pspoj;
          end;*)

{mozem pouzit dany spoj ?}
function mayiuse(where:longint):boolean;
var
   x:char;
   r,r2:string;
   q,q2:byte;
   mozem:boolean;
begin
     seek(connschar,where);
     r:='';
     read(connschar,x);
     while x<>#10 do
     begin
          r:=r+x;
          read(connschar,x);
     end;
     mozem:=true;

     q:=opt.typst-1;
     q2:=paramcount;
     while (mozem) and (q<q2) do
     begin
          q:=q+1;
          r2:=paramstr(q);
          r2:=copy(r2,2,length(r2)-1);
          if copy(r,1,length(r2))=r2 then mozem:=false;
     end;
     mayiuse:=mozem;
end;

{hladajme spoje}
procedure search;
var
   searched:longint;
   plus:longint;
   psp:pspoj;
   src,kam:ptown;
   den,zalden:longint;
   fx:file of longint;
   adr:longint;
   conn:tconn;
   zal:ptown;
   casprich:longint;
   mincas,minden:longint;
   an,bn:tnod;
   posun:boolean;
begin
     psp:=hsp;
     while psp<>nil do
     begin
          if psp^.typ then
          begin
               maketownsfalse(hh);
               src:=town(psp^.aky,hh);
               src^.odk:=0;
               src^.arrived:=0;
               src^.finished:=true;
               src^.time:=opt.stime;
               src^.date:=opt.date;
               src^.pres:=0;
               bubbledspec(src);
               den:=src^.date;
               zalden:=den;
               posun:=false;
               searched:=1;
               while (not evok) and (src<>nil) do
               begin
                    if opt.pros then writeln(searched,' Search : ',townstr(src^.index),' ',src^.time,' ',src^.date);
                    seek(nodesaddlong,src^.adress);
                    read(nodesaddlong,adr);
                    while adr<>lowlongint do
                    begin
                         if (adr=src^.arrived) or (src^.arrived=0) or
                            ((src^.arrived<>adr) and (src^.pres<opt.maxsw)) then
                         begin
                              loadcon(adr,conn);
                              plus:=0;
                              while (not cani(den,conn.srem)) and (plus<=4) do
                              begin
                                   den:=den+1;
                                   plus:=plus+1;
                                   posun:=true;
                              end;
                              if (plus<=4) and (mayiuse(conn.spos)) then
                              begin
                                   loadnod(src^.index,conn.snod,an,bn);
                                   kam:=town(bn.index,hh);
                                   if not posun then
                                     if (adr<>src^.arrived) and (src^.arrived<>0) then
                                     begin
                                          if conn.stime+an.rdepa<src^.time+opt.minst then
                                          begin
                                               den:=den+1;
                                               posun:=true;
                                          end;
                                     end else
                                     if conn.stime+an.rdepa<src^.time then
                                     begin
                                          den:=den+1;
                                          posun:=true;
                                     end;
                                   casprich:=conn.stime+an.rdepa+(bn.rarri-an.rdepa);
                                   while casprich>=1440 do
                                   begin
                                        casprich:=casprich-1440;
                                        den:=den+1;
                                   end;
                                   if (casprich>=0) and (casprich<1440) then
                                   if ((den<kam^.date) or ((den=kam^.date) and (casprich<kam^.time)))
                                     and (not kam^.finished) then
                                   begin
                                        if (src^.arrived=0) or (adr=src^.arrived) then kam^.pres:=src^.pres else
                                           kam^.pres:=src^.pres+1;
                                        kam^.odk:=src^.index;
                                        kam^.arrived:=adr;
                                        kam^.date:=den;
                                        kam^.time:=casprich;
                                        bubble(kam);
                                   end;
                              end;
                         end;
                         read(nodesaddlong,adr);
                         den:=zalden;
                         posun:=false;
                    end;
                    mincas:=maxlongint;
                    minden:=maxlongint;
                    kam:=hh;
                    src:=kam;
                    if src<>nil then
                    begin
                         src^.finished:=true;
                         bubbledspec(src);
                         den:=src^.date;
                         zalden:=den;
                         searched:=searched+1;
                    end;
               end;
               writespojs(town(psp^.aky,hh));
          end;
          psp:=psp^.next;
     end;
end;

{otvor casto vyuzivane subory}
procedure openall;
begin
     assign(connsbyte,opt.dir+'conns.dat');
     reset(connsbyte);
     assign(nodesdatbyte,opt.dir+'nodes.dat');
     reset(nodesdatbyte);
     assign(remchar,opt.dir+'remarks.txt');
     reset(remchar);
     assign(connschar,opt.dir+'conns.txt');
     reset(connschar);
     if not opt.force then
     begin
          assign(nodesaddlong,opt.dir+'nodes.add');
          reset(nodesaddlong);
     end;
end;

{a na konci ich zavri}
procedure closeall;
begin
     close(connsbyte);
     close(nodesdatbyte);
     close(remchar);
     close(connschar);
     if not opt.force then close(nodesaddlong);
end;

begin
     globalstop:=false;
     setparams;
     if opt.help then
     begin
          writeln;
          writeln;
          writeln('Format :');
          writeln('tr "twn1" "twn2" time date [-!] [-i] [#Os | #Sp_123...] [-sNUM] [-tNUM] [-?]');
          writeln('   [-dSTRING]');
          writeln;
          writeln('twn1, twn2        must be strings');
          writeln('                  may contain "+" at the end which represent ANY string');
          writeln('time              must be in format hh:mm (must be separated by colon)');
          writeln('                       hh - hours');
          writeln('                       mm - minutes');
          writeln('date              must be in format dd:mm:yyyy (must be separated by colons)');
          writeln('                       dd   - day');
          writeln('                       mm   - month');
          writeln('                       yyyy - year');
          writeln('-!                negates all options and upgrades database');
          writeln('                  (adds "nodes.add" & "Tadress.dat" neccessary for searching)');
          writeln('-i                switches the writening of searching progress on');
          writeln('#Os | #Sp_123...  specification of train restrictions');
          writeln('                  must be the last options (!)');
          writeln('                  cannot start with character belonging to other options');
          writeln('-sNUM             NUM specifies the maximum number of train switches');
          writeln('-tNUM             NUM specifies the minimum time for train switching');
          writeln('-?                negates all options (including "-!") and writes this help');
          writeln('-dSTRING          STRING is an absolute path to dir where the database is');
          writeln('                  finishing with "\"');
          writeln('                  also must be specified after the starting time is known');
          writeln('Default options :');
          writeln('-! off    -i off   -? off   -w off');
{mest1,mest2,upgrade,vlaky,maxprestupy,mincasnaprestup}
     end else
     if opt.force then
     begin
          openall;
          inittowns;
          loadcons;             {upgrade dbase}
          addfiles;
          closeall;
     end else
     begin
          openall;
          inittowns;
          expandstropt;
          loadadresses;
          search;
          donespoj;
          closeall;
     end;
     donestruct;
end.