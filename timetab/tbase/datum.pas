unit datum;

interface
  type Tdatum=record
         y,m,d,w:integer;
       end;
       Tkratkedatum=record
         m,d:integer;
       end;
       Tcas=integer;
       Tcasstr=string;
       Tdatumstr=string[20];
       Trimskecislo=string[4];
  const cas_prazdny='     ';
        rimske:array [1..12] of Trimskecislo = ('I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
        rimskecislice=['I','V','X'];
        novyrok:Tkratkedatum=(m:1;d:1);
        silvestr:Tkratkedatum=(m:12;d:31);
        stddatum:Tdatum=(y:2000;m:1;d:1;w:6);

function prvni(oddelovac:char;var s:string):string;
function posledni(oddelovac:char;var s:string):string;
function cas_casstr(cas:Tcas):Tcasstr;
function casstr_cas(casstr:Tcasstr):Tcas;
function pocdnu(m,y:integer):integer;
function platnekratkedatum(d:Tkratkedatum):boolean;
function datum_datumstr(d:Tdatum;tyden:integer):Tdatumstr;
function datumstr_datum(s:Tdatumstr;var vystup:Tdatum):boolean;
function prectimesic(var s:string;del:boolean):integer;
function rozdil(d1,d2:Tdatum):longint;
procedure denvtydnu(var d:Tdatum;vychozi:Tdatum);
function cislotydne(d:Tdatum):integer;
procedure opravdatum(vstup:Tdatum;cas:Tcas;var d:Tdatum);
function porovnejdatum(d1,d2:Tkratkedatum):integer; {navrat: 0:d1=d2, 1:d1<d2, 2:d2>d1}

IMPLEMENTATION

var mesice:array [1..12] of integer;

function prvni(oddelovac:char;var s:string):string;
  var i:integer;
begin
  i:=0;
  while i<length(s) do
    if s[i+1]=oddelovac then
      break
    else
      inc(i);
  prvni:=copy(s,1,i);
  delete(s,1,i);
  if s<>'' then
    if s[1]=oddelovac then
      delete(s,1,1);
end;

function posledni(oddelovac:char;var s:string):string;
  var i:integer;
begin
  i:=length(s);
  while i>1 do
    if s[i-1]=oddelovac then
      break
    else
      dec(i);
  posledni:=copy(s,i,length(s)-i+1);
  delete(s,i,length(s)-i+1);
  if s<>'' then
    if s[length(s)]=oddelovac then
      delete(s,length(s),1);
end;

function cas_casstr(cas:Tcas):Tcasstr;
  var navrat,pom:Tcasstr;
begin
  cas:=cas mod 1440;
  str((cas div 60):2,navrat);
  str(cas mod 60,pom);
  while length(pom)<2 do
    pom:='0'+pom;
  navrat:=navrat+':'+pom;
  cas_casstr:=navrat;
end;

function casstr_cas(casstr:Tcasstr):Tcas;
  var navrat:Tcas;
      chyba,i:integer;
begin
  val(prvni(':',casstr),i,chyba);
  if (i<0)or(i>23)or(chyba<>0) then begin
    casstr_cas:=-1;
    exit;
  end;
  navrat:=60*i;
  val(casstr,i,chyba);
  if (i<0)or(i>59)or(chyba<>0) then begin
    casstr_cas:=-1;
    exit;
  end;
  inc(navrat,i);
  casstr_cas:=navrat;
end;

function prestupny(rok:integer):boolean;
begin
  if (rok mod 400=0)or((rok mod 4=0)and(rok mod 100<>0)) then
    prestupny:=true
  else
    prestupny:=false;
end;

procedure unor(rok:integer);
begin
  if (rok=0)or(prestupny(rok)) then
    mesice[2]:=29
  else
    mesice[2]:=28;
end;

function pocdnu(m,y:integer):integer;
begin
  if m=2 then
    unor(y);
  pocdnu:=mesice[m];
end;

function platnekratkedatum(d:Tkratkedatum):boolean;
begin
  platnekratkedatum:=false;
  unor(0);
  if (d.m>=1)and(d.m<=12) then
    if (d.d>=1)and(d.d<=mesice[d.m]) then
      platnekratkedatum:=true;
end;

function datum_datumstr(d:Tdatum;tyden:integer):Tdatumstr;
  var navrat,pom:Tdatumstr;
begin
  navrat:='';
  str(d.d,pom);
  navrat:=navrat+pom+'.';
  str(d.m,pom);
  navrat:=navrat+pom+'.';
  str(d.y,pom);
  navrat:=navrat+pom;
  pom:='';
  case tyden of
    0:pom:='';
    1:case d.w of
      1:pom:=' Po';
      2:pom:=' Ut';
      3:pom:=' St';
      4:pom:=' Ct';
      5:pom:=' Pa';
      6:pom:=' So';
      7:pom:=' Ne';
    end;
    2:case d.w of
      1:pom:=' Pondeli';
      2:pom:=' Utery';
      3:pom:=' Streda';
      4:pom:=' Ctvrtek';
      5:pom:=' Patek';
      6:pom:=' Sobota';
      7:pom:=' Nedele';
    end;
  end;
  navrat:=navrat+pom;
  datum_datumstr:=navrat;
end;

function datumstr_datum(s:Tdatumstr;var vystup:Tdatum):boolean;
  var chyba:integer;
      navrat:boolean;
begin
  navrat:=true;
  with vystup do begin
    w:=-1;
    val(prvni('.',s),d,chyba);
    if chyba<>0 then
      navrat:=false;
    val(prvni('.',s),m,chyba);
    if chyba<>0 then
      navrat:=false;
    val(s,y,chyba);
    if chyba<>0 then
      navrat:=false;
    unor(y);
    if (d<1)or(m<1)or(m>12)or(y<1) then
      navrat:=false;
    if navrat then
      if d>mesice[m] then
        navrat:=false;
  end;
  datumstr_datum:=navrat;
end;

function prectimesic(var s:string;del:boolean):integer;
  var i,m:integer;
begin
  prectimesic:=0;
  i:=0;
  if s<>'' then
    while s[i+1] in rimskecislice do begin
      inc(i);
      if i>=length(s) then
        break;
    end;
  if i=0 then
    exit;
  for m:=1 to 12 do
    if copy(s,1,i)=rimske[m] then begin
      prectimesic:=m;
      if del then
        delete(s,1,i);
      break;
    end;
end;

function rozdil(d1,d2:Tdatum):longint;
  var mensi,koef:integer;
      dpom:Tdatum;
      navrat:longint;
      i:integer;
begin
  koef:=+1;
  navrat:=0;
  mensi:=0;
  if d1.d<d2.d then
    mensi:=1;
  if d1.d>d2.d then
    mensi:=2;
  if d1.m<d2.m then
    mensi:=1;
  if d1.m>d2.m then
    mensi:=2;
  if d1.y<d2.y then
    mensi:=1;
  if d1.y>d2.y then
    mensi:=2;
  if mensi=2 then begin
    dpom:=d1;
    d1:=d2;
    d2:=dpom;
    koef:=-1;
  end;
  if d1.y=d2.y then begin
    unor(d1.y);
    if d1.m=d2.m then
      inc(navrat,d2.d-d1.d)
    else begin
      inc(navrat,mesice[d1.m]-d1.d);
      inc(navrat,d2.d);
      for i:=d1.m+1 to d2.m-1 do
        inc(navrat,mesice[i]);
    end;
  end else begin
    unor(d1.y);
    inc(navrat,mesice[d1.m]-d1.d);
    for i:=d1.m+1 to 12 do
      inc(navrat,mesice[i]);
    unor(d2.y);
    inc(navrat,d2.d);
    for i:=1 to d2.m-1 do
      inc(navrat,mesice[i]);
    inc(navrat,(d2.y-d1.y-1)*365);
    inc(navrat,((d2.y-1)div 4)-(d1.y div 4));
    dec(navrat,((d2.y-1)div 100)-(d1.y div 100));
    inc(navrat,((d2.y-1)div 400)-(d1.y div 400));
  end;
  rozdil:=koef*navrat;
end;

procedure denvtydnu(var d:Tdatum;vychozi:Tdatum);
  var pom:longint;
begin
  pom:=vychozi.w;
  inc(pom,rozdil(vychozi,d));
  if pom<1 then
    inc(pom,7*(1+trunc(abs(pom)/7)));
  d.w:=pom mod 7;
  if d.w=0 then
    d.w:=7;
end;

function cislotydne(d:Tdatum):integer;
  var i,poc:integer;
begin
  if d.w=-1 then begin
    cislotydne:=-1;
    exit;
  end;
  unor(d.y);
  poc:=0;
  for i:=1 to d.m-1 do
    inc(poc,mesice[i]);
  inc(poc,d.d);
  inc(poc,(7-(poc+7-d.w)mod 7)mod 7);
  cislotydne:=1+(poc-1) div 7;
end;

procedure opravdatum(vstup:Tdatum;cas:Tcas;var d:Tdatum);
begin
  d:=vstup;
  inc(d.d,cas div 1440);
  unor(d.y);
  while d.d>mesice[d.m] do begin
    dec(d.d,mesice[d.m]);
    inc(d.m);
    if d.m>12 then begin
      d.m:=1;
      inc(d.y);
      unor(d.y);
    end;
  end;
end;

function porovnejdatum(d1,d2:Tkratkedatum):integer;
begin
  porovnejdatum:=0;
  if d1.d<d2.d then
    porovnejdatum:=1;
  if d1.d>d2.d then
    porovnejdatum:=2;
  if d1.m<d2.m then
    porovnejdatum:=1;
  if d1.m>d2.m then
    porovnejdatum:=2;
end;





BEGIN
  mesice[1]:=31;
  mesice[2]:=28;
  mesice[3]:=31;
  mesice[4]:=30;
  mesice[5]:=31;
  mesice[6]:=30;
  mesice[7]:=31;
  mesice[8]:=31;
  mesice[9]:=30;
  mesice[10]:=31;
  mesice[11]:=30;
  mesice[12]:=31;
END.
