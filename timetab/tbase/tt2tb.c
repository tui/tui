/* -*- linux-c -*-
 * Connection finder, distribute under GPL version 2 or later
 * Copyright 2000, 2001 Pavel Machek <pavel@ucw.cz>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#define COMPATIBLE
#ifndef COMPATIBLE
#include <unistd.h>
#include <sys/mman.h>
#endif
#include <time.h>
#include <ctype.h>
#include <errno.h>
#include "../timetab.h"

#define MEMORY_SIZE 	(128*1024*1024)
#define MEMORY_START	(0x60000000)
static int memory_handle = -1;
#define GRP_CHAR	'+'
#define SIG "timetab-mem-image"
#undef LITE

#define MAX_REMARKS	255
#define REMARK_SIZE	14

#define SEP "\n"


char *outname = "output.tb";

FILE *subfile(char *subname)
{
	char buf[10240];
	sprintf(buf, "%s/%s", outname, subname);
	return fopen(buf, "w");
}

void putlong(FILE *f, unsigned long i)
{
	fprintf(f, "%c%c%c%c", (0xff & (i >> 24)), (0xff & (i >> 16)), (0xff & (i >> 8)),  (0xff & (i >> 0)));
}

int putvar(FILE *f, unsigned long i)
{
	long j = i;
	i &= 0x7ffff;	/* FIXME: This is probably buggy for -1 etc. */
	if ((j>(127*128*128*128)) || (j<(-127*128*128*128)))
		die("putvar: var %lx too big\n", i);
	if (i<128)
		return fprintf(f, "%c", i);
	if (i<(128*128))
		return fprintf(f, "%c%c", (0x80 | (0x7f & (i >> 7))), (0x7f & i));
	if (i<(128*128*128))
		return fprintf(f, "%c%c%c", (0x80 | (0x7f & (i >> 14))), (0x80 | (0x7f & (i >> 7))), (0x7f & i));
	if (i<(128*128*128*128))
		return fprintf(f, "%c%c%c%c", (0x80 | (0x7f & (i >> 21))), (0x80 | (0x7f & (i >> 14))), (0x80 | (0x7f & (i >> 7))), (0x7f & i));

	
	die("putvar: variable really too big\n");
}

int
putline(FILE *txt, FILE *dat, char *s)
{
	long int pos = ftell(txt);
	fprintf(txt, "%s" SEP, s);
	putlong(dat, pos);
	return ftell(dat)/4+1;
}

FILE *readme, *towns_txt, *towns_dat, *remarks_txt, *flags_txt, *flags_dat, *conns_txt, *conns_dat, *nodes_dat;


int pargc;
char **pargv;
char *table_file = NULL, *memory_file = "stdin.mem";

typedef unsigned char uchar;

struct id {
	struct id *next;
	char *name;
        char *cond;	/* Condition which days the train goes */
        char *note;     /* Train note */
	char *company;	/* Company which owns the node */
	int _id;
        uchar remark;
	int at;
	int start_time;
} __attribute__((__packed__));

static int town_id;

struct node {
	struct node *next;
	struct town *to;
	struct id *id;
	union {
		struct { 				/* Data used by -p */
			int linenum1, linenum2;
		} prep;
		struct {				/* Data used by normal search */
			struct town *from;
			unsigned short length;
		} search;
	} x;
	unsigned short time1, time2;
	uchar remark;
} __attribute__((__packed__));

#define TIME1 time1
#define TIME2 time2

struct town {
	uchar *name;
	struct town *next;
	struct node *last_node;
	struct node *nodes;
	unsigned short _id;	/* Warning: it is possible to hit this limit */
	struct node *by;	/* changed at runtime */
	int heapindex;		/* changed at runtime */
	unsigned short time;	/* changed at runtime */
	char final;		/* changed at runtime */
} __attribute__((__packed__));

#define HASHSIZE 60000
#define INF 65000
#define ANY (25*60)

struct state {
	char version[20];
	struct town *townhash[HASHSIZE];
	struct id *ids;
	unsigned short numtowns;
	int numnodes;
	int id_id;
        int n_remarks;
        char remarkvec[MAX_REMARKS][REMARK_SIZE];
	char *signature;
} __attribute__((__packed__));

struct state *state;

int finalized = -1;

struct town **townheap;

uchar *memory, *memory_start, *memory_end;

char *opts = "";

int compatible = 0, opt_memory_size = MEMORY_SIZE;

inline void *my_malloc(int size)
{
	void *res = memory;
	memory += size;
	if (memory>memory_end)
		die( "Allocated memory exhausted\n" );
	return res;
}

inline uchar *my_strdup(uchar *s)
{
#ifndef LITE
	char *res = my_malloc(strlen(s)+1);
	strcpy(res, s);
	return res;
#else
	return NULL;
#endif
}

inline int hashfn(uchar *name)
{
	if (strlen(name)>3)
		return (name[0] + 64*name[1] + (64*64)*name[2] + (64*64*64)*name[3])%HASHSIZE;	
	else
		return 0;
}

struct town *get_town(uchar *name, int create)
{
	int hash = hashfn(name);
	struct town *res = state->townhash[hashfn(name)];

	while (res != NULL) {
		if (!strcmp(res->name, name))
			return res;
		res = res->next;
	}
	if (!create)
		return NULL;	

	res = my_malloc(sizeof(struct town));
	memset(res, 0, sizeof(struct town));
	res->name = my_malloc(strlen(name)+1);
	strcpy(res->name, name);
	res->next = state->townhash[hash];
	res->time = INF;
	res->_id = town_id++;
	state->townhash[hash] = res;
	state->numtowns++;
	return res;
}


int pstate = 0, curtime, curkm, lasttime;

int inline
my_atoi(char *s)
{
	int t1 = 0;
	char *t = s;
	while ((*s>='0') && (*s<='9'))
		t1 = t1*10 + (*s++)-'0';
	if (t == s)
		return -1;
	return t1;
}

int inline
decodetime(char *s)
{
	int t1 = -1, t2 = -1;

	if (*s=='+')
		return lasttime = lasttime+my_atoi(s+1);
	if (sscanf( s, "%d:%d", &t1, &t2 ) == 2)
		return lasttime = t1*60+t2;
	fprintf( stderr, "Could not find time on %s", s );
	exit(1);
}


struct town *
decodeline(struct id *id, uchar *buf, struct town *prev)
{
	int i = 0;
	char *townname;
	int remindx = 0;
	char remark[10240];
	int from = -1, to = -1, km = -1;

	remark[0] = 0;
	if (buf[i]!='\t')
		from = decodetime(buf+i);
	while (buf[i]!='\t') i++;
	i++;

	if (buf[i]!='\t')
		to = decodetime(buf+i);
	else { to = -1; pstate = 0; }
	while (buf[i]!='\t') i++;
	i++;
        while (buf[i]!='\t') {
		remark[remindx++]=buf[i++];
	}
	remark[remindx]=0;
	i++;

	if (from==-1) from=to;
	if (to==-1) to=from;
	if (from==-1) {
		fprintf( stderr, "\nNo time on line %s\n", buf );
		return NULL;
	}

	if (buf[i] != '-') {
		km = my_atoi(buf+i);
		if (km == -1) {
			fprintf( stderr, "Kilometers not right on %s *%s\n", buf, buf+i );
			exit(1);
		}
	}
	while (buf[i]!='\t') i++;
	i++;
	townname = buf+i;

//	fprintf( stderr, "Got %s: %s %d:%d %d:%d\n", id->name, townname, from/60, from%60, to/60, to%60 );
	
	{
		struct town *this;

		this = get_town(townname, 1);
		if (prev) {
			struct node *node = my_malloc(sizeof(struct node));
			memset(node, 0, sizeof(struct node));

			node->next = prev->nodes;
			node->x.search.from = prev;
			node->to = this;
			node->TIME1 = curtime;
			node->TIME2 = from;
			node->x.search.length = km;
			node->id = id;
                        prev->nodes = node;
			state->numnodes++;
		} else {
			id->at = ftell(nodes_dat);
			id->start_time = from;
			curtime = from;
		}

		putvar(nodes_dat, this->_id+1);
#define FIX(a) (a>=0?a:a+(24*60))
		putvar(nodes_dat, FIX(from-curtime));
		putvar(nodes_dat, FIX(to-from));
		if (remark[0]) {
			putvar(nodes_dat, ftell(flags_txt));
			fprintf(flags_txt, "%s" SEP, remark);
		} else
			putvar(nodes_dat, 0);
		putvar(nodes_dat, km);

		curkm = km;
		curtime = to;
		return this;
	}
}

void do_end_id(struct id *id, char **note, char **company)
{
        if (!id) return;
        if (*note) {
        	id->note=my_strdup(*note);
        	free(*note);
                *note=NULL;
        }
        if (*company) {
        	id->company=my_strdup(*company);
        	free(*company);
                *company=NULL;
	}
	putvar(nodes_dat, 0);
        state->id_id++;
}

void
readit(void)
{
	uchar buf[10240];
	char *note = NULL;
	char *company = NULL;
	struct id *id = NULL;
	FILE *f = table_file ?  fopen( table_file, "r" ) : stdin;
	int lineno = 0;
	struct town *prev = NULL;

	fprintf(stderr, "Reading...\n");
	if (!f) {
		fprintf(stderr, "Error reading %s: %m\n", table_file);
		exit(1);
	}
	state = my_malloc(sizeof(struct state));
	state->numtowns = 0;
	state->numnodes = 0;
	strcpy(state->version, __DATE__);
	state->id_id = 0;
        state->n_remarks = 1;
        state->remarkvec[0][0]=0;
	while (1) {
		lineno++;
		if (!(lineno % 10000))
			fprintf( stderr, "Reading: %dK lines, %d towns, %d nodes, %.1fMB mem\r", lineno/1000, state->numtowns, state->numnodes, ((double) ((char *) memory - (char *)memory_start))/(1024*1024));
		if (fgets(buf, 10230, f)==NULL) break;
		if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
                if (buf[0]==0) continue;
		switch (*buf) {
		case '#':
                	do_end_id(id,&note,&company);
			lasttime = -9999;
			id = my_malloc(sizeof(struct id));
			id->name = my_malloc(strlen(buf+2)+1);
			id->next = state->ids;
			id->_id = state->id_id;
			state->ids = id;
			strcpy(id->name, buf+2);
			pstate = 1;
			prev = NULL;
			continue;
		case 'R':
			id->cond = my_strdup(buf);
			pstate = 0;
			break;
		case 'G':
			if (!company) {
				company = malloc(strlen(buf+1)+1);
				strcpy(company,"");
			} else {
				company=realloc(company,strlen(company)+1+strlen(buf+1)+1);
				strcat(company,"\n");
			}
			strcat(company, buf+1);
			pstate = 0;
			break;
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'H': case 'I': case 'J': case 'K': case 'L': case 'M':
		case 'N': case 'O': case 'P': case 'Q': case 'S': case 'T':
		case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z':
		case '[':
			if (note==NULL) {
				note = malloc(strlen(buf)+1);
				strcpy(note,"");
			} else {
				note=realloc(note,strlen(note)+1+strlen(buf)+1);
				strcat(note,"\n");
			}
			strcat(note, buf);
			pstate = 0;
			break;
		case ';':	/* Remark */
			continue;
		case '!':	/* Followed by keyword */
			{
				int y1, m1, d1, y2, m2, d2, i, j;
				if (sscanf(buf+1, "valid y%d m%d d%d .. y%d m%d d%d\n",
					   &y1, &m1, &d1, &y2, &m2, &d2) == 6) {
					continue;
				}

				if (sscanf(buf+1, "version %d.%d\n", &i, &j) == 2) {
					continue;
				}

			}
			break;
		case 0:
			break;
		default:
			if (pstate == 1)
				prev = decodeline(id, buf, prev);
			else
				fprintf( stderr, "Line in unexpected state: %s\n", buf );
		}
	}
	do_end_id(id,&note,&company);

	state->signature = my_strdup(SIG);
	if (!compatible) {
#ifndef COMPATIBLE
		fprintf( stderr, "Msync  :\r");	/* Msync is slow, but definitely needed on NFS. MS_SYNC works over NFS */
		msync((void *) MEMORY_START, MEMORY_SIZE, MS_SYNC);
#endif
	}
	fprintf( stderr, "Ready  : %d lines, %d towns, %d nodes, %.2fMB mem\n", lineno, state->numtowns, state->numnodes, ((double) ((char *) memory-(char *)memory_start))/(1024*1024));
	if (f!=stdin)
	        fclose(f);
}

int lastrem(struct node *nd)
{
        struct id *myid=nd->id;
        struct node *pnode;

        pnode=nd->to->nodes;
        while (pnode->next!=NULL && pnode->id!=myid) pnode=pnode->next;
	if (pnode->id!=myid)
		return myid->remark;
	return pnode->remark;
}

void prefix(char *pref,char *s)
{
	int i1=0;
        char *p;
        char tmp[1024];
	
	puts(pref); puts(s);
#if 0
	/* This should prefix all lines of s with prefix pref.
	   IT was buggy and caused problems -> commented out.
	*/
        for(;;) {
	        strcpy(tmp,pref);
        	if ((p=strchr(s+i1,'\n'))==NULL) break;
	        strncat(tmp,s+i1,p-s-i1);
        	puts(tmp);
                i1=p-s+1;
	}
        strcat(tmp,s+i1);
        puts(tmp);
#endif
}


void
townlist(void)
{
	int i;
	struct town *town;
	for (i=0; i<HASHSIZE; i++) {
		town = state->townhash[i];
		while (town) {
			fseek(towns_dat, town->_id*TOWNS_SIZE, SEEK_SET);
			putline(towns_txt, towns_dat, town->name);
			town = town->next;
		}
	}
}

/* Currently not used; may be used instead of map_private(): reduces memory
   requirements but slows things down.
 */

static void
map_private(void)
{
#ifndef COMPATIBLE
	memory = memory_start = (uchar *) MEMORY_START;
	memory_end = memory + MEMORY_SIZE;
	if (mmap(memory, MEMORY_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, memory_handle, 0)== (void *) ~0)
		die( "Could not allocate memory: %m\n" );
	state = (struct state *) memory;
#endif
}

void
idlist(void)
{
	struct id *id = state->ids;
	printf("%d\n", state->id_id);
	while (id) {
		putline(conns_txt, conns_dat, id->name);
		putline(remarks_txt, conns_dat, id->cond);
		fprintf(remarks_txt, "%s" SEP "%c", id->note, 0);
		putlong(conns_dat, id->at);
		putlong(conns_dat, id->start_time);
		id = id->next;
	}
	
}

void
setup_memory(void)
{
	memory = memory_start = malloc(opt_memory_size);
	if (!memory)
		die( "Not enough memory in system?" );

	memory_end = memory + MEMORY_SIZE;
	readit();
}


int
main(int argc, char *argv[])
{
	if (argc > 1)
		if (*argv[1] == '-') {
			opts = argv[1]+1;
			argv++;
			argc--;
		}

	mkdir(outname, 0755);
	readme = subfile("README");
	towns_txt = subfile("towns.txt");
	towns_dat = subfile("towns.dat");
	conns_txt = subfile("conns.txt");
	conns_dat = subfile("conns.dat");
	remarks_txt = subfile("remarks.txt");
	nodes_dat = subfile("nodes.dat");
	flags_txt = subfile("flags.txt");
	flags_dat = subfile("flags.dat");

	putline(flags_txt, flags_dat, "(empty)");	/* Empty remark to be stored @0 */
	setup_memory();

	fprintf(readme, "!version 1.1\n");
	fprintf(readme, "!towns %d\n", state->numtowns);
	fprintf(readme, "!conns %d\n", state->id_id);
	fprintf(readme, "!nodes %d\n", state->numnodes);
	fprintf(readme, "!remarks %d\n", state->id_id);

	townlist();
	idlist();


	return 0;
}

