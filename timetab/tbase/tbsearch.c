/*****************************************************************
tbsearch - search utility for .tb database format  
Distribute under GPL version 2.   
Copyright (C),  Roman Krejcik, 2001,  roman.krejcik@ruk.cuni.cz   
******************************************************************/
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

#define MAX_CONFIG_LINE 254
#define ANY_TIME 0x00020000U   // connection goes any time - usually walk)

#define ANY_TIME_IN_TT 0x05DC // maybe bug in tt2tb 

#define TB_LONG unsigned int
#define TBL_MAX UINT_MAX
#define HANDICAP_TYPE unsigned int
#define HT_MAX UINT_MAX

enum {
  SUCCES,
  NO_CONFIG_FILE,
  UNKNOWN_SECTION_IN_CONFIG,
  UNKNOWN_VAR_IN_CONFIG,
  CANNOT_OPEN_FILE,
  INVALID_FILE_FORMAT,
  NO_MEMORY,
  UNKNOWN_OPTION,
  MANY_PARAM,
  STATION_NOT_FOUND,
  FEW_PARAM,
  INVALID_TIME,
  INVALID_DATE,
  WRONG_OPTION
} ERROR_CODES;

enum {
  PRINT_RESULTS,
  SHORT_PRINT,
  NO_WARNING,
  _FLAGS
} OPTIONS;

int flags[_FLAGS];

struct SIZE {
  TB_LONG towns, conns, nodes, remarks;
} size;

struct NODE {
  TB_LONG town, arr, dep, flags, dist;
};

/*** GRAPH FOR DIJKSTRA SEARCH *******/

struct VERTEX {
  TB_LONG conn; // conn number
  TB_LONG dep; // departure time
  TB_LONG time; // transport time; time + dep = arrival time  
  TB_LONG from; // from
  TB_LONG dest; // destination
  TB_LONG dist; // distancew
  HANDICAP_TYPE handicap;
  TB_LONG heap_index;
  TB_LONG list_index;
  TB_LONG day;  // day of use
  struct VERTEX *backpath;
} *vertex, ***vlist;

TB_LONG *list_counter;
 
TB_LONG vertex_ptr = 0;

struct LIST {
  struct VERTEX *vertex;
  struct LIST *next;
} **list;  // array of lists of nodes begins in  town (town is array index)

struct VERTEX **heap;
TB_LONG heap_size;

char **town_names;
char **conn_names;

char path[MAX_CONFIG_LINE];
int path_len; 

TB_LONG *towns_from;
TB_LONG match_from;

TB_LONG *is_dest;

TB_LONG *remark_index;

struct D_REMARK {
  unsigned int day_start, days_count;
  char *weeks;
} *d_remarks;

/* format D-REMARK in remark file: Dyyyy-mm-dd X ABABAB...
   where X is number of days for each is remark valid, AB two chars,
   (int)A number of week with mask B, (sum of all A)*7 must be higher then X */

TB_LONG conn_without_d_remark;

/******* PENALITES *********************/

HANDICAP_TYPE P_TRAIN_GO = 5;
HANDICAP_TYPE P_TRAIN_WAIT = 6;
HANDICAP_TYPE P_CHANGE_WAIT = 4;
HANDICAP_TYPE P_CONN_CHANGE = 80;
HANDICAP_TYPE P_WAIT_BEFORE_START = 3;
HANDICAP_TYPE P_DISTANCE = 2;

/***************************************/

/******* dobre **
HANDICAP_TYPE P_TRAIN_GO = 5;
HANDICAP_TYPE P_TRAIN_WAIT = 6;
HANDICAP_TYPE P_CHANGE_WAIT = 4;
HANDICAP_TYPE P_CONN_CHANGE = 80;
HANDICAP_TYPE P_WAIT_BEFORE_START = 3;
****************************/


void read_config_file(void);
void read_README_file(void);
void read_towns(void);
void make_graph(void);
TB_LONG get_long(FILE *f);
TB_LONG read_vari(char **pointer);
void add_vertex(TB_LONG conn, struct NODE *to);
void update_handicap(struct VERTEX *ver, TB_LONG handicap);
void bubble_down_in_heap(TB_LONG heap_index);
void search(int time, int date);
HANDICAP_TYPE edge_handicap(struct VERTEX *vertex); // edge between heap[0] and vertex
void del_from_list(void); //delete vertex in heap[0] from list
inline void print_time(int t);
void print_remarks(FILE *remarks, TB_LONG last_conn);
void print_result(void);
void name_to_town_num(char *from, char *to);
void die(int error_code, char *p);
int date2int(int d,int m,int y);
int get_number(char *f, int *n);
int get_d_remark(char *f, struct D_REMARK *dr);
void read_d_remark(int conn, char *first);
char *mk_path(char *p);

int main(int argc, char *argv[]) {
  char *from = NULL, *to = NULL;
  int i, stime = -1, date = -1;

  strcpy(path, "./");
  path_len = 2;

  flags[PRINT_RESULTS] = 1;
  for(i = 1; i < argc; i++) {
    if (*argv[i] == '-') { /* option */
 
      if (strcmp(argv[i],"-n") == 0) {
	if (i + 1 == argc) die(WRONG_OPTION, argv[i]);
	flags[PRINT_RESULTS] = atoi(argv[i+1]);
	if (flags[PRINT_RESULTS] < 1) die(WRONG_OPTION, argv[i]);
	i++;
	continue;    
      }
      if (strcmp(argv[i],"-s") == 0) {
	flags[SHORT_PRINT] = 1;
	continue;
      }
      if (strcmp(argv[i],"-h") == 0) {
	printf("Usage: tgo [-n <number>] [-w] [-p <path>] [-h] <from>[+] <date> <time> <to>[+]\n");
	return 0;
      }

      if (strcmp(argv[i],"-w") == 0) {
	flags[NO_WARNING] = 1;
	continue;
      }
      if (strcmp(argv[i],"-p") == 0) {
	int l;
	if (i + 1 == argc) die(WRONG_OPTION, argv[i]);
	strncpy(path, argv[i+1], MAX_CONFIG_LINE - 20);
	l = strlen(path);
	if (l && path[l-1] != '/') { path[l] = '/'; path[l+1] = '\0'; path_len = l+1; } else path_len = l;
	i++;
	continue;
      }
      die(UNKNOWN_OPTION, NULL);
    } else {  /* other param */
      if (!from) {
	from = argv[i];
        continue;
      }
      if (date == -1) {
	int d,m,y;
	if (sscanf(argv[i],"%d.%d.%d",&d,&m,&y) != 3 || d < 1 || d > 31 || m < 1 || m > 12 || y < 1970) die(INVALID_DATE, NULL);
	date = date2int(d,m,y);
	continue;

      }
      if (stime == -1) {
	int h,m;
	if (sscanf(argv[i],"%d:%d",&h,&m) != 2 || h > 23 || h < 0 || m < 0 || m > 59) die(INVALID_TIME, NULL);
	stime = h * 60 + m;
        continue;
      }
      if (!to) {
        to = argv[i];
        continue;
      }
      die(MANY_PARAM, NULL);
    }
  }
  if (!to) die(FEW_PARAM, NULL);

  read_config_file();
  read_README_file();
  read_towns();
  name_to_town_num(from, to); 
  make_graph();
  // printf(".tb mapped\n\n");   //--------

  search(stime, date);

  return 0;
}

void read_config_file(void) {
  /*  FILE *config_file;
  char line[MAX_CONFIG_LINE], *ch;
  int section = 0;   // 0 no section, 1 general, 2 change
  config_file = fopen(".tgo","r");
  if (!config_file) die(NO_CONFIG_FILE, NULL);
  while (fgets(line, MAX_CONFIG_LINE-1, config_file)) {
    line[strlen(line)-1] = '\0';
    if (line[0] == '#') continue;
    if (line[0] == '[') {
      if (strcmp(line, "[general]") == 0) { section = 1; continue; }
      if (strcmp(line, "[change]") == 0) { section = 2; continue; }
      die(UNKNOWN_SECTION_IN_CONFIG, line);
    }
    switch (section) {
    case 1: 
      ch = strchr(line,' ');
      if (line[0] == '\0') continue;
      if (!ch) die(UNKNOWN_VAR_IN_CONFIG, line);
      *ch = '\0';
      //     if (strcmp(line, "path") == 0) { strcpy(path, ch+1); continue; }
      if (strcmp(line, "path") == 0) continue;
      die(UNKNOWN_VAR_IN_CONFIG, line);
      break; 
    case 2: break;
    }
  }
  fclose(config_file);*/
}

void read_README_file(void) {
  FILE *readme;
  char line[MAX_CONFIG_LINE], **endptr = NULL;
  if (! (readme = fopen(mk_path("README"),"r"))) die(CANNOT_OPEN_FILE,"README");
  while (fgets(line, MAX_CONFIG_LINE, readme)) {
    line[strlen(line)-1] = '\0';
    if (line[0] == '!') {
      if (!strncmp(line+1,"towns",5)) {
	size.towns = strtol(line+7,endptr,10);
	if (endptr) die(INVALID_FILE_FORMAT, "README");
      }
      if (!strncmp(line+1,"conns",5)) {
	size.conns = strtol(line+7,endptr,10);
	if (endptr) die(INVALID_FILE_FORMAT, "README");
      }
      if (!strncmp(line+1,"nodes",5)) {
	size.nodes = strtol(line+7,endptr,10);
	if (endptr) die(INVALID_FILE_FORMAT, "README");
      }
      if (!strncmp(line+1,"remarks",7)) {
	size.remarks = strtol(line+9,endptr,10);
	if (endptr) die(INVALID_FILE_FORMAT, "README");
      }
    }
  }
  fclose(readme);
}

void read_towns(void) {
  FILE *f_dat;
  int f_txt,i;
  unsigned int offset;
  char *first, *ch, *t;
  if ((f_txt = open(mk_path("towns.txt"),O_RDONLY)) == -1) die(CANNOT_OPEN_FILE,"towns.txt");
  offset = lseek(f_txt, 0, SEEK_END);
  first = (char *) mmap(0, offset, PROT_READ, MAP_PRIVATE, f_txt, 0);
  if (first == MAP_FAILED) die(NO_MEMORY, NULL);

  if (!(town_names = (char **) malloc(sizeof(char *)*size.towns))) die(NO_MEMORY, NULL);
  if (!(f_dat = fopen(mk_path("towns.dat"),"rb"))) die(CANNOT_OPEN_FILE,"towns.dat");
  for( i = 0; i < size.towns; i++) {
    if (!(town_names[i] = (char *) malloc(sizeof(char) * MAX_CONFIG_LINE))) die(NO_MEMORY, NULL);
    ch = first + get_long(f_dat);
    t = town_names[i];
    while (*ch != '\n') { *t++ = *ch++; }
    *t = '\0';
  }
  munmap((void *) first, offset);
  close(f_txt);
  fclose(f_dat);
}

void make_graph(void) {
  int fnodes, f_txt, r_txt;
  FILE *fconns;
  unsigned int node_offset, offset, r_offset;
  //struct CONN conn;
  TB_LONG conn_node_ptr, conn_time; 
  char *node_element, *position, *first, *t, *ch, *r_first;
  struct NODE node; 
  struct LIST *ptr;
  TB_LONG i,j;
  if (!(fconns = fopen(mk_path("conns.dat"),"rb"))) die(CANNOT_OPEN_FILE,"conns.dat");
  if ((fnodes = open(mk_path("nodes.dat"),O_RDONLY)) == -1) die(CANNOT_OPEN_FILE,"nodes.dat");
  node_offset = lseek(fnodes, 0, SEEK_END);
  node_element = (char *) mmap(0, node_offset, PROT_READ, MAP_PRIVATE, fnodes, 0);
  if (node_element == MAP_FAILED) die(NO_MEMORY, NULL);
  if (!(vertex = (struct VERTEX *) malloc(sizeof(struct VERTEX)*size.nodes))) die(NO_MEMORY, NULL);
  if (!(list = (struct LIST **) malloc(sizeof(struct LIST *)*size.towns))) die(NO_MEMORY, NULL);
  if (!(list_counter = (TB_LONG *) malloc(sizeof(TB_LONG)*size.towns))) die(NO_MEMORY, NULL);
  for(i = 0; i < size.towns; i++) {
    list[i] = NULL;
    list_counter[i] = 0;
  }

  if ((f_txt = open(mk_path("conns.txt"),O_RDONLY)) == -1) die(CANNOT_OPEN_FILE,"conns.txt");
  offset = lseek(f_txt, 0, SEEK_END);
  first = (char *) mmap(0, offset, PROT_READ, MAP_PRIVATE, f_txt, 0);
  if (first == MAP_FAILED) die(NO_MEMORY, NULL);
  if (!(conn_names = (char **) malloc(sizeof(char *)*size.conns))) die(NO_MEMORY, NULL);

  if ((r_txt = open(mk_path("remarks.txt"),O_RDONLY)) == -1) die(CANNOT_OPEN_FILE,"remarks.txt");
  r_offset = lseek(r_txt, 0, SEEK_END);
  r_first = (char *) mmap(0, r_offset, PROT_READ, MAP_PRIVATE, r_txt, 0);
  if (r_first == MAP_FAILED) die(NO_MEMORY, NULL);

  if (!(remark_index = (TB_LONG *) malloc(sizeof(TB_LONG)*size.conns))) die(NO_MEMORY, NULL);
  if (!(d_remarks = (struct D_REMARK *) malloc(sizeof(struct D_REMARK)*size.conns))) die(NO_MEMORY, NULL);

  for(i = 0; i < size.conns; i++) {
    if (!(conn_names[i] = (char *) malloc(sizeof(char) * MAX_CONFIG_LINE))) die(NO_MEMORY, NULL);
    ch = first + get_long(fconns);
    t = conn_names[i];
    while (*ch != '\n') { *t++ = *ch++; }
    *t = '\0';
    remark_index[i] = get_long(fconns);
    read_d_remark(i, r_first);
    conn_node_ptr = get_long(fconns);
    conn_time = get_long(fconns);
    position = node_element + (conn_node_ptr / sizeof(char));
    node.town = read_vari(&position);
    add_vertex(conn_time, NULL); // send conn time to add_vertex function
    while (node.town) {
      node.arr = read_vari(&position);
      node.dep = read_vari(&position);
      node.flags = read_vari(&position);
      node.dist = read_vari(&position);
      add_vertex(i,&node);
      node.town = read_vari(&position);
    }
  }

  if (conn_without_d_remark && (! flags[NO_WARNING])) printf("Warning: %u connections without date remark!\n",conn_without_d_remark);
  munmap((void *) r_first, r_offset);
  close(r_txt);
  munmap((void *) node_element, node_offset);
  close(fnodes);
  munmap((void *) first, offset);
  close(f_txt);

  if (!(vlist = (struct VERTEX ***) malloc(sizeof(struct VERTEX **) * size.towns))) die(NO_MEMORY, NULL);
  for(i = 0; i < size.towns; i++) {
    if (list_counter[i] == 0) continue;
    if (!(vlist[i] = (struct VERTEX **) malloc(sizeof(struct VERTEX *) * list_counter[i]))) die(NO_MEMORY, NULL);
    ptr = list[i];
    j = 0;
    while (ptr) {
      vlist[i][j] = ptr->vertex;
      ptr->vertex->list_index = j;
      j++;
      ptr = ptr->next;
    }
  }
}

TB_LONG get_long(FILE *f) {
  int i;
  TB_LONG ret_val = 0;
  unsigned char read;
  for(i = 24; i >= 0; i -= 8) {
    fscanf(f,"%c",&read);
    ret_val |= (read << i);
  }
  return ret_val;
}

TB_LONG read_vari(char **pointer) {
  TB_LONG ret_val = 0;
  unsigned char val;
  do {
    val = **pointer;
    (*pointer)++; 
    ret_val <<= 7;
    ret_val |= (val & 0x7f);
  } while (val & 0x80);  
  return ret_val;
  
}

void add_vertex(TB_LONG conn, struct NODE *to) {
  static TB_LONG time;
  static int conn_goes_any_time;
  struct LIST *l;
  static struct NODE from;

  if (to == NULL) {
    if (conn == ANY_TIME_IN_TT) {
      conn_goes_any_time = 1;
    } else {
      time = conn;
      conn_goes_any_time = 0;
    }
    from.arr = -1;
    return;
  } 
  if (from.arr != -1) {
    if (conn_goes_any_time) {
      vertex[vertex_ptr].dep  = ANY_TIME;
      vertex[vertex_ptr].time = to->arr;
    } else {
      time += from.arr;
      time += from.dep;
      if (time > 1439) time -= 1440; // 1440 is equal 24:00
      vertex[vertex_ptr].dep  = time;
      vertex[vertex_ptr].time = to->arr;
    }
    vertex[vertex_ptr].conn = conn;
    vertex[vertex_ptr].from = from.town-1;
    vertex[vertex_ptr].dest = to->town-1;
    vertex[vertex_ptr].dist = to->dist;
    vertex[vertex_ptr].handicap = HT_MAX;
    if (!(l = (struct LIST *) malloc(sizeof(struct LIST)))) die(NO_MEMORY, NULL);
    l->vertex = vertex + vertex_ptr;
    l->next = list[from.town-1];
    list[from.town-1] = l;
    list_counter[from.town-1]++;
    vertex_ptr++;
  }
   
  from = *to;
}

int goes(struct VERTEX *v, int day) {
  struct D_REMARK *d = d_remarks + v->conn;
  int diff;
  unsigned char mask;
  if (d->day_start == UINT_MAX) return -1; // D mark not available
  diff = day - d->day_start;
  if (diff < 0 || diff >= d->days_count) return -1; //out of range
  mask = d->weeks[diff / 7];
  return mask & ( 1 << (diff % 7)); 
}

HANDICAP_TYPE edge_handicap(struct VERTEX *vertex) {
  HANDICAP_TYPE ret_val;
  TB_LONG arr = heap[0]->dep + heap[0]->time; 
    // in heap[0]->dep must be real dep time, not ANY_TIME
  if (vertex->dep & ANY_TIME) {
    ret_val = 0;     
  } else {
    if (vertex->dep < arr) {
      if (goes(vertex, heap[0]->day + 1) == 0) return HT_MAX; //not goes
      ret_val = 1440 + vertex->dep - arr;
    } else {
      if (goes(vertex, heap[0]->day) == 0) return HT_MAX; //not goes
      ret_val = vertex->dep - arr;
    }
  }
  if (vertex->conn == heap[0]->conn) {
    ret_val *= P_TRAIN_WAIT;
  } else {
    ret_val *= P_CHANGE_WAIT;
    ret_val += P_CONN_CHANGE;
  }
  ret_val += vertex->time * P_TRAIN_GO;
  ret_val += vertex->dist * P_DISTANCE;
  return ret_val;
}

void update_handicap(struct VERTEX *ver, TB_LONG handicap) {
  TB_LONG hi,up;

  if (ver->dep & ANY_TIME) {
    ver->dep = ((heap[0]->dep + heap[0]->time) % 1440) | ANY_TIME;  
  }

  if (ver->handicap == HT_MAX) {  /* not yet in heap */
    heap[heap_size] = ver;
    ver->heap_index = heap_size++;
  }
  ver->handicap = handicap;
  ver->backpath = heap[0];
  hi = ver->heap_index;
  while (hi) {
    up = (hi+1)/2 - 1;
    if (heap[up]->handicap < handicap) break;
    heap[hi] = heap[up];
    heap[up] = ver;
    ver->heap_index = up;
    heap[hi]->heap_index = hi;
    hi = up;
  }
  bubble_down_in_heap(hi);
}

void bubble_down_in_heap(TB_LONG heap_index) {
  TB_LONG l,r;
  struct VERTEX *p;
  while(heap_index < TBL_MAX / 2 - 1) {
    l = (heap_index+1) * 2 - 1;
    r = (heap_index+1) * 2;
    if (l >=  heap_size) break;
    if (r < heap_size) l = heap[l]->handicap < heap[r]->handicap ? l : r;
    if (heap[heap_index]->handicap < heap[l]->handicap) break;
    p = heap[heap_index];
    heap[heap_index] = heap[l];
    heap[l] = p;
    heap[l]->heap_index = l;
    heap[heap_index]->heap_index = heap_index;
    heap_index = l;
  }
}

void search(int time, int date) {
  HANDICAP_TYPE handicap;
  TB_LONG i,j,dest, from, flag = 0; 
  HANDICAP_TYPE h;
  heap = (struct VERTEX **) malloc(sizeof(struct VERTEX *)*size.nodes);
  for(j = 0; j < match_from; j++) {   // adding strting staions (with names match FROM pattern) to heap 
    from = towns_from[j];
    for(i = 0; i < list_counter[from]; i++) {
      if (vlist[from][i]->dep & ANY_TIME) {
	h = 0;
       	vlist[from][i]->dep = time | ANY_TIME;   // ORIGINAL VALUE CHANGED
	vlist[from][i]->day = date;
      } else {
	if (time > vlist[from][i]->dep) { 
	  if (goes(vlist[from][i], date + 1) == 0) { 
	    // not goes 
	    // ---------------> taky  by se mohlo byt zde  smazani z list_counteru 
	    continue;
	  } 
	  h = 1440 + vlist[from][i]->dep - time;
	  vlist[from][i]->day = date+1;
	} else {
	  if (goes(vlist[from][i], date) == 0) { 
	    // not goes
	    // ---------------> taky  by se mohlo byt zde  smazani z list_counteru 
	    continue;
	  } 
	  h = vlist[from][i]->dep - time;
	  vlist[from][i]->day = date;
	}
      }
      h *= P_WAIT_BEFORE_START;
      h += vlist[from][i]->time * P_TRAIN_GO;
      h += vlist[from][i]->dist * P_DISTANCE;
      if (heap_size > 0) {
	update_handicap(vlist[from][i], h);
      } else {
	heap[0] = vlist[from][i];
	heap[0]->handicap = h;
	heap[0]->heap_index = 0;
	heap_size++;
      }
      vlist[from][i]->backpath = NULL;
    }
  }

  for(;;) {   /* dijksra algorithm */
    if (heap_size == 0) {
      printf("Connection not found.\n");
      exit(0);
    }
    if (heap[0]->dep & ANY_TIME) {  // speed optimalization - temporary remove ANY_TIME flag in dep
      heap[0]->dep &= 0xFFFF; 
      flag = 1;
    } 

    if (is_dest[heap[0]->dest]) {
      print_result();
      if (! --flags[PRINT_RESULTS]) return;
    } else {
      dest = heap[0]->dest;
      for(i = 0; i < list_counter[dest]; i++) {
	handicap = edge_handicap(vlist[dest][i]);
	if (handicap == HT_MAX) { //not goes this day
	  /*** mohlo by tu byt mazani z list_couteru (mozna)****/
	  continue;
	}
	handicap += heap[0]->handicap;
	if (handicap < vlist[dest][i]->handicap) {
	  update_handicap(vlist[dest][i], handicap);
	}
      }
    }
    if (flag) {
      heap[0]->dep |= ANY_TIME;
      flag = 0;
    }
    del_from_list();
    heap[0] = heap[--heap_size];
    bubble_down_in_heap(0);
  }
} 

void del_from_list() {
  TB_LONG from = heap[0]->from;
  vlist[from][heap[0]->list_index] = vlist[from][--list_counter[from]];
}

inline void print_time(int t){
  if (t < 0) t += 1440;
  printf("%2d:%02d",t/60,t%60);
}

void print_remarks(FILE *remarks, TB_LONG last_conn) {
  char last = '\n', ch;
  putchar('\n');
  fseek(remarks, remark_index[last_conn], SEEK_SET);
  while ((ch = getc(remarks)) != '\0')  {
    if (last != '\n') putchar(ch);
    last = ch;
  } 
}

void print_result(void) {
  struct VERTEX **stack;
  TB_LONG dist, sp = 0, num_AT_begin = 0;
  int sum, i;
  TB_LONG last_conn = TBL_MAX;
  struct VERTEX *ptr = heap[0];
  FILE *remarks;

  if (! (remarks = fopen(mk_path("remarks.txt"),"r"))) die(CANNOT_OPEN_FILE,"remarks.txt");

  if (!(stack = (struct VERTEX **) malloc(sizeof(struct VERTEX *)*size.nodes))) die(NO_MEMORY,NULL);
  dist = 0;
  while (ptr) {
    stack[sp++] = ptr;
    dist += ptr->dist;
    if (ptr->dep & ANY_TIME) {
      ptr->dep &= 0xFFFF;
      num_AT_begin++;
    } else num_AT_begin = 0;
    ptr=ptr->backpath;
  }
  if (num_AT_begin && num_AT_begin < sp) {  /* some conns going ANY_TIME on begin */
    sum = stack[sp - num_AT_begin -1]->dep - (stack[sp - num_AT_begin]->dep + stack[sp - num_AT_begin]->time);
    if (sum < 0) sum += 1440;
    for(i = sp-1; i > sp - num_AT_begin - 1; i--) {
      stack[i]->dep += sum;
    }
  }
  printf("===============================================================\nConnection from: %s\n             to: %s\n===============================================================\n%u kilometres,\ttravel time ",town_names[stack[sp-1]->from],town_names[stack[0]->dest],dist);
  sum = stack[0]->dep + stack[0]-> time - stack[sp-1]->dep;
  print_time(sum);
  putchar('\n');
  dist = 0;
  for (;;) {
    if (sp == 0) {
      printf("\t\t\t\t%3u\t%s\n",dist,town_names[stack[sp]->dest]);
      print_remarks(remarks,last_conn);
      break;
    } else sp--;
    if (stack[sp]->conn != last_conn) {
      if (last_conn != TBL_MAX) {
	printf("\t\t\t\t%3u\t%s\n",dist,town_names[stack[sp]->from]);
	print_remarks(remarks,last_conn);
      }
      dist = 0;
      last_conn = stack[sp]->conn;
      printf("\n# %s\n---------------------\n",conn_names[stack[sp]->conn]);
    }
    putchar('\t');
    print_time(stack[sp]->dep);    
    putchar('\t');
    /* TO DO print ramrks */ 
    putchar('\t');
    /* DEBUG **/
    //       printf("%6u\t",stack[sp]->handicap);
    /***/
    printf("\t%3u\t%s\n",dist,town_names[stack[sp]->from]);
    dist += stack[sp]->dist;
    sum = (stack[sp]->dep + stack[sp]->time) % 1440;
    print_time(sum);
  }
  printf("\n\n");
  fclose(remarks);
  free(stack);
}

void name_to_town_num(char *from, char *to) {
  int i, nto = MAX_CONFIG_LINE, nfrom = MAX_CONFIG_LINE, lento, lenfrom, any_dest = 0;
  lento = strlen(to);
  lenfrom = strlen(from);
  if (lento == 0 || lenfrom == 0) die(STATION_NOT_FOUND, "");
  if (to[lento-1] == '+') nto = lento-1;
  if (from[lenfrom-1] == '+') nfrom = lenfrom-1;

  if (!(towns_from = (TB_LONG *) malloc(sizeof(TB_LONG)*size.towns))) die(NO_MEMORY, NULL);
  if (!(is_dest = (TB_LONG *) malloc(sizeof(TB_LONG)*size.towns))) die(NO_MEMORY, NULL);

  for(i = 0; i < size.towns; i++) {
    if (strncasecmp(from, town_names[i], nfrom) == 0) {
      towns_from[match_from++] = i;
    }
    if (strncasecmp(to, town_names[i], nto) == 0) { is_dest[i] = 1; any_dest = 1; } else is_dest[i] = 0;
  }
  if (match_from == 0) die(STATION_NOT_FOUND, from);
  if (any_dest == 0) die(STATION_NOT_FOUND, to);
}

void die(int error_code, char *p) {
  switch (error_code) {
  case NO_CONFIG_FILE: fprintf(stderr, "Cannot locate config file ( .tgo ).\nProgram aborted.\n" ); break;
  case UNKNOWN_SECTION_IN_CONFIG: fprintf(stderr, "Unknown section \"%s\" in config file.\nProgram aborted.\n", p); break;
  case UNKNOWN_VAR_IN_CONFIG: fprintf(stderr, "Unknown variable \"%s\" in config file.\nProgram aborted.\n", p); break;
  case CANNOT_OPEN_FILE: fprintf(stderr, "Cannot open file. (%s)\nProgram aborted.\n", p); break;
  case INVALID_FILE_FORMAT: fprintf(stderr, "Invalid file format. (%s)\nProgram aborted.\n", p); break;
  case NO_MEMORY: fprintf(stderr, "Not enough memory.\nProgram aborted.\n"); break;
  case UNKNOWN_OPTION: fprintf(stderr, "Unknown option. Try -h option for help.\n"); break;
  case MANY_PARAM: fprintf(stderr, "Too many parameters. Try -h option for help.\n"); break;
  case STATION_NOT_FOUND: fprintf(stderr, "Station %s not found in town index.\nProgram aborted.\n",p); break;
  case FEW_PARAM: fprintf(stderr, "Too few parameters. Try -h option for help.\n"); break;
  case INVALID_TIME : fprintf(stderr, "Invalid time.\nProgram aborted.\n"); break;
  case INVALID_DATE : fprintf(stderr, "Invalid date.\nProgram aborted.\n"); break;
  case WRONG_OPTION: fprintf(stderr, "Wrong option %s format option.\n",p); break;
  }
  exit(error_code);
}

int date2int(int d,int m,int y) {
  struct tm date;
  time_t t;
  date.tm_mday = d;
  date.tm_sec=0;
  date.tm_min=0;
  date.tm_hour=0;
  date.tm_mon = m-1;
  date.tm_year = y - 1900;
  t = mktime(&date);
  return t / 86400;
}


int get_number(char *f, int *n) {
  int digits = 0;
  *n = 0;
  while (isdigit(*f)) {
    *n = *n * 10 + (*f - '0');
    digits++;
    f++;
  }
  return digits;
}

int get_d_remark(char *f, struct D_REMARK *dr) {
  int d,m,y, weeks;
  get_number(f,&y);
  f += 5;
  get_number(f,&m);
  f += 3;
  get_number(f,&d);
  f += 3;
  dr->day_start = date2int(d,m,y);
  f += get_number(f,&d) + 1;
  dr->days_count = d;
  weeks = d/7;
  if (d % 7) weeks++;
  if (! (dr->weeks = (char *) malloc(weeks))) return -2; 
  m = 0;
  while (m < weeks) {
    y = *f++;
    while(y-- > 0) {
      dr->weeks[m++] = *f;
    }
    f++;
  }
  return m;
}

void read_d_remark(int conn, char *first) {
  first = first + remark_index[conn];
  for (;;) {
    if (*first == 'D') {
      get_d_remark(++first, d_remarks+conn);
      return;
    }
    if (*first == '\0') {
      d_remarks[conn].day_start = UINT_MAX;
      conn_without_d_remark++;
      return;
    }
    while (*first++ != '\n') ;
  }
}

char *mk_path(char *p) {
  path[path_len] = '\0';
  strcat(path, p);
  return path;
}
