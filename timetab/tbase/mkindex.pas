{distribute under GPL v 2,
copyright 2002 Radim Krupicka <radim.krupicka@seznam.cz>}

program makeindex;
        type
        	fchar = file of char;
                fbyte = file of byte;

        const
		maxvar = 60000;
                maxlong= 60000;
	var
		p,k,q:longint;
        	naselvar: array [1..2000,1..4] of longint;
        	pocetvar,j,i:integer;
        	tt,rt,ct,ft:fchar;
      	    	td,cd,nd,fd:file;
                id,t2:fbyte;
                b:byte;

function nactivar(var f:file):longint;
	var     i:integer;
	        a:longint;
        	b,c:byte;
		z:word;

begin
	a:=0;
        i:=1;
        c:=0;
        repeat
                blockread(f,b,1,z);
                a := a * 128 ;
                if b >= 128 then c := b - 128
                else c := b;
                a := a + c;
        until b < 128;
        nactivar := a;

end;

procedure hledejvar(co:longint;var f:file);

	var x:byte;
            b,kon,k,p:longint;
            i,j,l,o:integer;
            a:array [1..4] of byte;
            nasel,test,konec:boolean;
            pole:array[1..maxvar] of byte;
            post:byte;
            z,pos:word;

begin
        seek(f,0);
        pocetvar:=0;
	b:=co;
	i:=1; l:=1;
	k:= 128*128*128;
        nasel:=false;
        for i:=1 to 4 do a[i]:=0;
	for i:=1 to 3 do begin
     			if ((b div k) <>0) then begin
	     			a[l] := b div k;
                        	a[l] := a[l] + 128;
                        	l:=l+1;
                	end;
        	b := b mod k;
        	k := k div 128;
        end;
      	a[l]:=b;
        blockread(f,pole,sizeof(pole),z);
        pos:=1;
        konec:=true;
        post:=1;
        kon:=1;
        while (z <> 0) do begin
                p:=filepos(f);
                i:=1;
                o:=0;

                 if pole[pos] = 0 then begin
                        if pos+1 <= z then
					pos:=pos+1
               		else begin
                        	blockread(f,pole,sizeof(pole),z);
				pos:=1;
                        end;
                        kon:=pos;
                  end;

                if pole[pos]<>0 then begin
			while (a[i] = pole[pos]) and (i <= l) do begin
                                post:=pole[pos];
                        	inc(pos);
                        	if pos > z then begin
                                        kon := kon - z;
                                        blockread(f,pole,sizeof(pole),z);
                        		pos:=1;

                		end;
                        	if (post < 128) then o:=1;
				inc(i);

                	end;
                end;
                {$B-}
                if (o = 1) and (i=l+1) and (a[i-1] = post) then begin
                        pocetvar:=pocetvar+1;
                        naselvar[pocetvar,1] := filepos(f) - z + kon -1;
                        naselvar[pocetvar,2] := filepos(f) - z + pos -1 -l;

                end;

                j := 1;
                while j <= (5 - o) do begin
        		if pole[pos] < 128 then
					inc(j);
                	inc(pos);
                       	if pos > z then begin
                       		kon := kon - z;
                               	blockread(f,pole,sizeof(pole),z);
			        if z = 0 then exit;
			        pos:=1;
                	end;
              	end;
        end;
        if pocetvar = 0 then naselvar[1,1] := -2;

end;

function hledejlong(co:longint;krok:boolean;var f:file):longint;
	var     a:array [1..4] of byte;
             pole:array [1..maxlong] of byte;
            nasel:boolean;
            i,j:integer;
            z,h:word;
            pos:longint;

begin
        pos := 1;
        if krok then
		pos := 9;
        nasel:=false;
        move(co,a,sizeof(a));
        seek(f,0);
        i:=4;
        blockread(f,pole,sizeof(pole),z);
        {$B-}
        while (not(nasel)) and (z <> 0) do begin
                while (i >= 1) and (a[i] = pole[pos]) do begin
        		dec(i);
                        pos := pos+1;
                end;
        	if i=0 then begin
			nasel := true;
                        hledejlong := filepos(f) - z + pos - 5 ;
                end;
                if krok then begin
                		if (z<>0) and ((pos + i + 12) <= z) then
					pos := pos + i + 12
                                else begin
                                        h:=z;
                                	blockread(f,pole,sizeof(pole),z);
                                        pos := pos + i + 12 - h;
                                	end;
                                end
                	else begin
				pos := pos+i;
                		if pos > z then begin
                        	pos:= pos - z;
				blockread(f,pole,sizeof(pole),z);
                        end;
                        end;
        i:=4;
        end;
if not(nasel) then hledejlong := -2;
end;

procedure nacticas(var f:file);
	var i,z:integer;
            a,d:longint;
            k:byte;
begin
	for i:=1 to pocetvar do begin
        	seek(f,naselvar[i,1]);
                while filepos(f) <= naselvar[i,2] do begin
                        blockread(f,k,1,z);
                	while k >= 128 do blockread(f,k,1,z);
                        a := nactivar(nd);
                        d := nactivar(nd);
                        blockread(f,k,1,z);
                        while k >= 128 do blockread(f,k,1,z);
                        blockread(f,k,1,z);
                        while k >= 128 do blockread(f,k,1,z);
                	naselvar[i,3]:=  naselvar[i,3] + a + d;
                        naselvar[i,4] := naselvar[i,4] + a + d;
                end;
                naselvar[i,3] := naselvar[i,3] - d;
        end;
end;

function nactilong(var f:file):longint;
var     i,z:integer;
        a:array[1..4] of byte;
        b:longint;

begin

        for i:=4 downto 1 do blockread(f,a[i],1,z);
        move(a,b,sizeof(a));
        nactilong:=b;

end;

procedure srovnej;
	var
		p: longint;
		i,j,k:integer;
begin
        for j:=1 to pocetvar - 1 do
	for i:=1 to pocetvar - 1 do
        	if naselvar[i,3] > naselvar[i+1,3] then
                	begin
                        	for k:=1 to 4 do begin
					p := naselvar[i,k];
                        		naselvar[i,k] := naselvar[i+1,k];
                        		naselvar[i+1,k] := p;
                                end;
                        end;

end;

procedure otevri(cesta:string);

begin
	assign(tt,cesta+'towns.txt');  reset(tt);
	assign(rt,cesta+'remarks.txt'); reset(rt);
	assign(ct,cesta+'conns.txt'); reset(ct);
	assign(ft,cesta+'flags.txt'); reset(ft);
	assign(td,cesta+'towns.dat'); reset(td,1);
	assign(cd,cesta+'conns.dat'); reset(cd,1);
        assign(nd,cesta+'nodes.dat'); reset(nd,1);
	assign(fd,cesta+'flags.dat'); reset(fd,1);
        assign(t2,cesta+'towns2.dat'); rewrite(t2);
        assign(id,cesta+'index.dat'); rewrite(id);
end;

procedure zavri;

begin
	close(tt);close(rt);close(ct);close(ft);
	close(td);close(cd);close(nd);close(fd);
        close(id);close(t2);
end;


begin
	otevri('');
        seek(td,0);
        seek(id,0);
        seek(t2,0);
        p:=1;
        for p:=1 to (filesize(td) div 4) do begin
                q := filepos(id);
                k := 256*256*256;
                for i:=1 to 4 do begin
                        b := q div k;
                        write(t2,b);
                        q := q mod k;
                        k := k div 256;

                end;
        	hledejvar(p,nd);

                if naselvar[1,1] = -2 then begin
                	write('Nenasel');
                        exit;
                end;
                nacticas(nd);
                for j:=1 to pocetvar do begin
                	naselvar[j,1] := hledejlong(naselvar[j,1],true,cd) - 8;
                        seek(cd,naselvar[j,1]+12);
                	q := nactilong(cd);
        		naselvar[j,3] := naselvar[j,3] + q;
                	naselvar[j,4] := naselvar[j,4] + q;
                end;
                srovnej;

                for j:=1 to pocetvar do begin
                	naselvar[j,1] := (naselvar[j,1] div 4) + 1;
			k := 128*128*128;
                        for i:=1 to 3 do begin
                                b := (naselvar[j,1] div k) + 128;
     				if (b <> 128) then write(id,b);
        			naselvar[j,1] := naselvar[j,1] mod k;
        			k := k div 128;
                        end;
                        b := naselvar[j,1];
                        write(id,b);
                end;
                b:=0;
                write(id,b);
                write(p);
        end;
        zavri;
end.