#!/usr/bin/python
#
# Wrapper for Maemo's GPS.
# Copyright 2012 Pavel Machek <pavel@ucw.cz>, GPLv2

import location
import gobject
import time

class Position:
    def __init__(self):
        self.debug = 0

    def on_fix(self, device, data):
        pass

    def on_changed(self, device, data):
        if not device.fix:
            if self.debug:
                print "no device.fix", device.fix
            return
            
        if not device.fix[1] & location.GPS_DEVICE_LATLONG_SET:
            if self.debug:
                print "no fix", device.fix
            return

        if self.debug:
            print "mode = ", device.fix[0], ("lat = %f, long = %f" % device.fix[4:6]), "accuracy = ", device.fix[6]
        # data.stop() commented out to allow continuous loop for a reliable fix
        self.fix = device.fix
        self.parse_fix()
        self.on_fix(device, data)

    # Precision in cm
    def position_loop(self, interval = location.INTERVAL_DEFAULT):
        self_ = self

        def on_error(control, error, data):
            print "location error: %d... quitting" % error
            data.quit()
 
        def on_changed(device, data):
            if not device:
                return
            self_.on_changed(device, data)
	
        def on_stop(control, data):
            data.quit()
 
        def start_location(data):
            data.start()
            return False

        self.loop = gobject.MainLoop()
        self.control = location.GPSDControl.get_default()
        self.device = location.GPSDevice()
        self.control.set_properties(preferred_method=location.METHOD_USER_SELECTED,
                                    preferred_interval=interval)
 
        self.control.connect("error-verbose", on_error, self.loop)
        self.device.connect("changed", on_changed, self.control)
        self.control.connect("gpsd-stopped", on_stop, self.loop)
 
        gobject.idle_add(start_location, self.control)
 
        self.loop.run()

    def parse_fix(self):
# mode: The mode of the fix
# fields: A bitfield representing which items of this tuple contain valid data
# time: The timestamp of the update (location.GPS_DEVICE_TIME_SET)
# ept: Time accuracy
# latitude: Fix latitude (location.GPS_DEVICE_LATLONG_SET)
# longitude: Fix longitude (location.GPS_DEVICE_LATLONG_SET)
# eph: Horizontal position accuracy (centimeters)  
# altitude: Fix altitude in meters (location.GPS_DEVICE_ALTITUDE_SET)
# double epv: Vertical position accuracy
# track: Direction of motion in degrees (location.GPS_DEVICE_TRACK_SET)
# epd: Track accuracy
# speed: Current speed in km/h (location.GPS_DEVICE_SPEED_SET)
# eps: Speed accuracy
# climb: Current rate of climb in m/s (location.GPS_DEVICE_CLIMB_SET)
# epc: Climb accuracy
        self.lat = self.fix[4]
        self.lon = self.fix[5]
        self.eph = self.fix[6]            # centimeters
        self.altitude = self.fix[7]       # meters
        self.speed = self.fix[11]	  # speed in km/h
        self.track = self.fix[9]

    def to_android(self):
        res = {}
        res["altitude"] = self.altitude
        res["provider"] = "gps"
        res["longitude"] = self.lon
        res["latitude"] = self.lat
        res["time"] = time.time() * 1000
        res["accuracy"] = self.eph
        res["speed"] = self.speed
        res["track"] = self.track
        return res

class OneShotPosition(Position):
    def get_position(self, precision, timeout = 999999):
        self.precision = precision
        self.timeout = time.time() + timeout
        self.position_loop(location.INTERVAL_1S)
        return self.fix

    def on_fix(self, device, data):
        if time.time() > self.timeout:
            data.stop()
        if self.fix[6] < self.precision:
            data.stop()

#pos = Position()
#pos.get_position(100011)
#print "I'm at", pos.lat, pos.lon
