#!/usr/bin/python
#
# Get weather forecast
# Copyright 2012 Pavel Machek <pavel@ucw.cz>, GPLv2

import os
import android

print "init"
droid = android.Android()

print "locate"
droid.startLocating()
eph = 99999999
while eph > 100000:
    (a, pos, b) = droid.getLastKnownLocation()
    if "gps" in pos: 
        pos = pos["gps"]
        eph = pos["accuracy"]
        lat = pos["latitude"]
        lon = pos["longitude"]

droid.stopLocating()

print "I'm at", lat, lon

droid.ttsSpeak("Loading.")
page = 'http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/index.html?display=var&gmap_zoom=7&opa1=0.7&opa2=0.8&nselect=7&nselect_fct=6&di=1&rep=2&add=4&update=4&lat=%f&lon=%f' % ( lat, lon )

droid.webViewShow(page)
