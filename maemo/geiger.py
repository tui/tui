#!/usr/bin/python

"""
obrazek4 -- prekroceni v red i green.


sudo ./geiger.py | tee geiger.log

https://xkcd.com/radiation/
https://medium.com/the-physics-arxiv-blog/ce9b464d68aa
http://www.radiation-watch.co.uk/?new=1
http://effbot.org/imagingbook/image.htm

...pry umi .05uSv, maji specialni foto diodu

https://play.google.com/store/apps/details?id=com.rdklein.radioactivity

They claim to sense 

100 rem = 1 Sv
1 rem = 0.01 Sv
1 mrem = 0.01 mSv = 10 uSv

1mrem/hour    somehow detectable
100mrem/hour  reliably on Nexus S
1rem/hour     reliably on Nexus 4
10rem/hour    reliably on Nexus Galaxy
100rem/hour   max they tested. (Kills human within 2 hours.)

10uSv/hour    somehow detectable

10uSv	      background dose for one day
0.42uSv/hour  background
~8uSv/hour    airplane flight

50mSv	      yearly limit for worker
2Sv	      sometimes fatal

apt-get install gstreamer-tools

uraninite

http://www.minresco.com/radioactive/radioactive03.htm

70g	50mR/hour ze vzdalenosti 1"
umi davat 30mR/hour v gamma oblasti

	umyt ruce, nedavat blizko k telu, nedychat.

KCl is useful as a beta radiation source for calibration of radiation
monitoring equipment, because natural potassium contains 0.0118% of
the isotope 40K. One kilogram of KCl yields 16350 becquerels of
radiation consisting of 89.28% beta and 10.72% gamma with 1.46083 MeV.
"""

import os
import Image
import sys
from math import sqrt

class Camera:
    def get_picture_jpeg(m, name = "/tmp/geiger.jpg"):
        os.system("/usr/bin/gst-launch v4l2camsrc device=/dev/video0 num-buffers=1 \! video/x-raw-yuv,width=2592,height=1968  \! ffmpegcolorspace \! jpegenc \! filesink location=%s > /dev/null" % name)

    def get_picture_raw(m):
        # Produced file is exactly width*height*2 bytes big.
        os.system("/usr/bin/gst-launch v4l2camsrc device=/dev/video0 num-buffers=1 \! video/x-raw-yuv,width=2592,height=1968  \! ffmpegcolorspace \! filesink location=delme.jpg")

class Process:
    def __init__(m):
        m.log = None

    def analyze(m, colors, get_val, name, debug = 0):
        if debug:
            print(name)
        #print(colors)
        sum = 0.
        total = 0
        max = 0
        for (count, val) in colors:
            v = get_val(val)
            # print(count, val, v)
            sum += v*count
            total += count
            if v > max:
                max = v
            
        average = sum / total
        if debug:
            print("...average ", average)
            print("...maximum ", max)

        sum = 0.0
        for (count, val) in colors:
            v = get_val(val)
            sum += ((v-average) * (v-average)) * count

        std_dev = sqrt(sum / total)
        if debug:
            print("...std_dev ", std_dev)

        limit = average + 7*std_dev + 15
        over_limit = 0
        for (count, val) in colors:
            v = get_val(val)
            if v > limit:
                over_limit += count

        print "%2.0f" % limit,
        if m.log:
            print >>m.log, "%2.0f" % limit,
        if debug or over_limit:
            print "...over limit", name, over_limit, "limit", limit, "max", max
            if m.log:
                print >>m.log, "...over limit", name, over_limit, "limit", limit, "max", max
        return over_limit
            
    def load_image(m, name):
        im = Image.open(name)
#        print im.format, im.size, im.mode
#        im.show()
        colors = im.getcolors(im.size[0]*im.size[1])
        debug = 0
        over = 0
        over += m.analyze(colors, lambda (x, y, z): x, "red", debug)
        over += m.analyze(colors, lambda (x, y, z): y, "green", debug)
        over += m.analyze(colors, lambda (x, y, z): z, "blue", debug)
        if m.log:
            print >>m.log, " - limits"
        print " - limits"
        return over

if not sys.argv[1:]:
    cam = Camera()
    proc = Process()
    proc.log = open('./geiger.log', 'w+')

    i = 0
    o = 0
    while 1:
        i += 1
        cam.get_picture_jpeg()
        over = proc.load_image("/tmp/geiger.jpg")
        if over:
            o += 1
            os.system("cp /tmp/geiger.jpg geiger-%d.jpg" % i)
        print("%d/%d pictures are over limits" % (o,i))
        print >>proc.log, "%d/%d pictures are over limits" % (o,i)
        proc.log.flush()

proc = Process()
for a in sys.argv[1:]:
    proc.load_image(a)
