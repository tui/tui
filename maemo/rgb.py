#!/usr/bin/python

# http://www.christianfaur.com/color/Site/Picking%20Colors.html
# 

import os
import time

class Led:
    def __init__(m):
        m.path = "/sys/class/leds/lp5523:"
        m.scale = 0.1

    def write(m, s, v):
        f = open(m.path + s + "/brightness", "w")
        f.write(str(int(v*m.scale)))
        f.close()
        
    def set(m, val):
        (r, g, b) = val 
        m.write("r", r)
        m.write("g", g)
        m.write("b", b)

class AlphaLed(Led):
    def __init__(m):
        Led.__init__(m)
        m.map = {}
        m.map[' '] = (0, 0, 0)

        m.map['i'] = (255, 255, 0)
        m.map['o'] = (255, 0, 0)
        m.map['a'] = (0, 0, 180)
        m.map['e'] = (255, 118, 0)
        m.map['u'] = (0, 154, 37)

        m.map['t'] = (83, 140, 208)
        m.map['s'] = (121, 33, 135)
        m.map['l'] = (202, 62, 94)
        m.map['h'] = (100, 100, 100)
        m.map['v'] = (178, 220, 205)
        m.map['j'] = (55, 19, 112)
        m.map['w'] = (255, 152, 213)        
        m.map['f'] = (185, 185, 185)

        m.map['d'] = (255, 200, 47)
        m.map['n'] = (12, 75, 100)
        m.map['c'] = (146, 248, 70)
        m.map['r'] = (37, 70, 25)
        m.map['k'] = (255, 255, 150)
        m.map['m'] = (205, 145, 63)
        m.map['p'] = (175, 155, 50)        
        m.map['b'] = (175, 13, 102)
        m.map['y'] = (175, 200, 74)
        m.map['v'] = (178, 220, 205)
        m.map['x'] = (0, 0, 74)        
        m.map['g'] = (235, 235, 222)
        m.map['q'] = (0, 0, 0)
        m.map['z'] = (63, 25, 12)

    def set_letter(m, let):
        m.set(m.map[let])

    def set_word(m, word):
        for l in word:
            m.set_letter(l)
            time.sleep(.5)

l = AlphaLed()
l.set((0,0,0))
time.sleep(1)
l.set((255,255,255))

l.set_word("aeiou tslhvjwf ")
        
