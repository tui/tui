# sl4a-somehow-compatible interface to maemo.
# Copyright 2012,2013 Pavel Machek <pavel@ucw.cz>, GPLv3
# http://code.google.com/p/android-scripting/wiki/ApiReference
# http://wiki.maemo.org/Phone_control

import util
import time
import os
import re
import signal
import subprocess
import sys

is_maemo = 1
try:
    import events
    import gps_maemo
    import dbus
except:
    is_maemo = 0

if not is_maemo:
    if sys.version_info[0] == 2:
        try:
            import gps
        except:
            print("Gps support unavailable")
    import collections
    Result = collections.namedtuple('Result', 'id,result,error')

def result(res, error = 0):
    if is_maemo:
        return (0, res, error)
    else:
        return Result(id=0, result=res, error=0, )

class AndroidBase:
    player = 0

    def do_vibrate(self, on):
        return 0

    def get_registration_status(self):
        return 0

    def get_signal_strength(self):
        return 0

    def phoneCallNumber(self, number):
        return

    def vibrate(self, msec = 300):
        self.do_vibrate(1)
        time.sleep(msec/1000.0)
        self.do_vibrate(0)
        return result(None)

    def readSignalStrength(self):
        # Should return some kind of map
        pass

    def getNetworkOperator(self):
        # Should return MCC+MNC
        pass

    def getNetworkType(self):
        # 2G/3G?
        pass

    def toggleRingerSilentMode(self, enabled):
        if enabled: self.profile("general")
        else:       self.profile("silent")
        return result(None)

    def batteryGetPlugType(self):
        return result(self.isBatteryCharging())

    def smsGetMessageCount(self, unread):
        return result(0)

    def startLocating(self):
        return

    def stopLocating(self):
        return
    
    def startSensing(self):
        return result(0)

    def stopSensing(self):
        return result(0)

    def parse_iwlist(self, cmd):
        scan = []
        lines = os.popen(cmd).readlines()
        for line in lines:
            if re.match(".*is down.*", line):
                return 0
            # Cell 01 - Address:
            match = re.match(".*Cell .* - Address: ([0-9A-Z:]*).*$", line)
            if match:
                ap = {}
                ap['bssid'] = match.group(1)
                # ap['ssid'] = "pavel"
                # ap['frequency'] = 2467
                # map(lambda(s): s.decode("hex"), "12:23:45:56:78:90".split(":"))
                scan = scan + [ ap ]
        return scan

    def wifiGetScanResults(self):
        # sudo aptitude install wireless-tools
        scan = self.parse_iwlist("sudo iwlist wlan0 scan 2>&1")
        if scan != 0:
            return result(scan)

        print("Wifi was down; toggling it for a scan")
        scan = self.parse_iwlist("sudo ifconfig wlan0 up; sleep 1; sudo iwlist wlan0 scan; sudo ifconfig wlan0 down")
        return result(scan)

    def makeToast(self, s):
        return

    def notify(self, title, message = ""):
        return

    def cameraCapturePicture(filename, autofocus = 1):
        return

    def ttsSpeak(self, s):
        util.say(s)

# Result(id=18, result={u'network': {u'altitude': 0, u'provider': u'network', u'longitude': 14.512464400000001, u'time': 1356223447997L, u'latitude': 50.151344600000002, u'speed': 0, u'accuracy': 698}, u'gps': {u'altitude': 361, u'provider': u'gps', u'longitude': 14.748517870903015, u'time': 1355320902000L, u'latitude': 50.004610419273376, u'speed': 0.70710676908493042, u'accuracy': 0}}, error=None)

        

class Debian_nogps(AndroidBase):
    def profile(self, s):
        if s == 'silent':
            os.system("amixer set Master mute; xset b off")
            return 0
        os.system("amixer set Master unmute; xset b on")
        return 0

    def webViewShow(self, url):
        os.system("chromium-browser '%s' &" % url)

    def getLastKnownLocation(self):
        res = {}
        pos = {}
        pos["accuracy"] = 100000
        pos["latitude"] = 50.14
        pos["longitude"] = 14.50
        pos["speed"] = 10
        pos["track"] = 270
        pos["altitude"] = 260
        res["gps"] = pos
        return result(res)

    def mediaPlay(self, file):
        if not self.playback_finished():
            self.player.send_signal(signal.SIGTERM)
        self.player = subprocess.Popen([ 'mplayer', file ], stdin=subprocess.PIPE)

    def mediaPlayPause(self):
        if self.player:
            self.player.send_signal(signal.SIGSTOP)

    def mediaPlaySeek(self):
        print("mediaPlaySeek")

    def mediaPlayStart(self):
        if self.player:
            self.player.send_signal(signal.SIGCONT)

    def playback_finished(self):
        if not self.player:
            return 1
        if self.player.poll() is None:
            return 0
        return 1

    def readBatteryPercent(self):
            s = util.pcmd("acpi -s").split(",")
            # Not all machines have battery
            if s == ['']:
                return 100
            s = s[1]	# field with percent
            s = s[:-1]	# remove percent
            return int(s)

    def isBatteryCharging(self):
            s = util.pcmd("acpi -a").split(":")
            # Not all machines have battery
            if s == ['']:
                return 1
            return s[1] == " on-line"

class Debian(Debian_nogps):
    def __init__(self):
        self.gpsd = None
        
    def startLocating(self):
        self.gpsd = gps.gps()
        self.gpsd.stream()

    def stopLocating(self):
        self.gpsd = None

    def getLastKnownLocation(self):
        res = {}
        pos = {}

        pos["accuracy"] = 100000
        pos["latitude"] = 50.15
        pos["longitude"] = 14.50
        pos["speed"] = 10
        if self.gpsd.waiting():
            report = self.gpsd.next()
            print(report)
            if report['class'] == 'TPV':
                pos["accuracy"] = 100
                pos["latitude"] = report["lat"]
                pos["longitude"] = report["lon"]
                pos["speed"] = report["speed"]
                if "track" in report:
                    pos["track"] = report["track"]
                if "alt" in report:
                    pos["altitude"] = report["alt"]

        res["gps"] = pos
        return result(res)

    
        
        
class Maemo(AndroidBase):
    def __init__(self):
        self.bus = dbus.SystemBus()
        self.gps_pid = 0
        try:
            self.sess = dbus.SessionBus()
        except:
            print("Session bus not available")

    def ttsSpeak(self, s):
        os.system("killall mediaplayer")
        util.say(s)

    def startLocating(self):
        if self.gps_pid:
            return
        self.gps_pid = os.fork()
        if not self.gps_pid:
            while 1:
                pos = gps.Position()
                pos.position_loop()

    def stopLocating(self):
        if not self.gps_pid:
            print("stopLocating when not locating?")
            return
        os.kill(self.gps_pid, 15)
        self.gps_pid = 0

    def sensorsReadAccelerometer(self):
        file = open("/sys/class/i2c-adapter/i2c-3/3-001d/coord")
        line = file.readline()
        res = [int(s)/110.0 for s in line.split(" ")]
        return result(res)


    def smsGetMessageCount(self, unread):
        return result(events.sms_count(unread))

    def dbus_i(self, s1, s2, s3):
        return dbus.Interface(self.bus.get_object(s1, s2), s3)

    def dbus_i_nokia(self, s1, s2, s3):
        return self.dbus_i('com.nokia.'+s1, '/com/nokia/'+s2, s3)

    def phoneCallNumber(self, number):
        # dbus-send --system --type=method_call --print-reply ...
        i = self.dbus_i_nokia('csd', 'csd/call', 'com.nokia.csd.Call')
        i.CreateWith(str(number), dbus.UInt32(0))
    
    def dbus_i_net(self):
        return self.dbus_i_nokia('phone.net', 'phone/net', 'Phone.Net')

    # These only work from console. ouch.
    def get_signal_strength(self):
        i = self.dbus_i_net()
        return i.get_signal_strength()

    def get_registration_status(self):
        i = self.dbus_i_net()
        return i.get_registration_status()

    def do_vibrate(self, on):
        i = self.dbus_i_nokia('mce', 'mce/request', 'com.nokia.mce.request')
        if on:
            i.req_vibrator_pattern_activate(str("PatternIncomingCall"))
        else:
            i.req_vibrator_pattern_deactivate(str("PatternIncomingCall"))

    def dbus_s(self, s1, s2, s3):
        return dbus.Interface(self.sess.get_object(s1, s2), s3)

    def dbus_s_nokia(self, s1, s2, s3):
        return self.dbus_s('com.nokia.'+s1, '/com/nokia/'+s2, s3)

    def profile(self, s):
        # Use general/silent
        i = self.dbus_s_nokia('profiled', 'profiled', 'com.nokia.profiled')
        i.set_profile(str(s))

    def webViewShow(self, url):
        # Android can do many different urls, apparently.
        i = self.dbus_i_nokia('osso_browser', 'osso_browser/request', 'com.nokia.osso_browser')
        i.load_url(str(url))

    def i_notifications(self):
        return self.dbus_i('org.freedesktop.Notifications', '/org/freedesktop/Notifications', 'org.freedesktop.Notifications')

    def makeToast(self, s):
        # Message that disappears itself
        i = self.i_notifications()
        i.SystemNoteInfoprint(s)

    def notify(self, title, message = ""):
        # User has to click to hide message
        i = self.i_notifications()
        i.SystemNoteDialog(title + " " + message, 0, "OK")

    def mediaPlay(self, file):
        i = self.dbus_s_nokia('mediaplayer', 'mediaplayer', 'com.nokia.mediaplayer')
        i.mime_open(str("file://"+file))
        pass

    def dbus_gstrenderer(self):
        return self.dbus_s_nokia('mafw.renderer.Mafw-Gst-Renderer-Plugin.gstrenderer', 
                              'mafw/renderer/gstrenderer',
                              'com.nokia.mafw.renderer')

# dbus-send --print-reply --dest=com.nokia.mediaplayer /com/nokia/mediaplayer com.nokia.mediaplayer.mime_open string:"file:///$1"

# dbus-send --system --type=method_call --dest=com.nokia.osso_browser /com/nokia/osso_browser/request com.nokia.osso_browser.load_url string:"google.com"

    def mediaPlayPause(self):
        i = self.dbus_gstrenderer()
        i.stop()

    def mediaPlaySeek(self):
        pass

    def mediaPlayStart(self):
        i = self.dbus_gstrenderer()
        i.resume()

        # Nokia media player can also do:
        # stop, next, previous, play

    def playback_finished(self):
        return 0

# Open note file
# dbus-send --print-reply --dest=com.nokia.osso_notes /com/nokia/osso_notes com.nokia.osso_notes.mime_open string:/home/user/MyDocs/.documents/file_name

# Open folder
# dbus-send --print-reply --dest=com.nokia.osso_filemanager /com/nokia/osso_filemanager com.nokia.osso_filemanager.open_folder string:/home/user/MyDocs/

# Forced reboot
# dbus-send --system --type=method_call --print-reply --dest=com.nokia.mce "/com/nokia/mce/request" com.nokia.mce.request.req_reboot

# Enable FM transmitter
# /usr/bin/fmtx_client -p 1

    def cameraCapturePicture(filename, autofocus = 1):
        os.system("/usr/bin/gst-launch v4l2camsrc device=/dev/video0 num-buffers=1 \! video/x-raw-yuv,width=2592,height=1968  \! ffmpegcolorspace \! jpegenc \! filesink location="+filename)

    def readBatteryPercent(self):
            s = util.pcmd("hal-device bme | grep charge_level.percentage").split(" ")
            return int(s[4])

    def isBatteryCharging(self):
            s = util.pcmd("hal-device bme | grep maemo.rechargeable.charging_status").split(" ")
            if s[4] == "'full'": return 1
            if s[4] == "'on'": return 1
            return 0

    def getLastKnownLocation(self):
        pos = gps.OneShotPosition()
        pos.get_position(10000, -1)
        res = {}
        res["gps"] = pos.to_android()
        #print("getLastKnown...: ", res)
        return result(res)

def Android():
    if is_maemo:
        return Maemo()
    else:
        return Debian()

if __name__ == "__main__":
    print("Self test:")
    droid = Android()
    if 0:
        print("Url")
        print(droid.open_url("google.com"))
    if 0:
        print("Call")
        print(droid.phoneCallNumber("800123456"))
    if 0:
        print("Vibrate")
        droid.vibrate()
        time.sleep(1)
        droid.vibrate(100)
    if 0:
        print("Signal")
        print(droid.get_signal_strength())
        print("Status")
        droid.get_registration_status()
    if 0:
        print("Profile")
        print(droid.profile("general"))
        time.sleep(1)
        print(droid.profile("silent"))
        time.sleep(1)
        print(droid.profile("general"))
    if 0:
        print("Notifications")
        print(droid.makeToast("Hello world"))
        time.sleep(1)
        print(droid.notify("Blocking the world"))
    if 1:
        print(droid.mediaPlay("/home/user/MyDocs/mp3/asonance/DvaHavrani/09 Susan Cooper.mp3"))
        time.sleep(5)
        print("Pausing")
        print(droid.mediaPlayPause())
        time.sleep(5)
        print("Playing")
        print(droid.mediaPlayStart())


# >>> droid.getCellLocation()
# Result(id=4, result={u'lac': 16434, u'cid': 1967}, error=None)

# >>> droid.getNetworkOperator()
# Result(id=5, result=u'23001', error=None)

# Po batteryStartMonitoring
# >>> droid.batteryGetStatus()
# Result(id=15, result=4, error=None)

# >>> droid.getLastKnownLocation()

# >>> droid.readBatteryData()
# Result(id=19, result={u'status': 4, u'health': 2, u'plugged': 0}, error=None)


