#!/usr/bin/env python 
# GPLv2
# Thanks to http://wiki.maemo.org/PyMaemo/Using_Location_API

import socket
import time
import android
import getopt
import sys

class Base:
    def __init__(s):
        s.lat = 50.028635
        s.lon = 14.613579
        s.speed = 0

    def get_position(s):
        (a, pos, b) = droid.getLastKnownLocation()
        if "gps" in pos:
            pos = pos["gps"]
            print("GPS: %f deg %f deg %f ? %f m %f km/h\n" % (pos["latitude"], pos["longitude"], pos["accuracy"], pos["altitude"], 0))

        s.lat = pos["latitude"]
        s.lon = pos["longitude"]
        s.speed = pos["speed"]
        return (s.lat, s.lon)

    def handle(s, client, data):
        print "Got request: >", data, "<"
        
        if s.handle_letter(client, data[0]):
            return 1

        client.send("Error\r\n") 

    def handle_client(s, client):
        while 1:
            data = client.recv(1024)
            if data == '':
                print "Client terminated"
                break
            s.handle(client, data)

    def receive_loop(s):
        print "Listening on network"
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('', s.port))
        sock.listen(5) 
        while 1: 
            client, address = sock.accept() 
            print "Client connected"
            s.handle_client(client)
            client.close()

    def run(s):
        droid.startLocating()
        s.receive_loop()
        droid.stopLocating()

class Gpsd(Base):
    port = 2947

    def handle_letter(s, client, l):
        if l == 'p':
            client.send("GPSD,P=%f %f\r\n" % s.get_position())
            return 1
        if l == 'v':
            # Maemo gives speed in km/h, GPSD wants it in knots
            client.send("GPSD,V=%f\r\n" % (s.speed * 0.539956803))
            return 1
        if l == 'y':
            client.send("GPSD,Y=MID4 1118327704.280 8:23 6 84 0 0:28 7 160 0 0:8 66 189 45 1:29 13 273 0 0:10 51 304 0 0:4 15 199 34 1:2 34 241 41 1:27 71 76 42 1:\r\n")
            return 1
        return 0

class Nmead(Base):
    port = 2948

    def write_nmea(s, client, t):
        csum = 0
        for c in t:
            csum ^= ord(c)
        client.send("$%s*%02x\r\n" % (t, csum))

    def format_one(s, mask, val):
        print "val ", val
        if val < 0:
            val = -val
        ret = mask % int(val)
        val -= int(val)
        val *= 60
        ret += "%02d." % int(val)
        val -= int(val)
        ret += "%04d" % int(val*10000)
        print "ret ", ret
        return ret

    def format(s):
        ret = s.format_one("%02d", s.lat)
        if s.lat >= 0:
            ret += ",N,"
        else:
            ret += ",S,"
        ret += s.format_one("%03d", s.lon)
        if s.lon >= 0:
            ret += ",E"
        else:
            ret += ",W"
        return ret

    def handle_client(s, client):
        while 1:
            s.write_nmea(client, "GPGGA,010203.444,%s,1,04,,,M,,M,0000" % s.format())
            s.get_position()
            time.sleep(1)

def help():
    print "No help"
    sys.exit(1)

droid = android.Android()
d = Gpsd()
try:
    opts, args = getopt.getopt(sys.argv[1:], "n", ["nmea"])
except getopt.GetoptError:
    help()
for opt, arg in opts:
    if opt == '-h':
        help()
    elif opt in ("-n", "--nmea"):
        d = Nmead()

print "Starting network server"
d.run()

