#!/usr/bin/python

""" (C) 2009 by Kurt Fleisch
    (C) 2012 by Pavel Machek

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys, os.path, sqlite3

EL_PATH = "/home/user/.rtcom-eventlogger/el-v1.db"

def sms_count(unread):
    conn = sqlite3.connect(EL_PATH)
    cur = conn.cursor()
    cur.execute("select * from events")
    count = 0
    for row in cur: 
      if row[1] == 3:
        if not unread or not row[6]:
          count += 1 
    cur.close()
    return count

def listall():
  if os.path.isfile(EL_PATH):
    conn = sqlite3.connect(EL_PATH)
    cur = conn.cursor()
    cur.execute("select * from events")
    for row in cur: 
      if row[1] == 3:
          if row[6] == 0:
              print("sms ", "read : ", row[6], " from ", row[13])
      elif row[1] == 1:
          if row[2] == 2:
              # Missed calls; but it is impossible to tell if they are "new" missed calls
              print("is_read ", row[6], " flags ", row[8], "call ", row[11], " from ", row[13])
#        print row
      elif row[1] == 2:
#        print "jabber "
          pass
      else:
        print(row)
    cur.close()
  else:
    print("Unable to open eventlogger-archive file")
    sys.exit(1)

if __name__ == "__main__":
    listall()
