#!/usr/bin/env python 

import socket

lat = 50.1434
lon = 14.54679

def get_position():
    global lat, lon
    lat = lat - 0.0001
    lon = lon + 0.0000
    return (lat, lon)

def handle_letter(client, l):
    if l == 'p':
        client.send("GPSD,P=%f %f\r\n" % get_position())
        return 1
    if l == 'v':
        # in knots
        client.send("GPSD,V=%f\r\n" % 70.0)
        return 1
    if l == 'y':
        client.send("GPSD,Y=MID4 1118327704.280 8:23 6 84 0 0:28 7 160 0 0:8 66 189 45 1:29 13 273 0 0:10 51 304 0 0:4 15 199 34 1:2 34 241 41 1:27 71 76 42 1:\r\n")
        return 1
    return 0

def handle(client, data):
    print "Got request: >", data, "<"
        
    if handle_letter(client, data[0]):
        return 1

    client.send("Error\r\n") 

host = '' 
port = 2947 
backlog = 5 
size = 1024 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((host,port)) 
s.listen(backlog) 
while 1: 
    client, address = s.accept() 
    while 1:
        data = client.recv(size)
        if data == '':
            print "Client terminated"
            break
        handle(client, data)
    client.close()
