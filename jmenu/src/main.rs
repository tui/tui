extern crate sdl2;

use std::env;
use std::path::Path;
use std::path::PathBuf;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::TextureQuery;
use sdl2::render::Canvas;
use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::surface::Surface;
 
static SCREEN_WIDTH: u32 = 800;
static SCREEN_HEIGHT: u32 = 600;

// handle the annoying Rect i32
macro_rules! rect(
    ($x:expr, $y:expr, $w:expr, $h:expr) => (
        Rect::new($x as i32, $y as i32, $w as u32, $h as u32)
    )
);

// Scale fonts to a reasonable size when they're too big (though they might look less smooth)
fn get_centered_rect(rect_width: u32, rect_height: u32, cons_width: u32, cons_height: u32) -> Rect {
    let wr = rect_width as f32 / cons_width as f32;
    let hr = rect_height as f32 / cons_height as f32;

    let (w, h) = if wr > 1f32 || hr > 1f32 {
        if wr > hr {
            println!("Scaling down! The text will look worse!");
            let h = (rect_height as f32 / wr) as i32;
            (cons_width as i32, h)
        } else {
            println!("Scaling down! The text will look worse!");
            let w = (rect_width as f32 / hr) as i32;
            (w, cons_height as i32)
        }
    } else {
        (rect_width as i32, rect_height as i32)
    };

    let cx = (SCREEN_WIDTH as i32 - w) / 2;
    let cy = (SCREEN_HEIGHT as i32 - h) / 2;
    rect!(cx, cy, w, h)
}

struct Params {
    font_path: PathBuf,
}

struct State {
    mouse: i32,
    item: i32,
}

fn draw(params: &Params, state: &State, canvas: &mut Canvas<sdl2::video::Window>) -> Result<(), String> {
    let texture_creator = canvas.texture_creator();
    let ttf_context = sdl2::ttf::init().map_err(|e| e.to_string())?;
    // Load a font
    let mut font = ttf_context.load_font(params.font_path.as_path(), 128)?;

    font.set_style(sdl2::ttf::FontStyle::NORMAL);

    canvas.set_draw_color(Color::RGBA(195, 217, 255, 255));
    canvas.clear();

    // If the example text is too big for the screen, downscale it (and center irregardless)
    //let padding = 64;
    //let TextureQuery { width, height, .. } = texture.query();
    //println!("Text is {} {}", width, height);
    //let target = get_centered_rect( width, height + y, SCREEN_WIDTH - padding, SCREEN_HEIGHT - padding);

    // render a surface, and convert it to a texture bound to the canvas
    let surface = font
        .render("Main menu")
        .blended(Color::RGBA(0, 0, 0, 255))
        .map_err(|e| e.to_string())?;
    
    let texture = texture_creator
        .create_texture_from_surface(&surface)
        .map_err(|e| e.to_string())?;
    let target = rect!(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/4);
    canvas.copy(&texture, None, Some(target))?;

    let items = vec!(&"Speedbrake: off", &"Flaps UP ______", &"Flaps DOWN ______", &"Gear: up ______" );
    let mut pos = SCREEN_HEIGHT/4;
    let mut item = 0;
    let step = SCREEN_HEIGHT/8;
    for i in items {
        let mut color = Color::RGBA(0, 0, 0, 255);
	if state.item == item {
	   color = Color::RGBA(255, 0, 0, 255);
	}
        let surface = font
    	    .render(i)
            .blended(color)
 	    .map_err(|e| e.to_string())?;
    
        let texture = texture_creator
    	    .create_texture_from_surface(&surface)
            .map_err(|e| e.to_string())?;
 	let target = rect!(0, pos, 800, step);
 	canvas.copy(&texture, None, Some(target))?;
	pos += step;
	item += 1;
    }

    let surface = font
        .render(">")
        .blended(Color::RGBA(255, 0, 0, 255))
        .map_err(|e| e.to_string())?;
    
    let texture = texture_creator
        .create_texture_from_surface(&surface)
        .map_err(|e| e.to_string())?;
    let target = rect!(0, (state.mouse/100)+300, 100, 100);
    canvas.copy(&texture, None, Some(target))?;

    canvas.present();

    Ok(())
}

fn run(params: &Params) -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let video_subsys = sdl_context.video()?;
    let joystick_subsystem = sdl_context.joystick()?;

    let available = joystick_subsystem
        .num_joysticks()
        .map_err(|e| format!("can't enumerate joysticks: {}", e))?;

    println!("{} joysticks available", available);

    // Iterate over all available joysticks and stop once we manage to open one.
    let mut joystick = (0..available)
        .find_map(|id| match joystick_subsystem.open(id) {
            Ok(c) => {
                println!("Success: opened \"{}\"", c.name());
                Some(c)
            }
            Err(e) => {
                println!("failed: {:?}", e);
                None
            }
        })
        .expect("Couldn't open any joystick");

    // Print the joystick's power level
    println!(
        "\"{}\" power level: {:?}",
        joystick.name(),
        joystick.power_level().map_err(|e| e.to_string())?
    );

    let window = video_subsys
        .window("Joystick Menu", SCREEN_WIDTH, SCREEN_HEIGHT)
        .position_centered()
        .build()
        .map_err(|e| e.to_string())?;

    // Enable gamepad events when running in background
    println!("ALLOW_BACKGROUND_EVENTS={}", sdl2::hint::set("SDL_JOYSTICK_ALLOW_BACKGROUND_EVENTS", "1"));

    let mut canvas = window.into_canvas().build().map_err(|e| e.to_string())?;
    let mut state = State { mouse: 0, item: 0 };

    draw(params, &state, &mut canvas)?;

    'mainloop: loop {
        for event in sdl_context.event_pump()?.wait_iter() {
            match event {
                Event::KeyDown { keycode: Some(Keycode::Escape), .. }
                | Event::Quit { .. } => break 'mainloop,
		
		Event::KeyDown { keycode: Some(Keycode::W), .. } |
		Event::JoyButtonDown { button_idx: 18, .. } => { state.item -= 1; }
		Event::KeyDown { keycode: Some(Keycode::S), .. } |
		Event::JoyButtonDown { button_idx: 20, .. } => { state.item += 1; }
 		Event::JoyAxisMotion { axis_idx: 5, value: val, .. } => { state.mouse = val as i32; }
		Event::JoyButtonDown { button_idx: 4, .. } => { println!("Select!"); }
 		Event::JoyAxisMotion { axis_idx, value: val, .. } => {
 		    // Axis motion is an absolute value in the range
 		    // [-32768, 32767]. Let's simulate a very rough dead
                    // zone to ignore spurious events.
 		    let dead_zone = 10_000;
 		    if val > dead_zone || val < -dead_zone {
                       println!("Axis {} moved to {}", axis_idx, val);
 		       }
 		}
		Event::JoyButtonDown { button_idx, .. } => {
 		    println!("Button {} down", button_idx);
 		}
 		Event::JoyButtonUp { button_idx, .. } => {
                    println!("Button {} up", button_idx);
                }
 		Event::JoyHatMotion { hat_idx, state, .. } => {
 		    println!("Hat {} moved to {:?}", hat_idx, state)
 		}
                _ => {}
            }
 	    draw(params, &state, &mut canvas)?
        }
    }

    Ok(())
}

fn main() -> Result<(), String> {
    let args: Vec<_> = env::args().collect();
    let p: Params = Params{ font_path: PathBuf::from(&"/usr/share/games/renpy/launcher/game/fonts/NanumGothic.ttf") };

    println!("linked sdl2_ttf: {}", sdl2::ttf::get_linked_version());

    run(&p)?;

    Ok(())
}
