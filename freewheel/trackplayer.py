#!/usr/bin/python3

from libbike import Bike
import os
import subprocess

class Player(Bike):
    path = "~/.mplayer/trackplayer"

    def __init__(m):
        m.aspeed = -1
        super().__init__()

    def set_speed(m, v):
        if (m.aspeed != v):
            print("Speed: ", v)
            m.proc.stdin.write(b"speed_set %f\n" % v)
            m.proc.stdin.flush()
        m.aspeed = v

    def summary(m):
        kph = m.speed * 3.6
        if kph > 25:
            mv = 4.5
        elif kph > 23:
            mv = 1.25
        elif kph > 20:
            mv = 1.
        elif kph > 5:
            mv = kph/20
        else:
            mv = 0
        m.set_speed(mv)
        super().summary()

    def run(m):
        cmd = ["mplayer", "ride.mp4", "-slave" ]
        print("Launching")
        with open('/dev/null', 'w') as dev_null:
            m.proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=dev_null, stderr=dev_null)
        print("Done")
        super().run()

if __name__ == "__main__":
    w = Player()
    w.run()


