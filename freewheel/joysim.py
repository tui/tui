#!/usr/bin/env python3
#
# This example shows how to 'filter' device events from a device node.
# While real filtering is not possible, we can duplicate the input device as
# a new virtual input device and replay all events of the input events on
# our virtual one.

import sys
import libevdev
from libbike import Bike
import threading

speed = -1
message = ""

class MyBike(Bike):
    def update(m, km, kph):
        global speed, message
        speed = kph
        print("speed: ", kph, message)
        message = ""

class Sim:
    def run(m):
        path = "/dev/jssrc"
        code_from = libevdev.evbit("ABS_Y")
        code_to = libevdev.evbit("ABS_Y")

        print('Remapping {} to {}'.format(code_from, code_to))

        fd = open(path, 'rb')
        d = libevdev.Device(fd)
        d.grab()

        # create a duplicate of our input device
        # 76  -- min, 227 -- middle,  400 -- max
        ax_min = 76
        ax_thr = 170
        ax_mid = 210
        ax_max = 400
        abs = libevdev.InputAbsInfo(minimum=ax_min, maximum=ax_max)
        d.enable(libevdev.EV_ABS.ABS_Y, abs)
        d.name = "Bike passthrough"
        uidev = d.create_uinput_device()
        print('Device is at {}'.format(uidev.devnode))

        while True:
            for e in d.events():
                global message
                # change any event with our event code to
                # the one we want to map to, but pass all other events
                # through
                if e.code == code_from:
                    v = e.value
                    lim = ""
                    message = "idle"
                    if v < ax_mid:
                        p = (ax_mid - v) / (ax_mid - ax_thr)
                        if p > 1:
                            p = 1
                        message = "boost"
                        if p > (speed / 20):
                            lim = "### LIMIT ###"
                            message = "LIMIT \a"
                            p = speed / 20
                        if p < 0:
                            p = 0
                        v = (1-p) * (ax_mid - ax_min) + ax_min
                        v = int(v)
                        print("Boost!    ", p, v, speed, lim)
                    else:
                        print("Norm.     ", v, speed)
                    e = libevdev.InputEvent(code_to, v)
                    
                uidev.send_events([e])

def run_sim():
    s = Sim()
    s.run()

if __name__ == "__main__":
    if len(sys.argv) < 1:
        print("Usage: {} /dev/input/eventX <from> <to>".format(sys.argv[0]))
        print("   where <from> and <to> are event codes, e.g. REL_X")
        sys.exit(1)
    thr = threading.Thread(target=run_sim, name="bike reading")
    thr.start()
    w = MyBike()
    w.run()
