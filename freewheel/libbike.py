#!/usr/bin/env python3

# https://python-libevdev.readthedocs.io/en/latest/examples.html

import sys
import libevdev
import time
import signal

class TimeoutException(Exception):
    def __init__(self, *args, **kwargs):
        pass

def alarm_callback(v, x):
    print("Callback")
    raise TimeoutException()

class Bike():
    def print_capabilities(m, l):
        v = l.driver_version
        print("Input driver version is {}.{}.{}".format(v >> 16, (v >> 8) & 0xff, v & 0xff))
        id = l.id
        print("Input device ID: bus {:#x} vendor {:#x} product {:#x} version {:#x}".format(
            id["bustype"],
            id["vendor"],
            id["product"],
            id["version"],
        ))
        print("Input device name: {}".format(l.name))
        print("Supported events:")

        for t, cs in l.evbits.items():
            print("  Event type {} ({})".format(t.value, t.name))

            for c in cs:
                if t in [libevdev.EV_LED, libevdev.EV_SND, libevdev.EV_SW]:
                    v = l.value[c]
                    print("    Event code {} ({}) state {}".format(c.value, c.name, v))
                else:
                    print("    Event code {} ({})".format(c.value, c.name))

                if t == libevdev.EV_ABS:
                    a = l.absinfo[c]
                    print("       {:10s} {:6d}".format('Value', a.value))
                    print("       {:10s} {:6d}".format('Minimum', a.minimum))
                    print("       {:10s} {:6d}".format('Maximum', a.maximum))
                    print("       {:10s} {:6d}".format('Fuzz', a.fuzz))
                    print("       {:10s} {:6d}".format('Flat', a.flat))
                    print("       {:10s} {:6d}".format('Resolution', a.resolution))

        print("Properties:")
        for p in l.properties:
            print("  Property type {} ({})".format(p.value, p.name))

    def print_event(m, e):
        print("Event: time {}.{:06d}, ".format(e.sec, e.usec), end='')
        if e.matches(libevdev.EV_SYN):
            if e.matches(libevdev.EV_SYN.SYN_MT_REPORT):
                print("++++++++++++++ {} ++++++++++++".format(e.code.name))
            elif e.matches(libevdev.EV_SYN.SYN_DROPPED):
                print(">>>>>>>>>>>>>> {} >>>>>>>>>>>>".format(e.code.name))
            else:
                print("-------------- {} ------------".format(e.code.name))
        else:
            print("type {:02x} {} code {:03x} {:20s} value {:4d}".format(e.type.value, e.type.name, e.code.value, e.code.name, e.value))

    def tick(m):
        m.speed = m.factor / (time.time() - m.last)
        m.summary()

    def handle_event(m, e):
        if e.type != libevdev.EV_KEY:
            return
        if e.code.name != "BTN_MIDDLE":
            return
        if e.value != 1:
            return
        m.pulses += 1
        #m.now = e.sec + e.usec / 1000000
        m.now = time.time()
        m.speed = m.factor / (m.now - m.last)
        m.summary()
        #m.print_event(e)
        m.last = m.now

    def update(m, km, kph):
        pass

    def summary(m):
        km = (m.factor * m.pulses / 1000);
        kph = (m.speed * 3.6)
        m.update(km, kph)
        print("%.3f km" % km, "%.1f km/h" % kph)
        if m.pulses > m.log_pulses + 100:
            m.log.write("utime=%f cycle_pulses=%d\n" % (time.time(), m.pulses - m.log_pulses))
            m.log_pulses = m.pulses
            m.log.flush()
            
    def __init__(m):
        m.factor = 6.0
        m.pulses = 0
        m.log_pulses = 0
        m.speed = 0.
        m.last = time.time()
        m.log = None

    def run(m):
        path = "/dev/bike"
        m.log = open("freewheel.egt", "a")
        try:
            signal.signal(signal.SIGALRM, alarm_callback)
            with open(path, "rb") as fd:
                dev = libevdev.Device(fd)
                m.print_capabilities(dev)
                print("# Waiting for events")

                # dev.grab()
                while True:
                    try:
                        signal.alarm(2)
                        for e in dev.events():
                            m.handle_event(e)
                            signal.alarm(2)
                        signal.alarm(0)
                    except libevdev.EventsDroppedException:
                        for e in dev.sync():
                            m.handle_event(e)
                    except TimeoutException:
                        m.tick()
                        print("Got signal")

        except KeyboardInterrupt:
            pass
        except IOError as e:
            import errno
            if e.errno == errno.EACCES:
                print("Insufficient permissions to access {}".format(path))
            elif e.errno == errno.ENOENT:
                print("Device {} does not exist".format(path))
            else:
                raise e

if __name__ == "__main__":
    w = Bike()
    w.run()
