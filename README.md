Debian GNU/Linux on Nokia N900

(This git tree contains also code for experiments on old Siemens SX1,
OpenMoko, and for N900 running Maemo).

You'll want to install U-boot on your n900.

For kernel build, crosscompiler is preffered. I'm using eldk 5.4,
armv7a version.  Qemu is not needed.

(I'm buildling libcmtspeechdata	natively on n900. It takes few seconds
so that's ok).

Next step is debian install ( something like
https://wiki.debian.org/n900-wheezy-armhf , it would be good to create
up-to-date instructions).

Next steps are next ;-).

Basic system is working, power management needs solving, I'm working
on voice call quality. Pulseaudio is involved :-(.

Kernel hints

N900 does not properly reset hardware on reset.

1) When booting using 0xffff, always remove battery before boot. Start
0xffff, unplug USB, remove battery, plug USB, insert battery.

Debian packages need

python3-pip

2) installing on vanilla N900.

Some howto. Have not actually used it.

http://elektranox.org/n900/installation.html

install "u-boot with kernel 2.6.28-omap1", ...kernel-power.

/etc/bootmenu.d/
u-boot-update-bootmenu

/data/l/linux-n900$ iscp arch/arm/boot/zImage-dtb root@10.0.0.105:/boot/zImage-dtb-4.5-rc0

3) getting phonecalls to work

ofono should work on Debian, out of the box. You can use python
scripts in ofono/(tests?) to verify it works for you. Then, put tui
into /my/tui, and libcmtspeechdata in /my/libcmtspeechdata. Boot the
system into X, run tui/ofone/xb and tui/ofone/xa scripts, and ofone
should come up, allowing voice calls. Test sms first, as they do not
depend on pulseaudio. Voice does not yet work for me on Debian 8.



https://gitlab.com/libcmtspeechdata/libcmtspeechdata


--

Making it fit:

https://wiki.debian.org/ReduceDebian#Reducing_the_size_of_the_Debian_Installation_Footprint
https://wiki.debian.org/ListInstalledPackages