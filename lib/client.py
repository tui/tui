#!/usr/bin/python
# aptitude install python-gps, there's python3-gps
# http://catb.org/gpsd/libgps.html

from __future__ import print_function

import sys
import gps
import math
import time

# Unfortunately, "eph" from gps3 does not seem to be passed here.

class GpsQuality:
    def __init__(m):
        m.mode = 0
        m.sats = []
        m.quality = 0
        m.start = time.time()
        m.last = time.time()
        m.quit_on_acquire = False
        m.verbose = False

    def calc_sats(m):
        m.num_sats = 0
        m.num_used = 0
        m.num_medium = 0
        m.num_strong = 0
        m.num_weak = 0        
        m.num_known = 0
        for sat in m.sats:
            m.num_sats += 1
            if sat['used']:
                m.num_used += 1
            if sat['ss'] > 70 and sat['el'] > 30:
                m.num_strong += 1
            if sat['ss'] > 50 and sat['el'] > 0:
                m.num_medium += 1
            # gps3 seems to report "no signal" as 15.36%
            if sat['ss'] > 16:
                m.num_weak += 1
            if sat['el'] > 0:
                m.num_known += 1
                

    def no_signal(m):
        m.calc_sats()
        # On n900, "used" reporting does not seem to be passed from gps3 through gpsd
        bonus = m.num_used * .0
        m.quality = bonus
        bonus = m.num_sats * .005
        if m.verbose:
            print(m.num_sats, "sats bonus", bonus)
        m.quality += bonus
        bonus = m.num_strong * 0.08
        if m.verbose:
            print(m.num_strong, "strong sats bonus", bonus)
        m.quality += bonus
        bonus = m.num_medium * 0.04
        if m.verbose:
            print(m.num_medium, "medium sats bonus", bonus)
        m.quality += bonus
        bonus = m.num_weak * 0.01
        if m.verbose:
            print(m.num_weak, "weak sats bonus", bonus)
        m.quality += bonus
        bonus = m.num_known * 0.02
        if m.verbose:
            print(m.num_known, "known sats bonus", bonus)
        m.quality += bonus
        m.quality *= .5
        if m.quality > .5:
            m.quality = .5
        return '%d/%d sats' % (m.num_strong, m.num_sats)

    def fix2(m, report):
        m.calc_sats()
        m.quality = m.num_used * .1
        m.quality += m.num_sats * .02
        m.quality += m.num_strong * 0.04
        m.quality += m.num_medium * 0.03
        if m.quality < .51:
            m.quality = .51
        if m.quality > .60:
            m.quality = .60
        return ''

    def fix3(m, report):
        m.calc_sats()
        m.quality = .65
        if 'epv' in report:
            err = report['epv']
            print("Error epv ", report['epv'] )
        if 'epx' in report and 'epy' in report:
            err = report['epx'] + report['epy']
            bonus = .04 * (7 - math.log(err))
            m.quality += bonus
            print("Error ", report['epx'], report['epy'] , " bonus:", bonus)
        bonus = (m.num_used - 4) * 0.1
        print("Sattelites ", m.num_used, " bonus", bonus)
        m.quality += bonus
        if m.quality > 1:
            m.quality = 1
        if m.quality < .61:
            m.quality = .61
        return ''
    
    def print_report(m, report, t):
        q = int(m.quality * 20)
        s = '%4d %3d%% ' % (time.time() - m.start, int(m.quality * 100))
        s += 'X'*q + '.'*(20-q)
        s += ' '
        s += t
        if m.mode >= 2:
            if 'lat' in report:
                s += '%f %f ' % (report['lat'], report['lon'])
            else:
                s += '!!!'

            if m.mode == 3:
                if 'alt' in report and report['alt'] != 32768.:
                    s += '%f m' % report['alt']
                    if m.quit_on_acquire:
                        sys.exit(0)
                else:
                    s += '!!!'
            else:
                s += '???'
        print(s)
    
    def cycle(m, report):
        if report['class'] == 'TPV':
            m.last = time.time()
            m.mode = report['mode']
            #print(report['lat'], report['lon'], report['mode'])
            #print(report)
            #if 'epx' in report:
            #    print("error: ", report['epx'])
            # 'track', 'speed', 'time', 'alt'
            if m.mode < 2:
                t = m.no_signal()
            elif m.mode == 2:
                t = m.fix2(report)
            elif m.mode == 3:
                t = m.fix3(report)
            else:
                print('Unknown mode', m.mode)

            m.print_report(report, t)
            return
        if report['class'] == 'SKY':
            m.sats = report['satellites']
            if time.time() - m.last < 2:
                return
            t = m.no_signal()
            m.print_report([], t)
            return
        print("unknown report ", report['class'])

    def run(m):
        session = gps.gps(host="localhost", port="2947")
        session.stream(flags=gps.WATCH_JSON)
        while True:
            report = None
            while session.waiting():
                report = session.next()
            if not report:
                time.sleep(.1)
                continue
            m.cycle(report)
        del session

if __name__ == "__main__":
    q = GpsQuality()
    if len(sys.argv) > 1:
        for s in sys.argv:
            if s == "-q":
                q.quit_on_acquire = True
    q.run()

        
