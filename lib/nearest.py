import util
import time

class Place(util.FixPos):
    def __init__(m, lat, lon, name = "(unset)"):
        m.lat = lat
        m.lon = lon
        m.name = name
        
    def fmt(m):
        return "%f %f %s" % (m.lat, m.lon, m.name)

class Data:
    def __init__(m):
        m.load_builtin()

    def load_builtin(m):
        prague = Place(50.1, 14.3, "Prague")

        kbely = Place(50.14, 14.34, "Kbely")
        ricany = Place(49.993, 14.681, "Ricany")
        zernovka = Place(50.00, 14.75, "Zernovka")

        poi = []

        Hotel = Place(52.50737, 13.36393, 'Hotel')
        poi += [ Hotel ]
        #Conference = Place(51.255830, 6.740950, 'Conference')
        #poi += [ Conference ]

        c1 = Place(52 + 30.957/60., 13 + 21.162/60., 'c1')
        poi += [ c1 ]        
        c1 = Place(52 + 30.699/60., 13 + 21.435/60., 'c2')
        poi += [ c1 ]        
        c1 = Place(52 + 30.917/60., 13 + 21.858/60., 'c3 nano')
        poi += [ c1 ]        
        c1 = Place(52 + 30.983/60., 13 + 22.336/60., 'c4 micro')
        poi += [ c1 ]        

        LKPR = Place(50.102600, 14.234700, 'LKPR')
        poi += [ LKPR ]
        DUS = Place(51.276700, 6.755000, 'DUS')
        poi += [ DUS ]

        m.places = [ ( "city", [ prague ] ),
                     ( "village", [ kbely, ricany, zernovka ] ),
                     ( "poi", poi ) ]

    def get_nearest(m, point, list, minv = 999999999):
        minp = None
        for i in list:
            d = util.distance(point, i)
            if d < minv:
                minp = i
                minv = d
        return ( minp, minv )


