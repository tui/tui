#!/usr/bin/python
#
# Misc utilities for Maemo
# Copyright 2012 Pavel Machek <pavel@ucw.cz>, GPLv2+

import os
import math

class FakePos:
    pass

class FixPos(FakePos):
    def __init__(m, lat, lon):
        m.lat = lat
        m.lon = lon
        m.name = None

    def write_waypoint(m):
        f = open(os.getenv("HOME") + "/herd/wd/destination", "w")
        f.write("%f %f > %s" % (m.lat, m.lon, m.name))

def distance(p1, p2):
  """using the spherical law of cosines from 
  http://www.movable-type.co.uk/scripts/latlong.html.
  returns distance in kilometers"""
  R = 6371.
  lon1 = math.radians(p1.lon)
  lon2 = math.radians(p2.lon)
  lat1 = math.radians(p1.lat)
  lat2 = math.radians(p2.lat)
  ac = math.sin(lat1)*math.sin(lat2) + math.cos(lat1)*math.cos(lat2) * math.cos(lon2-lon1)
  try:
      return math.acos(ac) * R
  except:
      # Distance failed,  0.875121525985 0.253888752778  x  0.875121525985 0.253888752778  acos()  1.0
      # I guess ac is something like 1.0000001 then this fails
      #print("Distance failed, ", lat1, lon1, " x ", lat2, lon2, " acos() ", ac)
      return 0.0

# Aptitude install python3-geopy, http://stackoverflow.com/questions/7222382/get-lat-long-given-current-point-distance-and-bearing

# Thanks to http://gis.stackexchange.com/questions/29239/calculate-bearing-between-two-decimal-gps-coordinates
# It does not work well for great distances, there, bearing changes through the trip.

def bearing(start, end):
    startLat = math.radians(start.lat)
    startLong = math.radians(start.lon)
    endLat = math.radians(end.lat)
    endLong = math.radians(end.lon)

    dLong = endLong - startLong

    dPhi = math.log(math.tan(endLat/2.0+math.pi/4.0)/math.tan(startLat/2.0+math.pi/4.0))
    if abs(dLong) > math.pi:
        if dLong > 0.0:
            dLong = -(2.0 * math.pi - dLong)
        else:
            dLong = (2.0 * math.pi + dLong)

    bearing = (math.degrees(math.atan2(dLong, dPhi)) + 360.0) % 360.0;
    return bearing

def say(a):
#    os.system("echo '"+a+"' | espeak")
    os.system("echo '"+a+"' | festival --tts")
# /usr/bin/espeak -a 100 -p 50 -s 170 -g 1 -v europe/cs -f README.cs --pho

def pcmd(c):
    return os.popen(c).readline()[:-1]

def format_ssid(ssid):
    return "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c" % (ssid[0], ssid[1], ssid[2], ssid[3], ssid[4], ssid[5], ssid[6], ssid[7], ssid[8], ssid[9], ssid[10], ssid[11])

