#!/usr/bin/python3
# apt-get  install python3-pip
# sudo pip3 install pyephem
# zpells runs on python2, thus
# sudo pip install pyephem

import ephem
import time
import util
from basic_formulate import *

# http://rhodesmill.org/pyephem/
# http://rhodesmill.org/pyephem/rise-set.html#computing-twilight
# http://rhodesmill.org/pyephem/date.html

class TimeOfDay(Formulate):
    def __init__(m, hour = 0.):
        m.val = hour

    def format(m, v=0):
        hour = int(m.val)
        minute = (m.val - hour)*60
        minute = int(minute)
        return "%d:%02d" % (hour, minute)

    def now(m):
        (year, month, day, hour, minute, second, u1, u2, u3) = time.localtime()
        m.val = hour+minute/60.+second/3600.

# lib/ephem.py uses more recent library, should be more suitable

class AstroUtil(Formulate):
    def __init__(m):
        pos = util.FakePos()
        pos.lat = 50.14
        pos.lon = 14.50
        m.pos = pos
        m.time = time.localtime()

    def get_pos(m):
        mypos = ephem.Observer()
        # mypos.date = "2016/03/30 21:00:00"
        mypos.pressure = 0
        mypos.lat, mypos.lon = str(m.pos.lat), str(m.pos.lon)
        return mypos

    def to_hours(m, t):
        t = ephem.localtime(t)
        t = t.hour + t.minute/60.
        return t

    def is_visible(m, obj):
        pos = m.get_pos()
        alt = obj(pos).alt
        return alt > 0

    def moon_data(m, hour):
        pos = m.get_pos()
        # FIXME: hour would need to be in GMT here
        pos.date = int(pos.date) + hour/24. - 0.5

        print(pos.date)
        moon = ephem.Moon(pos)
        return moon.alt, moon.phase, moon.moon_phase

    def is_day(m):
        return m.is_visible(ephem.Sun)
        
    def transit(m):
        mypos = m.get_pos()
        t1 = mypos.next_transit(ephem.Sun())
        t2 = mypos.next_antitransit(ephem.Sun())
        m.all_times[t1] = "noon"
        m.all_times[t2] = "midnight"        
        t1 = mypos.previous_transit(ephem.Sun())
        t2 = mypos.previous_antitransit(ephem.Sun())
        m.all_times[t1] = "noon"
        m.all_times[t2] = "midnight"        
        t1, t2 = m.to_hours(t1), m.to_hours(t2)

        return "Noon is at "+TimeOfDay(t1).formulate()+ " midnight is at " + TimeOfDay(t2).formulate() + "\n"
        
    # offs is "sun", "noon" or how many degrees below horizon (implies sun)
    def one_time_int(m, name, offs, do_formulate):
        mypos = m.get_pos()

        if offs == "moon":
            t1 = mypos.previous_rising(ephem.Moon())
            t1a = mypos.next_rising(ephem.Moon())            
            t2 = mypos.next_setting(ephem.Moon())
            t2a = mypos.previous_setting(ephem.Moon())                        
        elif offs == "sun":
            t1 = mypos.previous_rising(ephem.Sun())
            t1a = mypos.next_rising(ephem.Sun())            
            t2 = mypos.next_setting(ephem.Sun())
            t2a = mypos.previous_setting(ephem.Sun())                        
        else:
            mypos.horizon = offs
            t1 = mypos.previous_rising(ephem.Sun(), use_center=True)
            t1a = mypos.next_rising(ephem.Sun(), use_center=True)            
            t2 = mypos.next_setting(ephem.Sun(), use_center=True)
            t2a = mypos.previous_setting(ephem.Sun(), use_center=True)
            
        m.all_times[t1]  = name + " start"
        m.all_times[t2]  = name + " end"
        m.all_times[t1a]  = name + " start"
        m.all_times[t2a]  = name + " end"
        ( start, end ) = m.to_hours(t1), m.to_hours(t2)
        if do_formulate:
            return name + " starts at " + TimeOfDay(start).formulate() + " and ends at " + TimeOfDay(end).formulate() + ".\n"
        else:
            return name + ": " + TimeOfDay(start).format() + ".." + TimeOfDay(end).format() + "\n"

    def one_time(m, name, offs, do_formulate):
        try:
            return m.one_time_int(name, offs, do_formulate)
        except:
            return name + "???\n"

    def format(m, do_formulate = 0):
        m.all_times = {}
        r = ""
        r += m.one_time("Sunrise", "sun", do_formulate)
        r += m.one_time("Civil twilight", "-6", do_formulate)
        r += m.one_time("Nautical twilight", "-12", do_formulate)
        r += m.one_time("Astronomical twilight", "-18", do_formulate)
        r += m.one_time("Moonrise", "moon", do_formulate)
        r += m.transit()
        return r

    def fmt_hours(m, v):
        h = int(v)
        m = v*60 - h*60
        return "%2d:%02d" % (h, m)

    def sun_time(m):
        mypos = m.get_pos()
        t1 = mypos.next_rising(ephem.Sun())
        t2 = mypos.next_setting(ephem.Sun())
        sunrise = (t1-ephem.now()) * 24.
        sunset = (t2-ephem.now()) * 24.
        print(t1, t2)
        if sunrise < sunset:
            print("Sunrise in ", m.fmt_hours(sunrise), ".")
        else:
            print("Sunset in ", m.fmt_hours(sunset), ".")            

        if sunrise < sunset:
            print("Sunrise in ", sunrise, ".")
        else:
            print("Sunset in ", sunset, ".")            
            
def debug():
    a = AstroUtil()
    print(a.format())
    print(a.one_time("Time for light", "-3", 0))
    print(a.one_time("Time for light", "-3.692", 0))
    print(a.one_time("Time for light", "-3.693", 0))    
    
    print(a.transit())
    print(a.is_day())
    print(a.is_visible(ephem.Moon))
    print(a.moon_data(0))
    print(a.moon_data(8))
    print(a.moon_data(11.5))    
    print(a.moon_data(12))
    print(a.moon_data(13))            
    
if __name__ == "__main__":
    a = AstroUtil()
    print(a.format())
    a.sun_time()
