
class Formulate:
    def formulate(m):
        return m.format(1)

    # Verbosity level
    # 5 == verbose voice message
    # 1 == terse voice message
    # 0 == terse text message
    # -5 == verbose text message
    def format(m, v = 0):
        m.v = v
        return m.fmt()

    # Format data according to m.v verbosity level
    def fmt(m):
        return "implement me"

    # Round integers in suitable way for voice output
    def num(m, i):
        return i

    def alt(m, s1, s2):
        if m.v > 0:
            return s2
        return s1

    def selftest(m):
        print("Format: ", m.format())
        print("Formulate: ", m.formulate())
