import os
import pyinotify
from pyinotify import *

# https://github.com/seb-m/pyinotify/wiki/Tutorial

class EventHandler(ProcessEvent):
    def process_IN_CREATE(self, event):
        print("File Created: %s" %  event.pathname)

    def process_IN_DELETE(self, event):
        print("File Removed: %s" %  event.pathname)

    def process_IN_MODIFY(self, event):
        print("File modified: %s" %  event.pathname)
        
    def process_IN_CLOSE_WRITE(self, event):
        print("File modified: %s" %  event.pathname)

    def process_default(self, event):
        print("File modified: %s" %  event.pathname)
        
#mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE
mask = pyinotify.IN_CLOSE_WRITE
# | pyinotify.IN_MODIFY

wm = WatchManager()
handler = EventHandler()
notifier = pyinotify.Notifier(wm)

#wm.add_watch('/tmp/delme.x', mask, rec=True)

wm.watch_transient_file('/tmp/delme.x/foo', mask, EventHandler)

notifier.loop()

# https://github.com/seb-m/pyinotify/blob/master/python3/pyinotify.py
# notifier._fd ... it looks like it would be useful for select.
