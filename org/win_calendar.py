#!/usr/bin/python3

from __future__ import print_function

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time
import pango

import rotatable

class AppsWindow(rotatable.SubWindow):
    def __init__(m):
        rotatable.SubWindow.__init__(m)

    def aux_interior(m):
        table = gtk.Table(5,4,True)

        w = gtk.Calendar()
        m.calendar = w
        w.set_detail_height_rows(2)
        #w.set_detail_width_chars(3)
        w.set_property("show-details", True)
        w.set_detail_func(m.detail) # Does not work in python2/gtk2?
        w.override_font(pango.FontDescription("sans 12"))
        w.connect("day-selected-double-click", m.on_date_selected)
        w.connect("day-selected", m.on_date_selected)
        w.connect("month-changed", m.on_date_selected)
        w.select_month(4, 2017)
        w.select_day(18)
        help(w)
        #w.mark_day / unmark_day
        table.attach(w, 0,5,0,5)

        # .select_month
        # .mark_day / .unmark_day
        
        return table

    def on_date_selected(self, calendar):
        year, month, day = self.calendar.get_date()
        month += 1

        print("Date selected: %i/%i/%i" % (year, month, day))
                            

    def detail(self, calendar, year, month, date):
        #print(calendar, year, month, date)
        if year == 2017 and date == 24:
            return "Ok\nfoo\nbar"
                                

    def main_interior(m):
        table = gtk.Table(5,4,True)

        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())
        table.attach(w, 3,4, 4,5)

        _, w = m.font_button(m.big("prev"))
        w.connect("clicked", lambda _: m.calendar.prev_month)
        table.attach(w, 0,1,0,1)

        return table

    def show(m):
        rotatable.SubWindow.show(m)

if __name__ == "__main__":
    test = AppsWindow()
    test.basic_main_window()
    gtk.main()
