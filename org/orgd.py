#!/usr/bin/python3

import os
import subprocess
import sys
import copy
import PyOrgMode
import re


def pcmd(c):
    return os.popen(c).readline()[:-1]

""" Needs pip3 install PyOrgMode (?) """

class Appointment:
    def __init__(m, name, start):
        m.name = name
        m.start = start

class Organizer:
    def read(m, name = "test.org"):
        base = PyOrgMode.OrgDataStructure()
        base.load_from_file(name)
        print(base)

        m.schedule = m.get_elements(base.root)

    def sanitize(m, s):
        s = re.sub("\<", "(", s)
        s = re.sub("\>", ")", s)
        return s

    def get_elements(m, element, data=[]):
        """
        Grab the data from all scheduled elements for all the tree defined by 'element' recursively.
        Returns all the elements as an array.
        """
        if hasattr(element, "content"):
            for child in element.content:
                if hasattr(child, "TYPE"):
                    if element.TYPE == "NODE_ELEMENT" and child.TYPE == "SCHEDULE_ELEMENT":
                        if child.type & child.DEADLINE:
                            #print("Node", element.heading)
                            #print("....deadline", child.deadline.value)
                            data.append(Appointment(element.heading, child.deadline.value))

                    if child.TYPE == "APPOINTMENT_ELEMENT":
                        print("Appointment", child.title, child, child.start)
                        #print("%d-%02d-%02d %s" % (child.start.value.tm_year, child.start.value.tm_mon, child.start.value.tm_mday, m.sanitize(child.title)))
                        data.append(Appointment(child.title, child.start.value))

                m.get_elements(child, data)
        return data

    def write(m, name = "/tmp/delme.cal", mode = "w+"):
        f=open(name, mode)
        for a in m.schedule:
            text = "%d-%02d-%02d %s\n" % (a.start.tm_year, a.start.tm_mon, a.start.tm_mday, m.sanitize(a.name))
            print("%02d %02d:%02d %s\n" % (a.start.tm_mday, a.start.tm_hour, a.start.tm_min, m.sanitize(a.name)))
            f.write(text)
        f.close()
        

# yad --form --field=title --field=text:TXT --field=date:DT --field=test:CHK

class Base:
    mydir = b"/data/pavel/g/tui/org/"
    mypath = mydir + b"orgd.py"
    mycalendar = mydir+b"new_calendar.org"
    mycontacts = mydir+b"new_contacts.org"

class Dialog(Base):
    def dialog(m, s, pre=''):
        # --width=800 --height=600
        res = pcmd(pre+'yad '+s)
        return res

    def append_file(m, name, text):
        f=open(name, "a+")
        f.write(text)
        f.close()

    def new_contact(m, number=""):
        s = m.dialog('--form --title="New contact" --field="Phone" --field="Name" --field="Note"', "echo '%s' | " % number)
        phone, name, note, _ = s.split('|')
        org = "\n:PROPERTIES:\n"
        org += ":VERSION: 3.0\n"
        org += ":N: "+name+"\n"
        org += ":PHONE: "+phone+"\n"
        org += ":NOTE: "+note+"\n"
        org += ":END:\n"
        m.append_file(m.mycontacts, org)

    def new_calendar(m, date=""):
        s = m.dialog('--form --title="Calendar entry" --field="Date:DT" --field="Name" --field="Note:TXT" --date-format=%Y-%m-%d', "echo '%s' | " % date)
        date, title, note, _ = s.split('|')
        org = "\n* "+title+" <"+date+">\n"
        org += note+"\n"
        m.append_file(m.mycalendar, org)

    def convert_calendar(m):
        c = Organizer()
        #c.read("/data/pavel/bin/calendar.org")
        c.read(m.mycalendar)
        c.write("/tmp/delme.cal")

    def show_calendar(m):
        m.convert_calendar()
        s = m.dialog("--calendar --date-format=%Y-%m-%d --details=/tmp/delme.cal")
        print("Selected date: ", s)
        m.new_calendar(s)
        
class Menu(Base):
    def notification(m):
        p = subprocess.Popen("yad --notification --listen",
                             shell=True, stdin=subprocess.PIPE)
        # stdout=subprocess.STDOUT
        p.stdin.write(b"tooltip:hello world\n")
        p.stdin.write(b"action:%s show\n" % m.mypath)
        p.stdin.write(b"menu:Show calendar!%s show|New calendar!%s calendar|New contact!%s contact\n" %
                      (m.mypath, m.mypath, m.mypath))
        p.stdin.flush()
        #p.communicate()
        p.wait()

if len(sys.argv) > 1:
    if sys.argv[1] == "contact":
        d = Dialog()
        d.new_contact()
    if sys.argv[1] == "calendar":
        d = Dialog()
        d.new_calendar()
    if sys.argv[1] == "daemon":
        m = Menu()
        m.notification()
    if sys.argv[1] == "show":
        d = Dialog()
        d.convert_calendar()    
        d.show_calendar()
    if sys.argv[1] == "menu":
        m = Menu()
        m.notification()
else:
    d = Dialog()
    d.convert_calendar()    
    # ./organizer.py > /tmp/delme.cal
    #d.new_contact("800123456")
    #d.new_calendar("2020-01-28")

