#!/usr/bin/env python
# http://www.pygtk.org/pygtk2tutorial/sec-Calendar.html

import sys
sys.path += [ "../maemo", "../lib", "/usr/share/unicsy/lib" ]
import mygtk
mygtk.setup()

import gtk
import gobject
import os
import time
import watchdog

import ephemeris
import rotatable
import organizer
import pango

class TimeWindow(rotatable.SubWindow):
    def __init__(m):
        rotatable.SubWindow.__init__(m)
        m.wd = watchdog.LocationWatchdog()
        m.org = organizer.Organizer()
        m.org.read(os.getenv("HOME")+"/bin/calendar.org")

    def main_interior(m):
        table = gtk.Table(5,4,True)

        m.time_label, button = m.font_button(m.big("(time)")+"\n"+m.small("date"))
        table.attach(button, 0,1,0,1)

        m.loc_label, button = m.font_button(m.big("(position)")+"\n"+m.small("date"))
        table.attach(button, 1,3,0,1)

        m.sun_label, button = m.font_button("Sun times go here")
        table.attach(button, 1,4,1,3)

        _, w = m.font_button(m.big("ntp"))
        w.connect("clicked", lambda _: os.system("sudo ntpdate tak.cesnet.cz"))
        table.attach(w, 0,1,1,2)

        _, w = m.font_button(m.big("new") + "\ncalendar")
        w.connect("clicked", lambda _: m.new_calendar())
        table.attach(w, 0,1,2,3)
        
        _, w = m.font_button(m.big('Close'))
        w.connect("clicked", lambda _: m.hide())
        table.attach(w, 3,4, 0,1)

        m.details = gtk.Label()
        table.attach(m.details, 0,4, 3,5)

        m.update_month()
        return table

    def aux_interior(m):
        table = gtk.Table(5,4,True)
        calendar = gtk.Calendar()
        m.calendar = calendar

        #w.display_options(CALENDAR_WEEK_START_MONDAY)
        calendar.connect("month_changed", m.calendar_month_changed)
        calendar.connect("day_selected", m.calendar_changed)
        calendar.connect("day_selected_double_click", m.calendar_changed)
        calendar.connect("prev_month", m.calendar_changed)
        calendar.connect("next_month", m.calendar_changed)
        calendar.connect("prev_year", m.calendar_changed)
        calendar.connect("next_year", m.calendar_changed)
        
        #calendar.override_font(pango.FontDescription("sans 17"))

        table.attach(calendar, 0,5, 0,3)

        return table

    def new_calendar(m):
        d = m.calendar.get_date()
        w = NewCalendar()
        w.in_date = "%d-%02d-%02d" % d
        w.basic_window()

    def update_month(m):
        year, month, day = m.calendar.get_date()
        s = ''
        m.calendar.clear_marks()
        for a in m.org.schedule:
            if year == a.start.tm_year and month+1 == a.start.tm_mon:
                m.calendar.mark_day(a.start.tm_mday)
                s += "%d %s\n" % (a.start.tm_mday, a.name)
        print("set_text: ", s)
        m.details.set_text(s)

    def calendar_date_to_string(m):
        year, month, day = m.calendar.get_date()
        mytime = time.mktime((year, month+1, day, 0, 0, 0, 0, 0, -1))
        s = time.strftime("%x", time.localtime(mytime))
        print("Time update: ", s)
        return s

    def calendar_changed(m, widget):
        print("calendar changed", m.calendar_date_to_string())
        m.update_month()
        
    def calendar_month_changed(m, widget):
        #buffer = "month_changed: %s" % m.calendar_date_to_string()
        print("month changed")
        m.update_month()

    def tick_time(m):
        if m.time_label:
            dt = time.localtime()
            t = "%d:%02d" % (dt.tm_hour, dt.tm_min)
            t = m.big(t)
            t += "\n"
            t += "%d.%d.%d" % (dt.tm_mday, dt.tm_mon, dt.tm_year)
            m.time_label.set_text(t)
            m.time_label.set_use_markup(True)
        a = ephemeris.AstroUtil()
        m.sun_label.set_text(a.format())
            
    def tick_loc(m):
        m.wd.read_loc()
        if m.wd.pos:
            t = m.big(m.wd.pos.name)
            t += "%f %f" % (m.wd.pos.lat, m.wd.pos.lon)
        else:
            t = "(position unavailable)"
        m.loc_label.set_text(t)
        m.loc_label.set_use_markup(True)

    def tick(m):
        print("Tick tock")
        m.tick_loc()        
        m.tick_time()

        gobject.timeout_add(30000, lambda: m.tick())

    def show(m):
        rotatable.SubWindow.show(m)
        m.tick()

class NewCalendar(rotatable.SubWindow):
    def interior(m):
        table = gtk.Table(6,6,True)

        w = m.font_label(m.big("Start"))
        table.attach(w, 0,1,0,1)

        w = m.font_entry()
        w.set_text(m.in_date)
        m.start = w
        table.attach(w, 1,3,0,1)

        w = m.font_entry()
        w.set_text("00:00")
        m.start_time = w
        table.attach(w, 1,3,1,2)

        w = m.font_label(m.big("End"))
        table.attach(w, 3,4,0,1)

        w = m.font_entry()
        w.set_text(m.in_date)
        m.end = w
        table.attach(w, 4,6,0,1)
        
        w = m.font_entry()
        w.set_text("24:00")
        m.end_time = w
        table.attach(w, 4,6,1,2)
        
        w = m.font_label(m.big("Title"))
        table.attach(w, 0,1,2,3)

        w = m.font_entry()
        m.title = w
        table.attach(w, 1,6,2,3)
        
        if True:
            scrolledwindow = gtk.ScrolledWindow()
            if mygtk.is3:
                scrolledwindow.set_hexpand(True)
                scrolledwindow.set_vexpand(True)

            textview = gtk.TextView()
            textview.modify_font(pango.FontDescription("sans 22"))
            m.textbuffer = textview.get_buffer()
            m.textbuffer.set_text("")
            m.note = m.textbuffer

            if mygtk.is3:
                textview.set_wrap_mode(gtk.WrapMode.WORD)
            scrolledwindow.add(textview)

            w = scrolledwindow
        table.attach(w, 0,6,3,5)

        _, button = m.font_button(m.big("Close"))
        button.connect("clicked", lambda _: m.cancel())
        table.attach(button, 4,5,5,6)

        _, button = m.font_button(m.big("Save"))
        button.connect("clicked", lambda _: m.save())
        table.attach(button, 5,6,5,6)
        return table

    def save(m):
        have_time = False
        t1 = m.start_time.get_text()
        t2 = m.end_time.get_text()
        if t1 == "00:00" and t2 == "24:00":
            have_time = True
        d1 = m.start.get_text()
        d2 = m.end.get_text()
        t = m.title.get_text()
        n = "" # m.note.get_text()

        s = "* "+t+" "
        s += "<"+d1+" "+t1+">"
        s += "--"
        s += "<"+d2+" "+t2+">\n"
        s += n

        print(s)
        pass

    def cancel(m):
        pass
        
if __name__ == "__main__":
    test = TimeWindow()
    test.basic_main_window()
    test.tick()
    gtk.main()
