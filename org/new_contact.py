#!/usr/bin/python2

# Obsolete!
# orgd.py contains better version.
# yad --form --field=title --field=text:TXT --field=date:DT --field=test:CHK

import os

def pcmd(c):
    return os.popen(c).readline()[:-1]



class Dialog:
    
    def zenity(m, s):
        # --width=800 --height=600
        res = pcmd('zenity '+s)
        return res

    def new_contact(m):
        s = m.zenity('--forms --title="New contact" --text="Contact" --add-entry=Name: --add-entry=Phone: --add-entry=Note:')
        name, phone, note = s.split('|')
        org = ":PROPERTIES:\n"
        org += ":VERSION: 3.0\n"
        org += ":N: "+name+"\n"
        org += ":PHONE: "+phone+"\n"
        org += ":NOTE: "+note+"\n"
        org += ":END:\n"
        print(org)

    def new_calendar(m):
        s = m.zenity('--forms --title="Calendar entry" --text="Entry" --add-entry=Title: --add-calendar=Date: --forms-date-format="%y-%m-%d" --add-entry=Note:')
        title, date, note = s.split('|')
        org = "* "+title+" <"+date+">\n"
        org += note+"\n"
        print(org)
        

d = Dialog()
#d.new_contact()
d.new_calendar()
