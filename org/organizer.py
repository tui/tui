#!/usr/bin/python3

# This is obsolete, moved into orgd.py

import copy
import PyOrgMode
import re

""" Needs pip3 install PyOrgMode (?) """

class Appointment:
    def __init__(m, name, start):
        m.name = name
        m.start = start

class Organizer:
    def read(m, name = "test.org"):
        base = PyOrgMode.OrgDataStructure()
        base.load_from_file(name)
        print(base)

        m.schedule = m.Get_Elements(base.root)

    def sanitize(m, s):
        s = re.sub("\<", "(", s)
        s = re.sub("\>", ")", s)
        return s

    def Get_Elements(m, element, data=[]):
        """
        Grab the data from all scheduled elements for all the tree defined by 'element' recursively.
        Returns all the elements as an array.
        """
        if hasattr(element, "content"):
            for child in element.content:
                if hasattr(child, "TYPE"):
                    if element.TYPE == "NODE_ELEMENT" and child.TYPE == "SCHEDULE_ELEMENT":
                        if child.type & child.DEADLINE:
                            print("Node", element.heading)
                            print("....deadline", child.deadline.value)
                            data.append(Appointment(element.heading, child.deadline.value))

                    if child.TYPE == "APPOINTMENT_ELEMENT":
                        print("Appointment", child.title, child.start.value)
                        print("%d-%02d-%02d %s" % (child.start.value.tm_year, child.start.value.tm_mon, child.start.value.tm_mday, m.sanitize(child.title)))
                        data.append(Appointment(child.title, child.start.value))

                m.Get_Elements(child, data)
        return data

    def write(m, f):
        for a in c.schedule:
            if a.start.tm_mon == 7:
                print(a.name)
        

class Alarm:
    def run(m):
        """
date +%s -d "8:00 tomorrow"

sudo rtcwake -l -m mem -t `date +%s -d "+10 sec"`

Tohle chodi:

sudo rtcwake -l -m mem -s 5

sudo rtcwake -l -m no -s 5
"""

if __name__ == "__main__":
    c = Organizer()
    c.read("/data/pavel/bin/calendar.org")
    print("-------------------")
    for a in c.schedule:
        if a.start.tm_mon == 7:
            print(a.name)
