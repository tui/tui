
/* Thanks to http://joelmccracken.github.io/entries/a-simple-web-app-in-rust-pt-4-cli-option-parsing/

cargo run --target i686-unknown-linux-musl
rustup target add i686-unknown-linux-musl
*/

use std::io::prelude::*;
use std::fs::OpenOptions;
use std::io;

#[macro_use]
extern crate nickel;
use nickel::Nickel;

extern crate chrono;
use chrono::{DateTime, Local};

extern crate clap;
use clap::{App, Arg};

fn formatted_time_entry() -> String {
    let local: DateTime<Local> = Local::now();
    let formatted = local.format("%a, %b %d %Y %I:%M:%S %p\n").to_string();
    formatted
}

fn record_entry_in_log(filename: &Option<String>, bytes: &[u8]) -> io::Result<()> {
    if let &Some(ref name) = filename {
      let mut file = try!(
        OpenOptions::new()
            .append(true)
            .write(true)
            .create(true)
            .open(name)
      );
      try!(file.write_all(bytes));
      Ok(())
   } else { Ok(()) }
}

fn log_time(filename: &Option<String>) -> io::Result<String> {
    let entry = formatted_time_entry();
    {
        let bytes = entry.as_bytes();

        try!(record_entry_in_log(filename, &bytes));
    }
    Ok(entry)
}

fn do_log_time(logfile_path: &Option<String>, auth_token: &Option<String>) -> String {
    match log_time(logfile_path) {
        Ok(entry) => format!("Entry Logged: {}", entry),
        Err(e) => format!("Error: {}", e),
    }
}

fn link(url: &str, text: &str) -> String {
   format!("<a href=\"{}\">{}</a>\n", url, text)
}

fn get_page(title: String, body: String) -> String {
   format!("<html><head><title>{}</title></head><body>{}</body></html>", title, body)
}

fn get_home() -> String {
   let body = format!("{}<br><b>Czech republic</b>{}{}{}{}{}{}",
	    link("https://mbasic.facebook.com/", "facebook"),
	    link("http://m.mobilecity.cz/", "sms"),
	    link("http://m.jizdnirady.cz/", "vlak/bus"),
	    link("http://m.mobilecity.cz/ConnMHD.aspx", "Praha"),
	    link("http://m.idnes.cz/", "idnes"),
	    link("http://portal.chmi.cz/aktualni-situace/aktualni-stav-pocasi/ceska-republika/radary", "radar"),
	    link("http://www.yr.no/place/Czech_Republic/Prague/Prague/hour_by_hour.html", "yr")
    );

    get_page("Unicsy homepage".to_string(), body)
}

fn main() {
    let matches = App::new("simple-log")
        .version("v0.0.1")
        .arg(
            Arg::with_name("LOG FILE")
                .short("l")
                .long("logfile")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("AUTH TOKEN")
                .short("t")
                .long("token")
                .takes_value(true),
        )
        .get_matches();

    let logfile_path = match matches.value_of("LOG FILE") {
        Some(str) => Some(str.to_string()),
        None => None,
    };    
    let auth_token = match matches.value_of("AUTH TOKEN") {
        Some(str) => Some(str.to_string()),
        None => None,
    };

    let mut server = Nickel::new();
    server.utilize(router! {
	        get "/log_time.cgi" => |_req, _res| {
		            do_log_time(&logfile_path, &auth_token)
                }
		get "/" =>  |_req, _res| { get_home() }
		get "**" =>  |_req, _res| { "Unknown path" }
    });

    server.listen("127.0.0.1:6767");

}
