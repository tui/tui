#!/usr/bin/python3

import os
import time
import sys
import datetime

class RecParam:
    def __init__(m, w, h, fps):
        m.height = h
        m.width = w
        m.fps = fps
        m.bayer = False

class Recorder:
    dev_capture = "/dev/v4l/by-path/platform-1cb0000.csi-video-index0"
    
    def run_timed(m, s):
        t = time.time()
        os.system(s)
        print("... ", time.time() - t, " sec")
        
    def setup_media_dev(m, s, dev):
        cmdline = "/my/v4l-utils/utils/media-ctl/media-ctl -d %s -V '" % dev
        cmdline += '"ov5640 4-004c":0 [%s]' % s
        cmdline += "'"
        m.run_timed(cmdline)

    def setup_media(m, s):
        # Crazy hack, but...
        m.setup_media_dev(s, "/dev/media0")
        m.setup_media_dev(s, "/dev/media1")

    def setup_media_rec(m, rp):
        s = "UYVY8_2X8"
        if rp.bayer:
            s = "SBGGR8_1X8"

        s = "%s %dx%d" % (s, rp.width, rp.height)
        print("Media setup", s)
        m.setup_media(s)

    def test_output(m):
        s = "gst-launch-1.0 "
        s += "videotestsrc \! queue \! autovideosink"
        m.run_timed(s)

    def test_mirror(m):
        m.setup_media("UYVY8_2X8 1280x720")
        s = "gst-launch-1.0 "
        if True:
            s += "v4l2src device=%s num-buffers=150 " % m.dev_capture
            s += " \! video/x-raw,width=1280,height=720,format=UYVY,framerate=15/1 "
            s += " \! queue leaky=downstream \! videoconvert \! autovideosink "
        m.run_timed(s)

    def save_raw(m):
        m.setup_media("UYVY8_2X8 1280x720")
        s = "gst-launch-1.0 "
        #s += "matroskamux name = mux \! filesink location=/tmp/delme.flv sync=false "
        # matroskamux... ~same as flvmux
        s += "v4l2src device=%s num-buffers=300 " % m.dev_capture
        s += " \! video/x-raw,width=1280,height=720,format=UYVY,framerate=30/1 "
        #s += " \! videoconvert \! x264enc "
        s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=640,height=360 "
        s += " \! filesink location=/my/delme.raw sync=false "
        m.run_timed(s)
        #                    17 seconds. 500MB/10second, 50MB/second.
        # avenc-mjpeg:       88 seconds
        # x264enc:           81 seconds
        # scale to half:    119 seconds
        # videoscale method=0 envelope=1   17 seconds. 12MB/second...

    def record_raw(m):
        m.setup_media("UYVY8_2X8 1280x720")
        s = "gst-launch-1.0 "
        s += "v4l2src device=%s num-buffers=300 " % m.dev_capture
        s += " \! video/x-raw,width=1280,height=720,format=UYVY,framerate=30/1 "
        s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=640,height=360 "
        s += " \! matroskamux"
        s += " \! filesink location=/my/delme.raw.mkv sync=false "
        m.run_timed(s)
        #                    17 seconds. 500MB/10second, 50MB/second.
        
    def parse_raw(m):
        s = "gst-launch-1.0 "
        s += "filesrc location=/my/delme.raw \! videoparse format=5 framerate=30/1 width=640 height=360 "
        s += " \! videoconvert \! autovideosink"
        m.run_timed(s)

    def convert_raw(m):
        s = "gst-launch-1.0 "
        s += "filesrc location=/my/delme.raw \! videoparse format=5 framerate=30/1 width=640 height=360 "
        s += " \! videoconvert \! x264enc \! matroskamux \! filesink location=/my/delme.mkv sync=false"
        m.run_timed(s)

    def get_v4lsrc(m, rp):
        s = "v4l2src device=%s" % m.dev_capture
        if False:
             s += " num-buffers=300"
        s += " \! video/x-raw,width=%d,height=%d,format=UYVY,framerate=%d/1 " % ( rp.width, rp.height, rp.fps)
        return s

    # video/x-bayer,width=2592,height=1944,format=bggr,framerate=10/1
    # video/x-bayer,width=1920,height=1080,format=bggr,framerate=15/1

    def record_try(m):
        rp = RecParam(1280, 720, 30)
        #rp = RecParam(1920, 1080, 15)
        #rp = RecParam(2592, 1944, 10)   # Does not work?
        m.record_rec(rp)

    def record(m, rp = None, compress = False, fname = "/tmp/delme.mkv"):
        if not rp:
            rp = RecParam(1280, 720, 30)
        m.setup_media_rec(rp)
        s = "gst-launch-1.0 "
        s += "matroskamux name = mux \! filesink location=%s sync=false " % fname
        # avimux... resulting file is "fast"? Due to dropped frames?

        video = True
        preview = True
        audio = True

        if video:
            s += m.get_v4lsrc(rp)
            # 30/30:                              18.87 sec
            # 30/30, 320x180 save:                18.91 sec, 158% CPU
            #        640x360 raw save:            17.74 sec, 70% CPU
            #        640x360 raw save + a + p:
            if not compress:
                s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=640,height=360 "
            else:
                s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=320,height=180 "
            #s += " \! queue "
            if preview:
                s += " \! tee name=t "
            s += " \! queue "                
            #s += " \! avenc_mjpeg \! mux. "
            #s += " \! ffmpegcolorspace \! jpegenc \! mux. "
            #s += " \! videoconvert \! matroskamux \! filesink location=/tmp/delme.flv sync=false"
            if compress:
                s += " \! videoconvert "
                s += " \! x264enc \! mux. "
            else:
                s += " \! mux. "
            if preview:
                s += " t. "
                #s += " \! videoconvert  "
                s += " \! queue leaky=downstream "
                s += " \! videorate \! video/x-raw,framerate=3/1 "
                s += " \! videoscale method=0 envelope=1 \! video/x-raw,width=320,height=180 "
                s += " \! videoconvert "
                #s += " \! queue leaky=downstream \! autovideosink "
                s += " \! autovideosink "
                #s += " \! fakesink "

        # Works okay when ran locally. Does not work well over ssh. Expected?
        if audio:
            s += "autoaudiosrc \! queue \! audioconvert \! mux. "
        m.run_timed(s)
        os.system("ls -alh "+fname)

    def photo(m):
        rp = RecParam(1280, 720, 30)
        #rp = RecParam(2592, 1944, 10)   # Does not work? ... running megapixels fixes it.
        rp.bayer = True
        m.setup_media_rec(rp)
        s = "gst-launch-1.0 "
        #s += "v4l2src device=%s num-buffers=2 " % m.dev_capture
        s += "videotestsrc num-buffers=100 "
        s += " \! video/x-bayer,width=%d,height=%d,format=bggr,framerate=10/1 " % (rp.width, rp.height)
        s += " \! bayer2rgb \! videoconvert "
        s += " \! jpegenc \! filesink location=/tmp/delme.jpg"
        #s += " \! autovideosink "
        #s += " \! fakesink "
        m.run_timed(s)
        # With videotestsrc, ~ 435msec per frame, wow.
        #                       85msec per frame in 1280x720 config. On one cpu.
        #                   ... 89msec per frame in 1280x720 ... when all 4 CPUs are loaded.
        #                                       -> 44.9fps.

    def jpeg_speedtest(m):
        rp = RecParam(1280, 720, 30)
        #rp = RecParam(2592, 1944, 10)   # Does not work? ... running megapixels fixes it.
        rp.bayer = True
        m.setup_media_rec(rp)
        s = "gst-launch-1.0 "
        #s += "v4l2src device=%s num-buffers=2 " % m.dev_capture
        s += "videotestsrc num-buffers=100 "
        s += " \! video/x-raw,width=%d,height=%d,format=UYVY,framerate=10/1 " % (rp.width, rp.height)
        s += " \! videoconvert "
        s += " \! jpegenc \! filesink location=/tmp/delme.jpg"
        m.run_timed(s)
        # With videotestsrc, ~ 54msec per frame in 1280x720, on one cpu.

    def get_name(m):
        s = os.getenv("HOME")+"/Pictures/VID"
        s += datetime.datetime.now().strftime("%y%m%d%H%M%S")
        s += ".mkv"
        return s

    def handle_cmdline(m, argv):
        if len(argv) < 2:
            m.record(fname = m.get_name())
            return
        if argv[1] == "-t":
            m.jpeg_speedtest()
#r.save_raw()
#r.parse_raw()
#r.convert_raw()
#r.test_output()
#r.test_mirror()
#r.record_raw_audio()

r = Recorder()
r.handle_cmdline(sys.argv)
