#include <stdio.h>
#include <fcntl.h>

/* cat /dev/zero | head -c 2073600 > /tmp/delme.0

mobian@mobian:~/g/tui/pp$ time ./syscalltest 
0.00user 1.19system 1.55 (0m1.556s) elapsed 76.82%CPU
echo $?

file-to-file:
100 frames in 1.219 seconds -> 82fps.
zero-to-file:
in 0m0.690s -> 145fps.
 */

#define ZERO
#define SIZE 2073600

int main(void)
{
  int f1, f2;
  int i;
  static char buf[SIZE];

#ifdef ZERO
  f1 = open("/dev/zero", O_RDWR);
#endif
  f2 = open("/tmp/delme.1", O_CREAT | O_RDWR, 0777);

  for (i=0; i<100; i++) {
#ifndef ZERO
    f1 = open("/tmp/delme.0", O_RDWR);
#endif
    if (SIZE != read(f1, buf, SIZE))
      return 1;
#ifndef ZERO
    close(f1);
#endif
    if (SIZE != write(f2, buf, SIZE))
      return 2;
  }
  return 0;    
}
