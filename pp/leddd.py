#!/usr/bin/python3

import time
import os

class Debug:
	path = "/sys/class/leds/"
	def __init__(m):
		pass

	def name(m, n):
		return m.path+n+":indicator/brightness"

	def bright(m, n, v):
		path = m.name(n)
		with open(path, 'w') as file:
			# Write the string "1" to the file
			file.write(v)

	def startup(m):
		m.bright("red", "1")
		time.sleep(50)
		m.bright("red", "0")

d = Debug()
d.startup()
