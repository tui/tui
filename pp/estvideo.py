#!/usr/bin/python3

def rates(s, mbps):
    print("   ", s, " needs ", mbps, " MB/sec; 1GB RAM lasts ", "%.0f" % (1000/mbps), " seconds, 10GB disk lasts ", "%.0f" % (10000/(mbps*60)), " min")    

def check(megapixels, fps):
    print("At ", megapixels, " Mpix and ", fps, " fps:")
    rates("raw", megapixels*fps)


# 320x240 -> 0.077 mpix
# 640x480 -> 0.3 mpix
# 1280x720 -> (megapixel preview) -> 
# 1024x768 -> 0.79 mpix
# DVD video is 720x480, 25 or 30 fps, (interlaced from 60)
# DVD video is around 2..10Mbit/sec... <1.2MB/sec

# for m in [ 0.077, 0.3, 0.79, 1, 5 ]:
#    for fps in [ 25, 60 ]:
#        check(m, fps)

check(1.28*.72, 20)
        
