#!/usr/bin/python3

import os
import sys

def sy(s):
    os.system(s)

def insmod(n):
    sy("sudo insmod /my/modules/"+n)

def rmmod(n):
    sy("sudo rmmod "+n)
    
l = [ "videobuf2-core.ko", "videobuf2-v4l2.ko", "videobuf2-memops.ko", "video-bus-switch.ko",
      "smiaregs.ko", "smiapp-pll.ko", "smiapp.ko", "videobuf2-dma-contig.ko",
      "omap3-isp.ko", "et8ek8.ko", "adp1653.ko", "ad5820.ko" ]
 #

if len(sys.argv) > 1 and  sys.argv[1] == "--down":
    list.reverse(l)
    for m in l:
        rmmod(m)
else:
    for m in l:
        insmod(m)
