/*
  Thanks to
  http://sdl.beuc.net/sdl.wiki/Resizable_Windows

sudo aptitude install libsdl2-dev

g++ sdl_test.c -o delme -lSDL2
*/
#include <iostream>
#include <stdlib.h>
#include <SDL2/SDL.h>

typedef struct {
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 alpha;
} pixel;


void put_pixel(SDL_Surface* screen, int x, int y, pixel* p)
{
	Uint32* p_screen = (Uint32*)screen->pixels;
	p_screen += y*screen->w+x;
	*p_screen = SDL_MapRGBA(screen->format,p->r,p->g,p->b,p->alpha);
}

int render_screen(SDL_Surface* screen)
{
	pixel white;
	white.r = (Uint8)0xff;
	white.g = (Uint8)0xff;
	white.b = (Uint8)0xff;
	white.alpha = (Uint8)128;


	SDL_LockSurface(screen);
	/*do your rendering here*/

	for (int x=100; x<400; x++)
		put_pixel(screen, x, 120, &white);

	/*----------------------*/
	SDL_UnlockSurface(screen);

	/*flip buffers*/
	//SDL_Flip(screen);
	//clear_screen(screen);

	return 0;
}

int main(int argc, char* argv[])
{ //Our main program
	SDL_Window* window = NULL;
  	SDL_Surface *screen;
	SDL_Event event; //Events
	bool done = false; //Not done before we've started...

	if(SDL_Init(SDL_INIT_VIDEO) < 0){ //Could we start SDL_VIDEO?
		std::cerr << "Couldn't init SDL"; //Nope, output to stderr and quit
		exit(1);
	}

	atexit(SDL_Quit); //Now that we're enabled, make sure we cleanup

	window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN );
	if( window == NULL )
	  {
	    printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
	  }

	screen = SDL_GetWindowSurface( window );
	if(!screen){ //Couldn't create window?
		std::cerr << "Couldn't create screen"; //Output to stderr and quit
		exit(1);
	}

	//Fill the surface white
	SDL_FillRect( screen, NULL, SDL_MapRGB( screen->format, 0, 0, 0 ) );

	render_screen(screen);
	
	//Update the surface
	SDL_UpdateWindowSurface( window );

	//Wait two seconds
	SDL_Delay( 2000 );
	

	while(!done){ //While program isn't done
		while(SDL_PollEvent(&event)){ //Poll events
			switch(event.type){ //Check event type
			case SDL_QUIT: //User hit the X (or equivelent)
				done = true; //Make the loop end
				break; //We handled the event
			} //Finished with current event
		} //Done with all events for now
	} //Program done, exited
 
}
