#include <math.h>
#include <stdio.h>

int fcamToHW(float f)
{
  return log(f)/log(2) * 10;
}

float HWToFcam(int hw)
{
  return pow(2, (hw / 10.0));
}

int main(void)
{
  /* fcam 1.0 -> iso100 -> hw 0
     fcam 2.0 -> iso200 -> hw 10
     fcam 4.0 -> iso400 -> hw 20
     fcam 8.0 -> iso800 -> hw 30
     fcam 16.0-> iso1600-> hw 40 */
  float f = 1;
  while (f < 16.5) {
    int hw = fcamToHW(f);
    float f1 = HWToFcam(hw);
    printf("fcam %f, iso %f, hw %d, back %f\n", f, f*100, hw, f1);
    f = f+1;
  }

  return 0;
}
