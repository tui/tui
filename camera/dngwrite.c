/* -*- linux-c -*-

   Thanks to 
   http://stackoverflow.com/questions/38183888/libtiff-generating-tiff-ep-profile-2-raw-image-from-pixel-array#39839854

   Needs 
   sudo aptitude install libtiff5-dev

   gcc dngwrite.c -ltiff
   gcc --std=c11 dngwrite.c -ltiff && ./a.out && rawtherapee out.dng

   dcraw, ufraw

   https://github.com/illes/raspiraw/blob/master/raspi_dng.c
*/

#include <tiffio.h>
#include <stdio.h>
#include <math.h>

/* https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Desktop-generic/LSB-Desktop-generic/libtiff-ddefs.html */

/* https://github.com/ncsuarc/tiffutils/blob/master/libtiff-4.0.3.patch */
#define PHOTOMETRIC_CFA 32803
#define TIFFTAG_CFAREPEATPATTERNDIM 33421 /* dimensions of CFA pattern */
#define TIFFTAG_CFAPATTERN 33422 /* color filter array pattern */


void read_pgm(int *x, int *y, unsigned short *swapped)
{
	FILE *f = fopen("test.05-05/delme_1494001562.pgm", "r");
	char tmp[1024];
	
	*x = 1296;
	*y = 984;
	fgets(tmp, 1024, f);
	fgets(tmp, 1024, f);
	fgets(tmp, 1024, f);
	fgets(tmp, 1024, f);
	fgets(tmp, 1024, f);

	{
		int size = *x * *y;
		unsigned char img[size*2];

		if (fread(img, 2, size, f) != size) {
			printf("Error reading input.\n");
			exit(1);
		}
#if 0		
		for (int i=0; i<size*2; i+=2) {
			swapped[i] = img[i+1];
			swapped[i+1] = img[i];
		}
#endif
		for (int i=0; i<size; i++) {
			swapped[i] = (img[2*i] << 8) + img[2*i+1];
			//printf("%d ", (int) swapped[i]);
			 swapped[i] <<= 6;
		}
		
	}
}
  


int main(void)
{
   int width;
   int height;

   static const short bayerPatternDimensions[] = { 2, 2 };
   static const float ColorMatrix1[] =
   {
      1.0, 0.0, 0.0,
      0.0, 1.0, 0.0,
      0.0, 0.0, 1.0,
   };

   static const float AsShotNeutral[] =
   {
      1.0, 1.0, 1.0,
   };

   int row, i;
   float gamma = 80;

#define GR 0x80, 0x20, 0x08, 0x02, 0x00,
#define WH 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
#define BL 0x00, 0x00, 0x00, 0x00, 0x00,
#define BLK(N) N N N N N N N N

   // Arbitrary Bayer pixel values:
   unsigned char image[2000*1000*2] =
   {
      BLK(WH)
      BLK(GR)
      BLK(BL)
      BLK(WH)
      BLK(GR)
      BLK(BL)
      BLK(WH)
      BLK(GR)
      BLK(BL)
   };

   int tiff = 0;

   read_pgm(&width, &height, image);
//   width = 2048; height = 128;
//   memset(image, 0, width*height*2);

   TIFF* tif = TIFFOpen ("out.dng", "w"); 
   TIFFSetField (tif, TIFFTAG_DNGVERSION, "\01\01\00\00");
   TIFFSetField (tif, TIFFTAG_SUBFILETYPE, 0);
   TIFFSetField (tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
   TIFFSetField (tif, TIFFTAG_IMAGEWIDTH, width);
   TIFFSetField (tif, TIFFTAG_IMAGELENGTH, height);
   if (tiff)
	   TIFFSetField (tif, TIFFTAG_BITSPERSAMPLE, 16);
   else
	   TIFFSetField (tif, TIFFTAG_BITSPERSAMPLE, 10);
   TIFFSetField (tif, TIFFTAG_ROWSPERSTRIP, 1);
   TIFFSetField (tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
   if (tiff)
	   TIFFSetField (tif, TIFFTAG_PHOTOMETRIC, 4);
   else
	   TIFFSetField (tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_CFA);
   TIFFSetField (tif, TIFFTAG_SAMPLESPERPIXEL, 1);
   TIFFSetField (tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
   TIFFSetField (tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
   TIFFSetField (tif, TIFFTAG_CFAREPEATPATTERNDIM, bayerPatternDimensions);
   TIFFSetField (tif, TIFFTAG_CFAPATTERN, "\00\01\01\02");
   TIFFSetField (tif, TIFFTAG_MAKE, "DNG");
   TIFFSetField (tif, TIFFTAG_UNIQUECAMERAMODEL, "DNG");
   TIFFSetField (tif, TIFFTAG_COLORMATRIX1, 9, ColorMatrix1);
   TIFFSetField (tif, TIFFTAG_ASSHOTNEUTRAL, 3, AsShotNeutral);
   TIFFSetField (tif, TIFFTAG_CFALAYOUT, 1);
   TIFFSetField (tif, TIFFTAG_CFAPLANECOLOR, 3, "\00\01\02");

   unsigned char *cur = image;

   for (row = 0; row < height; row++)
   {
	   TIFFWriteScanline (tif, cur, row, 0);
	   cur += 2*width;
   }

   TIFFClose (tif);
   return 0;
}
