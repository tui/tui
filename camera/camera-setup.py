#!/usr/bin/python3

# This script is now maintained in /my/fcam-dev .

import subprocess
import time
import os
import os.path
import sys

# TODO: Camera gain does not work
# LED indicator mode is not usable
# LED torch mode only has one brightness setting

# sudo /my/v4l-utils/utils/media-ctl/media-ctl -p
# This seems to have good description:
# https://www.compulab.co.il/workspace/mediawiki/index.php5/CM-T3730:_Linux:_Camera

# aptitude install ufraw ?

class Camera:
    mc="/my/v4l-utils/utils/media-ctl/media-ctl"
    ya="/my/tui/yavta/yavta"

    def __init__(m, mode = 1200, back = True, raw = True):
        m.have_switch = os.path.isdir('/sys/bus/platform/devices/video-bus-switch')

        m.back = back
        m.win_x = -1
        if back:
            if mode == 2500:
                # 4 copies in preview mode, "CCDC won't become idle!" in raw mode.
                m.win_x, m.win_y = 2500, 1900
                m.cap_x, m.cap_y = 2592, 1968
            elif mode == 2501:
                # image is not high enough
                m.win_x, m.win_y = 800, 600
                m.cap_x, m.cap_y = 2592, 1968
            elif mode == 2502:
                # ?
                m.win_x, m.win_y = 2590, 1966
                m.cap_x, m.cap_y = 2592, 1968
            elif mode == 1200:
                # This one works, good.
                m.win_x, m.win_y = 1200, 900
                m.cap_x, m.cap_y = 1296, 984
            # Camera should be capable of 2592x1968 still pictures,
            # or 480p video at 25fps.
            elif mode == 800:
                m.win_x, m.win_y = 800, 600
                m.cap_x, m.cap_y = 864, 656
            elif mode == 801:
                # Resizer can only do certain ratios, 256/N, with N=64..1024
                m.win_x, m.win_y = 432, 328
                m.cap_x, m.cap_y = 864, 656
            elif mode == 640:
                m.win_x, m.win_y = 640, 480
                m.cap_x, m.cap_y = 648, 492
            elif mode == 320:
                # Not supported by hardware, downsampled with the resizer.
                m.win_x, m.win_y = 320, 200
                m.cap_x, m.cap_y = 648, 492
            elif mode == 200:
                # Does not work too well, picture is shifted.
                m.win_x, m.win_y = 240, 180
                m.cap_x, m.cap_y = 648, 492
            elif mode == 160:
                # Not supported by hardware, downsampled with the resizer.
                m.win_x, m.win_y = 160, 120
                m.cap_x, m.cap_y = 648, 492

            # Big .win works ok (but does not gain us much)
            # Big .cap makes copy of same image few times
            # Works, but gives same image 4 times
            #        m.cap_x, m.cap_y = 2592, 1968
            #        m.cap_x, m.cap_y = 1296, 984
            else:
                raise("Unknown width")
        else:
            if mode == 640:
                m.win_x, m.win_y = 656, 488
                # Originally, it said 648, but with that raw capture does not work
                m.cap_x, m.cap_y = 656, 488
            else:
                raise("Unknown width")

        m.raw = raw

        if m.win_x == -1:
            raise "Invalid size passed"

        m.list_l = []
        m.list_v = []

    def dev_link(m, unit, path):
        if unit in m.devices:
            src = m.devices[unit]
            os.system("sudo ln -sf /dev/"+src+" /dev/"+path)
        else:
            print("### ", unit, " missing!")
        
    def dev_probe(m):
        d = '/sys/class/video4linux/'
        l = os.listdir(d)
        m.devices = {}
        for i in l:
            s = open(d + i + '/name', 'r').readline()[:-1]
            print(i, s)
            m.devices[s] = i
        print(m.devices)

        m.dev_link('OMAP3 ISP CCDC output', 'video_ccdc')
        m.dev_link('OMAP3 ISP resizer output', 'video_resizer')
        m.dev_link('adp1653 flash', 'video_flash')
        m.dev_link('ad5820 focus', 'video_focus')
        m.dev_link('et8ek8 3-003e', 'video_sensor')

    def media_ctl(m, s):
        print(s)
        if 0 != subprocess.call(['sudo', m.mc] + s):
            print("Call ", s, " failed?")

    def media_l(m, s):
        m.list_l += [ ['-l', s] ]

    def media_v(m, s):
        m.list_v += [ ['-V', s] ]

    def common(m):
        if m.have_switch:
            m.media_l('"video-bus-switch":0 -> "OMAP3 ISP CCP2":0 [1]')
        m.media_l('"OMAP3 ISP CCP2":1 -> "OMAP3 ISP CCDC":0 [1]')

        size = "%dx%d" % (m.cap_x, m.cap_y)
        size2 = "%dx%d" % (m.cap_x, m.cap_y)
        m.media_v('"OMAP3 ISP CCP2":0 [SGRBG10 %s]' % size)
        m.media_v('"OMAP3 ISP CCP2":1 [SGRBG10 %s]' % size)
        m.media_v('"OMAP3 ISP CCDC":1 [SGRBG10 %s]' % size)
        m.media_v('"OMAP3 ISP CCDC":2 [SGRBG10 %s]' % size2)

    def common_raw(m):
        m.media_l('"OMAP3 ISP CCDC":1 -> "OMAP3 ISP CCDC output":0 [1]')

    def common_preview(m):
        m.media_l('"OMAP3 ISP CCDC":2 -> "OMAP3 ISP preview":0 [1]')
        # CCDC:2 can't be connected to resizer on N900, because resizer expect fmt:YUYV8_1X16/1296x984 and CCDC provides SGRGB10
        m.media_l('"OMAP3 ISP preview":1 -> "OMAP3 ISP resizer":0 [1]')
        m.media_l('"OMAP3 ISP resizer":1 -> "OMAP3 ISP resizer output":0 [1]')

        size = "%dx%d" % (m.cap_x, m.cap_y)
        # , crop:(10,4)/123x45
        #m.media_v('"OMAP3 ISP preview":0 [SGRBG10 %s]' % size)
        m.media_v('"OMAP3 ISP preview":1 [UYVY %s]' % size)
        m.media_v('"OMAP3 ISP resizer":1 [UYVY %dx%d]' % (m.win_x, m.win_y))

    # Back camera setup, suitable for preview/autofocus, fcam-dev works with it,
    # up to 1296x984
    def common_back(m):
        if m.have_switch:
            m.media_l('"et8ek8 3-003e":0 -> "video-bus-switch":1 [1]')
        else:
            m.media_l('"et8ek8 3-003e":0 -> "OMAP3 ISP CCP2":0 [1]')

        size = "%dx%d" % (m.cap_x, m.cap_y)
        m.media_v('"et8ek8 3-003e":0 [SGRBG10 %s]' % size)

    def common_front(m):
        if m.have_switch:
            m.media_l('"vs6555 binner 2-0010":1 -> "video-bus-switch":2 [1]')
        else:
            m.media_l('"vs6555 binner 2-0010":1 -> "OMAP3 ISP CCP2":0 [1]')

        size = "%dx%d" % (m.cap_x, m.cap_y)
        m.media_v('"vs6555 pixel array 2-0010":0 [SGRBG10/648x488 (0,0)/648x488 (0,0)/648x488]')
        m.media_v('"vs6555 binner 2-0010":1 [SGRBG10/648x488 (0,0)/648x488 (0,0)/648x488]')

    def setup_all(m):
        m.dev_probe()

        if m.back:
            m.common_back()
        else:
            m.common_front()

        m.common()
        
        if m.raw:
            m.common_raw()
            os.system("sudo ln -sf /dev/video_ccdc /dev/video_cam")
        else:
            m.common_preview()
            os.system("sudo ln -sf /dev/video_resizer /dev/video_cam")

    def setup(m):
        # It seems that -l needs to go first, then -V... for back preview
        m.setup_all()
        m.media_ctl(['-r'])
        for i in m.list_l:
            m.media_ctl(i)
        s = []
        for i in m.list_v:
            m.media_ctl(i)
            s += i

    def perms(m):
        os.system("sudo chmod 666 /dev/video* /dev/v4l-subdev*")

    def test(m):
        if m.raw:
            os.system(m.ya+" --capture=8 --skip 0 --format SGRBG10 --size %dx%d /dev/video2 --file=/tmp/delme#" % (m.cap_x, m.cap_y))
            # mplayer does not seem to support required format
            #os.system("mplayer -tv driver=v4l2:width=%d:height=%d:outfmt=uyvy:device=/dev/video6 -vo x11 -vf screenshot tv://" % (m.win_x, m.win_y))
        else:
            os.system(m.ya+" --capture=8 --skip 0 --format UYVY --size %dx%d /dev/video6 --file=/tmp/delme#" % (m.win_x, m.win_y))
            os.system("mplayer -tv driver=v4l2:width=%d:height=%d:outfmt=uyvy:device=/dev/video6 -vo x11 -vf screenshot tv://" % (m.win_x, m.win_y))

    def run(m):
        if m.raw:
            os.system("/my/fcam-dev/examples/bin/example1 raw:%dx%d" % (m.cap_x, m.cap_y))
        else:
            os.system("/my/fcam-dev/examples/bin/example1 preview:%dx%d" % (m.win_x, m.win_y))

    def yavta(m, s):
        if 0 != subprocess.call([m.ya] + s):
            print("Call ", m.ya, " failed?")

    def set_control(m, dev, ctrl, val):
        m.yavta( [ "--set-control", "0x%x %d" % (ctrl, val), "/dev/"+dev ] )

    def get_control(m, dev, ctrl):
        m.yavta( [ "--get-control", "0x%x" % ctrl, "/dev/"+dev ] )

    def get_all(m):
        print()
        print("Focus 0..1023")
        m.get_control("v4l-subdev8", 0x009a090a)
        print("Gain 0..40")
        c.get_control("v4l-subdev12", 0x00980913)
        print("Exposure 50..199410")
        # Exposure changes based on sensor mode. 67..199410 in 1.2MP
        # 398..198896 in 0.5MP
        c.get_control("v4l-subdev12", 0x00980911)

    def get_torch(m):
        print("Torch mode 0/2")
        c.get_control("v4l-subdev9", 0x009c0901)
        c.get_control("v4l-subdev9", 0x009c0908)
        c.get_control("v4l-subdev9", 0x009c0909)

    def list_controls(m, dev):
        m.yavta( [ "--list-controls", "/dev/"+dev ] )


argv = sys.argv
if len(argv) == 4:
    print("fcam helper mode", argv[1], argv[2], argv[3])
    if argv[1] == "RAW":
        raw = 1
    if argv[1] == "UYVY":
        raw = 0
    c = Camera(1200, back=1, raw=raw)
    c.cap_x = int(argv[2])
    c.cap_y = int(argv[3])
    c.win_x = int(argv[2])
    c.win_y = int(argv[3])
    
    c.setup()
    c.perms()
    sys.exit(0)

def setup_for_sdlcam(w):
    c = Camera(w, back=1, raw=1)
    c.setup()
    c.perms()
    c.test()
    sys.exit(0)


if len(argv) > 1:
    if argv[1] == "--all":
        c = Camera(1200, back=1, raw=1)
        while True:
            c.get_all()
            time.sleep(1)
        sys.exit(0)

    if argv[1] == "-f":
        print("Front camera")
        c = Camera(640, back=0, raw=0)
        c.setup()
        c.perms()
        c.test()
        sys.exit(0)

    if argv[1] == "-m": setup_for_sdlcam(2501)
    if argv[1] == "-i": setup_for_sdlcam(1200)
    if argv[1] == "-8": setup_for_sdlcam(800)
    if argv[1] == "-2": setup_for_sdlcam(200)
    # These do not work :-(
    if argv[1] == "-6": setup_for_sdlcam(640)
    if argv[1] == "-3": setup_for_sdlcam(320)
    if argv[1] == "-1": setup_for_sdlcam(160)
        
    # Init for fcam-dev
    if argv[1] == "-k":
        c = Camera(2500, back=1, raw=1)
        c.setup()
        c.perms()
        c.test()
        sys.exit(0)

if len(argv) == 2:
    c = Camera( int(argv[1]) , back=1, raw=0)
else:
    c = Camera( 320, back=1, raw=0)
c.setup()
c.perms()
c.test()
#c.run()

#c.list_controls("v4l-subdev3")
#c.set_control("v4l-subdev3", 0x00980901, 16)

#for i in range(13):
#    c.list_controls("v4l-subdev%d" % i)
