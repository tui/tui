#!/usr/bin/python3

# http://www.fredparker.com/ultexp1.htm#evfclux has nice explanation

import sys
sys.path += [ "../lib", "../ofone" ]

import subprocess
import os
import math

import hardware

class Lux:
    def __init__(m):
        pass

    # EV ISO 100 to lux
    def EV_to_lux(m, v):
        return 2.5 * 2**v

    def lux_to_EV(m, v):
        return math.log(v/2.5, 2)

    def test_lux(m):
        print("EV_100", "\t", "LUX")
        print(-3, "\t", m.EV_to_lux(-3))
        print(m.lux_to_EV(.31), "\t", .31)
        for i in range(-3, 18):
            print(i, "\t", m.EV_to_lux(i))
        print()

    def EV_to_f56(m, v):
        # return 1/time for v for given EV
        return 2**(v-5)

    def time_to_EV(m, v):
        # return EV for 1/v seconds and f/5.6
        return 5+math.log(v, 2)

    def f_to_EV(m, v):
        return math.log((v / 5.6) ** 2, 2)

    def ISO_to_EV(m, v):
        return -math.log(v / 100, 2)

    def test_times(m):
        # Assuming f/5.6
        # EV0 .. 30 sekund
        # EV5 .. 1 sekunda
        print("EV_100", "\t", "sec @ f/5.6")
        for i in range(-1, 21):
            print(i, "\t", m.EV_to_f56(i))
        print()

    def test_EV(m):
        print(0, m.time_to_EV(1/30.) + m.f_to_EV(5.6))
        print(8, m.time_to_EV(60) + m.f_to_EV(2.0))
        print(16, m.time_to_EV(250) + m.f_to_EV(16))
        print(11, m.time_to_EV(60) + m.f_to_EV(32) + m.ISO_to_EV(3200))
        print(18, m.time_to_EV(60) + m.f_to_EV(32) + m.ISO_to_EV(25))

    def run(m):
        m.test_lux()
        m.test_times()
        m.test_EV()

        print("Living room, main light on")
        print("4-6 lux? according to N900 light sensor")
        print(m.EV_to_lux(m.time_to_EV(10) + m.f_to_EV(5.6) + m.ISO_to_EV(800)), "lux")
        print(m.EV_to_lux(m.time_to_EV(6) + m.f_to_EV(5.6) + m.ISO_to_EV(800)), "lux")
        print((m.time_to_EV(10) + m.f_to_EV(5.6) + m.ISO_to_EV(800)), "EV")        
        print("max gain, max exposure, and N900 can take a photo")
        # N900 gain is actually in EV/10 units. Bless900 says it is up to ISO 3200,
        # original aplikation says to ISO 400.
        # Raw gain is 0-40. That's 0-4 EV.
        #      0      1       2       3       4
        # ISO  100    200     400     800     1600
        # ...makes sense.
        # N900 exposure is from 50 (1/2000, .5 msec) to ~33000 (1/3.03, 330 msec)
        # Lens says 2.8/5.2, that should mean f/2.8, 5.2mm lens, 31.2mm equiv.

        print()
        print("Brightest conditions for N900")
        ev = m.time_to_EV(2000) + m.f_to_EV(2.8) + m.ISO_to_EV(100)
        print(ev, "EV")
        print(m.EV_to_lux(ev), "lux")        
        print("Dimmest conditions for N900")
        ev = m.time_to_EV(3.03) + m.f_to_EV(2.8) + m.ISO_to_EV(1600)
        print(ev, "EV")
        print(m.EV_to_lux(ev), "lux")
        print("N900 base", m.f_to_EV(2.8), "EV")


        # Normally, after autogain, they want less than .5% overexposed pixels,
        # and would like 30% pixels in >66% of the range.
        
class Diopter:
    def pos_to_diopter(m, p):
        return (p/1023.) * 20

    def diopter_to_m(m, p):
        if p == 0:
            return 999999
        return 1./p

    def fmt(m, i):
        p = m.pos_to_diopter(i)
        #print(i, "diopter", p, "meters", m.diopter_to_m(p))
        if p == 0:
            return "inf"
        p = m.diopter_to_m(p)
        if p < 1.:
            return "%.f cm" % (p*100)

        return "%.2f m" % p
    
    def run(m):
        for i in range(40):
            print(i, m.fmt(i))
        print(300, m.fmt(300))
        print(360, m.fmt(300))
        print(500, m.fmt(500))
        print(600, m.fmt(600))
        print(1023, m.fmt(1023))

# Invalid: I changed computation method.
# Sunny outside, charging, light.                                                 
# light sensor 466, 15912 * 1.00 (1/62.85 sec, ISO100, EV 9.0, 1256.9 lux)        
# Sunny outside, charging, light. 12:50                                           
# light sensor 330, 19890 * 1.00 (1/50.28 sec, ISO100, EV 8.7, 1005.5 lux)        
# Sunny outside, kitchen, light.                                                  
# light sensor 14, camera ..28.8 lux                                              
# Still enough light to go without flashlight                                     
# light sensor 0, camera ..6.3 lux w/o light, 7.6lux w/light                      
# Sunny outside, charging, no light. 13:00                                        
# light sensor 272, 33415 * 2.00 (1/29.93 sec, ISO200, EV 6.9, 299.3 lux)         
# light sensor rotated 23                                                         
# In wood, shade
# light sensor cca 1900, camera says 3800.
# Direct sunlight on sensor: 150000+
# Sunny outside, charging, no light, 18:30
# light sensor 82, 33415 * 4.00 (1/29.93 sec, ISO400, EV 5.9, 149.6 lux)
# light sensor 77, 33415 * 4.00 (1/29.93 sec, ISO400, EV 5.9, 149.6 lux)
# Quite sunny outside, charging, no light, 18:45
# light sensor 48, 33415 * 6.50 (1/29.93 sec, ISO650, EV 5.2, 92.1 lux)
# Cloudy outside, charging, no light, 19:55
# sensor 18, 33415 * 13.93 (1/29.93 sec, ISO1393, EV 4.1, 43.0 lux)
# Cloudy outside, charging, no light, 19:55, curtains
# sensor 5, 169460 *16.00 (1/5.90 sec, ISO1600, EV 1.6, 7.4 lux)
# Cloudy outside, charging, no light, 20:25, curtains
# sensor 1, 

        
class AutoLux(Lux):        
    def auto_lux(m):
        # Units unknown
        lux = hardware.hw.light_sensor.get_illuminance()
        print("light sensor says %d" % lux)
        lux *= 4.8
        print("lux estimation %d" % lux)
        ev = c.lux_to_EV(lux)
        print("that should be %.1f EV" % ev)
        time = 2000
        while time > 1.:
            ev2 = -2 + m.time_to_EV(time) + m.ISO_to_EV(100)
            # + m.f_to_EV(5.6)
            #print("time 1/%.1f EV %.1f" % (time, ev2))
            if ev2 < ev:
                break
            time *= 0.9
        print("Decided to use time 1/%.1f at ISO100" % time)
        print("That should be %.1f EV" % (-2 + m.time_to_EV(time) + m.f_to_EV(5.6) + m.ISO_to_EV(100)))
        exp = 1000000/time
        print("That should be exposure %.0f" % exp)
        return exp

    def auto_run(m):
        exp = m.auto_lux()
        os.system("/my/fcam-dev/examples/bin/livecam -h -e %d" % exp)
        m.auto_lux()

c = AutoLux()
#c.auto_run()

#c.run()

#d = Diopter()
#d.run()


