#!/usr/bin/python3

import subprocess
import os

class Camera:
    gst="/usr/bin/gst-launch"
    def __init__(m):
        pass

    def run(m):
        if 0 != subprocess.call(
                [m.gst, "-v", "--gst-debug-level=2",
#                 "v4l2src", "device=/dev/video2", "num-buffers=3", "!",
#                 "video/x-raw-yuv,width=864,height=656,format=SGRB", "!",
                 "v4l2src", "device=/dev/video6", "num-buffers=3", "!",
                 "video/x-raw-yuv,width=800,height=600,format=(fourcc)UYVY", "!",
# ,format=(fourcc)YU12
                 "ffmpegcolorspace", "!",
                 "jpegenc", "!",
                 "filesink", "location=delme.jpg" ]):
            print("Call ", "", " failed?")

c = Camera()
c.run()

