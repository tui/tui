/* gcc asciicam.c /usr/lib/i386-linux-gnu/libv4l2.so.0.0.0 -o asciicam
   gcc asciicam.c /usr/lib/arm-linux-gnueabi/libv4l2.so.0 -o asciicam
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <linux/videodev2.h>

#define SIZE 10*1024*1024

char buf[SIZE];

void main(void)
{
  int fd = v4l2_open("/dev/video2", O_RDWR);
  int i;
  static struct v4l2_format fmt;

#if 1
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
  fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
  fmt.fmt.pix.width = 640;
  fmt.fmt.pix.height = 480;
  if (fmt.fmt.pix.pixelformat != 'RGB3') /* v4l2_fourcc('R', 'G', 'B', '3'); */
    printf("hmm. strange format?\n");
  
  printf("ioctl = %d\n", v4l2_ioctl(fd, VIDIOC_S_FMT, &fmt));
#endif

  for (i=0; i<500; i++) {
    int num = v4l2_read(fd, buf, SIZE);
    int y,x;
    
    printf("%d\n", num);
#if 0
    for (y = 0; y < 25; y++) {
      for (x = 0; x < 80; x++) {
	int y1 = y * 480/25;
	int x1 = x * 640/80;
	int c = buf[fmt.fmt.pix.width*3*y1 + 3*x1] +
	  buf[fmt.fmt.pix.width*3*y1 + 3*x1 + 1] +
	  buf[fmt.fmt.pix.width*3*y1 + 3*x1 + 2];

	if (c < 30) c = ' ';
	else if (c < 60) c = '.';
	else if (c < 120) c = '_';
	else if (c < 180) c = 'o';
	else if (c < 300) c = 'x';
	else if (c < 400) c = 'X';
	else c = '#';
	  
	printf("%c", c);
      }
      printf("\n");
    }
#endif    
  }
}
