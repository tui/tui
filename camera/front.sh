#!/bin/bash

MC="/my/v4l-utils/utils/media-ctl/media-ctl"
sudo $MC -r
sudo $MC -l '"vs6555 binner 2-0010":1 -> "video-bus-switch":2 [1]'
sudo $MC -l '"video-bus-switch":0 -> "OMAP3 ISP CCP2":0 [1]'
sudo $MC -l '"OMAP3 ISP CCP2":1 -> "OMAP3 ISP CCDC":0 [1]'
sudo $MC -l '"OMAP3 ISP CCDC":2 -> "OMAP3 ISP preview":0 [1]'
sudo $MC -l '"OMAP3 ISP preview":1 -> "OMAP3 ISP resizer":0 [1]'
sudo $MC -l '"OMAP3 ISP resizer":1 -> "OMAP3 ISP resizer output":0 [1]'
sudo $MC -V '"vs6555 pixel array 2-0010":0 [SGRBG10/648x488 (0,0)/648x488 (0,0)/648x488]'
sudo $MC -V '"vs6555 binner 2-0010":1 [SGRBG10/648x488 (0,0)/648x488 (0,0)/648x488]'
sudo $MC -V '"OMAP3 ISP CCP2":0 [SGRBG10 648x488]'
sudo $MC -V '"OMAP3 ISP CCP2":1 [SGRBG10 648x488]'
sudo $MC -V '"OMAP3 ISP CCDC":2 [SGRBG10 648x488]'
sudo $MC -V '"OMAP3 ISP preview":1 [UYVY 648x488]'
sudo $MC -V '"OMAP3 ISP resizer":1 [UYVY 656x488]'

YA="/my/tui/yavta/yavta"
sudo $YA --set-control '0x009e0903 240' /dev/v4l-subdev10
sudo $YA --set-control '0x00980911 485' /dev/v4l-subdev10

mplayer -tv driver=v4l2:width=656:height=488:outfmt=uyvy:device=/dev/video6 -vo x11 -vf screenshot tv://
# /my/tui/yavta/yavta --capture=8 --skip 0 --format UYVY --size 656x488 /dev/video6 --file=/tmp/delme#