from machine import Pin, PWM

	self.bl=Pin(12,Pin.OUT)

        pwm0 = PWM(Pin(12))
        pwm0.duty()

        >>> pwm0.duty(30)                                                               
>>> pwm0.duty(90)                                                               
>>> pwm0.duty(190)                                                              
>>> pwm0.duty(490)                                                              
>>> pwm0.duty(1000)                                                             
