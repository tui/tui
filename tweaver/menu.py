import time
import u

class Menu:
    def __init__(m, tw):
        m.tw = tw

    def prog(m, s):
        m.tw.paint_number(10, 10, 90, s)

    def run(m):
        m.tw.app_start()
        m.tw.cls(4)
        w = 5
        m.tw.rect(0, 80, 240, w, 32767)
        m.tw.rect(0, 160, 240, w, 32767)
        m.tw.rect(80, 0, w, 240, 32767)
        m.tw.rect(160, 0, w, 240, 32767)
        while True:
            if m.tw.btn(1):
                x, y = m.tw.mouse_x, m.tw.mouse_y
                m.tw.rect(x, y, w, w, 63)
                if y > 160:
                    if x > 160:
                        m.tw.power_off()
                    elif x < 80:
                        return

def run():        
    r = Menu(u.tw)
    r.run()
