import u
import machine
import esp
import esp32
import time
import network

def wifi_scan():
    print("Ready for wifi")
    wlan = network.WLAN(network.STA_IF) # create station interface
    wlan.active(True)       # activate the interface
    print("Wifi scan:", wlan.scan())             # scan for access points


def run():
    li = u.li
    print("Battery", li.axp.getBattPercentage(), "%,", li.axp.getTemp(), "C")
    print("CPU", machine.freq(), "MHz")
    print("Flash", esp.flash_size() / (1024*1024.), "MiB, user start", esp.flash_user_start() / (1024*1024.), "MiB")
    print("Hall sensor", esp32.hall_sensor())
    print("CPU temp", esp32.raw_temperature(), "F")
    print("From bootup", time.ticks_ms() / 1000., "s")
    wifi_scan()

run()
