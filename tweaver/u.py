from machine import Pin
import lily
import time
import sys
import os

global tw
global li

MOUSE_BUTTON_LEFT = 1

def boot_click():
    buzz=Pin(4,Pin.OUT)
    buzz.on()
    time.sleep_ms(10)
    buzz.off()

boot_click()
li = lily.LILY()

class TimeWeaver:
    def __init__(m):
        m.cls(0)
        m.mouse_x = 0
        m.mouse_y = 0

    def power_off(m):
        li.axp.shutdown()

    def screen_off(m):
        li.bl.off()

    def screen_on(m):
        li.bl.on()

    def click(m, l=10):
        li.buzz.on()
        time.sleep_ms(l)
        li.buzz.off()

    def app_start(m):
        m.cls(0)
        m.screen_on()

    def power_status(m):
        print("Battery", li.axp.getBattPercentage(), "%,", li.axp.getTemp(), "C")

    def rect(m, x, y, w, h, c):
        li.disp.fill_rect(x, y, w, h, c)

    def paint_digit(m, x, y, w, i):
        def c(i):
            if i: return 32767
            return 5
        def rect(a, b, c, d, e):
            li.disp.fill_rect(int(a), int(b), int(c), int(d), e)

        mask = 0x08
        if i == '-': mask = 0x40
        elif i == ' ': mask = 0x00
        elif i == '0': mask = 0x3f
        elif i == '1': mask = 0x06
        elif i == '2': mask = 0x5b
        elif i == '3': mask = 0x4f
        elif i == '4': mask = 0x66
        elif i == '5': mask = 0x6d
        elif i == '6': mask = 0x7d
        elif i == '7': mask = 0x07
        elif i == '8': mask = 0x7f
        elif i == '9': mask = 0x6f
        
        w1 = w * 0.75
        x1 = x + w1
        y1 = y + w1
        y2 = y + 2*w1
        w0 = w * 0.1
        #       1 
        #    20   2
        #      40
        #    10   4
        #       8
        rect(x, y, w1, w0, c(mask & 0x01))
        rect(x, y, w0, w1, c(mask & 0x20))
        rect(x1, y, w0, w1, c(mask & 0x02))

        rect(x, y1, w1, w0, c(mask & 0x40))
        rect(x, y1, w0, w1, c(mask & 0x10))
        rect(x1, y1, w0, w1, c(mask & 0x04))

        rect(x, y2, w1, w0, c(mask & 0x08))

    def paint_number(m, x, y, w, t):
        for d in t:
            m.paint_digit(x, y, w, d)
            x += w

    def datetime(m):
        # (23, 1, 31, 2, 18, 47, 36)
        return li.hwrtc.datetime()

    # pyxel-inspired
    def cls(m, c):
        li.disp.fill(c)

    def btn(m, key):
        if key != 1:
            return False
        if not li.ft.touched:
            return False
        touches = li.maptouch(li.ft.touches)
        touch = touches[0]
        m.mouse_x = touch['x']
        m.mouse_y = touch['y']
        return True

tw = TimeWeaver()
    
class BasicWatch:
    def __init__(m, tw):
        m.tw = tw

    def display_time(m):
        y, mo, d, wd, h, mi, s = m.tw.datetime()
        h = h+1
        s = "%2d-%02d" % (h, mi)
        m.tw.paint_number(10, 10, 45, s)
        s = "%d %d %d" % (d, mo, y+2000)
        m.tw.paint_number(10, 100, 20, s)
        pc = li.axp.getBattPercentage()
        s = "%3d" % pc
        m.tw.paint_number(10, 180, 20, s)

    def run(m):
        m.tw.app_start()
        step = 100 # msec
        timeout = 9000 / step
        longpress = 1000 / step
        update = 60000 / step
        is_on = True
        t = 0
        t1 = 0
        t2 = 0
        m.tw.screen_on()
        m.display_time()
        while True:
            if m.tw.btn(1):
                t = 0
                if t1 == 1:
                    m.tw.screen_on()
                    m.tw.click()
                    m.display_time()
                    t2 = 0
            else:
                t1 = 0
            t += 1
            t1 += 1
            t2 += 1
            if t == timeout:
                m.tw.screen_off()
            if t1 == longpress:
                m.tw.click(50)
                break
            if t2 == update:
                m.display_time()
                t2 = 0
            time.sleep_ms(step)

    def abort_on_touch(m):
        for i in range(100):
            s = "%d" % i
            m.tw.paint_number(120, 200, 20, s)
            if li.ft.touched:
                li.buzz.on()
                time.sleep_ms(50)
                li.buzz.off()
                return
            else:
                time.sleep_ms(50)
        li.disp.fill(3)
        #for i in range(240):
        #    m.tw.li.disp.line(0, 0, i, 240, 32767)
        m.tw.power_off()

def run_remote():
    import remote
    remote.run()

def run_menu():
    import menu
    menu.run()

def on_boot():
    global tw
    bw = BasicWatch(tw)
    while True:
        bw.run()
        run_menu()

def cat(name):
    f = open(name, "r")
    for a in f.readlines():
        print(a)

def ls(name = ""):
    print(os.listdir(name))

def off():
    tw.power_off()

def run(name):
    import name
    name.run()
    del sys.modules[name]

def webrepl():
    import webrepl
    webrepl.start(password="foo")
