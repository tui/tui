#!/usr/bin/python3
import time

def sieve_of_eratosthenes(limit):
    primes = [True] * limit
    primes[0] = primes[1] = False

    for i in range(2, int(limit**0.5) + 1):
        if primes[i]:
            for j in range(i * i, limit, i):
                primes[j] = False
    
    return [i for i, is_prime in enumerate(primes) if is_prime]

def benchmark_sieve(limit):
    start = time.time()
    for i in range(10000):
        primes = sieve_of_eratosthenes(limit)
    end = time.time()
    #print(primes)
    print("Time taken to find all primes up to", limit, "is", end - start, "seconds.")

benchmark_sieve(100)
