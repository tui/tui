import time
import network
import socket
import u

class Remote:
    def __init__(m, tw):
        m.tw = tw

    def prog(m, s):
        m.tw.paint_number(10, 10, 90, s)

    def run(m):
        m.tw.app_start()
        m.prog("0")
        ap = network.WLAN(network.AP_IF)
        ap.config(essid='Time Weaver')
        #ap.config(max_clients=2)
        ap.active(True)

        addr = socket.getaddrinfo('0.0.0.0', 1234)[0][-1]
        s = socket.socket()
        s.bind(addr)
        m.prog("1")
        s.listen(1)
        m.prog("2")
        while True:
            cl, addr = s.accept()
            m.prog("3")
            while True:
                x, y, z = m.tw.li.bma.read_accel()
                cl.send("%d %d %d %d\r\n" % (m.tw.li.ft.touched, x, y, z))
                time.sleep_ms(150)

def run():
    r = Remote(u.tw)
    r.run()
