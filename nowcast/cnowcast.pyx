import re
import numpy

# python setup.py build_ext --inplace

# "Fast" python version does --speedtest in 11.2sec
# This takes 10.9 sec

def empty():
    return numpy.zeros([400,300], numpy.int)

cdef int cmap_pixel(int c1, int c2, int c3):
    cdef int v

    v = 0
    if c1 == 168 and c2 ==168 and c3 ==168: v = 0
    if c1 == 56 and c2 ==0 and c3 ==112: v = 4
    if c1 == 48 and c2 ==0 and c3 ==168: v = 8
    if c1 == 0 and c2 ==0 and c3 ==252: v = 12
    if c1 == 0 and c2 ==108 and c3 ==192: v = 16
    if c1 == 0 and c2 ==160 and c3 ==0: v = 20
    if c1 == 0 and c2 ==188 and c3 ==0: v = 24
    if c1 == 52 and c2 ==216 and c3 ==0: v = 28
    if c1 == 156 and c2 ==220 and c3 ==0: v = 32
    if c1 == 224 and c2 ==220 and c3 ==0: v = 36
    if c1 == 252 and c2 ==176 and c3 ==0: v = 40
    if c1 == 252 and c2 ==132 and c3 ==0: v = 44
    if c1 == 252 and c2 ==88 and c3 ==0: v = 48
    if c1 == 252 and c2 ==0 and c3 ==0: v = 52
    if c1 == 160 and c2 ==0 and c3 ==0: v = 56
    if c1 == 252 and c2 ==252 and c3 ==252: v = 60
    return v

def map_pixel(pixel):
    (c1, c2, c3) = pixel
    return cmap_pixel(c1, c2, c3)
    
def load_stream(m, file, value_map):
        line = file.readline()
        if line != 'P3\n':
            print "Missing signature"
            exit

        line = file.readline()
        if line != '400 300\n':
            print "Bad size"
            exit

        line = file.readline()
        if line != '255\n':
            print "Bad # colors"
            exit

        line = file.read()
        line = re.sub("\n", " ", line)
        line = re.sub("  *", " ", line)
        line = re.sub(" $", "", line)
        pdata = map(int, line.split())
        cdef int data[400*300*3]
        cdef int i
        for 0 <= i < 400*300*3:
            data[i] = pdata[i]

        value = empty()

        known = 0
        unknown = 0

        cdef int x, y, v, c1, c2, c3

        for 73 <= y < 298:
            for 52 <= x < 356:
                c1 = data[(y*400+x)*3]
                c2 = data[(y*400+x)*3+1]
                c3 = data[(y*400+x)*3+2]
                # Use v = -1 to get known/unknown debugging to work
                v = 0
                if 0:
                    if color in value_map:
                        v = value_map[color]
                v = cmap_pixel(c1, c2, c3)

                if v == 60: print "max at ", x, " / ", y

                value[x, y] = v

        file.close()
        #print "Have ", known, " known values and  ", unknown, " unknowns."
        return value

def initcnowcast():
    pass