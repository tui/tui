#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/*

pavel@duo:~/g/tui/nowcast$ time gcc pypybench.c -lm
0.09user 0.03system 0.15 (0m0.154s) elapsed 83.23%CPU
pavel@duo:~/g/tui/nowcast$ time ./a.out 
0.001075
2.42user 0.01system 2.44 (0m2.443s) elapsed 99.72%CPU

pavel@duo:~/g/tui/nowcast$ time gcc -O3 pypybench.c -lm
0.14user 0.02system 0.34 (0m0.344s) elapsed 47.73%CPU
pavel@duo:~/g/tui/nowcast$ time ./a.out 
0.001075
1.27user 0.03system 1.32 (0m1.325s) elapsed 99.01%CPU
pavel@duo:~/g/tui/nowcast$ 

-- lfence test. Before:

gcc -O3 pypybench.c -lm -o delme
0.30user 0.01system 0.32 (0m0.320s) elapsed 99.80%CPU

barrier:

0.29user 0.02system 0.32 (0m0.320s) elapsed 99.83%CPU

lfence:

1.17user 0.02system 1.20 (0m1.205s) elapsed 99.40%CPU

smaller but repeated:

1.21user 0.00system 1.21 (0m1.218s) elapsed 99.78%CPU
lfence:
5.33user 0.00system 5.35 (0m5.357s) elapsed 99.67%CPU


 */

#define MAX (10*1024)

double arr1[MAX], arr2[MAX];

#define B do { asm volatile(""); } while(0)

void main(void)
{
  int i, j;
  int shift;

  for (i=0; i<MAX; i++) {
    B;
    arr1[i] = (double) random() / RAND_MAX; B;
    arr2[i] = (double) random() / RAND_MAX; B;
  }

  double minsum = 99999999;
  B;
  int iter;
  for (iter=0; iter < 500; iter++)
  for (shift = 0; shift<30; shift++) {
    B;
    double sum = 0; B;
    for (i=0; i<MAX; i++) { B;
      if (i+shift < MAX) { B;
	double v; B;
        v = arr1[i] - (arr2[i+shift]); B;
        v = v*v; B;
        sum += sqrt(v); B;
      }
      if (sum < minsum) {
	B;
        minsum = sum;
	B;
      }
    }
  }
  B;
  printf("%f\n", minsum);
  B;
}
 
