#!/usr/bin/python3
import os
import xml.etree.ElementTree as ET

os.system("wget https://www.chmi.cz/files/portal/docs/meteo/om/bulletiny/XOCZ50_OKPR.xml -O delme.cap.xml")

# CISORP 1100 = Praha

tree = ET.parse("delme.cap.xml")
root = tree.getroot()

for child in root.findall("{urn:oasis:names:tc:emergency:cap:1.2}status"):
    print("tag:", child.tag, "attr:", child.attrib, "text:", child.text)
    print("Status: ", child.text)

for child in root:
    language = None
    severity = None
    event = None
    description = None
    for child2 in child:
        if child2.tag == "{urn:oasis:names:tc:emergency:cap:1.2}language":
            language = child2.text
        if child2.tag == "{urn:oasis:names:tc:emergency:cap:1.2}severity":
            severity = child2.text
        if child2.tag == "{urn:oasis:names:tc:emergency:cap:1.2}event":
            event = child2.text
        if child2.tag == "{urn:oasis:names:tc:emergency:cap:1.2}description":
            description = child2.text
    if language != "cs":
        continue
    if severity == "Minor":
        continue
    
    print(severity, event, description)


