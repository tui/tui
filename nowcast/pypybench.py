#!/usr/bin/python

"""
pavel@duo:~/g/tui/nowcast$ time ./pypybench.py
0.0192463278955
47.76user 0.23system 48.17 (0m48.179s) elapsed 99.63%CPU
pavel@duo:~/g/tui/nowcast$ time pypy ./pypybench.py
0.0117354143481
2.34user 0.03system 2.42 (0m2.421s) elapsed 98.14%CPU
pavel@duo:~/g/tui/nowcast$ time python3 ./pypybench.py
0.0005906661283024972
68.66user 0.07system 68.96 (1m8.963s) elapsed 99.67%CPU

"""

import random
import math

max = 1024*1024
arr1 = [ random.random() for i in range(max) ]
arr2 = [ random.random() for i in range(max) ]

minsum = 99999999
for shift in range(30):
  sum = 0  
  for i in range(max):
    if i+shift < max:
        v = arr1[i] - (arr2[i+shift])
        v = v*v
        sum += math.sqrt(v)
    if sum < minsum:
        minsum = sum
print(minsum)
    
