/* -*- linux-c -*-
 * 
 * Faster version of nowcaster
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef unsigned char u8;
typedef u8 image[300][400];

#define SX 400
#define SY 300

#define Y1 73
#define Y2 298
#define X1 52
#define X2 356

/*

  time giftopnm  < /data/wnow/radar/1405/1405121945.gif | pnmtoplainpnm > delme.pnm

 */

image *
empty(void)
{
	return malloc(SX*SY);
}

int
cmap_pixel(int c1, int c2, int c3)
{
	if ((c1==168) && (c2==168) && (c3==168)) return 0;
	if ((c1==56) && (c2==0) && (c3==112)) return 4;
	if ((c1==48) && (c2==0) && (c3==168)) return 8;
	if ((c1==0) && (c2==0) && (c3==252)) return 12;
	if ((c1==0) && (c2==108) && (c3==192)) return 16;
	if ((c1==0) && (c2==160) && (c3==0)) return 20;
	if ((c1==0) && (c2==188) && (c3==0)) return 24;
	if ((c1==52) && (c2==216) && (c3==0)) return 28;
	if ((c1==156) && (c2==220) && (c3==0)) return 32;
	if ((c1==224) && (c2==220) && (c3==0)) return 36;
	if ((c1==252) && (c2==176) && (c3==0)) return 40;
	if ((c1==252) && (c2==132) && (c3==0)) return 44;
	if ((c1==252) && (c2==88) && (c3==0)) return 48;
	if ((c1==252) && (c2==0) && (c3==0)) return 52;
	if ((c1==160) && (c2==0) && (c3==0)) return 56;
	if ((c1==252) && (c2==252) && (c3==252)) return 60;
	return 0;
}

image *
load_pnm(char *name)
{
	FILE *f = fopen(name, "r");
	int x, y;
	image *val = empty();

	if (!f) {
		printf("File %s not found\n", name);
		return;
	}

	if (0 != fscanf(f, "P3\n")) {
		printf("Bad header\n");
		goto error;
	}
	if (2 != fscanf(f, "%d %d\n", &x, &y)) {
		printf("Error reading size\n");
		goto error;
	}
	if ((x != SX) || (y != SY)) {
		printf("Bad size\n");
		goto error;
	}
	if (1 != fscanf(f, "%d\n", &x)) {
		printf("Error reading colors\n");
		goto error;
	}
	if (x != 255) {
		printf("Bad # colors\n");
		goto error;
	}

	for (y=0; y<SY; y++)
		for (x=0; x<SX; x++) {
			int c1, c2, c3, v;
			if (3 != fscanf(f, "%d %d %d", &c1, &c2, &c3)) {
				printf("Bad content\n");
				goto error;
			}

			v = cmap_pixel(c1, c2, c3);
			(*val)[y][x] = v;
		}
	fclose(f);
	return val;

error:
	printf("Error reading\n");
	return NULL;
}

u8 map[] = {
	168, 168, 168,
	56, 0, 112,
	48, 0, 168,
	0, 0, 252,
	0, 108, 192,
	0, 160, 0,
	0, 188, 0,
	52, 216, 0,
	156, 220, 0,
	224, 220, 0,
	252, 176, 0,
	252, 132, 0,
	252, 88, 0,
	252, 0, 0,
	160, 0, 0,
	252, 252, 252
};

void
save_pnm(char *name, image *val)
{
	FILE *f = fopen(name, "w");
	int x, y, c1, c2, c3;

	if (!f) {
		printf("Out file %s not created\n", name);
		return;
	}

	fprintf(f, "P3\n");
	fprintf(f, "%d %d\n", SX, SY);
	fprintf(f, "255\n");

	for (y=0; y<SY; y++)
		for (x=0; x<SX; x++) {
			int v = (*val)[y][x];
			v = (v/4)*3;
			if ((v < 0) || (v >= sizeof(map))) {
				printf("Index %d out of range\n", v);
				return;
			}
			u8 *c = &map[v];
			fprintf(f, "%d %d %d ", c[0], c[1], c[2]);
		}
	fclose(f);
}

void
set_val(image *val, int x, int y, int v)
{
	if ((x < 0) || (x >= SX))
		return;
	if ((y < 0) || (y >= SY))
		return;
	(*val)[y][x] = v;
}

int
get_val(image *val, int x, int y)
{
	if ((x < 0) || (x >= SX))
		return 0;
	if ((y < 0) || (y >= SY))
		return 0;
	return (*val)[y][x];
}

int
get_val_limited(image *val, int x, int y)
{
	if ((x < X1) || (x >= X2))
		return 0;
	if ((y < Y1) || (y >= Y2))
		return 0;
	return (*val)[y][x];
}

void
add_cross(image *val, int x, int y, int s1, int s2)
{
	int s;
	for (s = s1; s < s2; s++) {
		set_val(val, x, y+s, 60);
		set_val(val, x, y-s, 60);
		set_val(val, x+s, y, 60);
		set_val(val, x-s, y, 60);
	}
}

int
diff_sq(image *v1, image *v2, int sx, int sy, int limit)
{
	long sum = 0;
	int x, y;
	for (y = Y1; y < Y2; y++) {
		for (x = X1; x < X2; x++) {
			int i1, i2;
			i1 = get_val(v1, x, y);
			i2 = get_val_limited(v2, x+sx, y+sy);
			sum += (i1-i2)*(i1-i2);
		}
		if (sum > limit) {
		  //printf("not finishing %d %d\n", sx, sy);
			return sum;
		}
	}
	return sum;			
}

int
best_shift(image *v1, image *v2, int x, int y)
{
	int mindiff = 1999999999, mindx = 0, mindy = 0;
	int sum;
	int dx, dy;
	int dist;

	/* FIXME: reindent; we now ignore greater distances. */
	for (dist = 0; dist < 8; dist++) {
	    for (dx = -7; dx < 8; dx++)
		for (dy = -7; dy < 8; dy++) {
		  int diff;

		  if (abs(dx) + abs(dy) != dist)
		    continue;

		  diff = diff_sq(v1, v2, dx, dy, mindiff);
			if (diff < mindiff) {
				mindiff = diff;
				mindx = dx;
				mindy = dy;
			}
		}
	}

	printf("Min dx %d, min dy %d, mindiff %d\n", mindx, mindy, mindiff);
	add_cross(v2, x-mindx, y-mindy, 5, 20);
	add_cross(v2, x-(mindx*4), y-(mindy*4), 5, 15);

}

#define ERROR() do { exit(1); } while(0)       

int
main(int argc, char *argv[])
{
#define NUM 10
	image *val[NUM+1] = { NULL, };
	image *orig[NUM+1] = { NULL, };
	int load = 0, save = 0;
	int x = 150, y = 150;
	signed char c;

	while ((c = getopt(argc, argv, "l:s:p:dr")) != -1)
		switch (c) {
		case 'p':	/* Position */
			printf("parsing %s\n", optarg);
			if (2 != sscanf(optarg, "%d,%d", &x, &y)) {
				printf("Invalid position\n");
				ERROR();
			}
			printf("have position\n");
			break;
		case 'l':	/* Load */
			if (load >= NUM) {
				printf("Too many pictures\n");
				ERROR();
			}
			orig[load] = load_pnm(optarg);
			val[load] = empty();
			memcpy(val[load], orig[load], SX*SY);
			load++;
			break;
		case 'd':	/* Draw */
			add_cross(val[save], x, y, 5, 25);
			break;
		case 's':	/* Save */
			if (load >= NUM)
				printf("Too many pictures\n");
			else
				save_pnm(optarg, val[save++]);
			break;
		case 'r':	/* pRedict */
			if (save > 0) {
				best_shift(orig[save-1], val[save], x, y);
			}
			break;
		default:
			printf("Unknown option %c\n", c);
			ERROR();
		}
	return 0;
}
