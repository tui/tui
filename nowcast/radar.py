#!/usr/bin/python
# Trivial weather nowcaster
#
# Copyright 2009,2014 Pavel Machek <pavel@ucw.cz>
# Distribute under GPLv3.
#
# 2to3 conversion: needs to be finished, breaks when converted.
#
# old.chmi.cz stopped working at 7/2014 :-(
#
#
# It predicts zero movement when clearly it is going to move
# Loading  pacz23.z_max3d.20140831.0645.0.png
# Loading  pacz23.z_max3d.20140831.0700.0.png
#
# Relfexivity at 2km should be better:
# .z_max3d. -> .z_cappi020.
#
# http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_cappi020/pacz2gmaps3.z_cappi020.20150329.2110.0.png
# http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d/pacz2gmaps3.z_max3d.20150402.2110.0.png
# http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-sum_merge_24h/pacz2gmaps3.merge.20150329.1600.1440.png
# http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-sum_merge_1h/pacz2gmaps3.merge.20150329.2100.60.png

# Forecasts:
# http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d_fct/20150423.1930/pacz2gmaps3.fct_z_max.20150423.1940.10.png
# http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_cappi020_fct/20150423.1930/pacz2gmaps3.fct_z_cappi020.20150423.1940.10.png


import sys
import re
import os
import time
import string
import datetime
from math import *
# Functions such as exp() are needed from numpy: they operate on arrays, too.
from numpy import *
import getopt
from copy import deepcopy

def todeg(deg, min, sec):
    return deg + min / 60.0 + sec / (60.0*60.0)

def write_func(name, now, func):
    global path
    file = open(name+".pnm", "w")

    (sx, sy) = now.shape
    file.write("P3\n")
    file.write("%d %d\n" % (sx, sy))
    file.write("255\n")

    for y in range(sy):
        for x in range(sx):
            file.write("%d %d %d\n" % func(now[x, y]))

    file.close()

class Chmi:
    def init_reverse_map(m):
        m.reverse_map = {}
        for v in m.value_map:
            m.reverse_map[m.value_map[v]] = v

    def empty(m):
        return zeros([400,300], int)

    def load_xy(m, fn, lat, lon):
        return m.load(fn)

    def load_stream_(m, file):
        import cnowcast
        return cnowcast.load_stream(m, file, m.value_map)

    def load_stream(m, file):
        line = file.readline()
        if line != 'P3\n':
            print("Missing signature")
            return

        value = m.empty()
        (sx, sy) = value.shape

        line = file.readline()
        if line != '%d %d\n' % value.shape:
            print("Bad size")
            return

        line = file.readline()
        if line != '255\n':
            print("Bad # colors")
            return

        data = []
        line = file.read()
        data = data + list(map(int, re.findall(r'(\d+)', line)))

        known = 0
        unknown = 0

        value_map = m.value_map
        for y in range(0, sy):
            for x in range(0, sx):
                color = ( data[(y*sx+x)*3], data[(y*sx+x)*3+1], data[(y*sx+x)*3+2] )
                # Use v = -1 to get known/unknown debugging to work
                v = m.map_pixel(color)

                if v == 60: print("max at ", x, " / ", y)

                value[x, y] = v
        m.clear_borders(value)

        file.close()
        #print "Have ", known, " known values and  ", unknown, " unknowns."
        return value

    def clear_borders(m, val):
        val[:52, :] = 0
        val[356:, :] = 0
        val[:, :73] = 0
        val[:, 298:] = 0

    def transform_old(m, lat, lon):
        north = todeg(50, 19, 18)
        south = todeg(48, 36, 55)
        y = ((lat-south)/(north-south) * (152.0-246.0)) + 246
        west = todeg(12, 10, 59)
        east = todeg(18, 50, 38)
        x = ((lon-west)/(east-west) * (307.0-66.0)) + 66.0
        return (x, y)

    # Thanks to Milan B.

    def transform(m, dlat, dlon):
    # converion of LONG LAT to picture XY,
    # picture [0,0] is the center of upper left pixel
    # gnomonic projection only
    # v.0.91 990520 ph,pn,jk

    # 230-67 pixels is cca 320km. => 1.95km per pixel

        rad2deg = 57.29577951308232 #  180/pi - prevod z radianu na stupne
    #    rad2deg = 1.0 #  180/pi - prevod z radianu na stupne

        geogr_lat0 = 50.008 / rad2deg
        geogr_lon0 = 14.447 / rad2deg
        geogr_x0 = 127.5
        geogr_y0 = 127.5
        cell_size = 2.0

        RZEME = 6378.144
        MY_PI = 3.141592653589793
        MY_2PI = 6.283185307179586
        MY_PI_2 = 1.570796326794897

        # zacatek sfer_polar(fi1,lam1,fi2,lam2,del,az)
        lam2 = -1 * ( dlon / rad2deg )
        fi2 = dlat / rad2deg
        lam1 = -1 * geogr_lon0
        fi1 = geogr_lat0
        s = ( sin( fi2 ) * sin( fi1 ) ) + ( cos( fi2 ) * cos( fi1 ) * cos( lam2 - lam1 ) )
        del_ = acos( s )
        s = sin( fi2 ) - ( sin( fi1 ) * s )
        r = ( cos( fi1 ) * sin( del_ ) )
        if r != 0:
            s = s / r
        else:
            s = 0

        az = acos( s ) #        Azimut v radianech
        dlam = lam2 - lam1;
        if ( ( lam2 > lam1 ) and ( dlam < MY_PI ) ): az = MY_2PI - az

        # konec sfer_polar(fi1,lam1,fi2,lam2,del_,az)
        # zacatek azim_gnomon(psi,alfa,x,y)

        psi = del_;
        alfa = az;
        if psi < MY_PI_2:
            ro = tan( psi )
            x = ro * sin( alfa )
            y = ro * cos( alfa )
        else:
            x = 0
            y = 0

        x = geogr_x0 + ( x * RZEME ) / cell_size
        y = geogr_y0 - ( ( y * RZEME ) / cell_size )

        return (x+20, y+44)

    def selftest(m):
        print("Should be 66,152:",  m.transform(todeg(50, 19, 18), todeg(12, 10, 59)))
        print("Should be 307,193:", m.transform(todeg(49, 31, 0.09), todeg(18, 50, 38)))
        print("Should be 240,246:", m.transform(todeg(48, 36, 55), todeg(16, 56, 39)))
        print("Should be 125,240:", m.transform(todeg(48, 46, 11), todeg(13, 50, 51)))

    def write_color(m, name, now):
        write_func(name, now, lambda x: m.reverse_map[x])

    def cache_path(m, fn):
        return m.cache_base(fn) + "/" + fn

class ChmiCache(Chmi):
    def nearest(m, d):
        min = int(d.minute/m.step)*m.step
        d = d.replace(minute = min, second = 30, microsecond = 0)
        return d

    def get_file(m, d):
        fn = m.filename(d)
        path = m.cache_path(fn)
        print(path)
        if os.path.exists(path):
            print("Cached exists.")
            return fn
        os.system("wget "+m.base_url+"/"+fn)
        base = m.cache_base(fn)
        print(base)
        if not os.path.exists(base):
            os.mkdir(base)
        if not os.path.exists(fn):
            raise "could not download"
        if os.path.exists(fn):
            os.system("mv %s %s" % (fn, path))
            time.sleep(1)
            return fn
        return None

    def get_recent(m, num = 8):
        l = []
        d = datetime.datetime.utcnow()
        d += datetime.timedelta(minutes=-3)
        #print "Odl data"
        #d += datetime.timedelta(hours=-2.5)
        print(d)
        d = m.nearest(d)
        dt = datetime.timedelta(minutes=-m.step)
        while num > 0:
            f = m.get_file(d)
            if f != None:
                l = [ f ] + l
                print(d, m.filename(d))
                num -= 1
            else:
                print("File ", d, " not available")
            d = d + dt
        return l

class Radar(ChmiCache):
    def init_map(m):
        #path = "/data/wnow/radar.history"

        map = {}
        # Transparency on radareu
        map[(3, 1, 1)] = 0
        map[(168, 168, 168)] = 0
        map[(56, 0, 112)] = 4
        map[(48, 0, 168)] = 8
        map[(0, 0, 252)] = 12
        map[(0, 108, 192)] = 16
        map[(0, 160, 0)] = 20
        map[(0, 188, 0)] = 24
        map[(52, 216, 0)] = 28
        map[(156, 220, 0)] = 32
        map[(224, 220, 0)] = 36
        map[(252, 176, 0)] = 40
        map[(252, 132, 0)] = 44
        map[(252, 88, 0)] = 48
        map[(252, 0, 0)] = 52
        map[(160, 0, 0)] = 56
        map[(252, 252, 252)] = 60
        m.value_map = map
        m.init_reverse_map()

    def map_pixel(m, pixel):
        pixel = tuple(pixel)
        if not pixel in m.value_map:
            return 0
        return m.value_map[pixel]


class ChmiRadarOld(Radar):
    def __init__(m):
        m.init_map()
        m.cache_dir = "/data/wnow/radar/"
        m.base_url = "http://old.chmi.cz/meteo/rad/data"
        m.step = 15

    def load_(m, fn):
        from scipy import misc
        import Image
        import cnowcast
        fn = m.cache_path(fn)
        im = Image.open(fn)
        im = im.convert("RGB")
        val = misc.fromimage(im)

        #val = map(lambda x: map(m.map_pixel, x), val)
        val = [list(map(cnowcast.map_pixel, x)) for x in val]
        #val = map(lambda x: map(lambda x: 0, x), val)

        val = asarray(val, dtype="int")
        val = transpose(val)
        m.clear_borders(val)
        return val

    def load(m, fn):
        file = os.popen("giftopnm <  "+m.cache_path(fn)+" | pnmtoplainpnm")
        return m.load_stream(file)

    def filename(m, d):
        return "%02d%02d%02d%02d%02d.gif" % (d.year - 2000, d.month, d.day, d.hour, d.minute)

    def cache_base(m, fn):
        return m.cache_dir + fn[0:4]

class ChmiCommon(Radar):
    def empty(m):
        return zeros([810,610], int)

    def transform(m, dlat, dlon):
        # Bottom left corner of old image corresponds to 5,557 in new image.
        x, y = Chmi.transform(m, dlat, dlon)
        y -= 300
        x *= 2
        y *= 2
        x += 5
        y += 557
        return x,y

    def clear_borders(m, val):
        val[728:, :] = 0
        val[:, :81] = 0
        val[:, 535:] = 0

    def load(m, fn):
        file = os.popen("pngtopnm <  "+m.cache_path(fn)+" | pnmtoplainpnm")
        return m.load_stream(file)

class ChmiRadarGMaps(ChmiCommon):
    pass

class ChmiRadarMax(ChmiRadarGMaps):
    def __init__(m):
        m.init_map()
        m.cache_dir = "/data/wnow/chmiMax/"
        m.base_url = "http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d"
        m.step = 30

    def filename(m, d):
        return "pacz2gmaps3.z_max3d.%04d%02d%02d.%02d%02d.0.png" % (d.year, d.month, d.day, d.hour, d.minute)

    def cache_base(m, fn):
        return m.cache_dir + fn[20:26]

class ChmiRadar2k(ChmiRadarGMaps):
    # http://portal.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/pacz23.z_max3d.20140715.1215.0.png
    # http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_cappi020/pacz2gmaps3.z_cappi020.20150329.2110.0.png
    def __init__(m):
        m.init_map()
        m.cache_dir = "/data/wnow/chmi2k/"
        m.base_url = "http://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_cappi020"
        m.step = 10

    def filename(m, d):
        return "pacz2gmaps3.z_cappi020.%04d%02d%02d.%02d%02d.0.png" % (d.year, d.month, d.day, d.hour, d.minute)

    def cache_base(m, fn):
        return m.cache_dir + fn[23:29]

class ChmiRadar(ChmiCommon):
    # http://portal.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km/pacz23.z_max3d.20140715.1215.0.png
    # New image is twice as big and shifted.
    def __init__(m):
        m.init_map()
        m.cache_dir = "/data/wnow/chmi/"
        m.base_url = "http://portal.chmi.cz/files/portal/docs/meteo/rad/data_tr_png_1km"
        m.step = 15

    def filename(m, d):
        return "pacz23.z_max3d.%04d%02d%02d.%02d%02d.0.png" % (d.year, d.month, d.day, d.hour, d.minute)

    def cache_base(m, fn):
        return m.cache_dir + fn[15:21]

class EuRadar(Radar):
    def __init__(m):
        m.init_map()
        m.cache_dir = "/data/wnow/radareu/"
        m.base_url = "http://www.radareu.cz/data/radar"
        m.step = 15

    # Thanks to http://forums.arcgis.com/threads/103827-Lat-Long-to-Web-Mercator
    def transform(m, y_lat, x_lon):
        if not (abs(x_lon) <= 180 and abs(y_lat) < 90):
            raise "Invalid coordinate values for conversion"

        num = x_lon * 0.017453292519943295
        x = 6378137.0 * num
        a = y_lat * 0.017453292519943295

        x_mercator = x
        y_mercator = 3189068.5 * math.log((1.0 + math.sin(a)) / (1.0 - math.sin(a)))

        x_map = x_mercator / 2100.
        y_map = y_mercator / -2100.
        x_map += 1423-653.5+12
        y_map += 2619+2979.6+117
        return x_map, y_map

    def clear_borders(m, val):
        pass

    def selftest(m):
        print("Should be 1423,2619:",  m.transform(50.3177, 12.1046))
        print("Should be 1684,2759:",  m.transform(48.61645, 16.94000))
        print("Should be 1577,2565:",  m.transform(50.9889, 14.9694))

    def empty(m):
        return zeros([2662,4000], int)

    def load(m, fn):
        file = os.popen("pngtopnm <  "+m.cache_path(fn)+" | pnmtoplainpnm")
        return m.load_stream(file)

    def map_pixel(m, pixel):
        pixel = tuple(pixel)
        if not pixel in m.value_map:
            best_diff = 99999999
            best = (0, 0, 0)
            p_ = asarray(pixel)
            for v in m.value_map:
                v_ = asarray(v)
                diff = add.reduce((p_ - v_) * (p_ - v_))
                if diff < best_diff:
                    best_diff = diff
                    best = v
            return m.value_map[best]
            #m.value_map[pixel] = m.value_map[best]
        return m.value_map[pixel]

    def filename(m, d):
        return "radar.anim.%04d%02d%02d.%02d%02d.0.png" % (d.year, d.month, d.day, d.hour, d.minute)

    def cache_base(m, fn):
        return m.cache_dir + fn[11:17]

class EuRadarSmall(EuRadar):
    def empty(m):
        return zeros([200,200], int)

    def transform(m, lat, lon):
        # Hack; assume they already loaded the center.
        return 100, 100

    def load_xy(m, fn, lat, lon):
        x, y = EuRadar.transform(m, lat, lon)

        file = os.popen("pngtopnm <  "+m.cache_path(fn)+" | pnmcut %d %d 200 200 | pnmtoplainpnm" % (x-100, y-100))
        return m.load_stream(file)

class ChmiLightn(ChmiCache):
    # 438 x 338
    def __init__(m):
        m.init_map()
        m.cache_dir = "/data/wnow/lightn/"
        m.base_url = "http://old.chmi.cz/meteo/rad/blesk/data-png"
        m.step = 10

    def init_map_human(m):
        #path = "/data/wnow/radar.history"

        map = {}
        for i in range(9):
            map[(255-13*i, 255-32*i, i)] = -10*i

        map[(0, 0, 0)] = 0
        map[(255, 255, 0)] = -5

        map[(151, 0, 8)] = -80
        map[(255, 255, 255)] = 60

        m.value_map = map
        m.init_reverse_map()

    def init_map(m):
        #path = "/data/wnow/radar.history"

        map = {}
        map[(0, 0, 0)] = 0
        map[(255, 255, 0)] = 10
        map[(255, 255, 255)] = 60

        m.value_map = map
        m.init_reverse_map()

    def load(m, fn):
        file = os.popen("pngtopnm <  "+m.cache_path(fn)+" | pnmcut -width 400 -top 38 | pnmtoplainpnm")
        return m.load_stream(file)

    def filename(m, d):
        return "pacz21.blesk.%04d%02d%02d.%02d%02d.10_9.png" % (d.year, d.month, d.day, d.hour, d.minute)

    def cache_base(m, fn):
        return m.cache_dir + fn[13:19]

def write_grey(name, now):
    write_func(name, now, lambda x: (x*4, x*4, x*4))

def write_scale(name, now):
    max = maximum.reduce(maximum.reduce(now))
    write_func(name, now, lambda x: ((x*256)/max, (x*256)/max, (x*256)/max))

def printval(value):
    (sx, sy) = value.shape
    for y in range(sy):
        s = ''
        for x in range(sx):
            v = " "
            if value[x, y] >= 5: v = "."
            if value[x, y] > 15: v = "X"
            s = s + v

        print(s)

def diffval(v1, v2):
    return add.reduce(add.reduce(abs(v1 - v2)))

def diffsqval(v1, v2):
    return add.reduce(add.reduce(abs(v1 - v2)*abs(v1 - v2)))

def print_score(val, num, diff):
    ( sx, sy ) = val.shape
    if (num > 1):
        return "difference %d in %d frames, %f per frame, %f per pixel" % ( diff, num, (diff*1.0)/num, (diff*1.0)/num/sx/sy)
    else:
        return "difference %d per frame, %f per pixel" % ( diff, (diff*1.0)/num/sx/sy)

def fmt(n):
    return "%2.2f" % (n*1.0)

def short_score(val, num, diff):
    ( sx, sy ) = val.shape
    return "%2.2f pp" % ((diff*1.0)/num/sx/sy)

# def predictor(history, num_how_far_ahead_to_predict):

def zero_predictor(history, num):
    return chmi.empty()

def prev_predictor(history, num):
    return history[-1]

def pprev_predictor(history, num):
    return history[-2]

def shift(val, s):
    (dx, dy) = s
    (sx, sy) = val.shape
    ret = zeros([sx,sy], int)

#    print "Shifting array ", sx, sy, " with deltas ", dx, dy
    for y in range(sy):
        if 0 < y+dy < sy:
            if dx == 0:
                ret[:, y+dy] = val[:,y]
            else:
                if dx >= 0:
                    ret[dx:sx, y+dy] = val[0:-dx, y]
                else:
                    ret[0:dx, y+dy] = val[-dx:sx, y]

    return ret

def best_shift(history, diff_func):
    mindiff = 999999999
    mindx = 0
    mindy = 0
    prev = history[-1]
    pprev = history[-2]
    for dx in range(-30, 30, 1):
        for dy in range(-30, 30, 1):
            diff = diff_func( shift(pprev, (dx, dy)), prev )
            if diff < mindiff:
                mindiff = diff
                mindx = dx
                mindy = dy

#    print "Min dx = ", mindx, " min dy = ", mindy, " mindiff = ", print_score(prev, 1, mindiff)
    return (mindx, mindy)

def move_any_predictor(history, num, diff_func):
    (mindx, mindy) = best_shift(history, diff_func)
    return shift(history[-1], (mindx*num, mindy*num))

def move_predictor(history, num):
    return move_any_predictor(history, num, diffval)

def move2a_predictor(history, num):
    return movesq_predictor([history[-4], history[-2]], 1)

def move2_predictor(history, num):
    return movesq_predictor([history[-3], history[-2]], 2)

def move3_predictor(history, num):
    return movesq_predictor([history[-4], history[-3]], 3)

def movesq_predictor(history, num):
    return move_any_predictor(history, num, diffsqval)

def parts_predictor(history, num):
    ret = chmi.empty()
    ( sx, sy ) = ret.shape
    px = 4
    py = 3
    prev = history[-1]
    pprev = history[-2]
    for x in range(px):
        for y in range(py):
            x1 =  x   *sx/px
            x2 = (x+1)*sx/px
            y1 =  y   *sy/py
            y2 = (y+1)*sy/py
#            print "size ", x1, x2, y1, y2
            ret[x1:x2,y1:y2] = movesq_predictor([pprev[x1:x2,y1:y2], prev[x1:x2,y1:y2]], num)
    return ret

def score(predictor, dataset, verbose):
    print("Scoring ", predictor)
    prev = chmi.load(dataset[0])
    pprev = prev
    first = prev
    diff = 0
    diffsq = 0
    num = 0
    start = time.time()
    for a in dataset:
        now = chmi.load(a)
        pred = predictor(pprev, prev, 1)
        dnow = diffval( pred, now )
        diff += dnow
        dsqnow = diffsqval( pred, now )
        diffsq += dsqnow
        num += 1
        if verbose or not (num%100):
            print("... progress ", num, " / ", len(dataset))
            print("... (", print_score(now, 1, dnow), ", squared: ", print_score(now, 1, dsqnow), ")")
        pprev = prev
        prev = now
    end = time.time()
    print("... time: ", end-start, " seconds, ", (end-start)/num , " seconds per frame")
    print("... scoresq: ", print_score(now, num, diffsq))

def par_score(predictors, dataset, verbose):
    print("Scoring ", predictors)
    history = []
    for a in range(10):
        history = history + [ chmi.empty() ]
    history[-1] = chmi.load(dataset[0])
    history[-2] = history[-1]
    diff = {}
    diffsq = {}
    num = 0
    start = time.time()
    for p in predictors:
        diff[ p ] = 0
        diffsq[ p ] = 0

    for a in dataset:
        history = history[-10:]
        now = chmi.load(a)
        for p in predictors:
            pred = p(history, 1)
            dnow = diffval( pred, now )
            diff[p] += dnow
            dsqnow = diffsqval( pred, now )
            diffsq[p] += dsqnow
        num += 1
        if verbose or not (num%100):
            print("... progress ", num, " / ", len(dataset))
        history = history + [ now ]

    end = time.time()
    print("Time: ", end-start, " seconds, ", (end-start)/num , " seconds per frame")

    for p in predictors:
        print("Predictor ", p, "... scoresq: ", print_score(now, num, diffsq[ p ]))

def evaluation(alldata):
    print(alldata)

    # Mam zhruba 1100 vstupnich souboru.
    # load/printval trva cca 25 minut na 1000 souboru.

    score(zero_predictor, alldata, 0)
    score(pprev_predictor, alldata, 0)
    score(prev_predictor, alldata, 0)
    score(move_predictor, alldata, 0)
    score(movesq_predictor, alldata, 0)
    score(parts_predictor, alldata, 0)

def par_evaluation(alldata):
    print(alldata)

    par_score([zero_predictor, prev_predictor, move_predictor, movesq_predictor, parts_predictor], alldata, 0)

def predict(alldata, predictor, lat, lon):
    pprev = chmi.load(alldata[-3])
    prev = chmi.load(alldata[-2])
    now_name = alldata[-1]
    print("Current weather data should be in ", now_name)
    now = chmi.load(now_name)
    x, y = chmi.transform(lat, lon)
    print("Weather now is ", now[x, y], " (according to radar)")
    now_predicted = predictor([pprev, prev], 1)
    print("Predicted weather for now is ", now_predicted[x, y])
    diff = diffsqval(now, now_predicted)
    print("sqError in prediction of current weather is ", print_score(now, 1, diff))
    future = predictor([prev, now], 1)
    print("Predicted weather for future is ", future[x, y])

def graph(i):
    i = i/4
    if i>10:
        i=10
    return i*'*' + (10-i)*' '

def graphval(i):
    return ("%2d" % i) + " " + graph(i)

def around(now, x, y):
    return maximum.reduce(maximum.reduce(now[x-5:x+5, y-5:y+5]))

def history(alldata, lat, lon):
    x, y = chmi.transform(lat, lon)
    for now_name in alldata:
        now = chmi.load(now_name)
        print("Radar", now_name, "was", graphval(now[x, y]), "around", graphval(around(now, x, y)))

def history_plus(alldata, predictor, lat, lon):
    pprev = chmi.empty()
    prev = chmi.empty()
    x, y = chmi.transform(lat, lon)
    for now_name in alldata:
        now = chmi.load(now_name)
        now_predicted = predictor([pprev, prev], 1)
        diff = diffsqval(now, now_predicted)

        print("Radar", now_name, "was", graphval(now[x, y]), "predicted", graphval(now_predicted[x, y]), "around", graphval(around(now, x, y)), "err", short_score(now, 1, diff))

        pprev = prev
        prev = now

    for r in range(10):
        future = predictor([pprev, prev], r)
        write_grey("future%d" % r, future)
        print("Predicted weather for ", r, " future is ", graphval(future[x, y]))

# Convert from dBZ to mm/h
def tomm(i):
    return exp(((i-24.0)/16)*log(10))

def statistics(alldata):
    start = time.time()
    num = 0
    maxdb = chmi.empty()
    sumdb = chmi.empty()
    sumrain = chmi.empty()

    for now_name in alldata:
        now = chmi.load(now_name)
        ( sx, sy ) = now.shape
        num += 1
        max = maximum.reduce(maximum.reduce(now))
        sum = add.reduce(add.reduce(now))
        avg = (sum*1.0) / sx / sy

        sum = add.reduce(add.reduce(now*now))
        sqavg = (sum*1.0) / sx / sy

        sum = add.reduce(add.reduce(tomm(now)))
        mmavg = (sum*1.0) / sx / sy

        maxdb = maximum(maxdb, now)
        sumdb += now
        sumrain += tomm(now)
        print(now_name, " max rain ", max, " ( ", fmt(tomm(max)), "mm/h) avg ", fmt(avg), " sqavg ", fmt(sqavg), " mmavg ", fmt(mmavg))

    end = time.time()
    print(num, " frames")
    print("... time: ", end-start, " seconds, ", (end-start)/num , " seconds per frame")
    write_grey("maxdb", maxdb)
    write_scale("sumdb", sumdb)
    write_scale("sumrain", sumrain)
    
def describe(text, val, x, y):
    v = val[x, y]
    a = around(val, x, y)
    if a or v:
        return "%s %d to %d. " % (text, v, a)
    return ""

def wwarn(s, alldata, lat, lon):
    predictor = movesq_predictor

#    print 'Position is %f, %f' % (lat, lon)
    #alldata = getlist("0907181[6789]* 0907182*")

    pprev = chmi.empty()
    prev = chmi.empty()

    say = ""
    msg = "" 

    x, y = chmi.transform(lat, lon)

    for now_name in alldata[-2:]:
#        print "Loading", now_name
        now = chmi.load(now_name)
#        print "Was ", now[x, y], ", around", around(now, x, y), ". "
        pprev = prev
        prev = now

    dry = max(pprev[x, y], prev[x, y])
    msg += describe("Before ", pprev, x, y)
    msg += describe("Now ", prev, x, y)
    s = best_shift([pprev, prev], diffsqval)
    future = prev

    for i in range(1, 4):
        future = shift(future, s)
        msg += describe("In %d minutes " % (i*15), future, x, y)
        v = future[x, y]
        a = around(future, x, y)
        if dry == 0 and a and say == "":
            say = "Rain around. "
        if dry == 0 and v and say != "Heavy rain predicted. ":
            say = "Rain predicted. "
        if dry < 20 and v > 20:
            say = "Heavy rain predicted. "

    if pprev[x, y] and not prev[x, y] and say == "":
        say = "Rain ceased. "

    if prev[x, y] and not pprev[x, y] and say == "":
        say = "Rain started. "

    if msg != "":
        print("Print: ", msg, "(", say, ")")
    else:
        print("Print: No significant weather.")
    if say != "":
        print("Say: ", say)


def add_cross_xy(val, x, y, s1, s2):
    val[x-s2:x-s1, y] = 60
    val[x+s1:x+s2, y] = 60
    val[x, y-s2:y-s1] = 60
    val[x, y+s1:y+s2] = 60

def show_cross(alldata, lat, lon):
    params = ""
    #alldata = getlist("1404081[34]*") # FIXME --nice test
    #alldata = getlist("140414*")
    x, y = chmi.transform(lat, lon)

    for index in range(-2, 0):
        picture = alldata[index]
        print("Loading ", picture)
        val = chmi.load_xy(picture, lat, lon)
        if index == -2:
            prev = copy(val)

        if index == -1:
            sx, sy = best_shift([prev, val], diffsqval)
            print("Best shift ", sx, sy)
            x, y = chmi.transform(lat, lon)
            add_cross_xy(val, x-sx, y-sy, 5, 20)
            add_cross_xy(val, x-(sx*4), y-(sy*4), 5, 15)

        add_cross_xy(val, x, y, 5, 25)
        
        out = "show_"+picture+".pnm"
        chmi.write_color("res", val)
        os.system("cat res.pnm | pnmscale -xscale=2 -yscale=2 > "+out)

        params += out + " "
    return params

def fast_cross(alldata, lat, lon):
    params = ""
    x, y = chmi.transform(lat, lon)
    fparams = "-p %d,%d " % (int(x), int(y))
    for index in range(-8, 0):
        fn = alldata[index]
        print("Loading ", fn)
        name = "in%d.pnm" % index
        os.system("giftopnm <  "+chmi.cache_path(fn)+" | pnmtoplainpnm > "+name)
        oname = "out%d.pnm" % index
        fparams += "-l "+name+" -d -r -s "+oname+" "

    print(fparams)
    os.system("time ./nowfast "+fparams)
    print("done")

    for index in range(-8, 0):
        name = "out%d.pnm" % index
#        os.system("cat %s | pnmscale -xscale=2 -yscale=2 > "+out)

        params += name + " "
    return params

class Query:
    def __init__(m):
        m.lat = 50.00
        m.lon = 14.75
        m.alldata = [] # m.getlist("*")

    def help(m):
        print("nowcast --pos=LAT,LON --graph|--wwarn")
        sys.exit(1)

    def getlist(m, mask):
    # Na zacatku jsou nezajimava data, kolem 09071916 je pekna fronta od zapadu
        file = os.popen("cd "+chmi.path+"; ls -1 "+mask)

        list = []
        while 1:
            line = file.readline()
            if line == '':
                break
            list = list + [ line[:-1] ]
        return list

    def experiment(m):
        # Nice thunderstorm moving west
        tame_thunder = m.getlist("0907191[456789]*")
        # All the data available when .2. was run
        long = getlist("*")[:1490]
        # Really all data
        alldata = getlist("*")
        # Recent
        recent = alldata[-24:]

        #predict(alldata, movesq_predictor, todeg(50, 6, 0), todeg(14, 34, 0))
        # Zernovka: 50.002, 14.7519
        #history(50.002, 14.7519)
        #history_plus(recent, movesq_predictor, 50.002, 14.7519)
        history_plus(recent, movesq_predictor, todeg(50, 6, 0), todeg(14, 34, 0))
        #evaluation()

        #statistics(alldata)

    def handle_opts(m, opts):
        global chmi
        for opt, arg in opts:
            if opt == '-h':
                help()
            elif opt in ("-p", "--pos"):
                if arg == "ricany":
                    m.lat = 49.993
                    m.lon = 14.681
                elif arg == "zernovka":
                    m.lat = 50.00
                    m.lon = 14.75
                else:
                    s = arg.split(',')
                    m.lat, m.lon = list(map(string.atof, s))
            elif opt in ("-w", "--wwarn"):
                wwarn(arg)
            elif opt in ("-g", "--graph"):
                history_plus(alldata[-10:], movesq_predictor, m.lat, m.lon)
            elif opt in ("-r", "--twirl"):
                path = "/data/wnow/radar.data"
                # Thunderstorm moving north at east and south at west
                twirl = m.getlist("0907181[6789]* 0907182*")
                par_evaluation(twirl)
            elif opt in ("-e", "--experiment"):
                experiment()
            elif opt in ("--selftest"):
                chmi.selftest()
            elif opt in ("--read-bench"):
                for a in alldata[-5:]: 
                    print("Loading ", a)
                    chmi.load(a)
            elif opt in ("-s", "--show"):
                show_cross()
            elif opt in ("-d", "--download"):
                chmi.get_recent(100000)
            elif opt in ("-l", "--lightn"):
                chmi = ChmiLightn()
            elif opt in ("-o", "--old"):
                chmi = ChmiRadarOld()
            elif opt in ("--profile"):
                import profile
                m.alldata = chmi.get_recent()
                print(m.alldata)
                import profile
                profile.run("show_cross(query.alldata, query.lat, query.lon)")
            elif opt in ("--speedtest"):
                m.alldata = chmi.get_recent()
                show_cross(query.alldata, query.lat, query.lon)

            elif opt in ("-x", "--gtk"):
                m.alldata = chmi.get_recent(2)
                print(m.alldata)
                names = show_cross(m.alldata, m.lat, m.lon)
                os.system("~/g/tui/nowcast/gnowcast "+names)
            elif opt in ("-f", "--fast"):
                m.alldata = chmi.get_recent()
                names = fast_cross(m.alldata, m.lat, m.lon)
                os.system("~/g/tui/nowcast/gnowcast "+names)
            elif opt in ("--eu"):
                chmi = EuRadar()
            elif opt in ("--2k"):
                chmi = ChmiRadar2k()

chmi = None
