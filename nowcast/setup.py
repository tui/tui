from distutils.core import setup
from distutils.extension import Extension
from Pyrex.Distutils import build_ext

setup(name='cnowcast', ext_modules=[Extension("cnowcast", ["cnowcast.pyx"])],
      cmdclass = {'build_ext': build_ext}
)
