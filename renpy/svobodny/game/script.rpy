# Declare characters used by this game.
define s = Character(_("Sylvie"), color="#c8ffc8")
define m = Character(_("Me"), color="#c8c8ff")
define d = Character(_("Devil"), color="#202020")

# This is a variable that is True if you've compared a VN to a book, and False
# otherwise.
default book = False


# The game starts here.
label start:
    $ respekt = 0
    $ duvera = 0
    $ fyzicka = 0
    $ moudrost = 0
    #jump k_bezpecna_vzdalenost
    jump k_dvojity_zarez

label k_bezpecna_vzdalenost:
    # Start by playing some music.
    play music "illurock.opus"

    scene bg meadow
    with fade

    "{b}Bezpecna vzdalenost{/b}"
    "Ve staji se objevil novy konik. Mel byt pro dite..."
    "...ale nenecha na sebe sahnout."

    menu:
        "Co je horsi, nenecha cloveka prijit na dva metry. Nevezme si mrkev."
        "Zkusim s nim pomoct.":
            jump zkusim
        "To neni kun pro me.":
            jump nic_pro_me

label nic_pro_me:
    scene black
    with dissolve

    "Na svete je spousta jinych koni, se kteryma muzu pracovat."
    "Na mirnej natlak udelaji, co jim reknu."
    "A ja jim vlastne nepotrebuju rozumet."
    "{b}Bad Ending{/b}."
    return

label zkusim:
    "Jenze jak pracovat s konem, ktery si drzi dva metry od lidi? Monty Roberts dava navod jak se s konem domluvit jazykem equus."
    d "(nekdo by rekl reci tela a bice)."
    "Ale na to potrebuju mit kone v kruhovce."
    $ zajem = 0

label chytam:
    "Zatim je kun ve vybehu, a to mu zrejme vyhovuje."
    menu:
        "Kobylu uz jsem odjezdil, ted je cas na zlutaka."
        "Zkusim ho chytit.":
            $ zajem -= 1
            "Beha rychleji nez ja..."
            "..."
            "...a ma vetsi vydrz nez ja."
            jump chytam

        "Donesu mu do vybehu mrkev.":
            $ zajem += 1
            "Stojim s mrkvi, a kun stoji kus ode me. Kdyz pujdu bliz, on pujde dal. Takze mu mrkev hazim a jdu domu."
            if zajem < 0:
                "Mam pocit ze lidi fakt nemusi."
            if zajem > 1:
                "Mam pocit ze dneska prisel o kousek bliz nez minule."
            if zajem > 3:
                jump mam
            jump chytam

        "Dohodnem se ve trech lidech a zkusime ho nahnat.":
            $ zajem -= 1
            $ respekt += 1
            jump lovime
        
        "Tohle neni kun pro me.":
            jump nic_pro_me

label lovime:
    "Beha rychleji nez my."
    "..."
    "A tvari se dost odhodlane. Zkousime ho nahnat do rohu, ale vzdycky mezi nama nejak proklickuje."

    menu:
        "Nechame toho, nema to cenu.":
            "Mame pocit ze kun vyhral. Neni to dobry pocit."
            d "(Kun asi nema pocit ze vyhral. Ma pocit ze prezil. Nejspis to taky neni dobry pocit)"
            jump chytam

        "Vezmem ohradnik, ten nevyklickuje.":
            $ zajem -= 5
            "Nevyklickoval. Zahnali jsme ho do rohu."
            "Vypada dost nestastne."
            "Ted ho mame."
            "Nemame. Kdyz bylo jasne, ze neunikne..."
            "...napral to plnou rychlosti do pasky. Neni jen rychlejsi, je taky silnejsi."
            jump chytam

label mam:
    scene black
    with dissolve
    "Trvalo to dlouho, ale jednoho dne si zlutak vzal mrkev z ruky."
    "Pak se nechal pohladit."

label konec_dobry:
    "Pak si vzal ohlavku, a o par dni pozdeji jsme sli do kruhovky."
    "(a ano, zkusil jsem i to Robertsovo 'napojeni'. Nebyl to dobry napad.)"
    "Pak jsem na nej sednul..."
    "...a on me prohodil ohradou."
    "To vsechno je davno. Zlutej uz se ted chova tak jak lidi od kone ocekavaji..."
    "...teda vetsinou, pokud jsem u toho."
    "Naucil jsem se hodne o konich... mozna az moc. A ziskal jsem partaka na cely zivot."
    "(protoze prodat ho nemuzu)"
    "{b}Good Ending{/b}."
    return

label k_dvojity_zarez:
    # Start by playing some music.
    play music "illurock.opus"

    scene bg meadow
    with fade

    "{b}Dvojity zarez{/b}"
    "Jel jsem na vyjizdku do sousedniho lesa. Bez sedla, jako obvykle. Vyjizdka probihala v pohode."
    "Sjizdeli jsme prudkej kopec, jenze pod nim jsem ja chtel doleva, a kun bezel doprava."
    "Ted sedim na zemi, a vidim zlutej zadek jak cvala smerem k domovu."
    
    menu:
        "To neni dobry, domu to ma pres silnici."
        "Rozbehnu se za nim":
            $ fyzicka += 1
            "Kun je rychlejsi."
        "Zkusim na nej zavolat at zastavi":
            "No, zkusil jsem to."
        "To nevypada dobre":
            $ moudrost += 1
            "Co nadelam, snad to zvladne."

    "Zlutej ocas zmizel za zatackou. Specham za nim."
    "Dalsi zatacka..."
    menu:
        "A tady je rozcesti. Kam asi sel?"
        "Doleva.":
            "FIXME."
        "Rovne.":
            "FIXME."        
        "Doprava.":
            "FIXME."        
        "To je jedno, nejspis miri do staje, takze tam zamirim taky.":
            $ moudrost = moudrost + 1
            "Co nadelam, snad to zvladne."

    "Cestu do staje jsem zvladnul v rekordnim case. Funim jak lokomotiva."
    "Jenze kun tu neni...?!"
    menu:
        "Je cas volat policii":
            "FIXME."        
        "Zavolam majitelce staje":
            "FIXME."        
        "Paniiikaaa!":
            "FIXME."        
    "Uplynulo par minut."
    "..."
    "A od nekud prisel zlutak. Nezranenej, ale bez uzdeni. Kudy to sakra sel? Uzdeni na ceste nelezelo..."
    "A ja se rozhodl, ze pujdu ztracene uzdeni najit. Kun mi prece ukaze, kudy sel, ne? pujcil jsem si jine uzdeni, ale nevzal jsem si sedlo.."
    "Dojeli jsme na misto padu..."
    "...projeli jsme kopec..."
    "...a uzdeni nikde."
    "Co je horsi, zacina se stmivat, ja nemam poradnou baterku, a jsme jeste celkem kus od domova."
    "No nic, posledni cval... posledni skok... a pak uskok bokem."
    "Bez sedla a s baterkou v ruce nemam sanci, takze v zapeti lezim na zemi, a pozoruju odchazejiciho kone."
    menu:
        "Zkusim ho dobehnout.":
            "FIXME."        
        "Jdu v klidu za nim.":
            "FIXME."        
    "Jenze kun zrychlil, a miri k silnici. A to... neni dobry. Na silnici jezdi auta."
    "Bezim za nim, blizi se krizovatka."
    "Na silnici jezdi auta. Nejaky auto prejede myho kone."
    menu:
        "A sakra, tady zrovna jedno jede."
        "Stojim uprostred silnice se zvednutou rukou. Kone si prejet nenecham.":
            "Auto zastavuje, co mu zbyva. Ja se vrham ke dverim spolujezdce..."
            "'Utekl mi kun.'"
            "Pani bere informaci prekvapive klidne... Uklizi krabici pizzy z predniho sedadla, a jedem."
        "Postavim se na krajnici a zoufale mavam.":
            "FIXME."
        "Auta si nevsimam a bezim dal ve smeru kam bezel kun.":
            "FIXME."
    "Za zatackou se objevuje kun."
    "(Pani si mozna trochu oddychla.)"
    "Jde prostredkem silnice, v klidku, rozhlizi se kam pujde dal."
    "Nevi o me, takze ho muzeme v klidu predjet. Nasleduje scena jak z americkeho filmu: auto brzdi, lehce napric silnici a ja vystupuju."
    "Zlutej trochu kouka."
    if respekt < 5:
        "Ale stoji na miste, a ja ho v pohode chytam."
    else:
        "Pak se otaci na zadnich a vyrazi pryc."
        "Hlavne pryc ode me."
        "Ze zatacky vyjizdi auto..."
        "Z ty zatacky do ktery zlutej prave vbiha."
        "Auto brzdi, kopyta klouzou po asfaltu..."
	"Nastesti auto zastavuje kousek od kone, kterej prcha do pryc."
	"Ridic neni rad, ale ja mam jiny starosti."
        "Tentokrat to dobre dopadlo... kun dobehl do staje chvili prede mnou, a je celej."
        jump konec_spatny

    "Prisel jsem o uzdeni, ale porad mam kone. Pani v aute jsem mozna trochu vydesil, ale asi to prezije..."
    "A ani to nevypada na problemy se zakonem. Takze asi dobry."
    "{b}Good Ending{/b}."
    return

label konec_spatny:
    "Ale ne, tohle neni dobry; tohle uz nechci zazit."
    "Ublizi sobe, me, nebo nekomu jinymu."
    "Tohle neni kun pro me."
    "{b}Bad Ending{/b}."
    return
