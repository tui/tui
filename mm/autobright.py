#!/usr/bin/python3

import time
import os
import re

class AutoBrightness:

    def set_bright(m, v):
        os.system('gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.freedesktop.DBus.Properties.Set org.gnome.SettingsDaemon.Power.Screen Brightness "<int32 %d>"' % v)

    def get_step(m, v):
        l = len(m.lux_max)
        for i in range(l):
            j = l-i-1
            if v >= m.lux_max[j]:
                return j+1
        return 0

    def monitor(m):
        for l in os.popen("monitor-sensor"): # .readlines():
            s = "    Light changed: "
            if l[:len(s)] == s:
                v = float(l[len(s):len(s)+7])
                st = m.get_step(v)
                print("Lux: ", v, " step : ", st, " cur: ", m.st_cur)
                if abs(st-m.st_cur) > 1:
                    m.set_bright(m.perc[st])
                    m.st_cur = st

    def run(m):
        m.lux_max = [  3, 10, 50, 300, 500, 1000, 5000 ]
        m.perc    = [  0,  0, 15,  30,  50, 100,  100, 100 ]
        m.st_cur = -10

        m.monitor()


#set_bright(0)
#time.sleep(1)
#set_bright(100)
#time.sleep(1)
#set_bright(0)

# ~0 lux -> 0%.
# ~50 lux ->  30%.
# ~1000 lux -> 100% (basically direct).

a = AutoBrightness()
a.run()



# gdbus introspect --system --dest net.hadess.SensorProxy \
#                          --object-path /net/hadess/SensorProxy

# gdbus monitor --system --dest net.hadess.SensorProxy
#      	      --object-path /net/hadess/SensorProxyp

      # monitor-sensor

# nhttps://www.freedesktop.org/software/geoclue/docs/gdbus-org.freedesktop.GeoClue2.Client.html

# /usr/libexec/geoclue-2.0/demos/where-am-i


# introspect --system --dest org.freedesktop.GeoClue2 --object-path /org/freedesktop/GeoClue2
# gdbus monitor --system --dest org.freedesktop.GeoClue2 --object-path /org/freedesktop/GeoClue2/Client/1

# https://stackoverflow.com/questions/59677218/how-do-i-get-geoclue-geolocation-in-python-what-is-desktop-id-parameter

#dbus-send --session --print-reply  --dest="org.gnome.SettingsDaemon" /org/gnome/SettingsDaemon/Power org.gnome.SettingsDaemon.Power.Screen.SetPercentage uint32:10

#dbus-send --session --type="method_call" --dest="org.gnome.SettingsDaemon" /org/gnome/SettingsDaemon/Power org.gnome.SettingsDaemon.Power.Screen.StepUp


