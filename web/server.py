#!/usr/bin/python3
import http.server
import socketserver

PORT = 8000

Handler = http.server.CGIHTTPRequestHandler
Handler.cgi_directories = ['/cgi-bin']

httpd = socketserver.TCPServer(("", PORT), Handler)
httpd.server_name = "test server"
httpd.server_port = PORT

print("serving at port", PORT)
httpd.serve_forever()
