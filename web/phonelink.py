#!/usr/bin/python3
"""
# Tool to link computer to phone

I'd like to connect phone and (Linux) pc in such a way that I could type
SMSes on PC keyboard. This does it: bookmark 10.0.0.9:8000 on your phone,
then when you want to send an SMS run this as

./phonelink.py 800123456 say hello to spolecnost proti telefonovani.

Open the bookmark on your phone, and you should be able to send a message
with few clicks.

# Copyright 2025 Pavel Machek <pavel@ucw.cz>, GPLv3+
"""

import http.server
import socketserver
import sys

PORT = 8000

import http.server

extra = "<h1>No data filled</h1>"

class CustomHandler(http.server.CGIHTTPRequestHandler):
    def send_no_cache_headers(self):
        self.send_header("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0")
        self.send_header("Pragma", "no-cache")  # HTTP/1.0 compatibility
        self.send_header("Expires", "0")  # Forces immediate expiration
    
    def do_GET(m):
        global extra
        if m.path == "/":
            m.send_response(200)
            m.send_header("Content-type", "text/html")
            m.send_no_cache_headers()
            m.end_headers()
            s = b"""
                <!DOCTYPE html>
                <html>
                <head><title>Phone link</title></head>
                <body><font size="+5">"""
            s += extra.encode()
            s += b"""
                </font>
                </body>
                </html>
            """            
            m.wfile.write(s)
        else:
            super().do_GET()

    def handle_argv(m, argv):
        pass

def handle_link(s):
    global extra
    extra = '<h1><a href="%s">%s</a></h1>' % (s, s)
            
def handle_args(a):
    global extra
    extra = "<h1>Hello world</h1>"
    if len(a) > 1:
        if a[1] == "link":
            handle_link(a[2])
        if a[1] == "sms":
            l = "sms:" + a[2] + "?body=" + " ".join(a[3:])
            handle_link(l)
        if a[1] == "tel":
            handle_link("tel:"+a[2])

# link "sms:800123456?body=test message from foo"

Handler = CustomHandler
Handler.cgi_directories = ['/cgi-bin']
handle_args(sys.argv)
print("Extra: ", extra)

httpd = socketserver.TCPServer(("", PORT), CustomHandler)
httpd.allow_reuse_address = True
httpd.server_name = "test server"
httpd.server_port = PORT

print("serving at port", PORT)
httpd.serve_forever()
