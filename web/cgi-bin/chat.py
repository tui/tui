#!/usr/bin/python3

import cgi

class Chat:
    def interior(m):
        form = cgi.FieldStorage()
        user = form.getfirst("user", "")
        text = form.getfirst("text", "")
        if "" != text:
            print("Got text, appending.<br>")
            with open("test.txt", "a") as myfile:
                myfile.write("\n")
                myfile.write("<b>")
                myfile.write(cgi.escape(user))
                myfile.write(":</b> ")
                myfile.write(cgi.escape(text))
                myfile.write("<br>\n")
        with open("test.txt", "r") as myfile:
            print(myfile.read())

        if "" == user:
            user = "me"

        print("<hr>")

        print('<form action="./chat.py">')
        print('<input type="text" name="user" value="%s" />' % user)
        print('<input type="text" name="text" value="" />')
        print('<input type="submit" />')
        
    def run(m):

        print("Content-Type: text/html")
        print()
        print("<html><head><title>Very quick hacks</title></head><body>")
        m.interior()
        print('</form>')
        print("</body>")

chat = Chat()
chat.run()
