#!/usr/bin/python3
# cat /dev/urandom | head -c 230400 > remote.raw # that would be for 32 bpp
# cat /dev/urandom | head -c 115200 > remote.raw # that would be for 32 bpp

import cgi
import time
import sys
import os

class Image:
    def __init__(m, sx, sy):
        m.sx = sx
        m.sy = sy
        m.ar = [[0 for i in range(sx)] for j in range(sy)]
        
    def write(m, name):
        f = open(name, "wb")
        for y in range(m.sy):
            for x in range(m.sx):
                v = m.ar[y][x]
                b = v
                g = v
                r = v
                # Pixel format: Blue: 5 bit, Green: 6 bit, Red: 5 bit, Alpha 8 bit #
                # May be wrong, see tic80 code.
                f.write(bytearray([(b >> 3) & 0xf8 | (r >> 5), (g>>3 | r << 6) & 0xff,255]))

    def test(m):
        for i in range(m.sy):
            m.ar[i][m.sx-i-1] = 255
            m.ar[i][i] = 255

class Screen:
    def init(m):
        return
    
    def prepare(m, form):
        return "main"

    def render_html(m):
        print("(Please implement html render)")

    def render_rem(m):
        print("bad")
        print("""0 0 240 20 5 Wrong0 80 240 120 1 Something0 120 240 160 1 Wrong""")
        
class MessageScreen(Screen):
    def __init__(m):
        m.link = "main"

    def render_rem(m):
        print(m.me)
        print("0 0 240 240 1 " + '\n'.join(m.text))
        print("")

    def render_html(m):
        for i in m.text:
            print("<br>%s" % i)
        print('<br><a href="remote.py?mode=html&page=%s">done</a>' % ( m.link ) )

class AboutScreen(MessageScreen):
    def init(m):
        m.me = "about"
        m.text = ("About", "Even wristwatch", "should run free", "software.")

class TimeScreen(MessageScreen):
    def init(m):
        m.me = "time"
        m.text = ("Unix", "%f" % time.time(), "Local", time.strftime("%H:%M:%S"))

class MenuScreen(Screen):
    def render_html(m):
        print("<h1>%s</h1>" % m.title)
        for (link, title) in m.items:
            print('<br><a href="remote.py?mode=html&page=%s">%s</a>' % ( link, title ) )
            
    def render(m, click):
        ret = "main"
        if click == -1:
            print(m.me)
        s = "0 0 240 20 5 %s" % m.title
        y = 40
        for (link, title) in m.items:
            if click >= y and click < y+40:
                ret = link
            s += "0 %d 240 %d 1 [%s]" % (y, y+40, title)
            y += 40
        if click == -1:
            print(s)
        return ret

    def render_rem(m):
        return m.render(-1)
    
    def prepare(m, form):
        click = int(form.getvalue('y'))
        return m.render(click)

class MainMenuScreen(MenuScreen):
    def init(m):
        m.me = "main"
        m.title = "Main menu"
        m.items = [ ( "about", "About" ), ( "time", "Time" ) ]
    
class Chat:
    def run_test(m):
        print("COOKIE")
        print("""0 0 240 40 3 Works!0 80 240 240 5 This is a longer text.
Should work now.
%f
%s""" % (time.time(), form.getvalue("foo")))
        print("""0 0 240 40 3 About0 80 240 40 1 Welcome0 120 32 32 8 http://10.0.0.9:8000/remote.raw""")
        
    def run_menu(m, page, phase):
        s = None
        if page == "main":
            s = MainMenuScreen()
        if page == "about":
            s = AboutScreen()
        if page == "time":
            s = TimeScreen()
        if not s:
            return "main"
        s.chat = m
        s.init()
        if not phase:
            return s.prepare(m.form)
        if m.html:
            return s.render_html()
        return s.render_rem()

    def run(m):
        m.form = cgi.FieldStorage()
        mode = m.form.getvalue("mode")
        m.html = mode == "html"
        if m.html:
            print("Content-Type: text/html")
            print()
            m.page = m.form.getvalue("page")
            print("<html><head><body>")
        else:
            print("Content-Type: text/remotedat")
            print()
            cookie = m.form.getvalue("cookie")
            m.page = m.run_menu(cookie, False)
        if False:
            print("Mode: ", m.html)
            print("Page: ", m.page)
        m.run_menu(m.page, True)
        if m.html:
            print("</html></head></body>")

im = Image(32, 32)
im.test()
im.write("/fast/pavel/g/tui/web/remote.raw")

chat = Chat()
chat.run()
