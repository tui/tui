#!/usr/bin/python2

# Copyright (c) 2011, Andrew de Quincey
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# N900 functionality added by Par Olsson

import bluetooth
import LiveViewMessages
import sys
import time
import struct
import dbus
import gobject
import os
import sqlite3
from dbus.mainloop.glib import DBusGMainLoop
from threading import Thread

def deoctify(arr):
    referencebit = 1
    doctect = []
    bnext = 0x00
    for i in arr:
        bcurr = ((i & (0xff >> referencebit)) << referencebit) >> 1
        bcurr = bcurr | bnext
        if referencebit != 7:
            doctect.append(bcurr)
            bnext = (i & (0xff << (8 - referencebit)) ) >> 8 - referencebit
            referencebit += 1
        else:
            doctect.append(bcurr)
            bnext = (i & (0xff << (8 - referencebit)) ) >> 8 - referencebit
            doctect.append(bnext)
            bnext = 0x00
            referencebit = 1
    return ''.join([chr(i) for i in doctect])

def smscallback(pdumsg, msgcenter, somestring, sendernumber):
    global smsdirty, menustate
    msglength = int(pdumsg[18])
    msgarray = pdumsg[19:len(pdumsg)]
    msg = deoctify(msgarray)
    if msg > 0:
        smsdirty = True
        sender = sendernumber
        dbname = "/home/user/.rtcom-eventlogger/el-v1.db"
        dbquery = "SELECT remote_name FROM remotes WHERE remote_uid='" + sendernumber + "'"
        try:
            _db = sqlite3.connect(dbname)
        except sqlite3.Error, e:
            print e.args[0]
        _cur = _db.cursor()
        _cur.execute(dbquery)
        while True:
            row = _cur.fetchone()
            if row == None:
                break
            sender = row[0].encode("ascii", "replace")
        print 'Message from %s' % sender
        print 'Message length %d' % msglength
        print 'Message: %s' % msg
        menustate = 3
        clientSocket.send(LiveViewMessages.EncodeDisplayPanel("Message from " + str(sender), msg, smsPng, True))

def callcallback(obj_path, callernumber):
    global calldirty, menustate
    calldirty = True
    caller = callernumber
    dbname = "/home/user/.rtcom-eventlogger/el-v1.db"
    dbquery = "SELECT remote_name FROM remotes WHERE remote_uid='" + callernumber + "'"
    try:
        _db = sqlite3.connect(dbname)
    except sqlite3.Error, e:
        print e.args[0]
    _cur = _db.cursor()
    _cur.execute(dbquery)
    while True:
        row = _cur.fetchone()
        if row == None:
            break
        caller = row[0].encode("ascii", "replace")
    print 'Call from '+caller+'...'
    menustate = 4
    clientSocket.send(LiveViewMessages.EncodeDisplayPanel("Call from " + str(caller), "", phonePng, True))
    
def listen():
    DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    bus.add_signal_receiver(smscallback, path='/com/nokia/phone/SMS', dbus_interface='Phone.SMS', signal_name='IncomingSegment')
    bus.add_signal_receiver(callcallback, path='/com/nokia/csd/call', dbus_interface='com.nokia.csd.Call', signal_name='Coming')
    #bus.remove_signal_receiver(smsCallback, path='/com/nokia/phone/SMS', dbus_interface='Phone.SMS', signal_name='IncomingSegment')

mediaplaying = False
def mediaprevious():
    os.system("dbus-send --dest=com.nokia.mafw.renderer.Mafw-Gst-Renderer-Plugin.gstrenderer /com/nokia/mafw/renderer/gstrenderer com.nokia.mafw.renderer.previous")

def medianext():
    os.system("dbus-send --dest=com.nokia.mafw.renderer.Mafw-Gst-Renderer-Plugin.gstrenderer /com/nokia/mafw/renderer/gstrenderer com.nokia.mafw.renderer.next")

def mediaplay():
    global mediaplaying
    os.system("dbus-send --dest=com.nokia.mafw.renderer.Mafw-Gst-Renderer-Plugin.gstrenderer /com/nokia/mafw/renderer/gstrenderer com.nokia.mafw.renderer.resume")
    mediaplaying = True

def mediapause():
    global mediaplaying
    os.system("dbus-send --dest=com.nokia.mafw.renderer.Mafw-Gst-Renderer-Plugin.gstrenderer /com/nokia/mafw/renderer/gstrenderer com.nokia.mafw.renderer.pause")
    mediaplaying = False

def smssend(number):
    os.system("dbus-send --type=method_call --dest=com.nokia.HildonDesktop.AppMgr /com/nokia/HildonDesktop/AppMgr com.nokia.HildonDesktop.AppMgr.LaunchApplication string:'rtcom-messaging-ui'")

def callanswer():
    os.system("dbus-send --system --print-reply --dest=com.nokia.csd.Call /com/nokia/csd/call/1 com.nokia.csd.Call.Instance.Answer")

def callreject():
    os.system("dbus-send --system --print-reply --dest=com.nokia.csd.Call /com/nokia/csd/call com.nokia.csd.Call.Release")

def callstart(number):
    os.system("dbus-send --system --type=method_call --print-reply --dest=com.nokia.csd.Call /com/nokia/csd/call com.nokia.csd.Call.CreateWith string:" + number + " uint32:0")

class SmsItem:
    def __init__(self, startdate, sendernumber, sendername, msg):
        #print type(msg)
        self.startdate = time.asctime(time.localtime(startdate))
        self.sendernumber = str(sendernumber)
        try:
            self.sender = sendername.encode("ascii", "replace")
        except:
            self.sender = str(sendernumber)
        self.msg = msg.encode("ascii", "replace")

    def __str__(self):
        return "SmsItem: Msg:%s>" % (self.msg)

class CallItem:
    def __init__(self, startdate, callernumber, callername):
        #print type(msg)
        self.startdate = time.asctime(time.localtime(startdate))
        self.callernumber = str(callernumber)
        try:
            self.caller = callername.encode("ascii", "replace")
        except:
            self.caller = str(callernumber)

    def __str__(self):
        return "CallItem: From:%s>" % (self.caller)

smslist = []
smscurrent = 0
smsdirty = True
def smssetup():
    global smsdirty, smslist, smscurrent
    if not smsdirty:
        return
    smslist = []
    smscurrent = 0
    dbname = "/home/user/.rtcom-eventlogger/el-v1.db"
    dbquery = "SELECT events.storage_time, events.remote_uid, remotes.remote_name, events.free_text FROM events, remotes WHERE events.remote_uid=remotes.remote_uid AND events.event_type_id IN (5,7) AND events.is_read=0 ORDER BY events.id DESC"
    try:
        _db = sqlite3.connect(dbname)
    except sqlite3.Error, e:
        print e.args[0]
    _cur = _db.cursor()
    _cur.execute(dbquery)
    while True:
        row = _cur.fetchone()
        if row == None:
            break
        smslist.append(SmsItem(row[0], row[1], row[2], row[3]))
    smsdirty = False

calllist = []
callcurrent = 0
calldirty = True
def callsetup():
    global calldirty, calllist, callcurrent
    if not calldirty:
        return
    calllist = []
    callcurrent = 0
    dbname = "/home/user/.rtcom-eventlogger/el-v1.db"
    dbquery = "SELECT events.storage_time, events.remote_uid, remotes.remote_name FROM events, remotes WHERE events.remote_uid=remotes.remote_uid AND events.event_type_id IN (3) AND events.is_read=0 ORDER BY events.id DESC"
    try:
        _db = sqlite3.connect(dbname)
    except sqlite3.Error, e:
        print e.args[0]
    _cur = _db.cursor()
    _cur.execute(dbquery)
    while True:
        row = _cur.fetchone()
        if row == None:
            break
        calllist.append(CallItem(row[0], row[1], row[2]))

menuVibrationTime = 5
is24HourClock = True

#testPngFd = open("test36.png")
#testPng = testPngFd.read()
#testPngFd.close()

#testPngFd = open("test128.png")
#testPng128 = testPngFd.read()
#testPngFd.close()

testPngFd = open("menu_sms.png")
smsPng = testPngFd.read()
testPngFd.close()

testPngFd = open("menu_phone.png")
phonePng = testPngFd.read()
testPngFd.close()

testPngFd = open("menu_right.png")
nextPng = testPngFd.read()
testPngFd.close()

testPngFd = open("menu_left.png")
prevPng = testPngFd.read()
testPngFd.close()

testPngFd = open("menu_music.png")
musicPng = testPngFd.read()
testPngFd.close()

testPngFd = open("menu_media.png")
mediaPng = testPngFd.read()
testPngFd.close()

serverSocket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
serverSocket.bind(("", 1))
serverSocket.listen(1)

bluetooth.advertise_service(serverSocket, "LiveView", 
                service_classes=[ bluetooth.SERIAL_PORT_CLASS ],
                profiles=[ bluetooth.SERIAL_PORT_PROFILE ]              
                )
clientSocket, address = serverSocket.accept()

clientSocket.send(LiveViewMessages.EncodeGetCaps())

currentmenuitemid = -1
menustate = 0
def test():
    while True:
        for msg in LiveViewMessages.Decode(clientSocket.recv(4096)):
            # Handle result messages
            if isinstance(msg, LiveViewMessages.Result):
                if msg.code != LiveViewMessages.RESULT_OK:
                    print "---------------------------- NON-OK RESULT RECEIVED ----------------------------------"
                    print msg
                continue
    
            # Handling for all other messages
            clientSocket.send(LiveViewMessages.EncodeAck(msg.messageId))
            if isinstance(msg, LiveViewMessages.GetMenuItems):
                smssetup()
                callsetup()
                smscount = len(smslist)
                callcount = len(calllist)
                clientSocket.send(LiveViewMessages.EncodeGetMenuItemResponse(0, False, 0, "Next", nextPng))
                clientSocket.send(LiveViewMessages.EncodeGetMenuItemResponse(1, False, 0, "Play / Pause", musicPng))
                clientSocket.send(LiveViewMessages.EncodeGetMenuItemResponse(2, False, 0, "Previous", prevPng))
                clientSocket.send(LiveViewMessages.EncodeGetMenuItemResponse(3, True, smscount, "Conversations", smsPng))
                clientSocket.send(LiveViewMessages.EncodeGetMenuItemResponse(4, True, callcount, "Phone", phonePng))
    
            elif isinstance(msg, LiveViewMessages.GetMenuItem):
                print "---------------------------- GETMENUITEM RECEIVED ----------------------------------"
                # FIXME: do something!
    
            elif isinstance(msg, LiveViewMessages.DisplayCapabilities):
                deviceCapabilities = msg
                clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
                clientSocket.send(LiveViewMessages.EncodeSetMenuSettings(menuVibrationTime, 0))
    
            elif isinstance(msg, LiveViewMessages.GetTime):
                clientSocket.send(LiveViewMessages.EncodeGetTimeResponse(int(time.mktime(time.gmtime()) - (time.daylight * time.altzone) - time.timezone), is24HourClock))
    
            elif isinstance(msg, LiveViewMessages.DeviceStatus):
                clientSocket.send(LiveViewMessages.EncodeDeviceStatusAck())
    
            elif isinstance(msg, LiveViewMessages.GetAlert):
                currentmenuitemid = msg.menuItemId
                if msg.menuItemId == 3:
                    global smscurrent
                    smscount = len(smslist)
                    if msg.alertAction == LiveViewMessages.ALERTACTION_NEXT:
                        smscurrent += 1
                        if smscurrent > smscount - 1:
                            smscurrent = 0
                    elif msg.alertAction == LiveViewMessages.ALERTACTION_PREV:
                        smscurrent -= 1
                        if smscurrent < 0:
                            smscurrent = smscount - 1
                    elif msg.alertAction == LiveViewMessages.ALERTACTION_FIRST:
                        smscurrent = 0
                    elif msg.alertAction == LiveViewMessages.ALERTACTION_LAST:
                        smscurrent = smscount - 1
                    si = smslist[smscurrent]
                    clientSocket.send(LiveViewMessages.EncodeGetAlertResponse(smscount, smscount, smscurrent, si.startdate, si.sender, si.msg, smsPng))
                elif msg.menuItemId == 4:
                    global callcurrent
                    callcount = len(calllist)
                    if msg.alertAction == LiveViewMessages.ALERTACTION_NEXT:
                        callcurrent += 1
                        if callcurrent > callcount - 1:
                            callcurrent = 0
                    elif msg.alertAction == LiveViewMessages.ALERTACTION_PREV:
                        callcurrent -= 1
                        if callcurrent < 0:
                            callcurrent = callcount - 1
                    elif msg.alertAction == LiveViewMessages.ALERTACTION_FIRST:
                        callcurrent = 0
                    elif msg.alertAction == LiveViewMessages.ALERTACTION_LAST:
                        callcurrent = callcount - 1
                    ci = calllist[callcurrent]
                    clientSocket.send(LiveViewMessages.EncodeGetAlertResponse(callcount, callcount, callcurrent, ci.startdate, ci.caller, "", phonePng))
                else:
                    clientSocket.send(LiveViewMessages.EncodeGetAlertResponse(20, 4, 15, "TIME", "HEADER", "01234567890123456789012345678901234567890123456789", testPng))
    
            elif isinstance(msg, LiveViewMessages.Navigation):
                global menustate
                print menustate
                if menustate == 0:
                    if msg.navAction == LiveViewMessages.NAVACTION_LONGPRESS:
                        menustate = 1
                        clientSocket.send(LiveViewMessages.EncodeDisplayPanel("Track...", "Artist...", mediaPng, False))
                    elif msg.wasInAlert:
                        if currentmenuitemid == 3:
                            #smssend(smslist[smscurrent].sendernumber)
                            smssend("")
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                        elif currentmenuitemid == 4:
                            callstart(calllist[callcurrent].callernumber)
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                        else:
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    else:
                        if msg.menuItemId == 2:
                            mediaprevious()
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                        elif msg.menuItemId == 1:
                            if mediaplaying:
                                mediapause()
                            else:
                                mediaplay()
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                        elif msg.menuItemId == 0:
                            medianext()
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                        else:
                            clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_EXIT))
                elif menustate == 1:
                    if msg.navType == LiveViewMessages.NAVTYPE_LEFT:
                        mediaprevious()
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    elif msg.navType == LiveViewMessages.NAVTYPE_DOWN:
                        if mediaplaying:
                            mediapause()
                        else:
                            mediaplay()
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    elif msg.navType == LiveViewMessages.NAVTYPE_RIGHT:
                        medianext()
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    else:
                        menustate = 0
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_CANCEL))
                        clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
                elif menustate == 3:
                    if msg.navType == LiveViewMessages.NAVTYPE_RIGHT:
                        smssend("")
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    else:
                        menustate = 0
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_CANCEL))
                        clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
                elif menustate == 4:                    
                    if msg.navType == LiveViewMessages.NAVTYPE_LEFT:
                        callreject()
#                        menustate = 0
#                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_CANCEL))
#                        clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    elif msg.navType == LiveViewMessages.NAVTYPE_RIGHT:
                        callanswer()
#                        menustate = 0
#                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_CANCEL))
#                        clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_OK))
                    else:
                        menustate = 0
                        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_CANCEL))
                        clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
            else:
                    menustate = 0
                    clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_CANCEL))
                    clientSocket.send(LiveViewMessages.EncodeSetMenuSize(5))
    
        #       clientSocket.send(LiveViewMessages.EncodeSetMenuSize(0))
        #       clientSocket.send(LiveViewMessages.EncodeClearDisplay())
        #       clientSocket.send(LiveViewMessages.EncodeDisplayBitmap(100, 100, testPng))
        #       clientSocket.send(LiveViewMessages.EncodeSetScreenMode(50, False))
        #       clientSocket.send(LiveViewMessages.EncodeDisplayText("WOOOOOOOOOOOO"))
    
        #       clientSocket.send(LiveViewMessages.EncodeLVMessage(31, ""))
    
    
        #       clientSocket.send(LiveViewMessages.EncodeSetScreenMode(0, False))
        #       clientSocket.send(LiveViewMessages.EncodeClearDisplay())
        #       clientSocket.send(LiveViewMessages.EncodeLVMessage(48, struct.pack(">B", 38) + "moo"))
    
        #       tmpxxx = "MOOO"
        #       clientSocket.send(LiveViewMessages.EncodeSetMenuSize(4))
        #       clientSocket.send(LiveViewMessages.EncodeDisplayText("moo"))
    
        #       clientSocket.send(LiveViewMessages.EncodeSetStatusBar(tmp.menuItemId, 200, testPng))
                
        #       clientSocket.send(EncodeLVMessage(5, LiveViewMessages.EncodeUIPayload(isAlertItem, totalAlerts, unreadAlerts, curAlert, menuItemId, top, mid, body, itemBitmap)))
    
        #       if msg.navType == LiveViewMessages.NAVTYPE_DOWN:
        #           if not msg.wasInAlert:
        #               clientSocket.send(LiveViewMessages.EncodeDisplayPanel("TOOOOOOOOOOOOOOOOOP", "BOTTTTTTTTTTTTTTTTTOM", testPng, False))
        #           clientSocket.send(LiveViewMessages.EncodeNavigationAck(LiveViewMessages.RESULT_OK))
        #           clientSocket.send(LiveViewMessages.EncodeDisplayText("ADQ WOS HERE"))
        #       elif tmp.navType == LiveViewMessages.NAVTYPE_SELECT:
        #           clientSocket.send(LiveViewMessages.EncodeNavigationAck(LiveViewMessages.RESULT_EXIT))
                
        #       clientSocket.send(LiveViewMessages.EncodeSetVibrate(1, 1000))
    
            print msg
    
    clientSocket.close()
    serverSocket.close()

listen()
gobject.threads_init()
loop = gobject.MainLoop()
t = Thread(target=test)
t.setDaemon(True)
t.start()
loop.run()
