#!/usr/bin/python
# Copyright 2013 Pavel Machek <pavel@ucw.cz>
# Distribute under GPLv3 or BSD license.

import sys
import gobject
import bluetooth
import LiveViewMessages
import sys
import time
import struct


try:  
    import pygtk  
    pygtk.require("2.0")  
except:  
    pass  
try:  
    import gtk  
    import gtk.glade  
except:  
    print("GTK Not Available")
    sys.exit(1)

class MainWindow:
    def delete(s, widget, event=None):
        gtk.main_quit()
        return False

    def arrow_clicked(s, navType):
        sock.send(LiveViewMessages.EncodeNavigation(LiveViewMessages.NAVACTION_PRESS, navType, 0))


    def arrow(s, title, x, y, navType):
        button = gtk.Button(title)
        button.connect("clicked", lambda w: s.arrow_clicked(navType))
        s.table.attach(button, x, x+1, y, y+1)
        button.show()

    def __init__(s, sock):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("delete_event", s.delete)
        window.set_border_width(10)

        s.table = gtk.Table(3,4,False)
        window.add(s.table)

        s.arrow("^", 1, 1, LiveViewMessages.NAVTYPE_UP)
        s.arrow("<", 0, 2, LiveViewMessages.NAVTYPE_LEFT)
        s.arrow(">", 2, 2, LiveViewMessages.NAVTYPE_RIGHT)
        s.arrow("v", 1, 3, LiveViewMessages.NAVTYPE_DOWN)

        s.table.show()
        window.show()

        vbox = gtk.VBox(False, 0)
        vbox.show()
        s.table.attach(vbox, 1, 2, 2, 3)

        s.top = gtk.Label("(top)")
        s.top.show()
        vbox.add(s.top)

        s.bottom = gtk.Label("(bottom)")
        s.bottom.show()
        vbox.add(s.bottom)

        gobject.io_add_watch(sock, gobject.IO_IN, s.input)

    def input(s, source, condition):
        print "Data ready"
	raw = sock.recv(4096)
	for msg in LiveViewMessages.Decode(raw):
		print msg
		if isinstance(msg, LiveViewMessages.GetCapabilities):
			sock.send(LiveViewMessages.EncodeDisplayCapabilities())
		elif isinstance(msg, LiveViewMessages.SetMenuSettings):
			sock.send(LiveViewMessages.EncodeGetMenuItems())
		elif isinstance(msg, LiveViewMessages.DisplayPanel):
			print "Got text to display..."
			print msg.top_text
                        s.top.set_text(msg.top_text)
                        s.bottom.set_text(msg.bottom_text)
		else: print "Don't know how to react"
        return True

    def do_power(s, b):
        print "hello world"

    def quit(s, widget):
        sys.exit(0)


sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
# duo
sock.connect(("00:16:CE:EC:4D:C2", 1))
# maemo
#sock.connect(("5C:57:C8:77:CE:5C", 1))
print "Connected"

# Try to discover?
# http://people.csail.mit.edu/albert/bluez-intro/c212.html

time.sleep(1)
m = MainWindow(sock)
print "Window ready"
gtk.main()

