#!/usr/bin/python


# Copyright (c) 2011, Andrew de Quincey
# Copyright (c) 2013, Pavel Machek <pavel@ucw.cz>
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import bluetooth
import LiveViewMessages
import sys
import time
import struct

sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
#sock.connect(("00:16:CE:EC:4D:C2", 1))
#sock.connect(("5C:57:C8:77:CE:5C", 1))
sock.connect(("6C:23:B9:9B:57:7D", 1))
print "Connected"

# Try to discover?
# http://people.csail.mit.edu/albert/bluez-intro/c212.html

time.sleep(1)

while True:
	raw = sock.recv(4096)
	for msg in LiveViewMessages.Decode(raw):
		print msg
		if isinstance(msg, LiveViewMessages.GetCapabilities):
			sock.send(LiveViewMessages.EncodeDisplayCapabilities())
		elif isinstance(msg, LiveViewMessages.SetMenuSettings):
			sock.send(LiveViewMessages.EncodeGetMenuItems())
		elif isinstance(msg, LiveViewMessages.DisplayPanel):
			print "Got text to display..."
			print msg.top_text
			sock.send(LiveViewMessages.EncodeNavigation(LiveViewMessages.NAVACTION_PRESS, LiveViewMessages.NAVTYPE_UP, 0))
		else: print "Don't know how to react"

sock.close()
