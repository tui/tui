#!/usr/bin/python
# If you have CONFIG_RFCOMM_EMULATION enabled in the kernel, it will fight
# over the port. See also http://ubuntuforums.org/showthread.php?t=2056285 .


# Copyright (c) 2011, Andrew de Quincey
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import bluetooth
import LiveViewMessages
import sys
import time
import struct
import os


menuVibrationTime = 5
is24HourClock = True

testPngFd = open("test36.png")
testPng = testPngFd.read()
testPngFd.close()

testPngFd = open("test128.png")
testPng128 = testPngFd.read()
testPngFd.close()

serverSocket = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
serverSocket.bind(("",1))
print "BT ready"
serverSocket.listen(1)

bluetooth.advertise_service(serverSocket, "LiveView", 
			    service_classes=[ bluetooth.SERIAL_PORT_CLASS ],
			    profiles=[ bluetooth.SERIAL_PORT_PROFILE ]
			    )
clientSocket, address = serverSocket.accept()
print "Got client"

msg = LiveViewMessages.EncodeGetCaps()
print msg
clientSocket.send(msg)
print "Sent caps"

def display(t1, t2):
    clientSocket.send(LiveViewMessages.EncodeDisplayPanel(t1, t2, testPng, False))

def pcmd(c):
    return os.popen(c).readline()[:-1]

class MenuItem:
    def __init__(s, text, help, select, up, down):
        s.text = text
	s.help = help
	s.select = select
	s.up = up
	s.down = down

    def on_select(s, msg):
        s.select()

    def on_navigation(s, msg):
        s.down()

class Decoder:
    def run(s):
        s.keep_running = True
        while s.keep_running:
	    for msg in LiveViewMessages.Decode(clientSocket.recv(4096)):
		print "Got message", msg
		# Handle result messages
		if isinstance(msg, LiveViewMessages.Result):
			if msg.code != LiveViewMessages.RESULT_OK:
				print "---------------------------- NON-OK RESULT RECEIVED ----------------------------------"
				print msg
			continue

		# Handling for all other messages
		clientSocket.send(LiveViewMessages.EncodeAck(msg.messageId))
		if isinstance(msg, LiveViewMessages.GetMenuItems):
			s.encode_menu_items()

		elif isinstance(msg, LiveViewMessages.GetMenuItem):
			print "---------------------------- GETMENUITEM RECEIVED ----------------------------------"
			# FIXME: do something!

		elif isinstance(msg, LiveViewMessages.DisplayCapabilities):
			deviceCapabilities = msg
			
			s.encode_menu_size()
			clientSocket.send(LiveViewMessages.EncodeSetMenuSettings(menuVibrationTime, 15))

		elif isinstance(msg, LiveViewMessages.GetTime):
			clientSocket.send(LiveViewMessages.EncodeGetTimeResponse(time.time()+3600*2, is24HourClock))

		elif isinstance(msg, LiveViewMessages.DeviceStatus):
			clientSocket.send(LiveViewMessages.EncodeDeviceStatusAck())

		elif isinstance(msg, LiveViewMessages.GetAlert):
                    s.display_alert(msg)

# Select in Menu produces GetAlert
# Up/Down direction in menu prodcues Navigation action 
		elif isinstance(msg, LiveViewMessages.Navigation):
                    s.handle_navigation(msg)

		print msg

class Menu(Decoder):
    def unimplemented(s):
	    print "Unimplemented"

    def encode_menu_items(s):
	i = 0
        for item in s.items:
	    clientSocket.send(LiveViewMessages.EncodeGetMenuItemResponse(i, True, 0, item.text, testPng))
	    i += 1

    def encode_menu_size(s):
        clientSocket.send(LiveViewMessages.EncodeSetMenuSize(len(s.items)))
	print "Menu has ", len(s.items), " entries."

    def display_alert(s, msg):
        item = s.items[msg.menuItemId]
        print "Pressed select on item ", item.text
        item.on_select(msg)

    def handle_navigation(s, msg):
        item = s.items[msg.menuItemId]
        print "Pressed up/down on item ", item.text
        clientSocket.send(LiveViewMessages.EncodeNavigationResponse(LiveViewMessages.RESULT_EXIT))
        item.on_navigation(msg)

    def submenu(s, dec):
        dec.encode_menu_size()
        dec.run()
        s.encode_menu_size()

    def display_command(s, cmd):
        clientSocket.send(LiveViewMessages.EncodeGetAlertResponse(1, 1, 1, "now", "", pcmd(cmd), testPng))

    def close(s):
        s.keep_running = False

class MainMenu(Menu):
    def __init__(s):
        items = []
	items.append(MenuItem("Music", "Control music player", lambda: s.display_command(""), lambda: s.submenu(SubMenu()), lambda: s.submenu(SubMenu())))
	items.append(MenuItem("Status", "Display system status", lambda: s.display_command("ps"), s.unimplemented, s.unimplemented))
	items.append(MenuItem("Herd", "Manipulate other machines", s.unimplemented, s.unimplemented, s.unimplemented))
	s.items = items

class SubMenu(Menu):
    def __init__(s):
        items = []
	items.append(MenuItem("Play", "Control music player", s.unimplemented, s.unimplemented, s.unimplemented))
	items.append(MenuItem("Next", "Control music player", s.unimplemented, s.unimplemented, s.unimplemented))
	items.append(MenuItem("Volume", "Control music player", s.unimplemented, s.unimplemented, s.unimplemented))
	items.append(MenuItem("Exit", "Manipulate other machines", s.unimplemented, s.unimplemented, lambda: s.close()))
	s.items = items

dec = MainMenu()
dec.run()

clientSocket.close()
serverSocket.close()
