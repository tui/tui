/*
   Crazy self-expanding benchmark

   time gcc -O3 expanding.c && time  ./a.out
   gcc -E expanding.c | wc -l
*/

#ifndef HAVE_MAIN

void
main(void)
{
  volatile double total_sum;

#define HAVE_MAIN
#include "expanding.c"

  printf("%f == 24502500\n", total_sum);
}

#else

#ifndef LEN
#define LEN 1000
#include "expanding.c"
#define LEN 2000
#include "expanding.c"
#define LEN 3000
#include "expanding.c"
#else

#ifndef TYPE_SUM
#define TYPE_SUM char
#include "expanding.c"
#define TYPE_SUM short
#include "expanding.c"
#define TYPE_SUM int
#include "expanding.c"
#define TYPE_SUM long
#include "expanding.c"
#define TYPE_SUM long long
#include "expanding.c"
#define TYPE_SUM float
#include "expanding.c"
#define TYPE_SUM double
#include "expanding.c"
#undef TYPE_SUM

#else

#ifndef TYPE_VAL
#define TYPE_VAL char
#include "expanding.c"
#define TYPE_VAL short
#include "expanding.c"
#define TYPE_VAL int
#include "expanding.c"
#define TYPE_VAL long
#include "expanding.c"
#define TYPE_VAL long long
#include "expanding.c"
#define TYPE_VAL float
#include "expanding.c"
#define TYPE_VAL double
#include "expanding.c"
#undef TYPE_VAL

#else

#ifndef ARRAY_OP
#define ARRAY_OP(a, b, c) a + b * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+1) * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+2) * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+3) * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+4) * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+5) * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+6) * c
#include "expanding.c"
#define ARRAY_OP(a, b, c) a + (b+7) * c
#include "expanding.c"
#undef ARRAY_OP
#else

#define INIT_OP(a) a + 1

{
  int i, j;
  TYPE_SUM v = 0, sum;
  TYPE_VAL array[LEN];

  for (i=0; i<LEN; i++) {
    array[i] = v;
    v = INIT_OP(v);
  }

  asm volatile("");

  sum = 0;
  for (i=0; i<LEN; i++)
    for (j=0; j<LEN; j++) {
      sum = ARRAY_OP(sum, array[i], array[j]);
  }
  total_sum += sum;
  printf("%d-%d-%d ", LEN, sizeof(TYPE_VAL), sizeof(TYPE_SUM));
}
#endif // ARRAY_OP
#endif // TYPE_VAL
#endif // TYPE_SUM
#endif // LEN
#endif
